#ifndef _EP_IR_OCV_DEFINE_
#define _EP_IR_OCV_DEFINE_

#define DEFAULT_IROCV_CHANNEL_COUNT	128

#define COM_STX			0x02
#define COM_ETX			0x0d
#define COM_ESC			0x1B
#define CMD_HEADER		COM_STX
#define CMD_ERROR		'!'

#define METER_ID_HD	"*IDN HIOKI,3560,0,V1.02"
#define METER_ID	"HIOKI,3560,0,V1.02"
#define NVRAM_VER	"EP011013"
#define CMD_DELAY	50

#define MSG_TIME_OUT	1000

#define NOT_GRADE				0
#define MAX_NVRAM_RD_RETRY		10
#define MAX_WITE_RETRY			5
#define MAX_METER_READ_RETRY	5

#define METER_RESPONSE_INTERVAL		200		//200ms

#define NON_CELL_IMP	99900				//999.9 Ohm
#define NON_CELL_VTG	190000				//19.999 V


#define METER_HIOKI		0
#define METER_ADEX		1

#endif //_EP_IR_OCV_DEFINE_