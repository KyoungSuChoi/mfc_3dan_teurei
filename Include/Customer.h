/********************************************************************
	created:	2002/11/01 -> 2008/11/05
	created:	1:11:2002   19:43
	file base:	Customer
	file ext:	h
	author:	 ljb	
	
	purpose:	add -> Tray Type 
*********************************************************************/

#ifndef _SPECIAL_HEADER_FILE_
#define _SPECIAL_HEADER_FILE_

#define LG_CHEMICAL		1		//세로 번호 체계, OnLine Mode 지원
#define FINE_CELL		2		//가로 번호 체계 
#define LGC_PB5			4
#define LSC				FINE_CELL

#define CUSTOMER_TYPE	SKI

#define TRAY_TYPE_CUSTOMER			0		//사용자 기본 (가로),레지스트리 COL 사용
#define TRAY_TYPE_LG_ROW_COL		1		//세로  번호 체계
#define TRAY_TYPE_LG_COL_ROW		2		//가로  번호 체계
#define TRAY_TYPE_PB5_ROW_COL		3		//각형 세로  번호 체계
#define TRAY_TYPE_PB5_COL_ROW		4		//각형 가로  번호 체계
#define TRAY_TYPE_LG_TEST			5		//CH:151 22, 21, 22, 21, 22, 21, 22


#endif //_SPECIAL_HEADER_FILE_