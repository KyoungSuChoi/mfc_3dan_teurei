#pragma once
//////////////////////////////////////////////////////////////////////////
static TCHAR *g_Test_pMsg = "@FM02IMS     eFMT2F01000220130213000000   0000000:";
//////////////////////////////////////////////////////////////////////////
#define MSGSTR '@'
#define MSGEND "*;"

#define LOCALPC_COMMAND 0x1000

#define MAX_SBC_COUNT 144	//20190925 KSJ  104 -> 144
#define MAX_CELL_COUNT 120
#define MAX_TRAYCELL_COUNT 40

#define FMS_HEAD_LEN 50

CONST INT MAX_BUFFER_LENGTH = 32768;
// CONST INT MAX_BUFFER_LENGTH = 229376;
CONST INT MAX_RESULT_BUFFER_LENGTH = 210000; // 203282
//////////////////////////////////////////////////////////////////////////
//ST MA
//장비 내부 타이머 정의
#define FMSSM_MAIN_TIMER	1000 //변경 되면 FM_SECOND변경 한다.
#define FMSSM_SECOND		FMSSM_MAIN_TIMER / 1000
#define FMSSM_MINITE		FMSSM_SECOND * 60
#define FMSSM_HOUR			FMSSM_MINITE * 60

#define ELEMENT_VALUE_MAX_LEN	32
#define FMS_ERROR_MSG_LEN		256

#define RETRY_IMPORT_ON		0x80000000//재시도 후 DOWN 상태로
#define RETRY_NOMAL_ON		0x40000000//재시도
#define RETRY_ALARM_ON		0x20000000//타이머 완료시 알람창
#define RETRY_CHECK			0x10000000//재시도중 매번 체크
#define RETRY_OFF			0x7FFFFFFF

typedef enum _FMS_STATE_CODE
{
	FMS_ST_OFF
	, FMS_ST_VACANCY
	, FMS_ST_TRAY_IN
	, FMS_ST_RUNNING
	, FMS_ST_END
	, FMS_ST_ERROR
	, FMS_ST_READY
	, FMS_ST_CONTACT_CHECK
	, FMS_ST_LOCAL_OPERATION_NOT_USE
	, FMS_ST_RED_READY
	, FMS_ST_BLUE_END
	, FMS_ST_RED_END
	, FMS_ST_RED_TRAY_IN
	, FMS_ST_CODE_END
	, FMS_ST_CALIBRATION		//19-02-12 엄륭 MES 사양 추가 관련
}FMS_STATE_CODE;

static TCHAR g_str_FMS_State[FMS_ST_CALIBRATION][ELEMENT_VALUE_MAX_LEN] = 
{
	TEXT("FMS_ST_OFF")
	, TEXT("FMS_ST_VACANCY")
	, TEXT("FMS_ST_TRAY_IN ")
	, TEXT("FMS_ST_RUNNING")
	, TEXT("FMS_ST_END")
	, TEXT("FMS_ST_ERROR")
	, TEXT("FMS_ST_READY")
	, TEXT("FMS_ST_CONTACT_CHECK")
	, TEXT("FMS_ST_LOCAL_OPERATION_NOT_USE")
	, TEXT("FMS_ST_RED_READY")
	, TEXT("FMS_ST_BLUE_END")
	, TEXT("FMS_ST_RED_END")
	, TEXT("FMS_ST_RED_TRAY_IN")
	, TEXT("FMS_ST_CALIBRATION")	//19-02-12 엄륭 MES 사양 추가 관련
};
typedef enum _FM_STATUS_ID
{
	EQUIP_ST_OFF

	, EQUIP_ST_AUTO
	,	AUTO_ST_VACANCY
	,	AUTO_ST_READY
	,	AUTO_ST_TRAY_IN
	,	AUTO_ST_CONTACT_CHECK
	,	AUTO_ST_RUN
	,	AUTO_ST_ERROR
	,	AUTO_ST_END

	, EQUIP_ST_CALI			//KSJ 20190902
	,	CALI_ST_VACANCY
	,	CALI_ST_READY
	,	CALI_ST_TRAY_IN
	,	CALI_ST_CALI
	,	CALI_ST_ERROR
	,	CALI_ST_END

	, EQUIP_ST_LOCAL
	,	LOCAL_ST_LOCAL
	,	LOCAL_ST_ERROR

	, EQUIP_ST_MAINT
	,	MAINT_ST_MAINT
	,	MAINT_ST_ERROR


	, EQIP_ST_END
}FM_STATUS_ID;

static TCHAR g_str_State[EQIP_ST_END][ELEMENT_VALUE_MAX_LEN] = 
{
	TEXT("OFF")

	, TEXT("AUTO")
	, TEXT("VACANCY")
	, TEXT("READY")
	, TEXT("TRAY_IN")
	, TEXT("CONTACT_CHECK")
	, TEXT("RUNNING")
	, TEXT("ERROR(AUTO)")
	, TEXT("END")
	//20190903 KSJ
	, TEXT("CALIBRATION")
	, TEXT("CALI_VACANCY")
	, TEXT("CALI_READY")
	, TEXT("CALI_TRAY_IN")
	, TEXT("CALI_CALI")
	, TEXT("CALI_ERROR")
	, TEXT("CALI_END")

	, TEXT("LOCAL")
	, TEXT("LO_LOCAL")
	, TEXT("ERROR(LOCAL)")

	, TEXT("MAINT")
	, TEXT("MA_LOCAL")
	, TEXT("ERROR(MAINT)")
};
typedef enum _FM_TRAY_STATUS_ID
{
	TRAY_ST_NONE
	, TRAY_ST_IN
	, TRAY_ST_INED
	, TRAY_ST_OUT
	, TRAY_ST_OUTED
	, TRAY_ST_END
}FM_TRAY_STATUS_ID;

static TCHAR g_str_TRAY_STATUS[TRAY_ST_END][ELEMENT_VALUE_MAX_LEN] = 
{
	TEXT("NONE")
	, TEXT("TRAY_IN")
	, TEXT("TRAY_INED")
	, TEXT("TRAY_OUT")
	, TEXT("TRAY_OUTED")
};

typedef enum _PROC_FN_ID
{
	FNID_ENTER
	, FNID_PROC
	, FNID_EXIT
}PROC_FN_ID;

typedef enum tag_FMS_ERROR
{
	FR_NONE

	,FR_END

}MES_ERROR;

static const TCHAR g_str_MES_ERROR[FR_END][256] = 
{
	TEXT("")
};

typedef enum tag_FMS_ERRORCODE
{
	FMS_ER_NONE
	, ER_Data_Error
	, ER_Status_Error

	, ER_Already_Reserve
	, ER_fnLoadWorkInfo
	, ER_fnTransFMSWorkInfoToCTSWorkInfo
	, ER_fnMakeTrayInfo
	, ER_fnSendConditionToModule
	, ER_FMS_Recv_Error_State

	, ER_Run_Fail				// 충방전기 센서 초기화 에러
	, ER_SBC_ConditionError		// 충방전기 센서 초기화 에러
	, ER_FMS_ConditionError
	, ER_SBC_NetworkError	

	, ER_File_Not_Found
	, ER_File_Open_error
	, ER_Contact_Error
	, ER_Capa_Error
	, ER_Over_Charge

	, ER_TrayID

	, ER_Settiing_Data_error
	, ER_Stage_Setting_Error	

	, ER_NO_Process

	, ER_Time_Set_Error

	, ER_Tray_In

	, ER_END
	, ER_fnRecipeOverC_Error	//20200411 엄륭 용량 상한
}FMS_ERRORCODE;

static TCHAR g_str_FMS_ERROR[ER_END][FMS_ERROR_MSG_LEN] = 
{
	TEXT("")
	, TEXT("ER_Data_Error")
	, TEXT("ER_Status_Error")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Already_Reserve")
	, TEXT("ER_fnLoadWorkInfo")
	, TEXT("ER_fnTransFMSWorkInfoToCTSWorkInfo")
	, TEXT("ER_fnMakeTrayInfo")
	, TEXT("ER_fnSendConditionToModule")
	, TEXT("ER_FMS_Recv_Error_State")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Run_Fail")
	, TEXT("ER_SBC_ConditionError")
	, TEXT("ER_FMS_ConditionError")
	, TEXT("ER_SBC_NetworkError")	
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_File_Not_Found")
	, TEXT("ER_File_Open_error")
	, TEXT("ER_Contact_Error")
	, TEXT("ER_Capa_Error")
	, TEXT("ER_Over_Charge")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_TrayID")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Settiing_Data_error")
	, TEXT("ER_Stage_Setting_Error")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_NO_Process")
	//////////////////////////////////////////////////////////////////////////
	, TEXT("ER_Tray_In")
};

typedef struct _st_HSMSMSGHEAD
{	
	short	nDevId;
	short	nStrm;
	short	nFunc;
	BYTE	nId;
	long	lSysByte;	
}st_HSMSMSGHEAD;

typedef struct _st_HSMS_PACKET
{
	st_HSMSMSGHEAD head;
	CHAR data[4096];
}st_HSMS_PACKET;
//////////////////////////////////////////////////////////////////////////

typedef struct _st_S1F2
{
	st_HSMSMSGHEAD head;	
	CHAR MDLN[4+1];
	CHAR SOFTREV[6+1];
}st_S1F2;

typedef struct _st_S1F13
{
	st_HSMSMSGHEAD head;	
	CHAR MDLN[4+1];
	CHAR SOFTREV[6+1];
}st_S1F13;

typedef struct _st_S1F14
{
	st_HSMSMSGHEAD head;
	BYTE COMMACK;
	CHAR MDLN[4+1];
	CHAR SOFTREV[6+1];
}st_S1F14;

typedef struct _st_S1F17
{
	st_HSMSMSGHEAD head;
	CHAR STAGENUMBER[5+1];
	WORD CTRLMODE;
}st_S1F17;

typedef struct _st_S1F18
{
	st_HSMSMSGHEAD head;
	CHAR STAGENUMBER[5+1];
	BYTE ACK;
}st_S1F18;


// typedef struct _st_S1F101
// {
// 	st_HSMSMSGHEAD head;
// 	st_IMPOSSBLE_STATE EQ_State[MAX_SBC_COUNT]; 
// 	int StageCount;
// }st_S1F101;

typedef struct _st_IMPOSSBLE_STATE
{
	CHAR STAGENUMBER[5+1];
	WORD CTRLMODE;
	WORD EQSTATUS;
}st_IMPOSSBLE_STATE;

typedef struct _st_S1F101
{
	st_HSMSMSGHEAD head;
	st_IMPOSSBLE_STATE EQ_State[MAX_SBC_COUNT]; 
	INT STAGETOTALCOUNT;
}st_S1F101;


typedef struct _st_S1F6
{	
	BYTE SFCD;
	USHORT STAGETOTALCOUNT;
	CHAR TRAYTYPE[128+1];	
	st_IMPOSSBLE_STATE EQ_State[MAX_SBC_COUNT]; 
}st_S1F6;


typedef struct _st_NEW_IMPOSSBLE_STATE
{
	CHAR STAGENUMBER[5+1];
	WORD CTRLMODE;
	WORD EQSTATUS;
	CHAR LINENO[1+1];
}st_NEW_IMPOSSBLE_STATE;

typedef struct _st_S1F102
{	
	st_NEW_IMPOSSBLE_STATE EQ_State[MAX_SBC_COUNT]; 
	INT STAGETOTALCOUNT;
}st_S1F102;

typedef struct _st_S2F32
{	
	BYTE ACK;
}st_S2F32;

typedef struct _st_S2F411
{
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	FMS_ERRORCODE fmsERCode;
}st_S2F411;

typedef struct _st_S2F412
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;	
}st_S2F412;

typedef struct _st_S2F413
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;	
}st_S2F413;

typedef struct _st_S2F414
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;	
}st_S2F414;

typedef struct _st_S2F415
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	WORD MRCD;
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;	
}st_S2F415;

typedef struct _st_S2F416
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;	
}st_S2F416;

typedef struct _st_S2F417
{
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	FMS_ERRORCODE fmsERCode;
}st_S2F417;
typedef struct _st_S2F418 //19-01-25 엄륭 MES 사양 추가
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;	
}st_S2F418;

typedef struct _st_S2F419 //19-01-25 엄륭 MES 사양 추가
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;	
}st_S2F419;

typedef struct _st_S2F4110 //19-01-25 엄륭 MES 사양 추가
{	
	BYTE RCMD;
	CHAR STAGENUMBER[5+1];
	CHAR LINENO[1+1];
	
}st_S2F4110;


typedef struct _st_S2F42
{	
	BYTE ACK;
	CHAR STAGENUMBER[5+1];
// 	CHAR TRAYID_1[10+1];
// 	WORD TRAYPOSITION_1;
// 	CHAR TRAYID_2[10+1];
// 	WORD TRAYPOSITION_2;	
}st_S2F42;

typedef struct _st_S5F1
{	
	CHAR STAGENUMBER[5+1];
	WORD ALST;
	WORD ALID;
}st_S5F1;

// 20201118 KSCHOI Add Break Fire Report START
typedef struct _st_S5F3
{
	WORD STATUS;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S5F3;
// 20201118 KSCHOI Add Break Fire Report END

typedef struct _st_S5F101
{
	CHAR STAGENUMBER[5+1];
	WORD MRCD;
}st_S5F101;

typedef struct _st_S5F102
{	
	BYTE ACK;
}st_S5F102;

typedef struct _st_S6F11C1
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C1;

typedef struct _st_S6F11C4
{
	CHAR STAGENUMBER[5+1];
	FMS_ERRORCODE fmsERCode;
}st_S6F11C4;

typedef struct _st_S6F11C5
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C5;

typedef struct _st_S6F11C6
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C6;

typedef struct _st_S6F11C7
{
	CHAR STAGENUMBER[5+1];
	FMS_ERRORCODE fmsERCode;
}st_S6F11C7;

typedef struct _st_S6F11C11
{
	CHAR STAGENUMBER[5+1];
	USHORT CTRLMODE;
	USHORT EQSTATUS;
}st_S6F11C11;

typedef struct _st_S6F11C12
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C12;

typedef struct _st_S6F11C13
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C13;

typedef struct _st_S6F11C14
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C14;

typedef struct _st_S6F11C15
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C15;

typedef struct _st_S6F11C16  //19-01-24 엄륭 헝가리 MES 사양 추가
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C16;

typedef struct _st_S6F11C17  //19-01-24 엄륭 헝가리 MES 사양 추가
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C17;

typedef struct _st_S6F11C18  //19-01-24 엄륭 헝가리 MES 사양 추가
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C18;

typedef struct _st_S6F11C19  //19-01-24 엄륭 헝가리 MES 사양 추가
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C19;

typedef struct _st_S6F11C200
{
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F11C200;

typedef struct _st_S6F12
{
	BYTE ACK;
	CHAR STAGENUMBER[5+1];
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
}st_S6F12;

typedef struct _st_S6F11C100
{
	CHAR STAGENUMBER[5+1];
	USHORT CTRLMODE;
	USHORT EQSTATUS;
}st_S6F11C100;

typedef enum _FMS_Step_Type
{
	FMS_SKIP
	, FMS_CHG
	, FMS_DCHG
	, FMS_REST
	, FMS_OCV
	, FMS_CC_CHARGE
	, FMS_CC_CHARGE_FANOFF
	, FMS_COMMON			// 공통적용
	, FMS_TEST				
	, FMS_DCIR				// 19-01-09 엄륭 DCIR 관련 MES 수정
}FMS_Step_Type;

//////////////////////////////////////////////////////////////////////////
#define RESULT_CODE_OK						'0'

#define RESULT_CODE_DATA_ERROR				'1'
#define RESULT_CODE_STATUS_ERROR			'2'

#define RESULT_CODE_ALREADY_RESERVED		'2'

#define RESULT_CODE_INSPECTION				'3'

#define RESULT_CODE_STEP_SEND				'5'
#define RESULT_CODE_ERROR_STATE				'6'

#define RESULT_CODE_RUN_FAIL				'6'

#define RESULT_CODE_CONTACT_ERROR			'4'
#define RESULT_CODE_CAPA_ERROR				'5'
#define RESULT_CODE_OVER_CHARGE				'6'

#define RESULT_CODE_TRAY_ID_ERROR			'4'

#define RESULT_CODE_MIS_SEND				'2'
#define RESULT_CODE_FILE_OPEN_ERROR			'3'

#define RESULT_CODE_SETTING_DATA_ERROR		'3'
#define RESULT_CODE_STAGE_SETTING_ERROR		'4'

#define RESULT_CODE_TIME_SET_FAIL			'1'

#define RESULT_CODE_TRAY_IN_ERROR			'3'

#define RESULT_UNKNOW_ERROR					'9'


typedef enum tag_STEP_VALUE_RANGE
{
	Range_S_I
	, Range_S_V
	, Range_I
	, Range_V
	, Range_End_V
	, Range_SOC
	, Range_S_Min_Time
	, Range_Min_Time
	, Range_S_Sec_Time
	, Range_Sec_Time
	, Range_Capa
}STEP_VALUE_RANGE;

// RESULTDATA_STATE
// 값의 순서가 바뀌면 DB 정보와 매칭이 안됨
// 변경시 주의 - 마지막에 추가할것
// [2013/9/5 cwm]
typedef enum tag_RESULTDATA_STATE
{
	RS_NONE
	, RS_RUN
	, RS_END
	, RS_REPORT
	, RS_ERROR
}RESULTDATA_STATE;

/*
typedef struct _st_SEND_MIS_INFO
{
	UINT64 idxKey;
	CHAR szFMTFilePath[256];
}st_SEND_MIS_INFO;

typedef enum tag_RESPONS_RESERVE_STATE
{
	RRS_NONE
	, RRS_RECVSET
	, RRS_TIME_OUT
	, RRS_DEL

}RESPONS_RESERVE_STATE;

typedef struct _st_RESPONS_RESERVE
{
	FMS_COMMAND fmsCommand;
	UINT timer;
	RESPONS_RESERVE_STATE RRState;
	FMS_ERRORCODE fmsERCode;

}st_RESPONS_RESERVE;

#define HEAET_BEAT_F										"0001"
#define TIME_SET											"0002"
#define ERROR_NOTICE										"0011"	//FMS->PC
#define EQ_ERROR_RESPONSE									"0012"	//PC->FMS
//공통 명령

#define CHARGER_IMPOSSBLE									"0101"
#define CHARGER_INRESEVE									"0102"
#define CHARGER_IN											"0103"
#define CHARGER_WORKEND_RESPONSE							"0104"
#define CHARGER_RESULT										"0105"
#define CHARGER_RESULT_RECV									"0106"
#define CHARGER_OUT											"0107"
#define CHARGER_MODE_CHANGE									"0108"
#define CHARGER_STATE_RESPONSE								"0109"

#define CHARGER_IMPOSSBLE_RESPONSE							"1101"
#define CHARGER_INRESEVE_RESPONSE							"1102"
#define CHARGER_IN_RESPONSE									"1103"
#define CHARGER_WORKEND										"1104"
#define CHARGER_RESULT_RESPONSE								"1105"
#define CHARGER_RESULT_RECV_RESPONSE						"1106"
#define CHARGER_OUT_RESPONSE								"1107"
#define CHARGER_MODE_CHANGE_RESPONSE						"1108"
#define CHARGER_STATE										"1109"

#define HEAET_BEAT_P										"1001"
#define TIME_SET_RESPONSE									"1002"
#define ERROR_NOTICE_RESPONSE								"1011"
#define EQ_ERROR											"1012"
//공통 명령                              

typedef enum tag_FMS_COMMAND
{
	E_HEAET_BEAT_F											= 1
	, E_TIME_SET											= 2
	, E_ERROR_NOTICE										= 11	//FMS->PC
	, E_EQ_ERROR_RESPONSE									= 12	//PC->FMS

	, E_CHARGER_IMPOSSBLE									= 101
	, E_CHARGER_INRESEVE									= 102
	, E_CHARGER_IN											= 103
	, E_CHARGER_WORKEND_RESPONSE							= 104
	, E_CHARGER_RESULT										= 105
	, E_CHARGER_RESULT_RECV									= 106
	, E_CHARGER_OUT											= 107
	, E_CHARGER_MODE_CHANGE									= 108
	, E_CHARGER_STATE_RESPONSE								= 109	
}FMS_COMMAND;

typedef enum tag_FMS_OP_COMMAND
{
	E_HEAET_BEAT_P											= 1001
	, E_TIME_SET_RESPONSE									= 1002
	, E_ERROR_NOTICE_RESPONSE								= 1011
	, E_EQ_ERROR											= 1012
	
	, E_CHARGER_IMPOSSBLE_RESPONSE							= 1101
	, E_CHARGER_INRESEVE_RESPONSE							= 1102
	, E_CHARGER_IN_RESPONSE									= 1103
	, E_CHARGER_WORKEND										= 1104
	, E_CHARGER_RESULT_RESPONSE								= 1105
	, E_CHARGER_RESULT_RECV_RESPONSE						= 1106
	, E_CHARGER_OUT_RESPONSE								= 1107
	, E_CHARGER_MODE_CHANGE_RESPONSE						= 1108
	, E_CHARGER_STATE										= 1109
}FMS_OP_COMMAND;

static INT g_iHead_Map[] = 
{
	1
	, 4
	, 8
	, 8
	, 4
	, 14
	, 4
	, 6
	, 1
	, 0
};

typedef struct _st_FMSMSGHEAD
{
	CHAR MsgSTR[1+1];
	CHAR Line[4+1];
	CHAR Sender[8+1];
	CHAR Addressee[8+1];
	CHAR Command[4+1];
	CHAR SendTime[14+1];
	CHAR ResultcCode[4+1];
	CHAR DataLength[6+1];
	CHAR HeadEnd[1+1];
}st_FMSMSGHEAD;

//////////////////////////////////////////////////////////////////////////

// Error 통지 // 0011
static INT g_iERROR_NOTICE_Map[] = 
{
	3
	, 2
	, 0
};

typedef struct _st_ERROR_NOTICE
{
	CHAR Equipment_Num[3+1];
	CHAR Error_Code[2+1];
}st_ERROR_NOTICE;

// 설비 Error 통지 응답 // 0012
static INT g_iEQ_ERROR_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_EQ_ERROR_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_EQ_ERROR_RESPONSE;
//////////////////////////////////////////////////////////////////////////

// 시간 설정 응답 // 1002
static INT g_iTIME_SET_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_TIME_SET_RESPONSE
{
	CHAR Send_Fail_Count[3+1];
}st_TIME_SET_RESPONSE;

// Error 통지 응답 // 1011
static INT g_iERROR_NOTICE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_ERROR_NOTICE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_ERROR_NOTICE_RESPONSE;

// 설비 Error 통지 // 1012
static INT g_iEQ_ERROR_Map[] = 
{
	3
	, 2
	, 0
};

typedef struct _st_EQ_ERROR
{
	CHAR Equipment_Num[3+1];
	CHAR Error_Code[2+1];
}st_EQ_ERROR;

//////////////////////////////////////////////////////////////////////////

typedef struct _st_FMS_PACKET
{
	st_FMSMSGHEAD head;
	CHAR data[4096];
	UINT dataLen;
	INT64 inreservCode;
}st_FMS_PACKET;
*/