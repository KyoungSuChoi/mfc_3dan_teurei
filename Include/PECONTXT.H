#define main_graph_topic                10
#define graph_titles                    15
#define graph_viewingas                 20
#define graph_fontsize                  30
#define graph_plottingmethod            40
#define graph_graphplustable            50
#define graph_tablewhat                 60
#define graph_gridlines                 70
#define graph_precision                 80
#define graph_pointstograph             90
#define graph_subsetstograph            100
#define graph_fontsbutton               110
#define graph_colorsbutton              120
#define graph_gridinfront               125
#define graph_drawdatashadows           126
#define graph_zooming_topic             127
#define graph_showannotations           128

#define main_nsgraph_topic              130
#define nsgraph_titles                  135
#define nsgraph_viewingas               140
#define nsgraph_fontsize                150
#define nsgraph_subsetstograph          160
#define nsgraph_plottingmethod          170
#define nsgraph_gridlines               180
#define nsgraph_precision               190
#define nsgraph_fontsbutton             200
#define nsgraph_colorsbutton            210
#define nsgraph_gridinfront             215
#define nsgraph_drawdatashadows         216
#define nsgraph_showannotations         217

#define main_pie_topic                  220
#define pie_titles                      225
#define pie_viewingas                   230
#define pie_fontsize                    240
#define pie_sliceparameters             250
#define pie_precision                   260
#define pie_grouppercentages            270
#define pie_fontsbutton                 280
#define pie_colorsbutton                290
#define pie_drawdatashadows             295

#define main_export_topic               300
#define main_customization_topic        310
#define textdata_export                 320
#define filesaveas_export               330
#define print_export                    340
#define main_maximization_topic         350
#define main_pegrpsvr_topic             360



