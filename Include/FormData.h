///////EP_FORMATION_HEADER_FILE///////////////
//											//	
//	FormData.h							//	
//	ADPower Cell Test System API		//	
//	Byung Hum - Kim		2005. 5.13			//
//											//
//////////////////////////////////////////////

//ADPower Formation API 
#ifndef _FM_FORMATION_DEFINE_H_
#define _FM_FORMATION_DEFINE_H_

#include "CyclerModule.h"

#define FM_MAX_CH_CODE_LENGTH		128
#define FM_MAX_LONINID_LENGTH		16
#define FM_MAX_PASSWORD_LENGTH		16
#define FM_DATE_TIME_LENGTH			32		
#define FM_TRAY_NAME_LENGTH			32
#define FM_IP_NAME_LENGTH			16
#define FM_LOT_NAME_LENGTH			32
#define FM_TEST_SERIAL_LENGTH		24
#define FM_TRAY_SERIAL_LENGTH		8
#define FM_TEST_NAME_LENGTH			64
#define FM_RESULT_FILE_NAME_LENGTH	256
#define FM_FILE_DESCRIPTION_LENGTH	256
#define FM_FILE_VER_LENGTH			16
#define FM_FILE_ID_LENGTH			16
#define FM_TEST_DESCRIPTION_LENGTH	256
#define FM_CREATOR_NAME_LENGTH		64


//결과 파일의 Header
typedef struct tag_ResultFileHeader {					//Test Result File Header
	int		nModuleID;
	int		nGroupIndex;
	char	szDateTime[FM_DATE_TIME_LENGTH];			//Init. -> SendConditionStep()에서
	char	szModuleIP[FM_IP_NAME_LENGTH];				//SaveResultFileHeader()에서
	char	szTrayNo[FM_TRAY_NAME_LENGTH];				//Init. -> SendConditionStep()에서
	char	szLotNo[FM_LOT_NAME_LENGTH];				//Init. -> SendConditionStep()에서
	char	szOperatorID[FM_MAX_LONINID_LENGTH];
	char	szTestSerialNo[FM_TEST_SERIAL_LENGTH];
	char	szTraySerialNo[FM_TRAY_SERIAL_LENGTH];
} FM_RESULT_FILE_HEADER;


//Module에서 시행 되는 정보를 DataBase에 저장 하기 위한 Format
typedef struct tag_ModuleTestData {
	int		nModuleID;
	int		nGroupIndex;
	char	szDateTime[FM_DATE_TIME_LENGTH];			//Init. -> SendConditionStep()에서
	char	szModuleIP[FM_IP_NAME_LENGTH];			//SaveResultFileHeader()에서
	char	szTrayNo[FM_TRAY_NAME_LENGTH];			//Init. -> SendConditionStep()에서
	char	szLotNo[FM_LOT_NAME_LENGTH];				//Init. -> SendConditionStep()에서
	char	szOperatorID[FM_MAX_LONINID_LENGTH];
	char	szTestSerialNo[FM_TEST_SERIAL_LENGTH];
	long	lTraySerialNo;
	long	lTestID;
	char	szTestName[FM_TEST_NAME_LENGTH];
	long	lModelID;
	char	szModelName[FM_TEST_NAME_LENGTH];
	char	szResultFileName[FM_RESULT_FILE_NAME_LENGTH];
	int		nCellNo;
	BYTE	procedureType;
	BYTE	reserved1;
	WORD	reserved2;
} FM_PROCEDURE_DATA;

typedef struct tag_ElicoPowerFileHeader {
	char	szFileID[FM_FILE_ID_LENGTH];
	char	szFileVersion[FM_FILE_VER_LENGTH];
	char	szCreateDateTime[FM_DATE_TIME_LENGTH];
	char	szDescrition[FM_FILE_DESCRIPTION_LENGTH];
	char	szReserved[128];
}	FM_FILE_HEADER, *LPFM_FILE_HEADER;

// 
typedef struct tag_ConditionHeader {
	LONG	lID;
	char	szName[FM_TEST_NAME_LENGTH];
	char	szDescription[FM_TEST_DESCRIPTION_LENGTH];
	char	szCreator[FM_CREATOR_NAME_LENGTH];
	char	szModifiedTime[FM_DATE_TIME_LENGTH];
} FM_STR_CONDITION_HEADER;
// ==> size = 292

typedef struct tag_ConditionData {			//Test Condition 
	char szTestSerialNo[FM_TEST_SERIAL_LENGTH];
	FM_STR_CONDITION_HEADER	conditionHeader;
//	FM_TEST_HEADER		testHeader;
//	FM_CELL_CHECK_PARAM	boardParam;
	CPtrArray			apStepList;
	CPtrArray			apGradeList;
} FM_STR_CONDITION;

typedef struct tag_TestInformaiton {
	FM_STR_CONDITION_HEADER	modelData;
	FM_STR_CONDITION_HEADER	testData;
} FM_STR_TEST_INFO;


//Step 파일의 Header
typedef struct tag_ResultStepHeader {					//Test Result File Header
	int		nModuleID;
	int		nGroupIndex;
	char	szStartDateTime[FM_DATE_TIME_LENGTH];			//Init. -> SendConditionStep()에서
	char	szModuleIP[FM_IP_NAME_LENGTH];				//SaveResultFileHeader()에서
	char	szTrayNo[FM_TRAY_NAME_LENGTH];			//Init. -> SendConditionStep()에서
	char	szEndDateTime[FM_DATE_TIME_LENGTH];				//Init. -> SendConditionStep()에서
	char	szOperatorID[FM_MAX_LONINID_LENGTH];
	char	szTestSerialNo[FM_TEST_SERIAL_LENGTH];
	char	szTraySerialNo[FM_TRAY_SERIAL_LENGTH];
} FM_RESULT_STEP_HEADER;


typedef struct tag_Code {
	int nCode;
	int reserved;
} FM_CODE;

typedef struct tag_ChannelCodeMessage {
	int nCode;
	int nData;
	char szMessage[FM_MAX_CH_CODE_LENGTH];
} FM_CH_MSG;

//Monitoring의 List에서 표시할 Column Index
/*
#define		MAX_ITEM_COL_SIZE			24
#define		COL_INDEX_MODULE			0
#define		COL_INDEX_CHANNEL			1
#define		COL_INDEX_STATE				2
#define		COL_INDEX_VOLTAGE			3	
#define		COL_INDEX_CURRENT			4
#define		COL_INDEX_CAPACITY			5
#define		COL_INDEX_TESTNAME			6
#define		COL_INDEX_PATTERN			7
#define		COL_INDEX_CURSTEPTIME		8
#define		COL_INDEX_TOTALTIME			9
#define		COL_INDEX_CURSTEPCYCLE		10
#define		COL_INDEX_TOTALCYCLE		11
#define		COL_INDEX_STEPNO			12
#define		COL_INDEX_ERRORCODE			13
#define		COL_INDEX_GRADE				14
#define		COL_INDEX_SERIAL			15
#define		COL_INDEX_IMPEDANCE			16
*/

////////////////////////////////////////////////////////
//registry section define
#define		CT_CONFIG_REG_SEC		"Config"

//Log Level
//log를 5개의 level로 분리 하여 저장한다.
#define		CT_LOG_LEVEL_CUSTOMER	0x00	//일반 Operating용 Log(어떤 동작을 했다만 기록)
#define		CT_LOG_LEVEL_NORMAL		0x01	//작업 및 Command 동작 Log포함 (주로 실패 까지 기록)
#define		CT_LOG_LEVEL_DETAIL		0x02	//사용자 상세 정보(성공 여부도 기록) 
#define		CT_LOG_LEVEL_SYSTEM		0x03	//System 정보까지 (System 정보까지 기록)
#define		CT_LOG_LEVEL_NETWORK	0x04	//Network 통신까지 포함(Network 상세 정보 기록)
#define		CT_LOG_LEVEL_DEBUG		0x05	//Debug용으로 상세 기록(프레임 구조및 if문 실패 및 return 원인 기록)

//#define CT_MAX_STATE_COLOR				16	
//
////각 상태별로 표시할 색상을 정의 
//typedef struct tag_State_Display
//{
//	BOOL		bStateFlash;
//	COLORREF	TStateColor;
//	COLORREF	BStateColor;
//	char		szMsg[32];
//} _CT_STATE_CONFIG;
//
//typedef struct tag_Over_Display
//{
//	float		fValue;			//0 : Voltage //1: Current
//	BOOL		bFlash;
//	COLORREF	TOverColor;
//	COLORREF	BOverColor;	
//} _CT_OVER_CONFIG;
//
//typedef struct tag_Color_Config
//{
//	BYTE		bShowText;
//	BYTE		bShowColor;
//	BYTE		bShowOver;
//
//	_CT_STATE_CONFIG	stateConfig[CT_MAX_STATE_COLOR];
//	_CT_OVER_CONFIG		VOverConfig;
//	_CT_OVER_CONFIG		IOverConfig;
//
//} CT_COLOR_CONFIG;

#endif //_EP_FORMATION_DEFINE_H_