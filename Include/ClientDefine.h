#ifndef __CLIENT_DEFINE_H__
#define __CLIENT_DEFINE_H__

typedef struct 
{
	int nCmd;
	int nModuleID;
	int nSequence;
	int nData;
	char szBuffer[128];
	int nValidDataSize;
} S_MESSAGE_PARAM, *LP_MESSAGE_PARAM;

#endif//__CLIENT_DEFINE_H__