//////////////////////////////////////////////////////////////////////////
//
//		ADPOWER Battery Charge/Discharge System
//		Inculde file of PSData.dll
//		PSData.dll Class include file
//
//////////////////////////////////////////////////////////////////////////
#ifndef _ADPOWER_PSDATA_DLL_INCLUDE_H_
#define _ADPOWER_PSDATA_DLL_INCLUDE_H_

#include "../BFGraphData/Data.h"
#include "../BFGraphData/Plane.h"
#include "../BFGraphData/Line.h"
#include "../BFGraphData/ResultTestSelDlg.h"

#endif	//_ADPOWER_PSDATA_DLL_INCLUDE_H_