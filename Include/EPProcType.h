//////////Procedure_Type_HEADER_FILE//////////////
//												//	
//	EPProcType.h								//	
//	Elico Power Formation System Procedure Type	//	
//	Byung Hum - Kim		2001.7.11				//
//												//			
//////////////////////////////////////////////////

//Elico Power Preocedure Type 
#ifndef _EP_PROCEDURE_TYPE_DEFINE_H_
#define _EP_PROCEDURE_TYPE_DEFINE_H_

#define EP_REPORT_GRADING				10000			//종합 등급 Code 
#define EP_REPORT_CH_CODE				10001			//최종 Cell Code

//Charge 공정
#define EP_PROC_TYPE_CHARGE_LOW		0x10000
#define EP_PROC_TYPE_CHARGE_HIGH	0x1FFFF

#define EP_PROC_TYPE_DISCHARGE_LOW	0x20000
#define EP_PROC_TYPE_DISCHARGE_HIGH	0x2FFFF

#define EP_PROC_TYPE_REST_LOW		0x30000
#define EP_PROC_TYPE_REST_HIGH		0x3FFFF

#define EP_PROC_TYPE_OCV_LOW		0x40000
#define EP_PROC_TYPE_OCV_HIGH		0x4FFFF

#define EP_PROC_TYPE_IMPEDANCE_LOW	0x50000
#define EP_PROC_TYPE_IMPEDANCE_HIGH	0x5FFFF

#define EP_PROC_TYPE_END_LOW		0x60000
#define EP_PROC_TYPE_END_HIGH		0x6FFFF

#define EP_PROC_TYPE_AGING_LOW		0x70000
#define EP_PROC_TYPE_AGING_HIGH		0x7FFFF

#define EP_PROC_TYPE_GRADING_LOW	0x80000
#define EP_PROC_TYPE_GRADING_HIGH	0x8FFFF

#define EP_PROC_TYPE_SELECTING_LOW	0x90000
#define EP_PROC_TYPE_SELECTING_HIGH	0x9FFFF

#define EP_PROC_TYPE_NONE				0
#define EP_PROC_TYPE_CHARGE				1
#define EP_PROC_TYPE_CHARGE1			65536
#define EP_PROC_TYPE_CHARGE2			65537
#define EP_PROC_TYPE_CHARGE3			65538
#define EP_PROC_TYPE_CHARGE4			65539
#define EP_PROC_TYPE_CHARGE5			65540
#define EP_PROC_TYPE_CHARGE6			65541
#define EP_PROC_TYPE_DISCHARGE			2
#define EP_PROC_TYPE_DISCHARGE1			131072
#define EP_PROC_TYPE_DISCHARGE2			131073
#define EP_PROC_TYPE_DISCHARGE3			131074
#define EP_PROC_TYPE_DISCHARGE4			131075
#define EP_PROC_TYPE_DISCHARGE5			131076
#define EP_PROC_TYPE_DISCHARGE6			131077
#define EP_PROC_TYPE_REST				3
#define EP_PROC_TYPE_REST1				196608
#define EP_PROC_TYPE_OCV				4
// #define EP_PROC_TYPE_OCV1				262144
// #define EP_PROC_TYPE_OCV2				262145
// #define EP_PROC_TYPE_OCV3				262146
// #define EP_PROC_TYPE_OCV4				262147
#define EP_PROC_TYPE_IMPEDANCE			5
#define EP_PROC_TYPE_END				6
#define EP_PROC_TYPE_END1				393216

#define GetStepTypeofProcType(type)	((int)type>>16)
#define IsNoneProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_NONE ? TRUE : FALSE)
#define IsChargeProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_CHARGE ? TRUE : FALSE)
#define IsDischargeProc(type)		(GetStepTypeofProcType(type) == EP_TYPE_DISCHARGE ? TRUE : FALSE)
#define IsRestProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_REST ? TRUE : FALSE)
#define IsOcvProc(type)				(GetStepTypeofProcType(type) == EP_TYPE_OCV ? TRUE : FALSE)
#define IsImpedanceProc(type)		(GetStepTypeofProcType(type) == EP_TYPE_IMPEDANCE ? TRUE : FALSE)
#define IsEndProc(type)				(GetStepTypeofProcType(type) == EP_TYPE_END ? TRUE : FALSE)
#define IsAgingProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_AGING ? TRUE : FALSE)
#define IsGradingProc(type)			(GetStepTypeofProcType(type) == EP_TYPE_GRADING ? TRUE : FALSE)
#define IsSelectingProc(type)		(GetStepTypeofProcType(type) == EP_TYPE_SELECTING ? TRUE : FALSE)

#define EP_PROC_TYPE_OCV1	(EP_PROC_TYPE_OCV_LOW)
#define EP_PROC_TYPE_OCV2	(EP_PROC_TYPE_OCV_LOW+1)
#define EP_PROC_TYPE_OCV3	(EP_PROC_TYPE_OCV_LOW+2)
#define EP_PROC_TYPE_OCV4	(EP_PROC_TYPE_OCV_LOW+3)
#define EP_PROC_TYPE_OCV5	(EP_PROC_TYPE_OCV_LOW+4)
#define EP_PROC_TYPE_OCV6	(EP_PROC_TYPE_OCV_LOW+5)

#endif _EP_PROCEDURE_TYPE_DEFINE_H_