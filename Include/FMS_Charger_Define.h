#pragma once

//////////////////////////////////////////////////////////////////////////
// 입고 가능 장비 요구	// 0101
// 입고 예약			// 0102
typedef struct _st_Cell_ID
{
	CHAR Cell_ID[20+1];
}st_Cell_ID;

typedef struct _st_CELL_INFO
{
	CHAR Cell_Info[4+1];
}st_CELL_INFO;

typedef enum tag_CHARGER_INRESEVE_F
{
	C_MACHINID
	, C_TYPE
	, C_PROCESS_NO
	, C_BATCH_NO
	, C_TRAY_ID
	, C_ARCELL_ID
	, C_TAP_DEEPTH
	, C_TRAY_HEIGHT
	, C_TRAY_TYPE
	, C_TOTAL_STEP
	, C_CIF_END

}CHARGER_INRESEVE_F;
static const TCHAR g_str_CHARGER_INRESEVE_F[C_CIF_END][32] = 
{
	TEXT("MachinID")
	, TEXT("Type")
	, TEXT("Process_No")
	, TEXT("Batch_No")
	, TEXT("Tray_ID")
	, TEXT("arCell_ID")
	, TEXT("Tap_Deepth")
	, TEXT("Tray_Height")
	, TEXT("Tray_Type")
	, TEXT("Total_Step")
};
//////////////////////////////////////////////////////////////////////////

typedef struct _st_Contact_Set
{
	CHAR Charge_Cur[10+1];
	CHAR Charge_Time[10+1];
	CHAR Inverse_Vol[10+1];		// 초기 전압 확인
	CHAR Upper_Vol_Check[10+1];	// 전압 상한치 확인
	CHAR Lower_Vol_Check[10+1];	// 전압 하한치 확인
	CHAR Upper_Cur_Check[10+1];	// 전류 상한치 확인
	CHAR Lower_Cur_Check[10+1];	// 전류 하한치 확인
	CHAR Delta_Vol_Limit[10+1];	// Delta Voltage Limit	
}st_Contact_Set;

typedef struct _st_Protect_Limit_Set
{
	CHAR Charge_LIMIT_Vol[10+1];
	CHAR Charge_LIMIT_Cur[10+1];
// 	CHAR Charge_H_Cap[10+1];
// 	CHAR Charge_L_Cap[10+1];

	CHAR DisCharge_LIMIT_Vol[10+1];
	CHAR DisCharge_LIMIT_Cur[10+1];
// 	CHAR DisCharge_H_Cap[10+1];
// 	CHAR DisCharge_L_Cap[10+1];

	CHAR Charge_H_Time_Vol[10+1];	
	CHAR Charge_L_Time_Vol[10+1];	
	CHAR Charge_Vol_Get_Time[10+1];


	//19-01-09 엄륭 DCIR 관련 MES 수정 

	CHAR Calc_Cap_Type[10+1];
	CHAR Const_A[10+1];
	CHAR Const_B[10+1];
	CHAR Const_C[10+1];
	CHAR Const_D[10+1];
	CHAR Const_E[10+1];

	CHAR DCIR[10+1];
	CHAR DCIR_A[10+1];
	CHAR DCIR_B[10+1];
	CHAR DCIR_C[10+1];
	CHAR DCIR_MIN[10+1];
	CHAR DCIR_MAX[10+1];

	///////////////////////////////////

}st_Protect_Limit_Set;

typedef struct _st_Step_Set
{
	WORD Step_ID;
	BYTE Step_Type;
	CHAR Current[10+1];
	CHAR Volt[10+1];
	CHAR Time[10+1];
	CHAR CutOff_Current[10+1];
	CHAR CutOff_Volt[10+1];	
	CHAR CutOff_A[10+1];			// Capacity
	CHAR GetTime_Setting[10+1];
	CHAR Cap_H[10+1];
	CHAR Cap_L[10+1];
	CHAR CutOff_SOC[10+1];			// Delta Capacity
	CHAR Step_Start_Volt[10+1];
}st_Step_Set;

typedef struct _st_CHARGER_INRESEVE
{
	CHAR MachinID[5+1];
	BYTE nTraykind;
	CHAR Process_Type[4+1];
	WORD Process_No;	
	CHAR Batch_No[20+1];
	WORD Tap_Deepth;
	WORD Tray_Height;
	BYTE Tray_Type;			// 0:양방향, 1:단방향
	BYTE Channel_Insert_Type;
	WORD Total_Step;
	WORD Contact_Error_Setting;
	WORD Capa_Error_Limit;	
	CHAR Tray_ID_1[10+1];
	CHAR Tray_ID_2[10+1];
	st_Cell_ID arCell_ID[MAX_CELL_COUNT];
	st_CELL_INFO arCellInfo[MAX_CELL_COUNT];
	st_Contact_Set Contact;	
	st_Protect_Limit_Set Protect;
	st_Step_Set Step_Info[100];	
}st_CHARGER_INRESEVE;

typedef struct _st_CHARGER_IN
{
	CHAR Equipment_Num[5+1];
	CHAR Tray_ID[6+1];
}st_CHARGER_IN;

typedef struct _st_Contact_Result
{
	float Initial_Vol;
	float Contact_Vol;
	float Contact_Cur;	
	float Delta_OCV;
} st_Contact_Result;

typedef struct _st_Result_Step_Result
{
	WORD StepNo;
	BYTE Action;
	float Voltage[MAX_CELL_COUNT];
	float Capacity[MAX_CELL_COUNT];
	float CheckTimeTemp[MAX_CELL_COUNT][16];
	float CutCurrent[MAX_CELL_COUNT];
	float StepEndTime[MAX_CELL_COUNT];
	float CC_Time[MAX_CELL_COUNT];
	float CC_Capacity[MAX_CELL_COUNT];
	float CV_Time[MAX_CELL_COUNT];
	float CV_Capacity[MAX_CELL_COUNT];
	float Delta_Capacity[MAX_CELL_COUNT];
	float StartVoltage[MAX_CELL_COUNT];
	float ChecktimeVoltage[MAX_CELL_COUNT];

	float AverageTimeTemp[MAX_CELL_COUNT][16];
	float DCIR_Measure[MAX_CELL_COUNT];

}st_Result_Step_Result;

typedef struct _st_RESULT_FILE
{
	CHAR Stage_No[5+1];
	BYTE nTraykind;
	BYTE RESULTTYPE;
	// WORD PROCESSTYPE;
	CHAR PROCESSTYPE[4+1];
	WORD PROCESSNO;
	WORD TRAYTYPE;
	BYTE CHANNELINSERTTYPE;
	CHAR BACHNO[20+1];
	CHAR STIME[14+1];
	CHAR ETIME[14+1];
	WORD TotalStep;
	CHAR TRAYID_1[10+1];
	WORD TRAYPOSITION_1;
	CHAR TRAYID_2[10+1];
	WORD TRAYPOSITION_2;
	st_Cell_ID arCell_ID[MAX_CELL_COUNT];
	st_CELL_INFO arCellInfo[MAX_CELL_COUNT];		
	st_Contact_Result sContactResult[MAX_CELL_COUNT];
}st_RESULT_FILE;

// 검사 종료 응답 // 0104
static INT g_iCHARGER_WORKEND_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_WORKEND_RESPONSE
{
	CHAR Equipment_Num[5+1];

}st_CHARGER_WORKEND_RESPONSE;
// 결과 FILE 요구 // 0105
static INT g_iCHARGER_RESULT_Map[] = 
{
	3
	, 6
	, 0
};

typedef struct _st_CHARGER_RESULT
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[6+1];
}st_CHARGER_RESULT;

// 결과 FILE 수신 확인 통지 // 0106
static INT g_iCHARGER_RESULT_RECV_Map[] = 
{
	3
	, 1
	, 6
	, 0
};

typedef struct _st_CHARGER_RESULT_RECV
{
	CHAR Equipment_Num[3+1];
	CHAR Notify_Mode[1+1];
	CHAR Tray_ID[6+1];
}st_CHARGER_RESULT_RECV;

// 출고 완료 통지 // 0107
static INT g_iCHARGER_OUT_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_OUT
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_OUT;

// Mode 변경 요구 // 0108
static INT g_iCHARGER_MODE_CHANGE_Map[] = 
{
	3
	, 1
	, 0
};

typedef struct _st_CHARGER_MODE_CHANGE
{
	CHAR Equipment_Num[3+1];
	CHAR Equipment_State[1+1];
}st_CHARGER_MODE_CHANGE;

// 설비 상태 통지 응답 // 0109
static INT g_iCHARGER_STATE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_STATE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_STATE_RESPONSE;

static INT g_iCHARGER_INRESEVE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_INRESEVE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_INRESEVE_RESPONSE;

// 입고 완료 응답 // 1103
static INT g_iCHARGER_IN_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_IN_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_IN_RESPONSE;

// 검사 종료 통지 // 1104
static INT g_iCHARGER_WORKEND_Map[] = 
{
	3
	, 6
	, 0
};

typedef struct _st_CHARGER_WORKEND
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[6+1];
}st_CHARGER_WORKEND;

//결과파일 전송 //1105


// 결과 FILE 수신 확인 응답 // 1106
static INT g_iCHARGER_RESULT_RECV_RESPONSE_Map[] = 
{
	3
	, 6
	, 0
};

typedef struct _st_CHARGER_RESULT_RECV_RESPONSE
{
	CHAR Equipment_Num[3+1];
	CHAR Tray_ID[6+1];
}st_CHARGER_RESULT_RECV_RESPONSE;
// 출고 완료 응답 // 1107
static INT g_iCHARGER_OUT_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_OUT_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_OUT_RESPONSE;

// Mode 변경 응답 // 1108
static INT g_iCHARGER_MODE_CHANGE_RESPONSE_Map[] = 
{
	3
	, 0
};

typedef struct _st_CHARGER_MODE_CHANGE_RESPONSE
{
	CHAR Equipment_Num[3+1];
}st_CHARGER_MODE_CHANGE_RESPONSE;

// 설비 상태 통지 // 1109
static INT g_iCHARGER_STATE_Map[] = 
{
	3
	, 2
	, 0
};

typedef struct _st_CHARGER_STATE
{
	CHAR Equipment_Num[3+1];
	CHAR Equipment_State[2+1];
}st_CHARGER_STATE;

static INT g_iCON_IR_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};

typedef struct _st_CON_IR
{
	CHAR CON_IR[6+1]; // ##.### m옴
}st_CON_IR;

//////////////////////////////////////////////////////////////////////////
// 결과 FILE 응답 - Stage No ~ Total Step 까지 // 1105
static INT g_iResult_File_Response_L_Map[] = 
{
	3
	, 2
	, 3
	, 2
	, 20
	, 6
	
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6

	, 3
	, 0
};

typedef struct _st_VOLTAGE_WORTH_RESPONSE
{
	CHAR Voltage_Worth[5+1];
}st_VOLTAGE_WORTH_RESPONSE;

typedef struct _st_CURRENT_WORTH_FIVE_MIN_RESPONSE
{
	CHAR Current_Worth_Five_Min[6+1];
}st_CURRENT_WORTH_FIVE_MIN_RESPONSE;


// Step 용량값
static INT g_iCapacity_Worth_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};

typedef struct _st_CAPACITY_WORTH_RESPONSE
{
	CHAR Capacity_Worth[6+1];
}st_CAPACITY_WORTH_RESPONSE;

// Step 전류값(시간)
static INT g_iCurrent_Worth_Hour_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};
typedef struct _st_CURRENT_WORTH_HOUR_RESPONSE
{
	CHAR Current_Worth_Hour[6+1];
}st_CURRENT_WORTH_HOUR_RESPONSE;


// Step 종지 전류값(discharge)
static INT g_iDischarge_Current_Worth_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};
typedef struct _st_DISCHARGE_CURRENT_WORTH_RESPONSE
{
	CHAR Discharge_Current_Worth[6+1];
}st_DISCHARGE_CURRENT_WORTH_RESPONSE;


// Step 종료 시간
static INT g_iStep_End_Time_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};
typedef struct _st_STEP_END_TIME_RESPONSE
{
	CHAR Step_End_Time[6+1];
}st_STEP_END_TIME_RESPONSE;


// Step 전압값(시간)
static INT g_iStep_Voltage_Time_Map[] = 
{
	5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5, 5, 5, 5, 5
	, 5, 5, 5, 5
	, 0
};
typedef struct _st_STEP_VOLTAGE_TIME_RESPONSE
{
	CHAR Step_Voltage_Time[5+1];
}st_STEP_VOLTAGE_TIME_RESPONSE;


// Step 정전류값(시간)
static INT g_iStep_Current_Time_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};
typedef struct _st_STEP_CURRENT_TIME_RESPONSE
{
	CHAR Step_Current_Time[6+1];
}st_STEP_CURRENT_TIME_RESPONSE;
// Step 정전류 용량 (CC 용량)
static INT g_iStep_Current_Capacity_CC_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};
typedef struct _st_STEP_CURRENT_CAPACITY_CC
{
	CHAR Step_Current_Capacity_CC[6+1];
}st_STEP_CURRENT_CAPACITY_CC;
//////////////////////////////////////////////////////////////////////////

// Step CC 시간
static INT g_iStep_CV_Time_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};
typedef struct _st_STEP_CV_TIME
{
	CHAR Step_CV_Time[6+1];
}st_STEP_CV_TIME;
//////////////////////////////////////////////////////////////////////////

// Step CV 용량
static INT g_iStep_CV_Capacity_Map[] = 
{
	6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6, 6, 6, 6, 6
	, 6, 6, 6, 6
	, 0
};
typedef struct _st_STEP_CV_CAPACITY
{
	CHAR Step_CV_Capacity[6+1];
}st_STEP_CV_CAPACITY;

typedef struct st_RESULT_FILE_RESPONSE_L
{
	CHAR R_Step_No[3+1];
	CHAR R_Action[1+1];
	st_VOLTAGE_WORTH_RESPONSE  R_Voltage_Worth[MAX_CELL_COUNT];
	st_CURRENT_WORTH_FIVE_MIN_RESPONSE  R_Current_Worth_Five_Min[MAX_CELL_COUNT];
	st_CAPACITY_WORTH_RESPONSE  R_Capacity_Worth[MAX_CELL_COUNT];
	st_CURRENT_WORTH_HOUR_RESPONSE  R_Current_Worth_Hour[MAX_CELL_COUNT];
	st_DISCHARGE_CURRENT_WORTH_RESPONSE  R_Discharge_Current_Worth[MAX_CELL_COUNT];
	st_STEP_END_TIME_RESPONSE  R_Step_End_Time[MAX_CELL_COUNT];
	st_STEP_VOLTAGE_TIME_RESPONSE  R_Step_Voltage_Time[MAX_CELL_COUNT];
	st_STEP_CURRENT_TIME_RESPONSE  R_Step_Current_Time[MAX_CELL_COUNT];
	//////////////////////////////////////////////////////////////////////////
	st_STEP_CURRENT_CAPACITY_CC  R_Step_Current_Capacity_CC[MAX_CELL_COUNT];
	st_STEP_CV_TIME  R_Step_CV_Time[MAX_CELL_COUNT];
	st_STEP_CV_CAPACITY  R_Step_CV_Capacity[MAX_CELL_COUNT];
}st_RESULT_FILE_RESPONSE_L;

typedef enum tag_Result_Step_Set
{
	R_STEP_NO
	,R_ACTION
	, R_VOLTAGE_WORTH
	, R_CURRENT_WORTH_FIVE_MIN
	, R_CAPACITY_WORTH
	, R_CURRENT_WORTH_HOUR
	, R_DISCHARGE_CURRENT_WORTH
	, R_STEP_END_TIME
	, R_STEP_VOLTAGE_TIME
	, R_STEP_CURRENT_TIME
	, RSS_END

}Result_Step_Set;
static const TCHAR g_str_Result_Step_Set[RSS_END][32] = 
{
	TEXT("Step_No")
	, TEXT("Action")
	, TEXT("전압값")
	, TEXT("전류값(5분후)")
	, TEXT("용량값")
	, TEXT("전류값(시간)")
	, TEXT("종지 전류값")
	, TEXT("Step 종료 시간")
	, TEXT("전압값(시간)")
	, TEXT("정전류 시간")
};

// 결과 File 응답 Cell_Info
static INT g_iResult_File_Response_Cell_Info_Map[] = 
{
	2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2
	, 0
};

typedef struct _st_RESULT_FILE_RESPONSE_CELL_INFO
{
	CHAR Cell_Info[2+1];
}st_RESULT_FILE_RESPONSE_CELL_INFO;

// 결과 FILE 응답 - 현재 진행 Step 번호 ~ 검사 종료 일시 까지 // 1105
static INT g_iResult_File_Response_R_Map[] = 
{
	3
	, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2, 2, 2, 2, 2
	, 2, 2, 2, 2
	, 14
	, 14
	, 0
};

typedef struct _st_RESULT_FILE_RESPONSE_R
{
	CHAR Now_Step_No[3+1];
	st_RESULT_FILE_RESPONSE_CELL_INFO arCellInfo[MAX_CELL_COUNT];
	

}st_RESULT_FILE_RESPONSE_R;

//////////////////////////////////////////////////////////////////////////