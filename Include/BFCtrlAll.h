//////////////////////////////////////////////////////////////////////////
// Control calss include
//////////////////////////////////////////////////////////////////////////

#ifndef _CTRL_CLASS_INCLUDE_H_
#define		_CTRL_CLASS_INCLUDE_H_

#include "BFCtrl/XPButton.h"
#include "BFCtrl/Label.h"
#include "BFCtrl/LedButton.h"
#include "BFCtrl/ProgressWnd.h"
#include "BFCtrl/LabelStatic.h"
#include "BFCtrl/StaticCounter.h"
#include "BFCtrl/ColorEdit.h"
#include "BFCtrl/StaticFilespec.h"
#include "BFCtrl/ListBoxColorPickerST.h"

#endif	//_CTRL_CLASS_INCLUDE_H_