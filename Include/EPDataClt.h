///////////////////////////////////////
//
//		PEN cell test system 
//
///////////////////////////////////////

//EPDLL_API_HEADER_FILE
#ifndef _EP_FORMATION_DATA_CLIENT_DLL_DEFINE_H_
#define _EP_FORMATION_DATA_CLIENT_DLL_DEFINE_H_

#include "EpDataSvr.h"

#ifdef __cplusepluse
extern "C" {
#endif //__cplusepluse

#ifndef __EP_FORM_DLL__
#define __EP_FORM_DLL__

#ifdef EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllexport)
#else	//EXPORT_EPDLL_API
#defnie EPDLL_API	__declspec(dllimport)
#endif	//EXPORT_EPDLL_API

#endif	//__EP_FORM_DLL__

EPDLL_API int dcConnectToServer(LPSTR serverIP, HWND hMsgWnd, UINT nSystemID);
EPDLL_API int dcDisConnect();
EPDLL_API int dcSendData(int nCommand, LPVOID lpData, int nSize );
EPDLL_API void dcSetLog(BOOL bWrite);
EPDLL_API LPSTR dcGetClientIP();

#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif	//_EP_FORMATION_DATA_CLIENT_DLL_DEFINE_H_


