///////////////////////////////////////
//
//		Elico Power Formatin System
//		OffLine => OnLine화 정책 
//		2002/11/6
//		Kim Byung Hum
//
///////////////////////////////////////

//EPDLL_API_HEADER_FILE
#ifndef _EP_FORMATION_DATA_ROUTER_DLL_DEFINE_H_
#define _EP_FORMATION_DATA_ROUTER_DLL_DEFINE_H_

#define EN_SEQUENCE_NO_LENGTH		2
#define EN_NORMAL_CMD_NAME_LENGTH	6
#define EN_RESPONSE_CMD_NAME_LENGTH	3
#define EN_BAR_CODE_LENGTH			10

#define CONNECTION_RETRY_INTERVAL	10000
#define HOST_WND_NAME	"PowerFormation"

#define EN_TX_BUFF_LENGTH	4096
#define EN_RX_BUFF_LENGTH	4096

#define EN_DEFAULT_SOCKET_PORT	5031
#define EN_MAX_CH_PER_MD		256

#ifdef EP_IP_NAME_LENGTH
#undef EP_IP_NAME_LENGTH
#endif

#define EP_IP_NAME_LENGTH	16

#define EN_CMD_REQSTS	170		//ENAX -> ELICO
#define EN_CMD_ACKSTS	180		//Response of EN_CMD_REQSTS

#define EN_CMD_SETREQ	200		//ENAX -> ELICO		Reponse EN_CMD_NAK or EN_CMD_ACK

#define EN_CMD_DATREQ	210		//ENAX -> ELICO
#define EN_CMD_ACKDAT	220		//Response of EN_CMD_DATREQ

#define EN_CMD_SUSREQ	230		//ENAX -> ELICO		Reponse EN_CMD_NAK or EN_CMD_ACK
#define EN_CMD_RESREQ	231		//ENAX -> ELICO		Reponse EN_CMD_NAK or EN_CMD_ACK
#define EN_CMD_STPREQ	240		//ENAX -> ELICO		Reponse EN_CMD_NAK or EN_CMD_ACK

#define EN_CMD_NAK		254
#define EN_CMD_ACK		255


#define ENWM_SETREQ		WM_USER + 3000
#define ENWM_REQSTS		WM_USER + 3001
#define ENWM_SUSREQ		WM_USER	+ 3002
#define ENWM_RESREQ		WM_USER + 3003
#define ENWM_STPREQ		WM_USER + 3004
#define ENWM_DATREQ		WM_USER	+ 3005
#define ENWM_RX_MESSAGE WM_USER + 3006
#define ENWM_NET_ERROR	WM_USER + 3007

#define ENWM_NACK		WM_USER + 3008

typedef struct tag_enax_MessageHeader 
{
	union {
		WORD	wSequence;	
		char	sequence[EN_SEQUENCE_NO_LENGTH];
	};
	BYTE sendID;
	BYTE command;
	BYTE boxID;
} enMsgHeader, *lpenMsgHeader;

#define enSizeofHeader()	(sizeof(struct tag_enax_MessageHeader))

//EN_CMD_REQSTS(170)
typedef struct tag_enax_status_data 
{
	char cmdName[EN_NORMAL_CMD_NAME_LENGTH];	//REQSTS
	BYTE fanStartTemp;							//1~100	 -> OCV에서는 미사용	
	BYTE emgTemp;								//1~100	 -> OCV에서는 미사용
	char reserved[50];
//	BYTE checksum;
} enStatusData;

//Status1 bit define
#define S1_BIT_OFFLINE		0x01
#define S1_BIT_IDLE			0x02
#define S1_BIT_STANDBY		0x04
#define S1_BIT_RUN			0x08
#define S1_BIT_NET_ERROR	0x10
#define S1_BIT_OTHER_ERROR	0x20

//status2 define
#define S2_STANDBY			0x00
#define S2_PRE_RUN			0x01
#define S2_RUN				0x02
#define S2_END				0x03
#define S2_STOPPING			0x04
#define S2_STOP				0x05
#define S2_FAULT			0x06
#define S2_MAINTENANCE_MODE	0xF0

//status3 bit define
#define S3_BIT_BOX1			0x01
#define S3_BIT_BOX2			0x02
#define S3_BIT_BOX3			0x04
#define S3_BIT_BOX4			0x08
#define	TrayBit( box )	(0x01 << (box-1))

//errInfo bit define
#define ERR_SENSOR			0x01
#define ERR_SYSTEM			0x02
#define ERR_VOLTAGE			0x04
#define ERR_CURRENT			0x08

//sensorinfo bit define
#define SENSOR_SMOKE		0x01
#define SENSOR_TEMP			0x02
#define SENSOR_CIRCUIT		0x04

//hardwareinfo bit define
#define FAULT_AIR_PRESS		0x01
#define FAULT_TRAY_DIRECTION	0x02
#define FAULT_TRAY			0x04
#define FAULT_UP_DOWN_CYLINDER	0x08
#define FAULT_LATCH_CYLINDER	0x10
#define FAULT_STACKER			0x20
#define FAULT_FAN_ON			0x40
#define FAULT_EMG_SWITCH		0x80

typedef struct tag_enax_error_data
{
	BYTE	errorInfo;
	BYTE	sensorInfo;
	BYTE	hardwareInfo;
	BYTE	voltageErr;
	BYTE	currentErr;
	BYTE	state;				//0: Standby, 1:Run, 2:End, 3:Error
	BYTE	testItem;
	BYTE	boxTemp;
}	enError;


//EN_CMD_ACKSTS(180)
typedef struct tag_enax_status_response
{
	char	cmdName[EN_NORMAL_CMD_NAME_LENGTH];		//'ACKSTS'
	BYTE	status1;
	BYTE	status2;
	BYTE	status3;
	enError	error[4];
	char	barCodeID[EN_BAR_CODE_LENGTH];			//없을시 공백(20h)로 채운다. 
	char	reserved[7];
//	BYTE	checksum;
}	enStatusResponse;



//EN_CMD_ACK (255)
typedef struct tag_enax_Ack
{
	char cmdName[EN_RESPONSE_CMD_NAME_LENGTH];		//'ACK'
	char	reserved[55];
//	BYTE	checksum;
} enAck;

//errID	define
#define ERR_CHECKSUM	0x01
#define ERR_COMMAND		0x02
#define ERR_OTHER		0x03
#define ERR_RUNNING		0x04
#define ERR_STACKER_RUN	0x05
#define ERR_CHARGE_PARAM	0x06
#define ERR_DISCHARGE_PARAM	0x07
#define ERR_STOP			0x08
#define ERR_PAUSE			0x09
#define ERR_RESUME			0x0A
#define ERR_BOX_ID			0x0B
#define ERR_PROGRAM			0xFF

//EN_CMD_NAK (254)
typedef struct tag_enax_Nack
{
	char cmdName[EN_RESPONSE_CMD_NAME_LENGTH];		//'NAK'
	BYTE	errID;
	char	reserved[54];
//	BYTE	checksum;
} enNack;

#define EN_TYPE_CHARGE		1
#define EN_TYPE_DISCHARGE	2
#define EN_TYPE_OCV			3

//EN_CMD_SETREQ (200)
typedef struct tag_enax_Start
{
	char cmdName[EN_NORMAL_CMD_NAME_LENGTH];		//'SETREQ'
	BYTE	type;				//1:Charge, 2: Discharge, 3:OCV
	WORD	current;			//
	WORD	time;				//1~999 sec
	char	reserved[303];
//	BYTE	checksum;
} enStartParam;

//EN_CMD_DATREQ (210)
typedef struct tag_enax_Data_Request
{
	char cmdName[EN_NORMAL_CMD_NAME_LENGTH];		//'DATREQ'
	char	reserved[52];
//	BYTE	checksum;
} enDataRequest;

//defne bad cell code
#define	CELL_GOOD			0x00
#define CELL_LOW_VOLTAGE	0x01		//충방전기
#define CELL_HIGH_VOLTAGE	0x02		//충방전기
#define CELL_OCV_LIMIT		0x04		//OCV 기		//==>발생시 Impedance Data를 9999 전송 
#define CELL_IMPDEANCE_CURRENT_LIMIT	0x05			//==>발생시 Impedance Data를 9999 전송 

#define OCV_LIMIT_VOLTAGE			2700		//2700mA
#define IMPEDANCE_CURRENT_LIMIT		20			//20mA

//EN_CMD_ACKDAT (220)
typedef struct tag_enax_End_Data
{
	char	cmdName[EN_NORMAL_CMD_NAME_LENGTH];		//'ACKDAT'
	WORD	voltage[EN_MAX_CH_PER_MD];				//
	WORD	current[EN_MAX_CH_PER_MD];
	WORD	impedance[EN_MAX_CH_PER_MD];
	WORD	capacity[EN_MAX_CH_PER_MD];
	BYTE	badcell[32];					//0 =>Good	1->Bad Cell
	WORD	cellCode[EN_MAX_CH_PER_MD];
	char	reserved[20];
//	BYTE	checksum;
}	enChData;


//SUSREQ 와 RESREQ는 Formation 설비만 지원
//SUSREQ는 Run일 경우만 지원 
//RESREQ는 Pause에서만 지원 
typedef struct tag_enax_Command
{
	char	cmdName[EN_NORMAL_CMD_NAME_LENGTH];		//'SUSREQ', 'RESREQ', 'STPREQ'
	char	reserved[52];
//	BYTE	checksum;
}	enCommand;


#ifdef __cplusepluse
extern "C" {
#endif //__cplusepluse

#ifndef __EP_FORM_DLL__
#define __EP_FORM_DLL__

#ifdef EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllexport)
#else	//EXPORT_EPDLL_API
#define EPDLL_API	__declspec(dllimport)
#endif	//EXPORT_EPDLL_API

#endif	//__EP_FORM_DLL__

EPDLL_API int enConnectToServer(LPSTR serverIP, UINT nPort);
EPDLL_API int enDisConnect();
EPDLL_API int enSendData(int nSequenceID, int CommandID, LPVOID lpData, int nSize );
EPDLL_API void enSetLog(BOOL bWrite);
EPDLL_API LPSTR enGetClientIP();
EPDLL_API void enSetParent(HWND hWnd);
EPDLL_API void enSetSystemID(int nID);

#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif	//_EP_FORMATION_DATA_ROUTER_DLL_DEFINE_H_


