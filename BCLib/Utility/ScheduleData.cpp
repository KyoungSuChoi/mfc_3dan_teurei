// ScheduleData.cpp: implementation of the CScheduleData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ScheduleData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScheduleData::CScheduleData()
{

}

CScheduleData::~CScheduleData()
{
	ResetData();
}

//Schedule File로 부터 공정 조건을 Load한다.
BOOL CScheduleData::SetSchedule(CString strFileName)
{
	if(strFileName.IsEmpty())	return FALSE;
	
	CString strTemp;

	//File Open
	FILE *fp = fopen(strFileName, "rt");
	if(fp == NULL){
		return FALSE;
	}

	ResetData();

	//파일 Header 기록 
	PS_FILE_ID_HEADER	fileHeader;
	if(fread(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	//Header 정보 검사
	if(fileHeader.nFileID != SCHEDULE_FILE_ID)
	{
		fclose(fp);
		return FALSE;
	}
	//Header 정보 검사
	if(fileHeader.nFileVersion  > SCHEDULE_FILE_VER)
	{
		fclose(fp);
		return FALSE;
	}

	//모델 정보 기록 
	FILE_TEST_INFORMATION modelData;
	if(fread(&modelData, sizeof(modelData), 1, fp) < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ModelData = modelData;

	//공정 정보 기록 
	FILE_TEST_INFORMATION testData;
	if(fread(&testData, sizeof(testData), 1, fp)  < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ProcedureData = testData;

	//cell check 조건 기록 
	FILE_CELL_CHECK_PARAM cellCheck;
	if(fread(&cellCheck, sizeof(cellCheck), 1, fp)  < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_CellCheck = cellCheck;

	//step 정보 기록 
	CStep *pStep;
	FILE_STEP_PARAM	stepData;
	
	while(1)
	{
		if(fread(&stepData, sizeof(stepData), 1, fp) < 1)			//기록 실시 
			break;				
		
		pStep = new CStep;
		pStep->SetStepData(stepData);
		m_apStepArray.Add(pStep);
	}
	fclose(fp);

	return TRUE;
}

//공정 조건을 File로 저장한다.
BOOL CScheduleData::SaveToFile(CString strFileName)
{
	CString strTemp;
	if(strFileName.IsEmpty())	return FALSE;

	FILE *fp = fopen(strFileName, "wt");
	if(fp == NULL){
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	FileHeader;

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(FileHeader.szCreateDateTime, "%s", dateTime.Format());			//기록 시간
	FileHeader.nFileID = SCHEDULE_FILE_ID;									//파일 ID
	FileHeader.nFileVersion = SCHEDULE_FILE_VER;							//파일 Version
	sprintf(FileHeader.szDescrition, "ADPOWER power supply scheule file.");	//파일 서명 
	fwrite(&FileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);					//기록 실시 
	
	//모델 정보 기록 
	fwrite(&m_ModelData, sizeof(m_ModelData), 1, fp);				//기록 실시 

	//공정 정보 기록 
	fwrite(&m_ProcedureData, sizeof(m_ProcedureData), 1, fp);		//기록 실시 

	//cell check 조건 기록 
	fwrite(&m_CellCheck, sizeof(m_CellCheck), 1, fp);				//기록 실시 
	
	//step 정보 기록 
	FILE_STEP_PARAM fileStep;
	CStep *pStep;
	for(int i = 0; i< m_apStepArray.GetSize(); i++)
	{
		pStep = (CStep *)m_apStepArray.GetAt(i);
		fileStep = pStep->GetFileStep();
		fwrite(&fileStep, sizeof(fileStep), 1, fp);					//기록 실시 

	}
	fclose(fp);
	return TRUE;
}

UINT CScheduleData::GetStepSize()
{
	return m_apStepArray.GetSize();
}

void CScheduleData::ResetData()
{
	memset(&m_ModelData, 0, sizeof(m_ModelData));
	memset(&m_ProcedureData, 0, sizeof(m_ProcedureData));
	memset(&m_CellCheck, 0, sizeof(m_CellCheck));

	CStep *pStep;
	for(int i = m_apStepArray.GetSize()-1; i>=0; i--)
	{
		pStep = (CStep *)m_apStepArray[i];
		delete pStep;
		pStep = NULL;
		m_apStepArray.RemoveAt(i);
	}
	m_apStepArray.RemoveAll();
}

BOOL CScheduleData::SetSchedule(CString strDBName, long lModelPK, long lTestPK)
{
	CDaoDatabase  db;
	if(strDBName.IsEmpty())		return 0;

	try
	{
		db.Open(strDBName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	ResetData();

	if(LoadModelInfo(db, lModelPK) == FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadProcedureInfo(db, lModelPK, lTestPK)== FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadStepInfo(db, lTestPK) == FALSE)
	{
		db.Close();
		return FALSE;
	}

	db.Close();


	return FALSE;
}

BOOL CScheduleData::LoadModelInfo(CDaoDatabase &db, long lPK)
{
	ASSERT(db.IsOpen());

	CString strSQL;
	strSQL.Format("SELECT No, ModelName, Description, CreatedTime FROM BatteryModel WHERE ModelID = %d ORDER BY No", lPK);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}
	
	//No
	data = rs.GetFieldValue(0);
	m_ModelData.lID = data.lVal;
	//ModelName
	data = rs.GetFieldValue(1);
	sprintf(m_ModelData.szName, "%s", data.pcVal);
	//Description
	data = rs.GetFieldValue(2);
	sprintf(m_ModelData.szDescription, "%s", data.pcVal);
	//DateTime
	data = rs.GetFieldValue(3);
	COleDateTime  date = data;
	sprintf(m_ModelData.szModifiedTime, "%d/%d/%d %d:%d:%d", date.Format());

	rs.Close();

	return TRUE;
}

BOOL CScheduleData::LoadProcedureInfo(CDaoDatabase &db, long lModelPK, long lProcPK)
{
	ASSERT(db.IsOpen());

	CString strSQL;
	strSQL.Format(" SELECT TestNo, ProcTypeID, TestName, Description, Creator, ModifiedTime FROM TestList WHERE ModelID =  %d AND TestID = %d ORDER BY TestNo", 
					lModelPK, lProcPK);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}
	
	//TestNo
	data = rs.GetFieldValue(0);
	m_ProcedureData.lID = data.lVal;
	//ProcTypeID
	data = rs.GetFieldValue(1);
	//TestName
	data = rs.GetFieldValue(2);
	sprintf(m_ProcedureData.szName, "%s", data.pcVal);
	//Description
	data = rs.GetFieldValue(3);
	sprintf(m_ProcedureData.szDescription, "%s", data.pcVal);
	//Creator
	data = rs.GetFieldValue(4);
	//DateTime
	data = rs.GetFieldValue(5);
	COleDateTime  date = data;
	sprintf(m_ProcedureData.szModifiedTime, "%d/%d/%d %d:%d:%d", date.Format());

	rs.Close();

	return TRUE;
}

BOOL CScheduleData::LoadStepInfo(CDaoDatabase &db, long lProcPK)
{
	ASSERT(db.IsOpen());
	
	CString strSQL, strQuery;
	CString strFrom;
	
	strSQL += "StepID, StepNo, StepProcType, StepType, StepMode, Vref, Iref, EndTime, EndV, EndI, EndCapacity, End_dV, End_dI, CycleCount ";	//14
	strSQL += ", OverV, LimitV, OverI, LimitI, OverCapacity, LimitCapacity, OverImpedance, LimitImpedance, DeltaTime, DeltaTime1, DeltaV, DeltaI ";	//12
	strSQL += ", Grade, CompTimeV1, CompTimeV2, CompTimeV3,	CompVLow1,	CompVLow2, CompVLow3, CompVHigh1, CompVHigh2, CompVHigh3 ";	//10
	strSQL += ", CompTimeI1, CompTimeI2, CompTimeI3, CompILow1, CompILow2, CompILow3, CompIHigh1, CompIHigh2, CompIHigh3, RecordTime, RecordDeltaV, RecordDeltaI ";	//12
	strSQL += ", CapVLow, CapVHigh ";	//2
//	strSQL += ", EndCheckVLow, EndCheckVHigh, EndCheckILow, EndCheckIHigh ";		//4
//	strSQL += ", Value0, Value1, Value2, Value3, Value4, Value5, Value6, Value7, Value8, Value9 ";	//10

	strFrom.Format(" FROM Step WHERE TestID = %d ORDER BY StepNo", lProcPK);
	
	strQuery = strSQL+strFrom;

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CStep *pStepData;
	while(!rs.IsEOF())
	{
		pStepData = new CStep;
		ASSERT(pStepData);

		data = rs.GetFieldValue(0);		//long	m_StepID;	
		pStepData->m_StepID = data.lVal;
		data = rs.GetFieldValue(1);		//long	m_StepNo;
		pStepData->m_StepIndex = data.lVal-1;
		data = rs.GetFieldValue(2);		//long	m_StepProcType;
		pStepData->m_lProcType = data.lVal;
		data = rs.GetFieldValue(3);		//long	m_StepType;
		pStepData->m_type = (BYTE)data.lVal;
		data = rs.GetFieldValue(4);		//long	m_StepMode;
		pStepData->m_mode = (BYTE)data.lVal;
		data = rs.GetFieldValue(5);		//float	m_Vref;
		pStepData->m_fVref = data.fltVal;
		data = rs.GetFieldValue(6);		//float	m_Iref;
		pStepData->m_fIref = data.fltVal;
		data = rs.GetFieldValue(7);		//long	m_EndTime;
		pStepData->m_ulEndTime = data.lVal;
		data = rs.GetFieldValue(8);		//float	m_EndV;
		pStepData->m_fEndV = data.fltVal;
		data = rs.GetFieldValue(9);		//float	m_EndI;
		pStepData->m_fEndI = data.fltVal;
		data = rs.GetFieldValue(10);	//float	m_EndCapacity;
		pStepData->m_fEndC = data.fltVal;
		data = rs.GetFieldValue(11);	//float	m_End_dV;
		pStepData->m_fEndDV = data.fltVal;
		data = rs.GetFieldValue(12);	//float	m_End_dI;
		pStepData->m_fEndDI = data.fltVal;
		data = rs.GetFieldValue(13);	//long	m_CycleCount;
		pStepData->m_nLoopInfoCycle = data.lVal;
		
		data = rs.GetFieldValue(14);	//float	m_OverV;
		pStepData->m_fHighLimitV = data.fltVal;
		data = rs.GetFieldValue(15);	//float	m_LimitV;
		pStepData->m_fLowLimitV = data.fltVal;
		data = rs.GetFieldValue(16);	//float	m_OverI;
		pStepData->m_fHighLimitI = data.fltVal;
		data = rs.GetFieldValue(17);	//float	m_LimitI;
		pStepData->m_fLowLimitI = data.fltVal;
		data = rs.GetFieldValue(18);	//float	m_OverCapacity;
		pStepData->m_fHighLimitC = data.fltVal;
		data = rs.GetFieldValue(19);	//float	m_LimitCapacity;
		pStepData->m_fLowLimitC = data.fltVal;
		data = rs.GetFieldValue(20);	//float	m_OverImpedance;
		pStepData->m_fHighLimitImp = data.fltVal;
		data = rs.GetFieldValue(21);	//float	m_LimitImpedance;
		pStepData->m_fLowLimitImp = data.fltVal;

		data = rs.GetFieldValue(22);	//long	m_DeltaTime;
		pStepData->m_lDeltaTimeV = data.lVal;
		data = rs.GetFieldValue(23);	//long	m_DeltaTime1;
		pStepData->m_lDeltaTimeI = data.lVal;
		data = rs.GetFieldValue(24);	//float	m_DeltaV;
		pStepData->m_fDeltaV = data.fltVal;
		data = rs.GetFieldValue(25);	//float	m_DeltaI;
		pStepData->m_fDeltaI = data.fltVal;
		data = rs.GetFieldValue(26);	//BOOL	m_Grade;
		pStepData->m_bGrade = (BYTE)data.lVal;
		
		data = rs.GetFieldValue(27);	//long	m_CompTimeV1;
		pStepData->m_lCompTimeV[0] = data.lVal;
		data = rs.GetFieldValue(28);	//long	m_CompTimeV2;
		pStepData->m_lCompTimeV[1] = data.lVal;
		data = rs.GetFieldValue(29);	//long	m_CompTimeV3;
		pStepData->m_lCompTimeV[2] = data.lVal;
		data = rs.GetFieldValue(30);	//float	m_CompVLow1;
		pStepData->m_fCompLowV[0] = data.fltVal;
		data = rs.GetFieldValue(31);	//float	m_CompVLow2;
		pStepData->m_fCompLowV[1] = data.fltVal;
		data = rs.GetFieldValue(32);	//float	m_CompVLow3;
		pStepData->m_fCompLowV[2] = data.fltVal;
		data = rs.GetFieldValue(33);	//float	m_CompVHigh1;
		pStepData->m_fCompHighV[0] = data.fltVal;
		data = rs.GetFieldValue(34);	//float	m_CompVHigh2;
		pStepData->m_fCompHighV[1] = data.fltVal;
		data = rs.GetFieldValue(35);	//float	m_CompVHigh3;
		pStepData->m_fCompHighV[2] = data.fltVal;
		
		data = rs.GetFieldValue(36);	//long	m_CompTimeI1;
		pStepData->m_lCompTimeI[0] = data.lVal;
		data = rs.GetFieldValue(37);	//long	m_CompTimeI2;
		pStepData->m_lCompTimeI[1] = data.lVal;
		data = rs.GetFieldValue(38);	//long	m_CompTimeI3;
		pStepData->m_lCompTimeI[2] = data.lVal;
		data = rs.GetFieldValue(39);	//float	m_CompILow1;
		pStepData->m_fCompLowI[0] = data.fltVal;
		data = rs.GetFieldValue(40);	//float	m_CompILow2;
		pStepData->m_fCompLowI[1] = data.fltVal;
		data = rs.GetFieldValue(41);	//float	m_CompILow3;
		pStepData->m_fCompLowI[2] = data.fltVal;
		data = rs.GetFieldValue(42);	//float	m_CompIHigh1;
		pStepData->m_fCompHighI[0] = data.fltVal;
		data = rs.GetFieldValue(43);	//float	m_CompIHigh2;
		pStepData->m_fCompHighI[1] = data.fltVal;
		data = rs.GetFieldValue(44);	//float	m_CompIHigh3;
		pStepData->m_fCompHighI[2] = data.fltVal;

		data = rs.GetFieldValue(45);	//long	m_RecordTime;
		pStepData->m_ulReportTime = data.lVal;
		data = rs.GetFieldValue(46);	//float	m_RecordDeltaV;
		pStepData->m_fReportV = data.fltVal;
		data = rs.GetFieldValue(47);	//float	m_RecordDeltaI;
		pStepData->m_fReportI = data.fltVal;

		data = rs.GetFieldValue(48);	//float	m_CapVLow;
		pStepData->m_fCapaVoltage1 = data.fltVal;
		data = rs.GetFieldValue(49);	//float	m_CapVHigh;
		pStepData->m_fCapaVoltage2 = data.fltVal;

		//Not Use
//		data = rs.GetFieldValue(50);	//float	m_EndCheckVLow;
//		data = rs.GetFieldValue(51);	//float	m_EndCheckVHigh;
//		data = rs.GetFieldValue(52);	//float	m_EndCheckILow;
//		data = rs.GetFieldValue(53);	//float	m_EndCheckIHigh;
//		data = rs.GetFieldValue(54);	//CString	m_Value0;
//		data = rs.GetFieldValue(55);	//CString	m_Value1;	
//		data = rs.GetFieldValue(56);	//CString	m_Value2;	
//		data = rs.GetFieldValue(57);	//CString	m_Value3;	
//		data = rs.GetFieldValue(58);	//CString	m_Value4;	
//		data = rs.GetFieldValue(59);	//CString	m_Value5;	
//		data = rs.GetFieldValue(60);	//CString	m_Value6;	
//		data = rs.GetFieldValue(61);	//CString	m_Value7;	
//		data = rs.GetFieldValue(62);	//CString	m_Value8;	
//		data = rs.GetFieldValue(63);	//CString	m_Value9;	


		if(pStepData->m_bGrade == TRUE)
		{
			LoadGradeInfo(db, pStepData);
		}
		rs.MoveNext();

		m_apStepArray.Add(pStepData);
	}

	rs.Close();
	
	return m_apStepArray.GetSize();
}

BOOL CScheduleData::LoadGradeInfo(CDaoDatabase &db, CStep *pStepData)
{
	ASSERT(db.IsOpen());
	ASSERT(pStepData);

	CString strSQL, strQuery;
	CString strFrom;
	
	strSQL.Format("SELECT Value, Value1, GradeItem, GradeCode FROM GRADE WHERE StepID = %d ORDERBY GradeID", pStepData->m_StepID);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	
	float fdata1, fdata2; 
	CString strcode;
	while(!rs.IsEOF())
	{
		//value0
		data = rs.GetFieldValue(0);	
		fdata1 = data.fltVal;
		//value1
		data = rs.GetFieldValue(1);	
		fdata2 = data.fltVal;
		//Item
		data = rs.GetFieldValue(2);	
		pStepData->m_Grading.m_lGradingItem = data.lVal;
		//code
		data = rs.GetFieldValue(3);	
		strcode = data.pcVal;
		pStepData->m_Grading.AddGradeStep(fdata1, fdata2, strcode);
		rs.MoveNext();
	}

	return pStepData->m_Grading.GetGradeStepSize();
}

CStep* CScheduleData::GetStepData(UINT nStepIndex)
{
	if(GetStepSize() < nStepIndex)
	{
		return NULL;
	}
	return (CStep *)m_apStepArray[nStepIndex];	
}

BOOL CScheduleData::ExecuteEditor(long lModelPK, long lTestPK)
{

	return FALSE;
}
