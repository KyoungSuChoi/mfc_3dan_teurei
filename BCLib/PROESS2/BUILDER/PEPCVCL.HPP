//----------------------------------------------------------------------------
// Pepcvcl.hpp - bcbdcc32 generated hdr (DO NOT EDIT) rev: 0
// From: Pepcvcl.pas
//----------------------------------------------------------------------------
#ifndef PepcvclHPP
#define PepcvclHPP
//----------------------------------------------------------------------------
#include <Pegrpapi.hpp>
#include <Controls.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <Messages.hpp>
#include <Windows.hpp>
#include <SysUtils.hpp>
#include <System.hpp>
namespace Pepcvcl
{
//-- type declarations -------------------------------------------------------
enum eViewingStyle { pcColor, pcMonochrome, pcMonoPlusSymbols };

enum eDataPrecision { pcNoDecimals, pcOneDecimal, pcTwoDecimals, pcThreeDecimals };

enum eFontSize { pcLarge, pcMedium, pcSmall };

enum eGroupingPercent { pcNoGrouping, pcOnePercent, pcTwoPercent, pcThreePercent, pcFourPercent, pcFivePercent 
	};

enum eDataLabelType { pcPercentage, pcDataValue };

enum eDefOrientation { pcDriverDefault, pcLandscape, pcPortrait };

enum eAutoExplode { pcNoAutoExplode, pcExplodeAllSubsets, pcExplodeIndSubsets };

enum ePEactions { pcReinitAndReset, pcReinitialize, pcResetImage, pcInvalidateImage, pcMaximize, pcCustomize, 
	pcExportDialog, pcTextExportDialog, pcPrintDialog, pcColorDialog, pcFontDialog, pcPopupMenu, pcMetaToClip, 
	pcMetaToFile, pcBmpToClip, pcBmpToFile, pcOleToClip, pcReinitCustoms, pcPrintGraph, pcReset };

enum TScrollCode { scLineUp, scLineDown, scPageUp, scPageDown, scPosition, scTrack, scTop, scBottom, 
	scEndScroll };

typedef void __fastcall (__closure *TScrollEvent)(System::TObject* Sender, TScrollCode ScrollCode, int 
	&ScrollPos);

typedef void __fastcall (__closure *TSubsetHotSpot)(System::TObject* Sender, int DblClick, int SubsetIndex
	);

typedef void __fastcall (__closure *TPointHotSpot)(System::TObject* Sender, int DblClick, int SubsetIndex
	, int PointIndex);

class __declspec(delphiclass) PEPieChart;
class __declspec(pascalimplementation) PEPieChart : public Controls::TWinControl
{
	typedef Controls::TWinControl inherited;
	
private:
	Windows::TRect r;
	int hObj;
	int hMeta;
	int bSizeAltered;
	int bPrepareImages;
	int hDC;
	int hBitmap;
	int hMemBitmap;
	int bCacheBmp;
	int hTmpBmp;
	int hMemDC;
	int nOldMode;
	int nOldROP2;
	tagPAINTSTRUCT ps;
	int w;
	int h;
	long l;
	Messages::TMessage tmsg;
	int wOld;
	long lOld;
	float fxratio;
	float fyratio;
	tagPOINT tpts[5];
	HRGN hRgn1;
	HRGN hRgn2;
	HRGN hRgn3;
	int min;
	int max;
	int pos;
	Windows::TRect *tptr;
	HWND h1;
	HWND h2;
	char tmpszstr[255];
	System::AnsiString tmpstring;
	bool bPEcreated;
	float tmpS;
	double tmpD;
	int tmpN;
	int tmpPEactions;
	int tmpPEnarg1;
	int tmpPEnarg2;
	System::AnsiString tmpPEstrarg1;
	Classes::TNotifyEvent FOnParamUpdate;
	TScrollEvent FOnVertScroll;
	TSubsetHotSpot FOnSubsetHotSpot;
	TPointHotSpot FOnPointHotSpot;
	void __fastcall ResetandInvalidate(void);
	int __fastcall GethObject(void);
	eViewingStyle __fastcall GetViewingStyle(void);
	void __fastcall SetViewingStyle(eViewingStyle p0);
	bool __fastcall GetPrepareImages(void);
	void __fastcall SetPrepareImages(bool p0);
	Graphics::TColor __fastcall GetGraphForeColor(void);
	void __fastcall SetGraphForeColor(Graphics::TColor p0);
	Graphics::TColor __fastcall GetGraphBackColor(void);
	void __fastcall SetGraphBackColor(Graphics::TColor p0);
	Graphics::TColor __fastcall GetDeskColor(void);
	void __fastcall SetDeskColor(Graphics::TColor p0);
	Graphics::TColor __fastcall GetTextColor(void);
	void __fastcall SetTextColor(Graphics::TColor p0);
	Graphics::TColor __fastcall GetShadowColor(void);
	void __fastcall SetShadowColor(Graphics::TColor p0);
	System::AnsiString __fastcall GetMainTitle(void);
	void __fastcall SetMainTitle( System::AnsiString p0);
	System::AnsiString __fastcall GetSubTitle(void);
	void __fastcall SetSubTitle( System::AnsiString p0);
	int __fastcall GetSubsets(void);
	void __fastcall SetSubsets(int p0);
	int __fastcall GetPoints(void);
	void __fastcall SetPoints(int p0);
	eDataPrecision __fastcall GetDataPrecision(void);
	void __fastcall SetDataPrecision(eDataPrecision p0);
	bool __fastcall GetAllowCustomization(void);
	void __fastcall SetAllowCustomization(bool p0);
	bool __fastcall GetAllowExporting(void);
	void __fastcall SetAllowExporting(bool p0);
	bool __fastcall GetAllowMaximization(void);
	void __fastcall SetAllowMaximization(bool p0);
	bool __fastcall GetAllowPopup(void);
	void __fastcall SetAllowPopup(bool p0);
	bool __fastcall GetAllowUserInterface(void);
	void __fastcall SetAllowUserInterface(bool p0);
	eFontSize __fastcall GetFontSize(void);
	void __fastcall SetFontSize(eFontSize p0);
	System::AnsiString __fastcall GetMainTitleFont(void);
	void __fastcall SetMainTitleFont( System::AnsiString p0);
	bool __fastcall GetMainTitleBold(void);
	void __fastcall SetMainTitleBold(bool p0);
	bool __fastcall GetMainTitleItalic(void);
	void __fastcall SetMainTitleItalic(bool p0);
	bool __fastcall GetMainTitleUnderline(void);
	void __fastcall SetMainTitleUnderline(bool p0);
	System::AnsiString __fastcall GetSubTitleFont(void);
	void __fastcall SetSubTitleFont( System::AnsiString p0);
	bool __fastcall GetSubTitleBold(void);
	void __fastcall SetSubTitleBold(bool p0);
	bool __fastcall GetSubTitleItalic(void);
	void __fastcall SetSubTitleItalic(bool p0);
	bool __fastcall GetSubTitleUnderline(void);
	void __fastcall SetSubTitleUnderline(bool p0);
	System::AnsiString __fastcall GetLabelFont(void);
	void __fastcall SetLabelFont( System::AnsiString p0);
	bool __fastcall GetLabelBold(void);
	void __fastcall SetLabelBold(bool p0);
	bool __fastcall GetLabelItalic(void);
	void __fastcall SetLabelItalic(bool p0);
	bool __fastcall GetLabelUnderline(void);
	void __fastcall SetLabelUnderline(bool p0);
	eGroupingPercent __fastcall GetGroupingPercent(void);
	void __fastcall SetGroupingPercent(eGroupingPercent p0);
	eDataLabelType __fastcall GetDataLabelType(void);
	void __fastcall SetDataLabelType(eDataLabelType p0);
	bool __fastcall GetDataShadows(void);
	void __fastcall SetDataShadows(bool p0);
	bool __fastcall GetThreeDDialogs(void);
	void __fastcall SetThreeDDialogs(bool p0);
	eDefOrientation __fastcall GetDefOrientation(void);
	void __fastcall SetDefOrientation(eDefOrientation p0);
	int __fastcall GetObjectType(void);
	float __fastcall GetXData(int p0, int p1);
	void __fastcall SetXData(int p0, int p1, float p2);
	float __fastcall GetYData(int p0, int p1);
	void __fastcall SetYData(int p0, int p1, float p2);
	System::AnsiString __fastcall GetSubsetLabels(int p0);
	void __fastcall SetSubsetLabels(int p0,  System::AnsiString p1);
	System::AnsiString __fastcall GetPointLabels(int p0);
	void __fastcall SetPointLabels(int p0,  System::AnsiString p1);
	bool __fastcall GetAllowSubsetHotSpots(void);
	void __fastcall SetAllowSubsetHotSpots(bool p0);
	bool __fastcall GetAllowPointHotSpots(void);
	void __fastcall SetAllowPointHotSpots(bool p0);
	Graphics::TColor __fastcall GetSubsetColors(int p0);
	void __fastcall SetSubsetColors(int p0, Graphics::TColor p1);
	eAutoExplode __fastcall GetAutoExplode(void);
	void __fastcall SetAutoExplode(eAutoExplode p0);
	ePEactions __fastcall GetPEactions(void);
	void __fastcall SetPEactions(ePEactions p0);
	int __fastcall GetPEnarg1(void);
	void __fastcall SetPEnarg1(int p0);
	int __fastcall GetPEnarg2(void);
	void __fastcall SetPEnarg2(int p0);
	System::AnsiString __fastcall GetPEstrarg1(void);
	void __fastcall SetPEstrarg1( System::AnsiString p0);
	int __fastcall GetVertScrollPos(void);
	void __fastcall SetVertScrollPos(int p0);
	bool __fastcall GetAllowDebugOutput(void);
	void __fastcall SetAllowDebugOutput(bool p0);
	System::AnsiString __fastcall GetMultiSubTitles(int p0);
	void __fastcall SetMultiSubTitles(int p0,  System::AnsiString p1);
	System::AnsiString __fastcall GetMultiBottomTitles(int p0);
	void __fastcall SetMultiBottomTitles(int p0,  System::AnsiString p1);
	bool __fastcall GetFocalRect(void);
	void __fastcall SetFocalRect(bool p0);
	float __fastcall GetFontSizeGlobalCntl(void);
	void __fastcall SetFontSizeGlobalCntl(float p0);
	float __fastcall GetFontSizeTitleCntl(void);
	void __fastcall SetFontSizeTitleCntl(float p0);
	bool __fastcall GetSubsetByPoint(void);
	void __fastcall SetSubsetByPoint(bool p0);
	bool __fastcall GetAllowOleExport(void);
	void __fastcall SetAllowOleExport(bool p0);
	int __fastcall GetArrowCursor(void);
	void __fastcall SetArrowCursor(int p0);
	int __fastcall GetHandCursor(void);
	void __fastcall SetHandCursor(int p0);
	int __fastcall GetZoomCursor(void);
	void __fastcall SetZoomCursor(int p0);
	int __fastcall GetNoDropCursor(void);
	void __fastcall SetNoDropCursor(int p0);
	
public:
	__fastcall virtual PEPieChart(Classes::TComponent* p0);
	__fastcall virtual ~PEPieChart(void);
	MESSAGE void __fastcall WMCreate(Messages::TWMCreate &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMDestroy(Messages::TWMNoParams &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMPaint(Messages::TWMPaint &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMSize(Messages::TWMSize &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMMouseMove(Messages::TWMMouse &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMLButtonDown(Messages::TWMMouse &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMLButtonUp(Messages::TWMMouse &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMLButtonDblClk(Messages::TWMMouse &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMRButtonUp(Messages::TWMMouse &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMKeyDown(Messages::TWMKey &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMKeyUp(Messages::TWMKey &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMSetFocus(Messages::TWMSetFocus &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMKillFocus(Messages::TWMKillFocus &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMVScroll(Messages::TWMScroll &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMCommand(Messages::TWMCommand &Pepcvcl_);
	HIDESBASE MESSAGE void __fastcall WMEraseBkGnd(Messages::TWMEraseBkgnd &Pepcvcl_);
	MESSAGE void __fastcall WMWindowPosChanging(Messages::TWMWindowPosMsg &Pepcvcl_);
	MESSAGE void __fastcall WMGetDlgCode(Messages::TWMNoParams &Pepcvcl_);
	MESSAGE void __fastcall PEWMInvalidateRect(Messages::TMessage &Pepcvcl_);
	HIDESBASE virtual void __fastcall Loaded(void);
	__property int hObject = {read=GethObject, nodefault};
	__property int ObjectType = {read=GetObjectType, nodefault};
	__property float XData[int nSubset][int nPoint] = {read=GetXData, write=SetXData};
	__property float YData[int nSubset][int nPoint] = {read=GetYData, write=SetYData};
	__property System::AnsiString SubsetLabels[int nIndex] = {read=GetSubsetLabels, write=SetSubsetLabels
		};
	__property System::AnsiString PointLabels[int nIndex] = {read=GetPointLabels, write=SetPointLabels}
		;
	__property Graphics::TColor SubsetColors[int nIndex] = {read=GetSubsetColors, write=SetSubsetColors
		};
	__property ePEactions PEactions = {read=GetPEactions, write=SetPEactions, nodefault};
	__property int PEnarg1 = {read=GetPEnarg1, write=SetPEnarg1, nodefault};
	__property int PEnarg2 = {read=GetPEnarg2, write=SetPEnarg2, nodefault};
	__property System::AnsiString PEstrarg1 = {read=GetPEstrarg1, write=SetPEstrarg1, nodefault};
	__property int VertScrollPos = {read=GetVertScrollPos, write=SetVertScrollPos, nodefault};
	__property bool AllowDebugOutput = {read=GetAllowDebugOutput, write=SetAllowDebugOutput, nodefault}
		;
	__property System::AnsiString MultiSubTitles[int nIndex] = {read=GetMultiSubTitles, write=SetMultiSubTitles
		};
	__property System::AnsiString MultiBottomTitles[int nIndex] = {read=GetMultiBottomTitles, write=SetMultiBottomTitles
		};
	__property bool SubsetByPoint = {read=GetSubsetByPoint, write=SetSubsetByPoint, nodefault};
	__property int ArrowCursor = {read=GetArrowCursor, write=SetArrowCursor, nodefault};
	__property int HandCursor = {read=GetHandCursor, write=SetHandCursor, nodefault};
	__property int ZoomCursor = {read=GetZoomCursor, write=SetZoomCursor, nodefault};
	__property int NoDropCursor = {read=GetNoDropCursor, write=SetNoDropCursor, nodefault};
	
__published:
	__property Left ;
	__property Top ;
	__property Width ;
	__property Height ;
	__property Hint ;
	__property HelpContext ;
	__property Tag ;
	__property ShowHint ;
	__property TabStop ;
	__property Visible ;
	__property TabOrder ;
	__property ParentShowHint ;
	__property Enabled ;
	__property Cursor ;
	__property DragCursor ;
	__property DragMode ;
	__property Name ;
	__property Align ;
	__property eViewingStyle ViewingStyle = {read=GetViewingStyle, write=SetViewingStyle, nodefault};
	__property bool PrepareImages = {read=GetPrepareImages, write=SetPrepareImages, nodefault};
	__property Graphics::TColor GraphForeColor = {read=GetGraphForeColor, write=SetGraphForeColor, nodefault
		};
	__property Graphics::TColor GraphBackColor = {read=GetGraphBackColor, write=SetGraphBackColor, nodefault
		};
	__property Graphics::TColor DeskColor = {read=GetDeskColor, write=SetDeskColor, nodefault};
	__property Graphics::TColor TextColor = {read=GetTextColor, write=SetTextColor, nodefault};
	__property Graphics::TColor ShadowColor = {read=GetShadowColor, write=SetShadowColor, nodefault};
	__property System::AnsiString MainTitle = {read=GetMainTitle, write=SetMainTitle, nodefault};
	__property System::AnsiString SubTitle = {read=GetSubTitle, write=SetSubTitle, nodefault};
	__property int Subsets = {read=GetSubsets, write=SetSubsets, nodefault};
	__property int Points = {read=GetPoints, write=SetPoints, nodefault};
	__property eDataPrecision DataPrecision = {read=GetDataPrecision, write=SetDataPrecision, nodefault
		};
	__property bool AllowCustomization = {read=GetAllowCustomization, write=SetAllowCustomization, nodefault
		};
	__property bool AllowExporting = {read=GetAllowExporting, write=SetAllowExporting, nodefault};
	__property bool AllowMaximization = {read=GetAllowMaximization, write=SetAllowMaximization, nodefault
		};
	__property bool AllowPopup = {read=GetAllowPopup, write=SetAllowPopup, nodefault};
	__property bool AllowUserInterface = {read=GetAllowUserInterface, write=SetAllowUserInterface, nodefault
		};
	__property eFontSize FontSize = {read=GetFontSize, write=SetFontSize, nodefault};
	__property System::AnsiString MainTitleFont = {read=GetMainTitleFont, write=SetMainTitleFont, nodefault
		};
	__property bool MainTitleBold = {read=GetMainTitleBold, write=SetMainTitleBold, nodefault};
	__property bool MainTitleItalic = {read=GetMainTitleItalic, write=SetMainTitleItalic, nodefault};
	__property bool MainTitleUnderline = {read=GetMainTitleUnderline, write=SetMainTitleUnderline, nodefault
		};
	__property System::AnsiString SubTitleFont = {read=GetSubTitleFont, write=SetSubTitleFont, nodefault
		};
	__property bool SubTitleBold = {read=GetSubTitleBold, write=SetSubTitleBold, nodefault};
	__property bool SubTitleItalic = {read=GetSubTitleItalic, write=SetSubTitleItalic, nodefault};
	__property bool SubTitleUnderline = {read=GetSubTitleUnderline, write=SetSubTitleUnderline, nodefault
		};
	__property System::AnsiString LabelFont = {read=GetLabelFont, write=SetLabelFont, nodefault};
	__property bool LabelBold = {read=GetLabelBold, write=SetLabelBold, nodefault};
	__property bool LabelItalic = {read=GetLabelItalic, write=SetLabelItalic, nodefault};
	__property bool LabelUnderline = {read=GetLabelUnderline, write=SetLabelUnderline, nodefault};
	__property eGroupingPercent GroupingPercent = {read=GetGroupingPercent, write=SetGroupingPercent, nodefault
		};
	__property eDataLabelType DataLabelType = {read=GetDataLabelType, write=SetDataLabelType, nodefault
		};
	__property bool DataShadows = {read=GetDataShadows, write=SetDataShadows, nodefault};
	__property bool ThreeDDialogs = {read=GetThreeDDialogs, write=SetThreeDDialogs, nodefault};
	__property eDefOrientation DefOrientation = {read=GetDefOrientation, write=SetDefOrientation, nodefault
		};
	__property bool AllowSubsetHotSpots = {read=GetAllowSubsetHotSpots, write=SetAllowSubsetHotSpots, nodefault
		};
	__property bool AllowPointHotSpots = {read=GetAllowPointHotSpots, write=SetAllowPointHotSpots, nodefault
		};
	__property bool FocalRect = {read=GetFocalRect, write=SetFocalRect, nodefault};
	__property float FontSizeGlobalCntl = {read=GetFontSizeGlobalCntl, write=SetFontSizeGlobalCntl, nodefault
		};
	__property float FontSizeTitleCntl = {read=GetFontSizeTitleCntl, write=SetFontSizeTitleCntl, nodefault
		};
	__property bool AllowOleExport = {read=GetAllowOleExport, write=SetAllowOleExport, nodefault};
	__property OnClick ;
	__property OnDblClick ;
	__property OnDragDrop ;
	__property OnDragOver ;
	__property OnEndDrag ;
	__property OnMouseDown ;
	__property OnMouseMove ;
	__property OnMouseUp ;
	__property OnEnter ;
	__property OnKeyUp ;
	__property OnKeyDown ;
	__property OnExit ;
	__property OnKeyPress ;
	__property Classes::TNotifyEvent OnParamUpdate = {read=FOnParamUpdate, write=FOnParamUpdate};
	__property TScrollEvent OnVertScroll = {read=FOnVertScroll, write=FOnVertScroll};
	__property TSubsetHotSpot OnSubsetHotSpot = {read=FOnSubsetHotSpot, write=FOnSubsetHotSpot};
	__property TPointHotSpot OnPointHotSpot = {read=FOnPointHotSpot, write=FOnPointHotSpot};
public:
	/* TWinControl.CreateParented */ __fastcall PEPieChart(HWND ParentWindow) : Controls::TWinControl(ParentWindow
		) { }
	
};

//-- var, const, procedure ---------------------------------------------------
extern void __fastcall Register(void);

}	/* namespace Pepcvcl */
#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using namespace Pepcvcl;
#endif
//-- end unit ----------------------------------------------------------------
#endif	// Pepcvcl
