//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "Unit5.h"
//---------------------------------------------------------------------------
#pragma link "Pepcvcl"
#pragma resource "*.dfm"
TForm5 *Form5;
//---------------------------------------------------------------------------
__fastcall TForm5::TForm5(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm5::FormCreate(TObject *Sender)
{
int s,p;

PEPieChart1->PrepareImages = True;

PEPieChart1->Subsets = 5;
PEPieChart1->Points = 12;
for (s=0; s<5; s++)
   for(p=0; p<12; p++)
        PEPieChart1->XData[s][p] = 5 + (95 * ((float) rand() / (float) RAND_MAX));

PEPieChart1->SubsetLabels[0] = "Apples";
PEPieChart1->SubsetLabels[1] = "Oranges";
PEPieChart1->SubsetLabels[2] = "Pears";
PEPieChart1->SubsetLabels[3] = "Plums";
PEPieChart1->SubsetLabels[4] = "Peaches";

PEPieChart1->PointLabels[0] = "Texas";
PEPieChart1->PointLabels[1] = "Oklahoma";
PEPieChart1->PointLabels[2] = "Kansas";
PEPieChart1->PointLabels[3] = "New Mexico";
PEPieChart1->PointLabels[4] = "Colorado";
PEPieChart1->PointLabels[5] = "Wyoming";
PEPieChart1->PointLabels[6] = "Utah";
PEPieChart1->PointLabels[7] = "Nebraska";
PEPieChart1->PointLabels[8] = "Ohio";
PEPieChart1->PointLabels[9] = "Iowa";
PEPieChart1->PointLabels[10] = "North Dakota";
PEPieChart1->PointLabels[11] = "South Dakota";

PEPieChart1->SubsetColors[0] = clLime;
PEPieChart1->SubsetColors[1] = clBlue;
PEPieChart1->SubsetColors[2] = clRed;
PEPieChart1->SubsetColors[3] = clFuchsia;
PEPieChart1->SubsetColors[4] = clAqua;
PEPieChart1->SubsetColors[5] = clTeal;
PEPieChart1->SubsetColors[6] = clPurple;
PEPieChart1->SubsetColors[7] = clGray;
PEPieChart1->SubsetColors[8] = clLime;
PEPieChart1->SubsetColors[9] = clBlue;
PEPieChart1->SubsetColors[10] = clRed;
PEPieChart1->SubsetColors[11] = clFuchsia;
PEPieChart1->SubsetColors[12] = clAqua;

PEPieChart1->MainTitle = "Produce by State";
PEPieChart1->SubTitle = "";
PEPieChart1->FocalRect = False;

PEPieChart1->DataPrecision = pcOneDecimal;
PEPieChart1->FontSize = pcLarge;
PEPieChart1->GroupingPercent = pcThreePercent;

PEPieChart1->PEactions = pcReinitAndReset;
}
//--------------------------------------------------------------------------- 