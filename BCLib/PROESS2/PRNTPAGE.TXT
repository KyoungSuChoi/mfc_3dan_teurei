The following code shows how to programmatically send one or more images to 
the printer allowing you to combine other printed output with multiple
images.

This file contains 4 sections:

1) VB3 and VB4-16 bit example code
2) VB4-32 bit example code
3) Delphi v1 16 bit example code
4) Delphi v2 32 bit example code


'--------------------------------------------------------------------------------
'- VB3 and VB4-16 Example to place multiple images and other graphics on a page.
'--------------------------------------------------------------------------------

Global Declarations

Declare Function SetMapMode Lib "GDI" (ByVal hDC%, ByVal Mode%) As Integer
Declare Function SetViewportExt Lib "GDI" (ByVal hDC%, ByVal x%, ByVal y%) As Long
Declare Function SetViewportOrg Lib "GDI" (ByVal hDC%, ByVal x%, ByVal y%) As Long
Declare Function PlayMetafile Lib "GDI" (ByVal hDC%, ByVal hMeta%) As Integer
Declare Function PEgetmeta Lib "PEGRAPHS.DLL" (ByVal hObject%) As Integer
Declare Function PEresetimage Lib "PEGRAPHS.DLL" (ByVal hObject%, ByVal nLength%, ByVal nHeight%) As Integer

Sub Command2_Click ()

Dim hMeta As Integer
Dim hRes As Integer
Dim vRes As Integer
Dim test As Integer
Dim oldMM As Integer
Dim lresult As Long

'** send printer something so VB knows to start page **'
Printer.Print " "

'** get size of page **'
Printer.ScaleMode = 3
hRes = Printer.ScaleWidth
vRes = Printer.ScaleHeight

'** set mapping mode MM_ANSIOTROPIC **'
oldMM = SetMapMode(Printer.hDC, 8)

'** Set viewport org and extents **'
lresult = SetViewportOrg(Printer.hDC, 0, 0)
lresult = SetViewportExt(Printer.hDC, hRes, vRes)

'** reset image shape to match shape defined by SetViewportExt **'
test = PEresetimage(PEGraph1, hRes, vRes)
hMeta = PEgetmeta(PEGraph1)
test = PlayMetafile(Printer.hDC, hMeta)

Printer.EndDoc

' ** reset mapping mode **'
test = SetMapMode(Printer.hDC, oldMM)
' ** reset image to current aspect ratio **'
test = PEresetimage(PEGraph1, 0, 0)

End Sub



'-----------------------------------------------------------------------
'- VB4-32 Example to place multiple images and other graphics on a page.
'-----------------------------------------------------------------------

Global Declarations

Type POINTSTRUCT
    x As Long
    y As Long
End Type

Declare Function SetMapMode Lib "GDI32" (ByVal hDC&, ByVal Mode&) As Long
Declare Function SetViewportExtEx Lib "GDI32" (ByVal hDC&, ByVal x&, ByVal y&, lpPoint As Any) As Long
Declare Function SetViewportOrgEx Lib "GDI32" (ByVal hDC&, ByVal x&, ByVal y&, lpPoint As Any) As Long
Declare Function PlayMetaFile Lib "GDI32" (ByVal hDC&, ByVal hMeta&) As Long
Declare Function PEgetmeta Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PEresetimage Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nLength&, ByVal nHeight&) As Long

Sub Command2_Click ()

Dim hMeta As Long
Dim hRes As Long
Dim vRes As Long
Dim test As Long
Dim oldMM As Long
Dim lresult As Long

'** send printer something so VB knows to start page **'
Printer.Print " "

'** get size of page **'
Printer.ScaleMode = 3
hRes = Printer.ScaleWidth
vRes = Printer.ScaleHeight

'** set mapping mode MM_ANSIOTROPIC **'
oldMM = SetMapMode(Printer.hDC, 8)

'** Set viewport org and extents **'
Dim pt As POINTSTRUCT
lresult = SetViewportOrgEx(Printer.hDC, 0, 0, pt)
lresult = SetViewportExtEx(Printer.hDC, hRes, vRes, pt)

'** reset image shape to match shape defined by SetViewportExt **'
test = PEresetimage(Pesgo1, hRes, vRes)
hMeta = PEgetmeta(Pesgo1)
test = PlayMetaFile(Printer.hDC, hMeta)

Printer.EndDoc

' ** reset mapping mode **'
test = SetMapMode(Printer.hDC, oldMM)
' ** reset image to current aspect ratio **'
test = PEresetimage(Pesgo1, 0, 0)

End Sub



{-------------------------------------------------------------------
Delphi v1.0 16 bit example of placing multiple images and text
on a printed page.  You need to add Printers and
Pegrpapi to your uses clause}
--------------------------------------------------------------------}

procedure TForm2.FormClick(Sender: TObject);
var
hMeta: Integer;
test: Integer;
oldMM: Integer;
lresult: Longint;
wSize: Integer;
hSize: Integer;
begin

Printer.BeginDoc;
Printer.Canvas.TextOut(100,100, 'Hello World!');

{'** set mapping mode MM_ANSIOTROPIC **'}
oldMM := SetMapMode(Printer.Handle, 8);

{'** Set viewport org and extents **'}
hSize := Printer.PageHeight div 2;
wSize := Printer.PageWidth div 2;
lresult := SetViewportOrg(Printer.Handle, 0, hSize);
lresult := SetViewportExt(Printer.Handle, wSize, hSize);

{'** reset image shape to match shape defined by SetViewportExt **'}
PEresetimage(PEGraph1.hObject, wSize, hSize);
hMeta := PEgetmeta(PEGraph1.hObject);
PlayMetafile(Printer.Handle, hMeta);

{'** just place same image, but could be from another control**}
lresult := SetViewportOrg(Printer.Handle, wSize, hSize);
lresult := SetViewportExt(Printer.Handle, wSize, hSize);
PlayMetafile(Printer.Handle, hMeta);

Printer.EndDoc;

{' ** reset mapping mode **'}
test := SetMapMode(Printer.Handle, oldMM);
{' ** reset image to current aspect ratio **'}
PEresetimage(PEGraph1.hObject, 0, 0);

end;



{-------------------------------------------------------------------
Delphi v2.0 32 bit example of placing multiple images and text
on a printed page.  You need to add Printers and
Pegrpapi to your uses clause}
--------------------------------------------------------------------}

procedure TForm1.FormClick(Sender: TObject);
var
hMeta: Integer;
test: Integer;
oldMM: Integer;
lresult: Longbool;
wSize: Integer;
hSize: Integer;
pt: TPoint;
begin

Printer.BeginDoc;
Printer.Canvas.TextOut(100,100, 'Hello World!');

{'** set mapping mode MM_ANSIOTROPIC **'}
oldMM := SetMapMode(Printer.Handle, 8);

{'** Set viewport org and extents **'}
hSize := Printer.PageHeight div 2;
wSize := Printer.PageWidth div 2;
lresult := SetViewportOrgEx(Printer.Handle, 0, hSize, @pt);
lresult := SetViewportExtEx(Printer.Handle, wSize, hSize, @pt);

{'** reset image shape to match shape defined by SetViewportExt **'}
PEresetimage(PESGraph1.hObject, wSize, hSize);
hMeta := PEgetmeta(PESGraph1.hObject);
PlayMetafile(Printer.Handle, hMeta);

{'** just place same image, but could be from another control**}
lresult := SetViewportOrgEx(Printer.Handle, wSize, hSize, @pt);
lresult := SetViewportExtEx(Printer.Handle, wSize, hSize, @pt);
PlayMetaFile(Printer.Handle, hMeta);

Printer.EndDoc;

{' ** reset mapping mode **'}
test := SetMapMode(Printer.Handle, oldMM);
{' ** reset image to current aspect ratio **'}
PEresetimage(PESGraph1.hObject, 0, 0);

end;

