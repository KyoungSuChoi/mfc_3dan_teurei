unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Pegvcl;

type
  TForm2 = class(TForm)
    PEGraph1: PEGraph;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

procedure TForm2.FormCreate(Sender: TObject);
var
   s: Integer;
   p: Integer;
begin

{** Make Random Data for Graph Object}
PEGraph1.Subsets := 4;
PEGraph1.Points := 12;
For s := 0 To 3 do       {4 subsets}
    For p := 0 To 11 do  {12 points}
        PEGraph1.YData[s, p] := (p + 1) * 5 + Random(25);

{** Set SubsetLabels property array for 4 subsets **}
PEGraph1.SubsetLabels[0] := 'Texas';
PEGraph1.SubsetLabels[1] := 'Florida';
PEGraph1.SubsetLabels[2] := 'Washington';
PEGraph1.SubsetLabels[3] := 'California';

{** Set PointLabels property array for 12 points **}
PEGraph1.PointLabels[0] := 'January';
PEGraph1.PointLabels[1] := 'February';
PEGraph1.PointLabels[2] := 'March';
PEGraph1.PointLabels[3] := 'April';
PEGraph1.PointLabels[4] := 'May';
PEGraph1.PointLabels[5] := 'June';
PEGraph1.PointLabels[6] := 'July';
PEGraph1.PointLabels[7] := 'August';
PEGraph1.PointLabels[8] := 'September';
PEGraph1.PointLabels[9] := 'October';
PEGraph1.PointLabels[10] := 'November';
PEGraph1.PointLabels[11] := 'December';

{** tells object to automatically generate statistical
** comparison subsets, Object will actually have
** 6 subsets when finished reinitializing.}
PEGraph1.AutoStatSubsets[0] := 53; {PEAS_AVGAP}
PEGraph1.AutoStatSubsets[1] := 54; {PEAS_AVGPP}

{** Scroll through data one subset at a time **}
PEGraph1.ScrollingSubsets := 1;

{** Since we are scrolling subsets one at a time,
** we also can set the PEAS_AVGPP subset as
** permanent. This allows individual subsets to be
** compared to the average of all points subset.}
PEGraph1.RandomSubsetsToGraph[0] := 4;
PEGraph1.RandomSubsetsToGraph[1] := 5;

{** this is how to change subset colors **}
PEGraph1.SubsetColors[0] := clRed;
PEGraph1.SubsetColors[1] := clBlue;
PEGraph1.SubsetColors[2] := clLime;
PEGraph1.SubsetColors[3] := clFuchsia;
PEGraph1.SubsetColors[4] := clAqua;
PEGraph1.SubsetColors[5] := clGray;

{** this is how to change line types **}
PEGraph1.SubsetLineTypes[0] := 0; {PELT_THINSOLID}
PEGraph1.SubsetLineTypes[1] := 1; {PELT_DASH}
PEGraph1.SubsetLineTypes[2] := 2; {PELT_DOT}
PEGraph1.SubsetLineTypes[3] := 3; {PELT_DASHDOT}
PEGraph1.SubsetLineTypes[4] := 4; {PELT_DASHDOTDOT}
PEGraph1.SubsetLineTypes[5] := 5; {PELT_MEDIUMSOLID}

{** this is how to change point types **}
PEGraph1.SubsetPointTypes[0] := 0; {PELT_PLUS}
PEGraph1.SubsetPointTypes[1] := 1; {PELT_CROSS}
PEGraph1.SubsetPointTypes[2] := 2; {PELT_DOT}
PEGraph1.SubsetPointTypes[3] := 3; {PELT_DOTSOLID}
PEGraph1.SubsetPointTypes[4] := 4; {PELT_SQUARE}
PEGraph1.SubsetPointTypes[5] := 5; {PELT_SQUARESOLID}

{**Set various other properties **}
PEGraph1.MainTitle := 'Units Sold per Month';
PEGraph1.SubTitle := '';
PEGraph1.YAxisLabel := 'Units Sold';
PEGraph1.NoStackedData := False;
PEGraph1.FocalRect := False;
PEGraph1.FontSizeLegendCntl := 0.8;
PEGraph1.FontSizeGlobalCntl := 1.1;
PEGraph1.PlottingMethod := gBar;
PEGraph1.GridLineControl := gNoGrid;

{'** Always Refresh after giving an object new data,
'** YData, XData, SubsetLabels, PointLabels.}

PEGraph1.PEactions := gReinitAndReset;  {Better than Refresh}

end;

end.
