{ /////////////////////////////////////////////////////////////////////////////////////////////// }
{ /////////////////////////////////////////////////////////////////////////////////////////////// }
{ ProEssentials DLL Declarations///////////////////////////////////////////////////////////////// }
{ /////////////////////////////////////////////////////////////////////////////////////////////// }
{ /////////////////////////////////////////////////////////////////////////////////////////////// }
{ // Copyright (C) 1996-1997 Gigasoft, Inc.  All Rights Reserved //}

{$D-}
{$L-}
{$Q-}
{$R-}
{$S-}
{$T-}
{$V-}
{$Y-}

unit Pegrpapi;

interface

uses
  WinTypes, Messages, Graphics;

const

  PECONTROL_GRAPH           = 300;
  PECONTROL_PIE             = 302;
  PECONTROL_SGRAPH          = 304;
  PECONTROL_PGRAPH          = 308;

  PESTA_CENTER              = 0;
  PESTA_LEFT                = 1;
  PESTA_RIGHT               = 2;

  PEDO_DRIVERDEFAULT        = 0;
  PEDO_LANDSCAPE            = 1;
  PEDO_PORTRAIT             = 2;

  PEVS_COLOR                = 0;
  PEVS_MONO                 = 1;
  PEVS_MONOWITHSYMBOLS      = 2;

  PEFS_LARGE                = 0;
  PEFS_MEDIUM               = 1;
  PEFS_SMALL                = 2;

  PEVB_NONE                 = 0;
  PEVB_TOP                  = 1;
  PEVB_BOTTOM               = 2;
  PEVB_TOPANDBOTTOM         = 3;

  PEAC_AUTO                 = 0;
  PEAC_NORMAL               = 1;
  PEAC_LOG                  = 2;

  {Only used with PEnset}
  PEGPM_LINE                = 0;
  PEGPM_BAR                 = 1;
  PEGPM_STICK               = 4;
  PEGPM_POINT               = 2;
  PEGPM_AREA                = 3;
  PEGPM_AREASTACKED         = 4;
  PEGPM_AREASTACKEDPERCENT  = 5;
  PEGPM_BARSTACKED          = 6;
  PEGPM_BARSTACKEDPERCENT   = 7;
  PEGPM_POINTSPLUSBFL       = 8;
  PEGPM_POINTSPLUSBFLGRAPHED= 9;
  PEGPM_HISTOGRAM           = 10;
  PEGPM_SPECIFICPLOTMODE    = 11;
  PEGPM_BUBBLE              = 12;
  PEGPM_POINTSPLUSBFC       = 13;
  PEGPM_POINTSPLUSBFCGRAPHED= 14;
  PEGPM_POINTSPLUSSPLINE    = 15;
  PEGPM_SPLINE              = 16;
  PEGPM_POINTSPLUSLINE      = 17;
  PEGPM_HORIZONTALBAR       = 18;

  PEGLC_BOTH                = 0;
  PEGLC_YAXIS               = 1;
  PEGLC_XAXIS               = 2;
  PEGLC_NONE                = 3;

  PEAS_SUMPP                = 51;
  PEAS_MINAP                = 1;
  PEAS_MINPP                = 52;
  PEAS_MAXAP                = 2;
  PEAS_MAXPP                = 53;
  PEAS_AVGAP                = 3;
  PEAS_AVGPP                = 54;
  PEAS_P1SDAP               = 4;
  PEAS_P1SDPP               = 55;
  PEAS_P2SDAP               = 5;
  PEAS_P2SDPP               = 56;
  PEAS_P3SDAP               = 6;
  PEAS_P3SDPP               = 57;
  PEAS_M1SDAP               = 7;
  PEAS_M1SDPP               = 58;
  PEAS_M2SDAP               = 8;
  PEAS_M2SDPP               = 59;
  PEAS_M3SDAP               = 9;
  PEAS_M3SDPP               = 60;
  PEAS_PARETO_ASC           = 90;
  PEAS_PARETO_DEC           = 91;

  PEPTGI_FIRSTPOINTS        = 0;
  PEPTGI_LASTPOINTS         = 1;  

  PEPTGV_SEQUENTIAL         = 0;
  PEPTGV_RANDOM             = 1;

  PEGPT_GRAPH               = 0;
  PEGPT_TABLE               = 1;
  PEGPT_BOTH                = 2;

  PETW_GRAPHED              = 0;
  PETW_ALLSUBSETS           = 1;

  PEDLT_PERCENTAGE          = 0;
  PEDLT_VALUE               = 1;

  PEMSC_NONE                = 0;
  PEMSC_MIN                 = 1;
  PEMSC_MAX                 = 2;
  PEMSC_MINMAX              = 3;

  IDEXPORTBUTTON            = 1015;
  IDMAXIMIZEBUTTON          = 1016;
  IDORIGINALBUTTON          = 1109;   

  PEHS_NONE                 = 0;
  PEHS_SUBSET               = 1;
  PEHS_POINT                = 2;
  PEHS_GRAPH                = 3;
  PEHS_TABLE                = 4;
  PEHS_DATAPOINT            = 5;
  PEHS_ANNOTATION           = 6;
  PEHS_XAXISANNOTATION      = 7;
  PEHS_YAXISANNOTATION      = 8;
  PEHS_HORZLINEANNOTATION   = 9;
  PEHS_VERTLINEANNOTATION   = 10;

  PESPM_NONE                = 0;
  PESPM_HIGHLOWBAR          = 1;
  PESPM_HIGHLOWLINE         = 2;
  PESPM_HIGHLOWCLOSE        = 3;
  PESPM_OPENHIGHLOWCLOSE    = 4;
  PESPM_BOXPLOT             = 5;

{ HORIZONTAL LINE ANNOTATIONS CAN BE WITH RESPECT TO RIGHT Y AXIS COORDINATES
  BY ADDING 1000 TO THE FOLLOWING CONSTANTS }
  PELT_THINSOLID            = 0;
  PELT_DASH                 = 1;
  PELT_DOT                  = 2;
  PELT_DASHDOT              = 3;
  PELT_DASHDOTDOT           = 4;
  PELT_MEDIUMSOLID          = 5;
  PELT_THICKSOLID           = 6;
  PELAT_GRIDTICK            = 7;
  PELAT_GRIDLINE            = 8;
  PELT_MEDIUMTHINSOLID	    = 9;
  PELT_MEDIUMTHICKSOLID     = 10;
  PELT_EXTRATHICKSOLID      = 11;

  PEPS_SMALL                = 0;
  PEPS_MEDIUM               = 1;
  PEPS_LARGE                = 2;
  PEPS_MICRO                = 3;

  PEPT_PLUS                 = 0;
  PEPT_CROSS                = 1;
  PEPT_DOT                  = 2;
  PEPT_DOTSOLID             = 3;
  PEPT_SQUARE               = 4;
  PEPT_SQUARESOLID          = 5;
  PEPT_DIAMOND              = 6;
  PEPT_DIAMONDSOLID         = 7;
  PEPT_UPTRIANGLE           = 8;
  PEPT_UPTRIANGLESOLID      = 9;
  PEPT_DOWNTRIANGLE         = 10;
  PEPT_DOWNTRIANGLESOLID    = 11;

  PEADL_NONE                = 0;
  PEADL_DATAVALUES          = 1;
  PEADL_POINTLABELS         = 2;
  PEADL_DATAPOINTLABELS     = 3;

  PEAZ_NONE                 = 0;
  PEAZ_HORIZONTAL           = 1;
  PEAZ_VERTICAL             = 2;
  PEAZ_HORZANDVERT          = 3;

  PEBFD_2ND                 = 0;
  PEBFD_3RD                 = 1;
  PEBFD_4TH                 = 2;

  PEBS_SMALL                = 0;
  PEBS_MEDIUM               = 1;
  PEBS_LARGE                = 2;

  PECG_COARSE               = 0;
  PECG_MEDIUM               = 1;
  PECG_FINE                 = 2;

  PEAE_NONE                 = 0;
  PEAE_ALLSUBSETS           = 1;
  PEAE_INDSUBSETS           = 2;

  PECM_NOCURSOR             = 0;
  PECM_POINT                = 1;
  PECM_DATACROSS            = 2;
  PECM_DATASQUARE           = 3;
  PECM_POINTPLUSHORZ        = 4;
  PECM_CROSSHAIR            = 5;

{ GRAPH ANNOTATIONS CAN BE WITH RESPECT TO RIGHT Y AXIS COORDINATES
  BY ADDING 1000 TO THE FOLLOWING CONSTANTS }
  PEGAT_NOSYMBOL             = 0;
  PEGAT_PLUS                 = 1;
  PEGAT_CROSS                = 2;
  PEGAT_DOT                  = 3;
  PEGAT_DOTSOLID             = 4;
  PEGAT_SQUARE               = 5;
  PEGAT_SQUARESOLID          = 6;
  PEGAT_DIAMOND              = 7;
  PEGAT_DIAMONDSOLID         = 8;
  PEGAT_UPTRIANGLE           = 9;
  PEGAT_UPTRIANGLESOLID      = 10;
  PEGAT_DOWNTRIANGLE         = 11;
  PEGAT_DOWNTRIANGLESOLID    = 12;
  PEGAT_SMALLPLUS            = 13;
  PEGAT_SMALLCROSS           = 14;
  PEGAT_SMALLDOT             = 15;
  PEGAT_SMALLDOTSOLID        = 16;
  PEGAT_SMALLSQUARE          = 17;
  PEGAT_SMALLSQUARESOLID     = 18;
  PEGAT_SMALLDIAMOND         = 19;
  PEGAT_SMALLDIAMONDSOLID    = 20;
  PEGAT_SMALLUPTRIANGLE      = 21;
  PEGAT_SMALLUPTRIANGLESOLID = 22;
  PEGAT_SMALLDOWNTRIANGLE    = 23;
  PEGAT_SMALLDOWNTRIANGLESOLID=24;
  PEGAT_LARGEPLUS            = 25;
  PEGAT_LARGECROSS           = 26;
  PEGAT_LARGEDOT             = 27;
  PEGAT_LARGEDOTSOLID        = 28;
  PEGAT_LARGESQUARE          = 29;
  PEGAT_LARGESQUARESOLID     = 30;
  PEGAT_LARGEDIAMOND         = 31;
  PEGAT_LARGEDIAMONDSOLID    = 32;
  PEGAT_LARGEUPTRIANGLE      = 33;
  PEGAT_LARGEUPTRIANGLESOLID = 34;
  PEGAT_LARGEDOWNTRIANGLE    = 35;
  PEGAT_LARGEDOWNTRIANGLESOLID=36;

  PEGAT_POINTER               =37;

  PEGAT_THINSOLIDLINE        = 38;
  PEGAT_DASHLINE             = 39;
  PEGAT_DOTLINE              = 40;
  PEGAT_DASHDOTLINE          = 41;
  PEGAT_DASHDOTDOTLINE       = 42;
  PEGAT_MEDIUMSOLIDLINE      = 43;
  PEGAT_THICKSOLIDLINE       = 44;
  PEGAT_LINECONTINUE         = 45;

  PEGAT_TOPLEFT              = 46;
  PEGAT_BOTTOMRIGHT          = 47;

  PEGAT_RECT_THIN            = 48;
  PEGAT_RECT_DASH            = 49;
  PEGAT_RECT_DOT             = 50;
  PEGAT_RECT_DASHDOT         = 51;
  PEGAT_RECT_DASHDOTDOT      = 52;
  PEGAT_RECT_MEDIUM          = 53;
  PEGAT_RECT_THICK           = 54;
  PEGAT_RECT_FILL            = 55;

  PEGAT_ROUNDRECT_THIN       = 56;
  PEGAT_ROUNDRECT_DASH       = 57;
  PEGAT_ROUNDRECT_DOT        = 58;
  PEGAT_ROUNDRECT_DASHDOT    = 59;
  PEGAT_ROUNDRECT_DASHDOTDOT = 60;
  PEGAT_ROUNDRECT_MEDIUM     = 61;
  PEGAT_ROUNDRECT_THICK      = 62;
  PEGAT_ROUNDRECT_FILL       = 63;

  PEGAT_ELLIPSE_THIN         = 64;
  PEGAT_ELLIPSE_DASH         = 65;
  PEGAT_ELLIPSE_DOT          = 66;
  PEGAT_ELLIPSE_DASHDOT      = 67;
  PEGAT_ELLIPSE_DASHDOTDOT   = 68;
  PEGAT_ELLIPSE_MEDIUM       = 69; 
  PEGAT_ELLIPSE_THICK        = 70;
  PEGAT_ELLIPSE_FILL         = 71;

  PEDTM_NONE                 = 0;
  PEDTM_VB                   = 1;
  PEDTM_DELPHI               = 2;

  PESA_ALL                   = 0;
  PESA_AXISLABELS            = 1;
  PESA_GRIDNUMBERS           = 2;
  PESA_NONE                  = 3;
  PESA_LABELONLY             = 4;
  PESA_EMPTY                 = 5;

  PEGS_THIN                  = 0;
  PEGS_THICK                 = 1;
  PEGS_DOT                   = 2;
  PEGS_DASH                  = 3;

  PEFVP_AUTO                 = 0;
  PEFVP_VERT                 = 1;
  PEFVP_HORZ                 = 2;
                           
  PEMAS_NONE                 = 0;
  PEMAS_THIN                 = 1;
  PEMAS_MEDIUM               = 2;
  PEMAS_THICK                = 3;
  PEMAS_THICKPLUSTICK        = 4;

  PEWM_INVALIDATERECT        = WM_USER + 2938;
  PEWN_CHANGINGPARMS         = WM_USER + 2937;
  PEWN_ZOOMIN                = WM_USER + 2942;
  PEWN_ZOOMOUT               = WM_USER + 2943;
  PEWN_CURSORMOVED           = WM_USER + 2929;
  PEWN_PRECURSORMOVE         = WM_USER + 2949;

  PEP_nOBJECTTYPE            = 2100;
  PEP_szMAINTITLE            = 2105;
  PEP_szSUBTITLE             = 2110;
  PEP_nSUBSETS               = 2115;
  PEP_nPOINTS                = 2120;
  PEP_szaSUBSETLABELS        = 2125;
  PEP_szaPOINTLABELS         = 2130;
  PEP_faXDATA                = 2135;
  PEP_faYDATA                = 2140;
  PEP_bMONOWITHSYMBOLS       = 2145;
  PEP_nDEFORIENTATION        = 2150;
  PEP_bPREPAREIMAGES         = 2155;
  PEP_b3DDIALOGS             = 2160;
  PEP_bALLOWCUSTOMIZATION    = 2165;
  PEP_bALLOWEXPORTING        = 2170;
  PEP_bALLOWMAXIMIZATION     = 2175;
  PEP_bALLOWPOPUP            = 2180;
  PEP_bALLOWUSERINTERFACE    = 2185;
  PEP_dwaSUBSETCOLORS        = 2190;
  PEP_dwaSUBSETSHADES        = 2195;
  PEP_nPAGEWIDTH             = 2200;
  PEP_nPAGEHEIGHT            = 2205;
  PEP_rectLOGICALLOC         = 2210;
  PEP_bDIRTY                 = 2215;
  PEP_bDIALOGSHOWN           = 2220;
  PEP_bCUSTOM                = 2225;
  PEP_nVIEWINGSTYLE          = 2230;
  PEP_nCVIEWINGSTYLE         = 2235;
  PEP_bDATASHADOWS           = 2240;
  PEP_bCDATASHADOWS          = 2245;
  PEP_dwMONODESKCOLOR        = 2250;
  PEP_dwMONOTEXTCOLOR        = 2255;
  PEP_dwMONOSHADOWCOLOR      = 2260;
  PEP_dwMONOGRAPHFORECOLOR   = 2265;
  PEP_dwMONOGRAPHBACKCOLOR   = 2270;
  PEP_dwMONOTABLEFORECOLOR   = 2275;
  PEP_dwMONOTABLEBACKCOLOR   = 2280;
  PEP_dwCMONODESKCOLOR       = 2285;
  PEP_dwCMONOTEXTCOLOR       = 2290;
  PEP_dwCMONOSHADOWCOLOR     = 2295;
  PEP_dwCMONOGRAPHFORECOLOR  = 2300;
  PEP_dwCMONOGRAPHBACKCOLOR  = 2305;
  PEP_dwCMONOTABLEFORECOLOR  = 2310;
  PEP_dwCMONOTABLEBACKCOLOR  = 2315;
  PEP_dwDESKCOLOR            = 2320;
  PEP_dwTEXTCOLOR            = 2325;
  PEP_dwSHADOWCOLOR          = 2330;
  PEP_dwGRAPHFORECOLOR       = 2335;
  PEP_dwGRAPHBACKCOLOR       = 2340;
  PEP_dwTABLEFORECOLOR       = 2345;
  PEP_dwTABLEBACKCOLOR       = 2350;
  PEP_dwCDESKCOLOR           = 2355;
  PEP_dwCTEXTCOLOR           = 2360;
  PEP_dwCSHADOWCOLOR         = 2365;
  PEP_dwCGRAPHFORECOLOR      = 2370;
  PEP_dwCGRAPHBACKCOLOR      = 2375;
  PEP_dwCTABLEFORECOLOR      = 2380;
  PEP_dwCTABLEBACKCOLOR      = 2385;
  PEP_dwWDESKCOLOR           = 2390;
  PEP_dwWTEXTCOLOR           = 2395;
  PEP_dwWSHADOWCOLOR         = 2400;
  PEP_dwWGRAPHFORECOLOR      = 2405;
  PEP_dwWGRAPHBACKCOLOR      = 2410;
  PEP_dwWTABLEFORECOLOR      = 2415;
  PEP_dwWTABLEBACKCOLOR      = 2420;
  PEP_nDATAPRECISION         = 2425;
  PEP_nCDATAPRECISION        = 2430;
  PEP_nFONTSIZE              = 2435;
  PEP_nCFONTSIZE             = 2440;
  PEP_szMAINTITLEFONT        = 2445;
  PEP_bMAINTITLEBOLD         = 2450;
  PEP_bMAINTITLEITALIC       = 2455;
  PEP_bMAINTITLEUNDERLINE    = 2460;
  PEP_szCMAINTITLEFONT       = 2465;
  PEP_bCMAINTITLEBOLD        = 2470;
  PEP_bCMAINTITLEITALIC      = 2475;
  PEP_bCMAINTITLEUNDERLINE   = 2480;   
  PEP_szSUBTITLEFONT         = 2485;
  PEP_bSUBTITLEBOLD          = 2490;
  PEP_bSUBTITLEITALIC        = 2495;
  PEP_bSUBTITLEUNDERLINE     = 2500;
  PEP_szCSUBTITLEFONT        = 2505;
  PEP_bCSUBTITLEBOLD         = 2510;
  PEP_bCSUBTITLEITALIC       = 2515;
  PEP_bCSUBTITLEUNDERLINE    = 2520;
  PEP_szLABELFONT            = 2525;
  PEP_bLABELBOLD             = 2530;
  PEP_bLABELITALIC           = 2535;
  PEP_bLABELUNDERLINE        = 2540;
  PEP_szCLABELFONT           = 2545;
  PEP_bCLABELBOLD            = 2550;
  PEP_bCLABELITALIC          = 2555;
  PEP_bCLABELUNDERLINE       = 2560;
  PEP_szTABLEFONT            = 2565;
  PEP_szCTABLEFONT           = 2570;
  PEP_bCACHEBMP              = 2574;
  PEP_hMEMBITMAP             = 2575;
  PEP_hMEMDC                 = 2580;
  PEP_bALLOWSUBSETHOTSPOTS   = 2600;
  PEP_bALLOWPOINTHOTSPOTS    = 2605;
  PEP_structHOTSPOTDATA      = 2610;
  PEP_bAUTOIMAGERESET        = 2615;
  PEP_bALLOWTITLESDIALOG     = 2616;
  PEP_nCURSORMODE            = 2617;
  PEP_nCURSORSUBSET          = 2618;
  PEP_nCURSORPOINT           = 2619;
  PEP_nCURSORPROMPTSTYLE     = 2620;
  PEP_bCURSORPROMPTTRACKING  = 2621;
  PEP_bMOUSECURSORCONTROL    = 2622;
  PEP_bALLOWANNOTATIONCONTROL= 2623;
  PEP_naSUBSETSTOLEGEND      = 2624;
  PEP_naLEGENDANNOTATIONTYPE = 2625;
  PEP_szaLEGENDANNOTATIONTEXT= 2626;
  PEP_dwaLEGENDANNOTATIONCOLOR=2627;
  PEP_nVERTSCROLLPOS         = 2628;
  PEP_bALLOWDEBUGOUTPUT      = 2629;
  PEP_szaMULTISUBTITLES      = 2630;
  PEP_szaMULTIBOTTOMTITLES   = 2631;
  PEP_bFOCALRECT             = 2632;
  PEP_fFONTSIZEGLOBALCNTL    = 2634;
  PEP_fFONTSIZETITLECNTL     = 2635;
  PEP_bSUBSETBYPOINT         = 2636;
  PEP_ptLASTMOUSEMOVE        = 2637;
  PEP_bALLOWOLEEXPORT        = 2638;
  PEP_faZDATA                = 2900;
  PEP_bINVALID               = 2905;
  PEP_bOBJECTINSERVER        = 2910;
  PEP_hwndPARENTALCONTROL    = 2915;
  PEP_bPAINTING              = 2916;
  PEP_hARROWCURSOR           = 2917;
  PEP_hZOOMCURSOR            = 2918;
  PEP_hHANDCURSOR            = 2919;
  PEP_hNODROPCURSOR          = 2920;
  PEP_szXAXISLABEL           = 3000;
  PEP_szYAXISLABEL           = 3005;
  PEP_nVBOUNDARYTYPES        = 3010;
  PEP_fUPPERBOUNDVALUE       = 3015;
  PEP_fLOWERBOUNDVALUE       = 3020;
  PEP_szUPPERBOUNDTEXT       = 3025;
  PEP_szLOWERBOUNDTEXT       = 3030;
  PEP_nINITIALSCALEFORYDATA  = 3035;
  PEP_nSCALEFORYDATA         = 3040;
  PEP_nYAXISSCALECONTROL     = 3045;
  PEP_nMANUALSCALECONTROLY   = 3050;
  PEP_fMANUALMINY            = 3055;
  PEP_fMANUALMAXY            = 3060;
  PEP_bNOSCROLLINGSUBSETCONTROL=3065;
  PEP_nSCROLLINGSUBSETS      = 3070;
  PEP_nCSCROLLINGSUBSETS     = 3075;
  PEP_naRANDOMSUBSETSTOGRAPH = 3080;
  PEP_naCRANDOMSUBSETSTOGRAPH=3085;
  PEP_nPLOTTINGMETHOD        = 3090;
  PEP_nCPLOTTINGMETHOD       = 3095;
  PEP_nGRIDLINECONTROL       = 3100;
  PEP_nCGRIDLINECONTROL      = 3105;
  PEP_bGRIDINFRONT           = 3110;
  PEP_bCGRIDINFRONT          = 3115;
  PEP_bTREATCOMPSASNORMAL    = 3120;
  PEP_bCTREATCOMPSASNORMAL   = 3125;
  PEP_nCOMPARISONSUBSETS     = 3130;
  PEP_bALLOWCOORDPROMPTING   = 3200;
  PEP_bALLOWGRAPHHOTSPOTS    = 3205;
  PEP_bALLOWDATAHOTSPOTS     = 3210;
  PEP_bMARKDATAPOINTS        = 3215;
  PEP_bCMARKDATAPOINTS       = 3220;
  PEP_nRYAXISCOMPARISONSUBSETS=3225;
  PEP_nRYAXISSCALECONTROL    = 3230;
  PEP_nINITIALSCALEFORRYDATA = 3235;
  PEP_nMANUALSCALECONTROLRY  = 3240;
  PEP_fMANUALMINRY           = 3245;
  PEP_fMANUALMAXRY           = 3250;
  PEP_szRYAXISLABEL          = 3255;
  PEP_nSCALEFORRYDATA        = 3256;
  PEP_bALLOWPLOTCUSTOMIZATION= 3260;
  PEP_bNEGATIVEFROMXAXIS     = 3261;
  PEP_bMANUALYAXISTICKNLINE  = 3262;
  PEP_fMANUALYAXISTICK       = 3263;
  PEP_fMANUALYAXISLINE       = 3264;
  PEP_bMANUALRYAXISTICKNLINE = 3265;
  PEP_fMANUALRYAXISTICK      = 3266;
  PEP_fMANUALRYAXISLINE      = 3267;
  PEP_fNULLDATAVALUE         = 3268;
  PEP_nPOINTSIZE             = 3269;
  PEP_naSUBSETPOINTTYPES     = 3270;
  PEP_naSUBSETLINETYPES      = 3271;
  PEP_bALLOWBESTFITCURVE     = 3272;
  PEP_nBESTFITDEGREE         = 3273;
  PEP_bALLOWSPLINE           = 3274;
  PEP_nCURVEGRANULARITY      = 3275;
  PEP_faAPPENDYDATA          = 3276;
  PEP_szaAPPENDPOINTLABELDATA= 3277;
  PEP_bALLOWLINE             = 3279;
  PEP_bALLOWPOINT            = 3280;
  PEP_bALLOWBESTFITLINE      = 3281;
  PEP_nALLOWZOOMING          = 3282;
  PEP_bZOOMMODE              = 3283;
  PEP_fZOOMMINY              = 3284;
  PEP_fZOOMMAXY              = 3285;
  PEP_bFORCERIGHTYAXIS       = 3286;
  PEP_bALLOWPOINTSPLUSLINE   = 3287;
  PEP_bALLOWPOINTSPLUSSPLINE = 3288;
  PEP_nSYMBOLFREQUENCY       = 3289;
  PEP_bSHOWANNOTATIONS       = 3290;
  PEP_bCSHOWANNOTATIONS      = 3202;
  PEP_dwANNOTATIONCOLOR      = 3203;
  PEP_dwCANNOTATIONCOLOR     = 3204;
  PEP_faGRAPHANNOTATIONX     = 3291;
  PEP_faGRAPHANNOTATIONY     = 3292;
  PEP_szaGRAPHANNOTATIONTEXT = 3293;
  PEP_nMAXAXISANNOTATIONCLUSTER=3296;
  PEP_faXAXISANNOTATION      = 3297;
  PEP_szaXAXISANNOTATIONTEXT = 3298;
  PEP_faYAXISANNOTATION      = 3299;
  PEP_szaYAXISANNOTATIONTEXT = 3201;
  PEP_bANNOTATIONSINFRONT    = 3208;
  PEP_nCURSORPAGEAMOUNT      = 3211;
  PEP_fLINEGAPTHRESHOLD      = 3212;
  PEP_faHORZLINEANNOTATION   = 3213;
  PEP_szaHORZLINEANNOTATIONTEXT=3214;
  PEP_naHORZLINEANNOTATIONTYPE =3216;
  PEP_dwaHORZLINEANNOTATIONCOLOR=3217;
  PEP_faVERTLINEANNOTATION   = 3218;
  PEP_szaVERTLINEANNOTATIONTEXT=3219;
  PEP_naVERTLINEANNOTATIONTYPE=3221;
  PEP_dwaVERTLINEANNOTATIONCOLOR=3222;
  PEP_bSHOWGRAPHANNOTATIONS   = 3223;
  PEP_bSHOWXAXISANNOTATIONS   = 3224;
  PEP_bSHOWYAXISANNOTATIONS   = 3226;
  PEP_bSHOWHORZLINEANNOTATIONS= 3227;
  PEP_bSHOWVERTLINEANNOTATIONS= 3228;
  PEP_bALLOWGRAPHANNOTHOTSPOTS= 3229;
  PEP_bALLOWXAXISANNOTHOTSPOTS= 3231;
  PEP_bALLOWYAXISANNOTHOTSPOTS= 3232;
  PEP_bALLOWHORZLINEANNOTHOTSPOTS= 3233;
  PEP_bALLOWVERTLINEANNOTHOTSPOTS= 3234;
  PEP_dwaGRAPHANNOTATIONCOLOR =3236;
  PEP_dwaXAXISANNOTATIONCOLOR =3237;
  PEP_dwaYAXISANNOTATIONCOLOR =3238;
  PEP_nGRAPHANNOTATIONTEXTSIZE =3242;
  PEP_nAXESANNOTATIONTEXTSIZE =3243;
  PEP_nLINEANNOTATIONTEXTSIZE =3244;
  PEP_naGRAPHANNOTATIONTYPE  = 3246;
  PEP_nZOOMINTERFACEONLY     = 3247;
  PEP_fZOOMMINX              = 3248;
  PEP_fZOOMMAXX              = 3249;
  PEP_nDATAHOTSPOTLIMIT      = 3251;
  PEP_nHOURGLASSTHRESHOLD    = 3252;
  PEP_nHORZSCROLLPOS         = 3253;
  PEP_bALLOWAREA             = 3254;
  PEP_bVERTORIENT90DEGREES   = 3257;
  PEP_dwaPOINTCOLORS         = 3258;
  PEP_naMULTIAXESSUBSETS     = 3001;
  PEP_naGRAPHANNOTATIONAXIS  = 3002;
  PEP_naHORZLINEANNOTATIONAXIS= 3003;
  PEP_naYAXISANNOTATIONAXIS  = 3004;
  PEP_nWORKINGAXIS           = 3006;
  PEP_faMULTIAXESPROPORTIONS = 3007;
  PEP_naLEGENDANNOTATIONAXIS = 3008;
  PEP_bLOGSCALEEXPLABELS     = 3009;
  PEP_nPLOTTINGMETHODII      = 3011;
  PEP_nCPLOTTINGMETHODII     = 3012;
  PEP_faXDATAII              = 3013;
  PEP_faYDATAII              = 3014;
  PEP_bUSINGXDATAII          = 3016;
  PEP_bUSINGYDATAII          = 3017;
  PEP_nDATETIMEMODE          = 3018;
  PEP_fBARWIDTH              = 3019;
  PEP_nSPECIFICPLOTMODE      = 3021;
  PEP_bALLOWBAR              = 3022;
  PEP_structGRAPHLOC         = 3023;
  PEP_faAPPENDYDATAII        = 3024;
  PEP_bYAXISONRIGHT          = 3026;
  PEP_nSHOWYAXIS             = 3027;
  PEP_nSHOWRYAXIS            = 3028;
  PEP_nSHOWXAXIS             = 3029;
  PEP_nGRIDSTYLE             = 3032;
  PEP_bINVERTEDYAXIS         = 3033;
  PEP_bINVERTEDRYAXIS        = 3034;
  PEP_dwYAXISCOLOR           = 3036;
  PEP_dwRYAXISCOLOR          = 3037;
  PEP_dwXAXISCOLOR           = 3038;                
  PEP_fFONTSIZEAXISCNTL      = 3041;
  PEP_fFONTSIZELEGENDCNTL    = 3042;
  PEP_bYAXISLONGTICKS        = 3043;
  PEP_bRYAXISLONGTICKS       = 3044;
  PEP_nMULTIAXESSEPARATORS   = 3046;
  PEP_nZOOMMINAXIS           = 3047;
  PEP_nZOOMMAXAXIS           = 3048;
  PEP_rectGRAPH              = 3049;
  PEP_rectAXIS               = 3051;
  PEP_szLEFTMARGIN           = 3052;
  PEP_szTOPMARGIN            = 3053;
  PEP_szRIGHTMARGIN          = 3054;
  PEP_szBOTTOMMARGIN         = 3056;
  PEP_bAUTOSCALEDATA         = 3057;
  PEP_faBESTFITCOEFFS        = 3058;
  PEP_naOVERLAPMULTIAXES     = 3059;
  PEP_bNOHIDDENLINESINAREA   = 3061;
  PEP_bSPECIFICPLOTMODECOLOR = 3062;
  PEP_nAUTOMINMAXPADDING     = 3063;
  PEP_naAUTOSTATSUBSETS      = 3300;
  PEP_bNOSTACKEDDATA         = 3305;
  PEP_nPOINTSTOGRAPHINIT     = 3310;
  PEP_nPOINTSTOGRAPHVERSION  = 3315;
  PEP_nCPOINTSTOGRAPHVERSION = 3320;
  PEP_nPOINTSTOGRAPH         = 3325;
  PEP_nCPOINTSTOGRAPH        = 3330;
  PEP_naRANDOMPOINTSTOGRAPH  = 3335;
  PEP_naCRANDOMPOINTSTOGRAPH = 3340;
  PEP_nFORCEVERTICALPOINTS   = 3345;
  PEP_nCFORCEVERTICALPOINTS  = 3350;
  PEP_nGRAPHPLUSTABLE        = 3355;
  PEP_nCGRAPHPLUSTABLE       = 3360;
  PEP_nTABLEWHAT             = 3365;
  PEP_nCTABLEWHAT            = 3370;
  PEP_bALLOWTABLEHOTSPOTS    = 3400;
  PEP_bALLOWHISTOGRAM        = 3401;
  PEP_naALTFREQUENCIES       = 3403;
  PEP_nTARGETPOINTSTOTABLE   = 3404;
  PEP_nALTFREQTHRESHOLD      = 3405;
  PEP_fMANUALSTACKEDMAXY     = 3406;
  PEP_nMAXPOINTSTOGRAPH      = 3407;
  PEP_bNORANDOMPOINTSTOGRAPH = 3408;
  PEP_szMANUALMAXPOINTLABEL  = 3409;
  PEP_szMANUALMAXDATASTRING  = 3410;
  PEP_bALLOWBESTFITLINEII    = 3413;
  PEP_bALLOWBESTFITCURVEII   = 3414;
  PEP_bAPPENDTOEND           = 3415;
  PEP_bALLOWHORIZONTALBAR    = 3416;
  PEP_nFIRSTPTLABELOFFSET    = 3417;
  PEP_fMANUALSTACKEDMINY     = 3418;
  PEP_nINITIALSCALEFORXDATA  = 3600;
  PEP_nSCALEFORXDATA         = 3605;
  PEP_nXAXISSCALECONTROL     = 3610;
  PEP_nMANUALSCALECONTROLX   = 3615;
  PEP_fMANUALMINX            = 3620;
  PEP_fMANUALMAXX            = 3625;
  PEP_bGRAPHDATALABELS       = 3630;
  PEP_bCGRAPHDATALABELS      = 3635;
  PEP_bALLOWBUBBLE           = 3640;
  PEP_nBUBBLESIZE            = 3641;
  PEP_nALLOWDATALABELS       = 3642;
  PEP_szaDATAPOINTLABELS     = 3643;
  PEP_bMANUALXAXISTICKNLINE  = 3644;
  PEP_fMANUALXAXISTICK       = 3645;
  PEP_fMANUALXAXISLINE       = 3646;
  PEP_bALLOWSTICK            = 3648;
  PEP_bSCROLLINGHORZZOOM     = 3652;
  PEP_bNORANDOMPOINTSTOEXPORT= 3653;
  PEP_bXAXISVERTNUMBERING    = 3654;
  PEP_bENGSTATIONFORMAT      = 3655;
  PEP_fNULLDATAVALUEX        = 3656;
  PEP_bASSUMESEQDATA         = 3657;
  PEP_faAPPENDXDATA          = 3658;  
  PEP_faAPPENDXDATAII        = 3659;
  PEP_nTXAXISCOMPARISONSUBSETS= 3661;
  PEP_nTXAXISSCALECONTROL    = 3662;
  PEP_nINITIALSCALEFORTXDATA = 3663;
  PEP_nMANUALSCALECONTROLTX  = 3664;
  PEP_fMANUALMINTX           = 3665;
  PEP_fMANUALMAXTX           = 3666;
  PEP_szTXAXISLABEL          = 3667;
  PEP_nSCALEFORTXDATA        = 3668;
  PEP_bMANUALTXAXISTICKNLINE = 3669;
  PEP_fMANUALTXAXISTICK      = 3670;
  PEP_fMANUALTXAXISLINE      = 3671;
  PEP_bFORCETOPXAXIS         = 3672;
  PEP_bXAXISONTOP            = 3673;
  PEP_bINVERTEDXAXIS         = 3674; 
  PEP_bINVERTEDTXAXIS        = 3675;
  PEP_nSHOWTXAXIS            = 3676;
  PEP_dwTXAXISCOLOR          = 3677;                
  PEP_bXAXISLONGTICKS        = 3078;
  PEP_bTXAXISLONGTICKS       = 3079;
  PEP_bSMITHCHART            = 3800;
  PEP_bSMARTGRID             = 3801;
  PEP_nSHOWPOLARGRID         = 3802;
  PEP_nZERODEGREEOFFSET      = 3803;
  PEP_nGROUPINGPERCENT       = 3900;
  PEP_nCGROUPINGPERCENT      = 3905;
  PEP_nDATALABELTYPE         = 3910;
  PEP_nCDATALABELTYPE        = 3915;
  PEP_nAUTOEXPLODE           = 3920;

type
   THotSpotData = record
       rectHotSpot: TRect;
       nHotSpotType: Integer;
       w1: Integer;
       w2: Integer;
   end;

{$IFDEF WIN32}
   TGraphLoc = record
       nAxis: Longint;
       nRes: Longint;
       fXval: Double;
       fYval: Double;
   end;
{$ELSE}
   TGraphLoc = record
       nAxis: Integer;
       fXval: Double;
       fYval: Double;
   end;
{$ENDIF}

   TTM = record
       nMonth: Integer;
       nDay : Integer;
       nYear: Integer;
       nHour: Integer;
       nMinute: Integer;
       nSecond: Integer;
       nWeekDay: Integer;
       nYearDay: Integer;
   end;

{$IFDEF WIN32}
Function PEcreate(nType:Integer; dwStyle:Longint; fData:pointer; ParentWnd:HWND; nID:Integer):Integer;
   Far; stdcall; external 'PEGRAP32.DLL';
Function PEdestroy(obj:hWnd):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEvset(obj:hWnd;nProperty: Integer; fData:pointer; count:integer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEvget(obj:hWnd;nProperty: Integer; fData:pointer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEvsetcell(obj:hWnd; nProp:Integer; nIndex: Integer; fData:pointer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEvgetcell(obj:hWnd; nProp:Integer; nIndex: Integer; fData:pointer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEvsetcellEx(obj:hWnd;nProp:Integer;nSub:Integer;nPt:Integer;fData:pointer):Integer;
   Far; stdcall; external 'PEGRAP32.DLL';
Function PEvgetcellEx(obj:hWnd;nProp:Integer;nSub:Integer;nPt:Integer;fData:pointer):Integer;
   Far; stdcall; external 'PEGRAP32.DLL';
Function PEnset(obj:hWnd; nProperty:Integer; nData:Integer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEnget(obj:hWnd; nProperty:Integer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEszset(obj:hWnd; nProperty:Integer; szData: PChar):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEszget(obj:hWnd; nProperty:Integer; szData: PChar):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEgetmeta(obj:hWnd):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEallocateindmemory(obj:hWnd):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEreinitialize(obj:hWnd):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEresetimage(obj:hWnd; nWidth: Integer; nHeight: Integer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEdrawcursor(obj:hWnd; hTargetDC: hDC; nAction: Integer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEcopybitmaptoclipboard(obj:hWnd; lpPoint: Pointer): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEcopybitmaptofile(obj:hWnd; lpPoint: Pointer; lpFileName: PChar): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEcopymetatoclipboard(obj:hWnd; lpPoint: Pointer): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEcopymetatofile(obj:hWnd; lpPoint: Pointer; lpFileName: PChar): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEcopyoletoclipboard(obj:hWnd; lpPoint: Pointer): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchcolordialog(obj: hWnd): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchcustomize(obj:hWnd): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchexport(obj:hWnd): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchfontdialog(obj:hWnd): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchmaximize(obj:hWnd): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchpopupmenu(obj:hWnd; lp: Pointer): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchprintdialog(obj:hWnd; bFullPage: Integer; lpPoint: Pointer): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PElaunchtextexport(obj:hWnd; bToFile: Integer; lpFileName: PChar): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEreinitializecustoms(obj: hWnd): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEprintgraph(obj:hWnd; hDC:Integer; nWidth:Integer; nHeight:Integer; nOrient:Integer): Integer;
   Far; stdcall; external 'PEGRAP32.DLL';
Function PEconvpixeltograph(obj:hWnd; nAxis:Pointer; nX:Pointer; nY:Pointer; fX:Pointer; fY:Pointer;
   bRight: Integer; bTop: Integer; bViceVersa: Integer): Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEreset(obj:hWnd):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEstore(obj:hWnd; lphGlobal:pointer; lpcount:pointer):Integer; Far; stdcall; external 'PEGRAP32.DLL';
Function PEload(obj:hWnd; lphGlobal:pointer):Integer; Far; stdcall; external 'PEGRAP32.DLL';

implementation
{$ELSE}
Function PEcreate(nType:Integer; dwStyle:Longint; fData:pointer; ParentWnd:HWND; nID:Integer):Integer; Far;
Function PEdestroy(obj:hWnd):Integer; Far;
Function PEvset(obj:hWnd;nProperty: Integer; fData:pointer; count:integer):Integer; Far;
Function PEvget(obj:hWnd;nProperty: Integer; fData:pointer):Integer; Far;
Function PEvsetcell(obj:hWnd; nProperty:Integer; nIndex: Integer; fData:pointer):Integer; Far;
Function PEvgetcell(obj:hWnd; nProperty:Integer; nIndex: Integer; fData:pointer):Integer; Far;
Function PEvsetcellEx(obj:hWnd;nProperty:Integer;nSub:Integer;nPt:Integer;fData:pointer):Integer; Far;
Function PEvgetcellEx(obj:hWnd;nProperty:Integer;nSub:Integer;nPt:Integer;fData:pointer):Integer; Far;
Function PEnset(obj:hWnd; nProperty:Integer; nData:Integer):Integer; Far;
Function PEnget(obj:hWnd; nProperty:Integer):Integer; Far;
Function PElset(obj:hWnd; nProperty:Integer; dwData:TColor):Integer; Far;
Function PElget(obj:hWnd; nProperty:Integer):TColor; Far;
Function PEszset(obj:hWnd; nProperty:Integer; szData: PChar):Integer; Far;
Function PEszget(obj:hWnd; nProperty:Integer; szData: PChar):Integer; Far;
Function PEgetmeta(obj:hWnd):Integer; Far;
Function PEallocateindmemory(obj:hWnd):Integer; Far;
Function PEreinitialize(obj:hWnd):Integer; Far;
Function PEresetimage(obj:hWnd; nWidth: Integer; nHeight: Integer):Integer; Far;
Function PEdrawcursor(obj:hWnd; hTargetDC: hDC; nAction: Integer):Integer; Far;
Function PEcopybitmaptoclipboard(obj:hWnd; lpPoint: Pointer): Integer; Far;
Function PEcopybitmaptofile(obj:hWnd; lpPoint: Pointer; lpFileName: PChar): Integer; Far;
Function PEcopymetatoclipboard(obj:hWnd; lpPoint: Pointer): Integer; Far;
Function PEcopymetatofile(obj:hWnd; lpPoint: Pointer; lpFileName: PChar): Integer; Far;
Function PEcopyoletoclipboard(obj:hWnd; lpPoint: Pointer): Integer; Far;
Function PElaunchcolordialog(obj: hWnd): Integer; Far;
Function PElaunchcustomize(obj:hWnd): Integer; Far;
Function PElaunchexport(obj:hWnd): Integer; Far;
Function PElaunchfontdialog(obj:hWnd): Integer; Far;
Function PElaunchmaximize(obj:hWnd): Integer; Far;
Function PElaunchpopupmenu(obj:hWnd; lp: Pointer): Integer; Far;
Function PElaunchprintdialog(obj:hWnd; bFullPage: Integer; lpPoint: Pointer): Integer; Far;
Function PElaunchtextexport(obj:hWnd; bToFile: Integer; lpFileName: PChar): Integer; Far;
Function PEreinitializecustoms(obj: hWnd): Integer; Far;
Function PEprintgraph(obj:hWnd; hDC:Integer; nWidth:Integer; nHeight:Integer; nOrient:Integer): Integer; Far;
Function PEconvpixeltograph(obj:hWnd; nAxis:Pointer; nX:Pointer; nY:Pointer; fX:Pointer; fY:Pointer; bRight:
Integer; bTop:Integer; bViceVersa:Integer): Integer; Far;
Function PEreset(obj:hWnd):Integer; Far;
Function PEstore(obj:hWnd; lphGlobal:pointer; lpcount:pointer):Integer; Far;
Function PEload(obj:hWnd; lphGlobal:pointer):Integer; Far;

implementation
Function PEcreate; external 'PEGRAPHS';
Function PEdestroy; external 'PEGRAPHS';
Function PEvset; external 'PEGRAPHS';
Function PEvget; external 'PEGRAPHS';
Function PEvsetcell; external 'PEGRAPHS';
Function PEvgetcell; external 'PEGRAPHS';
Function PEvsetcellEx; external 'PEGRAPHS';
Function PEvgetcellEx; external 'PEGRAPHS';
Function PEnset; external 'PEGRAPHS';
Function PEnget; external 'PEGRAPHS';
Function PElset; external 'PEGRAPHS';
Function PElget; external 'PEGRAPHS';
Function PEszset; external 'PEGRAPHS';
Function PEszget; external 'PEGRAPHS';
Function PEgetmeta; external 'PEGRAPHS';
Function PEallocateindmemory; external 'PEGRAPHS';
Function PEreinitialize; external 'PEGRAPHS';
Function PEresetimage; external 'PEGRAPHS';
Function PEdrawcursor; external 'PEGRAPHS';
Function PEcopybitmaptoclipboard; external 'PEGRAPHS';
Function PEcopybitmaptofile; external 'PEGRAPHS';
Function PEcopymetatoclipboard; external 'PEGRAPHS';
Function PEcopymetatofile; external 'PEGRAPHS';
Function PEcopyoletoclipboard; external 'PEGRAPHS';
Function PElaunchcolordialog; external 'PEGRAPHS';
Function PElaunchcustomize; external 'PEGRAPHS';
Function PElaunchexport; external 'PEGRAPHS';
Function PElaunchfontdialog; external 'PEGRAPHS';
Function PElaunchmaximize; external 'PEGRAPHS';
Function PElaunchpopupmenu; external 'PEGRAPHS';
Function PElaunchprintdialog; external 'PEGRAPHS';
Function PElaunchtextexport; external 'PEGRAPHS';
Function PEreinitializecustoms; external 'PEGRAPHS';
Function PEprintgraph; external 'PEGRAPHS';
Function PEconvpixeltograph; external 'PEGRAPHS';
Function PEreset; external 'PEGRAPHS';
Function PEstore; external 'PEGRAPHS';
Function PEload; external 'PEGRAPHS';

{$ENDIF}

end.

