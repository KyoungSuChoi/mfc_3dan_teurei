ProEssentials/DLL Powerbuilder Implementation Notes,

Be sure to destroy objects you create.
When implementing the ProEssentials with Powerbuilder via the DLL, you must call
PEdestroy for every call to PEcreate.  The most common place for PEcreate will be
the Open Event and the most common place for PEdestroy will be the Close Event.

Adjusting properties for an exising PE object.
Try to call PEreinitialize only once, lastly, after setting initial properties and
data in the Open Window Event.  Once the object is created, call PEreinitializecustoms,
PEresetimage, and the Window API call InvalidateRect to show adjustments in properties
in an previously initialized object.  

// make property adjustments
PEreinitializecustoms(hPE)
PEresetimage(hPE, 0, 0)
InvalidateRect(hPE, 0, 0)	

Declarations.
The following are the external function declarations:  You can cut and paste from
these as required.

FUNCTION int PEcreate(int nControl, long dwStyle, ref rect theloc, int hParent, int nID) LIBRARY "PEGRAPHS.DLL" 
FUNCTION int PEdestroy(int hPE) LIBRARY "PEGRAPHS.DLL"

FUNCTION int PEnset(int hPE, int nProp, int nData) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEvset(int hPE, int nProp, ref blob pData, int nItems) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEvsetcell(int hPE, int nProp, int nCell, ref blob pData) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEszset(int hPE, int nProp, ref string szData) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElset(int hPE, int nProp, long lData) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEnget(int hPE, int nProp) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEvget(int hPE, int nProp, ref blob pData) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEvgetcell(int hPE, int nProp, int nCell, ref blob pData) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEszget(int hPE, int nProp, ref string szData) LIBRARY "PEGRAPHS.DLL"
FUNCTION long PElget(int hPE, int nProp) LIBRARY "PEGRAPHS.DLL"

FUNCTION int PEcopybitmaptoclipboard(int hPE, ref pointstruct thesize) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEcopybitmaptofile(int hPE, ref pointstruct thesize, ref string szFileName) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEcopymetatoclipboard(int hPE, ref pointstruct thesize) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEcopymetatofile(int hPE, ref pointstruct thesize, ref string szFileName) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEcopyoletoclipboard(int hPE, ref pointstruct thesize) LIBRARY "PEGRAPHS.DLL"

FUNCTION int PEgetmeta(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEreinitialize(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEreinitializecustoms(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PEresetimage(int hPE, int xAspect, int yAspect) LIBRARY "PEGRAPHS.DLL"

FUNCTION int PElaunchcolordialog(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElaunchcustomize(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElaunchexport(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElaunchfontdialog(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElaunchmaximize(int hPE) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElaunchpopupmenu(int hPE, ref pointstruct theloc) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElaunchprintdialog(int hPE, int bFullPage, ref pointstruct thesize) LIBRARY "PEGRAPHS.DLL"
FUNCTION int PElaunchtextexport(int hPE, int bToFile, ref string szFileName) LIBRARY "PEGRAPHS.DLL"

Structures.
The ProEssentials deal with certain structures as part of function
arguments.  The following are pseudo code structures that need to be
defined as PB4 structures with the Structure Painter.

struct named "rect"
	left	integer
	top	integer
	right	integer
	bottom	integer
end struct
	
struct named "pointstruct"
	x	integer
	y	integer
end struct


Example Implementation
A Global Variable hPE is declared to store the handle to the PE object.

For the Open Event:

	// INITIALIZE TO ZERO SO WE KNOW OF OBJECT IS CREATED OR NOT //		
	hPE = 0

	// PREPARE A RECT WHERE CONTROL IS TO BE PLACED, PIXEL COORDINATES //
	rect myrect
	myrect.left = 25
	myrect.top = 25
	myrect.right = 700
	myrect.bottom = 450

	// CREATE OBJECT //
	hPE = PECREATE(354, 0, myrect, Handle(This), 1000) 

	// ALLOCATE SOME HELPER TEMP VARIABLES //
	blob {1024} bData
	string 	tmpstr
	int 	i
	Real 	rD;

	// SET THE AMOUNT OF DATA, SHOULD ALWAYS BE FIRST //
	PEnset(hPE, PEP_nSUBSETS, 3)
	PEnset(hPE, PEP_nPOINTS, 40)

	// PREPARE IMAGES IF OBJECT IS A USER INTERFACE OBJECT //
	PEnset(hPE, PEP_bPREPAREIMAGES, 1)

	// LET VIEW ONLY 10 POINTS AT A TIME //
	PEnset(hPE, PEP_nPOINTSTOGRAPH, 10)

	// SET POINTS PLUS LINE PLOTTING STYLE //
	PEnset(hPE, PEP_nPLOTTINGMETHOD, PEGPM_POINTSPLUSLINE)

	// SET AUTO COORD PROMPTING AND POINT LABEL HOT SPOTS //
	PEnset(hPE, PEP_bALLOWDATAHOTSPOTS, 1)
	PEnset(hPE, PEP_bALLOWCOORDPROMPTING, 1)
	PEnset(hPE, PEP_bALLOWPOINTHOTSPOTS, 1)
	PEnset(hPE, PEP_bALLOWSUBSETHOTSPOTS, 1)

	// SET THE MAIN TITLE OF GRAPH //
	tmpstr = "Some Title Text"
	PEszset(hPE, PEP_szMAINTITLE, tmpstr)

	// SET DESK COLOR TO LIGHT GREY //
	PElset(hPE, PEP_dwDESKCOLOR, RGB(192, 192, 192))

	// THIS IS HOW TO SET YOUR OWN SUBSET POINT TYPES //
	// NOTE THAT A POINT TYPE IS REPRESENTED WITH 2 BYTES (integer) //
	BlobEdit(bData, 1, 0)
	BlobEdit(bData, 3, 4)
	BlobEdit(bData, 5, 3)
	BlobEdit(bData, 7, 8)
	PEvset(hPE, PEP_naSUBSETPOINTTYPES, bData, 4)

	// THIS IS HOW TO SET YOUR OWN SUBSET COLORS //
	// NOTE THAT A COLOR IS REPRESENTED WITH 4 BYTES (long) //
	BlobEdit(bData, 1, RGB(0, 255, 0))
	BlobEdit(bData, 5, RGB(255, 0, 0))
	BlobEdit(bData, 9, RGB(0, 0, 255))
	BlobEdit(bData, 13, RGB(255, 255, 0))
	PEvset(hPE, PEP_dwaSUBSETCOLORS, bData, 4)

	// THIS IS ONE WAY OF PASSING PROPERTY ARRAYS, PREPARING ALL OF IT IN A BLOB VARIABLE //
	// BE SURE THAT BLOB IS BIG ENOUGH TO HANDLE ALL YOUR DATA //
	For i = 0 to 119	// NOTE THAN 0 to 119 IS 120 DATAPOINTS WHICH IS SUBSETS X POINTS //
		rD = (Rand(25) + 5) + (Rand(10) / 10.0)
		BlobEdit(bData, 1 + (i * 4), rD)
	Next 
	PEvset(hPE, PEP_faYDATA, bData, 120)

	// THIS IS ANOTHER WAY OF PASSING PROPERTY ARRAYS, INDIVIDUALLY SETTING EACH ELEMENT // 
	For i = 0 to 3
		tmpstr = "Data Set " + String(i)
		BlobEdit(bData, 1, tmpstr)
		PEvsetcell(hPE, PEP_szaSUBSETLABELS, i, bData)
	Next 
	For i = 0 to 39
		tmpstr = "Point " + String(i)
		BlobEdit(bData, 1, tmpstr)
		PEvsetcell(hPE, PEP_szaPOINTLABELS, i, bData)
	Next 

For the Close Event:

	IF hPE <> 0 then
		PEdestroy(hPE)
	end if	

For a user event attached to pbm_command message

	Blob {18} 	bData
	int 		hstype
	string		tmpstr

	// COULD ALSO TEST FOR PEWN_DBLCLICKED IF NEEDED //

	IF IntHigh(Message.LongParm) = PEWN_CLICKED THEN

		// USER CLICKED GRAPH, SO LET US SEE IF HOT SPOT DATA IS AVAILABLE //
		PEvget(IntLow(Message.LongParm), PEP_structHOTSPOTDATA, bData);
		hstype = Integer(BlobMid(bData, 9, 2))

		// TEST OF POINT LABEL HOT SPOT //
		IF hstype = PEHS_POINT THEN
			tmpstr = "You clicked point label " + String(Long(BlobMid(bData, 11, 4)))
			MessageBox("Point Hot Spot", tmpstr)
		END IF

		// TEST OF SUBSET LABEL HOT SPOT //
		IF hstype = PEHS_SUBSET THEN
			tmpstr = "You clicked subset label " + String(Long(BlobMid(bData, 11, 4)))
			MessageBox("Subset Hot Spot", tmpstr)
		END IF
	END IF

