///////Power Supply System API Define/////////
//											//	
//	PSCommon.h								//	
//	ADP Cell Test System					//	
//	define of PSCommon.dll					//
//	Byung Hum - Kim		2005.8.10			//
//											//
//////////////////////////////////////////////

#ifndef _PS_COMMON_API_DEFINE_H_
#define _PS_COMMON_API_DEFINE_H_

#include "PowerSys.h"
#include "ChannelCode.h"

#define PS_DATA_ANAL_PRO_CLASS_NAME		"ADPDataAnalPro"

#define PS_MAX_STATE_COLOR				26

//각 상태별로 표시할 색상을 정의 
typedef struct tag_State_Display
{
	BOOL		bStateFlash;
	COLORREF	TStateColor;
	COLORREF	BStateColor;
	char		szMsg[32];
} _PS_STATE_CONFIG;


typedef struct tag_Over_Display
{
	float		fValue;			//0 : Voltage //1: Current
	BOOL		bFlash;
	COLORREF	TOverColor;
	COLORREF	BOverColor;	
} _PS_OVER_CONFIG;

typedef struct tag_Color_Config
{
	BYTE		bShowText;
	BYTE		bShowColor;
	BYTE		bShowOver;

	_PS_STATE_CONFIG	stateConfig[PS_MAX_STATE_COLOR];
	_PS_OVER_CONFIG		VOverConfig;
	_PS_OVER_CONFIG		IOverConfig;

} PS_COLOR_CONFIG;

//파일 저장시 ID Header
typedef struct PS_FILE_ID_HEADER {
	UINT	nFileID;
	UINT	nFileVersion;
	char	szCreateDateTime[64];
	char	szDescrition[128];
	char	szReserved[128];
}	PS_FILE_ID_HEADER, *LPPS_FILE_ID_HEADER;

typedef struct PS_RECORD_FILE_HEADER {
	int		nColumnCount;
	WORD	awColumnItem[PS_MAX_FILE_SAVE_ITEM_NUM];
}  PS_RECORD_FILE_HEADER;

typedef struct PS_RAW_FILE_HEADER 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_RECORD_FILE_HEADER rsHeader;
} PS_RAW_FILE_HEADER;

typedef struct PS_TEST_FILE_HEADER
{
	char szStartTime[64];
	char szEndTime[64];
	char szSerial[64];
	char szUserID[32];
	char szDescript[128];
	char szTrayNo[64];
	char szBuff[64];
	int  nRecordSize;
	WORD wRecordItem[PS_MAX_FILE_ITEM_NUM];
} PS_TEST_FILE_HEADER;

typedef struct PS_TEST_RESULT_FILE_HEADER 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_TEST_FILE_HEADER testHeader;
} PS_TEST_RESULT_FILE_HEADER;

typedef struct PS_STEP_END_RECORD		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG lReserved;

	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			// 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fTemparature;
	float fPressure;
	float fAvgVoltage;
	float fAvgCurrent;
	ULONG  nGotoCycleNum;		//20081202 KHS
	float fReserved[7];
	
} PS_STEP_END_RECORD, *LPPS_STEP_END_RECORD;

typedef struct PS_STEP_END_RECORD_DATA		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG lReserved;

	float fData[PS_MAX_ITEM_NUM];
} PS_STEP_END_RECORD_DATA, *LPPS_STEP_END_RECORD_DATA;

#define PS_ADP_RECORD_FILE_ID		5640
#define PS_ADP_STEP_RESULT_FILE_ID	20060728

#define PS_RESULT_FILE_NAME_EXT		"cts"
#define PS_RAW_FILE_NAME_EXT		"cyc"


#ifdef __cplusepluse
extern "C" {
#endif ///__cplusepluse

#ifndef __PS_COMMON_DLL__
#define __PS_COMMON_DLL__

#ifdef PS_COMMON_EXPORT_DLL_API
#define PS_DLL_API	__declspec(dllexport)
#else	//EXPORTSFT_DLL_API
#define PS_DLL_API	__declspec(dllimport)
#endif	//EXPORTSFT_DLL_API

#endif	//__SFT_FORM_DLL__

PS_DLL_API	void PSCellCodeMsg(BYTE code, CString &strMsg, CString &strDescript);

//Cell 양부 검사 함수
PS_DLL_API BOOL PSIsCellOk(BYTE code);
PS_DLL_API BOOL PSIsCellBad(BYTE code);

PS_DLL_API BOOL PSIsNonCell(BYTE code);
PS_DLL_API BOOL PSIsSysFail(BYTE code);
PS_DLL_API BOOL PSIsCellCheckFail(BYTE code);

//color 설정 함수 
PS_DLL_API PS_COLOR_CONFIG*  PSGetColorCfgData(BOOL bShowEditDlg = FALSE);

//표기관련 
PS_DLL_API LPCTSTR PSGetTypeMsg(WORD type);
PS_DLL_API LPCTSTR PSGetStateMsg(WORD State, WORD type = PS_STEP_NONE);
PS_DLL_API void PSGetStateColor(WORD state, WORD type, COLORREF &textColor, COLORREF &bkColor);
PS_DLL_API void PSGetTypeColor(WORD type, COLORREF &textColor, COLORREF &bkColor);

PS_DLL_API LPCTSTR PSGetItemName(int nCode);

//dll inner ftn
BOOL	WriteColorCfgData();


#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif //_PS_COMMON_API_DEFINE_H_