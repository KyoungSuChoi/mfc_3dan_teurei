//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "Unit3.h"
#include <math.h>
//---------------------------------------------------------------------------
#pragma link "Pesgvcl"
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm3::FormShow(TObject *Sender)
{
int s, p;
double d, dt;
float f, f2, f3;
TDateTime tdt;

PESGraph1->PrepareImages = True;
PESGraph1->Subsets = 12;
PESGraph1->Points = 200;
tdt = tdt.CurrentDateTime();
dt = (Double) tdt;
for (p=0; p<200; p++)
{
    f = 150 + (sin(p * 0.034) * 60) + (30 * ((float) rand() / (float) RAND_MAX)) + (p / 2);
    f2 = 10 + (10 * ((float) rand() / (float) RAND_MAX));
    f3 = 35 + (20 * ((float) rand() / (float) RAND_MAX));
    d = dt + p;
    PESGraph1->XData[0][p] = d;
    PESGraph1->YData[0][p] = f - f3;

    PESGraph1->XData[1][p] = d;
    PESGraph1->YData[1][p] = f + f3;

    PESGraph1->XData[2][p] = d;
    PESGraph1->YData[2][p] = f - f2;

    PESGraph1->XData[3][p] = d;
    PESGraph1->YData[3][p] = f + f2;

    f = 250 + (cos(p * 0.06) * 70) + (40 * ((float) rand() / (float) RAND_MAX)) + (p / 2);
    PESGraph1->XData[4][p] = d;
    PESGraph1->YData[4][p] = f;

    PESGraph1->XData[5][p] = d;
    PESGraph1->YData[5][p] = f + (90 * ((float) rand() / (float) RAND_MAX));
    f = -10 + (30 * ((float) rand() / (float) RAND_MAX));
    PESGraph1->XData[6][p] = d;
    PESGraph1->YData[6][p] = f;
    f = -20 + (50 * ((float) rand() / (float) RAND_MAX));
    PESGraph1->XData[7][p] = d;
    PESGraph1->YData[7][p] = f;
    f = -50 + (cos(p * 0.06) * 70) + (30 * ((float) rand() / (float) RAND_MAX)) + p;
    PESGraph1->XData[8][p] = d;
    PESGraph1->YData[8][p] = f;
    f = 100 + (sin(p * 0.15) * 100) + (20 * ((float) rand() / (float) RAND_MAX)) + p;
    PESGraph1->XData[9][p] = d;
    PESGraph1->YData[9][p] = f;
}

for (s=10; s<12; s++)
{
    for (p=0; p<200; p++)
    {
        d = dt + p;
        PESGraph1->XData[s][p] = d;
        PESGraph1->YData[s][p] = 15 + (75 * ((float) rand() / (float) RAND_MAX)) + (s * 100);
    }
}

for (p=0; p<12; p++)
    PESGraph1->SubsetLineTypes[p] = PELT_THINSOLID;

PESGraph1->SubsetColors[0] = clBlue;
PESGraph1->SubsetColors[4] = clBlack;
PESGraph1->SubsetColors[5] = clBlack;
PESGraph1->SubsetColors[6] = clRed;
PESGraph1->SubsetColors[7] = clBlack;
PESGraph1->SubsetColors[8] = clLime;
PESGraph1->SubsetColors[9] = clBlack;

PESGraph1->AxesAnnotationTextSize = 96;
PESGraph1->XAxisAnnotation[0] = dt + 16;
PESGraph1->XAxisAnnotation[1] = dt + 18;
PESGraph1->XAxisAnnotation[2] = dt + 20;
PESGraph1->XAxisAnnotation[3] = dt + 22;
PESGraph1->XAxisAnnotationText[0] = "Axis Annotation 1";
PESGraph1->XAxisAnnotationText[1] = "Axis Annotation 2";
PESGraph1->XAxisAnnotationText[2] = "Axis Annotation 3";
PESGraph1->XAxisAnnotationText[3] = "Axis Annotation 4";
PESGraph1->XAxisAnnotationColor[0] = clBlack;
PESGraph1->XAxisAnnotationColor[1] = clBlack;
PESGraph1->XAxisAnnotationColor[2] = clBlack;
PESGraph1->XAxisAnnotationColor[3] = clBlack;

PESGraph1->HorzLineAnnotationAxis[0] = 0;
PESGraph1->HorzLineAnnotation[0] = 300;
PESGraph1->HorzLineAnnotationType[0] = PELT_DASH;
PESGraph1->HorzLineAnnotationColor[0] = clRed;
PESGraph1->HorzLineAnnotationText[0] = "UPPER LIMIT";

PESGraph1->HorzLineAnnotationAxis[1] = 2;
PESGraph1->HorzLineAnnotation[1] = 100;
PESGraph1->HorzLineAnnotationType[1] = PELT_DASHDOT;
PESGraph1->HorzLineAnnotationColor[1] = clGray;
PESGraph1->HorzLineAnnotationText[1] = "median";

PESGraph1->VertLineAnnotation[0] = dt + 60;
PESGraph1->VertLineAnnotationType[0] = PELT_DASH;
PESGraph1->VertLineAnnotationColor[0] = clBlack;
PESGraph1->VertLineAnnotationText[0] = "|t2nd Qrt";
PESGraph1->VertLineAnnotation[1] = dt + 120;
PESGraph1->VertLineAnnotationType[1] = PELT_DASH;
PESGraph1->VertLineAnnotationColor[1] = clBlack;
PESGraph1->VertLineAnnotationText[1] = "|t3rd Qrt";
PESGraph1->VertLineAnnotation[2] = dt + 180;
PESGraph1->VertLineAnnotationType[2] = PELT_DASH;
PESGraph1->VertLineAnnotationColor[2] = clBlack;
PESGraph1->VertLineAnnotationText[2] = "|t4th Qrt";

PESGraph1->GraphAnnotationTextSize = 96;
PESGraph1->GraphAnnotationAxis[0] = 3;
PESGraph1->GraphAnnotationX[0] = dt + 65;
PESGraph1->GraphAnnotationY[0] = 950;
PESGraph1->GraphAnnotationType[0] = PEGAT_NOSYMBOL; // text only
PESGraph1->GraphAnnotationText[0] = "Annotation Text Only";
PESGraph1->GraphAnnotationColor[0] = clBlack;

PESGraph1->GraphAnnotationAxis[1] = 3;
PESGraph1->GraphAnnotationX[1] = dt + 78;
PESGraph1->GraphAnnotationY[1] = 1161;
PESGraph1->GraphAnnotationType[1] = PEGAT_POINTER;
PESGraph1->GraphAnnotationText[1] = "Text with Pointer";
PESGraph1->GraphAnnotationColor[1] = clBlack;

PESGraph1->GraphAnnotationAxis[2] = 3;
PESGraph1->GraphAnnotationX[2] = dt + 138;
PESGraph1->GraphAnnotationY[2] = 950;
PESGraph1->GraphAnnotationType[2] = PEGAT_SMALLUPTRIANGLESOLID;
PESGraph1->GraphAnnotationText[2] = "Symbol w/wo Text";
PESGraph1->GraphAnnotationColor[2] = clBlack;

PESGraph1->AllowDataHotSpots = True;

//{**This will disable all subset legends**}
PESGraph1->SubsetsToLegend[0] = -1;

PESGraph1->MultiAxesSubsets[0] = 6;
PESGraph1->MultiAxesSubsets[1] = 2;
PESGraph1->MultiAxesSubsets[2] = 2;
PESGraph1->MultiAxesSubsets[3] = 2;

PESGraph1->MultiAxesProportions[0] = 0.5;
PESGraph1->MultiAxesProportions[1] = 0.15;
PESGraph1->MultiAxesProportions[2] = 0.15;
PESGraph1->MultiAxesProportions[3] = 0.2;

PESGraph1->WorkingAxis = 0;
PESGraph1->PlottingMethod = sgSpecificPlotMode;
PESGraph1->SpecificPlotMode = sgBoxPlot;
PESGraph1->RYAxisComparisonSubsets = 2;
PESGraph1->YAxisLabel = "Y Axis #1";
PESGraph1->RYAxisLabel = "Y Axis #2";
PESGraph1->ManualScaleControlY = sgManualMinAndMax;
PESGraph1->ManualMinY = 1;
PESGraph1->ManualMaxY = 400;

PESGraph1->WorkingAxis = 1;
PESGraph1->PlottingMethod = sgBar;
PESGraph1->YAxisLabel = "Y Axis #3";

PESGraph1->WorkingAxis = 2;
PESGraph1->PlottingMethod = sgArea;
PESGraph1->RYAxisComparisonSubsets = 1;
PESGraph1->YAxisLabel = "Y Axis #4";
PESGraph1->RYAxisLabel = "Y Axis #5";

PESGraph1->WorkingAxis = 3;
PESGraph1->PlottingMethod = sgArea;
PESGraph1->ManualScaleControlY = sgManualMin;
PESGraph1->ManualMinY = 900;
PESGraph1->ManualMaxY = 1500;
PESGraph1->YAxisLabel = "Y Axis #6";

PESGraph1->WorkingAxis = 0;

PESGraph1->DataShadows = False;
PESGraph1->DataPrecision = sgOneDecimal;
PESGraph1->MainTitle = "Multi Y Axes Example";
PESGraph1->SubTitle = "";
PESGraph1->MainTitleBold = True;
PESGraph1->LabelBold = True;
PESGraph1->ShowAnnotations = True;
PESGraph1->AllowAnnotationControl = True;
PESGraph1->AllowZooming = sgHorzPlusVertZooming;
PESGraph1->ScrollingHorzZoom = True;
PESGraph1->CursorMode = sgCrossHair;
PESGraph1->CursorPromptStyle = sgNoPrompt;
PESGraph1->CursorPromptTracking = True;
PESGraph1->MouseCursorControl = True;
PESGraph1->GridLineControl = sgNoGrid;
PESGraph1->FontSize = sgSmall;
PESGraph1->FocalRect = False;
PESGraph1->FontSizeGlobalCntl = 1.1;
PESGraph1->LineAnnotationTextSize = 90;

//{Note that Delphi 2->0 changed to a date/time format compatible with VB}
PESGraph1->DateTimeMode = sgVBDateTime;

PESGraph1->PEactions = sgReinitAndReset;
}
//--------------------------------------------------------------------------- 