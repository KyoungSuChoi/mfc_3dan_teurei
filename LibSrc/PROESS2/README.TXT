Very Important Information, Please Read!

// Version 2.2 Update Information 10-5-97 //

ProEssentials have been tested and work with the following recent 
releases: VB5, VC5, Delphi3, Builder, and Access 97.

V2.2 Changes and/or Additions are noted at the top of PEONLREF.HLP
Please read this information.

// UPDATING THE PROESSENTIALS PRODUCT //

Important, if you are updating from version 2.0, or 2.1
of ProEssentials, be sure to re-register the OCX and/or VCL 
interfaces if you are currently using these interfaces.  

If updating from the eval edition, run setup and install 
the retail version on-top of the eval version.  
No need to re-register OCXs.
VCLs will need to be removed and reinstalled into COMPLIB.
If using Delphi or Builder and you moved ProEssentials 
PAS, OBJ, LIB, HPP, etc files to a different location.
Be sure to repeat this process after installing retail
version.

Also, if updating from version 2.0, or 2.1 and using 
the OCX interfaces, it's highly recommended that you delete 
and reinsert the controls from your project's forms.  
Hopefully this will be the last time this is necessary.

// NEW USERS GETTING STARTED //

The best way to get started is to:

1) Read this file and any other ReadMe files.
2) Read Common Questions section in "PEONLREF.HLP"
3) Read VBX/OCX/VCL Specific Facts in "PEONLREF.HLP"
4) Load VBX/OCX/VCLs into your SDE.
5) Load and study installed example projects.
6) Make your first ProEssentials application a small
"Hello World" type app before trying to implement
ProEssentials into your existing projects.

You'll find several tech-notes in the directory where
you install ProEssentials.  These files have a TXT
extension.  Please review these since they also answer 
commonly asked questions.

// OCX INFO //

This installation program does not register the 
OCX interfaces automatically.  You will need
to register these by installing them into 
VB4 via Tools/CustomControls/Browse.  Or via
VB5, in Project/Components/Browse. The OCXs 
can be found in the system directory and are
as follows:

PEGO16.OCX  16 Bit Graph Object
PESGO16.OCX 16 Bit Scientific Graph Object
PEPSO16.OCX 16 Bit Polar/Smith Object
PEPCO16.OCX 16 Bit Pie Chart Object

PEGO32.OCX  32 Bit Graph Object
PESGO32.OCX 32 Bit Scientific Graph Object
PEPSO32.OCX 32 Bit Polar/Smith Object
PEPCO32.OCX 32 Bit Pie Chart Object

Be sure to register these before loading the
example projects.

** Microsoft VB4-16 NOTE
We have seen a bug unique to VB4-16 
when setting the Points property to a value 
larger than 7000 via the property window.  
This is not a problem in VB3, VB4-32, or 
Delphi.  It's a good practice to set the
Points property, as well as the Subsets 
property in code prior to your logic that 
passes data into the control.

** Microsoft VC 4.0 NOTE
If VC4 is used to register the 
ProEssentials OCXs, it may copy the OCXs into 
the MSDEV/TEMPLATE directory.  However, it 
does not copy the LIC file.  If you receive 
an error stating you don't have a license to
use the OCXs, copy the PEGO.LIC license file 
from the SYSTEM directory to the MSDEV/TEMPLATE
directory.  The license file PEGO.LIC must 
reside in the same directory as OCX files.

// BORLAND DELPHI VCL INFO //

The pascal source is provided for the VCL 
interfaces. These PAS files can be found in 
the PROESS2/DELPHI1 and PROESS2/DELPHI2 sub 
directories and should be used to install 
the VCL interfaces into Delphi.

The PAS VCL source code is identical in both the
Delphi1 and Delphi2 sub directories.  However, 
the resource files are in 16/32 respective formats.

PEGVCL.PAS - Graph Object
PESGVCL.PAS - Scientific Graph Object
PEPSVCL.PAS - Polar/Smith Chart Object
PEPCVCL.PAS - Pie Chart Object

PEGRPAPI.PAS - Declarations and constants.  Include 
in the Uses clause to call ProEssentials DLL functions 
and use constants such as line/point styles. DO NOT
try to load this file as a component.

When these components are installed, you will find
them in the "Additional Tab" section of Delphi's
control palette.

We have experienced problems when loading the PAS
VCL source code into Delphi 2 on Win95.  It seems 
that once the VCLs are installed, you should 
immediately exit Delphi 2.  Upon exit there may be 
a page fault.  Once you restart Delphi you should 
not see any further problems. Various sources have
confirmed this to be a Delphi2 problem.  Delphi3 and
Builder do not have this problem.

// BORLAND BUILDER //

In the Builder sub directory there are four files
which should be loaded as VCL components.

PEGCPP.CPP - Graph Object
PESGCPP.CPP - Scientific Graph Object
PEPSCPP.CPP - Polar/Smith Chart Object
PEPCCPP.CPP - Pie Chart Object

Read the Readme file in the Builder directory for
more information.

Try to install ProEssentials at C:\PROESS2 and
load the VCLs from the C:\PROESS2\BUILDER directory.
Builder is very touchy about moving a project from
its originating directory.  It's best to move
the ProEssentials OBJ and HPP files to the location 
where Builder stores its versions of these files.

When installed, these components will show up in the
Additional tab.

// VBX INFO //

The following VBX's are installed in your system dir:

PEGRPH.VBX - Graph Object
PESGRPH.VBX - Scientific Graph Object
PEPGRPH.VBX - Polar/Smith Chart Object
PEPIE.VBX - Pie Chart Object

// PORTING FROM VERSION 1.5 to 2.2 //

If you were using v1.5's VBX interfaces, porting to 
v2.2's VBX interfaces is automatic.  

If you're switching from VBXs to OCXs, the XData, 
YData, ZData, PointColors, and DataPointLabels 
properties are now two dimensional.  This will
simply and clarify your code.

If you were using the DLL via C/C++ or other 
SDE using ProEssentials DLL calls, there are some
important changes you need to address.  Namely,
most of the FLOAT properties changed to DOUBLE.
Since PEvset and PEvget are used for floating
point properties, you will need to make sure
PEvset and PEvget are pointing to the correct
floating point type.











