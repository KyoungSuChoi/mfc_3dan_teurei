Please read this license.  If you do not agree to the 
following terms and conditions, exit this installation 
program and return the product to whom it was purchased.

If not stated otherwise, any and all materials included 
are copyrighted property of Gigasoft, Inc. (C) (1994-1997)
All rights reserved.

Software License Agreement
GigaSoft(R) ProEssentials(TM) v2.0

Read This Before Use 

The Software contained in this package is subject to 
the following license terms and conditions.  Please 
read this license before you open the disk package 
or unzip.  If you do not agree to the terms and 
conditions of this license, or if after use you are 
dissatisfied with the Software, you may return the 
Software package and manual within thirty days to 
Gigasoft for a full refund.  Opening the packaging 
and/or using the ProEssentials Software indicates 
your acceptance of these terms.

SINGLE COPY SOFTWARE LICENSE BY GIGASOFT, INC.
THIS IS A SINGLE COPY SOFTWARE LICENSE granted by 
Gigasoft, Inc.  The software in this package is 
licensed to you as the end user.  This Software is 
owned by and remains the property of Gigasoft, Inc., 
is protected by United States copyright law and 
international treaty provisions, and is transferred 
to the original purchaser and any subsequent owner 
of the Software media for their use only on the 
license terms set forth below.

Once you have paid the required single copy license 
fee, you may use the software for as long as you like 
provided you do not violate the copyright and you 
follow these rules.
* Gigasoft, Inc. ("Gigasoft"), grants the original 
purchaser ("Licensee") the limited rights to possess 
and use the Gigasoft Software and User Manual for 
its intended purposes.  Licensee agrees that the 
Software will be used solely for Licensee's internal 
purposes.
* You may use the Software on any computer for which 
it is designed so long as no more than one person uses 
it at any one time.  You must pay for additional copies 
of the software if more than one person will be using 
it at the same time on one or more computers.
* If the Software is installed on a networked system, 
or on a computer connected to a file server or other 
system that physically allows shared access to the 
Software, Licensee agrees to provide technical or 
procedural methods to prevent use of the Software by 
more than one user.
* You may make no more than one(1) copies of the 
licensed software for back-up purposes and all such 
copies, together with the original, must be kept in 
your possession or control.  The back-up copy must 
display all proprietary notices, and be labeled 
externally to show that the back-up copy is the 
property of Gigasoft, and that use is subject to this 
license.  Documentation may not be copied in whole or 
part.
* Licensee may transfer its rights under this License 
provided that the party to whom such rights are 
transferred agrees to the terms and conditions of this 
License, and written notice is provided to Gigasoft.  
Upon such transfer, Licensee must transfer or destroy 
all copies of the Software.
* You may not make any changes or modification to the 
licensed software, and you may not decompile, disassemble, 
or otherwise reverse engineer the software. You may not 
rent or lease it to others.
* When distributing applications which implement the 
ProEssentials Software, only the following files are 
permitted to be redistributed: PEGRAPHS.DLL,
PEGRAP32.DLL, PEGRAPHS.HLP, PEGRPSVR.EXE, 
PEGRPH.VBX, PESGRPH.VBX, PEPGRPH.VBX, PEPIE.VBX, 
PEGO16.OCX, PESGO16.OCX, PEPSO16.OCX, PEPCO16.OCX, 
PEGO32.OCX, PESGO32.OCX, PEPSO32.OCX, and PEPCO32.OCX.
Gigasoft grants the licensee a nonexclusive royalty-free 
right to reproduce and distribute the object code version 
of the above mentioned files.  If you redistribute the 
above files, the licensee must 1) distribute the above 
files only in conjunction with and as part of your 
software application product where ProEssentials adds 
significant functionality; 2) the software application 
ProEssentials is being added to must provide some other 
primary functionality other than the production of 
presentation graphics; 3) do not use Gigasoft's 
name, logo, or trademarks to market your software product; 
4) include valid copyright notice on your software product; 
5) agree to indemnify, hold harmless, and defend Gigasoft 
from and against any claims or lawsuits, including 
attorneys' fees, that arise or result from the use or 
distribution of your software application product; and 
6) do not permit further redistribution of the above 
files by your end users.

LIMITED WARRANTY
Gigasoft warrants that the magnetic media which the 
software is recorded on and the documentation provided 
with it are free from defects in materials and 
workmanship under normal use for a period of ninety 
days from purchase.  During this period Gigasoft will 
replace at no cost any such media returned to Gigasoft 
postage prepaid.  This service is Gigasoft's sole 
liability under this warranty.

DISCLAIMER
LICENSE FEES FOR THE SOFTWARE DO NOT 
INCLUDE ANY CONSIDERATIONS FOR ASSUMPTION 
OF RISK BY GIGASOFT, AND GIGASOFT DISCLAIMS 
ANY AND ALL LIABILITY FOR INCIDENTAL OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE 
USE OR OPERATION OR INABILITY OT USE THE 
SOFTWARE, EVEN IF ANY OF THESE PARTIES HAVE 
BEEN ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGES.  FURTHERMORE, LICENSEE INDEMNIFIES 
AND AGREES TO HOLD GIGASOFT HARMLESS FROM
SUCH CLAIMS.  THE ENTIRE RISK AS TO THE 
RESULTS AND PERFORMANCE OF THE SOFTWARE IS 
ASSUMED BY THE LICENSEE.  THE WARRANTIES 
EXPRESSED IN THIS LICENSE ARE THE ONLY 
WARRANTIES MADE BY GIGASOFT AND ARE IN 
LIEU OF ALL OTHER WARRANTIES, EXPRESSED 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
IMPLIED WARRANTIES OF MERCHANTABILITY 
AND OF FITNESS FOR A PARTICULAR PURPOSE.
THIS WARRANTY GIVES YOU SPECIFIED LEGAL 
RIGHTS, AND YOU MAY ALSO HAVE OTHER RIGHTS 
WHICH VARY FROM JURISDICTION TO JURISDICTION. 
SOME JURISDICTIONS DO NOT ALLOW THE 
EXCLUSION OR LIMITATION OF WARRANTIES, SO 
THE ABOVE LIMITATIONS OR EXCLUSIONS MAY 
NOT APPLY TO YOU.

U.S. GOVERNMENT RESTRICTED RIGHTS
The Software and documentation are provided with 
restricted rights.  Use, duplication, or disclosure
by the Government is subject to restrictions as set 
fourth in subparagraph (c) (1) (ii) of the Rights in 
Technical Data and Computer Software clause at 
DFARS 252.227-7013 or subparagraph (c) (1) and (2) 
of the Commercial Computer Software-Restricted Rights 
at 48 CFR 52.227-19, as applicable.  Manufacturer is 
Gigasoft, Inc. / 696 Lantana / Keller, TX  76248.

This license is the complete and exclusive statement 
of the parties' agreement.  Should any provision of 
this license be held to be invalid by any court of 
competent jurisdiction, that provision will be 
enforced to the maximum extent permissible, and the 
remainder of the license shall nonetheless remain in 
full force and effect.  This license shall be 
controlled by the laws of the State of Texas, and the 
United States of America.

COPYRIGHT (C) 1994-1997 BY GIGASOFT, INC. 
ALL RIGHTS RESERVED

