The following is another C/C++ Scientific Graph real-time scenario.
This example is a little easier to follow than the one that comes
in the VC example project.

// In your initialization code .......

        m_hPE = PEcreate(PECONTROL_SGRAPH, WS_VISIBLE, &rect, m_hWnd, cnt + 100);
        if (m_hPE)
        {
            cnt ++;
            SetGlobalPEStuff();
            srand( (unsigned)time( NULL ) );
            
            // SET PROPERTIES DEFINING AMOUNT OF DATA //
            PEnset(m_hPE, PEP_nSUBSETS, 1);
            nMaxBufferSize = 250;
            PEnset(m_hPE, PEP_nPOINTS, nMaxBufferSize); // some maximum buffer size
            
            PEszset(m_hPE, PEP_szMAINTITLE, "Real Time Example");
            PEszset(m_hPE, PEP_szSUBTITLE, "");
            
            PEvsetcell(m_hPE, PEP_szaSUBSETLABELS, 0, "Signal Input");
            
            PEszset(m_hPE, PEP_szYAXISLABEL, "Pressure");
            PEszset(m_hPE, PEP_szRYAXISLABEL, "Pressure");
            PEszset(m_hPE, PEP_szXAXISLABEL, "Distance");
            
            PEnset(m_hPE, PEP_bDATASHADOWS, FALSE);
            PEnset(m_hPE, PEP_bCURSORPROMPTTRACKING, FALSE);
            PEnset(m_hPE, PEP_bALLOWPOPUP, FALSE);
            PEnset(m_hPE, PEP_bNORANDOMPOINTSTOGRAPH, TRUE);
            PEnset(m_hPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
            PEnset(m_hPE, PEP_bFORCERIGHTYAXIS, TRUE);
            
            PEnset(m_hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
            double m = 0.0F;
            PEvset(m_hPE, PEP_fMANUALMINY, &m, 0);
            m = 100.0F;
            PEvset(m_hPE, PEP_fMANUALMAXY, &m, 0);
            PEnset(m_hPE, PEP_nMANUALSCALECONTROLRY, PEMSC_MINMAX);
            m = 0.0F;
            PEvset(m_hPE, PEP_fMANUALMINRY, &m, 0);
            m = 100.0F;
            PEvset(m_hPE, PEP_fMANUALMAXRY, &m, 0);

            
            PEnset(m_hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
            m = 0.0F;  
            PEvset(m_hPE, PEP_fMANUALMINX, &m, 0);
            m = .050F;
            PEvset(m_hPE, PEP_fMANUALMAXX, &m, 0);
            
            // Empty out first four default data points to start with an empty graph //
            f1 = 0.0F;
            PEvsetcell(m_hPE, PEP_faXDATA, 0, &f1);
            PEvsetcell(m_hPE, PEP_faXDATA, 1, &f1);
            PEvsetcell(m_hPE, PEP_faXDATA, 2, &f1);
            PEvsetcell(m_hPE, PEP_faXDATA, 3, &f1);
            PEvsetcell(m_hPE, PEP_faYDATA, 0, &f1);
            PEvsetcell(m_hPE, PEP_faYDATA, 1, &f1);
            PEvsetcell(m_hPE, PEP_faYDATA, 2, &f1);
            PEvsetcell(m_hPE, PEP_faYDATA, 3, &f1);
            
            PEnset(m_hPE, PEP_bALLOWDATAHOTSPOTS, FALSE);
            PEnset(m_hPE, PEP_bALLOWSUBSETHOTSPOTS, FALSE);
            PEnset(m_hPE, PEP_nALLOWDATALABELS, FALSE);
            PEnset(m_hPE, PEP_nALLOWZOOMING, FALSE);

            PEnset(m_hPE, PEP_nGRIDLINECONTROL, PEGLC_YAXIS);
			
            PEreinitialize(m_hPE);
            PEresetimage(m_hPE, 0, 0);

            m_nTimerID = SetTimer(1, 250, NULL);
        }    

// In your timer event......

        // Scientific Graph Real Time Feed //
        double m;
        float r1, r2, f;
        r1 = (float) 50.0F + ((float) sin((float) m_nRealTimeCounter * .075F) * 30.0F) + GetRandom(1, 15);
        r2 = (float) GetRandom(1000, 9999);
        
        if (m_nRealTimeCounter >= nMaxBufferSize)
        {
                // IF EXCEEDS BUFFER THEN APPEND NEW DATA (WHICH SHIFTS CURRENT DATA) //

	        f = (float) m_nRealTimeCounter * .001F; // for simulation purposes, our new XData is 1/1000 of our counter
                PEvset(m_hPE, PEP_faAPPENDXDATA, &f, 1);

	        f = r1 + (r2 * .0001F); // new random data for YData
                PEvset(m_hPE, PEP_faAPPENDYDATA, &f, 1);
        }
        else
        {
	        // UNTIL WE EXCEED BUFFER, JUST SET DATA AS WE GET IT //
	        f = (float) m_nRealTimeCounter * .001F; // for simulation purposes, our new XData is 1/1000 of our counter
                PEvsetcell(m_hPE, PEP_faXDATA, m_nRealTimeCounter, &f);

        	f = r1 + (r2 * .0001F); // new random data for YData
        	PEvsetcell(m_hPE, PEP_faYDATA, m_nRealTimeCounter, &f);
        }

        m_nRealTimeCounter ++;

        PEvget(m_hPE, PEP_fMANUALMAXX, &m);
        if (m_nRealTimeCounter > (unsigned int) (m * 1000.0F))
        {
                m +=.025F;
                PEvset(m_hPE, PEP_fMANUALMAXX, &m, 0);
                m -= .050001F; 
                PEvset(m_hPE, PEP_fMANUALMINX, &m, 0);
                PEreinitialize(m_hPE);
        }
        
        PEresetimage(m_hPE, 0, 0);
        ::InvalidateRect(m_hPE, NULL, FALSE);


