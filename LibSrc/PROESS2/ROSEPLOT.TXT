Rose Plot Supplemental Documentation

The Rose Plot mode is part of the Polar/Smith Object.  If using the 
OCX/VCL/VBX interfaces, you will need to call a few DLL calls to set 
certain properties because OCX/VCL/VBX interfaces will not be updated 
quite yet.

1)  The SmithChart property was changed from a Boolean to an Integer.  
Setting SmithChart = 2 initiates RosePlot Mode.  You will have to 
set this property like...

result = PEnset(Pepso1.hObject, 3800, 2)
'Note that 3800 equals PEP_bSMITHCHART

2)  The Subsets property needs to be set to the number of bins/classes.  
Not including the bin data for the inner circle of rose plot.

3)  The Points property is the number of rose peddles.  This will usually 
be set to 16 or 8 but any number is allowed.

4)  Next XData needs to be passed to describe degree location for each 
peddle.  This data will be identical for all subsets but needs to be set. 
By default, the XData-to-Degree has the following relationship...

Direction	XData
N		90.0 
NNE		67.5
NE		45.0
ENE		22.5
E		0.0
ESE		337.5
SE		315.0
SSE		292.5
S		270.0
SSW		247.5
SW		225.0
WSW		202.5
W		180.0
WNW		157.5
NW		135.0
NNW		112.5

Note than when Points is set to 16, peddles are 22.5 degrees apart.
Note that when Points is set to 8, peddles are 45 degrees apart.
Note the above relations are default, and there are new properties 
of Polar/Smith object that allow changing the location of 0 degrees and
the direction of positive rotation.  

5)  Next YData needs to be passed.  This can be percentage data or 
actual number of occurences.

6)  PointLabels need to be passed.  These labels need be defined 
starting with Degree 0 and going in positive rotation.  So, for the 
above default relationship, PointLabels should be assigned in the 
following order...

E  ENE  NE  NNE  N  NNW  NW  WNW  W  WSW  SW  SSW  S  SSE  SE  ESE

PointLabels is a new property for the Polar/Smith object so you will 
currently need to set this with either a PEvset or PEvsetcell DLL call.

' A VB example of this call would be...

Dim t as string
t = "2.5"
' setting the first pt label '0'
Call PEvsetcell(Pepso1, PEP_szaPOINTLABELS, 0, byVal t) 

7)  In order to get degree grid lines at the correct angles, you need 
to set...
ManualXAxisTicknLine = True
ManualXAxisLine = 22.5 if using 16 Points and 45.0 if using 8 Points.
ManualXAxisTick = 22.5 if using 16 Points and 45.0 if using 8 Points.

These properties are new to Polar/Smith so you will have to set these 
with DLL calls like...

Dim d As Double
d = 22.5
Call PEnset(Pepso1, 3644, 1) ' 3644 Equals PEP_bMANUALXAXISTICKNLINE
Call PEvset(Pepso1, 3645, d, 1) ' 3645 Equals PEP_fMANUALXAXISTICK
Call PEvset(Pepso1, 3646, d, 1) ' 3646 Equals PEP_fMANUALXAXISLINE

8)  SubsetLabels need to be set.  This should be the maximum value per 
bin but can actually be any text string.  Keep it short though.

9)  The inner/center circle can be labeled with a short string denoting 
"calm" conditions. For example "1.25|.01%" is a valid string.  The pipe 
symbol "|" if included will act as a carriage return to produce two lines 
of text.  Setting this property can be done only via a DLL call at this time.

... in VB use ...
temp$ = "1.25|.01%"
result = PEszset(Pepso1.hObject, 3410, ByVal temp$)

10)  You will likely want to get rid of default legend area since Rose Plot 
will have its own unique legends within graph.  To get rid of default 
legends use... Pepso1.SubsetsToLegend(0) = -1

11)  Finally and optionally, if ViewingStyle is non-zero (Monochrome) you 
can adjust the PEP_dwaSUBSETSHADES property in order to get alternating 
Black and White sections as follows....

... In VB use...
Redim NewSubsetShades(2) As Long
NewSubsetShades(0) = QBColor(0)  ' or RGB value for BLACK
NewSubsetShades(1) = QBColor(15)  ' or RGB value for WHITE
result = PEvset(Pepso1.hObject, 2195, NewSubsetShades(0), 2)
'Note that 2195 equals PEP_dwaSUBSETSHADES

The Following is VB4 example code.

Start a new project, place a Polar/Smith object onto form.  
Insert a module and insert file "VB4PEAPI.TXT" into this module.
Cut and paste the code into the project's Form_Load event.
You may need to fix where CRLFs have broken long lines of code.

Private Sub Form_Load()

Dim result As Long
result = PEnset(Pepso1, 3800, 2) 'Note 3800 equals PEP_bSMITHCHART

Pepso1.Subsets = 13
Pepso1.Points = 16
Pepso1.PrepareImages = True     ' No Flicker
Pepso1.SubsetsToLegend(0) = -1  ' Disable legends on top of graph
Pepso1.AllowZooming = True
Pepso1.CursorPromptTracking = True
Pepso1.CursorPromptStyle = 3

' ** Set some brighter colors ** '
Pepso1.SubsetColors(0) = QBColor(9)
Pepso1.SubsetColors(1) = QBColor(10)
Pepso1.SubsetColors(2) = QBColor(11)
Pepso1.SubsetColors(3) = QBColor(12)
Pepso1.SubsetColors(4) = QBColor(13)
Pepso1.SubsetColors(5) = QBColor(14)
Pepso1.SubsetColors(6) = QBColor(1)
Pepso1.SubsetColors(7) = QBColor(2)
Pepso1.SubsetColors(8) = QBColor(3)
Pepso1.SubsetColors(9) = QBColor(4)
Pepso1.SubsetColors(10) = QBColor(5)
Pepso1.SubsetColors(11) = QBColor(6)
Pepso1.SubsetColors(11) = QBColor(7)

Dim TempXData As Variant
TempXData = Array(90#, 67.5, 45#, 22.5, 0#, 337.5, 315#, 292.5, 270#, 247.5, 225#, 202.5, 
180#, 157.5, 135#, 112.5)

Dim TempYData(13) As Variant
TempYData(0) = Array(0.77, 0.75, 0.78, 0.88, 1.07, 0.98, 0.69, 0.51, 0.38, 0.5, 0.56, 
0.65, 0.72, 0.7, 0.62, 0.63)
TempYData(1) = Array(1.04, 1.72, 2.68, 4.76, 4.58, 2.59, 1.29, 0.99, 1.19, 1.77, 2.34, 
2.89, 2.05, 1.31, 1.16, 1.08)
TempYData(2) = Array(0.43, 1.05, 3.41, 7.4, 5.97, 1.48, 0.3, 0.39, 0.73, 1.68, 3.53, 2.73, 
1.24, 0.55, 0.25, 0.27)
TempYData(3) = Array(0.2, 1.07, 3.71, 4.49, 2.08, 0.23, 0.1, 0.11, 0.29, 1.16, 2.32, 1.26, 
0.54, 0.18, 0.09, 0.22)
TempYData(4) = Array(0.05, 0.48, 0.66, 1.31, 0.26, 0.01, 0.02, 0.04, 0.14, 0.33, 0.75, 
0.46, 0.18, 0.05, 0.07, 0.03)
TempYData(5) = Array(0.04, 0.1, 0.07, 0.02, 0.02, 0.02, 0.01, 0.01, 0.01, 0.05, 0.12, 
0.09, 0.04, 0.02, 0.03, 0.04)
TempYData(6) = Array(0.01, 0#, 0.01, 0.01, 0#, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.03, 
0#, 0#, 0.02, 0.01)
TempYData(7) = Array(0.01, 0#, 0.01, 0#, 0.01, 0#, 0.01, 0#, 0#, 0.01, 0#, 0.01, 0.02, 
0.01, 0.01, 0#)
TempYData(8) = Array(0.01, 0#, 0#, 0#, 0#, 0#, 0#, 0.01, 0#, 0#, 0#, 0.1, 0#, 0#, 0#, 0#)
TempYData(9) = Array(0#, 0#, 0.01, 0#, 0#, 0#, 0#, 0.01, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#)
TempYData(10) = Array(0#, 0#, 0.01, 0#, 0#, 0#, 0#, 0.01, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#)
TempYData(11) = Array(0#, 0#, 0.01, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#)
TempYData(12) = Array(0#, 0#, 0.01, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#)

Dim TempPtLabelData As Variant
TempPtLabelData = Array("E", "ENE", "NE", "NNE", "N", "NNW", "NW", "WNW", "W", "WSW", 
"SW", "SSW", "S", "SSE", "SE", "ESE")

Dim TempSubsetLabelData As Variant
TempSubsetLabelData = Array("3.75", "6.25", "8.75", "11.25", "13.75", "16.25", "18.75", 
"21.25", "23.75", "26.25", "28.75", "31.25", "33.75")

Dim s, p As Integer
Dim t As String
Dim d As Double

For s = 0 To 12
    Pepso1.SubsetLabels(s) = TempSubsetLabelData(s)
    For p = 0 To 15
        Pepso1.YData(s, p) = TempYData(s)(p)
        Pepso1.XData(s, p) = TempXData(p)
        If s = 0 Then
            t = TempPtLabelData(p)
            Call PEvsetcell(Pepso1, PEP_szaPOINTLABELS, p, ByVal t)
        End If
    Next p
Next s

d = 22.5 ' degrees between peddles
Call PEnset(Pepso1, 3644, 1) ' 3644 Equals PEP_bMANUALXAXISTICKNLINE
Call PEvset(Pepso1, 3645, d, 1) ' 3645 Equals PEP_fMANUALXAXISTICK
Call PEvset(Pepso1, 3646, d, 1) ' 3646 Equals PEP_fMANUALXAXISLINE

'** To set inner circle label ** '
t = "1.25|.01%"
Call PEszset(Pepso1, 3410, ByVal t)

'** Change PEP_dwaSUBSETSHADES to get better monochrome view **'
ReDim NewSubsetShades(2) As Long
NewSubsetShades(0) = QBColor(0)  ' or RGB value for BLACK
NewSubsetShades(1) = QBColor(15) ' or RGB value for BRIGHT WHITE
' You can comment out line below to see the difference this line makes.
Call PEvset(Pepso1, 2195, NewSubsetShades(0), 2) '2195 Equals PEP_dwaSUBSETSHADES

Pepso1.PEactions = 0




