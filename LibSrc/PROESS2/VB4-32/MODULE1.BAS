Attribute VB_Name = "Module1"
Option Explicit

'*****************************************************************'
'** ProEssentials Constants and Declares **'
'******************************************'

'// WINDOWS STRUCTURES //'
Type Rect
    left As Long
    top As Long
    right As Long
    bottom As Long
End Type

Type POINTSTRUCT
    X As Long
    Y As Long
End Type

Global Const PEAS_SUMPP = 51
Global Const PEAS_MINAP = 1
Global Const PEAS_MINPP = 52
Global Const PEAS_MAXAP = 2
Global Const PEAS_MAXPP = 53
Global Const PEAS_AVGAP = 3
Global Const PEAS_AVGPP = 54
Global Const PEAS_P1SDAP = 4
Global Const PEAS_P1SDPP = 55
Global Const PEAS_P2SDAP = 5
Global Const PEAS_P2SDPP = 56
Global Const PEAS_P3SDAP = 6
Global Const PEAS_P3SDPP = 57
Global Const PEAS_M1SDAP = 7
Global Const PEAS_M1SDPP = 58
Global Const PEAS_M2SDAP = 8
Global Const PEAS_M2SDPP = 59
Global Const PEAS_M3SDAP = 9
Global Const PEAS_M3SDPP = 60
Global Const PEAS_PARETO_ASC = 90
Global Const PEAS_PARETO_DEC = 91

Global Const PELT_THINSOLID = 0
Global Const PELT_DASH = 1
Global Const PELT_DOT = 2
Global Const PELT_DASHDOT = 3
Global Const PELT_DASHDOTDOT = 4
Global Const PELT_MEDIUMSOLID = 5
Global Const PELT_THICKSOLID = 6

Global Const PEPT_PLUS = 0
Global Const PEPT_CROSS = 1
Global Const PEPT_DOT = 2
Global Const PEPT_DOTSOLID = 3
Global Const PEPT_SQUARE = 4
Global Const PEPT_SQUARESOLID = 5
Global Const PEPT_DIAMOND = 6
Global Const PEPT_DIAMONDSOLID = 7
Global Const PEPT_UPTRIANGLE = 8
Global Const PEPT_UPTRIANGLESOLID = 9
Global Const PEPT_DOWNTRIANGLE = 10
Global Const PEPT_DOWNTRIANGLESOLID = 11

Global Const PEGAT_NOSYMBOL = 0
Global Const PEGAT_PLUS = 1
Global Const PEGAT_CROSS = 2
Global Const PEGAT_DOT = 3
Global Const PEGAT_DOTSOLID = 4
Global Const PEGAT_SQUARE = 5
Global Const PEGAT_SQUARESOLID = 6
Global Const PEGAT_DIAMOND = 7
Global Const PEGAT_DIAMONDSOLID = 8
Global Const PEGAT_UPTRIANGLE = 9
Global Const PEGAT_UPTRIANGLESOLID = 10
Global Const PEGAT_DOWNTRIANGLE = 11
Global Const PEGAT_DOWNTRIANGLESOLID = 12
Global Const PEGAT_SMALLPLUS = 13
Global Const PEGAT_SMALLCROSS = 14
Global Const PEGAT_SMALLDOT = 15
Global Const PEGAT_SMALLDOTSOLID = 16
Global Const PEGAT_SMALLSQUARE = 17
Global Const PEGAT_SMALLSQUARESOLID = 18
Global Const PEGAT_SMALLDIAMOND = 19
Global Const PEGAT_SMALLDIAMONDSOLID = 20
Global Const PEGAT_SMALLUPTRIANGLE = 21
Global Const PEGAT_SMALLUPTRIANGLESOLID = 22
Global Const PEGAT_SMALLDOWNTRIANGLE = 23
Global Const PEGAT_SMALLDOWNTRIANGLESOLID = 24
Global Const PEGAT_LARGEPLUS = 25
Global Const PEGAT_LARGECROSS = 26
Global Const PEGAT_LARGEDOT = 27
Global Const PEGAT_LARGEDOTSOLID = 28
Global Const PEGAT_LARGESQUARE = 29
Global Const PEGAT_LARGESQUARESOLID = 30
Global Const PEGAT_LARGEDIAMOND = 31
Global Const PEGAT_LARGEDIAMONDSOLID = 32
Global Const PEGAT_LARGEUPTRIANGLE = 33
Global Const PEGAT_LARGEUPTRIANGLESOLID = 34
Global Const PEGAT_LARGEDOWNTRIANGLE = 35
Global Const PEGAT_LARGEDOWNTRIANGLESOLID = 36
Global Const PEGAT_POINTER = 37
Global Const PEGAT_THINSOLIDLINE = 38
Global Const PEGAT_DASHLINE = 39
Global Const PEGAT_DOTLINE = 40
Global Const PEGAT_DASHDOTLINE = 41
Global Const PEGAT_DASHDOTDOTLINE = 42
Global Const PEGAT_MEDIUMSOLIDLINE = 43
Global Const PEGAT_THICKSOLIDLINE = 44
Global Const PEGAT_LINECONTINUE = 45
Global Const PEGAT_LINEEND = 46

Global Const PEP_faAPPENDYDATA = 3276
Global Const PEP_szaAPPENDPOINTLABELDATA = 3277
Global Const PEP_faAPPENDXDATAII = 3659
Global Const PEP_bAUTOIMAGERESET = 2615
Global Const PEP_ptLASTMOUSEMOVE = 2637

Declare Function PEvset Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByRef lpvData As Any, ByVal nItems&) As Long
Declare Function PEvget Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByRef lpvDest As Any) As Long
Declare Function PEvsetcell Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByVal nCell&, lpvData As Any) As Long
Declare Function PEvgetcell Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByVal nCell&, lpvDest As Any) As Long
Declare Function PEszset Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByVal szData$) As Long
Declare Function PEszget Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByVal szData$) As Long
Declare Function PEnset Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByVal nData&) As Long
Declare Function PEnget Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&) As Long
Declare Function PElset Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&, ByVal nData&) As Long
Declare Function PElget Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nProperty&) As Long
Declare Function PEcreate Lib "PEGRAP32.DLL" (ByVal nObjectType&, ByVal dwStyle&, lpRect As Rect, ByVal hParent&, ByVal nID&) As Long
Declare Function PEdestroy Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PEload Lib "PEGRAP32.DLL" (ByVal hObject&, lphGlbl As Any) As Long
Declare Function PEstore Lib "PEGRAP32.DLL" (ByVal hObject&, lphGlbl As Any, lpdwSize As Any) As Long
Declare Function PEgetmeta Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PEresetimage Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal nLength&, ByVal nHeight&) As Long
Declare Function PElaunchcustomize Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PElaunchexport Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PElaunchmaximize Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PElaunchtextexport Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal bToFile&, ByVal lpszFilename$) As Long
Declare Function PElaunchprintdialog Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal bFullPage&, lpPoint As POINTSTRUCT) As Long
Declare Function PElaunchcolordialog Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PElaunchfontdialog Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PElaunchpopupmenu Lib "PEGRAP32.DLL" (ByVal hObject&, lpPoint As POINTSTRUCT) As Long
Declare Function PEreinitialize Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PEreinitializecustoms Lib "PEGRAP32.DLL" (ByVal hObject&) As Long
Declare Function PEgethelpcontext Lib "PEGRAP32.DLL" (ByVal HWND&) As Long
Declare Function PEcopymetatoclipboard Lib "PEGRAP32.DLL" (ByVal hObject&, lpPoint As POINTSTRUCT) As Long
Declare Function PEcopymetatofile Lib "PEGRAP32.DLL" (ByVal hObject&, lpPoint As POINTSTRUCT, ByVal lpszFilename$) As Long
Declare Function PEcopybitmaptoclipboard Lib "PEGRAP32.DLL" (ByVal hObject&, lpPoint As POINTSTRUCT) As Long
Declare Function PEcopybitmaptofile Lib "PEGRAP32.DLL" (ByVal hObject&, lpPoint As POINTSTRUCT, ByVal lpszFilename$) As Long
Declare Function PEcopyoletoclipboard Lib "PEGRAP32.DLL" (ByVal hObject&, lpPoint As POINTSTRUCT) As Long
Declare Function PEprintgraph Lib "PEGRAP32.DLL" (ByVal hObject&, ByVal hDC&, ByVal nWidth&, ByVal nHeight&, ByVal nOrient&) As Long
Declare Function PEconvpixeltograph Lib "PEGRAP32.DLL" (ByVal hObject&, ByRef nAxis&, ByRef nX&, ByRef nY&, ByRef fX#, ByRef fY#, ByVal bRight&, ByVal bTop&, ByVal bVV&) As Long

