VERSION 4.00
Begin VB.Form Form5 
   Caption         =   "Form5"
   ClientHeight    =   4776
   ClientLeft      =   912
   ClientTop       =   1380
   ClientWidth     =   7164
   Height          =   5148
   Left            =   864
   LinkTopic       =   "Form5"
   ScaleHeight     =   4776
   ScaleWidth      =   7164
   Top             =   1056
   Width           =   7260
   Begin PepcoLib.Pepco Pepco1 
      Height          =   4692
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6972
      _version        =   65536
      _extentx        =   12298
      _extenty        =   8276
      _stockprops     =   96
      _allprops       =   "Form5.frx":0000
   End
End
Attribute VB_Name = "Form5"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

Dim s%, p%

Pepco1.PrepareImages = True

Pepco1.Subsets = 5
Pepco1.Points = 10
For s% = 0 To 4
    For p% = 0 To 9
        Pepco1.XData(s%, p%) = 5 + (Rnd * 95)
    Next p%
Next s%

Pepco1.SubsetLabels(0) = "Apples"
Pepco1.SubsetLabels(1) = "Oranges"
Pepco1.SubsetLabels(2) = "Pears"
Pepco1.SubsetLabels(3) = "Plums"
Pepco1.SubsetLabels(4) = "Peaches"

Pepco1.PointLabels(0) = "Texas"
Pepco1.PointLabels(1) = "Oklahoma"
Pepco1.PointLabels(2) = "Kansas"
Pepco1.PointLabels(3) = "New Mexico"
Pepco1.PointLabels(4) = "Colorado"
Pepco1.PointLabels(5) = "Wyoming"
Pepco1.PointLabels(6) = "Utah"
Pepco1.PointLabels(7) = "Nebraska"
Pepco1.PointLabels(8) = "Ohio"
Pepco1.PointLabels(9) = "Iowa"

Pepco1.SubsetColors(0) = QBColor(9)
Pepco1.SubsetColors(1) = QBColor(10)
Pepco1.SubsetColors(2) = QBColor(11)
Pepco1.SubsetColors(3) = QBColor(12)
Pepco1.SubsetColors(4) = QBColor(13)
Pepco1.SubsetColors(5) = QBColor(14)
Pepco1.SubsetColors(6) = QBColor(15)
Pepco1.SubsetColors(7) = QBColor(8)
Pepco1.SubsetColors(8) = QBColor(9)
Pepco1.SubsetColors(9) = QBColor(10)
Pepco1.SubsetColors(10) = QBColor(11)
Pepco1.SubsetColors(11) = QBColor(12)
Pepco1.SubsetColors(12) = QBColor(13)

Pepco1.MainTitle = "Produce by State"
Pepco1.SubTitle = ""
Pepco1.FocalRect = False

Pepco1.DataPrecision = 1
Pepco1.FontSize = 0
Pepco1.GroupingPercent = 3

Pepco1.PEactions = 0

End Sub


