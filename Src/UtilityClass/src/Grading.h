// Grading.h: interface for the CGrading class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_)
#define AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct GradeStep{
	long  lGradeItem;
	float fMin;
	float fMax;
	CString strCode;
} GRADE_STEP;

#include <afxtempl.h>

class CGrading  
{
public:
	long GetGradeStepItem(int iPos);
//	void SetGradeItem(int nItem)		{	m_nGradeItem = 	nItem;}
//	int GetGradeItem()		{	return m_nGradeItem;	}
	BOOL SetGradingData(CGrading &grading);
	GRADE_STEP GetStepData(int nIndex);
	int GetGradeStepSize();
	BOOL ClearStep();
	BOOL AddGradeStep(long item, float fMin, float fMax, CString Code);
	CString GetGradeCode(float data);
	CGrading();
	virtual ~CGrading();
//	void operator = (const CGrading grading); 

private:
	CArray<GRADE_STEP ,GRADE_STEP> m_ptArray;
//	int m_nGradeItem;

};

#endif // !defined(AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_)
