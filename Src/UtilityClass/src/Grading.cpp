// Grading.cpp: implementation of the CGrading class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Grading.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGrading::CGrading()
{

}

CGrading::~CGrading()
{
	m_ptArray.RemoveAll();
}

CString CGrading::GetGradeCode(float fData)
{
	for(int i=0; i<m_ptArray.GetSize(); i++)
	{
		GRADE_STEP& stepData = m_ptArray.ElementAt(i);

		if(stepData.fMin <= fData && stepData.fMax > fData)
		{
			return stepData.strCode;
		}
	}
	return "";
}

BOOL CGrading::AddGradeStep(long item, float fMin, float fMax, CString Code)
{
	if(fMin > fMax || Code.IsEmpty())	return FALSE;
	
	GRADE_STEP step;
	step.lGradeItem = item;
	step.fMin = fMin;
	step.fMax = fMax;
	step.strCode = Code;
	m_ptArray.Add(step);

	return FALSE;
}

BOOL CGrading::ClearStep()
{
	m_ptArray.RemoveAll();
//	m_nGradeItem = 0;
	return TRUE;
}
/*
void CGrading::operator=(const CGrading grading) 
{

}
*/
int CGrading::GetGradeStepSize()
{
	return m_ptArray.GetSize();
}

GRADE_STEP CGrading::GetStepData(int nIndex)
{
	ASSERT(nIndex >= 0 && nIndex <GetGradeStepSize());
	
	GRADE_STEP stepData = m_ptArray.GetAt(nIndex);
	return stepData;
}

BOOL CGrading::SetGradingData(CGrading &grading)
{
	ClearStep();
	
	GRADE_STEP step;
	for(int i = 0; i<grading.GetGradeStepSize(); i++)
	{
		step = grading.GetStepData(i);
		m_ptArray.Add(step);
	}
	return TRUE;
}

long CGrading::GetGradeStepItem(int iPos)
{
	if (m_ptArray.GetSize() < 1) return 0;
	else
		return m_ptArray.GetAt(iPos).lGradeItem;

}
