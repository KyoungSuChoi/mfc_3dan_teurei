// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "CTSGraphAnal.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "CTSGraphAnalView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_DATA_SHEET, OnDataSheet)
	ON_UPDATE_COMMAND_UI(ID_DATA_SHEET, OnUpdateDataSheet)
	ON_COMMAND(ID_LINE_TREE, OnLineTree)
	ON_UPDATE_COMMAND_UI(ID_LINE_TREE, OnUpdateLineTree)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
	m_pmyImageList = NULL;
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	//if( !CMDIChildWnd::PreCreateWindow(cs) )
	//	return FALSE;

	//cs.cx = 760;
	//cs.cy = 480;
	//cs.cx = 1024;
	//cs.cy = 768;

	//return TRUE;
	cs.style &= ~WS_THICKFRAME;
	return CFrameWnd::PreCreateWindow(cs);
}

void CChildFrame::OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt)
{
	if(bActive)             ((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::DATA_TOOLBAR);
	if(pActivateWnd==NULL) 	((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::MAIN_TOOLBAR);

	CMDIChildWnd::OnUpdateFrameMenu(bActive, pActivateWnd, hMenuAlt);
}


/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers


int CChildFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	EnableDocking(CBRS_ALIGN_ANY);

#ifdef _SCB_REPLACE_MINIFRAME
    m_pFloatingFrameClass = RUNTIME_CLASS(CSCBMiniDockFrameWnd);
#endif //_SCB_REPLACE_MINIFRAME

	CRect rBar;
/*	if (!m_wndDialogBar.Create(this,
		                       IDD_PLANE_SETTING_DIALOGBAR,
		                       CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY,
							   IDD_PLANE_SETTING_DIALOGBAR))
	{
		TRACE0("Failed to create dialog bar m_wndDialogBar\n");
		return -1;		// fail to create
	}

	m_wndDialogBar.EnableDocking(CBRS_ALIGN_LEFT|CBRS_ALIGN_RIGHT);
	DockControlBar(&m_wndDialogBar);

	RecalcLayout();
	m_wndDialogBar.GetWindowRect(rBar);
*/
	
	if (!m_wndLineListBar.Create("Line", this, 2003))
	{
		TRACE0("Failed to create dialog bar m_wndLineListBar\n");
		return -1;		// fail to create
	}
	m_wndLineListBar.SetBarStyle(m_wndLineListBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndLineListBar.EnableDocking(CBRS_ALIGN_ANY);
	rBar.OffsetRect(0, 5);
	DockControlBar(&m_wndLineListBar, AFX_IDW_DOCKBAR_LEFT, rBar);

/*	RecalcLayout();
	m_wndQueryBar.GetWindowRect(rBar);
	
	if (!m_wndGraphSetBar.Create("Setting", this, 2004))
	{
		TRACE0("Failed to create dialog bar m_wndDialogBar\n");
		return -1;		// fail to create
	}
	m_wndGraphSetBar.SetBarStyle(m_wndGraphSetBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndGraphSetBar.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndGraphSetBar, AFX_IDW_DOCKBAR_LEFT, rBar);
*/
	//Bottom Grid Bar
	if (!m_wndGridBar.Create("DataSheet", this, 2002))
	{
		TRACE0("Failed to create dialog bar m_wndGridBar\n");
		return -1;		// fail to create
	}
	m_wndGridBar.SetBarStyle(m_wndGridBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndGridBar.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndGridBar, AFX_IDW_DOCKBAR_BOTTOM);

	CString sProfile = _T("BarState");
	if (VerifyBarState(sProfile))
	{
		CSizingControlBar::GlobalLoadState(this, sProfile);
		LoadBarState(sProfile);
	}
	
	CTreeCtrl *pTree = GetTreeWnd();

	m_pmyImageList = new CImageList;
	ASSERT(m_pmyImageList);
	m_pmyImageList->Create(IDB_TEST_LIST_TREE_BITMAP, 18, 6, RGB(255,255,255));

	pTree->SetImageList(m_pmyImageList, TVSIL_NORMAL);

	return 0;
}

CMyGridWnd * CChildFrame::GetGridWnd()
{
	return (CMyGridWnd *)m_wndGridBar.GetClientWnd();
}

CTreeCtrl * CChildFrame::GetTreeWnd()
{
	return (CTreeCtrl *)m_wndLineListBar.GetClientWnd();
}

void CChildFrame::OnDataSheet() 
{
	// TODO: Add your command handler code here
	BOOL bShow = m_wndGridBar.IsVisible();
	if(!bShow && ((CCTSGraphAnalDoc *)GetActiveDocument())->m_bLoadedSheet == FALSE)
	{
		m_wndGridBar.DisplaySheetData(&((CCTSGraphAnalDoc *)GetActiveDocument())->m_Data);
		((CCTSGraphAnalDoc *)GetActiveDocument())->m_bLoadedSheet = TRUE;
	}
	ShowControlBar(&m_wndGridBar, !bShow, FALSE);
}

void CChildFrame::OnUpdateDataSheet(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_wndGridBar.IsVisible());	
}

void CChildFrame::OnLineTree() 
{
	// TODO: Add your command handler code here
	ShowControlBar(&m_wndLineListBar, !m_wndLineListBar.IsVisible(), FALSE);	
}

void CChildFrame::OnUpdateLineTree(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_wndLineListBar.IsVisible());		
}

BOOL CChildFrame::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	CString sProfile = _T("BarState");
	CSizingControlBar::GlobalSaveState(this, sProfile);
	SaveBarState(sProfile);

	if(m_pmyImageList)
	{
		delete m_pmyImageList;
		m_pmyImageList = NULL;
	}
	
	return CMDIChildWnd::DestroyWindow();
}

BOOL CChildFrame::VerifyBarState(LPCTSTR lpszProfileName)
{
    CDockState state;
    state.LoadState(lpszProfileName);

    for (int i = 0; i < state.m_arrBarInfo.GetSize(); i++)
    {
        CControlBarInfo* pInfo = (CControlBarInfo*)state.m_arrBarInfo[i];
        ASSERT(pInfo != NULL);
        int nDockedCount = pInfo->m_arrBarID.GetSize();
        if (nDockedCount > 0)
        {
            // dockbar
            for (int j = 0; j < nDockedCount; j++)
            {
                UINT nID = (UINT) pInfo->m_arrBarID[j];
                if (nID == 0) continue; // row separator
                if (nID > 0xFFFF)
                    nID &= 0xFFFF; // placeholder - get the ID
                if (GetControlBar(nID) == NULL)
                    return FALSE;
            }
        }
        
        if (!pInfo->m_bFloating) // floating dockbars can be created later
            if (GetControlBar(pInfo->m_nBarID) == NULL)
                return FALSE; // invalid bar ID
    }

    return TRUE;
}


void CChildFrame::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd = GetFocus();
	if(GetGridWnd() == pWnd)
	{
		GetGridWnd()->Copy();
	}	
	else
	{
		((CCTSGraphAnalView *)pWnd)->OnToolsScreencapture();
	}
}
