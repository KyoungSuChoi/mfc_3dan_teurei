/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: MainFrm.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CMainFrame class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__13FE5EA8_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_MAINFRM_H__13FE5EA8_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

// #define

// Class definition
//---------------------------------------------------------------------------
//	Class Name: CMainFrame
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:
	CBitmap m_bmToolbarHi;
// Operations
public:
	void LoadToolBar(int no);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndMainToolBar;	
	CToolBar    m_wndDataToolBar;
	CToolBar    m_wndGraphToolBar;	

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnClose();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	//}}AFX_MSG	

	DECLARE_MESSAGE_MAP()

//
public:
	CString m_strCopyMsgString;	
	enum{
		MAIN_TOOLBAR    = 0,		
		DATA_TOOLBAR    = 2,
		GRAPH_TOOLBAR	= 3
	};
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__13FE5EA8_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
