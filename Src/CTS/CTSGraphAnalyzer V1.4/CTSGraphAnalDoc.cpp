// CTSGraphAnalDoc.cpp : implementation of the CCTSGraphAnalDoc class
//

#include "stdafx.h"
#include "CTSGraphAnal.h"

#include "CTSGraphAnalDoc.h"
#include "ChildFrm.h"
#include "PlaneSettingDlgBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalDoc

IMPLEMENT_DYNCREATE(CCTSGraphAnalDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSGraphAnalDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSGraphAnalDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalDoc construction/destruction

CCTSGraphAnalDoc::CCTSGraphAnalDoc()
{
	// TODO: add one-time construction code here
	m_iCurSelPlaneIndex = 0;
	m_bStairLineType = FALSE;
}

CCTSGraphAnalDoc::~CCTSGraphAnalDoc()
{
}

#include "MainFrm.h"

BOOL CCTSGraphAnalDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

//	POSITION pos = GetFirstDocTemplatePosition();
//	CMultiDocTemplate *pTempate;
//	pTempate = (CMultiDocTemplate *)GetNextDocTemplate(pos);

//	CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
//	if(pMainFrm == NULL)	return FALSE;
//	CMonitorFrame *pMonFrm = (CMonitorFrame *)pMainFrm->GetActiveFrame();
//	if(pMonFrm == NULL)	return FALSE;
//	CMonitorDoc *pMonDoc = (CMonitorDoc *)pMonFrm->GetActiveDocument();
//	if(pMonDoc == NULL)	return FALSE;

//	CString strName, strTemp;
//	CStringList *pList = pMonDoc->GetSelFileList();
//	POSITION pos = pList->GetHeadPosition();
//	
//	while(pos)
//	{
//		strTemp.Format("%s\\%s?", pMonDoc->GetDataPath(), pList->GetNext(pos));
//		strName += strTemp;
//	}
	if( !m_Data.QueryData(NULL))				
		return FALSE;
	
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalDoc serialization

void CCTSGraphAnalDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalDoc diagnostics

#ifdef _DEBUG
void CCTSGraphAnalDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSGraphAnalDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalDoc commands

BOOL CCTSGraphAnalDoc::RequeryDataQuery() 
{
	if(m_Data.QueryData(NULL))
	{
//		POSITION pos = GetFirstViewPosition();
//		((CChildFrame*)(GetNextView(pos)->GetParentFrame()))->m_wndDialogBar.AddPlaneIntoComboBox();
		UpdateAllViews(NULL);
		return TRUE;
	}
	return FALSE;
}

void CCTSGraphAnalDoc::DrawView(CDC* pDC, CRect ClientRect)
{
	m_Data.DrawPlaneOnView(pDC, ClientRect, m_bStairLineType);
}

BOOL CCTSGraphAnalDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	return m_Data.QueryData(((CMainFrame *)AfxGetMainWnd())->m_strCopyMsgString);
}

void CCTSGraphAnalDoc::ReloadData()
{
	m_Data.ReLoadData();
	UpdateAllViews(NULL);
	return;
}
