// gridbar.cpp : implementation file
//

#include "stdafx.h"
#include "gridbar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGridBar

CGridBar::CGridBar()
{
}

CGridBar::~CGridBar()
{
}


BEGIN_MESSAGE_MAP(CGridBar, baseCMyBar)
	//{{AFX_MSG_MAP(CGridBar)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CGridBar message handlers

int CGridBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (baseCMyBar::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetSCBStyle(GetSCBStyle() | SCBS_SIZECHILD);

/*	if (!m_wndChild.Create(WS_CHILD|WS_VISIBLE|
			ES_MULTILINE|ES_WANTRETURN|ES_AUTOVSCROLL,
		CRect(0,0,0,0), this, 123))
		return -1;

	m_wndChild.ModifyStyleEx(0, WS_EX_CLIENTEDGE);
*/
	if (!m_GridWnd.Create(WS_CHILD|WS_VISIBLE|ES_AUTOVSCROLL|ES_AUTOHSCROLL,
		CRect(0,0,0,0), this, 123))
		return -1;


	m_GridWnd.ModifyStyleEx(0, WS_EX_CLIENTEDGE);

	m_GridWnd.SetScrollBarMode(SB_BOTH, gxnEnabled);	//Only Vertial Scroll Bar
	m_GridWnd.Initialize();
//	m_GridWnd.SetRowCount(100);
//	m_GridWnd.SetColCount(10);

	// older versions of Windows* (NT 3.51 for instance)
	// fail with DEFAULT_GUI_FONT
	if (!m_font.CreateStockObject(DEFAULT_GUI_FONT))
		if (!m_font.CreatePointFont(80, "MS Sans Serif"))
			return -1;

	m_GridWnd.SetFont(&m_font);
	m_GridWnd.GetParam()->EnableUndo(FALSE);
	return 0;
}

CWnd * CGridBar::GetClientWnd()
{
	return &m_GridWnd;
}

void CGridBar::SelectDataRange(int nStartIndex, int nEndIndex, int nColumn)
{
	int nRowCount = m_GridWnd.GetRowCount();

//	int nColCount = m_GridWnd.GetColCount();	
//	if(nColumn >= nColCount)	return;

	if(nStartIndex < 0)			nStartIndex	 = 0;
	if(nEndIndex >= nRowCount)	nEndIndex = nRowCount-1;

	//Reset Selection
//	if(nColumn < 1)
//	{
		m_GridWnd.SelectRange(CGXRange().SetTable(), FALSE);
//	}
//	else
//	{
//		m_GridWnd.SelectRange(CGXRange().SetCols(nColumn), FALSE);
//	}

	//Select Range
//	if(nColumn < 1)
//	{
		if(nStartIndex < nEndIndex)
			m_GridWnd.SelectRange(CGXRange().SetCells(nStartIndex+1, 1, nEndIndex+1, m_GridWnd.GetColCount()));
		else return;
/*	}
	else
	{
		m_GridWnd.SelectRange(CGXRange().SetCells(nStartIndex+1, nColumn+1, nEndIndex+1, nColumn+1));

		//x축 재선택
		m_GridWnd.SelectRange(CGXRange().SetCols(1), FALSE);
		m_GridWnd.SelectRange(CGXRange().SetCells(nStartIndex+1, 1, nEndIndex+1, 1));
	}
*/
	//Movie Current Cell in view
	m_GridWnd.ScrollCellInView(nStartIndex+1, 1);
}

void CGridBar::DisplaySheetData(CData *pData)
{
	ASSERT(pData);

	int pcount = pData->GetPlaneCount();

	CPlane *pPlane;
	CLine *pLine;
	fltPoint fltData;
	int nTotColumn = 1;	//X Axis value
	int nTotRow = 0;

	for(int i =0; i<pcount; i++)
	{
		pPlane = pData->GetPlaneAt(i);
		if(pPlane)
		{
			//라인의 총수를 구한다.
			nTotColumn += pPlane->GetLineCount();
			
			POSITION linePos = pPlane->GetLineHead();
			while(linePos)
			{
				//라인중에서 최대 data수를 구한다.
				pLine = pPlane->GetNextLine(linePos);
				if(pLine->GetDataCount() > nTotRow)
				{
					nTotRow = pLine->GetDataCount();
				}
			}
		}
	}

	// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
	/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
	/*♠*/ // CProgressCtrl ProgressCtrl;
	/*♠*/ CRect rect;
	/*♠*/ pStatusBar->GetItemRect(0,rect);
	/*♠*/ rect.left = rect.Width()/2;
	/*♠*/ // ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
	/*♠*/ // ProgressCtrl.SetRange(0, nTotColumn);
	/*♠*/ int nPos = 0;
	/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("sheet loading...");


	DWORD dwStart = GetTickCount();
 
	m_GridWnd.SetRowCount(nTotRow);
	m_GridWnd.SetColCount(nTotColumn);
	// m_GridWnd.SetColCount(4);

	CString strTemp, strPath;
	CAxis *pXAxis, *pYAxis;
	POSITION linePos, dataPos;
	int nRow = 1;
	int nCol = 2;
	char szYBuff[512];	//속도 향상을 위해 CString안쓰고 배열 사용 
	char szXBuff[256];	//속도 향상을 위해 CString안쓰고 배열 사용 

	for(i =0; i<pcount && pcount <65536; i++)
	{
		pPlane = pData->GetPlaneAt(i);
		if(pPlane)
		{
			pXAxis = pPlane->GetXAxis();
			sprintf(szXBuff, "%s(%s)", pXAxis->GetPropertyTitle().Left(3), pXAxis->GetUnitNotation()); 
			pYAxis = pPlane->GetYAxis();

			linePos = pPlane->GetLineHead();
			while(linePos)
			{
				pLine = pPlane->GetNextLine(linePos);				
				sprintf(szYBuff, "%s_%s(%s)", pLine->GetName(), pYAxis->GetPropertyTitle().Left(3), pYAxis->GetUnitNotation()); 
				
				dataPos = pLine->GetDataHead();
				nRow = 1;

				while(dataPos)
				{
					fltData = pLine->GetNextData(dataPos);
					//Column Heaeder 표시
					if(nRow == 1)
					{
						if(nCol == 2)
						{
							m_GridWnd.SetValueRange(CGXRange(0 , 1), szXBuff);
						}

						m_GridWnd.SetValueRange(CGXRange(0 , nCol), szYBuff);
					}
					//재일 첫번째 Column에 X축의 data를 표시한다.
					if(nCol == 2)
					{
						m_GridWnd.SetValueRange(CGXRange(nRow , 1), pXAxis->ConvertUnit(fltData.x));
					}
					//2번째 부터는 Y축 data를 하나씩 표시한다.
					m_GridWnd.SetValueRange(CGXRange(nRow++ , nCol), pYAxis->ConvertUnit(fltData.y));
				}

				nCol++;

				// ProgressCtrl.SetPos(nCol-1);
			}
		}
	}

	dwStart =  GetTickCount() - dwStart;
	strTemp.Format("Loading sheet time : %d", dwStart);
	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strTemp);

	SelectDataRange(0, nTotRow-1);
}

UINT CGridBar::Fun_GetTotalRow()
{
	return m_GridWnd.GetRowCount();
}

UINT CGridBar::Fun_GetTotalCol()
{
	return m_GridWnd.GetColCount();
}

CString CGridBar::Fun_GetLineString( UINT iLine, UINT iDataCnt )
{
	if( iDataCnt < 1 || iLine < 0 )
	{
		return "";
	}

	CString strTemp, strSaveData;
	strSaveData.Empty();

	int i = 0;
	int j = 0;
	int nToCnt = 0;
	bool bFinish = false;

	if( iDataCnt > 1 )
	{	
		j = (m_GridWnd.GetColCount() / iDataCnt);

		// 0:Line 수
		strTemp.Format("%s,",m_GridWnd.GetValueRowCol(iLine, nToCnt));
		strSaveData += strTemp;		
		nToCnt++;

		// 1:가로축 데이터
		strTemp.Format("%s,",m_GridWnd.GetValueRowCol(iLine, nToCnt));
		strSaveData += strTemp;		
		nToCnt++;
		
		while( !bFinish )
		{
			for ( i = nToCnt; i < m_GridWnd.GetColCount()+1; i = i+j)
			{
				strTemp.Format("%s,",m_GridWnd.GetValueRowCol(iLine, i));
				strSaveData += strTemp;
			}
			
			if( nToCnt == j+1 )
			{
				bFinish = true;
			}
			else
			{
				nToCnt++;
			}
		}			
	}
	else
	{
		for (i=0; i < m_GridWnd.GetColCount()+1; i++)
		{
			strTemp.Format("%s,",m_GridWnd.GetValueRowCol(iLine, i));
			strSaveData += strTemp;
		}
	}
	
	strSaveData += "\n";
	return strSaveData;
}