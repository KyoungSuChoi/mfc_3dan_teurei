/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: CTSGraphAnal.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CCTSGraphAnalApp class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// CTSGraphAnal.h : main header file for the CTSGraphAnal application
//

#if !defined(AFX_CTSGraphAnal_H__13FE5EA4_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CTSGraphAnal_H__13FE5EA4_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

// #include

#include "resource.h"       // main symbols
// #define

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalApp:
// See CTSGraphAnal.cpp for the implementation of this class
//

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CCTSGraphAnalApp
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CCTSGraphAnalApp : public CWinApp
{
public:

	CString *TEXT_LANG;

	bool LanguageinitMonConfig();

	CMultiDocTemplate* m_pDataTemplate;
	CMultiDocTemplate* m_pGraphTemplate;
	CCTSGraphAnalApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSGraphAnalApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCTSGraphAnalApp)
	afx_msg void OnAppAbout();
	afx_msg void OnShowMonitor();
	afx_msg void OnShowData();
	afx_msg void OnShowGraph();	
	afx_msg void OnUpdateShowHelp(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowMonitor(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowGraph(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
private:	
	CMultiDocTemplate* m_pHelpTemplate;
	BOOL               m_bInitial;

// Operations
	public:
		BOOL IsInitialState() { return m_bInitial;};
		void ConfirmInitialOperation() { m_bInitial=FALSE;};

// Static
public:
	BOOL ExecuteExcel(CString strFileName);
	CString GetExcelPath();
	void SetResultDataPath(CString strPath);
	char m_szCurrentDirectory [128];
	void InitDBPath();
	static CString m_strOriginFolderPath;
	static void SetOriginFolderPath(LPCTSTR str) { m_strOriginFolderPath=str;};
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSGraphAnal_H__13FE5EA4_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
