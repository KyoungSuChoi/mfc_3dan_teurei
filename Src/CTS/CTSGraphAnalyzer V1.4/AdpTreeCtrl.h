#if !defined(AFX_ADPTREECTRL_H__070E3AF0_320F_4B9D_AC6E_5082689DB856__INCLUDED_)
#define AFX_ADPTREECTRL_H__070E3AF0_320F_4B9D_AC6E_5082689DB856__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AdpTreeCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAdpTreeCtrl window

class CAdpTreeCtrl : public CTreeCtrl
{
// Construction
public:
	CAdpTreeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdpTreeCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CAdpTreeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CAdpTreeCtrl)
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADPTREECTRL_H__070E3AF0_320F_4B9D_AC6E_5082689DB856__INCLUDED_)
