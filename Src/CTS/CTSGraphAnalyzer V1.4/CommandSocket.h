#if !defined(AFX_COMMANDSOCKET_H__541160F5_B3E5_4D49_8CBD_AC7ED7C706F9__INCLUDED_)
#define AFX_COMMANDSOCKET_H__541160F5_B3E5_4D49_8CBD_AC7ED7C706F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommandSocket.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCommandSocket command target
#include "MonitorDoc.h"
class CCommandSocket : public CAsyncSocket
{
// Attributes
public:
	int m_nOffset;
	int m_nSockStatus;
	char m_pBuffer[1024];

	UINT MONITORING_PORT;
	CString MONITORING_IP;
	BYTE m_byUnitNo;
	BYTE m_byCurrentModuleID;
	bool m_bServer;
	CString m_strProgressFileName;
	CMonitorDoc* m_pDoc;
	CPtrArray m_aPacket;
	HANDLE m_hSendComplete;
// Operations
public:
	CCommandSocket();
	CCommandSocket(BYTE byUnitNo, bool bServer = false);
	virtual ~CCommandSocket();

// Overrides
public:
	bool ParsingCommand();
	void ReceiveModuleNo();
	UINT ReadAck();
	void SendCommand(BYTE byCmd, LPEP_CMD_PACKET pCmd, char* szTestName);
	void SendCommand(BYTE byCmd, BYTE ModuleNo, BYTE byChannelNum, DWORD dwChannel, char* szTestName);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommandSocket)
	public:
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CCommandSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMANDSOCKET_H__541160F5_B3E5_4D49_8CBD_AC7ED7C706F9__INCLUDED_)
