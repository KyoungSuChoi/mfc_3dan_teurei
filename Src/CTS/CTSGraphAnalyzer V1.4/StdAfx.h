// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__13FE5EA6_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_STDAFX_H__13FE5EA6_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#include <afxtempl.h>
#include <afxdao.h>
//#include <secall.h>
#include <afxsock.h>
#endif // _AFX_NO_AFXCMN_SUPPORT


#include "../../../Include/PSCommonAll.h"
#include "../../../Include/PSDataAll.h"
#include "../../../Include/IniParser.h"

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFGraphComD.lib ")
#pragma message("Automatically linking with BFGraphComD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFGraphCom.lib")
#pragma message("Automatically linking with BFGraphCom.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../Lib/BFGraphDataD.lib ")
#pragma message("Automatically linking with BFGraphDataD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../Lib/BFGraphData.lib")
#pragma message("Automatically linking with BFGraphData.lib By K.B.H")
#endif	//_DEBUG

extern int g_nLanguage;			
extern CString g_strLangPath;

#define REG_CONFIG_SECTION	"Config"

#include <grid/gxall.h>

#define _SCB_REPLACE_MINIFRAME
#include "sizecbar.h"
#include "scbarg.h"
#include "scbarcf.h"
#define baseCMyBar CSizingControlBarCF


//#include "../Include/EPMacro.h"
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__13FE5EA6_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
