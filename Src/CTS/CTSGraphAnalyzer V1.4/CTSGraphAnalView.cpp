// CTSGraphAnalView.cpp : implementation of the CCTSGraphAnalView class
//

#include "stdafx.h"
#include "CTSGraphAnal.h"

#include "ChildFrm.h"
#include "CTSGraphAnalView.h"

#include "GraphSetDlg.h"
#include "PrntScreen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalView

IMPLEMENT_DYNCREATE(CCTSGraphAnalView, CView)

BEGIN_MESSAGE_MAP(CCTSGraphAnalView, CView)
	//{{AFX_MSG_MAP(CCTSGraphAnalView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_COMMAND(IDM_TOOLS_ZOOM_IN, OnToolZoomIn)
	ON_UPDATE_COMMAND_UI(IDM_TOOLS_ZOOM_IN, OnUpdateToolZoomIn)
	ON_COMMAND(IDM_TOOLS_ZOOM_OUT, OnToolZoomOut)
	ON_UPDATE_COMMAND_UI(IDM_TOOLS_ZOOM_OUT, OnUpdateToolZoomOut)
	ON_COMMAND(IDM_TOOLS_ZOOM_OFF, OnToolZoomOff)
	ON_UPDATE_COMMAND_UI(IDM_TOOLS_ZOOM_OFF, OnUpdateToolZoomOff)
	ON_WM_CONTEXTMENU()
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()	
	ON_WM_TIMER()
	ON_COMMAND(ID_BTN_GET_CHANNEL, OnBtnGetChannel)
	ON_UPDATE_COMMAND_UI(ID_BTN_GET_CHANNEL, OnUpdateBtnGetChannel)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_LINE_TRACKING, OnLineTracking)
	ON_WM_SIZE()
	ON_COMMAND(IDM_DATA_QUERY, OnDataQuery)
	ON_LBN_SELCHANGE(200, OnLineListSelectChanged)
	ON_COMMAND(IDM_LINE_PATTERN, OnLinePattern)
	ON_COMMAND(IDM_LINE_COLOR, OnLineColor)
	ON_COMMAND(IDM_LINE_WIDTH_1, OnLineWidth1)
	ON_COMMAND(IDM_LINE_WIDTH_2, OnLineWidth2)
	ON_COMMAND(IDM_LINE_WIDTH_3, OnLineWidth3)
	ON_COMMAND(IDM_LINE_WIDTH_4, OnLineWidth4)
	ON_COMMAND(IDM_LINE_WIDTH_5, OnLineWidth5)
	ON_COMMAND(IDM_LINE_TYPE_SOLID, OnLineTypeSolid)
	ON_COMMAND(IDM_LINE_TYPE_DOT, OnLineTypeDot)
	ON_COMMAND(IDM_LINE_TYPE_DASHDOTDOT, OnLineTypeDashdotdot)
	ON_COMMAND(IDM_LINE_TYPE_DASHDOT, OnLineTypeDashdot)
	ON_COMMAND(IDM_LINE_TYPE_DASH, OnLineTypeDash)
	ON_UPDATE_COMMAND_UI(IDM_LINE_WIDTH_1, OnUpdateLineWidth1)
	ON_UPDATE_COMMAND_UI(IDM_LINE_WIDTH_2, OnUpdateLineWidth2)
	ON_UPDATE_COMMAND_UI(IDM_LINE_WIDTH_3, OnUpdateLineWidth3)
	ON_UPDATE_COMMAND_UI(IDM_LINE_WIDTH_4, OnUpdateLineWidth4)
	ON_UPDATE_COMMAND_UI(IDM_LINE_WIDTH_5, OnUpdateLineWidth5)
	ON_UPDATE_COMMAND_UI(IDM_LINE_TYPE_SOLID, OnUpdateLineTypeSolid)
	ON_UPDATE_COMMAND_UI(IDM_LINE_TYPE_DOT, OnUpdateLineTypeDot)
	ON_UPDATE_COMMAND_UI(IDM_LINE_TYPE_DASHDOTDOT, OnUpdateLineTypeDashdotdot)
	ON_UPDATE_COMMAND_UI(IDM_LINE_TYPE_DASHDOT, OnUpdateLineTypeDashdot)
	ON_UPDATE_COMMAND_UI(IDM_LINE_TYPE_DASH, OnUpdateLineTypeDash)
	ON_UPDATE_COMMAND_UI(IDM_LINE_COLOR, OnUpdateLineColor)
	ON_UPDATE_COMMAND_UI(IDM_LINE_PATTERN, OnUpdateLinePattern)
	ON_UPDATE_COMMAND_UI(ID_LINE_TRACKING, OnUpdateLineTracking)
	ON_COMMAND(ID_RELOAD, OnReload)
	ON_COMMAND(ID_AUTO_REDRAW, OnAutoRedraw)
	ON_UPDATE_COMMAND_UI(ID_AUTO_REDRAW, OnUpdateAutoRedraw)
	ON_COMMAND(ID_DATA_DETAIL_VIEW, OnDataDetailView)
	ON_COMMAND(ID_MARK_DATA_POINT, OnMarkDataPoint)
	ON_COMMAND(ID_MARK_DATA_POINT_NONE, OnMarkDataPointNone)
	ON_COMMAND(ID_LINE_NONE, OnLineNone)
	ON_UPDATE_COMMAND_UI(ID_LINE_NONE, OnUpdateLineNone)
	ON_UPDATE_COMMAND_UI(ID_MARK_DATA_POINT_NONE, OnUpdateMarkDataPointNone)
	ON_UPDATE_COMMAND_UI(ID_MARK_DATA_POINT, OnUpdateMarkDataPoint)
	ON_COMMAND(ID_AXIS_SETTING, OnAxisSetting)
	ON_COMMAND(ID_LOG_VIEW, OnLogView)
	ON_UPDATE_COMMAND_UI(ID_LOG_VIEW, OnUpdateLogView)
	ON_COMMAND(IDM_DATA_SAVE_WIDE, OnDataSaveWide)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalView construction/destruction

CCTSGraphAnalView::CCTSGraphAnalView()
{
	m_byToolFlag             = TOOL_NONE;
	m_ZoomRect               = CRect(0,0,0,0);
//	m_pMouseTrackWnd         = NULL;
	m_pMouseTrackChannelWnd	 = NULL;
	m_TrackingLineIndex.x    = 0;
	m_TrackingLineIndex.y    = 0;
	m_PointPosInTrackingLine = 0;
	m_bAutoReDraw = FALSE;
	LanguageinitMonConfig();

}

CCTSGraphAnalView::~CCTSGraphAnalView()
{
	DestoryMouseTraceWnd();

	if( TEXT_LANG != NULL )					
	{					
		delete[] TEXT_LANG;					
		TEXT_LANG = NULL;					
	}

/*	if(m_pMouseTrackWnd)
	{
		m_pMouseTrackWnd->DestroyWindow();
		delete m_pMouseTrackWnd;
		m_pMouseTrackWnd = NULL;
	}
*/
}


/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalView drawing

void CCTSGraphAnalView::OnDraw(CDC* pDC)
{
	CCTSGraphAnalDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// 현재 view의 Client영역의 크기를 구하여
	CRect rect;
	GetClientRect(rect);
	// CDC Class포인터와 함께 Document에 넘겨주면서
	// Document의 각 Plane을 View에 그린다.
	pDoc->DrawView(pDC,rect);
	//
	if(m_byToolFlag&TOOL_LINE_TRACKING) DrawTrackingLine();
}

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalView diagnostics

#ifdef _DEBUG
void CCTSGraphAnalView::AssertValid() const
{
	CView::AssertValid();
}

void CCTSGraphAnalView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CCTSGraphAnalDoc* CCTSGraphAnalView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSGraphAnalDoc)));
	return (CCTSGraphAnalDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSGraphAnalView message handlers

#include "MainFrm.h"

void CCTSGraphAnalView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();

//	((CChildFrame*)GetParentFrame())->m_wndDialogBar.AddPlaneIntoComboBox();
	
	//Frame Title 변경 
//	POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
//	CMultiDocTemplate *pTempate;
//	pTempate = (CMultiDocTemplate *)AfxGetApp()->GetNextDocTemplate(pos);

/*	CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
	if(pMainFrm)
	{
		CChildFrame *pMonFrm = (CChildFrame *)pMainFrm->GetActiveFrame();
		if(pMonFrm)
		{
			CString ss = ((CCTSGraphAnalDoc *)GetDocument())->m_Data.GetTestName();
			pMonFrm->SetWindowText(((CCTSGraphAnalDoc *)GetDocument())->m_Data.GetTestName());
		}
	}
*/

/*	CRect rect1(0, 0, 100, 100	);

	GetClientRect(rect1);
	rect1.left = rect1.right-240;
	rect1.top = rect1.bottom*0.05;
	rect1.right = rect1.left+230;
	rect1.bottom = rect1.bottom*0.95-20;
	m_LineListBox.Create(WS_CHILD|WS_VISIBLE|WS_VSCROLL|LBS_MULTIPLESEL|LBS_OWNERDRAWVARIABLE|LBS_NOTIFY|LBS_NOINTEGRALHEIGHT|LBS_HASSTRINGS, rect1, this, 200);
	m_LineListBox.ShowWindow(SW_SHOW);


	TRACE("1=> %d/%d/%d/%d(%d/%d)\n", rect1.left, rect1.top, rect1.right, rect1.bottom, rect1.Width(), rect1.Height());
*/
	AddLineList();

	if(((CChildFrame *)GetParentFrame())->m_wndGridBar.IsVisible())
	{
		((CChildFrame *)GetParentFrame())->m_wndGridBar.DisplaySheetData(&GetDocument()->m_Data);
		GetDocument()->m_bLoadedSheet = TRUE;
	}
	else
	{
		GetDocument()->m_bLoadedSheet = FALSE;
	}


	if(m_pMouseTrackChannelWnd == NULL)
	{
		m_pMouseTrackChannelWnd = new CStatic;
		m_pMouseTrackChannelWnd->Create(NULL, WS_BORDER|WS_CHILD|WS_VISIBLE|SS_CENTER,	CRect(0,0,0,0),this, 2001);
		m_pMouseTrackChannelWnd->ShowWindow(SW_HIDE);
/*		CFont font;
		font.CreateFont(
						   15,                        // nHeight
						   0,                         // nWidth
						   0,                         // nEscapement
						   0,                         // nOrientation
						   FW_NORMAL,                 // nWeight
						   FALSE,                     // bItalic
						   FALSE,                     // bUnderline
						   0,                         // cStrikeOut
						   ANSI_CHARSET,              // nCharSet
						   OUT_DEFAULT_PRECIS,        // nOutPrecision
						   CLIP_DEFAULT_PRECIS,       // nClipPrecision
						   DEFAULT_QUALITY,           // nQuality
						   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
						   "Arial"						// lpszFacename
						  );                 
		m_pMouseTrackChannelWnd->SetFont(&font);
*/	}

	//Tooltip timer
	SetTimer(1, 500, NULL);
}

void CCTSGraphAnalView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	SetFocus();

	if(m_pMouseTrackChannelWnd)
		m_pMouseTrackChannelWnd->ShowWindow(SW_HIDE);
	
	m_byToolFlag |= TOOL_ZOOM;
	
	if(m_byToolFlag&TOOL_ZOOM)
	{
		m_ZoomLeftTopPoint = point;
		m_ZoomRect=CRect(point.x,point.y,point.x,point.y);
		SetCapture();
	}
	
	//
	if(m_byToolFlag&TOOL_LINE_TRACKING)
	{
		//
		CDC* pDC = GetDC();
		CRect rect;
		GetClientRect(rect);
		CRect ScopeRect = GetDocument()->m_Data.GetScopeRect(pDC, rect);
		//
		if(ScopeRect.top<=point.y&&point.y<=ScopeRect.bottom
			&&ScopeRect.left<=point.x&&point.x<=ScopeRect.right)
		{
			DrawTrackingLine();
			//
			CPlane*  pPlane   = GetDocument()->m_Data.GetPlaneAt(m_TrackingLineIndex.x);
			CLine*   pLine    = pPlane->GetLineAt(m_TrackingLineIndex.y);
			fltPoint real_val = pPlane->GetRealPosition(point,ScopeRect);
			//
			m_PointPosInTrackingLine = pLine->GetApproximatePos(real_val);
			DrawTrackingLine();
		}
		ReleaseDC(pDC);
	}

	CView::OnLButtonDown(nFlags, point);
}

void CCTSGraphAnalView::OnMouseMove(UINT nFlags, CPoint point) 
{
	if(m_pMouseTrackChannelWnd)
	{
		if(m_ptCurrentMousePosition.x != point.x && m_ptCurrentMousePosition.y != point.y)	
		{
			m_pMouseTrackChannelWnd->ShowWindow(SW_HIDE);
		}
	}

	m_ptCurrentMousePosition = point;
//	TRACE("OnMouseMove()::X(%d, %d)\n", point.x, point.y);
	//
	CDC *pDC=GetDC();
	
	if((m_byToolFlag&TOOL_ZOOM) != TOOL_NONE && this==GetCapture()){
	
		//드래그 영역이 작을 경우
		//click이나 Dobule Click시 
		if(abs(point.x - m_ZoomLeftTopPoint.x) < ZOOM_MIN_SIZE || abs(point.y - m_ZoomLeftTopPoint.y) < ZOOM_MIN_SIZE)	return;

		pDC->SaveDC();
		pDC->SetROP2(R2_NOTXORPEN);
		CPen pen,*oldpen;
		CBrush brush, *poldBrush;
		pen.CreatePen(PS_DOT, 0, RGB(0, 0, 0));
		brush.CreateSolidBrush(RGB(230,230,230));
		oldpen=pDC->SelectObject(&pen);
		poldBrush = pDC->SelectObject(&brush);

/*		pDC->MoveTo(m_ZoomRect.TopLeft());
		pDC->LineTo(m_ZoomRect.right,m_ZoomRect.top);
		pDC->LineTo(m_ZoomRect.BottomRight());
		pDC->LineTo(m_ZoomRect.left,m_ZoomRect.bottom);
		pDC->LineTo(m_ZoomRect.TopLeft());
*/
		pDC->Rectangle(m_ZoomRect);
			
		m_ZoomRect.left = __min(m_ZoomLeftTopPoint.x, point.x);
		m_ZoomRect.top = __min(m_ZoomLeftTopPoint.y, point.y);
		m_ZoomRect.right = __max(m_ZoomLeftTopPoint.x, point.x);
		m_ZoomRect.bottom = __max(m_ZoomLeftTopPoint.y, point.y);

/*
//			|
//		2	|  1
//	  <-----+------>
//		3	|  4
//			|
		//4사분면 방향으로 Drag
//		if(point.x > m_ZoomLeftTopPoint.x && point.y > m_ZoomLeftTopPoint.y)
//		{
			m_ZoomRect.TopLeft() = m_ZoomLeftTopPoint;
			m_ZoomRect.BottomRight() = point;
		}
		//2사분면 방향으로 Drag
		else if(point.x < m_ZoomLeftTopPoint.x && point.y < m_ZoomLeftTopPoint.y)
		{
			m_ZoomRect.TopLeft() = point;
			m_ZoomRect.BottomRight() = m_ZoomLeftTopPoint;
		}
		//1사분면 방향으로 Drag
		else if(point.x > m_ZoomLeftTopPoint.x && point.y < m_ZoomLeftTopPoint.y)
		{
			m_ZoomRect.left = m_ZoomLeftTopPoint.x;
			m_ZoomRect.bottom = m_ZoomLeftTopPoint.y;
			m_ZoomRect.right = point.x;
			m_ZoomRect.top = point.y;
		}
		//3사분면 방향으로 Drag
		else if(point.x < m_ZoomLeftTopPoint.x && point.y > m_ZoomLeftTopPoint.y)
		{
			m_ZoomRect.top = m_ZoomLeftTopPoint.y;
			m_ZoomRect.right = m_ZoomLeftTopPoint.x;
			m_ZoomRect.left = point.x;
			m_ZoomRect.bottom = point.y;
		}
*/			
/*		pDC->MoveTo(m_ZoomRect.TopLeft());
		pDC->LineTo(m_ZoomRect.right,m_ZoomRect.top);
		pDC->LineTo(m_ZoomRect.BottomRight());
		pDC->LineTo(m_ZoomRect.left,m_ZoomRect.bottom);
		pDC->LineTo(m_ZoomRect.TopLeft());
*/
		pDC->Rectangle(m_ZoomRect);
		
		pDC->SelectObject(poldBrush);
		brush.DeleteObject();

		pDC->SelectObject(oldpen);
		pen.DeleteObject();
		
		pDC->RestoreDC(-1);
	}

	//Main Frame 하단에 좌표 표시 
	CRect rect;
	GetClientRect(rect);
	CRect ScopeRect = GetDocument()->m_Data.GetScopeRect(pDC, rect);
	//
	CString str, strMsg;
	//Line trace mode가 아니면서 그래프 영역이면 하단에 좌표 표시
	if( ScopeRect.top<=point.y&&point.y<=ScopeRect.bottom &&ScopeRect.left<=point.x&&point.x<=ScopeRect.right && ((m_byToolFlag & TOOL_LINE_TRACKING) == 0))
	{
		CPlane* pPlane;
		fltPoint val;
		for(int nP = 0; nP<GetDocument()->m_Data.GetPlaneCount(); nP++)
		{
			pPlane = GetDocument()->m_Data.GetPlaneAt(nP);
			val = pPlane->GetRealPosition(point, ScopeRect);
			if(nP == 0)
			{
				str.Format("X=%.3f%s", pPlane->GetXAxis()->ConvertUnit(val.x),  pPlane->GetXAxis()->GetUnitNotation());
				strMsg  += str;
			}
			str.Format(", Y%d=%.3f%s", nP+1, pPlane->GetYAxis()->ConvertUnit(val.y),  pPlane->GetYAxis()->GetUnitNotation());
			strMsg  += str;
		}
		((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
	}
	ReleaseDC(pDC);

	//현재 마우스 위치가 Data Point와 근접하면 Data값을 표시한다.
	
/*				fltPoint val = pPlane->GetRealPosition(point, ScopeRect);
				//val = pPlane->GetCloseData(point);
				str.Format("X=%.3f%s, Y=%.3f%s",
					       pPlane->GetXAxis()->ConvertUnit(val.x),
						   pPlane->GetXAxis()->GetUnitNotation(),
						   pPlane->GetYAxis()->ConvertUnit(val.y),
						   pPlane->GetYAxis()->GetUnitNotation());

				m_pMouseTrackWnd->SetWindowText(strMsg);
				m_pMouseTrackWnd->ShowWindow(SW_SHOW);


				//
				TEXTMETRIC Metrics;
				m_pMouseTrackWnd->GetDC()->GetTextMetrics(&Metrics);
				if(point.x<=rect.right-(str.GetLength()+4)*Metrics.tmAveCharWidth)
					m_pMouseTrackWnd->MoveWindow(point.x,point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
					(str.GetLength()+4)*Metrics.tmAveCharWidth,Metrics.tmHeight,TRUE);
				else if(point.x>=(str.GetLength()+4)*Metrics.tmAveCharWidth)
					m_pMouseTrackWnd->MoveWindow(point.x-(str.GetLength()+4)*Metrics.tmAveCharWidth,
					point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
					(str.GetLength()+4)*Metrics.tmAveCharWidth,Metrics.tmHeight,TRUE);

			}
			else 
			{
				m_pMouseTrackWnd->ShowWindow(SW_HIDE);
			}		       
		}
	}
*/
	//Added by KBH 2005/4/22
	//마우스가 움직이면 수직/수평선을 그어준다.
			//
//			static CPoint oldPoint(0, 0);
//
//			CRect rect;
//			GetClientRect(rect);
//			CRect ScopeRect = GetDocument()->m_Data.GetScopeRect(GetDC(), rect);
//			//
//			CString str;
//			if(ScopeRect.top<=point.y&&point.y<=ScopeRect.bottom
//				&&ScopeRect.left<=point.x&&point.x<=ScopeRect.right)
//			{
//				
//				CDC *pDC=GetDC();
//				pDC->SaveDC();
//				
//				pDC->SetROP2(R2_NOTXORPEN);
//
//				CPen pen,*oldpen;
//				pen.CreatePen(PS_SOLID, 0, RGB(0,0,0));
//				oldpen=pDC->SelectObject(&pen);
//
//				if(oldPoint.x !=0 && oldPoint.y !=0)
//				{
//					pDC->MoveTo(oldPoint.x, ScopeRect.top);
//					pDC->LineTo(oldPoint.x, ScopeRect.bottom);
//					pDC->MoveTo(ScopeRect.left, oldPoint.y);
//					pDC->LineTo(ScopeRect.right, oldPoint.y);
//				}
//					
//					pDC->MoveTo(point.x, ScopeRect.top);
//					pDC->LineTo(point.x, ScopeRect.bottom);
//					pDC->MoveTo(ScopeRect.left, point.y);
//					pDC->LineTo(ScopeRect.right, point.y);
//				
//				
//
//				oldPoint = point;
//				pDC->SelectObject(oldpen);
//				pen.DeleteObject();
//				pDC->RestoreDC(-1);
//			
//			}
/////////////////////////////////				       
			//

//	if(m_byToolFlag&TOOL_DISPLAY_CHANNEL_NUMBER&&m_bDisplayChannelFlag)
/*	if(m_byToolFlag&TOOL_DISPLAY_COORDINATE)
	{
		m_bDisplayChannelFlag = false;
		int index = ((CChildFrame*)GetParentFrame())->m_wndDialogBar.m_iCurSelPlaneIndex;
		if(index!=CB_ERR)
		{
			//
			CRect rect;
			GetClientRect(rect);
			CRect ScopeRect = GetDocument()->m_Data.GetScopeRect(GetDC(), rect);
			//
			CString str;
			if(ScopeRect.top<=point.y&&point.y<=ScopeRect.bottom
				&&ScopeRect.left<=point.x&&point.x<=ScopeRect.right)
			{
				if(m_bDisplayChannelFlag)
				{
					int nChannelIndex = GetDocument()->m_Data.CheckPointInWhichChannel(GetDC(), ScopeRect, point);
					if(nChannelIndex>=0)
					{
						CPlane* pPlane = GetDocument()->m_Data.GetPlaneAt(index);
						fltPoint val = pPlane->GetRealPosition(point, ScopeRect);
						//
						str.Format("Channel%d X=%.3f%s, Y=%.3f%s",
								   nChannelIndex,
								   pPlane->GetXAxis()->ConvertUnit(val.x),
								   pPlane->GetXAxis()->GetUnitNotation(),
								   pPlane->GetYAxis()->ConvertUnit(val.y),
								   pPlane->GetYAxis()->GetUnitNotation());
					}
					else str = "Hit Miss Channel";
				}
				else
				{
					CPlane* pPlane = GetDocument()->m_Data.GetPlaneAt(index);
					fltPoint val = pPlane->GetRealPosition(point, ScopeRect);
					//
					str.Format("X=%.3f%s, Y=%.3f%s",
							   pPlane->GetXAxis()->ConvertUnit(val.x),
							   pPlane->GetXAxis()->GetUnitNotation(),
							   pPlane->GetYAxis()->ConvertUnit(val.y),
							   pPlane->GetYAxis()->GetUnitNotation());
				}
			}
			else str = "Out of scope";
				       
			//
			TEXTMETRIC Metrics;
			m_pMouseTrackWnd->GetDC()->GetTextMetrics(&Metrics);
			if(point.x<=rect.right-(str.GetLength()+4)*Metrics.tmAveCharWidth)
				m_pMouseTrackWnd->MoveWindow(point.x,point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
				(str.GetLength()+4)*Metrics.tmAveCharWidth,Metrics.tmHeight,TRUE);
			else if(point.x>=(str.GetLength()+4)*Metrics.tmAveCharWidth)
				m_pMouseTrackWnd->MoveWindow(point.x-(str.GetLength()+4)*Metrics.tmAveCharWidth,
				point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
				(str.GetLength()+4)*Metrics.tmAveCharWidth,Metrics.tmHeight,TRUE);
			m_pMouseTrackWnd->SetWindowText(str);
		}
	}
*/
	//
	CView::OnMouseMove(nFlags, point);
}

void CCTSGraphAnalView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(m_byToolFlag&TOOL_ZOOM){
		if(this==GetCapture()) ReleaseCapture();

		//드래그 영역이 작을 경우
		//click이나 Dobule Click시 
		if(m_ZoomRect.Height() < ZOOM_MIN_SIZE || m_ZoomRect.Width() < ZOOM_MIN_SIZE)
		{
			return;
		}

		CDC *pDC=GetDC();
		pDC->SaveDC();
		pDC->SetROP2(R2_NOTXORPEN);
		CPen pen,*oldpen;
		pen.CreatePen(PS_DOT,0,RGB(0,0,0));
		oldpen=pDC->SelectObject(&pen);
		pDC->MoveTo(m_ZoomRect.TopLeft());
		pDC->LineTo(m_ZoomRect.right,m_ZoomRect.top);
		pDC->LineTo(m_ZoomRect.BottomRight());
		pDC->LineTo(m_ZoomRect.left,m_ZoomRect.bottom);
		pDC->LineTo(m_ZoomRect.TopLeft());
		pDC->SelectObject(oldpen);
		pen.DeleteObject();
		pDC->RestoreDC(-1);

		//
		CRect ClientRect;
		GetClientRect(ClientRect);

		RECT scopeRect = GetDocument()->m_Data.GetScopeRect(pDC,ClientRect);
		ReleaseDC(pDC);
		//
		POSITION pos = GetDocument()->m_Data.GetHeadPlanePos();
		int nColumnCount = 0;
		while(pos)
		{
			CPlane* pPlane = GetDocument()->m_Data.GetNextPlane(pos);
 			point.x = __min(m_ZoomRect.right,m_ZoomRect.left);
 			point.y = __max(m_ZoomRect.top,  m_ZoomRect.bottom);
			fltPoint Min = pPlane->GetRealPosition(point, scopeRect);

 			point.x = __max(m_ZoomRect.right,m_ZoomRect.left);
 			point.y = __min(m_ZoomRect.top,  m_ZoomRect.bottom);
			fltPoint Max = pPlane->GetRealPosition(point, scopeRect);


			pPlane->ZoomIn(Min, Max);
	
			CLine *pLine;
			for(int i=0; i<pPlane->GetLineCount(); i++)
			{
				pLine = pPlane->GetLineAt(i);
				if(pLine)
				{
					//Line별 별도로 구간을 지정하여야 한다.
					nColumnCount++;
					((CChildFrame *)GetParentFrame())->m_wndGridBar.SelectDataRange(pLine->GetApproximatePos(Min), pLine->GetApproximatePos(Max), nColumnCount);
				}
			}
		}
		//
		m_ZoomRect = CRect(0,0,0,0);
		Invalidate(FALSE);
//		((CChildFrame*)GetParentFrame())->m_wndDialogBar.UpdateAxesRange();
//		UpdateAxesRange();

		m_byToolFlag &= ~TOOL_ZOOM;

	}
	
	CView::OnLButtonUp(nFlags, point);
}

void CCTSGraphAnalView::OnToolZoomIn() 
{
//	if(m_byToolFlag&TOOL_DISPLAY_COORDINATE) OnToolsMousetrack();
//	if(m_byToolFlag&TOOL_DISPLAY_CHANNEL_NUMBER) OnBtnGetChannel();
	
	//reset tracking
	if(m_byToolFlag&TOOL_LINE_TRACKING)      OnToolsLinetrack(0,0);

	if(m_byToolFlag&TOOL_ZOOM) m_byToolFlag &= ~TOOL_ZOOM;
	else                       m_byToolFlag  =  TOOL_ZOOM;
}

void CCTSGraphAnalView::OnUpdateToolZoomIn(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck((m_byToolFlag&TOOL_ZOOM)==TOOL_ZOOM);
}

void CCTSGraphAnalView::OnToolZoomOut() 
{
		//
		POSITION pos = GetDocument()->m_Data.GetHeadPlanePos();
		while(pos)
		{
			CPlane* pPlane = GetDocument()->m_Data.GetNextPlane(pos);
			pPlane->ZoomOut();
		}
		//
		Invalidate(FALSE);
//		((CChildFrame*)GetParentFrame())->m_wndDialogBarUpdateAxesRange();
//		UpdateAxesRange();
}

void CCTSGraphAnalView::OnUpdateToolZoomOut(CCmdUI* pCmdUI) 
{
	BOOL bCheck = FALSE;
	POSITION pos = GetDocument()->m_Data.GetHeadPlanePos();
	if(pos)
	{
		CPlane* pPlane = GetDocument()->m_Data.GetNextPlane(pos);
		bCheck = pPlane->IsZoomInMode();
	}

	pCmdUI->Enable(bCheck);
}

void CCTSGraphAnalView::OnToolZoomOff() 
{
		//
		POSITION pos = GetDocument()->m_Data.GetHeadPlanePos();
		while(pos)
		{
			CPlane* pPlane = GetDocument()->m_Data.GetNextPlane(pos);
			pPlane->ZoomOff();
		}
		//
		Invalidate(FALSE);
		//((CChildFrame*)GetParentFrame())->m_wndDialogBar.UpdateAxesRange();
//		UpdateAxesRange();
}

void CCTSGraphAnalView::OnUpdateToolZoomOff(CCmdUI* pCmdUI) 
{
	BOOL bCheck = FALSE;
	POSITION pos = GetDocument()->m_Data.GetHeadPlanePos();
	if(pos)
	{
		CPlane* pPlane = GetDocument()->m_Data.GetNextPlane(pos);
		bCheck = pPlane->IsZoomInMode();
	}

	pCmdUI->Enable(bCheck);
}

void CCTSGraphAnalView::OnToolsScreencapture() 
{
	if ( !OpenClipboard() )
	{
		MessageBox(TEXT_LANG[0], "Error", MB_ICONEXCLAMATION);
		return;
	}
	// Remove the current Clipboard contents
	if( !EmptyClipboard() )
	{
		MessageBox(TEXT_LANG[1], "Error", MB_ICONEXCLAMATION);
		return;
	}
	// Get the currently selected data
	COLORREF BACKGROUND_COLOR=RGB(255,255,255);
	CBitmap *junk;
	junk=new CBitmap();
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);
	CRect client;
	GetClientRect(client);
	if(!junk->CreateCompatibleBitmap(&cdc,client.Width(),client.Height())){
		MessageBox(TEXT_LANG[2], "Error", MB_ICONEXCLAMATION);
	}
	dc.SelectObject(junk);
	CBrush fill(BACKGROUND_COLOR);
	dc.FillRect(client,&fill);
	OnDraw(&dc);
	//
	// For the appropriate data formats...
	if ( ::SetClipboardData( CF_BITMAP, HBITMAP(*junk)) == NULL )
	{
		MessageBox(TEXT_LANG[3], "Error", MB_ICONEXCLAMATION);
		CloseClipboard();
		return;
	}
	// ...
	CloseClipboard();
	delete junk;
}


void CCTSGraphAnalView::OnBtnGetChannel() 
{	
//	if(m_byToolFlag&TOOL_ZOOM)          m_byToolFlag &= ~TOOL_ZOOM;
	
	//라인 추적 모드 이면 없앰 
/*	if(m_byToolFlag&TOOL_LINE_TRACKING) OnToolsLinetrack(0,0);

	//채널 표시 모드 
	if(m_byToolFlag&TOOL_DISPLAY_CHANNEL_NUMBER)
	{
		m_byToolFlag &= ~TOOL_DISPLAY_CHANNEL_NUMBER;
		DestoryMouseTraceWnd();
		KillTimer(1);
	}
	else
	{
		m_byToolFlag = TOOL_DISPLAY_CHANNEL_NUMBER;
		SetTimer(1, 500, NULL);
	}	
*/
}

void CCTSGraphAnalView::OnUpdateBtnGetChannel(CCmdUI* pCmdUI) 
{
//	pCmdUI->SetCheck((m_byToolFlag&TOOL_DISPLAY_CHANNEL_NUMBER)==TOOL_DISPLAY_CHANNEL_NUMBER);
//	TRACE("0x%x\n", m_byToolFlag);
}

void CCTSGraphAnalView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	SetFocus();

	if(m_pMouseTrackChannelWnd)
		m_pMouseTrackChannelWnd->ShowWindow(SW_HIDE);

	//
//	if(m_byToolFlag&TOOL_DISPLAY_CHANNEL_NUMBER)	OnBtnGetChannel();

//	if(m_byToolFlag&TOOL_DISPLAY_COORDINATE) OnToolsMousetrack();
	//
	if(m_byToolFlag&TOOL_LINE_TRACKING) OnToolsLinetrack(0,0);
	//
	else if(m_byToolFlag&TOOL_ZOOM)
	{
		m_byToolFlag &= ~TOOL_ZOOM;
		if(this==GetCapture()) ReleaseCapture();
		CDC *pDC=GetDC();
		pDC->SaveDC();
		pDC->SetROP2(R2_NOTXORPEN);
		CPen pen,*oldpen;
		pen.CreatePen(PS_DOT,0,RGB(0,0,0));
		oldpen=pDC->SelectObject(&pen);
		pDC->MoveTo(m_ZoomRect.TopLeft());
		pDC->LineTo(m_ZoomRect.right,m_ZoomRect.top);
		pDC->LineTo(m_ZoomRect.BottomRight());
		pDC->LineTo(m_ZoomRect.left,m_ZoomRect.bottom);
		pDC->LineTo(m_ZoomRect.TopLeft());
		pDC->SelectObject(oldpen);
		pen.DeleteObject();
		pDC->RestoreDC(-1);
		m_ZoomRect = CRect(0,0,0,0);

		ReleaseDC(pDC);
	}
//	else{
//
		CMenu aMenu;
		aMenu.LoadMenu(IDR_GRAPH);
		aMenu.GetSubMenu(1)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, AfxGetMainWnd());
		
//	}
}

void CCTSGraphAnalView::OnKillFocus(CWnd* pNewWnd) 
{
//	if(m_byToolFlag&TOOL_DISPLAY_COORDINATE) OnToolsMousetrack();
	if(m_byToolFlag&TOOL_ZOOM)               m_byToolFlag &= ~TOOL_ZOOM;
	if(m_byToolFlag&TOOL_LINE_TRACKING)      OnToolsLinetrack(0,0);
//	if(m_byToolFlag&TOOL_DISPLAY_CHANNEL_NUMBER) OnBtnGetChannel();

	CView::OnKillFocus(pNewWnd);
}

void CCTSGraphAnalView::OnToolsShowLinePattern(int nPlane, int nLine)
{
	CPlane *pPlane = GetDocument()->m_Data.GetPlaneAt(nPlane);
	if(pPlane)
	{
		CLine* pLine = pPlane->GetLineAt(nLine);
		if(pLine)
		{
			GetDocument()->m_Data.ShowPatternOfChData(pLine->GetChPath());
		}
	}
}

void CCTSGraphAnalView::OnToolsLinetrack(int nPlane, int nLine)
{
	if(m_byToolFlag&TOOL_ZOOM)          m_byToolFlag &= ~TOOL_ZOOM;

//	if(m_byToolFlag&TOOL_DISPLAY_COORDINATE) OnToolsMousetrack();

//	if(m_byToolFlag&TOOL_DISPLAY_CHANNEL_NUMBER) OnBtnGetChannel();
	
	if(m_byToolFlag&TOOL_LINE_TRACKING)
	{
		m_byToolFlag &= ~TOOL_LINE_TRACKING;
		DrawTrackingLine();
		m_PointPosInTrackingLine = 0;
		m_TrackingLineIndex.x    = 0;
		m_TrackingLineIndex.y    = 0;
//		((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
	}
	else
	{
		m_byToolFlag |= TOOL_LINE_TRACKING;
		m_TrackingLineIndex.x    = nPlane;
		m_TrackingLineIndex.y    = nLine;
		m_PointPosInTrackingLine = GetDocument()->m_Data.GetPlaneAt(nPlane)->GetLineAt(nLine)->GetDataCount()/2;
		DrawTrackingLine();
	}
}

BOOL CCTSGraphAnalView::DrawTrackingLine()
{
	//
	CPlane*  pPlane = GetDocument()->m_Data.GetPlaneAt(m_TrackingLineIndex.x);
	if(pPlane == FALSE)		return	FALSE;
	CLine*   pLine  = pPlane->GetLineAt(m_TrackingLineIndex.y);
	if(pLine == FALSE)		return FALSE;
	POSITION pos    = pLine->FindDataIndex(m_PointPosInTrackingLine);
	//
	if(pos!=NULL)
	{
		//
		CDC* pDC = GetDC();
		CRect rect;
		GetClientRect(rect);
		CRect ScopeRect = GetDocument()->m_Data.GetScopeRect(pDC, rect);
		//
		fltPoint data_val = pLine->GetDataAt(pos);
		POINT    view_pt  = pPlane->GetViewPosition(data_val, ScopeRect);
		//
		CString str;
		str.Format("X=%.3f%s, Y=%.3f%s",
			       pPlane->GetXAxis()->ConvertUnit(data_val.x),
				   pPlane->GetXAxis()->GetUnitNotation(),
				   pPlane->GetYAxis()->ConvertUnit(data_val.y),
				   pPlane->GetYAxis()->GetUnitNotation());
		//
		pDC->SaveDC();
		pDC->SetROP2(R2_NOTXORPEN);
		CPen apen, *oldpen;
		apen.CreatePen(PS_DASH, 0, RGB(0,0,0));
		oldpen=pDC->SelectObject(&apen);
		pDC->MoveTo(view_pt.x,ScopeRect.top);
		pDC->LineTo(view_pt.x,ScopeRect.bottom);
		pDC->MoveTo(ScopeRect.left,view_pt.y);
		pDC->LineTo(ScopeRect.right,view_pt.y);
		pDC->SelectObject(oldpen);
		apen.DeleteObject();
		pDC->RestoreDC(-1);
		ReleaseDC(pDC);
		//
		((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(str);
		//
		SetFocus();
		return TRUE;
	}
	else return FALSE;
}

void CCTSGraphAnalView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(m_byToolFlag&TOOL_LINE_TRACKING)
	{
		if(nChar==VK_RIGHT){
			DrawTrackingLine();
			m_PointPosInTrackingLine++;
			if(!DrawTrackingLine()){
				m_PointPosInTrackingLine--;
				DrawTrackingLine();
			}
		}
		if(nChar==VK_LEFT){
			DrawTrackingLine();
			m_PointPosInTrackingLine--;
			if(!DrawTrackingLine()){
				m_PointPosInTrackingLine++;
				DrawTrackingLine();
			}
		}
		if(nChar==VK_UP){
			DrawTrackingLine();
			m_PointPosInTrackingLine+=20;
			if(!DrawTrackingLine()){
				m_PointPosInTrackingLine-=20;
				DrawTrackingLine();
			}
		}
		if(nChar==VK_DOWN){
			DrawTrackingLine();
			m_PointPosInTrackingLine-=20;
			if(!DrawTrackingLine()){
				m_PointPosInTrackingLine+=20;
				DrawTrackingLine();
			}
		}
	}
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CCTSGraphAnalView::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 100)		//자동 갱신 Timer
	{
		((CCTSGraphAnalDoc *)GetDocument())->ReloadData();
		return;
	}
	
	static POINT point = {0, 0};
	
//	TRACE("(%d,%d) (%d,%d)\n", m_ptCurrentMousePosition.x, m_ptCurrentMousePosition.y, point.x, point.y);
	//Mouse 움직임이 없을 경우 사용 안함
	if(	m_ptCurrentMousePosition.x-3 <= point.x  && point.x <= m_ptCurrentMousePosition.x +3
		&& m_ptCurrentMousePosition.y-3 <= point.y  && point.y <= m_ptCurrentMousePosition.y +3)
	{
		return;
	}
	else
	{
		if(m_pMouseTrackChannelWnd)
			m_pMouseTrackChannelWnd->ShowWindow(SW_HIDE);
	}

	//??? 가끔 Null이 return되는 경우가 있음 
	CDC *pDC = GetDC();
	if(pDC == NULL)	return;
	
	point = m_ptCurrentMousePosition;
	CRect rect;
	GetClientRect(rect);
	CRect ScopeRect = GetDocument()->m_Data.GetScopeRect(pDC, rect);
	ReleaseDC(pDC);

	int nPlanIndex, nLineIndex;
	fltPoint fltDataPoint;
	CString str;
	CPlane* pPlane;
	CLine *pLine;

	//Data point toop tip
	//마우스가 data 포인트 위로 올라가면 Point를 표시한다.
	if(GetDocument()->m_Data.CheckPointWhichPlane(ScopeRect, point, nPlanIndex, nLineIndex, fltDataPoint))
	{
		pPlane = GetDocument()->m_Data.GetPlaneAt(nPlanIndex);
		if(pPlane)
		{
			pLine = pPlane->GetLineAt(nLineIndex);
			str.Format("%s :: %.3f%s, %.3f%s (%d/%d)\n", pLine->GetName(), 
							   pPlane->GetXAxis()->ConvertUnit(fltDataPoint.x),
							   pPlane->GetXAxis()->GetUnitNotation(),
							   pPlane->GetYAxis()->ConvertUnit(fltDataPoint.y),
							   pPlane->GetYAxis()->GetUnitNotation(), pLine->GetSelect(), pLine->IsDataPointMark());
			TRACE("%s\n", str);

			m_pMouseTrackChannelWnd->SetWindowText(str);

			if(pLine->GetSelect() || pLine->IsDataPointMark())		//현재 라인이 선택되어 있을 경우 Tootip으로 표시
			{
				str.Format("%s :: %.3f%s, %.3f%s\n", pLine->GetName(), 
								   pPlane->GetXAxis()->ConvertUnit(fltDataPoint.x),
								   pPlane->GetXAxis()->GetUnitNotation(),
								   pPlane->GetYAxis()->ConvertUnit(fltDataPoint.y),
								   pPlane->GetYAxis()->GetUnitNotation());

				m_pMouseTrackChannelWnd->SetWindowText(str);
				m_pMouseTrackChannelWnd->ShowWindow(SW_SHOW);

//				TRACE("%s\n", str);
				
				//Commented By KBH 2005.4.22/////////////
				// 영역 밖은 표시 하지 않도록 하기 위해 위쪽으로 이동
				// 영역 밖이면 표기 윈도우를 숨기고 구역이면 표기한다.				
				TEXTMETRIC Metrics;
				CDC *pWndDC = m_pMouseTrackChannelWnd->GetDC();
				if(pWndDC)
				{
					pWndDC->GetTextMetrics(&Metrics);
					if(point.x<=rect.right-(str.GetLength())*Metrics.tmAveCharWidth)
							m_pMouseTrackChannelWnd->MoveWindow(point.x,point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
							(str.GetLength())*Metrics.tmAveCharWidth, Metrics.tmHeight+1,TRUE);
					else if(point.x>=(str.GetLength())*Metrics.tmAveCharWidth)
							m_pMouseTrackChannelWnd->MoveWindow(point.x-(str.GetLength())*Metrics.tmAveCharWidth,
							point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
							(str.GetLength())*Metrics.tmAveCharWidth,Metrics.tmHeight+1,TRUE);
					ReleaseDC(pWndDC);
				}

// 				if(m_nShowLineToopTip)		//항상 표시
// 				{
// 					m_pMouseTrackChannelWnd->ShowWindow(SW_SHOW);
// 					TRACE("Show line tooltip\n");
// 				}	
// 				else
// 				{	//선택 Line일 경우만 표시 
 					if(pLine->GetSelect() || pLine->IsDataPointMark())		//현재 라인이 선택되어 있을 경우 Tootip으로 표시
 					{
 						m_pMouseTrackChannelWnd->ShowWindow(SW_SHOW);
 					}
// 				}
			}

		}
	}
	else
	{
		if(m_pMouseTrackChannelWnd)
			m_pMouseTrackChannelWnd->ShowWindow(SW_HIDE);
	}

	
/*	//Channel 번호 찾기 
	if(m_byToolFlag == TOOL_DISPLAY_CHANNEL_NUMBER)
	{
		int index = ((CCTSGraphAnalDoc*)GetDocument())->m_iCurSelPlaneIndex;		
		if(index!=CB_ERR)
		{
			//생성 
			if(m_pMouseTrackChannelWnd == NULL)
			{
				m_pMouseTrackChannelWnd = new CStatic;
				m_pMouseTrackChannelWnd->Create(NULL, WS_BORDER|WS_CHILD|WS_VISIBLE|SS_CENTER,	CRect(0,0,100,20),this, 2001);
			}

			//
			if(ScopeRect.top <= point.y && point.y <= ScopeRect.bottom &&ScopeRect.left <=point.x &&point.x <= ScopeRect.right)
			{
				nLineIndex = GetDocument()->m_Data.CheckPointInWhichChannel(ScopeRect, point);
				if(nLineIndex >= 0)
				{
					pPlane = GetDocument()->m_Data.GetPlaneAt(index);
					if(pPlane)
					{
						pLine = pPlane->GetLineAt(nLineIndex);
						if(pLine)
						{
							str= pLine->GetName();		//해당 Line의 채널 번호 이름을 구함
						}
					}
					m_pMouseTrackChannelWnd->SetWindowText(str);
					m_pMouseTrackChannelWnd->ShowWindow(SW_SHOW);
			
					//Commented By KBH 2005.4.22/////////////
					// 영역 밖은 표시 하지 않도록 하기 위해 위쪽으로 이동
					// 영역 밖이면 표기 윈도우를 숨기고 구역이면 표기한다.				
					TEXTMETRIC Metrics;
					m_pMouseTrackChannelWnd->GetDC()->GetTextMetrics(&Metrics);
					if(point.x<=rect.right-(str.GetLength()+4)*Metrics.tmAveCharWidth)
							m_pMouseTrackChannelWnd->MoveWindow(point.x,point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
								(str.GetLength()+4)*Metrics.tmAveCharWidth,Metrics.tmHeight,TRUE);
					else if(point.x>=(str.GetLength()+4)*Metrics.tmAveCharWidth)
							m_pMouseTrackChannelWnd->MoveWindow(point.x-(str.GetLength()+4)*Metrics.tmAveCharWidth,
								point.y-(Metrics.tmHeight+Metrics.tmExternalLeading),
								(str.GetLength()+4)*Metrics.tmAveCharWidth,Metrics.tmHeight,TRUE);
			//		
				}
				else 
				{
					m_pMouseTrackChannelWnd->ShowWindow(SW_HIDE);
				}
				
			}
		}
	}
*/	
	CView::OnTimer(nIDEvent);
}

void CCTSGraphAnalView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	OnAxisSetting();
	CView::OnLButtonDblClk(nFlags, point);
}


void CCTSGraphAnalView::OnFilePrint() 
{
	// TODO: Add your command handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!"); //000591
     ScrCap->DoPrntScreen(2,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;		
}

void CCTSGraphAnalView::OnLineTracking() 
{
	// TODO: Add your command handler code here
/*	int iCurSelLineIndex = 0;
	if(m_LineListBox.GetSelItems(1,&iCurSelLineIndex) == 1)	//가장 처음 선택된 라인을 구한다.
	{
		DWORD dwData = m_LineListBox.GetItemData(iCurSelLineIndex);
		OnToolsLinetrack(HIWORD(dwData), LOWORD(dwData));
	}
*/

	int nPlaneIndex = 0;
	int nLineIndex = 0;
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		HTREEITEM hPItem = pTree->GetParentItem(hItem);
		if(hItem && hPItem)
		{
			//Get Line No
			while(hItem)
			{
				hItem = pTree->GetPrevSiblingItem(hItem);
				if(hItem)	nLineIndex++;
			}

			//Get Plane No
			while(hPItem)
			{
				hPItem = pTree->GetPrevSiblingItem(hPItem);
				if(hPItem)	nPlaneIndex++;
			}

			OnToolsLinetrack(nPlaneIndex, nLineIndex);
		}
	}
}

void CCTSGraphAnalView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	// TODO: Add your message handler code here
/*	if(m_LineListBox.GetSafeHwnd())
	{
		m_LineListBox.MoveWindow(cx-240, int(cy*0.05), 230, int(cy*0.95-20));
		
//		TRACE("2=> %d/%d/%d/%d\n", cx-180, (int)(cy*0.05), 160, (int)(cy*0.95)-20);
	}
*/
}

BOOL CCTSGraphAnalView::AddLineList()
{
	CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)GetDocument();

	HTREEITEM hti, hTree;
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	// populate the treectrl
	pTree->DeleteAllItems();

	CPlane* pPlane;
	CAxis *yAxis;
//	m_LineListBox.ResetContent();
//	int nCount =0;
//	int nPlaneIndex =0, nLineIndex = 0;
//	CString str;
//	char szBuff[512];


	POSITION pos1 = pDoc->m_Data.GetHeadPlanePos();
	while(pos1)
	{
		pPlane = pDoc->m_Data.GetNextPlane(pos1);
		yAxis = pPlane->GetYAxis();
		
		hti = pTree->InsertItem(yAxis->GetPropertyTitle());
		pTree->SetItemData(hti, (DWORD)pPlane);
		pTree->SetItemImage(hti, 0, 1);

		pTree->SetCheck(hti, TRUE);

		POSITION pos = pPlane->GetLineHead();
//		nLineIndex = 0;
		while(pos)
		{
			CLine* pLine = pPlane->GetNextLine(pos);
			hTree = pTree->InsertItem(pLine->GetName(), hti);

			pTree->SetCheck(hTree, pLine->GetShow());

			pTree->SetItemData(hTree,  (DWORD)pLine);
			pTree->SetItemImage(hTree, 6, 7);
		
/*			sprintf(szBuff, "%s(%s),%d,%d,%d,%d,%d",
				pLine->GetName(), yAxis->GetUnitNotation(),
				GetRValue(pLine->GetColor()),
				GetGValue(pLine->GetColor()),
				GetBValue(pLine->GetColor()),
				pLine->GetWidth(),
				pLine->GetType());
			m_LineListBox.AddString(szBuff);
			if(pLine->GetSelect())
			{
				m_LineListBox.GetSel(nCount);
			}

			m_LineListBox.SetItemData(nCount++, (DWORD)pLine);			
	
			nLineIndex++;
*/
		}
//		nPlaneIndex++;
	}
	return TRUE;
}

void CCTSGraphAnalView::OnDataQuery() 
{
	// TODO: Add your command handler code here
	if(((CCTSGraphAnalDoc *)GetDocument())->RequeryDataQuery())
	{
		AddLineList();
		if(((CChildFrame *)GetParentFrame())->m_wndGridBar.IsVisible())
		{
			((CChildFrame *)GetParentFrame())->m_wndGridBar.DisplaySheetData(&GetDocument()->m_Data);
			GetDocument()->m_bLoadedSheet = TRUE;
		}
		else
		{
			GetDocument()->m_bLoadedSheet = FALSE;
		}
	}
}

//해당 채널을 선택하면 그래프 상에서 선택된 상태로 표시
void CCTSGraphAnalView::OnLineListSelectChanged() 
{
/*	CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)GetDocument();
//	int index = pDoc->m_iCurSelPlaneIndex;
	CPlane* pPlane;//     = pDoc->m_Data.GetPlaneAt(index);

	int nCount = 0;
	POSITION pos1 = pDoc->m_Data.GetHeadPlanePos();
	while(pos1)
	{
		pPlane = pDoc->m_Data.GetNextPlane(pos1);
			//
		POSITION pos = pPlane->GetLineHead();
		while(pos)
		{
			CLine* pLine = pPlane->GetNextLine(pos);
			pLine->SetSelect(m_LineListBox.GetSel(nCount++));
		}
	}

	CRect rect;
	GetClientRect(rect);
	CDC *pDC = GetDC();
	CRect ScopeRect = pDoc->m_Data.GetScopeRect(pDC, rect);
	ReleaseDC(pDC);
	InvalidateRect(ScopeRect);
*/
}

void CCTSGraphAnalView::OnLinePattern() 
{
	// TODO: Add your command handler code here
/*	int iCurSelLineIndex = 0;
	if(m_LineListBox.GetSelItems(1,&iCurSelLineIndex) == 1)	//가장 처음 선택된 라인을 구한다.
	{
		DWORD dwData = m_LineListBox.GetItemData(iCurSelLineIndex);
		OnToolsShowLinePattern(HIWORD(dwData), LOWORD(dwData));	//int nPlane, int nLine	
	}
*/
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(!pTree->ItemHasChildren(hItem))
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					GetDocument()->m_Data.ShowPatternOfChData(pLine->GetChPath());
					return;
				}
			}
		}
	}
}

void CCTSGraphAnalView::OnLineColor() 
{
//	CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetDocument());

	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree == NULL)	return;

	CLine *pLine = NULL;
	HTREEITEM hItem = pTree->GetSelectedItem();
	if(hItem != NULL)
	{
		if(pTree->ItemHasChildren(hItem))
		{
			hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
			pLine = (CLine *)pTree->GetItemData(hItem);
		}
		else
		{
			pLine = (CLine *)pTree->GetItemData(hItem);
		}
	}

// 	int nCount = m_LineListBox.GetSelCount();
// 	CArray<int,int> aryListBoxSel;
// 	aryListBoxSel.SetSize(nCount);
// 	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());
// 	if(nCount < 1)	return;
// 
// 	DWORD dwData = m_LineListBox.GetItemData(aryListBoxSel.GetAt(0));
// 	int nPlane = HIWORD(dwData);
// 	int nLine = LOWORD(dwData);
// 
// 	CPlane* pPlane     = pDoc->m_Data.GetPlaneAt(nPlane);
//	CLine *pLine = pPlane->GetLineAt(nLine);

	COLORREF colorLine;
	CColorDialog aDlg;
	if(pLine)
	{
		aDlg.m_cc.rgbResult=pLine->GetColor();
	}
	aDlg.m_cc.Flags|=CC_FULLOPEN|CC_RGBINIT;
	if(aDlg.DoModal()==IDOK)
	{
		colorLine = aDlg.GetColor();
	}
	else
		return;


/*	for(int i=0; i<aryListBoxSel.GetSize(); i++)
	{
		pLine  = (CLine *)aryListBoxSel.GetAt(i);
		if(pLine)
		{
			pLine->SetColor(colorLine);
		}
	}	
	AddLineList();
*/
	//////////////////////////////////////////////////////////////////////////
	hItem = pTree->GetSelectedItem();
	if(hItem != NULL)
	{
		if(pTree->ItemHasChildren(hItem))
		{
			hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
			while(hItem)
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pLine->SetColor(colorLine);
				}
				hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
			}
		}
		else
		{
			pLine = (CLine *)pTree->GetItemData(hItem);
			if(pLine)
			{
				pLine->SetColor(colorLine);
			}
		}
	}
	pTree->Invalidate();	//redraw tree
	((CCTSGraphAnalDoc*)GetDocument())->UpdateAllViews(NULL);
}
/*
void CCTSGraphAnalView::StandLinesInALine(CPlane* pPlane)
{
	if(m_iCurSelPlaneIndex!=CB_ERR)
	{
		m_LineListBox.ResetContent();
		POSITION pos = pPlane->GetLineHead();
		while(pos)
		{
			CLine* pLine = pPlane->GetNextLine(pos);
			CString str;
			str.Format("%s,%d,%d,%d,%d,%d",
				pLine->GetName(),
				GetRValue(pLine->GetColor()),
				GetGValue(pLine->GetColor()),
				GetBValue(pLine->GetColor()),
				pLine->GetWidth(),
				pLine->GetType());
			m_LineListBox.AddString(str);
		}
	}
}
*/
void CCTSGraphAnalView::SetLineWidth(int width)
{
/*	int nCount = m_LineListBox.GetSelCount();
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	for(int i=0; i<aryListBoxSel.GetSize(); i++)
	{
		CLine * pLine = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine)
		{
			pLine->SetWidth(width);
		}
	}
	AddLineList();
*/

	//////////////////////////////////////////////////////////////////////////
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pLine->SetWidth(width);
				}
			}
		}
		pTree->Invalidate();	//redraw tree
	}

	((CCTSGraphAnalDoc*)GetDocument())->UpdateAllViews(NULL);
}

void CCTSGraphAnalView::SetLineType(int type)
{
/*	int nCount = m_LineListBox.GetSelCount();
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());
	
	for(int i=0; i<aryListBoxSel.GetSize(); i++)
	{
		CLine *pLine = (CLine *)aryListBoxSel.GetAt(i);
		if(pLine)
		{
			pLine->SetType(type);
			pLine->SetDrawLine(TRUE);
		}
	}
	AddLineList();
*/
	
	//////////////////////////////////////////////////////////////////////////
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetType(type);
						pLine->SetDrawLine(TRUE);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pLine->SetType(type);
					pLine->SetDrawLine(TRUE);
				}
			}
		}
		pTree->Invalidate();	//redraw tree
	}

	((CCTSGraphAnalDoc*)GetDocument())->UpdateAllViews(NULL);
}

void CCTSGraphAnalView::OnLineWidth1() 
{
	// TODO: Add your command handler code here
	SetLineWidth(1);
}

void CCTSGraphAnalView::OnLineWidth2() 
{
	// TODO: Add your command handler code here
	SetLineWidth(2);
	
}

void CCTSGraphAnalView::OnLineWidth3() 
{
	// TODO: Add your command handler code here
	SetLineWidth(3);
	
}

void CCTSGraphAnalView::OnLineWidth4() 
{
	// TODO: Add your command handler code here
	SetLineWidth(4);
	
}

void CCTSGraphAnalView::OnLineWidth5() 
{
	// TODO: Add your command handler code here
	SetLineWidth(5);
}

void CCTSGraphAnalView::OnLineTypeSolid() 
{
	// TODO: Add your command handler code here
	SetLineType(PS_SOLID);
}

void CCTSGraphAnalView::OnLineTypeDot() 
{
	// TODO: Add your command handler code here
	SetLineType(PS_DOT);
}

void CCTSGraphAnalView::OnLineTypeDashdotdot() 
{
	// TODO: Add your command handler code here
	SetLineType(PS_DASHDOTDOT);	
}

void CCTSGraphAnalView::OnLineTypeDashdot() 
{
	// TODO: Add your command handler code here
	SetLineType(PS_DASHDOT);	
}

void CCTSGraphAnalView::OnLineTypeDash() 
{
	// TODO: Add your command handler code here
	SetLineType(PS_DASH);
}

void CCTSGraphAnalView::OnUpdateLineWidth1(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	int nCount = m_LineListBox.GetSelCount();
	if(nCount < 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<nCount; i++)
	{
		pLine  = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine &&  pLine->GetWidth() == 1)
		{
			pCmdUI->SetCheck(TRUE);
		}
		else
		{
			pCmdUI->SetCheck(FALSE);
		}
	}	
*/

	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine &&  pLine->GetWidth() == 1)
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineWidth2(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	int nCount = m_LineListBox.GetSelCount();
	if(nCount < 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<nCount; i++)
	{
		pLine  = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine && pLine->GetWidth() == 2)
		{
			pCmdUI->SetCheck(TRUE);
		}
		else
		{
			pCmdUI->SetCheck(FALSE);
		}
	}	
*/

	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine &&  pLine->GetWidth() == 2)
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineWidth3(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	int nCount = m_LineListBox.GetSelCount();
	if(nCount < 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<nCount; i++)
	{
		pLine  = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine && pLine->GetWidth() == 3)
		{
			pCmdUI->SetCheck(TRUE);
		}
		else
		{
			pCmdUI->SetCheck(FALSE);
		}
	}	
*/
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine &&  pLine->GetWidth() == 3)
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineWidth4(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	int nCount = m_LineListBox.GetSelCount();
	if(nCount < 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<nCount; i++)
	{
		pLine  = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine && pLine->GetWidth() == 4)
		{
			pCmdUI->SetCheck(TRUE);
		}
		else
		{
			pCmdUI->SetCheck(FALSE);
		}
	}	
*/
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine &&  pLine->GetWidth() == 4)
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineWidth5(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	int nCount = m_LineListBox.GetSelCount();
	if(nCount < 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<nCount; i++)
	{
		pLine  = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine && pLine->GetWidth() == 5)
		{
			pCmdUI->SetCheck(TRUE);
		}
		else
		{
			pCmdUI->SetCheck(FALSE);
		}
	}
*/
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine &&  pLine->GetWidth() == 5)
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);	
}

void CCTSGraphAnalView::OnUpdateLineTypeSolid(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	int nCount = m_LineListBox.GetSelCount();
	if(nCount < 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<nCount; i++)
	{
		pLine  = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine && pLine->GetType() == PS_SOLID)
		{
			pCmdUI->SetCheck(TRUE);
		}
		else
		{
			pCmdUI->SetCheck(FALSE);
		}
	}		
*/
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine && pLine->GetType() == PS_SOLID)
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);
}

//굵기가 1Pix 이상인 선에서는 선의 종류를 지원하지 않음
void CCTSGraphAnalView::OnUpdateLineTypeDot(CCmdUI* pCmdUI) 
{
		pCmdUI->Enable(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineTypeDashdotdot(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
		pCmdUI->Enable(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineTypeDashdot(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
		pCmdUI->Enable(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineTypeDash(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
		pCmdUI->Enable(FALSE);
}

void CCTSGraphAnalView::OnUpdateLineColor(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(m_LineListBox.GetSelCount());		
}

void CCTSGraphAnalView::OnUpdateLinePattern(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
///	if(m_LineListBox.GetSelCount() != 1)
//		pCmdUI->Enable(FALSE);		

	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(!pTree->ItemHasChildren(hItem))
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pCmdUI->Enable(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->Enable(FALSE);

	
}

void CCTSGraphAnalView::OnUpdateLineTracking(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	if(m_LineListBox.GetSelCount() != 1)
//		pCmdUI->Enable(FALSE);	
	
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(!pTree->ItemHasChildren(hItem))
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pCmdUI->Enable(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->Enable(FALSE);
}

void CCTSGraphAnalView::DestoryMouseTraceWnd()
{
	
	if(m_pMouseTrackChannelWnd)
	{
		m_pMouseTrackChannelWnd->DestroyWindow();
		delete m_pMouseTrackChannelWnd;
		m_pMouseTrackChannelWnd = NULL;
	}
}

void CCTSGraphAnalView::OnReload() 
{
	// TODO: Add your command handler code here
	((CCTSGraphAnalDoc *)GetDocument())->ReloadData();
	
	if(((CChildFrame *)GetParentFrame())->m_wndGridBar.IsVisible())
	{
		((CChildFrame *)GetParentFrame())->m_wndGridBar.DisplaySheetData(&GetDocument()->m_Data);
		GetDocument()->m_bLoadedSheet = TRUE;
	}
}

void CCTSGraphAnalView::OnAutoRedraw() 
{
	// TODO: Add your command handler code here
	m_bAutoReDraw = !m_bAutoReDraw;
	TRACE("Redraw %d\n", m_bAutoReDraw);

	if(m_bAutoReDraw)	SetTimer(100, 60000, NULL);
	else KillTimer(100);
}

void CCTSGraphAnalView::OnUpdateAutoRedraw(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_bAutoReDraw);
}

void CCTSGraphAnalView::OnDataDetailView() 
{
	// TODO: Add your command handler code here
		// TODO: Add your command handler code here	
	//선택채널의 결과 Data 폴더명을 모은다.
	//폴더명1\n폴더명2\n....으로 전송('\n'으로 폴더들을 구별하여 보낸다.)

/*	int nCount = m_LineListBox.GetSelCount();
	CArray<int,int> aryListBoxSel;

	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData()); 

	// Dump the selection array.
	#ifdef _DEBUG
	   afxDump << aryListBoxSel;
	#endif

	CLine *pLine;
	DWORD dwData;
	CString strData, strTemp1;
	CStringList strChList;
	for(int i=0; i<aryListBoxSel.GetSize(); i++ )
	{
		pLine = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine)
		{
			BOOL bFind = FALSE;
			POSITION pos = strChList.GetHeadPosition();
			while(pos)
			{
				strTemp1 = strChList.GetNext(pos);
				if(strTemp1 ==  pLine->GetChPath())	//이미 추가 되어 있으면 
				{
					bFind = TRUE;
					break;
				}
			}
			if(bFind == FALSE)
			{
				strTemp1 = pLine->GetChPath();
				strChList.AddTail(strTemp1);
				strData = strData + strTemp1 +"\n";
			}
		}
	}
*/

	//////////////////////////////////////////////////////////////////////////
	int nIndex = 0;
	CString strData, strTemp1;
	CStringList strChList;
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						BOOL bFind = FALSE;
						POSITION pos = strChList.GetHeadPosition();
						while(pos)
						{
							strTemp1 = strChList.GetNext(pos);
							if(strTemp1 ==  pLine->GetChPath())	//이미 추가 되어 있으면 
							{
								bFind = TRUE;
								break;
							}
						}
						if(bFind == FALSE)
						{
							strTemp1 = pLine->GetChPath();
							strChList.AddTail(strTemp1);
							strData = strData + strTemp1 +"\n";
						}
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					strData = pLine->GetChPath() +"\n";
				}
			}
		}
		pTree->Invalidate();	//redraw tree
	}

	if(strData.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[5]);
		return;
	}

	nIndex = strData.GetLength()+1;
	char *pData = new char[nIndex];
	sprintf(pData, "%s", strData);
	pData[nIndex-1] = '\0';
	COPYDATASTRUCT CpStructData;
	CpStructData.dwData = 3;		//Data View 구별 Index
	CpStructData.cbData = nIndex;
	CpStructData.lpData = pData;

//	TRACE("Send File %s\n", pData);
		
	::SendMessage(AfxGetMainWnd()->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
	delete [] pData;
}

void CCTSGraphAnalView::OnMarkDataPoint() 
{
	// TODO: Add your command handler code here
/*	CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetDocument());

	int nCount = m_LineListBox.GetSelCount();
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<aryListBoxSel.GetSize(); i++)
	{
		pLine = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine)
			pLine->SetMarkDataPoint(TRUE);
	}	
*/
	//////////////////////////////////////////////////////////////////////////
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetMarkDataPoint(TRUE);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pLine->SetMarkDataPoint(TRUE);
				}
			}
		}
		pTree->Invalidate();	//redraw tree
	}

	((CCTSGraphAnalDoc*)GetDocument())->UpdateAllViews(NULL);	
}

void CCTSGraphAnalView::OnMarkDataPointNone() 
{
	// TODO: Add your command handler code here
/*	CCTSGraphAnalDoc* pDoc = (CCTSGraphAnalDoc*)(GetDocument());

	int nCount = m_LineListBox.GetSelCount();
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;// = pPlane->GetLineAt(nLine);
	for(int i=0; i<aryListBoxSel.GetSize(); i++)
	{
		pLine = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine)
			pLine->SetMarkDataPoint(FALSE);
	}
*/
	
	//////////////////////////////////////////////////////////////////////////
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetMarkDataPoint(TRUE);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pLine->SetMarkDataPoint(FALSE);
				}
			}
		}
		pTree->Invalidate();	//redraw tree
	}

	((CCTSGraphAnalDoc*)GetDocument())->UpdateAllViews(NULL);	
}

void CCTSGraphAnalView::OnLineNone() 
{
	// TODO: Add your command handler code here
	SetLineType(PS_NULL);
}

void CCTSGraphAnalView::OnUpdateLineNone(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	int nCount = m_LineListBox.GetSelCount();
	if(nCount < 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
	CArray<int,int> aryListBoxSel;
	aryListBoxSel.SetSize(nCount);
	m_LineListBox.GetSelItems(nCount, aryListBoxSel.GetData());

	CLine *pLine;
	for(int i=0; i<nCount; i++)
	{
		pLine  = (CLine *)m_LineListBox.GetItemData(aryListBoxSel.GetAt(i));
		if(pLine && pLine->GetType() == PS_NULL)
		{
			pCmdUI->SetCheck(TRUE);
		}
		else
		{
			pCmdUI->SetCheck(FALSE);
		}
	}	
*/
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine && pLine->GetType() == PS_NULL)
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);
}

void CCTSGraphAnalView::OnUpdateMarkDataPointNone(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(m_LineListBox.GetSelCount());

	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine &&  !pLine->IsDataPointMark())
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);	

}

void CCTSGraphAnalView::OnUpdateMarkDataPoint(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(m_LineListBox.GetSelCount());				
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(pTree->ItemHasChildren(hItem))
			{
/*				hItem = pTree->GetNextItem(hItem, TVGN_CHILD);
				while(hItem)
				{
					pLine = (CLine *)pTree->GetItemData(hItem);
					if(pLine)
					{
						pLine->SetWidth(width);
					}
					hItem = pTree->GetNextItem(hItem, TVGN_NEXT);
				}
*/
			}
			else
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine && pLine->IsDataPointMark())
				{
					pCmdUI->SetCheck(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->SetCheck(FALSE);
}

void CCTSGraphAnalView::OnAxisSetting() 
{
	// TODO: Add your command handler code here
	CGraphSetDlg dlg((CCTSGraphAnalDoc *)GetDocument(), this);

	if(dlg.DoModal() == IDOK)
	{		
	}
}

void CCTSGraphAnalView::OnLogView() 
{
	// TODO: Add your command handler code here
/*	int iCurSelLineIndex = 0;
	if(m_LineListBox.GetSelItems(1,&iCurSelLineIndex) == 1)	//가장 처음 선택된 라인을 구한다.
	{
		DWORD dwData = m_LineListBox.GetItemData(iCurSelLineIndex);
		CPlane *pPlane = GetDocument()->m_Data.GetPlaneAt(HIWORD(dwData));
		if(pPlane)
		{
			CLine* pLine = pPlane->GetLineAt(LOWORD(dwData));
			if(pLine)
			{
				CChData* pChData = GetDocument()->m_Data.GetChData(pLine->GetChPath());
				if(pChData!=NULL)
				{
					CString strName = pChData->GetLogFileName();
					if(strName.IsEmpty())	
					{
						AfxMessageBox("Log 파일을 찾을 수 없습니다.");
						return;
					}

					STARTUPINFO	stStartUpInfo;
					PROCESS_INFORMATION	ProcessInfo;
					ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
					ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
						
					stStartUpInfo.cb = sizeof(STARTUPINFO);
					stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
					stStartUpInfo.wShowWindow = SW_NORMAL;

					CString strTemp;
					strTemp.Format("notepad.exe %s", strName);
					
					BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
					if(bFlag == FALSE)
					{
						strTemp.Format("%s를 Open할 수 없습니다.", strName);
						AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
					}				
				}
			}
		}
	}	
*/
	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(!pTree->ItemHasChildren(hItem))
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					CChData* pChData = GetDocument()->m_Data.GetChData(pLine->GetChPath());
					if(pChData!=NULL)
					{
						CString strName = pChData->GetLogFileName();
						if(strName.IsEmpty())	
						{
							AfxMessageBox(TEXT_LANG[6]);
							return;
						}

						STARTUPINFO	stStartUpInfo;
						PROCESS_INFORMATION	ProcessInfo;
						ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
						ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
							
						stStartUpInfo.cb = sizeof(STARTUPINFO);
						stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
						stStartUpInfo.wShowWindow = SW_NORMAL;

						CString strTemp;
						strTemp.Format("notepad.exe %s", strName);
						
						BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
						if(bFlag == FALSE)
						{
							strTemp.Format(TEXT_LANG[7], strName);
							AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
						}
					}
				}
			}
		}
	}	
}

void CCTSGraphAnalView::OnUpdateLogView(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	if(m_LineListBox.GetSelCount() != 1)
//		pCmdUI->Enable(FALSE);			

	CTreeCtrl *pTree = ((CChildFrame *)GetParentFrame())->GetTreeWnd();
	if(pTree)
	{
		HTREEITEM hItem = pTree->GetSelectedItem();
		if(hItem != NULL)
		{
			CLine *pLine;
			if(!pTree->ItemHasChildren(hItem))
			{
				pLine = (CLine *)pTree->GetItemData(hItem);
				if(pLine)
				{
					pCmdUI->Enable(TRUE);
					return;
				}
			}
		}
	}
	pCmdUI->Enable(FALSE);
}

void CCTSGraphAnalView::OnDataSaveWide() 
{
	// TODO: Add your command handler code here
	CGridBar *pWndGridBar = &((CChildFrame *)GetParentFrame())->m_wndGridBar;
	
	UINT iRowCount;
	iRowCount = pWndGridBar->Fun_GetTotalRow();
	if (iRowCount < 1) return;
	
	// 	if(((CChildFrame *)GetParentFrame())->m_wndGridBar.IsVisible())
	// 	{
	// 		((CChildFrame *)GetParentFrame())->m_wndGridBar.DisplaySheetData(&GetDocument()->m_Data);
	// 		GetDocument()->m_bLoadedSheet = TRUE;
	// 	}
	// 	else
	// 	{
	// 		GetDocument()->m_bLoadedSheet = FALSE;
	// 	}

	CString strFileName,strGetData;
	strFileName.Format("%s.csv", ((CCTSGraphAnalDoc *)GetDocument())->m_Data.GetTestName());
	CFileDialog aDlg(FALSE,"csv",strFileName,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"Excel supported Text File(*.csv)|*.csv|");
	aDlg.m_ofn.lpstrTitle="Save data as excel-supported file(*.csv)";
	//	aDlg.m_ofn.lpstrInitialDir=CCTSGraphAnalApp::m_strOriginFolderPath+"\\csvfiles\\";
	if(aDlg.DoModal()==IDOK)
	{
		strFileName = aDlg.GetPathName();
		//BOOL bRtn = FALSE;
		//bRtn = GetDocument()->m_Data.SaveDataAsFile(strFileName);
		CStdioFile afile;
		if(!afile.Open(strFileName,CFile::modeCreate|CFile::typeText|CFile::modeWrite))		return;
		
		int iRow;

		int nDataCnt = ((CCTSGraphAnalDoc *)GetDocument())->m_Data.GetPlaneCount();	// 표시되는 데이터의 종류

		for (iRow=0; iRow < iRowCount+1; iRow++)
		{
			strGetData = pWndGridBar->Fun_GetLineString(iRow, nDataCnt);
			afile.WriteString(strGetData);
		}
		afile.Close();
		
		// 		if(bRtn)
		// 		{
		//Excel로 Open 여부를 묻는다.
		CString strTemp1;
		strTemp1.Format(TEXT_LANG[4], strFileName);
		if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
		{
			((CCTSGraphAnalApp *)AfxGetApp())->ExecuteExcel(strFileName);
		}	
		
		//		}
	}	
}



BOOL CCTSGraphAnalView::LanguageinitMonConfig() 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSGraphAnalView"), _T("TEXT_CCTSGraphAnalView_CNT"), _T("TEXT_CCTSGraphAnalView_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt];

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_CCTSGraphAnalView_%d", i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_CCTSGraphAnalView"), strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Languge error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}