#if !defined(__GRIDBAR_H__)
#define __GRIDBAR_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// gridbar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGridBar window

#ifndef baseCMyBar
#define baseCMyBar CSizingControlBarG
#endif

#include "NewGridWnd.h"
class CGridBar : public baseCMyBar
{
// Construction
public:
	CGridBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	void DisplaySheetData(CData *pData);
	void SelectDataRange(int nStartIndex, int nEndIndex, int nColumn = -1);
	CWnd * GetClientWnd();
	UINT Fun_GetTotalCol();
	UINT Fun_GetTotalRow();
	CString Fun_GetLineString(UINT iLine, UINT iDataCnt = 0 );		// iCh:0 => All
	virtual ~CGridBar();

protected:
	CMyGridWnd m_GridWnd;
	CEdit	m_wndChild;
	CFont	m_font;

	// Generated message map functions
protected:
	//{{AFX_MSG(CGridBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(__GRIDBAR_H__)
