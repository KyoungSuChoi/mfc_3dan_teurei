// AdpTreeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "CTSGraphAnal.h"
#include "AdpTreeCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAdpTreeCtrl

CAdpTreeCtrl::CAdpTreeCtrl()
{
}

CAdpTreeCtrl::~CAdpTreeCtrl()
{
}


BEGIN_MESSAGE_MAP(CAdpTreeCtrl, CTreeCtrl)
	//{{AFX_MSG_MAP(CAdpTreeCtrl)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_KEYDOWN()	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdpTreeCtrl message handlers

void CAdpTreeCtrl::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	CPoint myPoint;
	GetCursorPos(&myPoint); 
	ScreenToClient(&myPoint);
	HTREEITEM hit = HitTest(myPoint);

	// 선택되게 하는 법
	if(hit == NULL || SelectItem(hit) == FALSE)	return;

	// 이미 선택되어 있을 경우만 메뉴 띠우는 법
	if(GetSelectedItem() == hit)
	{
		 // 메뉴를 띠운다.
		CMenu menu; 
		menu.LoadMenu(IDR_CONTEXT_MENU); 
		CMenu* popmenu = menu.GetSubMenu(1); 
		GetCursorPos(&myPoint); 
		popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, myPoint.x, myPoint.y, AfxGetMainWnd()); 
	}
	*pResult = 0;
}
/*
void CAdpTreeCtrl::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here

	// Create a memory DC compatible with the paint DC
	CDC memDC;
	memDC.CreateCompatibleDC(&dc);

	CRect rcClip, rcClient;
	dc.GetClipBox( &rcClip );
	GetClientRect(&rcClient);

	// Select a compatible bitmap into the memory DC
	CBitmap bitmap;
	bitmap.CreateCompatibleBitmap( &dc, rcClient.Width(), rcClient.Height() );
	memDC.SelectObject( &bitmap );
	
	// Set clip region to be same as that in paint DC
	CRgn rgn;
	rgn.CreateRectRgnIndirect( &rcClip );
	memDC.SelectClipRgn(&rgn);
	rgn.DeleteObject();
	
	// First let the control do its default drawing.
	CWnd::DefWindowProc(WM_PAINT, (WPARAM)memDC.m_hDC, 0);

	HTREEITEM hItem = GetFirstVisibleItem();

	int iItemCount = GetVisibleCount() + 1;
	while(hItem && iItemCount--)
	{		
		CRect rect;

		// Do not meddle with selected items or drop highlighted items
		UINT selflag = TVIS_DROPHILITED | TVIS_SELECTED;
	
		//if ( !(GetTreeCtrl().GetItemState( hItem, selflag ) & selflag ) 
		//	&& m_mapColorFont.Lookup( hItem, cf ))
		
		if ((GetItemState(hItem, selflag) & selflag) 
			&& ::GetFocus() == m_hWnd)
			;
		else 
		{
//			CFont *pFontDC;
//			CFont fontDC;
//			LOGFONT logfont;

			memDC.SetTextColor(RGB(255, 0, 0));
//			memDC.SetTextColor(GetSysColor(COLOR_WINDOWTEXT));


			CString sItem = GetItemText(hItem);

			GetItemRect(hItem, &rect, TRUE);
			memDC.SetBkColor( GetSysColor(COLOR_WINDOW));
			memDC.TextOut(rect.left + 2, rect.top + 1, sItem);
			memDC.MoveTo(rect.right+10, rect.top+rect.Height()/2);
			memDC.LineTo(rect.right+ 100, rect.top+rect.Height()/2);
			
//			memDC.SelectObject(pFontDC);
		}
		hItem = GetNextVisibleItem(hItem);
	}


	dc.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &memDC, 
				rcClip.left, rcClip.top, SRCCOPY);

	memDC.DeleteDC();	
	// Do not call CTreeCtrl::OnPaint() for painting messages
}
*/

void CAdpTreeCtrl::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
 	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	if(pNMTreeView->itemOld.hItem != NULL)
	{
		CLine *pLine;
		if(!ItemHasChildren(pNMTreeView->itemOld.hItem))
		{
			pLine = (CLine *)GetItemData(pNMTreeView->itemOld.hItem);
			if(pLine)
			{
				pLine->SetSelect(FALSE);
			}
		}
	}	
	
//	HTREEITEM hItem = GetSelectedItem();
	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
//	ASSERT(hItem == pNMTreeView->itemNew.hItem);
	if(hItem != NULL)
	{
		CLine *pLine;
		if(!ItemHasChildren(hItem))
		{
			pLine = (CLine *)GetItemData(hItem);
			if(pLine)
			{
				pLine->SetSelect(TRUE);
			}
		}
	}	

	GetParentFrame()->Invalidate(FALSE);
	*pResult = 0;
}

void CAdpTreeCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
    // TODO: Add your message handler code here and/or call default
    UINT uFlags=0;
    HTREEITEM hItem = HitTest(point, &uFlags);
	if(hItem == NULL)
	{
		CTreeCtrl::OnLButtonDown(nFlags, point);
		return;
	}
	SelectItem(hItem);
	
	//check 표시 때문에 먼저 처리한다.
	CTreeCtrl::OnLButtonDown(nFlags, point);
	
	CLine *pLine;
	BOOL bCheck = GetCheck(hItem);
	
	if(!ItemHasChildren(hItem))		//하위 select
	{
		pLine = (CLine *)GetItemData(hItem);
		if(pLine)
		{
			pLine->ShowLine(bCheck);
		}
	}
	else	//상위 Select
	{
		hItem = GetNextItem(hItem, TVGN_CHILD);
		while(hItem)
		{
			pLine = (CLine *)GetItemData(hItem);
			SetCheck(hItem, bCheck);
			
			if(pLine)
			{
				pLine->ShowLine(bCheck);
			}
			hItem = GetNextItem(hItem, TVGN_NEXT);
		}
	}
	GetParentFrame()->Invalidate(FALSE);	
}

//20090911 KBH
void CAdpTreeCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	//check 표시 때문에 먼저 처리한다.
	CTreeCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
	
	if(nChar==VK_SPACE)
	{
		HTREEITEM hItem = GetSelectedItem();
		CLine *pLine;
		BOOL bCheck = GetCheck(hItem);
		
		if(!ItemHasChildren(hItem))		//하위 select
		{
			pLine = (CLine *)GetItemData(hItem);
			if(pLine)
			{
				pLine->ShowLine(bCheck);
			}
		}
		else	//상위 Select
		{
			hItem = GetNextItem(hItem, TVGN_CHILD);
			while(hItem)
			{
				pLine = (CLine *)GetItemData(hItem);
				SetCheck(hItem, bCheck);
				
				if(pLine)
				{
					pLine->ShowLine(bCheck);
				}
				hItem = GetNextItem(hItem, TVGN_NEXT);
			}
		}
		GetParentFrame()->Invalidate(FALSE);	
	}
}
