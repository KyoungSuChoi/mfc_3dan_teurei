# Microsoft Developer Studio Project File - Name="CTSGraphAnal" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CTSGraphAnal - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CTSGraphAnal.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CTSGraphAnal.mak" CFG="CTSGraphAnal - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CTSGraphAnal - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CTSGraphAnal - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CTSGraphAnal - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../Lib/"
# PROP Intermediate_Dir "../../obj/CTSGraphAnal_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"../../../../bin/Release/CTSGraphAnalyzer.exe"

!ELSEIF  "$(CFG)" == "CTSGraphAnal - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../Lib/"
# PROP Intermediate_Dir "../../obj/CTSGraphAnal_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W4 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../../../bin/Debug/CTSGraphAnalyzer.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "CTSGraphAnal - Win32 Release"
# Name "CTSGraphAnal - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AdpTreeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\CellInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSGraphAnal.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSGraphAnal.rc
# End Source File
# Begin Source File

SOURCE=.\CTSGraphAnalDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSGraphAnalView.cpp
# End Source File
# Begin Source File

SOURCE=.\DataDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\DataFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\DataView.cpp
# End Source File
# Begin Source File

SOURCE=.\ErrorCodeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FolderDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GridBar.cpp
# End Source File
# Begin Source File

SOURCE=.\HelpDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\HelpFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\HelpView.cpp
# End Source File
# Begin Source File

SOURCE=.\LineListBox.cpp
# End Source File
# Begin Source File

SOURCE=.\LogDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MonitorDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\MonitorFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\MonitorView.cpp
# End Source File
# Begin Source File

SOURCE=.\NetStateMonitoringDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NewGridWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\PasswordDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PlaneSettingDlgBar.cpp
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.cpp
# End Source File
# Begin Source File

SOURCE=.\RangeSelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\scbarcf.cpp
# End Source File
# Begin Source File

SOURCE=.\scbarg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelMdlDlgBar.cpp
# End Source File
# Begin Source File

SOURCE=.\SetNetworkDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\sizecbar.cpp
# End Source File
# Begin Source File

SOURCE=.\Splash.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TextParsing.cpp
# End Source File
# Begin Source File

SOURCE=.\TreeBar.cpp
# End Source File
# Begin Source File

SOURCE=.\UnitPathRecordset.cpp
# End Source File
# Begin Source File

SOURCE=.\UserSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Win32Process.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AdpTreeCtrl.h
# End Source File
# Begin Source File

SOURCE=.\CellInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\CTSGraphAnal.h
# End Source File
# Begin Source File

SOURCE=.\CTSGraphAnalDoc.h
# End Source File
# Begin Source File

SOURCE=.\CTSGraphAnalView.h
# End Source File
# Begin Source File

SOURCE=.\DataDoc.h
# End Source File
# Begin Source File

SOURCE=.\DataFrame.h
# End Source File
# Begin Source File

SOURCE=.\DataView.h
# End Source File
# Begin Source File

SOURCE=.\ErrorCodeDlg.h
# End Source File
# Begin Source File

SOURCE=.\FolderDialog.h
# End Source File
# Begin Source File

SOURCE=.\GraphSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\GridBar.h
# End Source File
# Begin Source File

SOURCE=.\HelpDoc.h
# End Source File
# Begin Source File

SOURCE=.\HelpFrame.h
# End Source File
# Begin Source File

SOURCE=.\HelpView.h
# End Source File
# Begin Source File

SOURCE=.\LineListBox.h
# End Source File
# Begin Source File

SOURCE=.\LogDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=..\COMMON\Ctrl\GridCtrl_src\MemDC.h
# End Source File
# Begin Source File

SOURCE=.\MonitorDoc.h
# End Source File
# Begin Source File

SOURCE=.\MonitorFrame.h
# End Source File
# Begin Source File

SOURCE=.\MonitorView.h
# End Source File
# Begin Source File

SOURCE=.\NetStateMonitoringDlg.h
# End Source File
# Begin Source File

SOURCE=.\NewGridWnd.h
# End Source File
# Begin Source File

SOURCE=.\PasswordDlg.h
# End Source File
# Begin Source File

SOURCE=.\PlaneSettingDlgBar.h
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.h
# End Source File
# Begin Source File

SOURCE=.\RangeSelDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\scbarcf.h
# End Source File
# Begin Source File

SOURCE=.\scbarg.h
# End Source File
# Begin Source File

SOURCE=.\SelMdlDlgBar.h
# End Source File
# Begin Source File

SOURCE=.\SetNetworkDlg.h
# End Source File
# Begin Source File

SOURCE=.\sizecbar.h
# End Source File
# Begin Source File

SOURCE=.\Splash.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TextParsing.h
# End Source File
# Begin Source File

SOURCE=.\TreeBar.h
# End Source File
# Begin Source File

SOURCE=.\UnitPathRecordset.h
# End Source File
# Begin Source File

SOURCE=.\UserSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\Win32Process.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=".\res\ADP�ΰ�.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cancelFocus.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cancelNormal.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cancelUp.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ch_sts_s.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CHART10.ICO
# End Source File
# Begin Source File

SOURCE=".\res\Computer colours.ico"
# End Source File
# Begin Source File

SOURCE=.\res\CTSGraphAnal.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSGraphAnal.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CTSGraphAnalDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\CYCLE.ICO
# End Source File
# Begin Source File

SOURCE=.\res\d0.ico
# End Source File
# Begin Source File

SOURCE=.\res\EnterDown.bmp
# End Source File
# Begin Source File

SOURCE=.\res\EnterFocus.bmp
# End Source File
# Begin Source File

SOURCE=.\res\EnterNormal.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Graph.ico
# End Source File
# Begin Source File

SOURCE=.\res\help1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon10.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon7.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_main.ico
# End Source File
# Begin Source File

SOURCE=.\res\Log.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MANUAL.ICO
# End Source File
# Begin Source File

SOURCE=.\res\Monitor.bmp
# End Source File
# Begin Source File

SOURCE=.\res\monitor1.bmp
# End Source File
# Begin Source File

SOURCE=".\res\net repair.ico"
# End Source File
# Begin Source File

SOURCE=.\res\Network.ico
# End Source File
# Begin Source File

SOURCE=.\res\PNELogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red_btn_.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Remote Access.ico"
# End Source File
# Begin Source File

SOURCE=".\res\server-hub.ico"
# End Source File
# Begin Source File

SOURCE=.\Splash16.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Spread.ico
# End Source File
# Begin Source File

SOURCE=.\res\state_ic.bmp
# End Source File
# Begin Source File

SOURCE=.\res\test_lis.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ToolDown.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ToolFocus.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ToolNormal.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VIEWER3.ICO
# End Source File
# Begin Source File

SOURCE=.\res\VIEWER4.ICO
# End Source File
# Begin Source File

SOURCE=.\res\YellowBar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
