// ProfileListInfo.cpp: implementation of the CProfileListInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataDown.h"
#include "ProfileListInfo.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProfileListInfo::CProfileListInfo()
{
}

CProfileListInfo::~CProfileListInfo()
{

}

BOOL CProfileListInfo::LoadData(CString strFile)
{
	m_strProfileInfoFileName = strFile;

	CString strResultFile;
	CStdioFile	file;
	if(!file.Open(strFile, CFile::modeRead|CFile::shareDenyNone))	
	{

#ifdef _DEBUG
		afxDump << "Configuration File Open Fail" << "\n";
#endif
		return FALSE;
	}
	
	CString buff;
	BOOL	bEOF;

	TRY
	{
		while(TRUE)
		{
			bEOF = file.ReadString(buff);
			if(!bEOF)	break;

			if(!buff.IsEmpty())
			{
				CString strConfig(buff);
				int a, b;
				a = strConfig.Find('[');
				b = strConfig.ReverseFind(']');

				if(a >=0 && b > 0 && a < b)
				{
					CString strTitle, strData;
					strTitle = strConfig.Mid(a+1, b-a-1);

					a = strConfig.ReverseFind('=');

					strData = strConfig.Mid( a+1 );

					strData.TrimLeft(" ");
					strData.TrimRight(" ");

					if(strTitle ==  "UNIT_IP")
					{
						m_strIpAddress =  strData;
					}	
					else if(strTitle == "TrayID")
					{
						m_strTrayID = strData;
					}
					else if(strTitle == "UNIT_ID")
					{
						m_lUnitID = atol(strData);
					}
					else if(strTitle == "Result")
					{
						m_strFmtFileName = strData;
					}
					else if(strTitle == "SaveDir")
					{
						m_strSaveDir = strData;
					}
					else if(strTitle == "UNIT_NAME")
					{
						m_strUnitName= strData;
					}
				}
			}
		}
		
	}
	CATCH (CFileException, e)
	{
		//AfxMessageBox(e->m_cause);
		file.Close();
		return FALSE;
	}
	END_CATCH

	file.Close();	
	return TRUE;	
}
