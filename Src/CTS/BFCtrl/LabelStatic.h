#if !defined(AFX_LABELSTATIC_H__E1485591_D0D2_11D3_A636_00105A7C2F91__INCLUDED_)
#define AFX_LABELSTATIC_H__E1485591_D0D2_11D3_A636_00105A7C2F91__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabelStatic.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLabelStatic window

#define	NM_LINKCLICK	WM_APP + 0x200

/////////////////////////////////////////////////////////////////////////////
// CLabelStatic window

class AFX_EXT_CLASS CLabelStatic : public CStatic
{
// Construction
public:
	enum FlashType {None, Text, Background };
	enum Type3D { Raised, Sunken};

	CLabelStatic();
	virtual CLabelStatic& SetBkColor(COLORREF crBkgnd);
	virtual CLabelStatic& SetTextColor(COLORREF crText);
	virtual CLabelStatic& SetText(const CString& strText);
	virtual CLabelStatic& SetFontBold(BOOL bBold);
	virtual CLabelStatic& SetFontName(const CString& strFont);
	virtual CLabelStatic& SetFontUnderline(BOOL bSet);
	virtual CLabelStatic& SetFontItalic(BOOL bSet);
	virtual CLabelStatic& SetFontSize(int nSize);
	virtual CLabelStatic& SetSunken(BOOL bSet);
	virtual CLabelStatic& SetBorder(BOOL bSet);
	virtual CLabelStatic& SetTransparent(BOOL bSet);
	virtual CLabelStatic& FlashText(BOOL bActivate);
	virtual CLabelStatic& FlashBackground(BOOL bActivate);
	virtual CLabelStatic& SetLink(BOOL bLink,BOOL bNotifyParent);
	virtual CLabelStatic& SetLinkCursor(HCURSOR hCursor);
	virtual CLabelStatic& SetFont3D(BOOL bSet,Type3D type=Raised);
	virtual CLabelStatic& SetRotationAngle(UINT nAngle,BOOL bRotation);


// Attributes
public:
protected:
	void ReconstructFont();
	COLORREF	m_crText;
	HBRUSH		m_hwndBrush;
	HBRUSH		m_hBackBrush;
	LOGFONT		m_lf;
	CFont		m_font;
	CString		m_strText;
	BOOL		m_bState;
	BOOL		m_bTimer;
	BOOL		m_bLink;
	BOOL		m_bTransparent;
	FlashType	m_Type;
	HCURSOR		m_hCursor;
	Type3D		m_3dType;
	BOOL		m_bFont3d;
	BOOL		m_bToolTips;
	BOOL		m_bNotifyParent;
	BOOL		m_bRotation;

	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLabelStatic)
	public:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLabelStatic();

	// Generated message map functions
protected:
	//{{AFX_MSG(CLabelStatic)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnPaint();
	afx_msg void OnSysColorChange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABELSTATIC_H__E1485591_D0D2_11D3_A636_00105A7C2F91__INCLUDED_)
