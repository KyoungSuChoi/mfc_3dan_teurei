// Channel.cpp: implementation of the CChannel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Channel.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
BOOL WriteALog(char *szLog)
{
	char szLogFileName[512];

	if(szLog == NULL)					return FALSE;

	SYSTEMTIME systemTime;   // system time
	::GetLocalTime(&systemTime);
	
	//주어진 Log File 명이 없으면 기본 파일명 생성
//	if(strlen(szLogFileName) == 0)
//	{
		sprintf(szLogFileName, ".\\log\\Debug.log", systemTime.wYear, systemTime.wMonth, systemTime.wDay);
//	}

	FILE *fp = fopen(szLogFileName, "ab+");
	if(fp == NULL)		return FALSE;
		
	TRACE("%s\n", szLog);
	fprintf(fp, "%02d/%02d/%02d %02d:%02d:%02d :: %s\r\n",	systemTime.wYear, systemTime.wMonth, systemTime.wDay,
												systemTime.wHour, systemTime.wMinute, systemTime.wSecond, 
												szLog);
	fclose(fp);
	return TRUE;
}
*/
CChannel::CChannel()
{
    m_wChIndex = 0;
	m_state = PS_STATE_IDLE;
    ZeroMemory(m_grade, sizeof(m_grade));
    m_cellCode  = 0;
	m_nStepNo = 0;
    m_ulStepTime = 0;
    m_ulTotalTime = 0;
    m_lVoltage = 0;
    m_lCurrent = 0;
    m_lWatt = 0;
    m_lWattHour = 0;
    m_lCapacity = 0;
    m_lImpedance = 0;
	m_lAvgVoltage = 0;
	m_lAvgCurrent = 0;
	m_wStepType = 0;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	m_lTemparature = 0;
	m_lPressure = 0;
	m_nCurrentCycleNum = 0;
	m_nTotalCycleNum = 0;
	m_lCapacitySum = 0;
	m_wSharingInfo = 0;
	m_nGotoCycleNum = 0;		//20081202 KHS

	memset(&m_StepStartDataInfo, 0, sizeof(SFT_MSEC_CH_DATA_INFO));

	//m_nRunMode = AfxGetApp()->GetProfileInt("Config", "RunMode", 1);
	//m_lStartCellCheckVoltageLimit = AfxGetApp()->GetProfileInt("Config", "NonCellLimit1", 500)*1000;
	//m_lEndCellCheckVoltageLimit = AfxGetApp()->GetProfileInt("Config", "NonCellLimit2", 500)*1000;

	m_nRunMode = GetProfileIntExt("Config", "RunMode", 1);
	m_lStartCellCheckVoltageLimit = GetProfileIntExt("Config", "NonCellLimit1", 500)*1000;
	m_lEndCellCheckVoltageLimit = GetProfileIntExt("Config", "NonCellLimit2", 500)*1000;

}

CChannel::~CChannel()
{
}

CChannel::operator =(const CChannel& chData)
{
	m_wChIndex =  chData.m_wChIndex;
	m_state = chData.m_state;
}

void CChannel::SetChannelData(SFT_CH_DATA chData)
{
//	char szLog[128];
//	sprintf(szLog, "befor data I:%d, C:%d :: Flag %d\n", chData.lCurrent, chData.lCapacity, m_nRunMode);
//	WriteALog(szLog);
	ConverChData(chData);	
//	sprintf(szLog, "befor data I:%d, C:%d\n", chData.lCurrent, chData.lCapacity);
//	WriteALog(szLog);

	//PC에서 Ready, End 상태를 만듬 2006/12/15
	//	m_state = chData.chState;
	//Ready 상태	:  Standby 상태에서 전압이 있을 경우 
	//End 상태		: 작업이 완료된 상태에서 전압이 있을 경우 
	//Run => Standby로 전이시 완료 상태로 만듬 
	if(m_state == PS_STATE_RUN  && chData.chState == PS_STATE_STANDBY)
	{
		m_state = PS_STATE_END;
	}

	//완료 상태에서 
	if(m_state == PS_STATE_END && chData.chState == PS_STATE_STANDBY)
	{
		//전압이 판단값보다 작으면 
		if(m_lVoltage < m_lStartCellCheckVoltageLimit)
		{
			//모듈 상태를 이용(Stanby)
			m_state = chData.chState;
		}
		else
		{
			//이전에 전압값이 판단값이상 이었는데 전압이 사라진 경우 
			if(chData.lVoltage < m_lStartCellCheckVoltageLimit)
			{
				//모듈 상태를 이용(Standby)
				m_state = chData.chState;
			}
		}
	}
	else
	{
		//모듈의 Channel 상태가 Standby가 아니면 현재 상태를 Update
		m_state = chData.chState;
	}

	//Standby 상태에서 
	if(m_state == PS_STATE_STANDBY)
	{
		//전압이 판단값 이상이면 Ready 상태로 변경
		if(m_lVoltage >= m_lEndCellCheckVoltageLimit)
		{
			m_state = PS_STATE_READY;
		}
	}
	//////////////////////////////////////////////////////////////////////////

	if(chData.chDataSelect == SFT_SAVE_STEP_END && chData.chStepType != PS_STEP_END)
	{
		m_grade[0] = chData.chGradeCode;
		TRACE("Grade : %d / %d (Select : %d)\r\n", m_grade[0], chData.chGradeCode, chData.chDataSelect);
	}
	
    m_cellCode = chData.chCode;
	m_nStepNo = chData.chStepNo;
    m_ulStepTime = chData.ulStepTime;
    m_ulTotalTime = chData.ulTotalTime;
    m_lVoltage = chData.lVoltage;

	m_lCurrent = chData.lCurrent;
//	m_lWatt = chData.lWatt;
	m_lWattHour = chData.lWattHour;
	m_lAvgVoltage = chData.lAvgVoltage;
	m_lAvgCurrent = chData.lAvgCurrent;
	m_wStepType = chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest

	m_wSharingInfo = chData.chSharingInfo;

	
	//Impedance, 용량 값은 최종값을 유지하도록 한다. 
	if(chData.chStepType == PS_STEP_CHARGE || chData.chStepType == PS_STEP_DISCHARGE || chData.chStepType == PS_STEP_IMPEDANCE)
	{
		//chData.chDataSelect == SFT_SAVE_STEP_END;
		m_lImpedance = chData.lImpedance;
		m_lCapacity = chData.lCapacity;
		TRACE("용량 : %ld / %ld\n", m_lCapacity, chData.lCapacity);
		m_lWatt = chData.lWatt;
		TRACE("Farad : %ld / %ld\n", m_lWatt, chData.lWatt);
		m_grade[0] = chData.chGradeCode;
		TRACE("Grade : %ld / %ld\n", m_grade[0], chData.chGradeCode);
	}
	//2006/12/14 SBC에서 값을 유지 하도록 수정
/*
#ifdef _EDLC_TEST_SYSTEM
	if(	chData.chDataSelect == SFT_SAVE_STEP_END && 
		(chData.chStepType == PS_STEP_CHARGE || chData.chStepType == PS_STEP_DISCHARGE))	
	{
		m_lCapacity = chData.lCapacity;
	}
#else
*/
	//m_lCapacity = chData.lCapacity;
//#endif
	
	m_lTemparature = chData.lTemparature;
	m_CmdReserved = chData.chReservedCmd;
//	m_lPressure = chData.lPressure;
//	chData.reserved
	m_nCurrentCycleNum = (WORD)chData.nCurrentCycleNum;
	m_nTotalCycleNum = (WORD)chData.nTotalCycleNum;
	m_nGotoCycleNum = (WORD)chData.wGotoCycleNum;		//20081202 KHS
}

long CChannel::GetVoltage()
{
	return m_lVoltage;
}

long CChannel::GetCurrent()
{
	return m_lCurrent;
}

long CChannel::GetCapacity()
{
	return m_lCapacity;
}

long CChannel::GetImpedance()
{
	return m_lImpedance;
}

WORD CChannel::GetState()
{
	return m_state;
}

UINT CChannel::GetStepTime()
{
	return m_ulStepTime;
}

BYTE CChannel::GetCode()
{
	return m_cellCode;
}

BYTE CChannel::GetGradeCode(int nItem)
{
	if(nItem < 0 && nItem >= PS_MAX_STEP_GRADE)
		return 0;

	return m_grade[nItem];
}

long CChannel::GetWatt()
{
	return m_lWatt;
}

long CChannel::GetWattHour()
{
	return m_lWattHour;
}

WORD CChannel::GetStepNo()
{
	return m_nStepNo;
}

UINT CChannel::GetTotalTime()
{
	return m_ulTotalTime;
}

long	CChannel::GetAvgCurrent()
{
	return m_lAvgCurrent;
}

long	CChannel::GetAvgVoltage()
{
	return m_lAvgVoltage;
}

long	CChannel::GetCapacitySum()
{
	return m_lCapacitySum;
}

long	CChannel::GetWattHourSum()
{
	return m_lWattHourSum;
}

long	CChannel::GetTemperature()
{
	return m_lTemparature;
}

long CChannel::GetPressure()
{
	return m_lPressure;
}


void CChannel::PushSaveData(SFT_CH_DATA chData)
{
	ConverChData(chData);
	
	//Critical section이 필요 할 것으로 생각됨 
	cs.Lock();

	if( GetSaveDataStackSize() < MAX_DATA_STACK_SIZE)
	{
		m_aSaveDataList.AddTail(chData);
//		TRACE("Pushed data to stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
	}

#ifdef _DEBUG
	else
	{
		TRACE("Channel %d data stack overflow!!!!!!!!!!!!\n", m_wChIndex+1);
	}
#endif

	cs.Unlock();
}

 BOOL CChannel::PopSaveData(SFT_CH_DATA &chData)
{
	int nRtn = FALSE;
	
	//critical section
	cs.Lock();
	if(GetSaveDataStackSize() > 0 )
	{
		POSITION pos = m_aSaveDataList.GetHeadPosition();
		if(pos)
		{
			chData = m_aSaveDataList.GetAt(pos);
			m_aSaveDataList.RemoveAt(pos);
			nRtn = TRUE;

//			TRACE("Poped data form stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
		}
	}
	cs.Unlock();

	return nRtn;
}

UINT CChannel::GetSaveDataStackSize()
{
	return m_aSaveDataList.GetCount();
}

WORD CChannel::GetTotalCycleCount()
{
	return m_nTotalCycleNum;
}

WORD CChannel::GetCurCycleCount()
{
	return m_nCurrentCycleNum;
}

WORD CChannel::GetStepType()
{
	return m_wStepType;
}

void CChannel::ResetData()
{
	m_state = PS_STATE_IDLE;
    ZeroMemory(m_grade, sizeof(m_grade));
    m_cellCode  = 0;
	m_nStepNo = 0;
    m_ulStepTime = 0;
    m_ulTotalTime = 0;
    m_lVoltage = 0;
    m_lCurrent = 0;
    m_lWatt = 0;
    m_lWattHour = 0;
    m_lCapacity = 0;
    m_lImpedance = 0;
	m_lAvgVoltage = 0;
	m_lAvgCurrent = 0;
	m_wStepType = 0;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	m_lTemparature = 0;
	m_lPressure = 0;
	m_nCurrentCycleNum = 0;
	m_nTotalCycleNum = 0;
	m_lCapacitySum = 0;
	m_lWattHourSum = 0;
	m_wSharingInfo = 0;
	m_nGotoCycleNum = 0;			//20081202 KHS
	
	m_aSaveDataList.RemoveAll();   // List of Save data
}

//최대 100msec로 1분가능 
BOOL CChannel::SetStepStartData(SFT_MSEC_CH_DATA_INFO *pInfo, LPVOID lpData)
{
	if(pInfo->lDataCount < 1)
	{
		return TRUE;
	}

	if(pInfo->lDataCount > MAX_MSEC_DATA_POINT)
	{
		pInfo->lDataCount = MAX_MSEC_DATA_POINT;
	}
	int nBuffSize = pInfo->lDataCount * sizeof(SFT_MSEC_CH_DATA);

	memcpy(m_aStepStartData, lpData, nBuffSize);
	memcpy(&m_StepStartDataInfo, pInfo, sizeof(SFT_MSEC_CH_DATA_INFO));
	
	return TRUE;
}

//convert data to run mode
void CChannel::ConverChData(SFT_CH_DATA &chData)
{
	if(m_nRunMode == 0 )	//
	{
		if( chData.chStepType == PS_STEP_CHARGE		|| 
			chData.chStepType == PS_STEP_DISCHARGE	|| 
			chData.chStepType == PS_STEP_PATTERN	||
			(chData.chStepType == PS_STEP_IMPEDANCE && chData.chMode == PS_MODE_DCIMP)
		)
		{
		}
		else
		{
			chData.lCapacity = 0;
			chData.lCurrent = 0;
			chData.lWatt = 0;
			chData.lWattHour = 0;
			chData.lAvgVoltage = 0;
			chData.lAvgCurrent = 0;
		}

		if(chData.chState == PS_STATE_IDLE || chData.chState == PS_STATE_STANDBY)
		{
			chData.lCurrent = 0;
			chData.lWatt = 0;
		}

		if(chData.lImpedance > 1000000000 || chData.lImpedance < 0)	//0 ~ 1KOhm
			chData.lImpedance = 0;
	}
}


int CChannel::GetProfileIntExt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault)
{
	ULONG keySize=4;
    unsigned char bszStr =0 ;
    LONG lreturn;
    HKEY hKey;
    CString SubFolder;
    CString Folder;
    DWORD temp;

	Folder = PS_REGISTRY_CTSMONPRO_NAME;

    SubFolder.Format("%s%s%s", Folder, _T("\\"), lpszSection);
    
    if(Folder != "")
    {
        lreturn=RegOpenKey(HKEY_CURRENT_USER, Folder, &hKey);
        if(lreturn!=ERROR_SUCCESS)    

        {
            return nDefault;
        }
    }
    
    if(SubFolder != "")
    {
        //HKEY_CURRENT_USER 에서 "SOFTWARE\\aaa\\(입력 SUB폴더값) " 열기
        lreturn=RegOpenKey(HKEY_CURRENT_USER, SubFolder, &hKey);
        if(lreturn!=ERROR_SUCCESS)    
        {
            return nDefault;
        }
    }

    // "SOFTWARE\\aaa\\(입력 폴더값) " 에 입력key 에 대한  출력 value 값 읽기 
    //lreturn = RegQueryValueEx(hKey, (LPSTR)lpszEntry, NULL, NULL, &bszStr, &keySize);
	DWORD dwValue;
	DWORD dwType;
	DWORD dwCount = sizeof(DWORD);
	lreturn = RegQueryValueEx(hKey, (LPTSTR)lpszEntry, NULL, &dwType,
			(LPBYTE)&dwValue, &dwCount);
    if(lreturn!=ERROR_SUCCESS)
    {
        return nDefault;
    }
    
    temp = dwValue;
    
 //닫기
    RegCloseKey(hKey);
    return temp;    

}

WORD CChannel::GetSharingInfo()
{
	return m_wSharingInfo;
}

WORD CChannel::GetGotoCycleNum()
{
	return m_nGotoCycleNum;
}
