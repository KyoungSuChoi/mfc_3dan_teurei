// PSServer.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "PSServer.h"

#include "Worker.h"
#include "ThreadDispatcher.h"
#include "Module.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CPSServerApp

BEGIN_MESSAGE_MAP(CPSServerApp, CWinApp)
	//{{AFX_MSG_MAP(CPSServerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSServerApp construction

CPSServerApp::CPSServerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	
}

CPSServerApp::~CPSServerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
//	this->m_pszProfileName = "PSServerD";
	
}
/////////////////////////////////////////////////////////////////////////////
// The one and only CPSServerApp object

CPSServerApp theApp;

//#ifndef _SFT_FORM_DLL_FTN
//#define _SFT_FORM_DLL_FTN

int		nErrorNumber = SFT_ERR_EMPTY;

/*

#ifdef _DEBUG
#pragma comment(lib, "PowerFormD.lib")
#pragma message("Automatically linking with PowerFormD.lib By K.B.H ")
#else
#pragma comment(lib, "PowerForm.lib")
#pragma message("Automatically linking with PowerForm.lib By K.B.H")
#endif	//_DEBUG

#include <epdlldef.h>

#endif	//_SFT_FORM_DLL_FTN
*/


//Shared Data 
#pragma data_seg("SHARDATA")

int nInstanceCount  = 0;			//Module Struct

#pragma data_seg()


int		m_Initialized = FALSE;
BOOL	m_bServerOpen = FALSE;
int		m_nInstalledModuleNum = 0;
CModule	*m_lpModule = NULL;
char	szLogFileName[512] = {""};
char	szErrorString[512];
char	szServerIP[32]; 
static	HINSTANCE	hInstance;				

static	Worker		m_worker;
static	CWizThreadDispatcher	*m_pDispather = NULL;

HANDLE hMutexInstance;

//Channel Mapping type of Monitoring Data 
//0: Use wChIndex( the member variable of SFT_CH_DATA)
//1: Network incomming Squence 								
//2: Read Channel Mapping Table from "formmapping.map" File

int		g_nChMappingType	= SFT_CH_INDEX_MEMBER;
int		g_anChannelMap[SFT_MAX_CH_PER_MD];
	
//#include "afxmt.h"

//extern CCriticalSection critic;
BOOL	InitGroupData (int nModuleIndex);
BOOL	CloseGroupData (int nModuleIndex);
inline BOOL ChannelIndexCheck(int nModuleIndex, int nGroupIndex, int nChIndex);
BOOL	ModuleIndexCheck(int nModuleIndex);
BOOL	GroupIndexCheck(int nModuleIndex, int nGroupIndex);
BOOL	LoadChMapFile();


//통신 및 함수 관련 Log 기록
BOOL WriteLog(char *szLog)
{
	if(szLog == NULL)					return FALSE;

	SYSTEMTIME systemTime;   // system time
	::GetLocalTime(&systemTime);
	
	//주어진 Log File 명이 없으면 기본 파일명 생성
//	if(strlen(szLogFileName) == 0)
//	{
		sprintf(szLogFileName, ".\\log\\%d%02d%02d.log", systemTime.wYear, systemTime.wMonth, systemTime.wDay);
//	}

	FILE *fp = fopen(szLogFileName, "ab+");
	if(fp == NULL)		return FALSE;
		
	TRACE("%s\n", szLog);
	fprintf(fp, "%02d/%02d/%02d %02d:%02d:%02d :: %s\r\n",	systemTime.wYear, systemTime.wMonth, systemTime.wDay,
												systemTime.wHour, systemTime.wMinute, systemTime.wSecond, 
												szLog);
	fclose(fp);
	return TRUE;
}

//동일 Port 사용 Scheduler가 한번만 실행 되도록 
BOOL IsFirstInstance(int nPort)
{
	char szName[128];
	sprintf(szName, "ADP cell test System %d", nPort);
	hMutexInstance = ::OpenMutex(MUTEX_ALL_ACCESS|SYNCHRONIZE, FALSE, _T(szName));
	if(hMutexInstance)
		return FALSE;
	
	
	//The system closes the handle automatically when the process terminates. 
	//The mutex object is destroyed when its last handle has been closed.
	hMutexInstance = ::CreateMutex(NULL, TRUE, _T(szName));
//	BOOL fMutexOwned = FALSE;
	if(hMutexInstance != NULL)
	{
		return TRUE;
	}
	return FALSE;
		
/*		if(GetLastError() == ERROR_ALREADY_EXISTS)
		{
			DWORD dwWaitResult = ::WaitForSingleObject(hMutexInstance, INFINITE);
			if(dwWaitResult == WAIT_OBJECT_0)
			{
				fMutexOwned = TRUE;
			}
		}
	}

//	if(hMutexInstance)	::ReleaseMutex(hMutexInstance);
//	::CloseHandle(hMutexInstance);
	return fMutexOwned;
*/
}

//Open Formation Scheduler Server
//	SFTForm Server 초기화 순서
// 1. SFTOpenFormServer()를 이용하여 Server Data 구조 형성
// 2. SFTSetSysParam()을 이용하여 Module Setting값 저장
// 3. PSServerStart()를 이용하여 Module 접속 대기  

SFT_DLL_APT	BOOL SFTOpenFormServer(int nInstalledModuleNo, HWND hMsgWnd)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	int nPortNo  = _SFT_FROM_SCHEDULER_TCPIP_PORT1;

	//중복 실행 방지
	if(!IsFirstInstance(nPortNo))			 
	{
		strcpy(szErrorString, "Initialize Socket Port Error(Use By other App)");
		WriteLog(szErrorString);
		nErrorNumber = SFT_ERR_WINSOCK_INITIALIZE;
		return FALSE;
	}

	//Module Count Check
	if(nInstalledModuleNo <= 0 || nInstalledModuleNo > SFT_MAX_MODULE_NUM)
	{
		strcpy(szErrorString, "Initialize Module Number Error");
		WriteLog(szErrorString);
		nErrorNumber = SFT_ERR_INVALID_ARGUMENT;
		return FALSE;
	}

	//랜카드(NIC) 설치 유무
	//iphlpapi.h lib 를 include 시켜야 합니다. windows core sdk 안에 포함되어있습니다.

//	IP_ADAPTER_INFO info;
//	unsigned long buf;
//	buf = sizeof(info);
//
//	if ( ::GetAdaptersInfo(&info, &buf) == ERROR_SUCCESS ) {
//	   printf("you are installed Network Interface Card");
//	   printf("NIC Name : %s", info.Description);
//	}
//	else
//	{
//		strcpy(szErrorString, "NIC is not installed");
//		WriteLog(szErrorString);
//		return FALSE;
//	}

	//PC쪽에서 채널을 매핑할 경우 매핑 파일을 읽는다.
	//SFTSetChMappingType() 함수에 의해 사용여부가 결정 된다.
	LoadChMapFile();

	//Winsock Version Check
	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD( 2, 2 );		//WinSock Version Greater than 2.2	
	int  err = ::WSAStartup( wVersionRequested, &wsaData );
	if (err != 0) 
	{
		// Tell the user that we couldn't find a usable WinSock DLL.
		sprintf(szErrorString, "Couldn't find a usable WinSock DLL. %d\n", err);
		WriteLog(szErrorString);
		nErrorNumber = SFT_ERR_WINSOCK_STARTUP;
		return FALSE;
	}	

/*	/* Confirm that the WinSock DLL supports 2.0.*/
/*	if ((LOBYTE( wsaData.wVersion ) == LOBYTE(wVersionRequested) &&
			HIBYTE( wsaData.wVersion ) < HIBYTE(wVersionRequested)) ||
			LOBYTE( wsaData.wVersion ) < LOBYTE(wVersionRequested))
	{
		WSACleanup();
//		sprintf(szErrorString, "%s. Win Socket Ver %d.%d require. Installed Ver %d.%d\n",
//			wsaData.szDescription,
//			LOBYTE(wVersionRequested), HIBYTE(wVersionRequested),
//			LOBYTE( wsaData.wVersion), HIBYTE( wsaData.wVersion));
		nErrorNumber = SFT_ERR_WINSOCK_VERSION;
		return FALSE; 
	}
*/

	//Get Server IP
	struct hostent *serverHostent;
	struct in_addr sin_addr;
	::gethostname( szServerIP,100 );
	serverHostent = ::gethostbyname( szServerIP );
	strcpy( szServerIP, serverHostent->h_name );
	memcpy( &sin_addr, serverHostent->h_addr, serverHostent->h_length);
	strcpy( szServerIP, inet_ntoa( sin_addr ) );

	//통신위한 Data 저장 구조 생성
	m_nInstalledModuleNum = nInstalledModuleNo;
	m_lpModule = new CModule[m_nInstalledModuleNum];
	ASSERT(m_lpModule);

	//Default 모듈 정보 입력 
//	for(register int i =0; i<m_nInstalledModuleNum; i++)
//	{
//		m_lpModule[i].SetModuleIndex(i);
//	}

/*	SFT_MD_SYSTEM_DATA	sysData;
	memset(&sysData, 0, sizeof(SFT_MD_SYSTEM_DATA));
	sysData.nVersion = ;
	for(register int i =0; i<m_nInstalledModuleNum; i++)
	{
		//Default Module system Data
		m_lpModule[i].m_sysData.nModuleID = i+1;

		m_stModule[i].sysData.nVersion = _SFT_PROTOCOL_VERSION;
		m_stModule[i].sysData.wInstalledBoard = SFT_DEFAULT_BD_PER_MD;
		m_stModule[i].sysData.wChannelPerBoard = SFT_DEFAULT_CH_PER_BD;
		
		m_stModule[i].sysData.nTotalGroupNo = 1;
		m_stModule[i].sysData.awChInGroup[0] = SFT_DEFAULT_CH_PER_MD;///2;
	//	m_stModule[i].sysData.awChInGroup[1] = SFT_DEFAULT_CH_PER_MD/2;

		//Set Default Parameter
		m_stModule[i].sysParam.nModuleID = i+1;
		m_stModule[i].sysParam.lMaxVoltage =  ExpFloattoLong(SFT_MAX_VOLTAGE, SFT_VTG_FLOAT);
		m_stModule[i].sysParam.lMinVoltage =  ExpFloattoLong(SFT_MIN_VOLTAGE, SFT_VTG_FLOAT);
		m_stModule[i].sysParam.lMaxCurrent =  ExpFloattoLong(SFT_MAX_CURRENT, SFT_CRT_FLOAT);
		m_stModule[i].sysParam.lMinCurrent =  ExpFloattoLong(SFT_MIN_CURRENT, SFT_CRT_FLOAT);
		m_stModule[i].sysParam.lMaxTemp = SFT_MAX_TEMPERATURE*(long)SFT_ETC_EXP;
		m_stModule[i].sysParam.lMaxGas =  SFT_MAX_GAS_LIMIT*(long)SFT_ETC_EXP;
		m_stModule[i].sysParam.bUseGasLimit = FALSE;
		m_stModule[i].sysParam.bUseTempLimit = FALSE;
		m_stModule[i].sysParam.lV24Over = ExpFloattoLong(SFT_MAX_V24_IN, SFT_VTG_FLOAT);
		m_stModule[i].sysParam.lV24Limit =  ExpFloattoLong(SFT_MIN_V24_IN, SFT_VTG_FLOAT);
		m_stModule[i].sysParam.lV12Over =  ExpFloattoLong(SFT_MAX_V12_OUT, SFT_VTG_FLOAT);
		m_stModule[i].sysParam.lV12Limit =  ExpFloattoLong(SFT_MIN_V12_OUT, SFT_VTG_FLOAT);
		m_stModule[i].sysParam.nAutoReportInterval = 2000;
		strcpy(m_stModule[i].sysParam.IPAddr, szServerIP);
		
		InitGroupData (i);
	}
*/
	//Message 보낼 WND
	m_worker.SetMsgWnd(hMsgWnd);

	//같은 장비에서 여러 Scheduler 실행 가능하게 하기 위해 Port 번호 지정 
	m_worker.SetListenPort(nPortNo);
	m_Initialized = TRUE;	//초기화 완료 

	return m_Initialized;
}

//Port Listen Thread 시작 하여 Client 접속 요청을 받아 들인다.
BOOL SFTServerStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	//Server 초기화가 되어 있지 않거나 이미 Open 되어 있을 경우 
	if(m_Initialized == FALSE || m_pDispather != NULL || m_bServerOpen == TRUE) 
		return FALSE;
	
	m_pDispather = new CWizThreadDispatcher(m_worker, m_nInstalledModuleNum);	
	m_pDispather->Start();
	m_bServerOpen = TRUE;
	WriteLog("Cycler Control Server Initilaized");

	return TRUE;
}

//PC쪽에서 Ch을 Mapping 할 경우 사용할 Mapping Table을 Loading 한다.
//사용 여부는 SFTSetChMappingType() 함수에 의해 결정 
BOOL LoadChMapFile()
{
	SHORT	*pTemp = NULL;
	BOOL	bRtn = FALSE;
	FILE *fp = fopen(SFT_CHANNEL_MAP_FILE_NAME, "rt");
	int nTemp;
	if(fp != NULL)
	{
		char buff[128];
		long ver;

		//FileVersion Read
		if(fscanf(fp, "%s", buff) > 0 && fscanf(fp, "%d", &ver) > 0)
		{
			int count = SFT_MAX_CH_PER_MD;
			
			//Channel 수 Read
			if(fscanf(fp, "%d", &count) > 0 && count > 0)
			{
				pTemp = new SHORT[count];		//중복된 Index가 있는지 검사 하기 위해 
				ASSERT(pTemp);
				for(INDEX n = 0; n<count; n++)	pTemp[n] = -1;

				for(INDEX i =0; i<count; i++)
				{				
					if(fscanf(fp, "%d", &nTemp) < 1)		//Read Fail Default Mapping
					{
						break;
					}

					if(nTemp < 0 || nTemp >= count)			//채널 범위를 벗어난 숫자 Default Mapping
					{
						sprintf(szErrorString, "Channel Mapping File Index Error (INDEX %d = %d)", i, nTemp);
						WriteLog( szErrorString );
						break;
					}
					
					if(pTemp[nTemp] != -1 )					//현재 Index에 이미 값이 할당되어 있는데 또 있을 경우 
					{
						sprintf(szErrorString, "Channel Mapping File Index Error (INDEX %d = %d)", i, pTemp[nTemp]);
						WriteLog( szErrorString );
						break;
					}

					g_anChannelMap[i] = nTemp;
					pTemp[nTemp] = (SHORT)nTemp;
				}
				
				if(count == i)	bRtn = TRUE;		//Read Success
			}
		}
		fclose(fp);
	}

	//파일을 읽지 못하였을 경우 기본 mapping으로 처리
	if(bRtn == FALSE)
	{
		for(INDEX i =0; i<SFT_MAX_CH_PER_MD; i++)
		{
			g_anChannelMap[i] = i;
		}
		sprintf(szErrorString, "Can't find mapping file or mapping count mismatch");
		WriteLog( szErrorString );
	}

	if( pTemp != NULL)
	{
		delete pTemp;
		pTemp = NULL;
	}

	return bRtn;
}

BOOL	ModuleIndexCheck(int nModuleIndex)
{
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	return TRUE;
}
/*
BOOL	GroupIndexCheck(int nModuleIndex, int nGroupIndex)
{
	if(ModuleIndexCheck(nModuleIndex) == FALSE)	return FALSE;

	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return FALSE;
	}
	return TRUE;
}

inline BOOL ChannelIndexCheck(int nModuleIndex, int nGroupIndex, int nChIndex)
{
	if(GroupIndexCheck(nModuleIndex, nGroupIndex) == FALSE)	return FALSE;
	if(nChIndex <0 || nChIndex >= m_stModule[nModuleIndex].sysData.awChInGroup[nGroupIndex])
	{
		nErrorNumber = SFT_ERR_CHANNEL_NOT_EXIST;
		return FALSE;
	}
	return TRUE;
}
/*
int SFTGetGroupNo(int nModuleIndex)
{
	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return -1;
	}
	if(ModuleIndexCheck(nModluleIndex) == FALSE)	return -1;
	return m_stModule[nModuleIndex].sysData.nTotalGroupNo;
}
*/
/*
BOOL	CloseGroupData (int nModuleIndex)
{
	if(ModuleIndexCheck(nModuleIndex) == FALSE)	return FALSE;
	
	int nGroupNo =  m_stModule[nModuleIndex].gpData.GetSize();
	LPSFT_GROUP_INFO	pGroup;
	LPSFT_CH_DATA	pChannel;

	for(INDEX i =0; i<nGroupNo; i++)
	{
		pGroup = (SFT_GROUP_INFO *)m_stModule[nModuleIndex].gpData[i];	
		ASSERT(pGroup);
		for(INDEX j =0; j<pGroup->chData.GetSize(); j++)
		{
			pChannel = (SFT_CH_DATA *)pGroup->chData[j];
			ASSERT(pChannel);
			delete pChannel;
			pChannel = NULL;
		}
		pGroup->chData.RemoveAll();
		delete pGroup;
		pGroup = NULL;
	}
	m_stModule[nModuleIndex].gpData.RemoveAll();
	return TRUE;
}
*/

SFT_DLL_APT SFT_MD_SYSTEM_DATA * SFTGetModuleSysData(int nModuleID)
{
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return NULL;
	return m_lpModule[nModuleIndex].GetModuleSystem();
}
/*
BOOL InitGroupData (int nModuleIndex)
{
	if(ModuleIndexCheck(nModuleIndex) == FALSE)	return FALSE;
	if(CloseGroupData(nModuleIndex) == FALSE)	return FALSE;	
	
	ASSERT(m_stModule[nModuleIndex].gpData.GetSize() == 0);

	LPSFT_GROUP_INFO		pGroup;
	LPSFT_CH_DATA		pChannel;
	LPSFT_MD_SYSTEM_DATA	pSysData;
	
	pSysData = SFTGetModuleSysData(nModuleIndex);
	ASSERT(pSysData);

	for(INDEX i =0; i<pSysData->nTotalGroupNo; i++)
	{
		pGroup = new SFT_GROUP_INFO;
		ASSERT(pGroup);
		ZeroMemory(&pGroup->gpData, sizeof(SFT_GP_DATA));

//		pGroup->gpData.sensorMinMax.gasData.lMax
//		pGroup->gpData.sensorMinMax.gasData.lMin
//		pGroup->gpData.sensorMinMax.tempData.lMax
//		pGroup->gpData.sensorMinMax.tempData.lMin

		pGroup->gpData.gpState.state = SFT_STATE_LINE_OFF;
		
		for(INDEX j =0; j<pSysData->awChInGroup[i]; j++)
		{
			pChannel = new SFT_CH_DATA;
			ASSERT(pChannel);
			ZeroMemory(pChannel, sizeof(SFT_CH_DATA));
			pGroup->chData.Add(pChannel);
		}

		m_stModule[nModuleIndex].gpData.Add(pGroup);	
	}
	return TRUE;
}
*/

//Moduel Data 전달 
SFT_DLL_APT CModule * SFTGetModule(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return NULL;

	return &m_lpModule[nModuleIndex];
}

//Close Formation Control Server
SFT_DLL_APT	BOOL SFTCloseFormServer()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(m_Initialized)
	{
/*		for (INDEX nI = 0; nI < m_nInstalledModuleNum; nI++)		//Close Event
		{	
			::CloseHandle(m_stModule[nI].m_hWriteEvent);
			::CloseHandle(m_stModule[nI].m_hReadEvent);
			CloseGroupData(nI);
		}
*/
		if (m_bServerOpen)				//Close Sever
		{
			m_pDispather->Stop(TRUE);
			delete m_pDispather;
			m_pDispather = NULL;
			m_bServerOpen = FALSE;
		}
		
		if(m_lpModule)					//Close Module Data Structure
		{
			delete []	m_lpModule;
			m_lpModule = NULL;
		}
		::WSACleanup();
	
		::ReleaseMutex(hMutexInstance);
		::CloseHandle(hMutexInstance);
	}
	m_Initialized= FALSE;
	WriteLog("Cycler Control Server Closed");
	return TRUE;
}

//Get Last Error Code
SFT_DLL_APT	int SFTGetLastError()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return nErrorNumber;
}

//Get Installed Module Number
SFT_DLL_APT	int SFTGetInstalledModuleNum()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return m_nInstalledModuleNum;
}

//Get Module ID
SFT_DLL_APT inline int SFTGetModuleID(int nModuleIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);
	
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return -1;
	}

	return m_lpModule[nModuleIndex].GetModuleID();
}

//Get Module Index
SFT_DLL_APT inline int SFTGetModuleIndex(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//#ifdef _DEBUG
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

//#endif

	for(INDEX i =0; i<m_nInstalledModuleNum; i++)
	{
		if(m_lpModule[i].GetModuleID() == nModuleID)	return i;
	}

	nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
	return -2;	
}

//Get Module Ip Address
SFT_DLL_APT	LPSTR SFTGetIPAddress(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = SFTGetModuleIndex(nModuleID);

	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);


/*	if(m_lpModule[nModuleIndex].GetState() ==PS_STATE_LINE_OFF)
	{
		nErrorNumber = SFT_ERR_LINE_OFF;
		return "LineOff";
	}
*/	
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return "MD Index Error";
	}

	return m_lpModule[nModuleIndex].GetModuleIP();
}

//모듈 ID에 맞는 System config를 Setting한다.
SFT_DLL_APT BOOL SFTSetSysParam(int nModuleIndex /*Zero Base*/,  int nModuleID/*One Base*/, SFT_SYSTEM_PARAM *pParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	if(pParam == NULL)
	{
		nErrorNumber = SFT_ERR_INVALID_ARGUMENT;
		return FALSE;
	}

	if(m_Initialized == FALSE)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return FALSE;
	}

	//모듈 정보에 모듈 번호와 Parameter를 설정한다.
	m_lpModule[nModuleIndex].SetModuleParam(nModuleID, pParam);
	
	return TRUE;
}

/*
//Set Module System Parameter
SFT_DLL_APT	BOOL SFTSetSysParam(int nModuleID, SFT_SYSTEM_PARAM *pParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	return SFTInitSysParam(nModuleIndex,  pParam);
}
*/

SFT_DLL_APT	SFT_SYSTEM_PARAM SFTGetSysParam(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);
	int nModuleIndex = SFTGetModuleIndex(nModuleID);

	SFT_SYSTEM_PARAM param;
	ZeroMemory(&param, sizeof(SFT_SYSTEM_PARAM));

	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return param;
	}
	param = m_lpModule[nModuleIndex].GetModuleParam();
	return param;
}


SFT_DLL_APT	WORD SFTGetModuleState(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = PS_STATE_LINE_OFF;

	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG	
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}
//#endif

	return	m_lpModule[nModuleIndex].GetState();
}
/*
SFT_DLL_APT	WORD SFTGetGroupState(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	WORD state = SFT_STATE_ERROR;
	if(m_Initialized == FALSE)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	//문제 있는 부분 수정이 필요
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif
	if(m_stModule[nModuleIndex].gpData.GetSize() <= 0)
		return state;

	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		state = pGroup->gpData.gpState.state;
	}
	return state;
}


SFT_DLL_APT	BOOL SFTSetGroupState(int nModuleID, int nGroupIndex, WORD newState)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
//	WORD state = SFT_STATE_ERROR;
	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return FALSE;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	//문제 있는 부분 수정이 필요
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return FALSE;
	}
//#endif
	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	pGroup->gpData.gpState.state = newState;
	
	return TRUE;
}


SFT_DLL_APT SFT_GP_DATA SFTGetGroupData(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	SFT_GP_DATA gpData;
	ZeroMemory(&gpData, sizeof(SFT_GP_DATA));

	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return gpData;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return gpData;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return gpData;
	}
//#endif

	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		gpData = pGroup->gpData;
	}
	return gpData;
}
*/

SFT_DLL_APT	WORD SFTGetChannelState(int nModuleID, int nChannelIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	WORD state = PS_STATE_ERROR;
	int nModuleIndex = SFTGetModuleIndex(nModuleID);

	if(nModuleIndex<0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}

	return m_lpModule[nModuleIndex].GetChannelState(nChannelIndex);
}

/*
SFT_DLL_APT int SFTGetGroupCount(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return 0;
	}
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0)	return 0;
	
	return  m_stModule[nModuleIndex].sysData.nTotalGroupNo;
}

SFT_DLL_APT	WORD SFTGetChInGroup(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return 0;
	}
	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nGroupIndex<0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
		return 0;
//#endif

	return m_stModule[nModuleIndex].sysData.awChInGroup[nGroupIndex];
}

*/

//ADP KBH	2005/06/08
//각 채널의 항목값을 구한다.
SFT_DLL_APT	long SFTGetChannelValue(int nModuleID, int nChannelIndex, int nItem)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	long lValue = 0;
	if(m_Initialized == FALSE)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return lValue;
	}

	CChannel* lpChannel = ::SFTGetChannelData(nModuleID, nChannelIndex);
	
	//찾을 수 없는 ID
	if(lpChannel == NULL)
		return lValue;
	
	switch(nItem)
	{
	case PS_STATE		:	//Channel State
		lValue =  (long)lpChannel->GetState();		break;
	case PS_VOLTAGE	:	//Voltage
		lValue =  lpChannel->GetVoltage();			break;
	case PS_CURRENT	:	//Current
		lValue =  lpChannel->GetCurrent();			break;
	case PS_CAPACITY	:	//Capacity
		lValue =  lpChannel->GetCapacity();			break;
	case PS_CODE:		//Failure Code
		lValue =  (long)lpChannel->GetCode();		break;
	case PS_TOT_TIME	:	//Total Time
		lValue =  (long)lpChannel->GetTotalTime();	break;
	case PS_STEP_TIME	:	//Step Time
		lValue =  lpChannel->GetStepTime();			break;	
	case PS_GRADE_CODE	:	//Grade Code
		lValue =  (long)lpChannel->GetGradeCode();	break;
	case PS_STEP_NO	:	//Current Step Number
		lValue =  (long)lpChannel->GetStepNo();		break;	
	case PS_WATT		:	//Watt
		lValue =  lpChannel->GetWatt();				break;	
	case PS_WATT_HOUR	:	//Watt per Hour
		lValue =  lpChannel->GetWattHour();			break;
	case PS_IMPEDANCE:		//Impedance
		lValue =  lpChannel->GetImpedance();		break;
	case PS_CUR_CYCLE	:	//Watt per Hour
		lValue =  lpChannel->GetCurCycleCount();	break;
	case PS_TOT_CYCLE:		//Impedance
		lValue =  lpChannel->GetTotalCycleCount();	break;
	case PS_STEP_TYPE:
		lValue = lpChannel->GetStepType();			break;
	case PS_AVG_VOLTAGE:
		lValue = lpChannel->GetAvgVoltage();		break;
	case PS_AVG_CURRENT:
		lValue = lpChannel->GetAvgCurrent();		break;
	case PS_CAPACITY_SUM:
		lValue = lpChannel->GetCapacitySum();		break;	
	case PS_TEMPERATURE:
		lValue = lpChannel->GetTemperature();		break;
	case PS_PRESSURE:
		lValue = lpChannel->GetPressure();			break;
	case PS_SHARING_INFO:
		lValue	= lpChannel->GetSharingInfo();		break;
	case PS_GOTO_COUNT:
		lValue = lpChannel->GetGotoCycleNum();		break;
	case PS_WATTHOUR_SUM:
		lValue = lpChannel->GetWattHourSum();		break;
	}
	return lValue;
}


//ADP	KBH	2005/06/08	
SFT_DLL_APT CChannel*  SFTGetChannelData(int nModuleID, int nChannelIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return NULL;
	}

	return m_lpModule[nModuleIndex].GetChannelData(nChannelIndex);
}

/*
SFT_DLL_APT BYTE SFTGetChCode(int nModuleID, int nGroupIndex, int nChannelIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	BYTE code = 0;
	
	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return code;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return code;
	}

	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo )
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return code;
	}
	if(nChannelIndex < 0 || nChannelIndex >= m_stModule[nModuleIndex].sysData.awChInGroup[nGroupIndex])
	{
		nErrorNumber = SFT_ERR_CHANNEL_NOT_EXIST;
		return code;
	}
//#endif
	
	LPSFT_CH_DATA  lpChannel;
	LPSFT_GROUP_INFO lpGroup;
	lpGroup = (SFT_GROUP_INFO *)m_stModule[nModuleIndex].gpData[nGroupIndex];
	ASSERT(lpGroup);
	lpChannel = (SFT_CH_DATA *)lpGroup->chData[nChannelIndex];
	ASSERT(lpChannel);
	code =  (BYTE)lpChannel->channelCode;
	return code;
}
*/
/*
SFT_DLL_APT WORD	SFTGetDoorState(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = SFT_STATE_ERROR;

	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.doorState;
	}
	return state;	
}
*/
/*
SFT_DLL_APT WORD	SFTGroupCode(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = SFT_STATE_ERROR;

	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.failCode;
	}
	return state;	
}


SFT_DLL_APT WORD	SFTJigState(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = SFT_STATE_ERROR;

	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.jigState;
	}
	return state;	
}


SFT_DLL_APT WORD	SFTTrayState(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = SFT_STATE_ERROR;

	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.trayState;
	}
	return state;	
}

SFT_DLL_APT WORD	SFTDoorState(int nModuleID, int nGroupIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	WORD state = SFT_STATE_ERROR;

	if(m_stModule == NULL)
	{
		nErrorNumber = SFT_ERR_NOT_INITIALIZED;
		return state;
	}

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

//#ifdef _DEBUG
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return state;
	}
	
	if(nGroupIndex < 0 || nGroupIndex >= m_stModule[nModuleIndex].sysData.nTotalGroupNo)
	{
		nErrorNumber = SFT_ERR_GROUP_NOT_EXIST;
		return state;
	}
//#endif

	SFT_GROUP_INFO *pGroup =  (SFT_GROUP_INFO*)m_stModule[nModuleIndex].gpData[nGroupIndex];
	if(pGroup != NULL)
	{
		state = (WORD)pGroup->gpData.gpState.doorState;
	}
	return state;	
}

/*
int WriteToModule(int nModuleIndex, LPVOID pData, int nLen)
{
	ASSERT(m_stModule && pData);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);

	memcpy(m_stModule[nModuleIndex].szTxBuffer, pData, nLen);
	m_stModule[nModuleIndex].nTxLength = nLen;

	::SetEvent(m_stModule[nModuleIndex].m_hWriteEvent);
	return nLen;
}

//Read Data to Buffer From Module  
int ReadFromModule(int nModuleIndex, LPVOID pData, int nLen)
{
	ASSERT(m_stModule && pData);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);

	int nRtn = -1;
	if (m_stModule[nModuleIndex].nRxLength >= nLen )		//Received
	{
		memcpy(pData, m_stModule[nModuleIndex].szRxBuffer, nLen);
		nRtn = nLen;
	} 
	else 
	{
		::ResetEvent(m_stModule[nModuleIndex].m_hReadEvent);
		
		if (::WaitForSingleObject(m_stModule[nModuleIndex].m_hReadEvent, _SFT_MSG_TIMEOUT) == WAIT_OBJECT_0 )
		{
			if (m_stModule[nModuleIndex].nRxLength >= nLen ) 
			{
				memcpy(pData, m_stModule[nModuleIndex].szRxBuffer, nLen);
				nRtn = nLen;
			}
			else
			{
				nErrorNumber = SFT_ERR_MODULE_READ_FAIL;
			}
		}
		else
		{
			nErrorNumber = SFT_ERR_RECEIVE_TIMEOUT;
		}
	}
	m_stModule[nModuleIndex].nRxLength = 0; 
	return nRtn;
}
*/

/*
//Read Data From module
int ReadModuleData(int nModuleIndex, int nLen)
{
	ASSERT(m_lpModule);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);

	int nRtn = -1;
	if (m_lpModule[nModuleIndex].m_nRxLength >= nLen )		//Received
	{
		nRtn = nLen;
	} 
	else 
	{
		::ResetEvent(m_lpModule[nModuleIndex].m_hReadEvent);
		
		if (::WaitForSingleObject(m_lpModule[nModuleIndex].m_hReadEvent, SFT_MSG_TIMEOUT) == WAIT_OBJECT_0 )
		{
			if (m_lpModule[nModuleIndex].m_nRxLength >= nLen ) 
			{
				nRtn = nLen;
			}
		}
		else
		{
			nErrorNumber = SFT_ERR_RECEIVE_TIMEOUT;
		}
	}
	m_lpModule[nModuleIndex].m_nRxLength = 0; 
	return nRtn;
}
*/

/*
//read From Command Response (ACK/NACK/FAIL)
int ReadAck(int nModuleIndex)
{
	ASSERT(m_lpModule);
	ASSERT(nModuleIndex>=0 && nModuleIndex < m_nInstalledModuleNum);
	int nAck = SFT_FAIL;
	if (::WaitForSingleObject(m_lpModule[nModuleIndex].m_hReadEvent, SFT_MSG_TIMEOUT) == WAIT_OBJECT_0 )
	{
//		::ResetEvent(m_stModule[nModuleIndex].m_hReadEvent);
		nAck = m_lpModule[nModuleIndex].m_CommandAck.nCode;
	}
	else
	{
		nAck =  SFT_TIMEOUT;
	}
	
	::ResetEvent(m_lpModule[nModuleIndex].m_hReadEvent);
	m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_FAIL;
	//TRACE("Read Response of Command %d\n", nAck);
	Sleep(0);
	return nAck;
}
*/

//Send Command to Module and Read response Data
SFT_DLL_APT LPVOID	SFTSendDataCmd(UINT nModuleID, UINT nCmd, LPSFT_CH_SEL_DATA lpChSelData, UINT nReadSize, LPVOID lpData, UINT nSize)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return NULL;
	}

	if(m_lpModule[nModuleIndex].SendCommand(nCmd, lpChSelData, lpData, nSize) == FALSE)
	{
		//전송 실패 
		return NULL;
	}

	//읽을 data가 있을 경우 읽음 
	if(nReadSize > 0)
	{
		return m_lpModule[nModuleIndex].ReadData(nReadSize);
	}
	return NULL;
}

//Send Command to Module and Read response 
//nGroupNo and nChannelNo is one base Index;
//nGroup == 0 => Modlule Command
//nChannelNo == 0 => Group Command
SFT_DLL_APT int SFTSendCommand(UINT nModuleID, UINT nCmd, LPSFT_CH_SEL_DATA lpChSelData /*= NULL*/, LPVOID lpData /*= NULL*/, UINT nSize /*= 0*/)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	//ModuleID 검사
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return SFT_FAIL;
	}

	
	if(m_lpModule[nModuleIndex].SendCommand(nCmd, lpChSelData, lpData, nSize) == FALSE)
	{
		//전송 실패 
		return m_lpModule[nModuleIndex].m_CommandAck.nCode;
	}
	
	
	//응답 기다림 
	return m_lpModule[nModuleIndex].ReadAck();
}


SFT_DLL_APT void SFTSetLogFileName(char *szFileName)
{
	if(szFileName == NULL)	return;
	strcpy(szLogFileName, szFileName);
}

SFT_DLL_APT int SFTGetInstalledBoard(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	
	return m_lpModule[nModuleIndex].GetModuleSystem()->wInstalledBoard;
}

SFT_DLL_APT int SFTGetChPerBoard(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	return m_lpModule[nModuleIndex].GetModuleSystem()->wChannelPerBoard;
}

/*
//Module Version
SFT_DLL_APT UINT SFTGetModuleVersion(int nModuleID)
{
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	return m_lpModule[nModuleIndex].GetModuleSystem()->nVersion;
}

/* 
SFT_DLL_APT UINT SFTInstledlTotalGroup()
{
	int count = 0;
	for(int i =0; i<m_nInstalledModuleNum; i++)
	{
		count += m_stModule[i].sysData.nTotalGroupNo;
	}
	return count;
}
*/

/*
//Setting Current Working Type (Auto /Normal)
SFT_DLL_APT BOOL	SFTSetAutoProcess(int nModuleID, BOOL bEnable)
{
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}
	m_lpModule[nModuleIndex].sysParam.bAutoProcess = bEnable;
	return TRUE;	
}

//Current Working Type (Auto/Manual)
*/
//SFT_DLL_APT BOOL	SFTGetAutoProcess(int nModuleID)
//{
//	//Must call SFTOpenFormServer() before 
//	ASSERT(m_Initialized);
//
//	int nModuleIndex = SFTGetModuleIndex(nModuleID);
//	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)
//	{
//		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
//		return FALSE;
//	}
//	return m_lpModule[nModuleIndex].GetModuleParam()->bAutoProcess;
//}


/*
//Group Fail Code
SFT_DLL_APT BYTE SFTGetGroupFailCode(int nModuleID, int nGroupIndex)
{
	SFT_GP_DATA gpData = SFTGetGroupData(nModuleID, nGroupIndex);

	return gpData.gpState.failCode;
}
*/

//Setting Monitoring Channel Mapping Type
SFT_DLL_APT void SFTSetChMappingType(int nType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(nType < 0)	g_nChMappingType = SFT_CH_INDEX_MEMBER;
	else g_nChMappingType = nType;
}


// Convert Monitoring Channel to Hardware Channel
// input  UINT : Monitoring Channel Index
// return UINT : Hardware Channel
SFT_DLL_APT UINT SFTGetHWChIndex(UINT nIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(SFT_CH_INDEX_MEMBER == SFT_CH_INDEX_MAPPING)
	{
		for(UINT i =0; i<SFT_MAX_CH_PER_MD; i++)
		{
			if((UINT)g_anChannelMap[i] == nIndex)
			{
				return i;
			}
		}
	}
	return nIndex;
}


//Command Fail Message
SFT_DLL_APT CString SFTCmdFailMsg(int nCode)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString strMsg;
	switch(nCode)
	{
	case SFT_ACK:				strMsg = "정상";					break;	//0x00000001
	case SFT_TIMEOUT:			strMsg = "응답 없음";				break;	//0x00000002
	case SFT_SIZE_MISMATCH:		strMsg = "Data Size 오류";			break;	//0x00000003
	case SFT_RX_BUFF_OVER_FLOW:	strMsg = "Rx Buffer Over Flow";		break;	//0x00000004
	case SFT_TX_BUFF_OVER_FLOW:	strMsg = "Tx Buffer Over Flow";		break;	//0x00000005
	case SFT_FAIL:				strMsg.Format("Fail Code %d", SFTGetLastError());	break;

	case 0x011:		strMsg = "정의되지 않은 명령";					break;
	case 0x012:		strMsg = "장비 ID가 맞지 않음";					break;
	case 0x013:		strMsg = "알수 없는 Code";						break;
	case 0x014:		strMsg = "Group ID 오류";						break;
	case 0x015:		strMsg = "알수 없는 Step Type";					break;
	case 0x016:		strMsg = "공정 정보가 전송되지 않았음";			break;
	case 0x017:		strMsg = "비교 Flag Disable";					break;

	case 0x0100:	strMsg = "IDLE 상태가 아님";					break;
	case 0x0101:	strMsg = "Standby나 End 상태가 아님";			break;
	case 0x0102:	strMsg = "Run이나 Pause 상태가 아님 ";			break;
	case 0x0103:	strMsg = "Run 상태가 아님";						break;
	case 0x0104:	strMsg = "Pause 상태가 아님";					break;
	case 0x0105:	strMsg = "모두 Fault 상태임";					break;
	case 0x0106:	strMsg = "전송 가능한 상태가 아님(Idle, Standby)";	break;	
	case 0x0107:	strMsg = "전송 가능한 상태가 아님(Idle, Standby, Run)";		break;
	case 0x0108:	strMsg = "전송 가능한 상태가 아님(Standby, End, Run)";		break;
	case 0x0109:	strMsg = "전송 가능한 상태가 아님(Standby or Run)";			break;
	case 0x010A:	strMsg = "상태전이 중";							break;

	case 0x0200:	strMsg = "Channel이  Run 상태가 아님";			break;

	case 0x0300:	strMsg = "Jig가 준비 되지 않았음";				break;
	case 0x0301:	strMsg = "Local Mode로 설정 되어 있음";			break;
	case 0x0302:	strMsg = "Door Open";							break;
	case 0x0303:	strMsg = "자동공정이 설정되어 있지 않음";		break;
	case 0x0304:	strMsg = "Jig의 Cell 높이 설정이 입력 Tray와 맞지 않음";	break;


	default:	strMsg.Format("Code %d", nCode);	break;
	}
	return strMsg;
}

/*
//2003/1/14
SFT_DLL_APT int SFTDllGetVersion()
{
	return SFT_FORM_VERSION;
}
*/


//2003/1/14
SFT_DLL_APT BOOL SFTSetAutoReport(int nModuleID, UINT nInterval)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);
	int nModuleIndex = SFTGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	SFT_AUTO_REPORT_SET autoSet;
	ZeroMemory(&autoSet, sizeof(autoSet));
	autoSet.nInterval = nInterval;
	int nRtn;

	if(SFT_ACK != (nRtn = SFTSendCommand(nModuleID, SFT_CMD_SET_AUTO_REPORT, NULL, &autoSet, sizeof(SFT_AUTO_REPORT_SET))))
	{
		TRACE("Auto Report Interval Setting Fail %d\n", nRtn);
		return FALSE;
	}
	TRACE("Send Auto Report Interval Setting %d\n", nInterval);
	return TRUE;
}

//모듈의 동작 모드를 Setting 한다.
SFT_DLL_APT BOOL SFTSetLineMode(int nModuleID, int nOnLineMode, int nControlMode)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);
	int nModuleIndex = SFTGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	SFT_LINE_MODE_SET lineSet;
	ZeroMemory(&lineSet, sizeof(lineSet));

	lineSet.bOnLineMode = nOnLineMode;
	lineSet.bControlMode = nControlMode;
	
	if(SFT_ACK != SFTSendCommand(nModuleID, SFT_CMD_SET_LINE_MODE, NULL, &lineSet, sizeof(lineSet)))
	{
		return FALSE;
	}
	return TRUE;
}

//모듈의 작업 모드를 읽어 온다.
SFT_DLL_APT BOOL SFTGetLineMode(int nModuleID, int &nOnLineMode, int &nControlMode)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);
	int nModuleIndex = SFTGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return FALSE;
	}

	SFT_LINE_MODE_SET lineSet;
	ZeroMemory(&lineSet, sizeof(lineSet));
	LPVOID lpData = SFTSendDataCmd(nModuleID, SFT_CMD_LINE_MODE_REQUEST, NULL, sizeof(SFT_LINE_MODE_SET));
	if(lpData == NULL)	
	{
		return FALSE;
	}
	else
	{
		memcpy(&lineSet, lpData, sizeof(lineSet));
		nOnLineMode = lineSet.bOnLineMode;
		nControlMode = lineSet.bControlMode;
	}
	return TRUE;
}

SFT_DLL_APT UINT SFTGetProtocolVer(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Must call SFTOpenFormServer() before 
	ASSERT(m_Initialized);

	int nModuleIndex = SFTGetModuleIndex(nModuleID);

	if(ModuleIndexCheck(nModuleIndex) == FALSE)
	{
		nErrorNumber = SFT_ERR_MODULE_NOT_EXIST;
		return 0;
	}
	if(m_Initialized == FALSE)	return 0;

	return m_lpModule[nModuleIndex].GetModuleSystem()->nProtocolVersion;
}

/*
UINT GetCmdDataSize(UINT nCmd)
{
	size_t nSize = 0;
	
	switch(nCmd)
	{
	case SFT_CMD_LINE_MODE_REQUEST	:	nSize = sizeof(SFT_LINE_MODE_SET);		break;
	case SFT_CMD_VER_DATA			:	nSize = sizeof(SFT_MD_SYSTEM_DATA);		break;
	case SFT_CMD_MD_SET_DATA			:	nSize = sizeof(SFT_MD_SET_DATA);			break;
	case SFT_CMD_RESPONSE			:	nSize = sizeof(SFT_RESPONSE);			break;
	case SFT_CMD_TEST_HEADERDATA		:	nSize = sizeof(SFT_TEST_HEADER);			break;
	case SFT_CMD_CHECK_PARAM			:	nSize = sizeof(SFT_PRETEST_PARAM);		break;
//	case SFT_CMD_STSFT_DATA			:	nSize = sizeof(SFT_)
	case SFT_CMD_AUTO_GP_STATE_DATA	:	nSize = sizeof();						break;
	case SFT_CMD_AUTO_GP_DATA		:	nSize = sizeof();						break;
	case SFT_CMD_AUTO_GP_STSFT_END_DATA	:	nSize = sizeof();					break;

	case SFT_CMD_MD_SET_DATA			:	nSize = sizeof();	break;

	case SFT_CMD_TESTINFO_REQUEST	:	nSize = sizeof();	break;
	case SFT_CMD_TESTINFO_DATA		:	nSize = sizeof();	break;

	case SFT_CMD_DATA_REQUEST		:	nSize = sizeof();	break;				
	case SFT_CMD_MD_DATA				:	nSize = sizeof();	break;

	case SFT_CMD_SYS_PARAM_REQUEST	:	nSize = sizeof();	break;
	case SFT_CMD_SYS_PARAM_DATA		:	nSize = sizeof();	break;

	case SFT_CMD_CHECK				:
	case SFT_CMD_RUN					:
	case SFT_CMD_PAUSE				:
	case SFT_CMD_RESTART				:
	case SFT_CMD_STOP				:
	case SFT_CMD_CLEAR				:
	case SFT_CMD_NEXTSTSFT			:
	case SFT_CMD_FAULTQUERY			:

	case SFT_CMD_FAULT_DATA			:

	case SFT_CMD_CAL_EXEC			:	
	case SFT_CMD_CAL_REQUEST			:
	case SFT_CMD_CAL_DATA			:

	case SFT_CMD_BOARDSET			:	nSize = sizeof();	break;		
	case SFT_CMD_BOARDDATA			:	nSize = sizeof();	break;
	case SFT_CMD_STSFTDATA			:	nSize = sizeof();	break;
	case SFT_CMD_GRADEDATA			:	nSize = sizeof();	break;
	case SFT_CMD_SHUTDOWN			:	nSize = sizeof();	break;
	case SFT_CMD_QUIT				:	nSize = sizeof();	break;

	case SFT_CMD_FAILCH				:	nSize = sizeof();	break;
	case SFT_CMD_PROCDATA			:	nSize = sizeof();	break;
	case SFT_CMD_SENDHEADER			:	nSize = sizeof();	break;
	case SFT_CMD_SENDCONTENT			:	nSize = sizeof();	break;
	case SFT_CMD_CLEARALL			:	nSize = sizeof();	break;
	case SFT_CMD_CHANGSFTROC			:	nSize = sizeof();	break;
	case SFT_CMD_CHANGEHEADER		:	nSize = sizeof();	break;
	case SFT_CMD_CLEARCONTENT		:	nSize = sizeof();	break;
	case SFT_CMD_SENDCURRENT			:	nSize = sizeof();	break;
	default:		break;
	}

	return nSize;
}
*/



SFT_DLL_APT char *SFTGetErrorString(int nErrorCode)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	switch(nErrorCode)
	{
	case	SFT_ERR_EMPTY	:			return	"Error Empty";					//0x00
	case	SFT_ERR_INVALID_ARGUMENT:	return	"Invalid argument";				//0x01
	case	SFT_ERR_WINSOCK_STARTUP	:	return	"Winsock startup error";		//0x02
	case	SFT_ERR_WINSOCK_VERSION	:	return	"Winsock version error";		//0x03
	case	SFT_ERR_NOT_INITIALIZED	:	return	"Server not initialized";		//0x04
	case	SFT_ERR_RECEIVE_TIMEOUT	:	return	"Message timeout error";		//0x05
	case	SFT_ERR_MODULE_READ_FAIL:	return	"Module message read fail";		//0x06
	case	SFT_ERR_WINSOCK_INITIALIZE:	return	"Winsock initialize error";		//0x07
	case	SFT_ERR_LINE_OFF		:	return	"network line is closed";		//0x08
	case	SFT_ERR_MODULE_NOT_EXIST:	return	"Module does not exist";		//0x09
	case	SFT_ERR_GROUP_NOT_EXIST	:	return	"Group does not exist";			//0x0A
	case	SFT_ERR_BOARD_NOT_EXIST	:	return	"Board does not exist";			//0x0B
	case	SFT_ERR_CHANNEL_NOT_EXIST:	return	"Channel does not exist";		//0x0C
	}

	return "Error Empty";
}

SFT_DLL_APT SYSTEMTIME * SFTGetConnectedTime(int nModuleID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return NULL;
	return m_lpModule[nModuleIndex].GetConnectedTime();
}

SFT_DLL_APT void SFTInitChannel(int nModuleID, int nChIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return;
	CChannel *pCh = m_lpModule[nModuleIndex].GetChannelData(nChIndex);
	if(pCh != NULL)
	{
		pCh->ResetData();
	}
}

SFT_DLL_APT BYTE	SFTGetReservedCmd(int nModuleID, int nChIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nModuleIndex = SFTGetModuleIndex(nModuleID);
	if(nModuleIndex < 0 || nModuleIndex >= m_nInstalledModuleNum)	return 0;
	CChannel *pCh = m_lpModule[nModuleIndex].GetChannelData(nChIndex);
	if(pCh != NULL)
	{
		return pCh->GetReservedCmd();
	}
	return 0;
}

/*
BOOL CPSServerApp::InitInstance()
{
	SetRegistryKey("ADP CTSPro");
//	this->m_pszAppName = "CTSMonPro";
	strOldProfileName = this->m_pszProfileName;
	this->m_pszProfileName= "CTSMonPro";
	return CWinApp::InitInstance();
}
*/
