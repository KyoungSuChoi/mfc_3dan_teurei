// Module.cpp: implementation of the CModule class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Module.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CModule::CModule()
{
	//Sequence No 0는 최초 접속시에 자동으로 전송되므로 최초 Sequence는 부터 시작한다.
	m_nCmdSequence = 1;
	m_nModuleID = 1;
	m_nChCount = 0;
	m_pChannel = NULL;
	m_state = PS_STATE_LINE_OFF;
	m_hWriteEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hReadEvent  = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hSyncWriteEvent  = ::CreateEvent(NULL, TRUE, FALSE, NULL);

	//Added by KBH 20080828 Even Reset
	::ResetEvent(m_hWriteEvent);
	::ResetEvent(m_hReadEvent);
	::ResetEvent(m_hSyncWriteEvent);


	//Default Module system Data
	memset(&m_sysData, 0, sizeof(SFT_MD_SYSTEM_DATA));
	m_sysData.nModuleID = 1;
	m_sysData.nProtocolVersion = _SFT_PROTOCOL_VERSION;
	m_sysData.wInstalledBoard = SFT_DEFAULT_BD_PER_MD;
	m_sysData.wChannelPerBoard = SFT_DEFAULT_CH_PER_BD;
	m_sysData.nInstalledChCount = SFT_DEFAULT_BD_PER_MD*SFT_DEFAULT_CH_PER_BD;
	m_sysData.nTotalJigNo = 1;
	m_sysData.awBdInJig[0] = SFT_DEFAULT_BD_PER_MD;
	m_sysData.nSystemType = SFT_ID_FORM_MODULE;				//Formation/IROCV/Aging/Grader/Selector
	strcpy(m_sysData.szModelName, "ADP Cell Test System");	//Module Model Name
	m_sysData.nOSVersion = 0x1000;							//unsigned int :System Version 
//	m_sysData.byTypeData[8];								//Module Location, Line No, Module Type, Etc...	

	m_sysData.wVoltageRange = 1;
	m_sysData.wCurrentRange = 1;
	m_sysData.nCurrentSpec[0] = 2000000;	//default current spec is 2000mA
	m_sysData.nVoltageSpec[0] = 5000000;	//default voltage spec is 5000mV

	m_lpBuff = NULL;
		
	//Set Default Parameter
//	memset(&m_sysParam, 0, sizeof(SFT_SYSTEM_PARAM));
//	m_sysParam.lMaxVoltage =  SFT_MAX_VOLTAGE;
//	m_sysParam.lMinVoltage =  SFT_MIN_VOLTAGE;
//	m_sysParam.lMaxCurrent =  SFT_MAX_CURRENT;
//	m_sysParam.lMinCurrent =  SFT_MIN_CURRENT;
//	m_sysParam.lMaxTemp = SFT_MAX_TEMPERATURE;
//	m_sysParam.lMaxGas =  SFT_MAX_GAS_LIMIT;
//	m_sysParam.bUseGasLimit = FALSE;
//	m_sysParam.bUseTempLimit = FALSE;
//	m_sysParam.lV24Over = SFT_MAX_V24_IN;
//	m_sysParam.lV24Limit =  SFT_MIN_V24_IN;
//	m_sysParam.lV12Over =  SFT_MAX_V12_OUT;
//	m_sysParam.lV12Limit =  SFT_MIN_V12_OUT;
//	m_sysParam.nAutoReportInterval = SFT_DEFAULT_AUTO_REPORT_INTERVAL;

	//기본 Channel 구조 생성한다.(모듈이 접속하기 이전에 기본값 표시를 위해)
	SetChannelCount(GetChannelCount());
}

CModule::~CModule()
{
	::CloseHandle(m_hWriteEvent);
	::CloseHandle(m_hReadEvent);
	::CloseHandle(m_hSyncWriteEvent);

	if(m_pChannel)
	{
		delete [] m_pChannel;
		m_pChannel = NULL;
	}

	if(m_lpBuff)
	{
		delete[] m_lpBuff;
		m_lpBuff = NULL;
	}
}

void CModule::SetChannelCount(int nCount)
{
	ASSERT(nCount > 0);

	//Edited by KBH 2006/8/21
//////////////////////////////////////////////////////////////////////////
	if(NULL == m_pChannel)
	{
		m_pChannel = new CChannel[nCount];
	}
	else
	{
		if(nCount != m_nChCount)
		{
			delete [] m_pChannel;
			m_pChannel = NULL;
			
			m_pChannel = new CChannel[nCount];
		}
	}
//////////////////////////////////////////////////////////////////////////

/*	if(NULL != m_pChannel  )
	{
		delete [] m_pChannel;
		m_pChannel = NULL;
	}
*/
		
	m_nChCount = nCount;
	ASSERT(m_pChannel);

	for(int i=0; i<nCount; i++)
	{
		m_pChannel[i].SetChIndex(i);
	}
}

//모듈로 부터 전송되어 온 System 정보에 의해 Setting한다.
BOOL CModule::SetModuleSystem(SFT_MD_SYSTEM_DATA *lpSysData)
{
	//CheckModuleIDValidate()에서 검사 함 
	ASSERT(lpSysData);
	ASSERT(lpSysData->nInstalledChCount > 0 || lpSysData->nInstalledChCount <= SFT_MAX_CH_PER_MD);	//CheckModuleIDValidate() 에서 검사 함 
	
	memcpy(&m_sysData, lpSysData, sizeof(SFT_MD_SYSTEM_DATA));

	//reconstruct channel structure
	SetChannelCount(lpSysData->nInstalledChCount);
	
	
	//Reset Command Sequence Number;(이전에 전송된 명령 Sequence는 무시한다.)
	RestCmdSequenceNo();

	//접속 시간을 구한다.
	::GetLocalTime(&m_timeConnect);

	sprintf(m_szConnectedTime, "%u/%u/%u %u:%02u:%02u", m_timeConnect.wYear, m_timeConnect.wMonth, m_timeConnect.wDay, 
								m_timeConnect.wHour, m_timeConnect.wMinute, m_timeConnect.wSecond);
	
	return TRUE;
}

SFT_MD_SYSTEM_DATA * CModule::GetModuleSystem()
{
	return &m_sysData;
}

//Data Base에 설정된 값으로 Setting하고 
//모듈이 접속되면 DB값을 Update 시킴
BOOL CModule::SetModuleParam(int nModuleID, SFT_SYSTEM_PARAM *lpSysParam)
{
	if(lpSysParam == NULL)	return FALSE;
	if(nModuleID < 1 )	return FALSE;

	//Set Module ID
	m_nModuleID = nModuleID;

	//접속 이전에 DB에서 이전값들을 Load해서 Setting한다.
	//config Module Data
	strcpy(m_szIpAddress, lpSysParam->szIPAddr);
	m_sysData.nSystemType = lpSysParam->lModuleType;
	m_sysData.wVoltageRange = lpSysParam->wVRangeCount;
	m_sysData.wCurrentRange = lpSysParam->wIRangeCount;
	
	for(int i =0; i<5 && i<SFT_MAX_VOLTAGE_RANGE; i++)
		m_sysData.nVoltageSpec[i] = lpSysParam->lVSpec[i];

	for( i =0; i<5 && i<SFT_MAX_CURRENT_RANGE; i++)
		m_sysData.nCurrentSpec[i] = lpSysParam->lISpec[i];

	m_sysData.nInstalledChCount = lpSysParam->wInstallChCount;
	
	//reconstruct channel structure
	SetChannelCount(m_sysData.nInstalledChCount);

	return TRUE;
}

SFT_SYSTEM_PARAM CModule::GetModuleParam()
{
	SFT_SYSTEM_PARAM sysParam;
	ZeroMemory(&sysParam, sizeof(SFT_SYSTEM_PARAM));

	strcpy(sysParam.szIPAddr, m_szIpAddress);

	sysParam.lModuleType = m_sysData.nSystemType;
	sysParam.wVRangeCount = m_sysData.wVoltageRange; 
	sysParam.wIRangeCount = m_sysData.wCurrentRange;
	
	for(int i =0; i<5 && i<SFT_MAX_VOLTAGE_RANGE; i++)
		sysParam.lVSpec[i] = m_sysData.nVoltageSpec[i];

	for( i =0; i<5 && i<SFT_MAX_CURRENT_RANGE; i++)
		sysParam.lISpec[i] = m_sysData.nCurrentSpec[i];

	 sysParam.wInstallChCount = (WORD)m_sysData.nInstalledChCount;

	return sysParam;
}

HANDLE CModule::GetWriteEventHandle()
{
	return m_hWriteEvent;
}


WORD CModule::GetChannelState(int nChIndex)
{
	WORD state = PS_STATE_ERROR;
	if(nChIndex < 0 || nChIndex >= GetChannelCount())	return state;
	
	return m_pChannel[nChIndex].GetState();
}

CChannel* CModule::GetChannelData(int nChIndex)
{
	if(nChIndex >= 0 && nChIndex < GetChannelCount())	
		return &m_pChannel[nChIndex];
	
	return NULL;
}

BOOL CModule::SendCommand(UINT nCmd, LPSFT_CH_SEL_DATA pChData/* = NULL*/, LPVOID lpData/* = NULL*/, UINT nSize/* = 0*/)
{
	ASSERT(nSize >=0);

	//Buffer Over Flow Check
	if(nSize >= _SFT_TX_BUF_LEN)
	{
		m_CommandAck.nCode = SFT_TX_BUFF_OVER_FLOW;
		return FALSE;
	}

	//인자 검사 
	if((nSize > 0 && lpData == NULL) || (nSize == 0 && lpData != NULL))		
	{
		m_CommandAck.nCode = SFT_ERR_INVALID_ARGUMENT;
		return FALSE;
	}

//	//Channel 번호 검사
//	if(nChannel < 0 || nChannel > GetChannelCount())
//	{
//		m_CommandAck.nCode = SFT_ERR_CHANNEL_NOT_EXIST;
//		return FALSE;
//	}

	//Buffer에 Header를 만듬 
	LPSFT_MSG_HEADER pMsgHeader;
	pMsgHeader = (LPSFT_MSG_HEADER)m_szTxBuffer;
	ASSERT(pMsgHeader);
	MakeCmdSerial(pMsgHeader);		//Serial No
    pMsgHeader->nLength = nSize;
	pMsgHeader->nCommand = nCmd;
	pMsgHeader->wCmdType = 0;
	pMsgHeader->wNum = 0;
	if(pChData == NULL)
	{
		pMsgHeader->lChSelFlag[0] = 0;
		pMsgHeader->lChSelFlag[1] = 0;	
	}
	else
	{
		pMsgHeader->lChSelFlag[0] = pChData->nSelBitData[0];
		pMsgHeader->lChSelFlag[1] = pChData->nSelBitData[1];	
	}

	//선택 Channel 수를 Count 한다.
//	for(int i=0; i<2; i++)
//	{
//		for(int j =0; j<sizeof(DWORD)*8; j++)
//		{
//			if((dwSelCh[i] & (0x01 << j)) > 0)
//				pMsgHeader->wNum++;
//		}
//	}

	//Buffer에 Body Copy
	if(lpData != NULL && nSize > 0)
		memcpy((char *)m_szTxBuffer+SizeofHeader(), lpData, nSize);

	//Buffer에 전송길이 설정 
	m_nTxLength = nSize+SizeofHeader();

	//Line State Check
	if(m_state == PS_STATE_LINE_OFF)
	{
		m_CommandAck.nCode = SFT_FAIL;
		return FALSE;
	}

	//전송 
	SetWriteEvent();
	

//	LPVOID lpTData = new char[70000];
//	nSize = 70000;
	//Buffer에 Body Copy
/*	if(lpData != NULL && nSize > 0)
	{
		int nWriteSize = 0;
		int nWriteCount = 0;
		

		while(nSize > 0)
		{
			ResetSyncWriteEvent();
			TRACE("ResetSync\n");
			if(nSize > 1000)
				nWriteSize = 1000;
			else
				nWriteSize = nSize;

			if(nWriteCount == 0)
			{
				memcpy((char *)m_szTxBuffer+SizeofHeader(), lpData, nWriteSize);
				m_nTxLength = nWriteSize + SizeofHeader();
			}
			else
			{
				memcpy((char *)m_szTxBuffer, (BYTE*)lpData+(nWriteCount*1000), nWriteSize);
				m_nTxLength = nWriteSize;
			}

			nSize -= nWriteSize;

			//Line State Check
			if(m_state == PS_STATE_LINE_OFF)
			{
				m_CommandAck.nCode = SFT_FAIL;
				return FALSE;
			}

			//전송
			SetWriteEvent();
			::WaitForSingleObject(m_hSyncWriteEvent, 500);
			TRACE("Detect SetSync ==> nSize : %d, nWriteSize : %d, nWriteCount : %d\n", nSize, nWriteSize, nWriteCount);
			
			nWriteCount++;

		}
		TRACE("Exit While\n");
		return TRUE;
	}

	//Buffer에 전송길이 설정 
	m_nTxLength = nSize+SizeofHeader();

	//Line State Check
	if(m_state == PS_STATE_LINE_OFF)
	{
		m_CommandAck.nCode = SFT_FAIL;
		return FALSE;
	}

	//전송 
	SetWriteEvent();*/

	
	
	return TRUE;
}

//Clinet 로 부터 ACK Cmd를 Read
int CModule::ReadAck()
{
	if(m_state == PS_STATE_LINE_OFF)
	{
		return SFT_FAIL;
	}

	int nAck = SFT_FAIL;
	if (::WaitForSingleObject(m_hReadEvent, SFT_MSG_TIMEOUT) == WAIT_OBJECT_0 )
	{
		nAck = m_CommandAck.nCode;
	}
	else
	{
		nAck =  SFT_TIMEOUT;
	}
	
	//Reset Read state
	::ResetEvent(m_hReadEvent);
	m_CommandAck.nCode = SFT_FAIL;

	//TRACE("Read Response of Command %d\n", nAck);
	Sleep(0);
	
	return nAck;
}

//Client에서 원하는 Size 만큼의 Data를 읽어 들임
LPVOID CModule::ReadData(int nSize)
{
	LPVOID	lpData = NULL;

	//라인 상태 검사
	if(m_state == PS_STATE_LINE_OFF)
	{
		return NULL;
	}
	
	//원하는 Size 만큼 이미 읽어 들임
	if (m_nRxLength >= nSize )		
	{
		lpData = (LPVOID)m_szRxBuffer;
	} 
	else 
	{
		//아직 읽혀 지지 않았을 경우 한번 더 기다림 
//		::ResetEvent(m_hReadEvent);
		if (::WaitForSingleObject(m_hReadEvent, SFT_MSG_TIMEOUT) == WAIT_OBJECT_0 )
		{
			if (m_nRxLength >= nSize ) 
			{
				lpData = (LPVOID)m_szRxBuffer;
			}
		}
		else
		{
			TRACE("Data Wait Time Out\n");
			lpData = NULL;
		}
	}
	
	//Read Evnet Reset 시킴 
	ResetReadEvent();

	return lpData;
}

//Cmd Header 생성
BOOL CModule::MakeCmdSerial(SFT_MSG_HEADER *pMsgHeader)
{
	ASSERT(pMsgHeader);

//	pMsgHeader->nCommand = nCmd;
	pMsgHeader->lCmdSerial = m_nCmdSequence++;
//	pMsgHeader->wCmdType = 0;
//	pMsgHeader->wNum = 0;
//	pMsgHeader->lChSelFlag[0] = 0;
//	pMsgHeader->lChSelFlag[1] = 0;
	

	return TRUE;
}

//재접속시 Cmd Sequence를 초기화 시킨다.
void CModule::RestCmdSequenceNo()
{
	m_nCmdSequence = 0;
}

void CModule::SetStateData(const SFT_MD_STATE_DATA &stateData )
{
	m_doorState = stateData.doorState;
	m_jigState = stateData.jigState;
	m_state = stateData.state;
	m_trayState = stateData.trayState;
}

int CModule::GetChannelCount()
{
//	int aa = m_sysData.wChannelPerBoard*m_sysData.wInstalledBoard;
//	return aa;

	return m_sysData.nInstalledChCount;
}

//
//void CModule::SetModuleNo(UINT nModuleNo)
//{
//	m_nModuleID = nModuleNo;
//}

//void CModule::SetModuleIndex(UINT nIndex)
//{
//	m_nModuleIndex = nIndex;
//}
//
//UINT CModule::GetModuleIndex()
//{
//	return m_nModuleIndex;
//}


BOOL CModule::GetHWChCount()
{

	int aa = 0;
	return aa;
}

BOOL CModule::SendResponse(LPSFT_MSG_HEADER lpReceivedCmd, int nCode)
{
	ASSERT(lpReceivedCmd);

	//Buffer 구조체 만듬
	SFT_PACKET_RESPONSE *pResponsePacket;
	pResponsePacket = (SFT_PACKET_RESPONSE *)m_szTxBuffer;
	ASSERT(pResponsePacket);

	ZeroMemory(&pResponsePacket, sizeof(SFT_PACKET_RESPONSE));
	pResponsePacket->msgHeader.nCommand = SFT_CMD_RESPONSE;				
	pResponsePacket->msgHeader.lCmdSerial = lpReceivedCmd->lCmdSerial;	// Command Serial Number..(Command ID)
//	pResponsePacket->msgHeader.wCmdType;	
//	pResponsePacket->msgHeader.wNum;									// 이 Command에 해당 사항이 있는 채널의 수
//	pResponsePacket->msgHeader.lChSelFlag[2];							// 이 시험조건이 전송될 Channel (Bit 당 Channel)
	pResponsePacket->msgHeader.nLength = sizeof(SFT_RESPONSE);			// Size of Body

	pResponsePacket->msgBody.nCmd = lpReceivedCmd->nCommand;			//default Reponse Code is Ack
	pResponsePacket->msgBody.nCode = nCode;


	//Buffer에 전송길이 설정 
	m_nTxLength = pResponsePacket->msgHeader.nLength+SizeofHeader();

	//Line State Check
	if(m_state == PS_STATE_LINE_OFF)
	{
		m_CommandAck.nCode = SFT_FAIL;
		return FALSE;
	}

	//전송 
	SetWriteEvent();
	
	return TRUE;
}


LPVOID CModule::GetBuffer()
{
	return m_lpBuff;
}

void CModule::SetBuffer(LPVOID lpBuff)
{
	if(m_lpBuff)
	{
		delete [] m_lpBuff;
		m_lpBuff = NULL;
	}

	m_lpBuff = lpBuff;
}

SYSTEMTIME * CModule::GetConnectedTime()
{
	return &m_timeConnect;
}

void	CModule::SetState(WORD state)
{	
	m_state = state;
	
	//Line Off시 모든 Data를 0으로 표시
	if(state == PS_STATE_LINE_OFF)
	{
		if(m_pChannel != NULL)
		{
			for(int ch =0; ch < GetChannelCount(); ch++)
			{
				m_pChannel[ch].ResetData();
			}
		}

	}
}
