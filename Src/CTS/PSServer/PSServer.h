// PSServer.h : main header file for the PSServer DLL
//

#if !defined(AFX_PSServer_H__E95808E9_0017_43B1_A555_47E448347F19__INCLUDED_)
#define AFX_PSServer_H__E95808E9_0017_43B1_A555_47E448347F19__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPSServerApp
// See PSServer.cpp for the implementation of this class
//

class CPSServerApp : public CWinApp
{
public:
	CString strOldProfileName;
//	BOOL InitInstance();
	CPSServerApp();
	~CPSServerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPSServerApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPSServerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSServer_H__E95808E9_0017_43B1_A555_47E448347F19__INCLUDED_)
