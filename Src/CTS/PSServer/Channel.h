// Channel.h: interface for the CChannel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHANNEL_H__723C1F2A_DDB9_467C_AF0C_0BC95D071DF5__INCLUDED_)
#define AFX_CHANNEL_H__723C1F2A_DDB9_467C_AF0C_0BC95D071DF5__INCLUDED_

#include <afxtempl.h>			//for CList by KBH

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_DATA_STACK_SIZE		2000
#define MAX_MSEC_DATA_POINT		100

#include <afxmt.h>


class CChannel  
{
public:
	WORD GetGotoCycleNum();
	WORD GetSharingInfo();
	int GetProfileIntExt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault);
	BOOL SetStepStartData(SFT_MSEC_CH_DATA_INFO *pInfo, LPVOID lpData);
	void ResetData();
	WORD GetStepType();
	WORD GetCurCycleCount();
	WORD GetTotalCycleCount();
	UINT GetSaveDataStackSize();
	BOOL PopSaveData(SFT_CH_DATA &chData);
	void PushSaveData(SFT_CH_DATA chData);
	UINT GetTotalTime();
	WORD GetStepNo();
	long GetWattHour();
	long GetWatt();
	BYTE GetGradeCode(int nItem = 0);
	BYTE GetCode();
	UINT GetStepTime();
	WORD GetState();
	long GetImpedance();
	long GetCapacity();
	long GetCurrent();
	long GetVoltage();
	long GetAvgCurrent();
	long GetAvgVoltage();
	long GetCapacitySum();
	long GetWattHourSum();
	long GetTemperature();
	long GetPressure();
	
	BYTE GetReservedCmd()	{	return m_CmdReserved;	}

	void SetChannelData(SFT_CH_DATA chData);
	void SetChIndex(int index)	{	m_wChIndex = (WORD)index;	}
	operator = (const CChannel& chData);

	CChannel();
	virtual ~CChannel();

//	LPVOID			m_lpStepStartData;
	SFT_MSEC_CH_DATA			m_aStepStartData[MAX_MSEC_DATA_POINT];
	SFT_MSEC_CH_DATA_INFO	m_StepStartDataInfo;
	
private:

    WORD			m_wChIndex;				//Zero Base Channel No
	CCriticalSection	cs;

	WORD			m_state;
    BYTE			m_grade[PS_MAX_STEP_GRADE];
    BYTE			m_cellCode;
	WORD			m_nStepNo;
	unsigned long	m_ulStepTime;
    unsigned long	m_ulTotalTime;
    long			m_lVoltage;
    long			m_lCurrent;
    long			m_lWatt;
    long			m_lWattHour;
    long			m_lCapacity;
    long 			m_lImpedance;
	long			m_lAvgVoltage;
	long			m_lAvgCurrent;
	WORD			m_wStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	long			m_lTemparature;
	long			m_lPressure;
	WORD			m_nCurrentCycleNum;
	WORD			m_nTotalCycleNum;
	long			m_lCapacitySum;
	long			m_lWattHourSum;
	
	//2006/06/08
	BYTE			m_CmdReserved;
	BOOL			m_nRunMode;

	//2008/10/21 kjh&kky use for Sharing Channel 
	WORD			m_wSharingInfo;

	//2008/12/02 KHS
	WORD			m_nGotoCycleNum;

	CList<SFT_CH_DATA, const SFT_CH_DATA& > m_aSaveDataList;   // List of Save data
protected:
	long m_lStartCellCheckVoltageLimit;
	long m_lEndCellCheckVoltageLimit;
	void ConverChData(SFT_CH_DATA &chData);
};

#endif // !defined(AFX_CHANNEL_H__723C1F2A_DDB9_467C_AF0C_0BC95D071DF5__INCLUDED_)
