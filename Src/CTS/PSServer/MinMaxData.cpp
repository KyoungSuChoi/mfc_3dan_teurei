// MinMaxData.cpp: implementation of the CMinMaxData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MinMaxData.h"
#include <math.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMinMaxData::CMinMaxData()
{
	ResetData();
}

CMinMaxData::~CMinMaxData()
{

}

void CMinMaxData::AddData(long lData)
{
	m_lValue = lData;
	m_nCount++;										//Index Count
	m_lSum += lData;								//Data Sum
	m_sqareSum += ((double)lData*(double)lData);	//Square Sum
	m_fAvg = (float)((float)m_lSum / (double)m_nCount);						//Average
	m_dStdD = (long)sqrt(fabs(m_sqareSum/(double)m_nCount - (double)(m_fAvg*m_fAvg)));

	SYSTEMTIME systime;
	::GetLocalTime(&systime);

	if(m_nCount == 1)		//If First Data
	{
		m_nMinIndex = m_nCount-1;
		m_nMaxIndex = m_nCount-1;
		m_lMin = lData;
		m_lMax = lData;
		sprintf(m_szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
		sprintf(m_szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
	}
	else
	{
		if(m_lMax < lData)		//Check Max Value
		{
			m_lMax = lData;
			m_nMaxIndex = m_nCount;
			sprintf(m_szMaxDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
		}
		else if(m_lMin > lData)	//Check Min Value
		{
			m_lMin = lData;
			m_nMinIndex = m_nCount;
			sprintf(m_szMinDateTime, "%04d/%02d/%02d %02d:%02d:%02d", systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond);
		}
	}
}

void CMinMaxData::ResetData()
{
	m_lValue = 0;
	m_lMax = 0;
	m_lMin = 0;
	m_fAvg = 0;
	m_dStdD = 0.0;
	m_nMaxIndex = 0;
	m_nMinIndex = 0;
	strcpy(m_szMaxDateTime, "");
	strcpy(m_szMinDateTime, "");

	m_nCount = 0;
	m_lSum = 0;
	m_sqareSum = 0;
}
