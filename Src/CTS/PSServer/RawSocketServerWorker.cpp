/////////////////////////////////////////////////////////////////////
// Class Creator Version 2.0.000 Copyrigth (C) Poul A. Costinsky 1994
///////////////////////////////////////////////////////////////////
// Implementation File RawSocketServerWorker.cpp
// class CWizRawSocketServerWorker
//
// 16/07/1996 17:53                             Author: Poul
///////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RawSocketServerWorker.h"
#include "Module.h"

#ifdef _DEBUG
//#include "Mmsystem.h"
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

/*
//Message Direction
#define _HOST_TO_MODULE		0x00
#define _MODULE_TO_HOST		0x01

//#define CheckMsgDirection(x)	((x) == _MODULE_TO_HOST ? TRUE : FALSE)	
*/

extern CModule	*m_lpModule;
//extern BOOL	InitGroupData (int nModuleIndex);	//Module Data
extern BOOL WriteLog(char *szLog);



//Channel Mapping FTN
extern int	g_nChMappingType;	
extern int g_anChannelMap[SFT_MAX_CH_PER_MD];



void ModuleClosed (int nModuleIndex , HWND hMsgWnd);
void ModuleConnected(int nModuleIndex, HWND hMsgWnd);
BOOL ParsingCommand(CWizReadWriteSocket *pSock, int nGroupIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd);
//void MakeMsgHeader (LPSFT_MSG_HEADER pHeader, int nBoardIndex, int nChannelIndex, int nCommand);
int ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hShutDownEvent);
void ProcessingMonitoringData(UINT nModuleIndex, void *pReceivedBuffer, int nSize, HWND hMsgWnd);


//BYTE GetNextPacketID();

///////////////////////////////////////////////////////////////////
//*****************************************************************
inline void ThrowIfNull(void* p)
{
	if (p == NULL)	AfxThrowMemoryException();
}

///////////////////////////////////////////////////////////////////
// class CWizRawSocketListener
///////////////////////////////////////////////////////////////////
//**********************************************************//
// Default Constructor										//
CWizRawSocketListener::CWizRawSocketListener(int nPort)     //
	: m_pListenSocket (NULL),								//
	  m_nPort         (nPort)								//
{															//
//	m_pDoc = NULL;	
	m_hMessageWnd = NULL;									//
}															//
//**********************************************************//
// Destructor												//
CWizRawSocketListener::~CWizRawSocketListener()				//
{															//
	if (m_pListenSocket != NULL)							//
	{														//
		TRACE("You Must call SFTCloseFormServer() before your application is closed\n");
		ASSERT(0);											//
		delete m_pListenSocket;								//
	}														//
}															//
//**********************************************************//

void CWizRawSocketListener::SetMsgWnd(HWND hMessageWnd)
{
	m_hMessageWnd = hMessageWnd;
}

// Method called from dispath thread.
void CWizRawSocketListener::Prepare ()
{
	// Create listening socket
	if (m_pListenSocket != NULL)
	{
		ASSERT(0);
		delete m_pListenSocket;
	}
	m_pListenSocket = new CWizSyncSocket(m_nPort);
	ThrowIfNull (m_pListenSocket);

#ifdef _DEBUG
	TCHAR buff[100];
	unsigned int nP;
	VERIFY(m_pListenSocket->GetHostName (buff,100,nP));
	TRACE(_T("Control Server Listening at %s:%d\n"), buff, nP);
#endif
}

//*****************************************************************
// Method called from dispath thread.
void CWizRawSocketListener::CleanUp()
{
	// close and destroy listening socket
	delete m_pListenSocket;
	m_pListenSocket = NULL;
}

//*****************************************************************
// Method called from dispath thread.
BOOL CWizRawSocketListener::WaitForData (HANDLE hShutDownEvent)
{
	HANDLE *lpHandles;
	lpHandles = new HANDLE[2];

	WSAEVENT myObjectEvent = WSACreateEvent();
	lpHandles[0]= myObjectEvent;
	lpHandles[1]= hShutDownEvent;

	WSAEventSelect( m_pListenSocket->H(), myObjectEvent, FD_ACCEPT);
	
	DWORD rtn;
	SOCKET h;

	while (1)
	{
		rtn = ::WaitForMultipleObjects(2, lpHandles, FALSE,   INFINITE);
		if( (rtn - WAIT_OBJECT_0) == 0 )
			 h = m_pListenSocket->Accept();
		else 
		{
			 WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return FALSE;
		}

		// Get accepted socket.	 If it's connected client, go to serve it.
		if (h != INVALID_SOCKET)
		{
//			WriteLog("A socket accepted");
			m_hAcceptedSocket = h;
			WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return TRUE;
		}
	} // while 1
	return TRUE;
}

//*****************************************************************
// Method called from dispath thread.
// return TRUE : Wait clinet connection
// return FALSE:  Server Close
//****************************************************************
BOOL CWizRawSocketListener::TreatData   (HANDLE hShutDownEvent, HANDLE hDataTakenEvent)
{
	char	msg[128];								//Display Message Buffer
	UINT	nPortNo, reqSize = 0;								//Receive Port No
	int		msgReadState  = _MSG_ST_CLEAR;			//Read Done Flag
	int		offset = 0, nRtn =0;		//Data Point Indicator
	
	LPSFT_MSG_HEADER	 lpMsgHeader;				//Message Header	
	lpMsgHeader = new SFT_MSG_HEADER;
	ASSERT(lpMsgHeader);

	LPVOID lpReadBuff = NULL;						//Body read Buffer
	UINT	nBuffSize = 0;							//size of buffer
	
	HANDLE 	*lpHandles;								//Handle
	lpHandles = new HANDLE[3];
	ASSERT(lpHandles);

////////////////////////////////////////////////////////////////
	INT		nModuleID = 0, nModuleIndex =0;			//Return Value, Module ID
	int idx;										// arrHandles index

//	LPVOID lpEndDataBuff = NULL;

	int nEndDataSize;
//	SFT_SYSTEM_PARAM *pParam = NULL;
	long nWaitCount = 0;
	
	// Create client side socket to communicate with client.
	CWizReadWriteSocket clientSide (m_hAcceptedSocket);
	// Signal dispather to continue waiting.
	::SetEvent(hDataTakenEvent);

	WSAEVENT myObjectEvent = WSACreateEvent();		//Event
	lpHandles[0] = myObjectEvent;					//Read 
	lpHandles[1] = hShutDownEvent;					//Server Side Shut down
	
	WSAEventSelect(clientSide.H(), myObjectEvent, FD_READ|FD_CLOSE);		//select Read and close event
	LPWSANETWORKEVENTS lpEvents = new WSANETWORKEVENTS;
	ASSERT(lpEvents);
	
	//Start Connection Sequence
	if((nModuleIndex = ConnectionSequence(&clientSide, lpHandles)) < 0)
	{
		sprintf(msg, "Module %d Connection Error. (Code %d)", nModuleID, nModuleIndex);
		goto _CLOSE_MODULE;
	}

	lpHandles[2] = m_lpModule[nModuleIndex].GetWriteEventHandle();
	nModuleID = m_lpModule[nModuleIndex].GetModuleID();
	//----------------Module Connection Complited-----------------------------//

	clientSide.GetPeerName(m_lpModule[nModuleIndex].m_szIpAddress, sizeof(m_lpModule[nModuleIndex].m_szIpAddress), nPortNo);	//Get Client Side Ip Address and Port Number
	sprintf(msg, "Module %d Connected From %s :: %d", nModuleID, m_lpModule[nModuleIndex].m_szIpAddress, nPortNo);
	WriteLog(msg);
	
//	InitGroupData (nModuleIndex);					//Make Group Data Structure
//	m_lpModule[nModuleIndex].SetModuleSystem();
	
	ModuleConnected(nModuleIndex, m_hMessageWnd);	//Send Message to Main Wnd

	//-----------------start Socket Read/Write--------------------------------//
	//  [7/23/2009 kky ]
	// for ljb 데이터 반복해서 써지는 문제 수정
	while(1)
	{
		nRtn = ::WaitForMultipleObjects(3, lpHandles, FALSE, SFT_HEART_BEAT_TIMEOUT);		//Wait for Read or Write Event

		if( nRtn == WAIT_FAILED )
		{
			sprintf(msg, "Module %d message failed %d, Disconnect module %d", nModuleID, GetLastError(), nModuleID);
			goto _CLOSE_MODULE;
		}
		else if( nRtn == WAIT_TIMEOUT )
		{
			nWaitCount++;
			sprintf( msg, "Module %d data read time out, Count %d", nModuleID, nWaitCount );
			if( nWaitCount > _SFT_MAX_WAIT_COUNT )
			{
				sprintf( msg, "Module %d Data Read Time Out. Disconnect Module %d", nModuleID, nModuleID );
				goto _CLOSE_MODULE;
			}
		}
		else if (nRtn >= WAIT_ABANDONED_0 && nRtn < WAIT_ABANDONED_0 + 3)
		{
			idx = nRtn - WAIT_ABANDONED_0;
			ResetEvent(lpHandles[idx]);
			continue;
		}
		else
		{
			idx = nRtn -WAIT_OBJECT_0;

			switch(nRtn)
			{		
			case WAIT_OBJECT_0:		// Client socket event FD_READ
				if(SOCKET_ERROR == ::WSAEnumNetworkEvents(clientSide.H(), myObjectEvent, lpEvents))		//Get Client side Socket Event
				{ 
					sprintf(msg, "Module %d socket error. (Code %d)", nModuleID, WSAGetLastError());
					goto _CLOSE_MODULE;
				}
			
				//---------------Data Read Event ---------------------------//
				if (lpEvents->lNetworkEvents & FD_READ)		//Read Event
				{
					nWaitCount = 0;		//Reset Wait Count;
					
	//				TRACE("Read Event Detect\n");
					//--------------------Message Head Read -----------------//
					if( msgReadState & (_MSG_ST_CLEAR | _MSG_ST_HEADER_REMAIN))				//Read Message Header
					{
						if(msgReadState & _MSG_ST_CLEAR)	reqSize = SizeofHeader();		
						offset = SizeofHeader() - reqSize;
						nRtn = clientSide.Read((char *)lpMsgHeader+offset, reqSize);		//Read Message Header
						
						if (nRtn != reqSize) 
						{
							if(nRtn == SOCKET_ERROR)	//Socket Error
							{
								msgReadState = _MSG_ST_CLEAR;
								sprintf(msg, "Module %d message header read fail. (Code %d)\n", nModuleID, WSAGetLastError());
								WriteLog(msg);
								break;
							}
							else
							{
								msgReadState = _MSG_ST_HEADER_REMAIN;
								TRACE("Module %d Message Header Data %d Byte Read %d Byte Remain\n", nModuleID, nRtn, reqSize);
							}
							reqSize -= nRtn;
						}
						else		//Read Done
						{
	//						TRACE("Module %d Message Header Receive Success. Cmd 0x%x(%d)\n", nModuleID, lpMsgHeader->nCommand, lpMsgHeader->nLength);
							if(lpMsgHeader->nLength <= 0 )	//
							{
								msgReadState = _MSG_ST_CLEAR;
							}
							else
							{
								msgReadState = _MSG_ST_HEAD_READED;
							}
						}
					} 
					else	//--------------------Message Body Read -----------------////Read Body And Command Parsing	
					{
						if (msgReadState & _MSG_ST_HEAD_READED)
						{
							reqSize = lpMsgHeader->nLength;		
							//too long packet 
							if(reqSize > _SFT_MAX_PACKET_LENGTH)
							{
								msgReadState = _MSG_ST_CLEAR;
								sprintf(msg, "Module %d message packet length error. receive %d/Max %d\n", nModuleID, reqSize, _MSG_ST_CLEAR);
								WriteLog(msg);
								goto _CLOSE_MODULE;
								break;
							}
							
							//if size is mismatch then release preallocated memory
							if(nBuffSize != reqSize && reqSize > 0)
							{
								if(lpReadBuff)		delete[] lpReadBuff;		

								//new allocate memory
								lpReadBuff = new char[reqSize];
								ASSERT(lpReadBuff);
								nBuffSize = reqSize;
							}
						}

						offset = lpMsgHeader->nLength - reqSize;
						nRtn = clientSide.Read((char *)lpReadBuff+offset, reqSize);
						if (nRtn != reqSize) 
						{
							if(nRtn == SOCKET_ERROR)	//Socket Error
							{
								msgReadState = _MSG_ST_CLEAR;
								sprintf(msg, "Module %d message body read fail. (Code %d)\n", nModuleID, WSAGetLastError());
								WriteLog(msg);
								break;
							}
							else
							{
								msgReadState = _MSG_ST_REMAIN;
	//							TRACE("Module %d Message Body Data %d Byte Read %d Byte Remain\n", nModuleID, nRtn, reqSize);
							}
							reqSize -= nRtn;
						}
						else		//Read Done
						{
	//						TRACE("Module %d Message Body Receive Success. Cmd 0x%x(%d)\n", nModuleID, lpMsgHeader->nCommand, lpMsgHeader->nLength);
							msgReadState = _MSG_ST_CLEAR;
						}
						
					}
					
					///---------------Parsing Command---------------------///
					if(	msgReadState & _MSG_ST_CLEAR)
					{
						nEndDataSize = ParsingCommand(&clientSide, nModuleIndex, lpMsgHeader, lpReadBuff, m_hMessageWnd);	//Parsing Recevied Command 
						
						//Step 결과 Data일 경우 저장 Memory를 따로 할당 받어 App에 넘김
						//(처리 속도 지연등이 있어서 Monitoring Data로 Update 될지 모르므로)
	//					if(lpMsgHeader->nCommand == SFT_CMD_AUTO_GP_STEP_END_DATA && nEndDataSize == TRUE)
	//					{
	//						nEndDataSize = lpMsgHeader->nLength-SizeofHeader();
	//						if(lpEndDataBuff != NULL)
	//						{
	//							delete[] lpEndDataBuff;
	//							lpEndDataBuff = NULL;
	//						}	
	//						lpEndDataBuff = new char[nEndDataSize];
	//						ASSERT(lpEndDataBuff);
	//							
	//						memcpy(lpEndDataBuff, lpReadBuff, nEndDataSize);
	//						if(m_hMessageWnd)
	//							::PostMessage(m_hMessageWnd, SFTWM_STEP_ENDDATA_RECEIVE, 
	//											(WPARAM)MAKELONG(lpMsgHeader->nGroupNum-1, m_lpModule[nModuleIndex].GetModuleID()), 
	//											(LPARAM)lpEndDataBuff
	//								 );			//Send to Parent Wnd Step End Data Received
	//						//TRACE("Module %d::Step Result Data Received\n", m_stModule[nModuleIndex].sysParam.nModuleID);
	//					}
					}
				}
				else if(lpEvents->lNetworkEvents == FD_CLOSE)		//Client Disconnected
				{
					sprintf(msg, "Disconnect event detected from Module %d.(Code %d)", nModuleID, lpEvents->lNetworkEvents);
					goto _CLOSE_MODULE;
				}
				else //else of if (lpEvents->lNetworkEvents & FD_READ) is Not Read Event
				{
					if (lpEvents->lNetworkEvents != 0) 
					{
						sprintf(msg, "Module %d UnKnown event %d detected. Disconnect Module %d", nModuleID, lpEvents->lNetworkEvents, nModuleID);
						goto _CLOSE_MODULE;
					}
				}
				//ResetEvent(myObjectEvent);
				//WSAEventSelect(clientSide.H(), myObjectEvent, FD_READ|FD_CLOSE);
			break;
			
			case WAIT_OBJECT_0 + 1: // Server side shutdown event
				{
					TRACE("ShutDown Event\n");
					sprintf(msg, "Server shutdown event event detected. Disconnect Module %d", nModuleID);
					WriteLog(msg);
					clientSide.Close();
					if(lpReadBuff)
					{
						delete [] lpReadBuff;			lpReadBuff = NULL;
					}
	//				if(lpEndDataBuff)
	//				{
	//					delete [] lpEndDataBuff;		lpEndDataBuff = NULL;
	//				}
					delete lpMsgHeader;
					delete [] lpHandles;
					delete lpEvents;
					return FALSE;			
				}
			
			case WAIT_OBJECT_0 + 2:	// client side write event
				{
	//				m_TxBuffCritic.Lock();	
	//				TRACE("Write Event\n");
					ASSERT(m_lpModule[nModuleIndex].m_nTxLength < _SFT_TX_BUF_LEN);
					nRtn = clientSide.Write(m_lpModule[nModuleIndex].m_szTxBuffer, m_lpModule[nModuleIndex].m_nTxLength);
	//#ifdef _DEBUG			
					LPSFT_MSG_HEADER lpHeader = (LPSFT_MSG_HEADER)m_lpModule[nModuleIndex].m_szTxBuffer;
					sprintf(msg, "Send command 0x%08x to Module %d,  %d/%d Byte ==>>", 
						lpHeader->nCommand, nModuleID, nRtn, m_lpModule[nModuleIndex].m_nTxLength);
	//				TRACE("%s\n", msg);
					WriteLog(msg);
	//#endif //_DEBUG	
					
					if(nRtn != m_lpModule[nModuleIndex].m_nTxLength)
					{
						sprintf(msg, "Module %d socket write error, %d Byte write - %d Byte remain", nModuleID, nRtn, m_lpModule[nModuleIndex].m_nTxLength - nRtn);
						WriteLog(msg);
					}
					//sprintf(msg, "Module %d - Socket Write %d Byte", nModuleID, nRtn);
					//WriteLog(msg);

					m_lpModule[nModuleIndex].ResetWriteEvent();
		//			m_lpModule[nModuleIndex].SetSyncWriteEvent();
					break;
				}
			}	// switch
			ResetEvent(lpHandles[idx]);
		}
	}	//while(1)


//Module Conntection Colse Label
_CLOSE_MODULE:				//Release Memory and close client socket
	WriteLog(msg);
	clientSide.Close();

///////////////
	ModuleClosed(nModuleIndex, m_hMessageWnd);
//	if(lpEndDataBuff)
//	{
//		delete [] lpEndDataBuff;	lpEndDataBuff = NULL;
//	}
///////////////

	if(lpReadBuff)
	{
		delete [] lpReadBuff;		lpReadBuff = NULL;
	}
	delete lpMsgHeader;			lpMsgHeader = NULL;
	delete [] lpHandles;		lpHandles = NULL;
	delete lpEvents;			lpEvents = NULL;
	return TRUE;
}

void ModuleConnected(int nModuleIndex, HWND hMsgWnd)
{
	if(m_lpModule == NULL || nModuleIndex < 0)	return ;					//Form Server is closed
	m_lpModule[nModuleIndex].SetState(PS_STATE_LINE_ON);					//Module Line On State

//	int nModuleID =  m_lpModule[nModuleIndex].sysData.nModuleID;			//Get Module ID
/*	for(INDEX x =0; x<m_stModule[nModuleIndex].sysData.nTotalGroupNo; x++)
	{
		LPSFT_GROUP_INFO lpGPData;
		lpGPData = (SFT_GROUP_INFO *)m_stModule[nModuleIndex].gpData[x];
		ASSERT(lpGPData);
		lpGPData->gpData.gpState.state = SFT_STATE_LINE_ON;
	}
*/
	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_CONNECTED, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)m_lpModule[nModuleIndex].GetModuleSystem());		//Send to Parent Wnd to Module Conntected 


	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_STATE_CHANGE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)0);		//Send to Parent Wnd To Module State Change
//	TRACE("Module %d Connected\n", nModuleID);
}

void ModuleClosed (int nModuleIndex , HWND hMsgWnd)
{
	if(m_lpModule == NULL || nModuleIndex < 0)	return ;					//Form Server is closed
	m_lpModule[nModuleIndex].SetState(PS_STATE_LINE_OFF);						//Module Line Off State

//	int nModuleID = m_lpModule[nModuleIndex].sysData.nModuleID;				//Get Module ID
/*	if(m_stModule[nModuleIndex].gpData.GetSize() != m_stModule[nModuleIndex].sysData.nTotalGroupNo)	return;
	for(INDEX x =0; x<m_stModule[nModuleIndex].sysData.nTotalGroupNo; x++)
	{
		LPSFT_GROUP_INFO lpGPData;
		lpGPData = (SFT_GROUP_INFO *)m_stModule[nModuleIndex].gpData[x];
		ASSERT(lpGPData);
		lpGPData->gpData.gpState.state = SFT_STATE_LINE_OFF;
	}
*/	
	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_CLOSED, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)0);	//Send to Parent Wnd to Module Close Message

	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_STATE_CHANGE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)0);			//Send to Parent Wnd to Module State change
//	TRACE("Module %d Disconnected\n", nModuleID);
}
/*
BYTE GetNextPacketID()
{
	static BYTE bPacketSerial = 0;
	return bPacketSerial++;
}

void	MakeMsgHeader (LPSFT_MSG_HEADER pHeader, int nGroupIndex, int nChannelIndex, int nCommand)
{
//	pHeader->bDirection = _HOST_TO_MODULE;
//	pHeader->bMessageID = GetNextPacketID();
//	pHeader->nID = SFT_ID_FORM_OP_SYSTEM;
	pHeader->nChannelNum = (WORD)nChannelIndex;
	pHeader->nCommand = nCommand;
	pHeader->nLength = SizeofHeader();
}

UINT	MakeCheckSum(LPVOID lpData, int nSize)
{
	UINT nSum = 0;
	BYTE *data = (BYTE *)lpData;
	while(nSize-->0)
	{
		nSum+=*data;
	}
	return nSum;
}
*/
inline int CheckMsgHeader(LPSFT_MSG_HEADER lpHeader)
{
	ASSERT(lpHeader);

#ifdef _DEBUG
	char msg[128];
	sprintf(msg, "==> CMD [0x%x] Serial [%03d] :: CH [0x%x/0x%x], Type [%d], Num [%d], Size [%d]\n", lpHeader->nCommand, lpHeader->lCmdSerial, 
				lpHeader->lChSelFlag[1], lpHeader->lChSelFlag[0], 
				lpHeader->wCmdType, lpHeader->wNum, lpHeader->nLength);
	if(lpHeader->nCommand != SFT_CMD_CH_DATA && lpHeader->nCommand != SFT_CMD_HEARTBEAT)
	{
		WriteLog(msg);
//		TRACE(msg);
	}
#endif

//	if(lpHeader->wGroupNum < 0 || lpHeader->wGroupNum > m_stModule[nModuleIndex].sysData.nTotalGroupNo)	return -2;
//	if(lpHeader->wChannelNum < 0 || lpHeader->wChannelNum > m_stModule[nModuleIndex].sysData.awChInGroup[lpHeader->wGroupNum-1])	return -3;
//	if(GetCmdDataSize(lpHeader->nCommand) != (lpHeader->nLength - SizeofHeader()))	return -4;

	for(int i=0; i<32; i++)
	{
		//최하위 채널을 return 한다. 
		if(0x01 << i & lpHeader->lChSelFlag[0])		
		{
			return i+1;
		}

		if(0x01 << i & lpHeader->lChSelFlag[1])		
		{
			return i+33;
		}
	}
	
	return 0;
}

//Parsing Response Command 
//Ack 가 필요 하지 않은 명령은 반드시 return 하여야 한다.
BOOL ParsingCommand(CWizReadWriteSocket *pSock, int nModuleIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd)
{
	ASSERT(pSock);
	ASSERT(lpMsgHeader);
//	ASSERT(lpReadBuff);		//최초 Command가 Body 없는 명령일 경우 lpReadBuff가 NULL이다.

	char				msg[128];
	unsigned long		wrSize = 0;
	int					nChannel = 0;
	INT					nResponseCode = SFT_ACK;

	LPSFT_MSG_HEADER lpHeader = (LPSFT_MSG_HEADER)lpMsgHeader;

	if((nChannel = CheckMsgHeader(lpHeader)) < 0)		//Header Check
	{
		sprintf(msg, "Module %d Message Header Error. Cmd = 0x%x [Code= %d]", m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand, nChannel);
		WriteLog(msg);
		return FALSE;
	}

	switch ( lpHeader->nCommand )
	{
	// 모듈이 최초 접속시에 보내는 모듈 아이디	//
	case SFT_CMD_INFO_DATA :			// SBC -> HOST
		{
			return TRUE;
		}
	
	// 모니터링 결과 데이터	매 1초마다 보고되는 Data//
	case SFT_CMD_CH_DATA :
		{
//			if(lpHeader->nLength == sizeof(SFT_CH_DATA)*m_lpModule[nModuleIndex].GetChannelCount() && lpReadBuff != NULL)
			//SFT_CH_DATA * SFT_MAX_CH_PER_MD 수로 전송되어 옴 
			//앞에서 부터 필요한 수량만큼 Data를 취득함 
			
			//sizeof(SFT_CH_DATA) * MAX_CH_PER_MD(64) 크기로 전송되어 온다.
			//MSG_HEADER flag bit를 보고 Data를 Update한다

//			DWORD dwtime = GetTickCount();

//			SFT_MD_SYSTEM_DATA * pMdConfig = m_lpModule[nModuleIndex].GetModuleSystem();
			int nSize = SizeofNetChData();
 			if(lpHeader->nLength >= nSize*m_lpModule[nModuleIndex].GetChannelCount() && lpReadBuff != NULL)
			{
				ProcessingMonitoringData(nModuleIndex, lpReadBuff, lpHeader->nLength, hMsgWnd); 
			}
			else
			{
				nResponseCode = SFT_NACK;
			}	
			
//			TRACE("Monitoring parsing elapsed %dmsec\n", GetTickCount()-dwtime);
			break;	//Send Ack
		}

	// 전송한 Command에 대한 Acknowledge //
	case SFT_CMD_RESPONSE :
		{
			if((lpHeader->nLength == sizeof(SFT_RESPONSE)) && lpReadBuff != NULL)
			{
				memcpy(&m_lpModule[nModuleIndex].m_CommandAck, lpReadBuff, sizeof(SFT_RESPONSE));
			}
			else
			{
				m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_SIZE_MISMATCH;
			}

			m_lpModule[nModuleIndex].SetReadEvent();
			
			sprintf(msg, "Response From Module %d : Command 0x%x, Code %d", 
							m_lpModule[nModuleIndex].GetModuleID(), 
							m_lpModule[nModuleIndex].m_CommandAck.nCmd, 
							m_lpModule[nModuleIndex].m_CommandAck.nCode); 			
			WriteLog(msg);
//			TRACE("%s\n", msg);
		}
		return TRUE;

		//교정 완료 명령 수신 
	case SFT_CMD_CALI_RESULT:
		{
			// Send to Main Window
			if(lpHeader->nLength == sizeof(SFT_CALI_END_DATA) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					LPVOID lpData = new char[sizeof(SFT_CALI_END_DATA)];
					m_lpModule[nModuleIndex].SetBuffer(lpData);
					memcpy(lpData, lpReadBuff, sizeof(SFT_CALI_END_DATA));

					::PostMessage(hMsgWnd, SFTWM_CALI_RESULT_DATA, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
				}
			}
			else
			{
				nResponseCode = SFT_NACK;
			}

			break;	//send response	//SFTWM_CALI_RESULT_DATA
		}
	case SFT_CMD_HEARTBEAT:		//Network Check Command
		{
	//		TRACE("===>Receive heart beat!!! Serial %d\n", lpHeader->lCmdSerial);
			break;
		}

	case SFT_CMD_EMERGENCY:
		{
			if(lpHeader->nLength == sizeof(SFT_EMG_DATA) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					LPVOID lpData = new char[sizeof(SFT_EMG_DATA)];
					m_lpModule[nModuleIndex].SetBuffer(lpData);
					memcpy(lpData, lpReadBuff, sizeof(SFT_EMG_DATA));
					::PostMessage(hMsgWnd, SFTWM_MODULE_EMG, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
				}
			}
			return TRUE;
		}

	case SFT_CMD_MSEC_CH_DATA:
		{
			if(lpReadBuff != NULL)
			{
//				TRACE("Msec data received %d\n", nChannel);

#ifdef _DEBUG
				DWORD dwTime = GetTickCount();
#endif

				SFT_MSEC_CH_DATA_INFO *pInfo = (SFT_MSEC_CH_DATA_INFO *)lpReadBuff;
				if(lpHeader->nLength ==  sizeof(SFT_MSEC_CH_DATA_INFO) + pInfo->lDataCount * sizeof(SFT_MSEC_CH_DATA))
				{
					CChannel *pCh = m_lpModule[nModuleIndex].GetChannelData(nChannel-1);
					if(hMsgWnd && pCh)
					{
						pCh->SetStepStartData(pInfo, LPVOID((char *)lpReadBuff + sizeof(SFT_MSEC_CH_DATA_INFO)));
 						sprintf(msg, "MiliSecond Data M%02dC%02d : C %d, S%d, Size %d", 
 									 m_lpModule[nModuleIndex].GetModuleID(), nChannel, pInfo->lTotCycNo, pInfo->lStepNo, pInfo->lDataCount);
//						TRACE("%s\n", msg);
// 						WriteLog(msg);
#ifdef _DEBUG
						if(nChannel == 1)
						{
//							static int a = 1;
//							TRACE("1[%03d] ===> Data reveive\n", a++);
//							TRACE("▶▶Milisecond time data save elapsed time %dmsec\n", GetTickCount()-dwTime);
						}
#endif
						
						//Data 양이 너무 많아 Message 처리가 지연됨
						//::PostMessage(hMsgWnd, SFTWM_MSEC_CH_DATA, MAKELONG((WPARAM)m_lpModule[nModuleIndex].GetModuleID(), nChannel), (LPARAM)0);
						::SendMessage(hMsgWnd, SFTWM_MSEC_CH_DATA, MAKELONG((WPARAM)m_lpModule[nModuleIndex].GetModuleID(), nChannel), (LPARAM)0);

						//LPVOID lpData = new char[lpHeader->nLength];
						//m_lpModule[nModuleIndex].SetBuffer(lpData);			//App에서 삭제
						//memcpy(lpData, lpReadBuff, lpHeader->nLength);
						//::PostMessage(hMsgWnd, SFTWM_MSEC_CH_DATA, MAKELONG((WPARAM)m_lpModule[nModuleIndex].GetModuleID(), nChannel), (LPARAM)lpData);
					}
				}
				else
				{
					nResponseCode = SFT_NACK;
				}
			}
			else
			{
				nResponseCode = SFT_NACK;
			}
		}	
		
		break;
		
	// Hardware Error Code						//
//	case CMD_HARDWARE_ERROR :
//		{
////			SFT_RESPONSE *lpRespnse = (SFT_RESPONSE *)lpReadBuff;
////			PostMessage(m_hMsgHwnd, EP_WM_SEND_EMERGENCY_CODE, m_nModuleID, lpRespnse->nCode);
////			TRACE("\t\t\t▷ Module %d H/W ERROR Receive Size : %d\n", m_nModuleID, m_sRxBuffer.dwDataSize);
//			return TRUE;
//		}
//
//	// Calibration 실시에 따른 결과 데이터		//

	//20071106
	case SFT_CMD_SET_MEASURE_DATA:
		{
			TRACE("SFT_CMD_SET_MEASURE_DATA START\r\n");
			if(lpHeader->nLength == sizeof(SFT_MESAURE_DATA_PACKET) && lpReadBuff!= NULL)
			{
				SFT_MESAURE_DATA_PACKET packet;
				memcpy(&packet, lpReadBuff, sizeof(SFT_MESAURE_DATA_PACKET));
			}
			
			return TRUE;
		}

	case SFT_CMD_CALI_READY_DATA:	//교정명령에 대한 완료 보고 
	case SFT_CMD_CALI_CHECK_RESULT:	//현재 정밀도 검사 수신 
	default: 
		{
			//	CCriticalSection m_RxBuffCritic;		//Rx Buffer Critical Section
			if(lpHeader->nLength > 0 && lpReadBuff != NULL)		//Body Data Length Check
			{
				wrSize = lpHeader->nLength;
				if(wrSize <= _SFT_RX_BUF_LEN)			//Rx Buffer Size Check
				{
					memcpy((char *)m_lpModule[nModuleIndex].m_szRxBuffer, lpReadBuff, wrSize);
					m_lpModule[nModuleIndex].m_nRxLength = wrSize;
					m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_ACK;
//					TRACE("Response Data 0x%08x ,%d\n", lpHeader->nCommand, m_stModule[nModuleIndex].nRxLength);
				}
				else									//Size Error
				{
					m_lpModule[nModuleIndex].m_nRxLength = 0;
					m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_RX_BUFF_OVER_FLOW;
//					sprintf(msg, "Module %d Group %d Received Data Length is too Long %d/%d", 
//								  m_stModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, wrSize, _EP_RX_BUF_LEN);
//					WriteLog(msg);
				}
				m_lpModule[nModuleIndex].SetReadEvent();
			}
		}
		return TRUE;
	}
	

	//응답용 Message 
//	m_lpModule[nModuleIndex].SendResponse(lpHeader, nResponseCode);
	
	SFT_PACKET_RESPONSE	response;
	ZeroMemory(&response, sizeof(SFT_PACKET_RESPONSE));
	response.msgHeader.nCommand = SFT_CMD_RESPONSE;			// Packet Command 종류	(예 : 이 패킷은 컨트롤 패킷이다)
	response.msgHeader.lCmdSerial = lpHeader->lCmdSerial;			// Command Serial Number..(Command ID)
//	response.msgHeader.wCmdType;	
//	response.msgHeader.wNum;				// 이 Command에 해당 사항이 있는 채널의 수
//	response.msgHeader.lChSelFlag[2];		// 이 시험조건이 전송될 Channel (Bit 당 Channel)
	response.msgHeader.nLength = sizeof(SFT_RESPONSE);			// Size of Body
	response.msgBody.nCmd = lpHeader->nCommand;		//default Reponse Code is Ack
	response.msgBody.nCode = nResponseCode;
	
	Sleep(5);	//==> Process Handle을 넘기기 위한 잠깐의 Delay 반드시 필요
	wrSize = pSock->Write(&response, sizeof(SFT_PACKET_RESPONSE));

	if(wrSize != sizeof(SFT_PACKET_RESPONSE))
	{
		sprintf(msg, "▶▶▶▶▶▶▶▶▶▶▶▶ Module %d :: Commnad 0x%x, ACK send fail. WSAGetLastError %d\n", 
						m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand,  
						WSAGetLastError()); 			
		WriteLog(msg);
		
		TRACE("\n▶▶▶▶▶▶▶▶▶▶▶▶\n");
		TRACE("Module %d :: Commnad 0x%x, ACK send fail(%d). WSAGetLastError %d\n", 
				m_lpModule[nModuleIndex].GetModuleID(), 
				lpHeader->nCommand, 
				wrSize,
				WSAGetLastError());
		TRACE("▶▶▶▶▶▶▶▶▶▶▶▶\n\n");
	} 

#ifdef _DEBUG	
	if(nResponseCode != SFT_ACK)
	{
		TRACE("Module %d :: Nack send. Command 0x0xd.\n", m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand);
	}

	if(wrSize != sizeof(SFT_PACKET_RESPONSE))
	{
//		TRACE("\n▶▶▶▶▶▶▶▶▶▶▶▶\n");
//		TRACE("Module %d :: Ack send fail. %d\n", m_lpModule[nModuleIndex].GetModuleID(), wrSize);
//		TRACE("▶▶▶▶▶▶▶▶▶▶▶▶\n\n");
		AfxMessageBox("Ack send fail...");
	}
	else
	{
//		if(lpHeader->nCommand != SFT_CMD_CH_DATA && lpHeader->nCommand != SFT_CMD_HEARTBEAT)
		if(lpHeader->nCommand == SFT_CMD_HEARTBEAT)
		{
			static DWORD dTime = GetTickCount();
			SYSTEMTIME systime;
			::GetLocalTime(&systime);
			long lElTime = GetTickCount()-dTime;
			if(lElTime > 5000)
			{
				TRACE("\n★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★\n");
				TRACE("<===%02d:%02d:%02d.%03d(%03d) :: Module %d send response code %d.\n", systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds, lElTime, m_lpModule[nModuleIndex].GetModuleID(), nResponseCode);
				TRACE("★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★\n\n");
			}
			else
			{
	//			TRACE("<===%02d:%02d:%02d.%03d(%03d) :: Module %d Commad serial %d\n", 
	//					systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds, lElTime, 
	//					m_lpModule[nModuleIndex].GetModuleID(), response.msgHeader.lCmdSerial);
			}
			dTime = GetTickCount();
		}
	}
#endif

	return TRUE;
}

void ProcessingMonitoringData(UINT nModuleIndex, void* pReceivedBuffer, int /*nSize*/, HWND hMsgWnd)
{
	SFT_CH_DATA sChannelResultData;
	CChannel *pChData;

//	char szBuff[512];

	static unsigned char szDataMode[SFT_MAX_CH_PER_MD] = {0,};
	memset(szDataMode, 0, sizeof(szDataMode));

//	SFT_MD_SYSTEM_DATA * pMdConfig = m_lpModule[nModuleIndex].GetModuleSystem();
	int nSize = SizeofNetChData();

	//Version check
	int nMappedCh = 0;
	for( int nChNo = 0; nChNo < m_lpModule[nModuleIndex].GetChannelCount(); nChNo++ )
	{
		memcpy(&sChannelResultData, (BYTE *)pReceivedBuffer+ nChNo * nSize, SizeofNetChData());
		
		//1 base channel no check
		if ( sChannelResultData.chNo <= 0 || sChannelResultData.chNo > m_lpModule[nModuleIndex].GetChannelCount())
		{
			break;
		}

		//Channel Mapping 실시
		switch(g_nChMappingType)
		{
		case SFT_CH_INDEX_MEMBER	:	nMappedCh = sChannelResultData.chNo-1;		break;	//Use wChIndex( the member variable of SFT_CH_DATA)
		case SFT_CH_INDEX_MAPPING	:	nMappedCh = g_anChannelMap[nChNo];			break;	//Read Channel Mapping Table from "mapping.dat" File
		default:						nMappedCh = nChNo;							break;	// SFT_CH_INDEX_SEQUENCE	//Network incomming Squence 
		}

		//채널 참조 포인트 구함
		pChData = m_lpModule[nModuleIndex].GetChannelData(nMappedCh);
		if(pChData != NULL)
		{
			//현재 채널 data를 갱신 한다.
			pChData->SetChannelData(sChannelResultData);

// 			sprintf(szBuff, "Ch %d : watt %d\n", sChannelResultData.chNo, sChannelResultData.lWatt);
// 			WriteLog(szBuff);
			
			//저장해야 할 Data일 경우 채널의 저장 stack에 저장한다.
			if(sChannelResultData.chDataSelect != SFT_SAVE_REPORT)
			{
				pChData->PushSaveData(sChannelResultData);
			}
		}
		else
		{
			TRACE("Channel Monitoring Data의 Channel No Error\n");
		}

		if(nMappedCh >= 0 && nMappedCh <SFT_MAX_CH_PER_MD)
		{
			szDataMode[nMappedCh] = sChannelResultData.chDataSelect;
//			TRACE("Ch %d : Data Select Type %d\n", nMappedCh+1, szDataMode[nMappedCh]);
		}
	}
	
	// Send Message to Main Window
	::PostMessage(hMsgWnd,   SFTWM_TEST_RESULT, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)szDataMode);
}


//Moduel ID duplication, Version, registration Check
int CheckModuleIDValidate(LPSFT_MD_SYSTEM_DATA pSysData)
{
	int nModuleIndex;

	//Version Check
	if(pSysData->nProtocolVersion > _SFT_PROTOCOL_VERSION)			return -1;				//Version Mismatch
	
	//DataBase에 등록여부 확인 
	if((nModuleIndex = SFTGetModuleIndex(pSysData->nModuleID)) < 0)	return -2;				//Not registed in DB 
	
	//변수 확인 
	if(pSysData->wChannelPerBoard < 1  || pSysData->wInstalledBoard >SFT_MAX_CH_PER_BD)		return -3;
	if(pSysData->nInstalledChCount < 1  || pSysData->nInstalledChCount > SFT_MAX_CH_PER_MD)	return -4;

	//이미 연결된 상태인지 검사 
	if(SFTGetModuleState(pSysData->nModuleID) != PS_STATE_LINE_OFF)						return -5;		//ID duplication
/*
	if(pSysData->nTotalGroupNo > SFT_MAX_BD_PER_MD || pSysData->nTotalGroupNo <= 0)		return -3;
	if(pSysData->wInstalledBoard < pSysData->nTotalGroupNo 
		|| pSysData->wInstalledBoard > SFT_MAX_BD_PER_MD
		|| pSysData->wInstalledBoard <= 0)							return -4;
	
	int nTotalCh = 0;

	for(INDEX i = 0; i<pSysData->nTotalGroupNo; i++)
	{
		nTotalCh += pSysData->awChInGroup[i];
	}
	if(pSysData->wChannelPerBoard*pSysData->wInstalledBoard != nTotalCh || nTotalCh <=0 || nTotalCh >SFT_MAX_CH_PER_MD)	return -6;
*/


	return nModuleIndex;
}

//Module이 접속 시도시 접속 절차에 의해 받아 들이고 Module 정보 확인
// 1. Host  => Module : Module Information Request명령 전송
// 2. Host <=  Module : Module 정보 전송
// 3. Host  => Module : Module Setting 정보 전송
// 4. Host <=  Module : Ack/Nack
int	ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hEvent)
{
	int nRtn;
	int nModuleIndex = 0;
	char msg[128];

	SFT_MSG_HEADER msgHeader;

	WSANETWORKEVENTS wsEvents;
	SFT_MD_SYSTEM_DATA sysData;
	ZeroMemory(&sysData, sizeof(sysData));
//	SFT_SYSTEM_PARAM *pParam = NULL;
	SFT_RESPONSE	response;
//	SFT_MD_SET_DATA mdSetData;

	//------------Send Version Information Request Command--------------------//
	//Serial No 0 Command is => SFT_CMD_INFO_REQUEST
	ZeroMemory(&msgHeader, sizeof(msgHeader));
	msgHeader.nCommand = SFT_CMD_INFO_REQUEST;
	nRtn = pSocket->Write(&msgHeader, SizeofHeader());			//Modlule Version Information Request
	if(nRtn != SizeofHeader())
	{ 
		sprintf(msg, "Module Socket Error %d\n", WSAGetLastError());
		nRtn = -1;
		goto CONNECTION_FAIL;
	}

	//------------wait Version Information Response Header--------------------//
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)													// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{
			//-----------------Read Module Information and Check validate ---------------------//
			nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			if(nRtn != SizeofHeader())	//Header Read Fail;
			{
				sprintf(msg, "Module information cmd header read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -2;
				goto CONNECTION_FAIL;
			}

			//Command validate Checking
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "Module information data cmd header fail.(%d)\n", nRtn);
				nRtn = -3;
				goto CONNECTION_FAIL;
			}

			Sleep(200);
			nRtn = pSocket->Read(&sysData , sizeof(sysData));				//Module Version Info Read
			if(nRtn != msgHeader.nLength)	//MsgBody Read Fail
			{
				sprintf(msg, "Module iInformation data read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -4;
				goto CONNECTION_FAIL;
			}

			UINT	nPortNo;
			char	szIP[36];	
			pSocket->GetPeerName(szIP, sizeof(szIP), nPortNo);	//Get Client Side Ip Address and Port Number
			sprintf(msg, "IP %s에서 Module %d로 접속이 시도 되었습니다.(Protocol Version: 0x%x)", 
						  szIP, sysData.nModuleID, sysData.nProtocolVersion);
			WriteLog(msg);
			
			nModuleIndex = CheckModuleIDValidate(&sysData);		//Accepted Module Number and Version Check
			if(nModuleIndex< 0)									//Accepted Module information error			
			{
				sprintf(msg, "Accepted module does not support. Code(%d).\n", nModuleIndex);
				WriteLog(msg);
				sprintf(msg, "제공 하지 않는 Module이 접속을 시도 합니다. code %d", nModuleIndex);
				nRtn = -5;
				goto CONNECTION_FAIL;
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -8;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong event detected0 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -9;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{
		sprintf(msg, "Module Disconnected :: Shutdown befor read module information.\n");
		nRtn = -10;
		goto CONNECTION_FAIL;
	}

	//------------Send Module Set Command --------------------//
	m_lpModule[nModuleIndex].MakeCmdSerial(&msgHeader);		//serial No 1 Command
	msgHeader.nCommand = SFT_CMD_MD_SET_DATA;
	msgHeader.nLength = sizeof(SFT_MD_SET_DATA);

	SFT_MD_SET_DATA mdSetData;
	ZeroMemory(&mdSetData, sizeof(mdSetData));
	//DB에 설정된 모듈의 설정값을 모듈에 전송한다.
//	pParam = m_lpModule[nModuleIndex].GetModuleParam();
//	pParam = SFTGetSysParam(sysData.nModuleID);
//	mdSetData.nAutoReportInterval = 0;
//	mdSetData.bConnectionReTry = nModuleIndex < 0 ? 0x00 : 0x01;
//	mdSetData.bAutoProcess =(BYTE)SFTGetAutoProcess(sysData.nModuleID);
//	mdSetData.bUseTemp = (BYTE)pParam->bUseTempLimit;
//	mdSetData.nMaxTemp = pParam->lMaxTemp;
//	mdSetData.bUseGas = (BYTE)pParam->bUseGasLimit;
//	mdSetData.nMaxGas = pParam->lMaxGas;
//	mdSetData.bTrayIDReadType = SFT_TRAY_REC_NVRAM;	//Tray Recognition Type		0: NVRAM	1: BarCode Reader

	nRtn = pSocket->Write(&msgHeader, SizeofHeader());		
	nRtn += pSocket->Write(&mdSetData, sizeof(SFT_MD_SET_DATA));
	if(nRtn != msgHeader.nLength+SizeofHeader())
	{
		sprintf(msg, "Module %d :: Write module set data fail. (Code %d)\n", sysData.nModuleID, WSAGetLastError());
		nRtn = -12;
		goto CONNECTION_FAIL;
	}

	Sleep(500);
	//------------Waiting for Response of Module Set Command --------------------//
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT);		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)						// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if(nRtn == SOCKET_ERROR)
		{
			sprintf(msg, "Module %d :: Socket Error%d\n", sysData.nModuleID, WSAGetLastError());
			nRtn = -7;
			goto CONNECTION_FAIL;
		}
		TRACE("WSAEnumNetworkEvents() returns %d\n", nRtn);

		if (wsEvents.lNetworkEvents & FD_READ)					//Read Event
		{
			//-----------------Read Module Information and Check validate ---------------------//
			nRtn = pSocket->Read(&msgHeader, SizeofHeader());			//Message Head Read
			if(nRtn != SizeofHeader())
			{
				sprintf(msg, "Module %d :: Module set command response read fail.(%d) (Read=%d/Request=%d)\n", sysData.nModuleID, WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -8;
				goto CONNECTION_FAIL;
			}
			
			//Command validate Checking
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "Module %d :: Module set command response header fail.(%d)\n", sysData.nModuleID, nRtn);
				nRtn = -8;
				goto CONNECTION_FAIL;
			}

			Sleep(200);
			nRtn = pSocket->Read(&response, sizeof(response));			//Message Body Read
			if(nRtn != msgHeader.nLength)
			{
				sprintf(msg, "Module %d :: Receive module set command response fail(%d/%d). (Code %d)\n", sysData.nModuleID, nRtn, msgHeader.nLength, WSAGetLastError());
				nRtn = -10;
				goto CONNECTION_FAIL;
			}


			if(msgHeader.nCommand == SFT_CMD_RESPONSE && response.nCode == SFT_ACK)
			{ 
				if(m_lpModule[nModuleIndex].SetModuleSystem(&sysData) == FALSE)
				{
					sprintf(msg, "Module %d :: Module setting error.\n", sysData.nModuleID);
					return -11;
				}
				return nModuleIndex;
			}
			else
			{
				sprintf(msg, "Module %d :: Module set command receive NACK(%d).\n", sysData.nModuleID, response.nCode);
				nRtn = -12;
				goto CONNECTION_FAIL;
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected1 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -13;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module %d Disconnected :: Wrong event detected1 (Code %d)\n", sysData.nModuleID, wsEvents.lNetworkEvents);
			nRtn = -14;
			goto CONNECTION_FAIL;
		}
	}
	else	//wrong event or Server Close Event
	{
		sprintf(msg, "Server Disconnected :: Wrong event detected (Code %d)\n", wsEvents.lNetworkEvents);
		nRtn = -15;
		goto CONNECTION_FAIL;
	}

CONNECTION_FAIL:
	WriteLog(msg);
	TRACE(msg);
	return nRtn;
}

int CWizRawSocketListener::SetListenPort(int nPort)
{
	m_nPort = nPort;
	return TRUE;
}

/*
void CClientThread::ProcessingCalResultData(BYTE* pReceivedBuffer, 
											int nSize, 
											ULONG ulReceivedCmdSerial)
{
	// 수신한 Calibration Result Packet에 대한 Acknowledge를 전송함
	S_CMD_ACK_HEADER cmdHeader;
	ZeroMemory(&cmdHeader, sizeof(S_CMD_ACK_HEADER));

	cmdHeader.chCmd					= PACKET_ACK_RESULT;
	cmdHeader.lDataSize				= sizeof(int) * 2;
	cmdHeader.nResultCode			= ACK_CODE_ACKNOWLEDGE ;
	cmdHeader.lCmdSendSerial		= m_AckID.SetID();
	cmdHeader.lCmdResponseSerial	= ulReceivedCmdSerial;

	int nSendSize = m_pSocket->Send(&cmdHeader, sizeof(S_CMD_ACK_HEADER));
	if( nSendSize < sizeof(S_CMD_ACK_HEADER) )
	{
		CString strLogMsg;
		strLogMsg.Format("Module %d::Send Calibration Result Data Ack Error", m_nModuleID);
		PrintLog(strLogMsg);
	}

	// 새로운 버퍼를 생성함. (추후 Main Window에서 꼭 삭제해야 함)
	BYTE *pTempBuffer = new BYTE [nSize];
	memcpy(pTempBuffer, pReceivedBuffer, nSize);

	// Send to Main Window
	::PostMessage(m_hMsgHwnd, 
				  EP_WM_SET_CALRESULT_DATA, 
				  (WPARAM)pTempBuffer, 
				  (LPARAM)m_nModuleID);

	TRACE("\t\t\t\t▷ Module %d::Send Acknowledge (S/N:%d)\n", m_nModuleID, cmdHeader.lCmdSendSerial);
}

*/
