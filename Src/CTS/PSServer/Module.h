// Module.h: interface for the CModule class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODULE_H__500F2D05_C0A1_4EF0_858D_46B988FD42F3__INCLUDED_)
#define AFX_MODULE_H__500F2D05_C0A1_4EF0_858D_46B988FD42F3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Channel.h"
#include "MinMaxData.h"

class CModule  
{
public:
	SYSTEMTIME * GetConnectedTime();
	void SetBuffer(LPVOID lpBuff);
	LPVOID GetBuffer();
	BOOL	SendResponse(LPSFT_MSG_HEADER lpReceivedCmd, int nCode = SFT_ACK);
	BOOL	GetHWChCount();
	int		GetChannelCount();
	void	SetStateData(const SFT_MD_STATE_DATA &stateData);
	void	RestCmdSequenceNo();
	BOOL	MakeCmdSerial(SFT_MSG_HEADER *pMsgHeader);
	LPVOID	ReadData(int nSize);
	BOOL	SendCommand(UINT nCmd, LPSFT_CH_SEL_DATA pChData = NULL, LPVOID lpData = NULL, UINT nSize = 0);
	CChannel *GetChannelData(int nChIndex);		
	WORD	GetChannelState(int nChIndex);
	HANDLE	GetWriteEventHandle();
	int		ReadAck();


	INT		GetModuleID()		{	return m_nModuleID;				}
	char *	GetModuleIP()		{	return m_szIpAddress;			}

	//state treat
	void	SetState(WORD state);
	WORD	GetState()			{	return m_state;					}

	//Event treat
	void	SetReadEvent()		{	::SetEvent(m_hReadEvent);		}
	void	ResetReadEvent()	{ 	m_nRxLength = 0;	::ResetEvent(m_hReadEvent);		}
	void	SetWriteEvent()		{	::SetEvent(m_hWriteEvent);		}
	void	ResetWriteEvent()	{	m_nTxLength = 0;	::ResetEvent(m_hWriteEvent);	}

	void	SetSyncWriteEvent()		{	::SetEvent(m_hSyncWriteEvent);		}
	void	ResetSyncWriteEvent()	{ 	m_nTxLength = 0;	::ResetEvent(m_hSyncWriteEvent);		}


	//Module set Data treat
	SFT_MD_SYSTEM_DATA	m_sysData;			//Module System Data (Module에서 올라오는 설정정보)
	BOOL SetModuleSystem(SFT_MD_SYSTEM_DATA *lpSysData);
	SFT_MD_SYSTEM_DATA *GetModuleSystem();

	//Module Config data treat
//	SFT_SYSTEM_PARAM	m_sysParam;			//System Parameter (Module로 하달하는 정보)
	BOOL SetModuleParam(int nModuleID, SFT_SYSTEM_PARAM *lpSysParam);
	SFT_SYSTEM_PARAM GetModuleParam();
	
	//construction,distruction
	CModule();
	virtual ~CModule();

	//Tx Buff
	char			m_szTxBuffer[_SFT_TX_BUF_LEN];		//Socket Write Buffer
	int				m_nTxLength;						//Tx Data Length

	//Rx Buff
	char			m_szRxBuffer[_SFT_RX_BUF_LEN];		//Socket Read Buffer	
	int				m_nRxLength;						//Rx Data Length

	//Command response
	SFT_RESPONSE	m_CommandAck;					//Response of Command

	//Ip Address
	char	m_szIpAddress[32];
	
protected:
	int m_nChCount;
	SYSTEMTIME m_timeConnect;

	LPVOID	m_lpBuff;

	//Read/Write Event Handle
	HANDLE			m_hWriteEvent;						//Module별 Write Event	
	HANDLE			m_hReadEvent;						//Module별 Read Event
	HANDLE			m_hSyncWriteEvent;					//64K 이상 데이터를 쓸때 동기화하는 Event

	char m_szConnectedTime[64];							//모듈과 통신이 연결된 시각
	void SetChannelCount(int nCount);

	UINT m_nCmdSequence;
	UINT m_nModuleID;						//ModuleID

	CMinMaxData		m_TempSensor;			//온도 기록용
	CMinMaxData		m_GasSensor;			//Gas Sensor 기록용


	WORD	m_state;						//Module State
	BYTE	m_jigState;						//Jig State 
	BYTE	m_trayState;					//Tray State
	BYTE	m_doorState;					//Door State
	BYTE	m_failCode;						//Module Fail Code

	//Channel Data
	CChannel	*m_pChannel;							
};

#endif // !defined(AFX_MODULE_H__500F2D05_C0A1_4EF0_858D_46B988FD42F3__INCLUDED_)
