/////////////////////////////////////////////////////////////////////
// Class Creator Version 2.0.000 Copyrigth (C) Poul A. Costinsky 1994
///////////////////////////////////////////////////////////////////
// Implementation File RawSocketServerWorker.cpp
// class CWizRawSocketServerWorker
//
// 16/07/1996 17:53                             Author: Poul
///////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RawSocketServerWorker.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

extern void SetUserTimer(int nIDEvent, int nElapsed);
extern void KillUserTimer(int nIDEvent);
extern LPDC_DATA_CLIENT GetClientObject(int nSystemID);
int nHeartBeatInterval = 2000;

extern BOOL WriteLog(char *szLog);
BOOL ParsingCommand(CWizReadWriteSocket *pSock, LPVOID lpMsgHeader, LPVOID lpReadBuff,  DC_DATA_CLIENT *lpParent, HWND hMsgWnd);
BOOL SendResponse(CWizReadWriteSocket *pSock, LPVOID lpMsgHeader, int nCode);

///////////////////////////////////////////////////////////////////
//*****************************************************************
inline void ThrowIfNull(void* p)
{
	if (p == NULL)	AfxThrowMemoryException();
}

///////////////////////////////////////////////////////////////////
// class CWizRawSocketListener
///////////////////////////////////////////////////////////////////
//**********************************************************//
// Default Constructor										//
CWizRawSocketListener::CWizRawSocketListener(int nPort)     //
	: m_pListenSocket (NULL),								//
	  m_nPort         (nPort)								//
{															//
//	m_pDoc = NULL;	
	m_hMessageWnd = NULL;									//
}															//
//**********************************************************//
// Destructor												//
CWizRawSocketListener::~CWizRawSocketListener()				//
{															//
	if (m_pListenSocket != NULL)							//
	{														//
	//	TRACE("You Must call EPCloseFormServer() before your application is closed\n");
		ASSERT(0);											//
		delete m_pListenSocket;								//
	}														//
}															//
//**********************************************************//

void CWizRawSocketListener::SetMsgWnd(HWND hMessageWnd)
{
	m_hMessageWnd = hMessageWnd;
}

// Method called from dispath thread.
void CWizRawSocketListener::Prepare ()
{
	// Create listening socket
	if (m_pListenSocket != NULL)
	{
		ASSERT(0);
		delete m_pListenSocket;
	}
	m_pListenSocket = new CWizSyncSocket(m_nPort);
	ThrowIfNull (m_pListenSocket);

#ifdef _DEBUG
	TCHAR buff[100];
	unsigned int nP;
	VERIFY(m_pListenSocket->GetHostName (buff,100,nP));
	TRACE(_T("Data Server Listening at %s:%d\n"), buff, nP);
#endif
}

//*****************************************************************
// Method called from dispath thread.
void CWizRawSocketListener::CleanUp()
{
	// close and destroy listening socket
	delete m_pListenSocket;
	m_pListenSocket = NULL;
}

//*****************************************************************
// Method called from dispath thread.
BOOL CWizRawSocketListener::WaitForData (HANDLE hShutDownEvent)
{
	HANDLE *lpHandles;
	lpHandles = new HANDLE[2];

	WSAEVENT myObjectEvent = WSACreateEvent();
	lpHandles[0]= myObjectEvent;
	lpHandles[1]= hShutDownEvent;

	::WSAEventSelect( m_pListenSocket->H(), myObjectEvent, FD_ACCEPT);
	
	DWORD rtn;
	SOCKET h;

	while (1)
	{
		rtn = ::WaitForMultipleObjects(2, lpHandles, FALSE,   INFINITE);
		if( (rtn - WAIT_OBJECT_0) == 0 )
			 h = m_pListenSocket->Accept();
		else 
		{
			::WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return FALSE;
		}

		// Get accepted socket.	 If it's connected client, go to serve it.
		if (h != INVALID_SOCKET)
		{
//			WriteLog("A socket accepted");
			m_hAcceptedSocket = h;
			::WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return TRUE;
		}
	} // while 1
	return TRUE;
}

//*****************************************************************
// Method called from dispath thread.
// return TRUE : Wait clinet connection
// return FALSE: 
//****************************************************************
BOOL CWizRawSocketListener::TreatData   (HANDLE hShutDownEvent, HANDLE hDataTakenEvent)
{
	char	msg[128];								//Display Message Buffer
	UINT	nPortNo = 0;
	int		msgReadState  = _MSG_ST_CLEAR;			//Read Done Flag
	int		reqSize = 0, offset = 0, nRtn =0;		//Data Point Indicator

	size_t msgHeadersize = SizeofHeader();			//Message Header Size
	LPVOID lpReadBuff = NULL;						//Body read Buffer
	HANDLE 	*lpHandles;								//Handle
	lpHandles = new HANDLE[3];
	
//////////////////////////////////////////////////////////////////////
	LPDC_DATA_CLIENT lpClientSystem;
	
	// Create client side socket to communicate with client.
	CWizReadWriteSocket clientSide(m_hAcceptedSocket);
	// Signal dispather to continue waiting.
	::SetEvent(hDataTakenEvent);

	WSAEVENT myObjectEvent = ::WSACreateEvent();		//Event
	lpHandles[0] = myObjectEvent;						//Read 
	lpHandles[1] = hShutDownEvent;						//Shut down

	::WSAEventSelect(clientSide.H(), myObjectEvent, FD_READ|FD_CLOSE);		//select Read and close event

	EP_SYSTEM_VER dataServer;
	Sleep(100);
	nRtn = ::WaitForMultipleObjects(2, lpHandles, FALSE, /*_EP_MSG_TIMEOUT*/INFINITE);		//Wait for Read or Write Event
	if(WAIT_OBJECT_0 != nRtn)
	{
		sprintf(msg, "Client Connection Time Out. %d", nRtn);
		delete[] lpHandles;
		return FALSE;
	}

	WSANETWORKEVENTS sEvents;
	EP_MSG_HEADER	 msgHeader;
	int nCode = EP_ACK;
	nRtn = clientSide.Read(&msgHeader, msgHeadersize);
	nRtn += clientSide.Read(&dataServer, sizeof(dataServer));
	if(nRtn != msgHeader.nLength || msgHeader.nCommand != DB_CMD_DATA_SERVER_VER)
	{
		sprintf(msg, "Client System Version Data Read Fail. %d/%d\n", nRtn, msgHeader.nLength);
		nCode = EP_SIZE_MISMATCH;
	}

////////////////////////////////////////////////////////////////
// 2003-02-26 Client List화

	char szIP[16];
	clientSide.GetPeerName(szIP, sizeof(szIP), nPortNo);	//Get Client Side Ip Address and Port Number
	TRACE("conncted from %s, port %d\n", szIP, nPortNo);


	lpClientSystem = GetClientObject(dataServer.nID);
	if(lpClientSystem == NULL)
	{
		sprintf(msg, "Client ID %d not found. %s에서 접속시도", dataServer.nID, szIP);
		nCode = EP_NACK;
	}
	else
	{
		if(lpClientSystem->bConnected)	
		{
			sprintf(msg, "중복된 Client ID %d 로 %s에서 접속 시도됨", dataServer.nID, szIP);
			nCode = EP_NACK;
		}
	}

	EP_RESPONSE_COMMAND	*lpResponse = new EP_RESPONSE_COMMAND;
	ZeroMemory(lpResponse, sizeof(EP_RESPONSE_COMMAND));
	lpResponse->msgBody.nCode = nCode;
	lpResponse->msgHeader.nID = EP_ID_DATA_SERVER;
	lpResponse->msgHeader.nCommand = EP_CMD_RESPONSE;
	lpResponse->msgHeader.nLength = msgHeadersize + sizeof(EP_RESPONSE);
	lpResponse->msgBody.nCmd = msgHeader.nCommand;
	nRtn = clientSide.Write(lpResponse, sizeof(EP_RESPONSE_COMMAND));
	delete lpResponse;

	//접속을 할수 없음 
	if(nCode != EP_ACK || nRtn != sizeof(EP_RESPONSE_COMMAND))
	{
		delete[] lpHandles;
		return FALSE;
	}

	lpClientSystem->nSystemID = dataServer.nID;
	lpClientSystem->bConnected = TRUE;
	lpClientSystem->sysVsersion = dataServer;
	lpHandles[2] = lpClientSystem->m_hWriteEvent;
	SetUserTimer(_TIMER_HEARTBEAT+lpClientSystem->nSystemID, nHeartBeatInterval);
	strcpy(lpClientSystem->szIpAddress, szIP);

	TRACE("System ID %d conncted from %s, port %d\n", lpClientSystem->nSystemID, lpClientSystem->szIpAddress, nPortNo);
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	if(m_hMessageWnd)
	{
		::PostMessage(m_hMessageWnd, DBWM_DB_SYS_CONNECTED, (WPARAM)lpClientSystem->nSystemID, (LPARAM)lpClientSystem->sysVsersion.nSystemType);
	}

	//-----------------start Socket Read/Write--------------------------------//
	while(1)
	{
		nRtn = ::WaitForMultipleObjects(3, lpHandles, FALSE, INFINITE);
		switch(nRtn)
		{
		case WAIT_OBJECT_0:		// Client socket event FD_READ
			if(SOCKET_ERROR == ::WSAEnumNetworkEvents(clientSide.H(), myObjectEvent, &sEvents))		//Get Client side Socket Event
			{ 
				sprintf(msg, "Socket Error. Code %d\n",WSAGetLastError());
				goto _CLOSE_MODULE;
			}
		
			//---------------Data Read Event ---------------------------//
			if (sEvents.lNetworkEvents & FD_READ)		//Read Event
			{
				//--------------------Message Head Read -----------------//
				if( msgReadState & (_MSG_ST_CLEAR | _MSG_ST_HEADER_REMAIN))				//Read Message Header
				{
					if(msgReadState & _MSG_ST_CLEAR)	reqSize = msgHeadersize;		
					offset = msgHeadersize - reqSize;
					nRtn = clientSide.Read((char *)&msgHeader+offset, reqSize);		//Read Message Header
					
					if (nRtn != reqSize) 
					{
						if(nRtn == SOCKET_ERROR)	//Socket Error
						{
							msgReadState = _MSG_ST_CLEAR;
							sprintf(msg, "Message Header Read Fail. Code %d\n", WSAGetLastError());
							WriteLog(msg);
							break;
						}
						else
						{
							msgReadState = _MSG_ST_HEADER_REMAIN;
//							TRACE("Module %d Message Header Data %d Byte Read %d Byte Remain\n", nModuleID, nRtn, reqSize);
						}
						reqSize -= nRtn;
					}
					else		//Read Done
					{
//						TRACE("Module %d Message Header Receive. G%d-C%d::Cmd 0x%x(%d)\n", nModuleID, lpMsgHeader->wGroupNum, lpMsgHeader->wChannelNum, lpMsgHeader->nCommand, lpMsgHeader->nLength);
						if(msgHeader.nLength <= msgHeadersize )	//Incorrect Message if reqSize <  msgHeadersize
						{
							msgReadState = _MSG_ST_CLEAR;
						}
						else
						{
							msgReadState = _MSG_ST_HEAD_READED;
						}
					}
				} 
				else	//--------------------Message Body Read -----------------////Read Body And Command Parsing	
				{
					if (msgReadState & _MSG_ST_HEAD_READED)
					{
						reqSize = msgHeader.nLength - msgHeadersize;		
						if(lpReadBuff)
						{
							delete[] lpReadBuff;
							lpReadBuff = NULL;
						}
						//if(reqSize > )

						lpReadBuff = new char[reqSize];
						ASSERT(lpReadBuff);
					}

					offset = (msgHeader.nLength - msgHeadersize) - reqSize;
					nRtn = clientSide.Read((char *)lpReadBuff+offset, reqSize);
					if (nRtn != reqSize) 
					{
						if(nRtn == SOCKET_ERROR)	//Socket Error
						{
							msgReadState = _MSG_ST_CLEAR;
							sprintf(msg, "Message Body Read Fail. Code %d\n", WSAGetLastError());
							WriteLog(msg);
							break;
						}
						else
						{
							msgReadState = _MSG_ST_REMAIN;
//							TRACE("Module %d Message Body Data %d Byte Read %d Byte Remain\n", nModuleID, nRtn, reqSize);
						}
						reqSize -= nRtn;
					}
					else		//Read Done
					{
						msgReadState = _MSG_ST_CLEAR;
					}
					
				}
				
				///---------------Parsing Command---------------------///
				if(	msgReadState & _MSG_ST_CLEAR)
				{
	 				ParsingCommand(&clientSide,  &msgHeader, lpReadBuff, lpClientSystem, m_hMessageWnd);	//Parsing Recevied Command 
/*					if(lpMsgHeader->nCommand == EP_CMD_AUTO_GP_STEP_END_DATA && nEndDataSize == TRUE)
					{
						nEndDataSize = lpMsgHeader->nLength-SizeofHeader();
						if(lpEndDataBuff != NULL)
						{
							delete[] lpEndDataBuff;
							lpEndDataBuff = NULL;
						}	
						lpEndDataBuff = new char[nEndDataSize];
						ASSERT(lpEndDataBuff);
							
						memcpy(lpEndDataBuff, lpReadBuff, nEndDataSize);
						if(m_hMessageWnd)
							::PostMessage(m_hMessageWnd, EPWM_STEP_ENDDATA_RECEIVE, 
											(WPARAM)MAKELONG(lpMsgHeader->wGroupNum-1, m_stModule[nModuleIndex].sysParam.nModuleID), 
											(LPARAM)lpEndDataBuff
								 );			//Send to Parent Wnd to Step End Data Received
						//TRACE("Module %d::Step Result Data Received\n", m_stModule[nModuleIndex].sysParam.nModuleID);
					}
*/				}
			}
			else if(sEvents.lNetworkEvents == FD_CLOSE)		//Client Disconnected
			{
				sprintf(msg, "Client ID : %d Disconnect event detected. Code %d.\n",
					lpClientSystem->nSystemID, sEvents.lNetworkEvents);
				goto _CLOSE_MODULE;
			}
			else //else of if (sEvents.lNetworkEvents & FD_READ) is Not Read Event
			{
				if (sEvents.lNetworkEvents != 0) 
				{
					sprintf(msg, "Client UnKnown event detected. Code %d\n", sEvents.lNetworkEvents);
					goto _CLOSE_MODULE;
				}
			}
			//ResetEvent(myObjectEvent);
			//WSAEventSelect(clientSide.H(), myObjectEvent, FD_READ|FD_CLOSE);
		break;
		
		case WAIT_OBJECT_0 + 1: // Server side shutdown event
			sprintf(msg, "Data Server shutdown event event detected.\n");
			TRACE("%s\n", msg);
			
			clientSide.Close();

			//Server side는 이미 delete되었다.
			//2006/9/4 KBH
/*			clientSystem->bConnected = FALSE;
			if(m_hMessageWnd)
			{
				WORD wdConnectedNum = m_clientSystem.GetSize()-1;
				DWORD dwParam = MAKELONG((WORD)clientSystem->nSystemID, (WORD)wdConnectedNum);
				::PostMessage(m_hMessageWnd, EPWM_DB_SYS_DISCONNECTED, 
					clientSystem->sysVsersion.nID , dwParam);
			}
*/

			if(lpReadBuff)
			{
				delete [] lpReadBuff;			lpReadBuff = NULL;
			}
			delete [] lpHandles;
			
			return FALSE;			
		
		case WAIT_OBJECT_0 + 2:	// client side write event
			nRtn = clientSide.Write(lpClientSystem->szTxBuffer, lpClientSystem->nTxLength);
			if(nRtn != lpClientSystem->nTxLength)
			{
				sprintf(msg, "Data Client Socket write Error, %d Byte Write - %d Byte remain\n", nRtn, lpClientSystem->nTxLength - nRtn);
			}
			lpClientSystem->nTxLength =0;
			::ResetEvent(lpClientSystem->m_hWriteEvent);
			break;
	
		case WAIT_TIMEOUT:
			sprintf(msg, "Client ID : %d Data Read Time Out. Disconnect Data Client.\n",
				lpClientSystem->nSystemID);
			goto _CLOSE_MODULE;

		default: //WAIT_FAILED ->>something wrong event
			sprintf(msg, "Error Code %d detected[%d]. Disconnect Data Client.\n", GetLastError(), nRtn);
			goto _CLOSE_MODULE;
		}
	}

//Module Conntection Colse Label
_CLOSE_MODULE:				//Release Memory and close client socket
	
	TRACE("%s\n", msg);
	WriteLog(msg);			//Write Log
	
	clientSide.Close();

	lpClientSystem->bConnected = FALSE;
	if(m_hMessageWnd)
	{
		::PostMessage(m_hMessageWnd, DBWM_DB_SYS_DISCONNECTED, (WPARAM)lpClientSystem->nSystemID, (LPARAM)lpClientSystem->sysVsersion.nSystemType);
	}

	if(lpReadBuff)
	{
		delete [] lpReadBuff;	lpReadBuff = NULL;
	}
	delete [] lpHandles;		lpHandles = NULL;
	return TRUE;
}

//Parsing Response Command 
BOOL ParsingCommand(CWizReadWriteSocket *pSock, LPVOID lpMsgHeader, LPVOID lpReadBuff, DC_DATA_CLIENT *lpParent, HWND hMsgWnd)
{
	ASSERT(pSock);
	ASSERT(lpMsgHeader);
	ASSERT(lpReadBuff);
	ASSERT(lpParent);

	INT		wrSize = 0;
	LPEP_MSG_HEADER lpHeader = (LPEP_MSG_HEADER)lpMsgHeader;
	BOOL nRtn = TRUE;

/*	if(( wrSize = CheckMsgHeader(nModuleIndex, lpHeader)) < 0)
	{
		sprintf(msg, "Module %d Message Header Error. Code %d", m_stModule[nModuleIndex].sysParam.nModuleID, wrSize);
		WriteLog(msg);
		return FALSE;
	}
*/
	switch(lpHeader->nCommand)
	{
	//version information
	case DB_CMD_DATA_SERVER_VER:
		if((lpHeader->nLength - SizeofHeader()) != sizeof(EP_SYSTEM_VER))		//Data Size Check
		{
			SendResponse(pSock, lpHeader, EP_SIZE_MISMATCH);
		}
		else
		{
			memcpy(&lpParent->sysVsersion, lpReadBuff, sizeof(EP_SYSTEM_VER));
/*			if(hMsgWnd)
			{
				::SendMessage(hMsgWnd,	EPWM_MODULE_CONNECTED, lpParent->nSystemID,  0);			//Send to Parent Wnd to Module State change
			}
*/
		}
		TRACE("Data Server Version Receive\n");
		break;

	//step 결과 항목별 전송된 명령
	case DB_CMD_TEST_RESULT:
		{
			wrSize = lpHeader->nLength - SizeofHeader();		//Data Size Check
		
			EP_RESULT_TO_DATABASE *pRltData = (EP_RESULT_TO_DATABASE *)lpReadBuff;
			if(wrSize != (sizeof(EP_RESULT_TO_DATABASE)+sizeof(EP_CH_RESULT)*pRltData->totalChCount))	
			{
				SendResponse(pSock, lpHeader, EP_SIZE_MISMATCH);
				TRACE("Test Result Data Size Error\n");
			}
			else
			{
				if(hMsgWnd)
				{
					SendResponse(pSock, lpHeader, EP_ACK);
					::SendMessage(hMsgWnd, DBWM_TEST_RESULT, (WPARAM)MAKELONG(lpParent->nSystemID, lpParent->sysVsersion.nSystemType), (LPARAM)lpReadBuff);
	//				::PostMessage(hMsgWnd, EPWM_TEST_RESULT, (WPARAM)lpParent->nSystemID, (LPARAM)lpReadBuff);
				}
			}
			TRACE("Test Result Data Received\n");
			break;
		}

	//작업 시작 명령
	case DB_CMD_PROCEDURE_DATA:
		{
			wrSize = lpHeader->nLength - SizeofHeader();		//Data Size Check
			if(wrSize != sizeof(EP_PROCEDURE_DATA))
			{
				SendResponse(pSock, lpHeader, EP_SIZE_MISMATCH);
				TRACE("Procedure Data Size Error\n");
			}
			else
			{
				if(hMsgWnd)
				{
					//::SendMessage(hMsgWnd, EPWM_PROCEDURE_DATA, (WPARAM)lpParent->nSystemID, (LPARAM)lpReadBuff);
					EP_PROCEDURE_DATA *lpBuff = new EP_PROCEDURE_DATA;
					memcpy(lpBuff, lpReadBuff, sizeof(EP_PROCEDURE_DATA));
					
					SendResponse(pSock, lpHeader, EP_ACK);
					::SendMessage(hMsgWnd, DBWM_PROCEDURE_DATA, (WPARAM)MAKELONG(lpParent->nSystemID, lpParent->sysVsersion.nSystemType), (LPARAM)lpBuff);
				}
			}
			break;
		}
		
	//명령에 대한 응답
	case DB_CMD_RESPONSE:
		{
			wrSize = lpHeader->nLength - SizeofHeader();
			if(wrSize == sizeof(EP_RESPONSE))
			{
				memcpy(&lpParent->CommandAck,  lpReadBuff, sizeof(EP_RESPONSE));
			}
			else
			{
				lpParent->CommandAck.nCode = EP_SIZE_MISMATCH;
			}
			::SetEvent(lpParent->m_hReadEvent);	

			return TRUE;
		}

	//DataBase 조작 명령
	case DB_CMD_DATA_MANAGE:
		{
			wrSize = lpHeader->nLength - SizeofHeader();		//Data Size Check
			if(wrSize != sizeof(EP_DATABASE_MANAGE_DATA))
			{
				SendResponse(pSock, lpHeader, EP_SIZE_MISMATCH);
				TRACE("DataBase management data size error(%d)\n", wrSize);
			}
			else
			{
				if(hMsgWnd)
				{
// 					//Send Response
// 					EP_RESPONSE_COMMAND response;
// 					memcpy(&response.msgHeader, lpHeader, SizeofHeader());
// 					response.msgHeader.nCommand = EP_CMD_RESPONSE;
// 					response.msgHeader.;
// 
// 					response.msgBody.nCode = result.nCode;
// 					response.msgBody.nCmd = lpHeader->nCommand;
// 
// 					LPEP_MSG_HEADER lpResponseHeader = NULL;
// 					LPVOID lpBuffer = NULL;
// 					int nBufferSize =0;
// 					nBufferSize = sizeof(EP_RESPONSE)+SizeofHeader();					//Response size
// 					lpBuffer = new char[nBufferSize];
// 					memcpy(lpBuffer, lpHeader, SizeofHeader());
// 					lpResponseHeader = (LPEP_MSG_HEADER)lpBuffer;
// 					lpResponseHeader->nLength = nBufferSize;
// 					lpResponseHeader->nCommand = EP_CMD_RESPONSE;
// 					memcpy((char *)lpBuffer + SizeofHeader(), &result, sizeof(result));
// 					
// 					wrSize = pSock->Write(lpBuffer, nBufferSize);		//Reply to Module
// 					if(wrSize != lpResponseHeader->nLength)
// 					{
// 						TRACE("e");
// 					}
// 					delete [] lpBuffer;
// 					TRACE("Send response %d\n", wrSize);
			
					SendResponse(pSock, lpHeader, EP_ACK);

					::SendMessage(hMsgWnd, DBWM_DB_MANAGE_DATA, (WPARAM)MAKELONG(lpParent->nSystemID, lpParent->sysVsersion.nSystemType), (LPARAM)lpReadBuff);
	//				::PostMessage(hMsgWnd, EPWM_PROCEDURE_DATA, (WPARAM)lpParent->nSystemID, (LPARAM)lpReadBuff);
				}
			}
			break;
		}

	//Step result가 통째로 전송되어온 경우 
	case DB_CMD_STEP_RESULT:
		{
			wrSize = lpHeader->nLength - SizeofHeader();		//Data Size Check
			if(wrSize < 0)
			{
				TRACE("Step result data size error(%d)\n", wrSize);
				SendResponse(pSock, lpHeader, EP_SIZE_MISMATCH);
			}
			else
			{
				if(hMsgWnd)
				{
					//Blocking 방지를 위해 Response를 먼저 보냄 
					SendResponse(pSock, lpHeader, EP_ACK);

					::SendMessage(hMsgWnd, DBWM_STEP_ENDDATA_RECEIVE, (WPARAM)MAKELONG(lpParent->nSystemID, lpParent->sysVsersion.nSystemType), (LPARAM)lpReadBuff);
					//::PostMessage(hMsgWnd, EPWM_PROCEDURE_DATA, (WPARAM)lpParent->nSystemID, (LPARAM)lpReadBuff);
				}
			}
			break;
		}

	default:
		SendResponse(pSock, lpHeader, EP_NACK);
		break;
	}

////////////////////////////Send Reaponse	/////////////////////////////////////
/*	LPEP_MSG_HEADER lpResponseHeader = NULL;
	LPVOID lpBuffer = NULL;
	int nBufferSize =0;
	switch(lpHeader->nCommand)
	{
	case EP_CMD_DATA_SERVER_VER:							//Send Response Command
	case EP_CMD_TEST_RESULT:
	case EP_CMD_PROCEDURE_DATA:
	case EP_CMD_DATA_MANAGE:
	case EP_CMD_STEP_RESULT:

		nBufferSize = sizeof(EP_RESPONSE)+SizeofHeader();					//Response size
		lpBuffer = new char[nBufferSize];
		memcpy(lpBuffer, lpHeader, SizeofHeader());
		lpResponseHeader = (LPEP_MSG_HEADER)lpBuffer;
		lpResponseHeader->nLength = nBufferSize;
		lpResponseHeader->nCommand = EP_CMD_RESPONSE;
		memcpy((char *)lpBuffer + SizeofHeader(), &result, sizeof(result));
		
		TRACE("Command Response Cmd 0x%08X, Rps 0x%08X Send\n", lpHeader->nCommand, result.nCode);

		break;

	default:
		TRACE("Invalied Command Receive\n");
		break;
	}
/////////////////////////////////////////////////////////////////////////////
	if(lpBuffer)					//delete reply Message Body		
	{
		wrSize = pSock->Write(lpBuffer, nBufferSize);		//Reply to Module

#ifdef _DEBUG
		if(wrSize != lpResponseHeader->nLength)
		{
			sprintf(msg, "Socket Write Fail %d/%d", result.nCode, lpResponseHeader->nLength);
			TRACE("%s\n", msg);
			AfxMessageBox(msg);
		}
#endif

		delete[] lpBuffer;
		lpBuffer = NULL;
	}
*/

	return nRtn;
}

BOOL SendResponse(CWizReadWriteSocket *pSock, LPVOID lpMsgHeader, int nCode)
{
	LPEP_MSG_HEADER lpHeader = (LPEP_MSG_HEADER)lpMsgHeader;

	EP_RESPONSE_COMMAND response;
	response.msgHeader.nLength = sizeof(EP_RESPONSE_COMMAND);
	response.msgHeader.nCommand = EP_CMD_RESPONSE;
	response.msgHeader.nID = lpHeader->nID;
	response.msgHeader.wChannelNum = lpHeader->wChannelNum;
	response.msgHeader.wGroupNum = lpHeader->wGroupNum;

	response.msgBody.nCmd = lpHeader->nCommand;
	response.msgBody.nCode = nCode;
	
	TRACE("Command response sending :: Cmd 0x%08X, code 0x%08X\n", lpHeader->nCommand, nCode);

	WORD wrSize = pSock->Write(&response, response.msgHeader.nLength);
	if(wrSize != response.msgHeader.nLength)
	{
#ifdef _DEBUG
		char msg[128];
		sprintf(msg, "Socket write size fail %d/%d", wrSize, response.msgHeader.nLength);
		TRACE("%s\n", msg);
		AfxMessageBox(msg);
#endif
		return FALSE;
	}
	return TRUE;
}
