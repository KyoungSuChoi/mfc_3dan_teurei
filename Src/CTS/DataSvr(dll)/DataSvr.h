// DataSvr.h : main header file for the DATASVR DLL
//

#if !defined(AFX_DATASVR_H__1F424155_5521_49F3_9227_9D5487934754__INCLUDED_)
#define AFX_DATASVR_H__1F424155_5521_49F3_9227_9D5487934754__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDataSvrApp
// See DataSvr.cpp for the implementation of this class
//
#include "DllTimer.h"
class CDataSvrApp : public CWinApp
{
public:
	void OnTimer(int nIDEvent);
	CDataSvrApp();
	CDllTimer m_Timer;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataSvrApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDataSvrApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATASVR_H__1F424155_5521_49F3_9227_9D5487934754__INCLUDED_)
