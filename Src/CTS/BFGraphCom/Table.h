/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Table.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CTable class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// Table.h: interface for the CTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CTable
//
//	Comment: 
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
#define RS_COL_VOLTAGE		"Voltage"
#define RS_COL_VOLTAGE1		"Voltage1"
#define RS_COL_VOLTAGE2		"Voltage2"
#define RS_COL_VOLTAGE3		"Voltage3"
#define RS_COL_VOLTAGE4		"Voltage4"
#define RS_COL_CURRENT		"Current"
#define RS_COL_CAPACITY		"Capacity"
#define RS_COL_TIME			"Time"
#define RS_COL_WATT			"Power"
#define RS_COL_WATT_HOUR	"WattHour"
#define RS_COL_TEMPERATURE	"Temperature"
#define RS_COL_TEMPERATURE1	"Temperature1"
#define RS_COL_TEMPERATURE2	"Temperature2"
#define RS_COL_PRESSURE		"Press"
#define RS_COL_AVG_VTG		"VoltageAverage"
#define RS_COL_AVG_CRT		"CurrentAverage"
#define RS_COL_STEP_NO		"No"
#define RS_COL_INDEX_FROM	"IndexFrom"
#define RS_COL_INDEX_TO		"IndexTo"
#define RS_COL_CUR_CYCLE	"CurCycle"
#define RS_COL_TOT_CYCLE	"TotalCycle"
#define RS_COL_TYPE			"Type"
#define RS_COL_STATE		"State"
#define RS_COL_TOT_TIME		"TotTime"
#define RS_COL_CODE			"Code"
#define RS_COL_GRADE		"Grade"
#define RS_COL_IR			"IR"
#define RS_COL_CH_NO		"ChNo"
#define RS_COL_SAVE_ACCU	"Select"
#define RS_COL_SEQ			"Sequence"
#define RS_COL_OCV			"OCV"

#define EP_STATE_IDLE			0x0000		//
#define EP_STATE_SELF_TEST		0x0001
#define EP_STATE_STANDBY		0x0002
#define EP_STATE_RUN			0x0003
#define EP_STATE_PAUSE			0x0004
#define EP_STATE_FAIL			0x0005
#define EP_STATE_MAINTENANCE	0x0006
#define EP_STATE_OCV			0x0007
#define EP_STATE_CHARGE			0x0008
#define EP_STATE_DISCHARGE		0x0009
#define EP_STATE_REST			0x000A
#define EP_STATE_IMPEDANCE		0x000B
#define EP_STATE_CHECK			0x000C
#define EP_STATE_STOP			0x000D
#define EP_STATE_END			0x000E
#define EP_STATE_FAULT			0x000F
#define EP_STATE_COMMON			0x0010
#define EP_STATE_NONCELL		0x0011
#define EP_STATE_READY			0x0012


class AFX_EXT_CLASS CTable  
{
////////////////
// Attributes //
////////////////

private:
	int FindItemIndex(WORD nItem);
	void				ReleaseRawDataMemory();
	LONG				m_lLastRecordIndex;
	LONG				m_lRecordIndex;
	LONG                m_lIndex;

	BOOL                m_bLoadData;
	BOOL                m_bLoadLastData;

	COleDateTime        m_StartTime;
	LONG                m_lStepNo;

	CStringList         m_strlistTitle;
	CWordArray			m_awlistItem;

	float*              m_pfltLastData;

	float**             m_ppfltData;
	LONG				m_lTotCycle;
	WORD				m_wType;
	CWordArray			m_DataItemList;

	LONG                m_lNumOfData;				//.cyc에 저장된 data 크기 

	BYTE	m_chNo;			
	BYTE	m_chState;		
	BYTE	m_chCode;		
	BYTE	m_chGradeCode;	

	ULONG	m_nCurrentCycleNum;
	ULONG	m_lSaveSequence;

///////////////////////////////////////////////
// Operations: "Construction and destruction //
///////////////////////////////////////////////
public:
//	CTable(LONG lIndex);
	CTable(LPCTSTR strTitle, LPCTSTR strFromList);
	CTable(CWordArray *paItem, int nStepNo, int nState, int nStartIndex);
	virtual ~CTable();

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:
	WORD GetTypeCode(int nState);
	BOOL SetTableData(CArray<float, float> *aryData);
	CString GetItemName(WORD wItem);
	WORD GetItemID(CString strTitle);
	CWordArray *	GetRecordItemList()	{	return &m_DataItemList;		}
	LONG	GetRecordItemSize()		{	return m_DataItemList.GetSize();	}
	LONG	GetRecordCount();
	WORD	GetType()			{	return m_wType;		}
	LONG	GetRecordIndex()	{	return m_lRecordIndex;	}
	LONG	GetLastRecordIndex()	{	return	m_lLastRecordIndex	; 	}
	LONG    GetStepNo()				{	return m_lStepNo;		};
	LONG    GetCycleNo()			{	return m_lTotCycle;		};
//	void	LoadDataA(LPCTSTR strChPath);
	float   GetLastData(LPCTSTR strTitle, WORD wPoint=0x0000);
//	float   GetDCIR()			{	return m_fltDCIR;		};
//	float   GetOCV()			{	return m_fltOCV;		};
	fltPoint * GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);

protected:
	long FindRsColumnIndex(LPCTSTR szTitle);
	long FindRsColumnIndex(WORD nItem);
	int  FindIndexOfTitle(LPCTSTR title);
	int FindIndexOfTitle(WORD wItem);

};

#endif // !defined(AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
