// Table.cpp: implementation of the CTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Table.h"
#include <math.h>
#include <float.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CTable::CTable(LPCTSTR strTitle, LPCTSTR strFromList)
{
	CString strName(strTitle);		//TableList 파일의 Column Header 목록  
	CString strValue(strFromList);	//현재 Table Index의 값

	// (1) m_strlistTitle
	// -> strName("Table List의 둘째 줄")에서 저장된 각 Transducer의 Name을 List에 넣는다.
	int s=0, p1=0;
	while(p1!=-1)
	{
		p1 = strName.Find(',', s);
		if(p1!=-1)
		{
			CString Title = strName.Mid(s, p1-s);
			s  = p1+1;
			m_strlistTitle.AddTail(Title);
			m_awlistItem.Add(GetItemID(Title));
		}
	}

	// strValue에서 Table의 Index와 각 Transducer의 마지막 값을 얻는다.
	s=0, p1=0;

	// (3) m_pfltLastData: 각 Transducer들의 마지막 값
	int a = m_strlistTitle.GetCount();
	m_pfltLastData = new float[a];
	for(int i=0;i<a;i++)
	{
		p1 = strValue.Find(',', s);
		m_pfltLastData[i] = (float) atof(strValue.Mid(s, p1-s));
		s  = p1+1;
	}
	m_bLoadLastData = TRUE;

	int nIndex1;
	nIndex1 = FindIndexOfTitle((WORD)PS_STATE);
	if(nIndex1 < 0 )		m_chState = 0;
	else					m_chState = (BYTE)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_STEP_NO);
	if(nIndex1 < 0 )		m_lStepNo = 0;
	else					m_lStepNo = (LONG)m_pfltLastData[nIndex1];

	m_wType = GetTypeCode(m_chState);	
	m_lTotCycle = 1;
	m_nCurrentCycleNum = 1;


/*	m_chNo = 

	nIndex1 = FindIndexOfTitle(PS_CODE);
	if(nIndex1 < 0 )		m_chCode = 0;
	else					m_chCode = (BYTE)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_GRADE_CODE);
	if(nIndex1 < 0 )		m_chGradeCode = 0;
	else					m_chGradeCode = (BYTE)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_DATA_SEQ);
	if(nIndex1 < 0 )		m_lSaveSequence = 0;
	else					m_lSaveSequence = (ULONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle("IndexFrom");
	if(nIndex1 < 0 )		m_lRecordIndex = 0;
	else					m_lRecordIndex = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle("IndexTo");
	if(nIndex1 < 0 )		m_lLastRecordIndex = 0;
	else					m_lLastRecordIndex = (LONG)m_pfltLastData[nIndex1];
*/
	
	// 전체 데이터는 아직 loading하지 않았음을 표시한다.
	m_lNumOfData = 0;
	m_bLoadData     = FALSE;
	m_ppfltData     = NULL;

}

CTable::CTable(CWordArray *paItem, int nStepNo, int nState, int nStartIndex)
{
	ASSERT(paItem);
	ASSERT(paItem->GetSize() > 0);
	
	m_DataItemList.Copy(*paItem);
	
	for(int i = 0; i<m_DataItemList.GetSize(); i++)
	{
		m_strlistTitle.AddTail(GetItemName(m_DataItemList.GetAt(i)));
	}


	//////////////////////////////////////////////////////////////////////////
//	m_chNo			= lpRecord->chNo;					// Channel Number

	m_chGradeCode	= 0;
	m_chCode		= 0;
	m_lRecordIndex = nStartIndex;
	m_lTotCycle = 1;
	m_nCurrentCycleNum = 1;

	//formation type state 분석	
	m_wType = GetTypeCode(nState);
	m_chState	= nState;				// Run, Stop(Manual, Error), End
	m_lStepNo = nStepNo;
	//////////////////////////////////////////////////////////////////////////

	// 전체 데이터는 아직 loading하지 않았음을 표시한다.
	m_bLoadData     = FALSE;
	m_ppfltData     = NULL;
	m_pfltLastData  = NULL;
	m_bLoadLastData = FALSE;
}

CTable::~CTable()
{
	ReleaseRawDataMemory();

	if(m_pfltLastData != NULL)
	{
		delete [] m_pfltLastData;
		m_pfltLastData = NULL;
	}
}

int CTable::FindItemIndex(WORD nItem)
{
	for(int a=0; a<m_DataItemList.GetSize(); a++)
	{
		if(m_DataItemList.GetAt(a) == nItem)
		{
			return a;
			break;
		}
	}
	return -1;
}

		
float CTable::GetLastData(LPCTSTR strTitle, WORD wPoint)
{
	if(!m_bLoadLastData) return FLT_MAX;

	int nIndex1 =0, nIndex2 = 0, nIndex3 = 0, nIndex4 = 0;

	if(CString(strTitle).CompareNoCase(RS_COL_WATT)==0)
	{
		nIndex1 = FindIndexOfTitle(PS_VOLTAGE);
		nIndex2 = FindIndexOfTitle(PS_CURRENT);
		if(nIndex1 < 0 || nIndex2 < 0)		return 0.0f;

		return m_pfltLastData[nIndex1]*m_pfltLastData[nIndex2];
	}

	if(CString(strTitle).CompareNoCase("V_MaxDiff")==0)
	{
		nIndex1 = FindIndexOfTitle(RS_COL_VOLTAGE1);
		nIndex2 = FindIndexOfTitle(RS_COL_VOLTAGE2);
		nIndex3 = FindIndexOfTitle(RS_COL_VOLTAGE3);
		nIndex4 = FindIndexOfTitle(RS_COL_VOLTAGE4);

		if(nIndex1 < 0 || nIndex2 < 0 || nIndex3< 0 || nIndex4 < 0)		return 0.0f;

		float fltV1 = m_pfltLastData[nIndex1];
		float fltV2 = m_pfltLastData[nIndex2];
		float fltV3 = m_pfltLastData[nIndex3];
		float fltV4 = m_pfltLastData[nIndex4];
		//
		BYTE byVPoint = LOBYTE(wPoint);
		if     (byVPoint==0x03) // 1번-2번
		{
			return (float)fabs(fltV1-fltV2);
		}
		else if(byVPoint==0x05) // 1번-3번
		{
			return (float)fabs(fltV1-fltV3);
		}
		else if(byVPoint==0x09) // 1번-4번
		{
			return (float)fabs(fltV1-fltV4);
		}
		else if(byVPoint==0x06) // 2번-3번
		{
			return (float)fabs(fltV2-fltV3);
		}
		else if(byVPoint==0x0A) // 2번-4번
		{
			return (float)fabs(fltV2-fltV4);
		}
		else if(byVPoint==0x0C) // 3번-4번
		{
			return (float)fabs(fltV3-fltV4);
		}
		else if(byVPoint==0x07) // 1번-2번-3번
		{
			return __max(__max(fltV1,fltV2),fltV3)-__min(__min(fltV1,fltV2),fltV3);
		}
		else if(byVPoint==0x0B) // 1번-2번-4번
		{
			return __max(__max(fltV1,fltV2),fltV4)-__min(__min(fltV1,fltV2),fltV4);
		}
		else if(byVPoint==0x0D) // 1번-3번-4번
		{
			return __max(__max(fltV1,fltV3),fltV4)-__min(__min(fltV1,fltV3),fltV4);
		}
		else if(byVPoint==0x0E) // 2번-3번-4번
		{
			return __max(__max(fltV2,fltV3),fltV4)-__min(__min(fltV2,fltV3),fltV4);
		}
		else if(byVPoint==0x0F) // 1번-2번-3번-4번
		{
			return __max(__max(__max(fltV1,fltV2),fltV3),fltV4)-__min(__min(__min(fltV1,fltV2),fltV3),fltV4);
		}
		else                    // 한개 선택 or 선택하지 않음
		{
			return 0.0f;
		}
	}
	else if(CString(strTitle).CompareNoCase("T_MaxDiff")==0)
	{
		nIndex1 = FindIndexOfTitle(RS_COL_TEMPERATURE1);
		nIndex2 = FindIndexOfTitle(RS_COL_TEMPERATURE2);
		if(nIndex1 < 0 || nIndex2 < 0 || nIndex3< 0 || nIndex4 < 0)		return 0.0f;

		float fltT1 = m_pfltLastData[nIndex1];
		float fltT2 = m_pfltLastData[nIndex2];
		//
		BYTE byTPoint = HIBYTE(wPoint);
		if(byTPoint==0x03) // 1번-2번
		{
			return (float)fabs(fltT1-fltT2);
		}
		else               // 한개 선택 or 선택하지 않음
		{
			return 0.0f;
		}
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_OCV)==0)	//첫번째 전압이 OCV임 
	{
		//OCV 항목을 그리기 이전에 반드시 LoadDataA()를 먼저 호출 하여야 한다.
		if(m_bLoadData == FALSE)
		{
			return 0.0f;
		}
		nIndex1 = FindIndexOfTitle(PS_VOLTAGE);
		if(nIndex1 < 0)	return 0.0f;
		else 		return m_ppfltData[nIndex1][0];		//재일 첫번째 전압이 OCV이다.
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_TYPE)==0)
	{
		return (float)GetType();
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_STEP_NO)==0)
	{
		return (float)GetStepNo();
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_TOT_CYCLE)==0)
	{
		return (float)GetCycleNo();
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_STATE)==0)
	{
		return (float)m_chState;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_CUR_CYCLE)==0)
	{
		return (float)m_nCurrentCycleNum;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_CODE)==0)
	{
		return (float)m_chCode;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_GRADE)==0)
	{
		return (float)m_chGradeCode;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_CH_NO)==0)
	{
		return (float)m_chNo;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_SEQ)==0)
	{
		return (float)m_lSaveSequence;
	}

	//default data
	nIndex1 = FindIndexOfTitle(strTitle);
	if(nIndex1 < 0 )		return 0.0f;

	return m_pfltLastData[nIndex1];
}

fltPoint* CTable::GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
{
	if(!m_bLoadData)	return NULL;

	int iXIndex=0, iYIndex1=0, iYIndex2=0, iYIndex3=0, iYIndex4=0;
	
	iXIndex = FindRsColumnIndex(strXAxisTitle);

	if(CString(strYAxisTitle).CompareNoCase(RS_COL_WATT)==0)
	{
		iYIndex1 = FindRsColumnIndex(PS_VOLTAGE);
		iYIndex2 = FindRsColumnIndex(PS_CURRENT);
	}
	else if(CString(strYAxisTitle).CompareNoCase("V_MaxDiff")==0)
	{
		iYIndex1 = FindRsColumnIndex(RS_COL_VOLTAGE1);
		iYIndex2 = FindRsColumnIndex(RS_COL_VOLTAGE2);
		iYIndex3 = FindRsColumnIndex(RS_COL_VOLTAGE3);
		iYIndex4 = FindRsColumnIndex(RS_COL_VOLTAGE4);
	}
	else if(CString(strYAxisTitle).CompareNoCase("T_MaxDiff")==0)
	{
		iYIndex1 = FindRsColumnIndex(RS_COL_TEMPERATURE1);
		iYIndex2 = FindRsColumnIndex(RS_COL_TEMPERATURE2);
	}
	else 
	{
		iYIndex1 = FindRsColumnIndex(strYAxisTitle);
	}

	fltPoint* pfltPt = NULL;
	//Data가 저장되어 있지 않을 경우 
	if(iXIndex < 0 || iYIndex1 < 0 || iYIndex2 < 0 || iYIndex3 < 0 || iYIndex4 < 0)
	{
		lDataNum = 0;
		return pfltPt;
	}

	//
	lDataNum = m_lNumOfData;
	pfltPt   = new fltPoint[lDataNum];
	//
	for(int i=0; i<m_lNumOfData; i++)
	{
		// X축값
		pfltPt[i].x = m_ppfltData[iXIndex][i];

		// Y축값
		if(CString(strYAxisTitle).CompareNoCase(RS_COL_WATT)==0)
		{
			pfltPt[i].y = m_ppfltData[iYIndex1][i]*m_ppfltData[iYIndex2][i]/1000.0f;
		}
		else if(CString(strYAxisTitle).CompareNoCase("V_MaxDiff")==0)
		{
			BYTE byVPoint = LOBYTE(wPoint);
			if     (byVPoint==0x03) // 1번-2번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex2][i]);
			}
			else if(byVPoint==0x05) // 1번-3번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex3][i]);
			}
			else if(byVPoint==0x09) // 1번-4번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x06) // 2번-3번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex2][i]-m_ppfltData[iYIndex3][i]);
			}
			else if(byVPoint==0x0A) // 2번-4번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex2][i]-m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0C) // 3번-4번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex3][i]-m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x07) // 1번-2번-3번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i])
					       -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]);
			}
			else if(byVPoint==0x0B) // 1번-2번-4번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0D) // 1번-3번-4번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0E) // 2번-3번-4번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex2][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(m_ppfltData[iYIndex2][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0F) // 1번-2번-3번-4번
			{
				pfltPt[i].y=__max(__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
			}
			else                    // 한개 선택 or 선택하지 않음
			{
				pfltPt[i].y=0.0f;
			}
		}
		else if(CString(strYAxisTitle).CompareNoCase("T_MaxDiff")==0)
		{
			BYTE byTPoint = HIBYTE(wPoint);
			if(byTPoint==0x03) // 1번-2번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex2][i]);
			}
			else               // 한개 선택 or 선택하지 않음
			{
				pfltPt[i].y=0.0f;
			}
		}
		else
		{
			pfltPt[i].y = m_ppfltData[iYIndex1][i];
		}
	}

	//
	return pfltPt;
}

//최종 결과의 Column의 Index를 구한다.
int CTable::FindIndexOfTitle(LPCTSTR title)
{
	BOOL bCheck = FALSE;
	for(int i=0; i<m_strlistTitle.GetCount(); i++)
	{
		if(m_strlistTitle.GetAt(m_strlistTitle.FindIndex(i)).CompareNoCase(title)==0)
		{
			bCheck = TRUE;
			break;
		}
	}

	if(bCheck) return i;
	
	return -1;
}


//최종 결과의 Column의 Index를 구한다.
int CTable::FindIndexOfTitle(WORD wItem)
{
//	BOOL bCheck = FALSE;
	for(int i=0; i<m_awlistItem.GetSize(); i++)
	{
		if(m_awlistItem.GetAt(i) == wItem)
		{
			return i;
		}
	}
	return -1;
}

//실처리 Data의 Column Index를 구한다.
long CTable::FindRsColumnIndex(WORD nItem)
{
	for(int i=0; i<GetRecordItemSize(); i++)
	{
		if(nItem == m_DataItemList.GetAt(i))
		{
			return i;
		}
	}

	return -1;
}


long CTable::FindRsColumnIndex(LPCTSTR szTitle)
{
	return FindRsColumnIndex(GetItemID(szTitle));
}

WORD CTable::GetItemID(CString strTitle)
{
//	No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temp,Press,AvgVoltage,AvgCurrent,
	if(!strTitle.CompareNoCase(RS_COL_VOLTAGE))
	{
		return PS_VOLTAGE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CURRENT))
	{
		return PS_CURRENT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CAPACITY))
	{
		return PS_CAPACITY;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TIME))
	{
		return PS_STEP_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_WATT))
	{
		return PS_WATT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_WATT_HOUR))
	{
		return PS_WATT_HOUR;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TEMPERATURE))
	{
		return PS_TEMPERATURE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_PRESSURE))
	{
		return PS_PRESSURE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_SEQ))
	{
		return PS_DATA_SEQ;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AVG_VTG))
	{
		return PS_AVG_VOLTAGE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AVG_CRT))
	{
		return PS_AVG_CURRENT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_STEP_NO))
	{
		return PS_STEP_NO;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CUR_CYCLE))
	{
		return PS_CUR_CYCLE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TOT_CYCLE))
	{
		return PS_TOT_CYCLE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TYPE))
	{
		return PS_STEP_TYPE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_STATE))
	{
		return PS_STATE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TOT_TIME))
	{
		return PS_TOT_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CODE))
	{
		return PS_CODE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_GRADE))
	{
		return PS_GRADE_CODE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_IR))
	{
		return PS_IMPEDANCE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CH_NO))
	{
		return PS_CHANNEL_NO;
	}

	return 0xffff;
}

CString CTable::GetItemName(WORD wItem)
{
	switch (wItem)
	{
	case PS_VOLTAGE:		return RS_COL_VOLTAGE;
	case PS_CURRENT:		return RS_COL_CURRENT;
	case PS_CAPACITY:		return RS_COL_CAPACITY;
	case PS_STEP_TIME:		return RS_COL_TIME;
	case PS_WATT:			return RS_COL_WATT;
	case PS_WATT_HOUR:		return RS_COL_WATT_HOUR;
	case PS_TEMPERATURE:	return RS_COL_TEMPERATURE;
	case PS_PRESSURE:		return RS_COL_PRESSURE;
	case PS_DATA_SEQ:		return RS_COL_SEQ;
	case PS_AVG_VOLTAGE:	return RS_COL_AVG_VTG;
	case PS_AVG_CURRENT:	return RS_COL_AVG_CRT;
	case PS_STEP_NO:		return RS_COL_STEP_NO;

	case PS_CUR_CYCLE:		return RS_COL_CUR_CYCLE;
	case PS_TOT_CYCLE:		return RS_COL_TOT_CYCLE;
	case PS_STEP_TYPE:		return RS_COL_TYPE;
	case PS_STATE:			return RS_COL_STATE;
	case PS_TOT_TIME:		return RS_COL_TOT_TIME;
	case PS_CODE:			return RS_COL_CODE;
	case PS_GRADE_CODE:		return RS_COL_GRADE;
	case PS_IMPEDANCE:		return RS_COL_IR;

	case PS_CHANNEL_NO:		return RS_COL_CH_NO;
	}
	return "Unknown";
}

void CTable::ReleaseRawDataMemory()
{
	//
	if(m_ppfltData != NULL)
	{
		for(int i=0; i<GetRecordItemSize(); i++)
		{
			if(m_ppfltData[i] != NULL)
			{
				delete [] m_ppfltData[i];
				m_ppfltData[i] = NULL;
			}
		}
		delete [] m_ppfltData;
		m_ppfltData = NULL;
	}
}

LONG CTable::GetRecordCount()
{
	return m_lNumOfData;
}

BOOL CTable::SetTableData(CArray<float, float> *aryData)
{
	if(m_bLoadData)		return TRUE;

	m_lNumOfData = aryData[0].GetSize();
	int nColumnSize = GetRecordItemSize();

	//저장된 Record수를 검색
	if(m_lNumOfData <=0 )	return	FALSE;	 		//실처리로 저장된 Data가 없음 
		
	ASSERT(m_ppfltData == NULL);
	ASSERT(m_pfltLastData == NULL);

	//Last data 저장용
	m_pfltLastData = new float[nColumnSize];

	//2차원 동적 배열 
	m_ppfltData = new float*[nColumnSize];
	for(int i=0; i<nColumnSize; i++)
	{
		m_ppfltData[i] = new float[m_lNumOfData];
	}

	for(int a = 0; a < nColumnSize; a++)
	{
		for(int b = 0; b < m_lNumOfData; b++)
		{		
			m_ppfltData[a][b] = aryData[a].GetAt(b);
		}
		//Step 마지막 Data 별도 보관 
		m_pfltLastData[a] = aryData[a].GetAt(m_lNumOfData-1);
		aryData[a].RemoveAll();
	}

	m_lLastRecordIndex = m_lRecordIndex+m_lNumOfData-1;
	m_lSaveSequence = m_lLastRecordIndex;

	m_bLoadLastData = TRUE;
	m_bLoadData     = TRUE;

	return TRUE;
}

WORD CTable::GetTypeCode(int nState)
{
	//formation type state 분석	
	WORD wType = PS_STEP_REST;
	
	switch(nState)
	{
	case EP_STATE_OCV		:	wType = PS_STEP_OCV;		break;	//0x0007
	case EP_STATE_CHARGE	:	wType = PS_STEP_CHARGE;		break;	//0x0008
	case EP_STATE_DISCHARGE	:	wType = PS_STEP_DISCHARGE;	break;	//0x0009
	case EP_STATE_REST		:	wType = PS_STEP_REST;		break;	//0x000A
	case EP_STATE_IMPEDANCE	:	wType = PS_STEP_IMPEDANCE;	break;	//0x000B
	case EP_STATE_END		:	wType = PS_STEP_END;		break;	//0x000E
	case EP_STATE_CHECK		:	wType = PS_STEP_CHECK;		break;	//0x000C

	case EP_STATE_IDLE		:	//0x0000		//
	case EP_STATE_SELF_TEST	:	//0x0001
	case EP_STATE_STANDBY	:	//0x0002
	case EP_STATE_RUN		:	//0x0003
	case EP_STATE_PAUSE		:	//0x0004
	case EP_STATE_FAIL		:	//0x0005
	case EP_STATE_MAINTENANCE :	//0x0006
	case EP_STATE_STOP		:	//0x000D
	case EP_STATE_FAULT		:	//0x000F	
	case EP_STATE_COMMON	:	//0x0010
	case EP_STATE_NONCELL	:	//0x0011
	case EP_STATE_READY		:	//0x0012
	default:					break;
	}
	return wType;
}
