// Grading.h: interface for the CGrading class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_)
#define AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Afxtempl.h>

typedef struct GradeStep{
	float fMin;
	float fMax;
	CString strCode;
} GRADE_STEP;
/*
#ifdef _DEBUG
	typedef struct s_FileTestInformation {
		WORD	lID;
		WORD	lType;			//Added 2005/08/31
		char	szName[64];
		char	szDescription[128];
		char	szCreator[64];
		char	szModifiedTime[64];
	} FILE_TEST_INFORMATION, SCHDULE_INFORMATION_DATA;
#else
*/	typedef struct s_FileTestInformation {
		LONG	lID;
		LONG	lType;			//Added 2005/08/31
		char	szName[64];
		char	szDescription[128];
		char	szCreator[64];
		char	szModifiedTime[64];
	} FILE_TEST_INFORMATION, SCHDULE_INFORMATION_DATA;
//#endif

typedef struct tag_FILE_CELL_CHECK_PARAMETER {
	float	fMaxVoltage;
	float	fMinVoltage;
	float	fMaxCurrent;
	float	fVrefVal;			//Vref
	float	fIrefVal;			//Iref
	float	fDeltaVoltage;
	ULONG	lTrickleTime;
	int		nMaxFaultNo;
	BOOL	bPreTest;
}	FILE_CELL_CHECK_PARAM, CELL_CHECK_DATA;

typedef struct tag_File_GRADE_CON {
	long	lGradeItem;
	BYTE	chTotalGrade;
	float	faValue1[10];
	float	faValue2[10];
	char	aszGradeCode[10];
}	FILE_GRADE;

typedef struct tag_FILE_STEP_CONDITION_V1000 {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref;
	float	fIref;

	ULONG	lEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	UINT	nLoopInfoGoto;
	UINT	nLoopInfoCycle;

	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	LONG	lDeltaTime;
	LONG	lDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	ULONG	ulCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	ULONG	ulCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	ULONG	ulReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	ULONG	ulDCRStartTime;			//EDLC DCR 검사시 Start Time
	ULONG	ulDCREndTime;			//EDLC DCR 검사시 End Time
	ULONG	ulLCStartTime;			//EDLC LC 측정 시작 시간
	ULONG	ulLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택

}	FILE_STEP_PARAM_10000;	

typedef struct tag_FILE_STEP_CONDITION {
	BYTE	chStepNo;
	BYTE	chReserved[3];		//Added 20071102

	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	WORD	wReserved1;			//Added 20071102
	
	float	fVref;
	float	fIref;

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	UINT	nLoopInfoGoto;
	UINT	nLoopInfoCycle;
	UINT	nLoopInfoEndTGoto;
	UINT	nLoopInfoEndVGoto;
	UINT	nLoopInfoEndIGoto;
	UINT	nLoopInfoEndCGoto;
	UINT	nGotoStepID;

	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];

	//20071106 (의미 변경)
	float	fPatternMinVal;			//Simulation file내  data의 최소값 
	float	fPatternMaxVal;			//Simulation file내  data의 최대값 
	float	fStartT;				//시작 온도 
	float	fEndT;					//종료 온도 

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fSocRate;				//비교 SOC Rate

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 200710/29
	float	fTref;
	float	fReserved[25];
	
	//Added 2005/11/08
	BYTE	bUseActualCapa;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	BYTE	bUseDataStepNo;			//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;
	
	//20071106
	char	szSimulationFile[64];
	long	lReserved[15];
	//////////////////////////////////////////////////////////////////////////
	
}	FILE_STEP_PARAM;

class AFX_EXT_CLASS CGrading  
{
public:
	WORD GetGradeItem()	{	return (WORD)m_lGradingItem;	}
	long m_lGradingItem;
	BOOL SetGradingData(CGrading &grading);
	GRADE_STEP GetStepData(int nIndex);
	int GetGradeStepSize();
	BOOL ClearStep();
	BOOL AddGradeStep(float fMin, float fMax, CString Code);
	CString GetGradeCode(float fData);
	CGrading();
	virtual ~CGrading();
//	void operator = (const CGrading grading); 

private:
	CArray<GRADE_STEP ,GRADE_STEP> m_ptArray;

};

#endif // !defined(AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_)
