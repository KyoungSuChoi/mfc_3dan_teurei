#pragma once

template <class T>
class CSBCSyncParent
{
	friend class CSBCSyncObj;
public:
	class CSBCSyncObj
	{
	private:
		const CSBCSyncParent &m_rOwner;

	public:
		CSBCSyncObj(const CSBCSyncParent &rOwner) : m_rOwner(rOwner) {m_rOwner.m_csSyncObj.Enter();}
		~CSBCSyncObj(VOID) {m_rOwner.m_csSyncObj.Leave();}
	};

private:
	mutable CSBCCriticalSection m_csSyncObj;
};

#define SBCSync SBCSync(*this)