#include "stdafx.h"
#include "SBCCriticalSection.h"
#include "SBCStaticSyncParent.h"
#include "SBCSyncParent.h"
#include "SBCMemoryPool.h"
#include "SBCManagedBuf.h"

CSBCManagedBuf::CSBCManagedBuf(void)
{
	ZeroMemory(m_aucBuf, sizeof(m_aucBuf));
}

CSBCManagedBuf::~CSBCManagedBuf(void)
{
}
