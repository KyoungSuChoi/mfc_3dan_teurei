#pragma once

template <class T>
class CSBCStaticSyncParent
{
	friend class CSBCStaticSyncObj;
public:
	class CSBCStaticSyncObj
	{
	public:
		CSBCStaticSyncObj(VOID) {T::m_scsSyncObj.Enter();}
		~CSBCStaticSyncObj(VOID) {T::m_scsSyncObj.Leave();}
	};

private:
	static CSBCCriticalSection m_scsSyncObj;
};

template <class T>
CSBCCriticalSection CSBCStaticSyncParent<T>::m_scsSyncObj;