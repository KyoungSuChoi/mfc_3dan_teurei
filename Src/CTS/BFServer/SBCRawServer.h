//#pragma once
//
//#define MAX_ACCEPT_POOL_CNT 5
//
//#define POST_MESSAGE_C(eIoType, poNetObj, dwLen)\
//	{\
//	CSBCNetIoStatus *ponios = new CSBCNetIoStatus();\
//	ponios->m_eIO = eIoType;\
//	ponios->m_poObject = poNetObj;\
//	ponios->m_dwNumOfByteTransfered = dwLen;\
//	WorkingSignal(ponios);\
//	}
//
//template <class TNetObj>
//DWORD WINAPI WorkerThreadThreadCallback(LPVOID lpParam);
//
//template <class TNetObj>
//class CSBCRawServer : public CSBCIocp
//{
//public:
//	CSBCRawServer(VOID) { m_dwAcceptPoolCnt = MAX_ACCEPT_POOL_CNT; }
//
//	BOOL Begin(USHORT usListenPort)
//	{
//		//메모리풀 이용시 new 객체 * 50 메모리 블럭을 할당
//		//현재로선 해제 불가
//		m_poNetIocp = new CSBCNetIocp<TNetObj>();
//		//-------------------------------------------------------------------------
//		m_poListenObj = new TNetObj();
//		//-------------------------------------------------------------------------	
//		for (DWORD i=0;i<m_dwAcceptPoolCnt;i++)
//		{
//			TNetObj *poAcceptObj = new TNetObj();
//			m_lstAcceptObj.push_back(poAcceptObj);
//		}
//
//		m_poNetIocp->SetOwnerIocp(this);
//
//		if (!m_poNetIocp->Begin(0))
//			return FALSE;
//
//		// Backlog와 AcceptPool의 비율은 10:1로 정의한다.
//		if (!m_poListenObj->Listen(usListenPort, m_dwAcceptPoolCnt * 10))
//			return FALSE;
//
//		if (!m_poNetIocp->RegSocketToIocp(m_poListenObj->GetSocket(), (ULONG_PTR) m_poListenObj))
//			return FALSE;
//
//		//if (!CIocp::Begin(1))
//		//	return FALSE;
//		m_bWorkerThreadFlag = TRUE;
//		m_hWorkerThread = CreateThread(NULL, 0, ::WorkerThreadThreadCallback<TNetObj>, this, 0, NULL);
//
//		for (std::list<TNetObj*>::iterator it=m_lstAcceptObj.begin();it!=m_lstAcceptObj.end();it++)
//		{
//			TNetObj *poAcceptObj = (TNetObj*)(*it);
//			if (!poAcceptObj->Accept(m_poListenObj->GetSocket()))
//				return FALSE;
//		}
//
//		return TRUE;
//	}
//
//	VOID End(VOID)
//	{
//		m_bWorkerThreadFlag = FALSE;
//
//		for (std::list<TNetObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
//		{
//			TNetObj *poSBCObj = (TNetObj*)(*it);
//			if (poSBCObj->ForceClose())
//			{
//				OnDisconnected(poSBCObj);
//				delete poSBCObj;
//			}
//		}
//
//		for (std::list<TNetObj*>::iterator it=m_lstAcceptObj.begin();it!=m_lstAcceptObj.end();it++)
//		{
//			TNetObj *poAcceptObj = (TNetObj*)(*it);
//			if (poAcceptObj->ForceClose())
//			{
//				delete poAcceptObj;
//			}
//		}
//
//		m_poNetIocp->End();
//		m_poListenObj->ForceClose();
//
//		delete m_poListenObj;
//		delete m_poNetIocp;
//
//		m_lstAcceptObj.clear();
//		m_lstConnectedObj.clear();
//	}
//
//	SOCKET GetListenSocket(VOID) { return m_poListenObj->GetSocket(); }
//	VOID PushIO(CSBCNetIoStatus* ponios) { m_qIO.Push(ponios); }
//
//	std::list<CSBCObj*>::iterator GetConnectOBJList() {return m_lstConnectedObj;}
//
//private:
//	std::list<TNetObj*> m_lstAcceptObj;
//	std::list<TNetObj*> m_lstConnectedObj;
//	TNetObj *m_poListenObj;
//	CSBCNetIocp<TNetObj> *m_poNetIocp;
//	DWORD m_dwAcceptPoolCnt;
//	CSBCCircularQueue<CSBCNetIoStatus*> m_qIO;
//	HANDLE m_hWorkerThread;
//	BOOL m_bWorkerThreadFlag;
//
//	BOOL m_bConnected;
//	TNetObj *m_poConnectObj;
//
//protected:
//	virtual VOID OnConnected(TNetObj *poNetObj) = 0;
//	virtual VOID OnDisconnected(TNetObj *poNetObj) = 0;
//	virtual VOID OnRead(TNetObj *poNetObj, EP_MSG_HEADER &_hdr, BYTE *pReadBuf, DWORD dwLen) = 0;
//	virtual VOID OnWrite(TNetObj *poNetObj, DWORD dwLen) = 0;
//public:
//
//	VOID WorkerThreadThreadCallback(VOID)
//	{
//		while (m_bWorkerThreadFlag)
//		{
//			CSBCNetIoStatus *ponios = NULL;
//			if (m_qIO.Pop(ponios))
//			{
//				TNetObj *poNetObj = (TNetObj*) ponios->m_poObject;
//				EP_MSG_HEADER hdr;
//				CSBCManagedBufSP Buf;
//				DWORD dwPacketLen = 0;
//				BOOL bReadBufRet = FALSE;
//
//				switch (ponios->m_eIO)
//				{
//				case IO_ACCEPT:
//					m_lstAcceptObj.remove(poNetObj);
//					m_lstConnectedObj.push_back(poNetObj);
//					OnConnected(poNetObj);
//					break;
//				case IO_NEW_ACCEPTOBJ:
//					m_lstAcceptObj.push_back(poNetObj);
//					break;
//				case IO_DISCONNECT:
//					m_lstConnectedObj.remove(poNetObj);
//					OnDisconnected(poNetObj);
//					delete poNetObj;
//					//poNetObj = NULL;
//					break;
//				case IO_READ:
//					while (bReadBufRet = poNetObj->ReadPacket(hdr, Buf->m_aucBuf, ponios->m_dwNumOfByteTransfered, dwPacketLen))
//					{
//						if (dwPacketLen > 0)
//							OnRead(poNetObj, hdr, Buf->m_aucBuf, dwPacketLen);
//						else
//						{
//							// 패킷이 변조되었을 때의 처리 (접속 종료)
//							poNetObj->ForceClose();
//							break;
//						}
//					}
//					break;
//				case IO_WRITE:
//					OnWrite(poNetObj, ponios->m_dwNumOfByteTransfered);
//					break;
//				}
//				////-------------------------------------------------------------------------
//				//WCHAR tempStr[1024] = {0,};
//				//_snwprintf_s(tempStr, 1024, L"WorkerThreadThreadCallback %d \n", ponios->m_eIO);
//				//OutputDebugString(tempStr);
//				//////-------------------------------------------------------------------------
//				delete ponios;
//
//			}
//		}
//	}
//};
//template <class TNetObj>
//DWORD WINAPI WorkerThreadThreadCallback(LPVOID lpParam)
//{
//	CSBCRawServer<CSBCNetObj<TNetObj>>* pSimpleServer = (CSBCRawServer<CSBCNetObj<TNetObj>>*) lpParam;
//	pSimpleServer->WorkerThreadThreadCallback();
//	return 0;
//}
#pragma once

#define MAX_ACCEPT_POOL_CNT 60

template <class TNetObj>
class CSBCRawServer : public CSBCIocp
{
public:
	CSBCRawServer(VOID) { m_dwAcceptPoolCnt = MAX_ACCEPT_POOL_CNT; }

	BOOL Begin(USHORT usListenPort)
	{
		m_poNetIocp = new CSBCNetIocp<TNetObj>();
		
		for (DWORD i=0;i<m_dwAcceptPoolCnt;i++)
		{
			TNetObj *poAcceptObj = new TNetObj();
			m_lstAcceptObj.push_back(poAcceptObj);
		}
		
		m_poNetIocp->SetOwnerIocp(this);
		if (!m_poNetIocp->Begin(8))
			return FALSE;

		// Backlog와 AcceptPool의 비율은 10:1로 정의한다.
		if (!m_poListenObj->Listen(usListenPort, m_dwAcceptPoolCnt * 10))
			return FALSE;

		if (!m_poNetIocp->RegSocketToIocp(m_poListenObj->GetSocket(), (ULONG_PTR) m_poListenObj))
			return FALSE;

		if (!CSBCIocp::Begin(1))//??
			return FALSE;
			
		if( m_poListenObj->Accept(m_poListenObj->GetSocket()))
		{
			return FALSE;
		}	
		
		for (std::list<TNetObj*>::iterator it=m_lstAcceptObj.begin();it!=m_lstAcceptObj.end();it++)
		{
			TNetObj *poAcceptObj = (TNetObj*)(*it);
			if (!poAcceptObj->Accept(m_poListenObj->GetSocket()))
				return FALSE;
			poAcceptObj->SetState(OBJ_ACET);
		}

		return TRUE;
	}

	VOID End(VOID)
	{
		CSBCIocp::End();
		TRACE("Work Thread Stop !!!!!!! \n");

		//for (std::list<TNetObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
		//{
		//	TNetObj *poSBCObj = (TNetObj*)(*it);
		//	if (poSBCObj->ForceClose())
		//		OnDisconnected(poSBCObj);
		//}

		for (std::list<TNetObj*>::iterator it=m_lstAcceptObj.begin();it!=m_lstAcceptObj.end();it++)
		{
			TNetObj *poAcceptObj = (TNetObj*)(*it);
			if (poAcceptObj->ForceClose())
				OnDisconnected(poAcceptObj);
		}
		TRACE("Connect Obj ForceClose !!!!!!! \n");

		m_poNetIocp->End();
		TRACE("Recv Thread Stop !!!!!!! \n");

		m_poListenObj->ForceClose();
		TRACE("ListenObj ForceClose !!!!!!! \n");

		delete m_poNetIocp;
		delete m_poListenObj;


		for (std::list<TNetObj*>::iterator it=m_lstAcceptObj.begin();it!=m_lstAcceptObj.end();it++)
		{
			TNetObj *poAcceptObj = (TNetObj*)(*it);
			delete poAcceptObj;
		}
		m_lstAcceptObj.clear();
		TRACE("Connect Obj Delete !!!!!!! \n");
		//m_lstConnectedObj.clear();
	}

	SOCKET GetListenSocket(VOID) { return m_poListenObj->GetSocket(); }
	std::list<TNetObj*> m_lstAcceptObj;
private:
	
	//std::list<TNetObj*> m_lstConnectedObj;
	TNetObj *m_poListenObj;
	CSBCNetIocp<TNetObj> *m_poNetIocp;
	DWORD m_dwAcceptPoolCnt;

protected:
	virtual VOID OnConnected(TNetObj *poNetObj) = 0;
	virtual VOID OnDisconnected(TNetObj *poNetObj) = 0;
	virtual VOID OnRead(TNetObj *poNetObj, EP_MSG_HEADER &_hdr, BYTE *pReadBuf, DWORD dwLen) = 0;
	virtual VOID OnWrite(TNetObj *poNetObj, DWORD dwLen) = 0;

	VOID OnIo(BOOL bSucc, DWORD dwNumOfByteTransfered, ULONG_PTR pCompletionKey, OVERLAPPED *pol)
	{
		CSBCNetIoStatus *ponios = (CSBCNetIoStatus*) pol;
		if (ponios)
		{
			TNetObj *poNetObj = (TNetObj*) ponios->m_poObject;
			CSBCManagedBufSP Buf;
			DWORD dwDataLen = 0;
			BOOL bReadBufRet = FALSE;

			EP_MSG_HEADER hdr;

			switch (ponios->m_eIO)
			{
			case IO_ACCEPT:
				//m_lstAcceptObj.remove(poNetObj);
				//m_lstConnectedObj.push_back(poNetObj);
				OnConnected(poNetObj);
				poNetObj->SetState(OBJ_CCON);
				break;
				
			case IO_NEW_ACCEPTOBJ:
				//m_lstAcceptObj.push_back(poNetObj);
				break;
				
			case IO_DISCONNECT:
				//m_lstConnectedObj.remove(poNetObj);
				OnDisconnected(poNetObj);
				poNetObj->SetState(OBJ_DCON);
				if (!poNetObj->Accept(m_poListenObj->GetSocket()))
				{
					CSBCLog::WriteErrorLog("\n\n\n\n poNetObj->Accept Fail\n\n\n\n\n");
				}				
				poNetObj->SetState(OBJ_ACET);
				//delete poNetObj;
				break;
				
			case IO_READ:
				//TRACE("================ start =================== IN READ [recvlen] %d, [dwPacketLen] %d \n"
				//	, ponios->m_dwNumOfByteTransfered, dwDataLen);

				while (bReadBufRet = poNetObj->ReadPacket(hdr, Buf->m_aucBuf
					, ponios->m_dwNumOfByteTransfered, dwDataLen))
				{
					//TRACE("After ReadPacket [recvlen] %d, [dwPacketLen] %d \n"
					//	, ponios->m_dwNumOfByteTransfered, dwDataLen);

					if (dwDataLen >= 0)
						OnRead(poNetObj, hdr, Buf->m_aucBuf, dwDataLen);
					else
					{
						// 패킷이 변조되었을 때의 처리 (접속 종료)
						//poNetObj->ForceClose();
						CSBCLog::WriteErrorLog("IO_READ - ReadPacket - dwDataLen < 0");
						break;
					}

					ZeroMemory(&hdr, sizeof(EP_MSG_HEADER));

					//TRACE("================ end =================== After OnRead [recvlen] %d, [dwPacketLen] %d \n"
					//	, ponios->m_dwNumOfByteTransfered, dwDataLen);
				}

				//if(poNetObj->getRecvTotlen() == poNetObj->getProcTotlen())
				//{
				//	TRACE("\n\n TOT [RECV]%d/[PROC]%d \n\n"
				//		, poNetObj->getRecvTotlen()
				//		,  poNetObj->getProcTotlen());

				//	poNetObj->ResetTotlen();
				//}

				break;
			case IO_WRITE:
				OnWrite(poNetObj, ponios->m_dwNumOfByteTransfered);
				delete ponios;
				break;
			}
		}
	}
};
