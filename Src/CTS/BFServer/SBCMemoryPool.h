#pragma once

#include <vector>

template <class T, int ALLOC_BLOCK_SIZE = 100>
class CSBCMemoryPool : public CSBCStaticSyncParent<T>
{
public:
	static VOID* operator new(size_t iAllocLen)
	{
		CSBCStaticSyncObj Sync2;

		ASSERT(sizeof(T) == iAllocLen);
		ASSERT(sizeof(T) >= sizeof(UCHAR*));

		if (!m_pucFree)
			allocBlock();

		UCHAR *pReturn = m_pucFree;
		m_pucFree = *reinterpret_cast<UCHAR**>(pReturn);
		++m_iLength;
		////-------------------------------------------------------------------------
		//WCHAR tempStr[1024] = {0,};
		//_snwprintf_s(tempStr, 1024, L"Use Capacity %d m_iLength %d \n", m_iCapacity, m_iLength);
		//OutputDebugString(tempStr);
		////-------------------------------------------------------------------------

		return pReturn;
	}

	static VOID	operator delete(VOID* pDelete)
	{
		CSBCStaticSyncObj Sync2;

		if (m_iLength == 0) throw "delete invalid pointer";
		--m_iLength;

		*reinterpret_cast<UCHAR**>(pDelete) = m_pucFree;
		m_pucFree = static_cast<UCHAR*>(pDelete);
		//if(m_isMemPoolEnd && m_iCapacity)
		//{
		//	for (DWORD i = 0; i < m_dellist.size(); i++)
		//	{
		//		UCHAR *dellobj = m_dellist[i];
		//		delete dellobj;
		//	}
		//	m_iCapacity = 0;
		//}
		//-------------------------------------------------------------------------
		//WCHAR tempStr[1024] = {0,};
		//_snwprintf_s(tempStr, 1024, L"delete Capacity %d m_iLength %d \n", m_iCapacity, m_iLength);
		//OutputDebugString(tempStr);
		////-------------------------------------------------------------------------
	}

	static INT GetLength(VOID) { CSBCStaticSyncObj Sync2; return m_iLength; }
	static INT GetCapacity(VOID) { CSBCStaticSyncObj Sync2; return m_iCapacity;}

private:
	static VOID	allocBlock()
	{
		m_pucFree = new UCHAR[sizeof(T) * ALLOC_BLOCK_SIZE];

		UCHAR **ppCur = reinterpret_cast<UCHAR **>(m_pucFree);
		UCHAR *pNext = m_pucFree;

		for (INT i=0;i<ALLOC_BLOCK_SIZE-1;++i)
		{
			pNext += sizeof(T);
			*ppCur = pNext;
			ppCur = reinterpret_cast<UCHAR**>(pNext);
		}

		*ppCur = 0;
		m_iCapacity += ALLOC_BLOCK_SIZE;
		//-------------------------------------------------------------------------
		TCHAR tempStr[1024] = {0,};
		_sntprintf_s(tempStr, 1024, "\n-------------------------------------------------------------------\n\
									----------------------Memory Leaks    allocBlock size %d / Capacity %d  \n\
									------------------------------------------------------------------------\n"
									, sizeof(T) * ALLOC_BLOCK_SIZE, m_iCapacity);
		OutputDebugString(tempStr);
		//-------------------------------------------------------------------------
	}

private:
	static UCHAR	*m_pucFree;
	static INT		m_iLength;
	static INT		m_iCapacity;

protected:
	~CSBCMemoryPool(){}
};

template <class T, int ALLOC_BLOCK_SIZE>
UCHAR* CSBCMemoryPool<T, ALLOC_BLOCK_SIZE>::m_pucFree;
template <class T, int ALLOC_BLOCK_SIZE>
INT CSBCMemoryPool<T, ALLOC_BLOCK_SIZE>::m_iLength;
template <class T, int ALLOC_BLOCK_SIZE>
INT CSBCMemoryPool<T, ALLOC_BLOCK_SIZE>::m_iCapacity;
