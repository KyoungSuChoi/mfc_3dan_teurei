#pragma once

class CSBCServer : public CSBCRawServer<CSBCObj>
{
public:
	CSBCServer();
	virtual ~CSBCServer();

private:
	INT						mConnectCnt;
	//std::list<CSBCObj*>		m_lstSBCObj;

	HWND					mPapaHwnd;
	HWND					mNotifyHwnd;
	HANDLE					mKeepThreadHandle;
	HANDLE					mKeepThreadDestroyEvent;

	BOOL					mMainThreadOn;
	HANDLE					mMainThreadHandle;
	HANDLE					mMainThreadDestroyEvent;
	
	HANDLE					mObjClose;

	HANDLE					mResetThreadHandle;
	HANDLE					mResetThreadDestroyEvent;

public:
	BOOL	NetBegin(HWND);
	BOOL	NetEnd(VOID);

	VOID	KeepThreadCallback(VOID);

	VOID	MainThreadCallback(VOID);

	VOID	ResetThreadCallback(VOID);

	BOOL	SavePacket(INT nModuleID, EP_MSG_HEADER  *_hdr, BYTE *pReadBuf, DWORD dwLen);

	BOOL	SendCommand(INT nModuleID);

	VOID	SetNotiHWND(INT, HWND, UINT);

protected:

	VOID OnConnected(CSBCObj *poNetObj);

	VOID OnDisconnected(CSBCObj *poNetObj);
	VOID OnRead(CSBCObj *poNetObj, EP_MSG_HEADER &_hdr,  BYTE *pReadBuf, DWORD dwLen);

	VOID OnWrite(CSBCObj *poNetObj, DWORD dwLen);
};