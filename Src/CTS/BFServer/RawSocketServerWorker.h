/////////////////////////////////////////////////////////////////////
// Class Creator Version 2.0.000 Copyrigth (C) Poul A. Costinsky 1994
///////////////////////////////////////////////////////////////////
// Header File RawSocketServerWorker.h
// class CWizRawSocketListener
//
// 16/07/1996 17:53                             Author: Poul
///////////////////////////////////////////////////////////////////

#ifndef __CWizRawSocketServerWorker_H
#define __CWizRawSocketServerWorker_H

//#include "ThreadDispatcher.h"
#include "RawSocket.h"
//#include "afxmt.h"				//CCriticalSection

class CWizMultiThreadedWorker
{
	public:
		CWizMultiThreadedWorker() {}
		virtual ~CWizMultiThreadedWorker() {}

		virtual void Prepare     () = 0;
		virtual BOOL WaitForData (HANDLE hShutDownEvent) = 0;
		virtual BOOL TreatData   (HANDLE hShutDownEvent, HANDLE hDataTakenEvent) = 0;
		virtual void CleanUp     () =  0;

	class Stack
		{
		public:
			Stack(CWizMultiThreadedWorker& rW)
				: m_rW(rW)
				{ m_rW.Prepare(); }
			~Stack()
				{ m_rW.CleanUp(); }
		private:
			CWizMultiThreadedWorker& m_rW;
		};
};

/////////////////////////////////////////////////////////////////////////////
// class CWizRawSocketServerWorker
class CWizRawSocketListener : public CWizMultiThreadedWorker
{
public:
	// Exceptions hierarchy
	struct Xeption {}; // common
	struct XCannotCreate : public Xeption {};  // createsocket fail
	struct XCannotSetHook : public Xeption {}; // WSASetBlockingHook fail
	struct XCannotSetHookEvent : public Xeption {}; // SetEven fail
	struct XCannotListen : public Xeption {};		// Listen fail
// Constructors:
	CWizRawSocketListener (int nPort = _EP_CONTROL_TCPIP_PORT1); // Constructor do almost nothing
// Destructor:
	virtual ~CWizRawSocketListener ();
public:
	int SetListenPort(int nPort);
// Operations:
// Virtual operations:
	// Interface for CWizThreadDispatcher
	virtual void Prepare();
	virtual BOOL WaitForData (HANDLE hShutDownEvent);
	virtual BOOL TreatData   (HANDLE hShutDownEvent, HANDLE hDataTakenEvent);
	virtual void CleanUp();
	// Pure virtual function - do something
	// with the socket connected to the client.
	// Should return TRUE if needs to continue I/O.
	virtual BOOL ReadWrite   (CWizReadWriteSocket& socket) = 0;
	void SetMsgWnd(HWND hMessageWnd = NULL);
//	CPowerFormationDoc		*m_pDoc;
	HWND m_hMessageWnd;
//	CCriticalSection m_TxBuffCritic;		//Tx Buffer Critical Section
//	CCriticalSection m_RxBuffCritic;		//Rx Buffer Critical Section
	
	BOOL QueDataPop(DWORD &dword, BYTE *data, DWORD &dataLength);
	BOOL QueDataIsEmpty();
	
protected:

// Implementation:
// Virtual implementation:
protected:
// Data Members:
	CWizSyncSocket*		m_pListenSocket;
	int					m_nPort;
	SOCKET				m_hAcceptedSocket;
};

/////////////////////////////////////////////////////////////////////////////
#endif // __CWizRawSocketServerWorker_H


