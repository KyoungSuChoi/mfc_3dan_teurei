#pragma once

#define MAX_QUEUE_LENGTH	1000

template <class T>
class CSBCCircularQueue : public CSBCSyncParent<CSBCCircularQueue<T>>
{
public:
	CSBCCircularQueue(VOID)
	{
		ZeroMemory(m_atDatas, sizeof(m_atDatas));
		m_dwHead = m_dwTail = 0;
		m_count = 0;
	}
	~CSBCCircularQueue(VOID){}

	UINT GetCount()
	{
		CSBCSyncObj SBCSync;

		return m_count;
	}

private:
	T		m_atDatas[MAX_QUEUE_LENGTH];
	DWORD	m_dwHead;
	DWORD	m_dwTail;

	UINT m_count;



public:
	BOOL Push(T tInputData)
	{
		CSBCSyncObj SBCSync;

		DWORD dwTempTail = (m_dwTail + 1) % MAX_QUEUE_LENGTH;

		if (dwTempTail == m_dwHead)
			return FALSE;

		CopyMemory(&m_atDatas[dwTempTail], &tInputData, sizeof(T));
		m_dwTail = dwTempTail;
		m_count++;
		return TRUE;
	}

	BOOL Pop(T& rtOutputData)
	{
		CSBCSyncObj SBCSync;

		if (m_dwHead == m_dwTail)
			return FALSE;

		DWORD dwTempHead = (m_dwHead + 1) % MAX_QUEUE_LENGTH;
		CopyMemory(&rtOutputData, &m_atDatas[dwTempHead], sizeof(T));
		m_dwHead = dwTempHead;
		m_count--;
		return TRUE;
	}

	BOOL GetIsEmpty(VOID)
	{
		CSBCSyncObj SBCSync;

		if (m_dwHead == m_dwTail)
			return TRUE;

		return FALSE;
	}
};