/***********************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Axis.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CAxis class is defined
***********************************************************************/

// Axis.h: interface for the CAxis class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AXIS_H__EBF948A0_EDEA_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_AXIS_H__EBF948A0_EDEA_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CAxis
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  The first settlement
//---------------------------------------------------------------------------
class AFX_EXT_CLASS CAxis  
{
////////////////
// Attributes //
////////////////

private:

	CString  m_strTitle;          // Title
	CString  m_strUnitNotation;   // Unit Notation
	float    m_fltUnitTransfer;   // 각 Transducer에서의 초기 획득 Data에서 선택된
	                              // Unit로 바꾸기 위해 곱하기/나누기 해 주어야 하는 값
	LOGFONT  m_Font;              // Label Font
	COLORREF m_LabelColor;        // Label Color
	float    m_fltMin;            // Minimum value of Scope
	float    m_fltMax;            // Maximum value of Scope
	float    m_fltGrid;           // Grid interval of Scope
	CList<fltPoint, fltPoint&> m_MinMaxBuffer;   // List of min/max value for the zoom operation
	BOOL	m_bShowGrid;


///////////////////////////////////////////////////////////
// Operations: "Constructor, destructor, and operator =" //
///////////////////////////////////////////////////////////

public:
	CAxis(LPCTSTR title = NULL, LPCTSTR notation = NULL, float transfer = 0.0f);
	virtual ~CAxis();
	CAxis& operator = (const CAxis& OrgAxis);


////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:

	//Grid 
	BOOL	IsShowGrid()	{	return m_bShowGrid;	}
	void	SetShowGrid(BOOL bShow = TRUE)	{	m_bShowGrid = bShow;}

	//	m_strTitle
	CString  GetTitle() { return m_strTitle; };
	void     SetTitle(LPCTSTR title) { m_strTitle = title; };

	//	m_strUnitNotation
	CString  GetUnitNotation() { return m_strUnitNotation; };
	void     SetUnitNotation(LPCTSTR notation) { m_strUnitNotation = notation; };

	//	m_fltUnitTransfer
	float    GetUnitTransfer() { return m_fltUnitTransfer; };
	void     SetUnitTransfer(float transfer) { m_fltUnitTransfer = transfer; };
	
	//	m_Font
	LOGFONT* GetFont() { return &m_Font; };

	//	m_LabelColor
	COLORREF GetLabelColor() { return m_LabelColor; };
	void     SetLabelColor(COLORREF color) { m_LabelColor = color; };

	//	m_fltMin, m_fltMax, m_fltGrid
	float    GetMin()  { return m_fltMin; };
	float    GetMax()  { return m_fltMax; };
	float    GetGrid() { return m_fltGrid; };
	void     SetMinMax(float min, float max, float grid) { m_fltMin=min; m_fltMax=max; m_fltGrid=grid;};
	
	//	m_MinMaxBuffer
	BOOL     IsMinMaxBufferEmpty() { return m_MinMaxBuffer.IsEmpty(); };
	fltPoint GetRecentMinMax()     { return m_MinMaxBuffer.RemoveTail(); };
	void     AddMinMaxBuffer(float min, float max);
	void     ResetMinMaxBuffer()   { m_MinMaxBuffer.RemoveAll(); };


///////////////////////////////////
// Operations: "Other functions" //
///////////////////////////////////

public:
	CString  GetPropertyTitle();
	float    ConvertUnit(float value) { if(m_fltUnitTransfer>0) return value/m_fltUnitTransfer; else return -1.0f*value*m_fltUnitTransfer; }
	float    ReverseUnit(float value) { if(m_fltUnitTransfer>0) return value*m_fltUnitTransfer; else return -1.0f*value/m_fltUnitTransfer; }
};

#endif // !defined(AFX_AXIS_H__EBF948A0_EDEA_11D4_88DF_006008CEDA07__INCLUDED_)
