// DataQueryCdn.cpp: implementation of the CDataQueryCondition class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataQueryCdn.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDataQueryCondition::CDataQueryCondition()
{
	m_iOutputOption = 0;
	m_iStartOption = 0;
	m_bModify       = FALSE;

//	m_dlgQuery.Create(IDD_DATA_QUERY_DIALOG);
}

CDataQueryCondition::~CDataQueryCondition()
{
	while(!m_YAxisList.IsEmpty())
	{
		CAxis* pAxis = m_YAxisList.RemoveTail();
		delete pAxis;
	}
}

BOOL CDataQueryCondition::GetUserSelection()
{
	//
	m_dlgQuery.SetQueryCondition(this);

//	m_dlgQuery.ShowWindow(SW_SHOW);
	if(IDOK == m_dlgQuery.DoModal()) 
	{
		m_strTestName = m_dlgQuery.m_strTestName;
		return TRUE;
	}
	
	return FALSE;
}

void CDataQueryCondition::ResetYAxisList()
{
	while(!m_YAxisList.IsEmpty())
	{
		CAxis* pAxis = m_YAxisList.RemoveTail();
		delete pAxis;
	}
}

BOOL CDataQueryCondition::UpdateCycleList(long* pCycle, int NumOfCycle)
{
	BOOL bChange = FALSE;

	if(m_llistCycle.GetCount()==NumOfCycle)
	{
		POSITION pos = m_llistCycle.GetHeadPosition();
		for(int i=0; i<NumOfCycle; i++)
		{
			if(pCycle[i]!=m_llistCycle.GetNext(pos))
			{
				bChange = TRUE;
				break;
			}
		}
	}
	else bChange = TRUE;

	if(bChange)
	{
		m_llistCycle.RemoveAll();
		for(int i=0; i<NumOfCycle; i++)
		{
			LONG cyc = pCycle[i];
			m_llistCycle.AddTail(cyc);
		}
	}

	return bChange;
}

CAxis* CDataQueryCondition::IsThereThisYAxis(LPCTSTR strTitle)
{
	POSITION pos = m_YAxisList.GetHeadPosition();
	while(pos)
	{
		CAxis* pAxis = m_YAxisList.GetNext(pos);
		if(pAxis->GetPropertyTitle().CompareNoCase(strTitle)==0) return pAxis;
	}
	return NULL;
}

BOOL CDataQueryCondition::GetModeList(CList<DWORD, DWORD&>* pdwListMode, LPCTSTR strChPath, LPCTSTR strYAxisTitle)
{
//	CPattern aPattern;
//	CFileFind afinder;
//	if(!afinder.FindFile(CString(strChPath)+"\\*.pat")) return FALSE;
//	afinder.FindNextFile();
//	if(!aPattern.Create(afinder.GetFilePath())) return FALSE;

	DWORD dwMode = 0;
/*	CScheduleData aPattern;
	CFileFind afinder;
	if(!afinder.FindFile(CString(strChPath)+"\\*.sch")) return FALSE;
	afinder.FindNextFile();
	if(!aPattern.SetSchedule(afinder.GetFilePath())) return FALSE;

	if(CString(strYAxisTitle).CompareNoCase("efficiency") == 0)
	{
		pdwListMode->AddTail(dwMode);
		return TRUE;
	}
*/
	dwMode = m_iOutputOption;
	pdwListMode->AddTail(dwMode);
	return TRUE;


/*	if(aPattern.GetStartMode()==CMode::MODE_CHARGE)
	{
		if((CString(strYAxisTitle).CompareNoCase("IR")==0)||
		   (CString(strYAxisTitle).CompareNoCase("OCV")==0))
		{
			switch(m_iOutputOption%100)
			{
			/////////////////////////////////////
			case  0:  // "ALL" - "Open Time 포함"
			case  1:  // "ALL" - "Open Time 제외"
			/////////////////////////////////////
				dwMode = MODE_CHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				dwMode = MODE_DISCHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				break;
			//////////////////////////////////////
			case 10:  // "충전" - "Open Time 포함"
			case 11:  // "충전" - "Open Time 제외"
			//////////////////////////////////////
				dwMode = MODE_CHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				break;
			//////////////////////////////////////
			case 20:  // "방전" - "Open Time 포함"
			case 21:  // "방전" - "Open Time 제외"
			//////////////////////////////////////
				dwMode = MODE_DISCHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				break;
			}
		}
		else
		{
			switch(m_iOutputOption%100)
			{
			/////////////////////////////////////
			case  0:  // "ALL" - "Open Time 포함"
			/////////////////////////////////////
				dwMode = MODE_CHARGE;
				pdwListMode->AddTail(dwMode);
				dwMode = MODE_CHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				dwMode = MODE_DISCHARGE;
				pdwListMode->AddTail(dwMode);
				dwMode = MODE_DISCHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				break;
			/////////////////////////////////////
			case  1:  // "ALL" - "Open Time 제외"
			/////////////////////////////////////
				dwMode = MODE_CHARGE;
				pdwListMode->AddTail(dwMode);
				dwMode = MODE_DISCHARGE;
				pdwListMode->AddTail(dwMode);
				break;
			//////////////////////////////////////
			case 10:  // "충전" - "Open Time 포함"
			//////////////////////////////////////
				dwMode = MODE_CHARGE;
				pdwListMode->AddTail(dwMode);
				dwMode = MODE_CHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				break;
			//////////////////////////////////////
			case 11:  // "충전" - "Open Time 제외"
			//////////////////////////////////////
				dwMode = MODE_CHARGE;
				pdwListMode->AddTail(dwMode);
				break;
			//////////////////////////////////////
			case 20:  // "방전" - "Open Time 포함"
			//////////////////////////////////////
				dwMode = MODE_DISCHARGE;
				pdwListMode->AddTail(dwMode);
				dwMode = MODE_DISCHARGE_OPEN;
				pdwListMode->AddTail(dwMode);
				break;
			//////////////////////////////////////
			case 21:  // "방전" - "Open Time 제외"
			//////////////////////////////////////
				dwMode = MODE_DISCHARGE;
				pdwListMode->AddTail(dwMode);
				break;
			}
		}
	}
	else if(aPattern.GetStartMode()==CMode::MODE_DISCHARGE)
	{
		BYTE byMode = 0;

		if((CString(strYAxisTitle).CompareNoCase("IR")==0)||
		   (CString(strYAxisTitle).CompareNoCase("OCV")==0))
		{
			switch(m_iOutputOption%100)
			{
			/////////////////////////////////////
			case  0:  // "ALL" - "Open Time 포함"
			case  1:  // "ALL" - "Open Time 제외"
			/////////////////////////////////////
				byMode = MODE_DISCHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				byMode = MODE_CHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				break;
			//////////////////////////////////////
			case 10:  // "충전" - "Open Time 포함"
			case 11:  // "충전" - "Open Time 제외"
			//////////////////////////////////////
				byMode = MODE_CHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				break;
			//////////////////////////////////////
			case 20:  // "방전" - "Open Time 포함"
			case 21:  // "방전" - "Open Time 제외"
			//////////////////////////////////////
				byMode = MODE_DISCHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				break;
			}
		}
		else
		{
			switch(m_iOutputOption%100)
			{
			/////////////////////////////////////
			case  0:  // "ALL" - "Open Time 포함"
			/////////////////////////////////////
				byMode = MODE_DISCHARGE;
				pbylistMode->AddTail(byMode);
				byMode = MODE_DISCHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				byMode = MODE_CHARGE;
				pbylistMode->AddTail(byMode);
				byMode = MODE_CHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				break;
			/////////////////////////////////////
			case  1:  // "ALL" - "Open Time 제외"
			/////////////////////////////////////
				byMode = MODE_DISCHARGE;
				pbylistMode->AddTail(byMode);
				byMode = MODE_CHARGE;
				pbylistMode->AddTail(byMode);
				break;
			//////////////////////////////////////
			case 10:  // "충전" - "Open Time 포함"
			//////////////////////////////////////
				byMode = MODE_CHARGE;
				pbylistMode->AddTail(byMode);
				byMode = MODE_CHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				break;
			//////////////////////////////////////
			case 11:  // "충전" - "Open Time 제외"
			//////////////////////////////////////
				byMode = MODE_CHARGE;
				pbylistMode->AddTail(byMode);
				break;
			//////////////////////////////////////
			case 20:  // "방전" - "Open Time 포함"
			//////////////////////////////////////
				byMode = MODE_DISCHARGE;
				pbylistMode->AddTail(byMode);
				byMode = MODE_DISCHARGE_OPEN;
				pbylistMode->AddTail(byMode);
				break;
			//////////////////////////////////////
			case 21:  // "방전" - "Open Time 제외"
			//////////////////////////////////////
				byMode = MODE_DISCHARGE;
				pbylistMode->AddTail(byMode);
				break;
			}
		}
	}
	else return FALSE;
*/
	return TRUE;
}

BOOL CDataQueryCondition::IsThereThisLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strYAxisTitle)
{
	BOOL bExit = FALSE;
	POSITION pos = m_YAxisList.GetHeadPosition();
	while(pos)
	{
		if(m_YAxisList.GetNext(pos)->GetTitle().CompareNoCase(strYAxisTitle)==0)
		{
			bExit = TRUE;
			break;
		}
	}
	if(!bExit) return FALSE;

	//
	if(m_strlistChPath.Find(strChPath) != NULL)
	{
		// X축이 Cycle인 경우,
/*		if (lCycle==0 && dwMode!=0)
		{
			switch(dwMode)
			{
			case MODE_CHARGE:
				if(((m_iOutputOption%100)/10)==0) return TRUE;
				if(((m_iOutputOption%100)/10)==1) return TRUE;
				break;
			case MODE_CHARGE_OPEN:
				if((CString(strYAxisTitle).CompareNoCase("IR")==0)&&
				   (CString(strYAxisTitle).CompareNoCase("OCV")==0))
				{
					if(((m_iOutputOption%100)/10)==0) return TRUE;
					if(((m_iOutputOption%100)/10)==1) return TRUE;
				}
				else
				{
					if((((m_iOutputOption%100)/10)==0)&&(m_iOutputOption%10==0)) return TRUE;
					if((((m_iOutputOption%100)/10)==1)&&(m_iOutputOption%10==0)) return TRUE;
				}
				break;
			case MODE_DISCHARGE:
				if(((m_iOutputOption%100)/10)==0) return TRUE;
				if(((m_iOutputOption%100)/10)==2) return TRUE;
				break;
			case MODE_DISCHARGE_OPEN:
				if((CString(strYAxisTitle).CompareNoCase("IR")==0)&&
				   (CString(strYAxisTitle).CompareNoCase("OCV")==0))
				{
					if(((m_iOutputOption%100)/10)==0) return TRUE;
					if(((m_iOutputOption%100)/10)==2) return TRUE;
				}
				else
				{
					if((((m_iOutputOption%100)/10)==0)&&(m_iOutputOption%10==0)) return TRUE;
					if((((m_iOutputOption%100)/10)==2)&&(m_iOutputOption%10==0)) return TRUE;
				}
				break;
			}
		}

		// X축이 Cycle이 아니고 Start Type이 "연속"인 경우,
		// 또는 X축이 Cycle이고 Y축이 Efficiency인 경우
		else if(lCycle == 0 && dwMode==0)
		{
			return TRUE;
		}
		// X축이 Cycle이 아니고 Start Type이 "영점시작"인 경우,
		
*/
		if(lCycle == 0)
		{
			TRACE("aa\n");
		}
		else //if(lCycle != 0)
		{
			if(m_llistCycle.Find(lCycle) != NULL)	return TRUE;
		}
	}
	return FALSE;
}

CString CDataQueryCondition::GetModeName(DWORD mode)
{
	CString strName;
	//
/*	switch(mode)
	{
	case MODE_CHARGE:
		strName = "충전";
		break;
	case MODE_CHARGE_OPEN:
		strName = "충휴";
		break;
	case MODE_DISCHARGE:
		strName = "방전";
		break;
	case MODE_DISCHARGE_OPEN:
		strName = "방휴";
		break;
	}
*/	//
	return strName;
}

WORD CDataQueryCondition::GetVTMeasurePoint()
{
	return MAKEWORD(m_dlgQuery.m_byVoltagePoint, m_dlgQuery.m_byTemperaturePoint);
}
