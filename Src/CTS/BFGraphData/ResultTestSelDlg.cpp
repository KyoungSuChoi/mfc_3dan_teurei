// ResultTestSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ResultTestSelDlg.h"
#include "FolderDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CResultTestSelDlg dialog


CResultTestSelDlg::CResultTestSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CResultTestSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CResultTestSelDlg)
	m_strFileFolder = _T("");
	//}}AFX_DATA_INIT
	m_bDateSortToggle = FALSE;
	m_bNameSortToggle = FALSE;
}


void CResultTestSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CResultTestSelDlg)
	DDX_Control(pDX, IDC_RESULT_TEST_LIST, m_ctrlTestList);
	DDX_Text(pDX, IDC_CUR_FOLDER, m_strFileFolder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CResultTestSelDlg, CDialog)
	//{{AFX_MSG_MAP(CResultTestSelDlg)
	ON_BN_CLICKED(IDC_DATA_FOLER_SEL_BUTTON, OnDataFolerSelButton)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_RESULT_TEST_LIST, OnColumnclickResultTestList)
	ON_NOTIFY(NM_DBLCLK, IDC_RESULT_TEST_LIST, OnDblclkResultTestList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResultTestSelDlg message handlers

void CResultTestSelDlg::InitTestListCtrl()
{
//	m_pChStsSmallImage = new CImageList;
//
//	//상태별 표시할 이미지 로딩
//	m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON,19,7,RGB(255,255,255));
//	m_ctrlTestList.SetImageList(m_pChStsSmallImage, LVSIL_SMALL);

	//
	m_ctrlTestList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);

	// Column 삽입
//	m_ctrlTestList.InsertColumn(0, "No", LVCFMT_LEFT, 50);
	m_ctrlTestList.InsertColumn(0, "Job name", LVCFMT_LEFT, 250);
	m_ctrlTestList.InsertColumn(1, "Workday", LVCFMT_LEFT, 250);
//	m_ctrlTestList.InsertColumn(3, "스케쥴명", LVCFMT_LEFT, 200);
	m_ctrlTestList.InsertColumn(2, "Number of enforcement", LVCFMT_LEFT, 100);
	

	
}

BOOL CResultTestSelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitTestListCtrl();

	m_strFileFolder = AfxGetApp()->GetProfileString("Control", "ResultDataPath", NULL);

	if(m_strFileFolder.IsEmpty())
	{
		TCHAR szLastSelDir[MAX_PATH];
		GetModuleFileName(AfxGetApp()->m_hInstance, szLastSelDir, MAX_PATH);
		m_strFileFolder = CString(szLastSelDir).Mid(0, CString(szLastSelDir).ReverseFind('\\')) + "\\Data";
	}

	LoadTestList();
	
	UpdateData(FALSE);
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CResultTestSelDlg::OnDataFolerSelButton() 
{
	// TODO: Add your control notification handler code here
	CFolderDialog dlg(m_strFileFolder);
	if( dlg.DoModal() == IDOK)
	{
		m_strFileFolder = dlg.GetPathName();
		AfxGetApp()->WriteProfileString("Control", "ResultDataPath", m_strFileFolder);
		UpdateData(FALSE);
		Refresh();
	}	
}

void CResultTestSelDlg::Refresh()
{
	UpdateData(TRUE);
	LoadTestList();
}

BOOL CResultTestSelDlg::LoadTestList()
{
	m_ctrlTestList.DeleteAllItems();
	ASSERT(m_ctrlTestList.GetItemCount() == 0);

	LVITEM lvItem; 
	::ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_IMAGE|LVIF_TEXT;
	
	CFileFind *pFinder, *pSubFinder;
	CString strName;
	pFinder = new CFileFind();
	strName.Format("%s\\*.*", m_strFileFolder);
	BOOL bWorking = pFinder->FindFile(strName);
	int nCount = 0;
	CTime createTime;
	SYSTEMTIME timeDest;
	COleDateTime oleTime;
	POSITION pos;

	int nFolderCount = 0;
	int nTotalFoler = 0;

	while (bWorking)
	{
		bWorking = pFinder->FindNextFile();
		nTotalFoler++;
	}

	//폴더가 하나도 없는 위치
	if(nTotalFoler <= 0)
	{
		delete pFinder;
		return FALSE;
	}

	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
	pProgressWnd->SetText("결과 목록을 검색중입니다.\n\n");
	pProgressWnd->Show();

	pSubFinder = new CFileFind();
	
	bWorking = pFinder->FindFile(strName);

	while (bWorking)
	{
		bWorking = pFinder->FindNextFile();
		
		pProgressWnd->SetPos(int((float)nFolderCount++/(float)nTotalFoler*100.0f));
		if(pProgressWnd->PeekAndPump() == FALSE)
			break;

		if(pFinder->IsDots())	continue;

		if (pFinder->IsDirectory())
		{
			//시행한 채널 수를 센다. 폴더수를 센다.
			strName.Format("%s\\%s", pFinder->GetFilePath(), FILE_FIND_FORMAT);
			BOOL bWorking1 = pSubFinder->FindFile(strName);
			int nChCount = 0;
			while (bWorking1)
			{
				bWorking1 = pSubFinder->FindNextFile();
				if(pSubFinder->IsDots())	continue;
				if (pSubFinder->IsDirectory())
				{
					nChCount++;
				}
			}
			//원하는 폴더가 아니면 M01Ch01 형태의 폴더명 검색
			if(nChCount <= 0)	continue;

			pProgressWnd->SetText("결과 목록을 검색중입니다.\n\n"+pFinder->GetFileName());

			//No
			lvItem.iItem = nCount++;
			lvItem.iSubItem = 0;
	//		lvItem.iImage = m_pDoc->m_pUnit[nI].GetNetworkState();
//			strName.Format("%d", nCount);
			strName.Format("%s", pFinder->GetFileName());
			lvItem.pszText = (LPSTR)(LPCTSTR)strName;
			m_ctrlTestList.InsertItem(&lvItem);
			m_ctrlTestList.SetItemData(lvItem.iItem, lvItem.iItem);
			
			//Name
//			m_ctrlTestList.SetItemText(lvItem.iItem, 1, pFinder->GetFileName());

			//현재 목록에 있으면 선택된 상태로 한다.
			for(int i=0; i<m_strSelTestList.GetCount(); i++)
			{
				pos = m_strSelTestList.GetHeadPosition();
				while(pos)
				{	
					strName = m_strSelTestList.GetNext(pos);
					if(strName == pFinder->GetFilePath())
					{
						m_ctrlTestList.SetItemState(lvItem.iItem,  LVIS_SELECTED, LVIS_SELECTED);
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////
			
			//파일 생성 시각 표시
			pFinder->GetCreationTime(createTime);
			createTime.GetAsSystemTime(timeDest);
			m_ctrlTestList.SetItemText(lvItem.iItem, 1, COleDateTime(timeDest).Format());

			strName.Format("%d", nChCount);
			m_ctrlTestList.SetItemText(lvItem.iItem, 2, strName);
			//////////////////////////////////////////////////////////////////////////
			

      }
	}

	delete pFinder;
	delete pSubFinder;

	//기본적으로 날짜 역순으로 정렬 
	m_ctrlTestList.SortItems(TestDateASCCompareProc,(LPARAM)(&m_ctrlTestList));
	for (int i=0;i < m_ctrlTestList.GetItemCount();i++)
	{
	   m_ctrlTestList.SetItemData(i, i);
	}

	pProgressWnd->Hide();
	delete pProgressWnd;
	pProgressWnd = NULL;

	return TRUE;
}

void CResultTestSelDlg::OnColumnclickResultTestList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	if(pNMListView->iSubItem ==0 )
	{
		if(m_bNameSortToggle) m_ctrlTestList.SortItems(TestNameASCCompareProc,(LPARAM)(&m_ctrlTestList));
		else                  m_ctrlTestList.SortItems(TestNameDECCompareProc,(LPARAM)(&m_ctrlTestList));
		
		for (int i=0;i < m_ctrlTestList.GetItemCount();i++)
		{
		   m_ctrlTestList.SetItemData(i, i);
		}
		m_bNameSortToggle=!m_bNameSortToggle;
	}
	else if(pNMListView->iSubItem == 1)
	{
		if(m_bDateSortToggle) m_ctrlTestList.SortItems(TestDateASCCompareProc,(LPARAM)(&m_ctrlTestList));
	
		else                  m_ctrlTestList.SortItems(TestDateDECCompareProc,(LPARAM)(&m_ctrlTestList));

		for (int i=0;i < m_ctrlTestList.GetItemCount();i++)
		{
		   m_ctrlTestList.SetItemData(i, i);
		}

		m_bDateSortToggle=!m_bDateSortToggle;
	}
	*pResult = 0;	
}

int CALLBACK CResultTestSelDlg::TestNameASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam2, 0);
   CString    strItem2 = pListCtrl->GetItemText(lParam1, 0);

   return strcmp(strItem2, strItem1);
}

int CALLBACK CResultTestSelDlg::TestNameDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam2, 0);
   CString    strItem2 = pListCtrl->GetItemText(lParam1, 0);

   return strcmp(strItem1, strItem2);
}

int CALLBACK CResultTestSelDlg::TestDateASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam1, 1);
   CString    strItem2 = pListCtrl->GetItemText(lParam2, 1);

   COleDateTime date1; date1.ParseDateTime(strItem1);
   COleDateTime date2; date2.ParseDateTime(strItem2);

   if     (date1>date2) return -1;
   else if(date1<date2) return  1;
   else                 return  0;
}

int CALLBACK CResultTestSelDlg::TestDateDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam1, 1);
   CString    strItem2 = pListCtrl->GetItemText(lParam2, 1);

   COleDateTime date1; date1.ParseDateTime(strItem1);
   COleDateTime date2; date2.ParseDateTime(strItem2);

   if     (date1>date2) return  1;
   else if(date1<date2) return -1;
   else                 return  0;
}

void CResultTestSelDlg::OnDblclkResultTestList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	UpdateSelTestList();
	CDialog::OnOK();

	*pResult = 0;
}



BOOL CResultTestSelDlg::UpdateSelTestList()
{
	POSITION pos = m_ctrlTestList.GetFirstSelectedItemPosition();
	int nItem;
	m_strSelTestList.RemoveAll();

	CString strTemp;
	while(pos)
	{
		nItem = m_ctrlTestList.GetNextSelectedItem(pos);
		strTemp.Format("%s\\%s", m_strFileFolder,m_ctrlTestList.GetItemText(nItem, 0));
		m_strSelTestList.AddTail(strTemp);

	}
	return TRUE;
}

CStringList * CResultTestSelDlg::GetSelList()
{
	return &m_strSelTestList;
}

void CResultTestSelDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateSelTestList()	;

	if(m_strSelTestList.GetCount() < 1)
	{
		MessageBox("선택된 결과가 없습니다.", "Error", MB_OK|MB_ICONSTOP);
		return;
	}
	CDialog::OnOK();
}

void CResultTestSelDlg::SetTestList(CStringList *pstrList)
{
	ASSERT(pstrList);

	m_strSelTestList.RemoveAll();

	CString strFileName;
	POSITION pos = pstrList->GetHeadPosition();
	while(pos)
	{	
		strFileName = pstrList->GetNext(pos);
		m_strSelTestList.AddTail(strFileName);
	}
}
