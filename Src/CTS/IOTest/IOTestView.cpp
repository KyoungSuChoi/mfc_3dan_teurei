// IOTestView.cpp : implementation of the CIOTestView class
//

#include "stdafx.h"
#include "IOTest.h"

#include "IOTestDoc.h"
#include "IOTestView.h"

#include "Mainfrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIOTestView

IMPLEMENT_DYNCREATE(CIOTestView, CFormView)

BEGIN_MESSAGE_MAP(CIOTestView, CFormView)
	//{{AFX_MSG_MAP(CIOTestView)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK33, OnCheck33)
	ON_BN_CLICKED(IDC_CHECK34, OnCheck34)
	ON_BN_CLICKED(IDC_CHECK35, OnCheck35)
	ON_BN_CLICKED(IDC_CHECK36, OnCheck36)
	ON_BN_CLICKED(IDC_CHECK37, OnCheck37)
	ON_BN_CLICKED(IDC_CHECK38, OnCheck38)
	ON_BN_CLICKED(IDC_CHECK39, OnCheck39)
	ON_BN_CLICKED(IDC_CHECK40, OnCheck40)
	ON_BN_CLICKED(IDC_CHECK41, OnCheck41)
	ON_BN_CLICKED(IDC_CHECK42, OnCheck42)
	ON_BN_CLICKED(IDC_CHECK43, OnCheck43)
	ON_BN_CLICKED(IDC_CHECK44, OnCheck44)
	ON_BN_CLICKED(IDC_CHECK45, OnCheck45)
	ON_BN_CLICKED(IDC_CHECK46, OnCheck46)
	ON_BN_CLICKED(IDC_CHECK47, OnCheck47)
	ON_BN_CLICKED(IDC_CHECK48, OnCheck48)
	ON_BN_CLICKED(IDC_CHECK49, OnCheck49)
	ON_BN_CLICKED(IDC_CHECK50, OnCheck50)
	ON_BN_CLICKED(IDC_CHECK51, OnCheck51)
	ON_BN_CLICKED(IDC_CHECK52, OnCheck52)
	ON_BN_CLICKED(IDC_CHECK53, OnCheck53)
	ON_BN_CLICKED(IDC_CHECK54, OnCheck54)
	ON_BN_CLICKED(IDC_CHECK55, OnCheck55)
	ON_BN_CLICKED(IDC_CHECK56, OnCheck56)
	ON_BN_CLICKED(IDC_CHECK57, OnCheck57)
	ON_BN_CLICKED(IDC_CHECK58, OnCheck58)
	ON_BN_CLICKED(IDC_CHECK59, OnCheck59)
	ON_BN_CLICKED(IDC_CHECK60, OnCheck60)
	ON_BN_CLICKED(IDC_CHECK61, OnCheck61)
	ON_BN_CLICKED(IDC_CHECK62, OnCheck62)
	ON_BN_CLICKED(IDC_CHECK63, OnCheck63)
	ON_BN_CLICKED(IDC_CHECK64, OnCheck64)
	ON_BN_CLICKED(IDC_CONNECT, OnConnect)
	ON_BN_CLICKED(IDC_DISCONNECT, OnDisconnect)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_AUTO_DEMO_START, OnAutoDemoStart)
	ON_BN_CLICKED(IDC_AUTO_DEMO_STOP, OnAutoDemoStop)
	ON_WM_KEYDOWN()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_REBOOT_BUTTON, OnRebootButton)
	ON_BN_CLICKED(IDC_HALT_BUTTON, OnHaltButton)
	ON_BN_CLICKED(IDC_KILL_PROCESS_BUTTON, OnKillProcessButton)
	ON_BN_CLICKED(IDC_CHECK73, OnCheck73)
	ON_BN_CLICKED(IDC_CHECK74, OnCheck74)
	ON_BN_CLICKED(IDC_CHECK75, OnCheck75)
	ON_BN_CLICKED(IDC_CHECK76, OnCheck76)
	ON_BN_CLICKED(IDC_CHECK77, OnCheck77)
	ON_BN_CLICKED(IDC_CHECK78, OnCheck78)
	ON_BN_CLICKED(IDC_CHECK79, OnCheck79)
	ON_BN_CLICKED(IDC_CHECK80, OnCheck80)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)

	ON_MESSAGE(WM_RECEIVE_DATA, OnReceive)
	ON_MESSAGE(WM_SOCKET_CONNECTED, OnServerConnected)
	ON_MESSAGE(WM_SOCKET_DISCONNECTED, OnServerDisConnected)
	ON_MESSAGE(WM_CMD_STANDBY, OnCmdStandBy)
	ON_MESSAGE(WM_UPDATE_COMPLITE, OnUpdateComplite)
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIOTestView construction/destruction

CIOTestView::CIOTestView()
	: CFormView(CIOTestView::IDD)
{
	//{{AFX_DATA_INIT(CIOTestView)
	m_nTotalCount = 0;
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	m_pClientSock = NULL;
	m_nPhase = PHASE_INIT_STATE;
	m_strExecuteCmd = "./ver64v22";
	m_strDir = "/project/PowerCell/current/hwTest/ver64v22";
	m_strRootPassword = "dusrn";
	m_strUserPassword = "dusrn";
	m_strUserID = "sbc";

	m_nInputAddress[0] = 0x504;
	m_nInputAddress[1] = 0x505;
	m_nInputAddress[2] = 0x506;
	m_nInputAddress[3] = 0x507;
	m_nInputAddress[4] = 0x508;
	m_nOutputAddress[0] =  0x511;
	m_nOutputAddress[1] =  0x512;
	m_nOutputAddress[2] =  0x513;
	m_nOutputAddress[3] =  0x514;
	m_nOutputAddress[4] =  0x515;

	ZeroMemory(m_inputData, sizeof(m_inputData));
	ZeroMemory(m_outputData, sizeof(m_outputData));
	ZeroMemory(m_outputMask, sizeof(m_outputMask));
	

	m_strIPAddress = "192.168.1.131";

	for(int i =0; i<40; i++)
	{
		
		m_strInputName[i].Format("Bit %d", i%8+1); 
		m_strOutputName[i].Format("Bit %d", i%8+1);

	}

	m_bFlash = FALSE;
	m_nTxCount = 0;
	m_nRxCount = 0;
//	m_bRx = FALSE;
//	m_bTx = FALSE;

	m_bOutMask = TRUE;
	m_timeOutFlag = FALSE;

	ZeroMemory(m_AutoSequence, sizeof(m_AutoSequence));
	m_nSeq = 0;
	m_nRepeatCount = 0;
	m_nRemainTime = 0;

	m_nMaxAutoSequence = 0;

	m_pTelnetSock = NULL;
	m_bTelnetCmdPhase = 0;

	m_nModuleState = EP_STATE_LINE_OFF;

	for(i =0 ; i<MAX_AUTO_SEQUENCE; i++)
	{
		m_AutoSequence[i].address = 0x511;
		m_AutoSequence[i].data = 0;
		m_AutoSequence[i].interval = 5000;
	}
}

CIOTestView::~CIOTestView()
{	
	if(m_pTelnetSock != NULL)
	{
		delete m_pTelnetSock;
		m_pTelnetSock = NULL;
	}


	if(m_pClientSock != NULL)
	{
		delete m_pClientSock;
		m_pClientSock = NULL;
	}
	::WSACleanup();

/*	if(m_IROCVPort.m_bInitedCard)
	{
		KillTimer(ID_LOCAL_IOCARD_SCAN_TIMER);
	}
*/
}

void CIOTestView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIOTestView)
	DDX_Control(pDX, IDC_RX_CHECK, m_rxState);
	DDX_Control(pDX, IDC_TX_CHECK, m_txState);
	DDX_Control(pDX, IDC_IPADDRESS1, m_ctrlIPAddress);
	DDX_Control(pDX, IDC_PROGRESS1, m_wndProgress);
	DDX_Text(pDX, IDC_REPEAT_COUNT, m_nTotalCount);
	//}}AFX_DATA_MAP
}

BOOL CIOTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CIOTestView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();


	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	if(LoadSetting() == FALSE)
	{
		AfxMessageBox("Can't find configuration File.(ioconfig-S.cfg)");
	}
	
	m_txState.SetImage( IDB_GREEN_BUTTON, 15);
	m_rxState.SetImage( IDB_GREEN_BUTTON, 15);

	for(int i =0; i<32; i++)
	{
		m_stateButton[i].SubclassWindow(GetDlgItem(IDC_CHECK1+i)->m_hWnd);
		m_stateButton[i].SetImage( IDB_RED_BUTTON, 15);
		m_stateButton[i].Depress(FALSE);

		m_stateButton[i].SetWindowText(m_strInputName[i]);
		GetDlgItem(IDC_CHECK33+i)->SetWindowText(m_strOutputName[i]);
	}

	for(i =32; i<40; i++)
	{
		m_stateButton[i].SubclassWindow(GetDlgItem(IDC_CHECK65+i-32)->m_hWnd);
		m_stateButton[i].SetImage( IDB_RED_BUTTON, 15);
		m_stateButton[i].Depress(FALSE);
		m_stateButton[i].SetWindowText(m_strInputName[i]);
	}

	for(i =0; i<8; i++)
	{
		GetDlgItem(IDC_CHECK73+i)->SetWindowText(m_strOutputName[32+i]);
	}


	CString strTemp;
	strTemp.Format("Input 1(0x%02x)", m_nInputAddress[0]);
	GetDlgItem(IDC_INPUT1_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Input 2(0x%02x)", m_nInputAddress[1]);
	GetDlgItem(IDC_INPUT2_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Input 3(0x%02x)", m_nInputAddress[2]);
	GetDlgItem(IDC_INPUT3_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Input 4(0x%02x)", m_nInputAddress[3]);
	GetDlgItem(IDC_INPUT4_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Input 5(0x%02x)", m_nInputAddress[4]);
	GetDlgItem(IDC_INPUT5_STATIC)->SetWindowText(strTemp);

	strTemp.Format("Output 1(0x%02x)", m_nOutputAddress[0]);
	GetDlgItem(IDC_OUTPUT1_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Output 2(0x%02x)", m_nOutputAddress[1]);
	GetDlgItem(IDC_OUTPUT2_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Output 3(0x%02x)", m_nOutputAddress[2]);
	GetDlgItem(IDC_OUTPUT3_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Output 4(0x%02x)", m_nOutputAddress[3]);
	GetDlgItem(IDC_OUTPUT4_STATIC)->SetWindowText(strTemp);
	strTemp.Format("Output 5(0x%02x)", m_nOutputAddress[4]);
	GetDlgItem(IDC_OUTPUT5_STATIC)->SetWindowText(strTemp);


	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD( 2, 2 );		//WinSock Version Greater than 2.0
	int  err = ::WSAStartup( wVersionRequested, &wsaData );
	if(err == 0)
	{
		//Get Server IP
		char	szServerIP[100]; 
		struct hostent *serverHostent;
		struct in_addr sin_addr;
		::gethostname( szServerIP,100 );
		serverHostent = ::gethostbyname( szServerIP );
		strcpy( szServerIP, serverHostent->h_name );
		memcpy( &sin_addr, serverHostent->h_addr, serverHostent->h_length);
		strcpy( szServerIP, inet_ntoa( sin_addr ) );

		m_strIPAddress= szServerIP;
	}
	m_strIPAddress = AfxGetApp()->GetProfileString("Settings", "IP_Address", m_strIPAddress);
	
/*	if(m_IROCVPort.InitIOCard() == FALSE)
	{
//		AfxMessageBox("Local IO Card를 찾을 수 없습니다. 원격 접속하여 사용 하십시요");
		GetDlgItem(IDC_CONNECT_STATIC)->SetWindowText("원격 접속하여 대기중입니다.");
	}
	else
	{
		GetDlgItem(IDC_CONNECT_STATIC)->SetWindowText("Local DIO Card가 동작중 입니다.");
		SetTimer(ID_LOCAL_IOCARD_SCAN_TIMER, 500, NULL);
	}
*/
	//App 에서 실행 
	SetControlLayout(GetDocument()->m_bSimpleTestMode);

	if(m_strProjectName.IsEmpty())	//1.2.0 이전 version 일 경우 
	{
		GetDlgItem(IDC_KILL_PROCESS_BUTTON)->EnableWindow(FALSE);
	}

	m_ctrlIPAddress.SetWindowText(m_strIPAddress);
	m_ctrlIPAddress.SetFieldFocus(3);
}

/////////////////////////////////////////////////////////////////////////////
// CIOTestView printing

BOOL CIOTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CIOTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CIOTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CIOTestView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CIOTestView diagnostics

#ifdef _DEBUG
void CIOTestView::AssertValid() const
{
	CFormView::AssertValid();
}

void CIOTestView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CIOTestDoc* CIOTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CIOTestDoc)));
	return (CIOTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIOTestView message handlers

LONG CIOTestView::OnReceive(UINT wParam,LONG lParam)
{
	if(!IsWindow(m_hWnd)) return FALSE;
	if(!IsWindowVisible()) return FALSE;
	
//	m_bRx = TRUE;	
	if(((CClientSocket *)lParam) == m_pTelnetSock)
	{
		LPCSTR data = (LPCSTR)wParam;
		CString log(data);

		TRACE("%s\n", log);
		
		return TRUE;
	}

	int nBytes = m_pClientSock->Receive(m_bBuf ,ioBuffSize );
	if(nBytes != SOCKET_ERROR)
	{
		while(GetLine(m_bBuf, nBytes) != TRUE);
		ProcessOptions();
		MessageReceived(m_strNormalText);
	}
	m_strLine.Empty();
	m_strResp.Empty();

	return TRUE;
}

BOOL CIOTestView::GetLine(unsigned char *bytes, int nBytes)
{
	BOOL bLine = FALSE;
	int ndx = 0;

	while ( bLine == FALSE && ndx < nBytes )
	{
		unsigned char ch = bytes[ndx];
		
		switch( ch )
		{
		case '\r': // ignore
			m_strLine += "\r\n"; //"CR";
			break;
		case '\n': // end-of-line
			m_strLine += '\n'; //"LF";
//			bLine = TRUE;
			break;
		default:   // other....
			m_strLine += ch;
			break;
		} 

		ndx ++;

		if (ndx == nBytes)
		{
			bLine = TRUE;
		}
	}
	return bLine;
}

void CIOTestView::ProcessOptions()
{
	CString strTemp;
	CString strOption;
	unsigned char ch;
	int ndx;
	int ldx;
	BOOL bScanDone = FALSE;

	strTemp = m_strLine;

//	TRACE("RX :: %s\n", strTemp);
	
	while(!strTemp.IsEmpty() && bScanDone != TRUE)
	{
		ndx = strTemp.Find(IAC);
		if(ndx != -1)
		{
			m_strNormalText += strTemp.Left(ndx);		//IAC 이전 
			ch = strTemp.GetAt(ndx + 1);				//IAC 다음 문자
			switch(ch)
			{
			case DO:
			case DONT:
			case WILL:
			case WONT:
				strOption		= strTemp.Mid(ndx, 3);	//IAC부터 3개의 문자 
				strTemp		= strTemp.Mid(ndx + 3);		//3개 이후의 문자 (새로운 문장)
				m_strNormalText	= strTemp.Left(ndx);	//IAC 이전 문자들 
				m_ListOptions.AddTail(strOption);
				break;
			case IAC:
				m_strNormalText	= strTemp.Left(ndx);
				strTemp		= strTemp.Mid(ndx + 1);
				break;
			case SB:
				m_strNormalText = strTemp.Left(ndx);
				ldx = strTemp.Find(SE);
				strOption	= strTemp.Mid(ndx, ldx);
				m_ListOptions.AddTail(strOption);
				strTemp		= strTemp.Mid(ldx);
				
				AfxMessageBox(strOption, MB_OK);
				break;
			}
		}
		else
		{
			m_strNormalText = strTemp;
			bScanDone = TRUE;
		}
	} 
	
	RespondToOptions();
}

void CIOTestView::RespondToOptions()
{
	CString strOption;
	
	while(!m_ListOptions.IsEmpty())
	{
		strOption = m_ListOptions.RemoveHead();

		ArrangeReply(strOption);
	}

	DispatchMessage(m_strResp);
	m_strResp.Empty();
}

void CIOTestView::ArrangeReply(CString strOption)
{

	unsigned char Verb;
	unsigned char Option;
	unsigned char Modifier;
	unsigned char ch;
	BOOL bDefined = FALSE;

	if(strOption.GetLength() < 3) return;

	Verb = strOption.GetAt(1);
	Option = strOption.GetAt(2);

	switch(Option)
	{
	case 1:	// Echo
	case 3: // Suppress Go-Ahead
		bDefined = TRUE;
		break;
	}

	m_strResp += IAC;

	if(bDefined == TRUE)
	{
		switch(Verb)
		{
		case DO:
			ch = WILL;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case DONT:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WILL:
			ch = DO;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WONT:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case SB:
			Modifier = strOption.GetAt(3);
			if(Modifier == SEND)
			{
				ch = SB;
				m_strResp += ch;
				m_strResp += Option;
				m_strResp += IS;
				m_strResp += IAC;
				m_strResp += SE;
			}
			break;
		}
	}

	else
	{
		switch(Verb)
		{
		case DO:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case DONT:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WILL:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WONT:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		}
	}
}

void CIOTestView::DispatchMessage(CString strText)
{
	ASSERT(m_pClientSock);
	m_pClientSock->Send(strText, strText.GetLength());

//	TRACE("TX :: %s\n", strText);

}

void CIOTestView::MessageReceived(LPCSTR pText)
{
//	TRACE("TX =>> %s\n", m_strSendedData);
//	TRACE("RX =>> %s\n", pText);
//	if( m_nPhase == PHASE_LOGIN_DONE)
//	{
//		BYTE data = ParsingData(pText);
		ParsingMessage(pText);
//	}
//	else
//	{

//	}
}


void CIOTestView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	CString strMsg;
	if(ID_RX_TIME_OUT_TIMER == nIDEvent)
	{
		if(m_timeOutFlag == TRUE)
		{
			KillTimer(ID_RX_TIME_OUT_TIMER);

			strMsg.Format("Can't connect to %s. Please check configuration.", m_strIPAddress);
			MessageBox(strMsg, "Connection Fail", MB_ICONSTOP|MB_OK);

			OnDisconnect();
		}
		m_timeOutFlag = TRUE;
	}
/*	else if(ID_DEMO_SEQUENCE_TIMER == nIDEvent)
	{
		COleDateTime timeElapse;
		timeElapse = m_startTime - COleDateTime::GetCurrentTime();
		GetDlgItem(IDC_ELAPS_TIME_STATIC)->SetWindowText(timeElapse.Format("%H:%M:%S"));


		if(m_nTotalCount > 0)
		{
			int time = m_nRemainTime - 5*(m_nSeq+m_nRepeatCount*m_nTotalSqCount);
			strMsg.Format("%02d:%02d:%02d", time/3600, (time%3600)/60, (time%3600)%60);
			
			GetDlgItem(IDC_REMAIN_TIME_STATIC)->SetWindowText(strMsg);
		}

		strMsg.Format("wr %x %x", m_AutoSequence[m_nSeq].address, m_AutoSequence[m_nSeq].data);
		SendData(strMsg);	

		if(m_IROCVPort.m_bInitedCard)
			m_IROCVPort.WriteToPort(m_AutoSequence[m_nSeq].address-m_nOutputAddress[0]+1, m_AutoSequence[m_nSeq].data);

		m_nSeq++;


		if(m_nTotalSqCount < m_nSeq)
		{
			m_nSeq = 0;
			m_nRepeatCount++;
			strMsg.Format("%d", m_nRepeatCount);
			GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(strMsg);
		}
		
		if(m_nTotalCount > 0 && m_nRepeatCount >= m_nTotalCount)
		{
			OnAutoDemoStop();
		}

	}	//Local IO Card 일 경우 
*/	else if(ID_AUTO_SEQUENCE_TIMER == nIDEvent )
	{
		COleDateTime timeElapse;
		timeElapse = m_startTime - COleDateTime::GetCurrentTime();
		GetDlgItem(IDC_ELAPS_TIME_STATIC)->SetWindowText(timeElapse.Format("%H:%M:%S"));


		TRACE("\t\t>> Elapsed Time : %.f Sec\n", timeElapse/1000.0f);
		if(m_nTotalCount > 0)
		{
			int time = m_nRemainTime - 5*(m_nSeq+m_nRepeatCount*m_nTotalSqCount);
			strMsg.Format("%02d:%02d:%02d", time/3600, (time%3600)/60, (time%3600)%60);
			
			GetDlgItem(IDC_REMAIN_TIME_STATIC)->SetWindowText(strMsg);
		}

		strMsg.Format("wr %x %x", m_AutoSequence[m_nSeq].address, m_AutoSequence[m_nSeq].data);
		SendData(strMsg);	

		KillTimer(nIDEvent);
		SetTimer(nIDEvent, m_AutoSequence[m_nSeq].interval, NULL);

		m_nSeq++;

		if(m_nTotalSqCount < m_nSeq)
		{
			m_nSeq = 0;
			m_nRepeatCount++;
			strMsg.Format("%d 회", m_nRepeatCount);
			GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(strMsg);
		}
		
		if(m_nTotalCount > 0 && m_nRepeatCount >= m_nTotalCount)
		{
			OnAutoDemoStop();
		}
	}
	else if(ID_LOCAL_IOCARD_SCAN_TIMER == nIDEvent)
	{
//		UpdateState(m_nInputAddress[0], m_IROCVPort.ReadFromPort(1));
//		UpdateState(m_nInputAddress[1], m_IROCVPort.ReadFromPort(2));
//		UpdateState(m_nInputAddress[2], m_IROCVPort.ReadFromPort(3));
//		UpdateState(m_nInputAddress[3], m_IROCVPort.ReadFromPort(4));
	}

	CFormView::OnTimer(nIDEvent);
}

int CIOTestView::SendData(CString strData)
{
//	m_bTx = TRUE;

	if(m_pClientSock == NULL)	return FALSE;
	
	int nSize = strData.GetLength();
	if(nSize > 0)
	{
		strData += "\n";
		nSize = strData.GetLength();
		if( nSize != m_pClientSock->Send(strData, nSize))
		{		
			OnDisconnect();
			TRACE("MessageSend Fail\n");
			return FALSE;
		}
		m_strSendedData = strData;
	}
	return TRUE;
}



BYTE CIOTestView::ParsingData(CString msg)
{
	CString strLogName, strDirTemp;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	
	strLogName.Format("%s\\%s", strDirTemp, LOG_FILE_NAME);

	BYTE data = 0;
	m_timeOutFlag = FALSE;

	TRACE("%s\n", msg);
	FILE *fp = fopen(LOG_FILE_NAME, "at+");
	if(fp != NULL)
	{
		fprintf(fp, "%s\n", msg);
		fclose(fp);
	}
	
	CString subMsg;

	switch(m_nPhase)
	{
	case PHASE_INIT_STATE:		//ID 입력
		subMsg = msg.Right(7);
		if(subMsg == "login: ")
		{
			SendData(m_strUserID);
			m_nPhase++;
			m_wndProgress.SetPos(15);
		}
		break;
	case PHASE_USER_PASSWORD:		//Password 입력
		subMsg = msg.Right(10);
		if(subMsg == "Password: ")
		{
			SendData(m_strUserPassword);
			m_nPhase++;
			m_wndProgress.SetPos(30);
		}
		break;
	case PHASE_CHANGE_ROOT:		//root 계정 변경 
		subMsg = msg.Right(m_strUserID.GetLength()+3);
		if(subMsg == m_strUserID+"]$ ")
		{
			SendData("su - root");
			m_nPhase++;
			m_wndProgress.SetPos(45);
		}
		break;

	case PHASE_ROOT_PASSWORD:		//root 암호 입력  
		subMsg = msg.Right(10);
		if(subMsg == "Password: ")
		{
			SendData(m_strRootPassword);
			m_nPhase++;
			m_wndProgress.SetPos(60);
		}
		break;

	case PHASE_CHANGE_DIRECTORY:		//Directory 이동   
		subMsg = msg.Right(7);
		if(subMsg == "root]# ")
		{
			SendData("cd "+m_strDir);
			m_nPhase++;
			m_wndProgress.SetPos(75);
		}
		break;

	case PHASE_EXECUTE:		//실행     
		subMsg = msg.Right(m_strCurdir.GetLength()+3);
		if(subMsg == ""+m_strCurdir+"]# ")
		{
			SendData(m_strExecuteCmd);
			m_nPhase++;
			m_wndProgress.SetPos(90);
		}
		break;
		
	case PHASE_LOGIN_DONE:		     
		m_wndProgress.SetPos(100);
		Connected();
		m_nPhase++;

	default:
		{
			if(msg == ">> ")
			{
				SendRequest();
			}

			if(msg.GetLength() < 10)	return 0;
		
			CString strHead, strData, strAddress;
			strHead = msg.Left(8);

			int index;
			if(strHead == "get data")
			{
				index = msg.ReverseFind(' ');
				if(index > strHead.GetLength())
				{
					strData = msg.Mid(index+1);
					msg = msg.Left(index);
					index = msg.ReverseFind(' ');
					if(index >= strHead.GetLength())
					{
						strAddress = msg.Mid(index+1);
					}
				}
				TRACE("Data Read : 0x%02x => 0x%02x\n", Hex2Dec(strAddress), Hex2Dec(strData));
				UpdateState(Hex2Dec(strAddress), Hex2Dec(strData));
			}
			data = Hex2Dec(strData);
			
		}
	}
	return data;
}

void CIOTestView::UpdateState(UINT address, BYTE data)
{
	if(address == m_nInputAddress[0])
	{
		m_inputData[0] = data;
		for(int i =0; i< 8; i++)
		{
			m_stateButton[i].Depress(data & (0x01<<i));
//			((CButton *)GetDlgItem(IDC_CHECK1+i))->SetCheck(data & (0x01<<i));
		}

	} 
	else if(address == m_nInputAddress[1])
	{
		m_inputData[1] = data;
		for(int i =0; i< 8; i++)
		{
			m_stateButton[i+8].Depress(data & (0x01<<i));
//			((CButton *)GetDlgItem(IDC_CHECK9+i))->SetCheck(data & (0x01<<i));
		}
	}
	else if(address == m_nInputAddress[2])
	{
		m_inputData[2] = data;
		for(int i =0; i< 8; i++)
		{
			m_stateButton[i+16].Depress(data & (0x01<<i));
//			((CButton *)GetDlgItem(IDC_CHECK17+i))->SetCheck(data & (0x01<<i));
		}
	}
	else if(address == m_nInputAddress[3])
	{
		m_inputData[3] = data;
		for(int i =0; i< 8; i++)
		{
			m_stateButton[i+24].Depress(data & (0x01<<i));
//			((CButton *)GetDlgItem(IDC_CHECK25+i))->SetCheck(data & (0x01<<i));
		}
	}
	else if(address == m_nInputAddress[4])
	{
		m_inputData[4] = data;
		for(int i =0; i< 8; i++)
		{
			m_stateButton[i+32].Depress(data & (0x01<<i));
//			((CButton *)GetDlgItem(IDC_CHECK25+i))->SetCheck(data & (0x01<<i));
		}
	}

}

void CIOTestView::OnCheck33() 
{
	// TODO: Add your control notification handler code here
	//LG 4호 Jig Down
	CString strMsg;
	BOOL bCheck = ((CButton *)GetDlgItem(IDC_CHECK33))->GetCheck();

	if(m_bOutMask)
	{
		//Jig Up Down 시는 반드시 Latch가 Close 상태 이어야 한다.
		if( (m_inputData[0] & 0x02) != 0x02 || (m_inputData[0] & 0x08) != 0x08 ||	//잠김 Sensor 감지 안됨
			(m_inputData[0] & 0x01) == 0x01 || (m_inputData[0] & 0x04) == 0x04)		//풀림 Sensor 감지
		{
		
			strMsg.Format("Lach close sensor is not detected.(0x%02x)", m_inputData[0]);
			MessageBox(strMsg, "Warning", MB_ICONSTOP); 
			RollBackOutState(0);
			return;
		}
	}
	WriteData(m_nOutputAddress[0]);
}

void CIOTestView::OnCheck34() 
{
	// TODO: Add your control notification handler code here
	//LG 4호 Latch Close
	CString strMsg;
	
	//Latch 동작시는 Jig가 반드시 Up 상태여야 한다.
	if(m_bOutMask)
	{
		if( (m_inputData[0] & 0x10) == 0x10 ||	//Down Sensor 감지 
			(m_inputData[0] & 0x40) != 0x40 || (m_inputData[0] & 0x80) != 0x80)	//풀림 Sensor 미감지
		{
		
			strMsg.Format("Jig up sensor is not detected.(0x%02x)", m_inputData[0]);
//			if(MessageBox(strMsg, "경고", MB_YESNO|MB_ICONQUESTION) != IDYES)
//			{	
				MessageBox(strMsg, "Warning", MB_ICONSTOP);
				RollBackOutState(0);
				return;
//			}
		}
	}
	WriteData(m_nOutputAddress[0]);
	
}

void CIOTestView::OnCheck35() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[0]);
	
}

void CIOTestView::OnCheck36() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[0]);
	
}

void CIOTestView::OnCheck37() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[0]);
	
}

void CIOTestView::OnCheck38() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[0]);
	
}

void CIOTestView::OnCheck39() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[0]);
	
}

void CIOTestView::OnCheck40() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[0]);
	
}

void CIOTestView::OnCheck41() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);	
}

void CIOTestView::OnCheck42() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);
	
}

void CIOTestView::OnCheck43() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);
	
}

void CIOTestView::OnCheck44() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);
	
}

void CIOTestView::OnCheck45() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);
	
}

void CIOTestView::OnCheck46() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);
	
}

void CIOTestView::OnCheck47() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);
	
}

void CIOTestView::OnCheck48() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[1]);
	
}

void CIOTestView::OnCheck49() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);
	
}

void CIOTestView::OnCheck50() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);
	
}

void CIOTestView::OnCheck51() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);
	
}

void CIOTestView::OnCheck52() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);

}

void CIOTestView::OnCheck53() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);
	
}

void CIOTestView::OnCheck54() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);
	
}

void CIOTestView::OnCheck55() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);
	
}

void CIOTestView::OnCheck56() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[2]);
	
}

void CIOTestView::OnCheck57() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);

}

void CIOTestView::OnCheck58() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);
	
}

void CIOTestView::OnCheck59() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);
	
}

void CIOTestView::OnCheck60() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);

}

void CIOTestView::OnCheck61() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);
	
}

void CIOTestView::OnCheck62() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);
	
}

void CIOTestView::OnCheck63() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);
	
}

void CIOTestView::OnCheck64() 
{
	// TODO: Add your control notification handler code here
	WriteData(m_nOutputAddress[3]);
}

void CIOTestView::WriteData(UINT address)
{
	UpdateData(TRUE);

	BYTE data = 0;
	CString strData;
	BOOL	bCheck;

	if(address == m_nOutputAddress[0])
	{
		for(int i =0; i< 8; i++)
		{
			bCheck = ((CButton *)GetDlgItem(IDC_CHECK33+i))->GetCheck();
			if(bCheck)
			{
				data |= (0x01 << i);
//				GetDlgItem(IDC_CHECK33+i)->GetDC()->SetBkColor(RGB(255, 0, 0));
//				GetDlgItem(IDC_CHECK33+i)->GetDC()->SetBkMode(TRANSPARENT);
				
			}
		}
/*		if(StateCheck(1, data) == FALSE)
		{
			RollBackOutState(0);
			MessageBox("실행 시킬수 있는 상태가 아닙니다.", "상태 오류", MB_ICONSTOP|MB_OK);
			return;
		}
*/
		m_outputData[0] = data;
//		strData.Format("wr %x %x", m_nOutputAddress[0], m_outputData[0]);
//		SendData(strData);
	}
	else if(address == m_nOutputAddress[1])
	{
		for(int i =0; i< 8; i++)
		{
			bCheck = ((CButton *)GetDlgItem(IDC_CHECK41+i))->GetCheck();
			if(bCheck)
				data |= (0x01 << i);
		}
/*		if(StateCheck(2, data) == FALSE)
		{
			MessageBox("실행 시킬수 있는 상태가 아닙니다.", "상태 오류", MB_ICONSTOP|MB_OK);
			return;
		}
*/		m_outputData[1] = data;
//		strData.Format("wr %x %x", m_nOutputAddress[1], m_outputData[1]);
//		SendData(strData);
	}
	else if(address == m_nOutputAddress[2])
	{
		for(int i =0; i< 8; i++)
		{
			bCheck = ((CButton *)GetDlgItem(IDC_CHECK49+i))->GetCheck();
			if(bCheck)
				data |= (0x01 << i);
		}
/*		if(StateCheck(3, data) == FALSE)
		{
			MessageBox("실행 시킬수 있는 상태가 아닙니다.", "상태 오류", MB_ICONSTOP|MB_OK);
			return;
		}
*/		m_outputData[2] = data;
//		strData.Format("wr %x %x", m_nOutputAddress[2], m_outputData[2]);
//		SendData(strData);
	}
	else if(address == m_nOutputAddress[3])
	{
		for(int i =0; i< 8; i++)
		{
			bCheck = ((CButton *)GetDlgItem(IDC_CHECK57+i))->GetCheck();
			if(bCheck)
				data |= (0x01 << i);
		}
/*		if(StateCheck(4, data) == FALSE)
		{
			MessageBox("실행 시킬수 있는 상태가 아닙니다.", "상태 오류", MB_ICONSTOP|MB_OK);
			return;
		}
*/
		m_outputData[3] = data;
//		strData.Format("wr %x %x", m_nOutputAddress[3], m_outputData[3]);
	}
	else if(address == m_nOutputAddress[4])
	{
		for(int i =0; i< 8; i++)
		{
			bCheck = ((CButton *)GetDlgItem(IDC_CHECK73+i))->GetCheck();
			if(bCheck)
				data |= (0x01 << i);
		}
		m_outputData[4] = data;
	}


	strData.Format("wr %x %x", address, data);
	if(!strData.IsEmpty())
	{
		SendData(strData);
	}

	//Local IO Card
//	if(m_IROCVPort.m_bInitedCard)
//		m_IROCVPort.WriteToPort(address-m_nOutputAddress[0]+1, data);

}

void CIOTestView::Connected()
{
	m_wndProgress.SetPos(100);
	GetDlgItem(IDC_CONNECT_STATIC)->SetWindowText("Connection Complited");

	if(GetDocument()->m_bUseOutPut)
	{
		//Input 표기 check
		for(int i =IDC_CHECK1; i<= IDC_CHECK32; i++)
		{
			GetDlgItem(i)->EnableWindow(TRUE);
		}
		//Input 표기 check
		for( i =IDC_CHECK65; i<= IDC_CHECK72; i++)
		{
			GetDlgItem(i)->EnableWindow(TRUE);
		}
		//Out put 표기 
		for( i =IDC_CHECK33; i<= IDC_CHECK64; i++)
		{
			GetDlgItem(i)->EnableWindow(m_outputMask[i-IDC_CHECK33]);
		}

		for( i =IDC_CHECK73; i<= IDC_CHECK80; i++)
		{
			GetDlgItem(i)->EnableWindow(m_outputMask[32+i-IDC_CHECK73]);
		}

	}
	else		//not use output
	{
		for(int i =0; i< 32; i++)
			GetDlgItem(IDC_CHECK1+i)->EnableWindow(TRUE);

	}
}

void CIOTestView::OnConnect()
{
	UpdateData(TRUE);

	BYTE address[4];
	m_ctrlIPAddress.GetAddress(address[0], address[1], address[2], address[3]);
	m_strIPAddress.Format("%d.%d.%d.%d", address[0], address[1], address[2], address[3]);

	//Update Module state 
	CString msg;
	if(UpdateModuleState() == EP_STATE_RUN)
	{
		msg.Format("설비[%s]는 현재 작업 중 입니다. 작업 진행 중에 OutPut 동작을 하게 되면 현재 진행 중인 작업이 정상 동작 하지 않을 수 있습니다.", m_strIPAddress);
		AfxMessageBox(msg);
	//		if(MessageBox(msg, "모듈 실행 종료", MB_YESNO|MB_ICONQUESTION) == IDNO)
	//			return;
	}

	ConnectModule(m_strIPAddress);

	AfxGetApp()->WriteProfileString("Settings", "IP_Address", m_strIPAddress);
}

LONG CIOTestView::OnServerConnected(UINT pParam, LONG lParam)
{
	if((CClientSocket *)lParam == m_pTelnetSock)
	{
		return TRUE;
	}


	GetDlgItem(IDC_CONNECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_IPADDRESS1)->EnableWindow(FALSE);
	GetDlgItem(IDC_DISCONNECT)->EnableWindow(TRUE);

	GetDlgItem(IDC_AUTO_DEMO_START)->EnableWindow(TRUE);
	GetDlgItem(IDC_AUTO_DEMO_STOP)->EnableWindow(FALSE);

	AfxGetMainWnd()->SetWindowText("IOTest "+ m_strIPAddress);

	m_nPhase = PHASE_INIT_STATE;
	SetTimer(ID_RX_TIME_OUT_TIMER, 5000, NULL);		//Time Out Timer
	m_timeOutFlag = FALSE;
	return TRUE;
}

LONG CIOTestView::OnServerDisConnected(UINT pParam, LONG lParam)
{
	if((CClientSocket *)lParam == m_pTelnetSock)
	{
		return TRUE;
	}

	OnDisconnect();

	AfxMessageBox("Host is disconnected.");
	return 0;
}

void CIOTestView::OnDisconnect() 
{
	// TODO: Add your control notification handler code here

	CString strData;
	
	KillTimer(ID_RX_TIME_OUT_TIMER);

	//접속이 완료된 상태 
	if(m_nPhase == PHASE_LOGIN_DONE)
	{
		ZeroMemory(m_outputData, sizeof(m_outputData));

		//Jig Down 이고 Latch Close 시 
		if(m_bOutMask && ((m_inputData[0] & 0x02) == 0x02 || ((m_inputData[0] & 0x08)) == 0x08) &&
			(m_inputData[0] & 0x10) == 0x10)
		{
			strData.Format("wr %x 2", m_nOutputAddress[0]);
			SendData(strData);
			Sleep(5000);
		}

		strData.Format("wr %x 0", m_nOutputAddress[0]);
		SendData(strData);
		strData.Format("wr %x 0", m_nOutputAddress[1]);
		SendData(strData);
		strData.Format("wr %x 0", m_nOutputAddress[2]);
		SendData(strData);
		strData.Format("wr %x 0", m_nOutputAddress[3]);
		SendData(strData);
		strData.Format("wr %x 0", m_nOutputAddress[4]);
		SendData(strData);
	}
	
	if(m_pClientSock != NULL)
	{
		m_pClientSock->Close();
		delete m_pClientSock ;
		m_pClientSock = NULL;
	}

	GetDlgItem(IDC_CONNECT)->EnableWindow(TRUE);
	GetDlgItem(IDC_IPADDRESS1)->EnableWindow(TRUE);
	GetDlgItem(IDC_DISCONNECT)->EnableWindow(FALSE);
	
	for(int i =IDC_CHECK1; i< IDC_CHECK64; i++)
		GetDlgItem(i)->EnableWindow(FALSE);

	for(i =IDC_CHECK65; i< IDC_CHECK72; i++)
		GetDlgItem(i)->EnableWindow(FALSE);

	for(i =IDC_CHECK73; i< IDC_CHECK80; i++)
		GetDlgItem(i)->EnableWindow(FALSE);

	for(i=0; i<40; i++)
		m_stateButton[i].Depress(FALSE);
	
	m_wndProgress.SetPos(0);
	GetDlgItem(IDC_CONNECT_STATIC)->SetWindowText("disconnectioned");
	AfxGetMainWnd()->SetWindowText("IOTest");

	m_txState.Depress(FALSE);
	m_rxState.Depress(FALSE);

}

BOOL CIOTestView::LoadSetting()
{
	CStdioFile	file;

	CFileException* e;
	e = new CFileException;

	if(!file.Open("ioconfig-S.cfg", CFile::modeRead|CFile::shareDenyNone))	
	{
		
#ifdef _DEBUG
		afxDump << "Configuration File Open Fail" << "\n";
#endif
		return FALSE;
	}
	
	CString buff;
	BOOL	bEOF;

	TRY
	{
		while(TRUE)
		{
			bEOF = file.ReadString(buff);
			if(!bEOF)	break;

			if(!buff.IsEmpty())
			{
				ParsingConfig(buff);
			}
		}
		
	}
	CATCH (CFileException, e)
	{
		AfxMessageBox(e->m_cause);
		file.Close();
		return FALSE;
	}
	END_CATCH

	file.Close();	
	return TRUE;
}

void CIOTestView::ParsingConfig(CString strConfig)
{
	if(strConfig.IsEmpty())		return;

	int a, b;
	a = strConfig.Find('[');
	b = strConfig.ReverseFind(']');

	if(b > 0 && a >= b)	return;

	CString strTitle, strData;
	strTitle = strConfig.Mid(a+1, b-a-1);

	a = strConfig.ReverseFind('=');

	strData = strConfig.Mid( a+1 );

	strData.TrimLeft(" ");
	strData.TrimRight(" ");

/*	//이전 공백 삭제 
	for(int i =0; i<strData.GetLength(); i++)
	{
		if(strData[0] == ' ')
			strData.Delete(0);
		else
			break;
	}

	//이후 공백 삭제 
	for(i = strData.GetLength()-1; i>=0; i--)
	{
		if(strData[i] == ' ')
			strData.Delete(i);
		else 
			break;
	}
*/
	if(strData.IsEmpty())	return;
	
	if(strTitle == "Input port 1")		{	if(Hex2Dec(strData) > 0)	m_nInputAddress[0] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 2")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[1] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 3")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[2] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 4")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[3] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 5")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[4] = Hex2Dec(strData);	}
	else if(strTitle == "Output port 1"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[0] = Hex2Dec(strData); }
	else if(strTitle == "Output port 2"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[1] = Hex2Dec(strData); }
	else if(strTitle == "Output port 3"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[2] = Hex2Dec(strData); }
	else if(strTitle == "Output port 4"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[3] = Hex2Dec(strData); }
	else if(strTitle == "Output port 5"){	if(Hex2Dec(strData) > 0)	m_nOutputAddress[4] = Hex2Dec(strData); }
	else if(strTitle == "Out Mask")		{   m_bOutMask = atol(strData);	}
	else if(strTitle == "Execute")		{	m_strExecuteCmd = strData;	}
	else if(strTitle == "Directory")	
	{	
		strData.TrimRight("/ ");
		m_strDir = strData;
		if(m_strDir.GetLength() >= 1)
		{
			if(m_strDir.Right(1) == "/")		//가장 마지막에 '/'가 있으면 
			{
				m_strCurdir = m_strDir.Left(m_strDir.GetLength()-1);
			}
			else
				m_strCurdir = m_strDir;
			
			int nIdnex = m_strCurdir.ReverseFind('/');
			if(nIdnex > 0)
			{
				m_strCurdir = m_strCurdir.Mid(nIdnex+1);
			}
		}	
	}
	
	if(strTitle == "Project Name")	{   m_strProjectName = strData;		}
	else if(strTitle == "root Password"){	m_strRootPassword = strData;	}
	else if(strTitle == "Login ID")		{	m_strUserID =  strData;			}
	else if(strTitle == "Login Password"){	m_strUserPassword =  strData;	}
	else if(strTitle == "Input1_1 Name"){	m_strInputName[0] =  strData;	}
	else if(strTitle == "Input1_2 Name"){	m_strInputName[1] =  strData;	}
	else if(strTitle == "Input1_3 Name"){	m_strInputName[2] =  strData;	}
	else if(strTitle == "Input1_4 Name"){	m_strInputName[3] =  strData;	}
	else if(strTitle == "Input1_5 Name"){	m_strInputName[4] =  strData;	}
	else if(strTitle == "Input1_6 Name"){	m_strInputName[5] =  strData;	}
	else if(strTitle == "Input1_7 Name"){	m_strInputName[6] =  strData;	}
	else if(strTitle == "Input1_8 Name"){	m_strInputName[7] =  strData;	}
	else if(strTitle == "Input2_1 Name"){	m_strInputName[8] =  strData;	}
	else if(strTitle == "Input2_2 Name"){	m_strInputName[9] =  strData;	}
	else if(strTitle == "Input2_3 Name"){	m_strInputName[10] =  strData;	}
	else if(strTitle == "Input2_4 Name"){	m_strInputName[11] =  strData;	}
	else if(strTitle == "Input2_5 Name"){	m_strInputName[12] =  strData;	}
	else if(strTitle == "Input2_6 Name"){	m_strInputName[13] =  strData;	}
	else if(strTitle == "Input2_7 Name"){	m_strInputName[14] =  strData;	}
	else if(strTitle == "Input2_8 Name"){	m_strInputName[15] =  strData;	}
	else if(strTitle == "Input3_1 Name"){	m_strInputName[16] =  strData;	}
	else if(strTitle == "Input3_2 Name"){	m_strInputName[17] =  strData;	}
	else if(strTitle == "Input3_3 Name"){	m_strInputName[18] =  strData;	}
	else if(strTitle == "Input3_4 Name"){	m_strInputName[19] =  strData;	}
	else if(strTitle == "Input3_5 Name"){	m_strInputName[20] =  strData;	}
	else if(strTitle == "Input3_6 Name"){	m_strInputName[21] =  strData;	}
	else if(strTitle == "Input3_7 Name"){	m_strInputName[22] =  strData;	}
	else if(strTitle == "Input3_8 Name"){	m_strInputName[23] =  strData;	}
	else if(strTitle == "Input4_1 Name"){	m_strInputName[24] =  strData;	}
	else if(strTitle == "Input4_2 Name"){	m_strInputName[25] =  strData;	}
	else if(strTitle == "Input4_3 Name"){	m_strInputName[26] =  strData;	}
	else if(strTitle == "Input4_4 Name"){	m_strInputName[27] =  strData;	}
	else if(strTitle == "Input4_5 Name"){	m_strInputName[28] =  strData;	}
	else if(strTitle == "Input4_6 Name"){	m_strInputName[29] =  strData;	}
	else if(strTitle == "Input4_7 Name"){	m_strInputName[30] =  strData;	}
	else if(strTitle == "Input4_8 Name"){	m_strInputName[31] =  strData;	}
	else if(strTitle == "Input5_1 Name"){	m_strInputName[32] =  strData;	}
	else if(strTitle == "Input5_2 Name"){	m_strInputName[33] =  strData;	}
	else if(strTitle == "Input5_3 Name"){	m_strInputName[34] =  strData;	}
	else if(strTitle == "Input5_4 Name"){	m_strInputName[35] =  strData;	}
	else if(strTitle == "Input5_5 Name"){	m_strInputName[36] =  strData;	}
	else if(strTitle == "Input5_6 Name"){	m_strInputName[37] =  strData;	}
	else if(strTitle == "Input5_7 Name"){	m_strInputName[38] =  strData;	}
	else if(strTitle == "Input5_8 Name"){	m_strInputName[39] =  strData;	}
	
	if(strTitle == "Output1_1 Name"){	m_strOutputName[0] =  strData;	}
	else if(strTitle == "Output1_2 Name"){	m_strOutputName[1] =  strData;	}
	else if(strTitle == "Output1_3 Name"){	m_strOutputName[2] =  strData;	}
	else if(strTitle == "Output1_4 Name"){	m_strOutputName[3] =  strData;	}
	else if(strTitle == "Output1_5 Name"){	m_strOutputName[4] =  strData;	}
	else if(strTitle == "Output1_6 Name"){	m_strOutputName[5] =  strData;	}
	else if(strTitle == "Output1_7 Name"){	m_strOutputName[6] =  strData;	}
	else if(strTitle == "Output1_8 Name"){	m_strOutputName[7] =  strData;	}
	else if(strTitle == "Output2_1 Name"){	m_strOutputName[8] =  strData;	}
	else if(strTitle == "Output2_2 Name"){	m_strOutputName[9] =  strData;	}
	else if(strTitle == "Output2_3 Name"){	m_strOutputName[10] =  strData;	}
	else if(strTitle == "Output2_4 Name"){	m_strOutputName[11] =  strData;	}
	else if(strTitle == "Output2_5 Name"){	m_strOutputName[12] =  strData;	}
	else if(strTitle == "Output2_6 Name"){	m_strOutputName[13] =  strData;	}
	else if(strTitle == "Output2_7 Name"){	m_strOutputName[14] =  strData;	}
	else if(strTitle == "Output2_8 Name"){	m_strOutputName[15] =  strData;	}
	else if(strTitle == "Output3_1 Name"){	m_strOutputName[16] =  strData;	}
	else if(strTitle == "Output3_2 Name"){	m_strOutputName[17] =  strData;	}
	else if(strTitle == "Output3_3 Name"){	m_strOutputName[18] =  strData;	}
	else if(strTitle == "Output3_4 Name"){	m_strOutputName[19] =  strData;	}
	else if(strTitle == "Output3_5 Name"){	m_strOutputName[20] =  strData;	}
	else if(strTitle == "Output3_6 Name"){	m_strOutputName[21] =  strData;	}
	else if(strTitle == "Output3_7 Name"){	m_strOutputName[22] =  strData;	}
	else if(strTitle == "Output3_8 Name"){	m_strOutputName[23] =  strData;	}
	else if(strTitle == "Output4_1 Name"){	m_strOutputName[24] =  strData;	}
	else if(strTitle == "Output4_2 Name"){	m_strOutputName[25] =  strData;	}
	else if(strTitle == "Output4_3 Name"){	m_strOutputName[26] =  strData;	}
	else if(strTitle == "Output4_4 Name"){	m_strOutputName[27] =  strData;	}
	else if(strTitle == "Output4_5 Name"){	m_strOutputName[28] =  strData;	}
	else if(strTitle == "Output4_6 Name"){	m_strOutputName[29] =  strData;	}
	else if(strTitle == "Output4_7 Name"){	m_strOutputName[30] =  strData;	}
	else if(strTitle == "Output4_8 Name"){	m_strOutputName[31] =  strData;	}
	else if(strTitle == "Output5_1 Name"){	m_strOutputName[32] =  strData;	}
	else if(strTitle == "Output5_2 Name"){	m_strOutputName[33] =  strData;	}
	else if(strTitle == "Output5_3 Name"){	m_strOutputName[34] =  strData;	}
	else if(strTitle == "Output5_4 Name"){	m_strOutputName[35] =  strData;	}
	else if(strTitle == "Output5_5 Name"){	m_strOutputName[36] =  strData;	}
	else if(strTitle == "Output5_6 Name"){	m_strOutputName[37] =  strData;	}
	else if(strTitle == "Output5_7 Name"){	m_strOutputName[38] =  strData;	}
	else if(strTitle == "Output5_8 Name"){	m_strOutputName[39] =  strData;	}
	else if(strTitle == "OutMask1_1")	{ m_outputMask[0] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_2"){ m_outputMask[1] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_3"){ m_outputMask[2] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_4"){ m_outputMask[3] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_5"){ m_outputMask[4] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_6"){ m_outputMask[5] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_7"){ m_outputMask[6] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask1_8"){ m_outputMask[7] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask2_1"){ m_outputMask[8] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_2"){ m_outputMask[9] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_3"){ m_outputMask[10] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_4"){ m_outputMask[11] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_5"){ m_outputMask[12] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_6"){ m_outputMask[13] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_7"){ m_outputMask[14] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask2_8"){ m_outputMask[15] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask3_1"){ m_outputMask[16] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_2"){ m_outputMask[17] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_3"){ m_outputMask[18] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_4"){ m_outputMask[19] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_5"){ m_outputMask[20] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_6"){ m_outputMask[21] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_7"){ m_outputMask[22] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask3_8"){ m_outputMask[23] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask4_1"){ m_outputMask[24] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_2"){ m_outputMask[25] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_3"){ m_outputMask[26] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_4"){ m_outputMask[27] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_5"){ m_outputMask[28] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_6"){ m_outputMask[29] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_7"){ m_outputMask[30] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask4_8"){ m_outputMask[31] = (BYTE)atol(strData);	}

	else if(strTitle == "OutMask5_1"){ m_outputMask[32] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_2"){ m_outputMask[33] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_3"){ m_outputMask[34] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_4"){ m_outputMask[35] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_5"){ m_outputMask[36] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_6"){ m_outputMask[37] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_7"){ m_outputMask[38] = (BYTE)atol(strData);	}
	else if(strTitle == "OutMask5_8"){ m_outputMask[39] = (BYTE)atol(strData);	}

	
	//////////////////////////////////////////////////////////
	// AutoSequence Configuration (Ver. 1.1.5.1)			//
	//////////////////////////////////////////////////////////
	else if(strTitle == "Auto_Sequence_Manualloading")
	{
		m_nMaxAutoSequence = atol(strData);
		if( m_nMaxAutoSequence > MAX_AUTO_SEQUENCE )
		{
			m_nMaxAutoSequence = 0;
			return;
		}
	}
	else if( strTitle.Find("Auto_Sequence_Address_") >= 0 )
	{
		int nIndex = atol(strTitle.Mid(22, strTitle.GetLength()-22))-1;
		if( nIndex < 0 || nIndex >= m_nMaxAutoSequence )
		{
			m_nMaxAutoSequence = 0;
			return;
		}
		if(Hex2Dec(strData) > 0)	
			m_AutoSequence[nIndex].address = Hex2Dec(strData);
	}

	else if( strTitle.Find("Auto_Sequence_Data_")  >= 0 )
	{
		int nIndex = atol(strTitle.Mid(19, strTitle.GetLength()-19))-1;
		if( nIndex < 0 || nIndex > m_nMaxAutoSequence )
		{
			m_nMaxAutoSequence = 0;
			return;
		}
		if(Hex2Dec(strData) > 0)	
			m_AutoSequence[nIndex].data = Hex2Dec(strData);
	}

	else if( strTitle.Find("Auto_Sequence_Interval_")  >= 0 )
	{
		int nIndex = atol(strTitle.Mid(23, strTitle.GetLength()-23))-1;
		if( nIndex < 0 || nIndex > m_nMaxAutoSequence )
		{
			return;
		}
		if(Hex2Dec(strData) > 0)	
			m_AutoSequence[nIndex].interval = atoi((LPSTR)(LPCTSTR)strData);
	}

	else if(strTitle == "Auto_Sequence_Autoloading")
	{
		m_AutoSequence[0].address = 0x511;
		m_AutoSequence[0].data = 0x29;

		m_AutoSequence[1].address = 0x511;
		m_AutoSequence[1].data = 0x25;

		m_AutoSequence[2].address = 0x511;
		m_AutoSequence[2].data = 0x15;
		
		m_AutoSequence[3].address = 0x511;
		m_AutoSequence[3].data = 0x25;

		m_AutoSequence[4].address = 0x511;
		m_AutoSequence[4].data = 0x29;

		m_AutoSequence[5].address = 0x511;
		m_AutoSequence[5].data = 0x2A;
	}
}


//주어진 Bit를 Set 시키고자 할 경우 Input Sensor를 확인 위치에 있어야 한다.
BOOL CIOTestView::StateCheck(int nPort, BYTE data)
{
	if(nPort < 1 || nPort > 4)	return	FALSE;

	for(int i=0; i<8; i++)
	{
		if(data & (0x01 << i))
		{
			if((m_inputData[nPort-1] & m_outputMask[i]) !=  m_outputMask[i+(nPort-1)*8])	return FALSE;
		}
	}

	return TRUE;
}

void CIOTestView::RollBackOutState(int nPort)
{
	if(nPort < 0 || nPort >= 4)	return;

	for(int i =0; i<8; i++)
	{
		((CButton *)GetDlgItem(IDC_CHECK33+(nPort)*8+i))->SetCheck(m_outputData[nPort] & (0x01 << i));
	}
}

//Parsing Data to Line String 
void CIOTestView::ParsingMessage(LPCSTR pText)
{
	CString buff(pText);
	if(buff.IsEmpty())	return;

	int nIndex;
	while((nIndex = buff.Find("\r\n")) >= 0)
	{
		m_strLineData += buff.Left(nIndex);
		buff = buff.Mid(nIndex+3);

		if(!m_strLineData.IsEmpty())
		{
			ParsingData(m_strLineData);
			m_strLineData.Empty();
		}
	}

	if(buff.GetLength() > 0)
	{
		m_strLineData += buff;
		ParsingData(m_strLineData);
	}
}

//Make Hexa String to Decimal
int CIOTestView::Hex2Dec(CString hexData)
{
	int data = 0;

	int val = 0, val1;
	int count = 0;
	hexData.MakeLower();

	//remove pre and post space
	int nSize = hexData.GetLength();
	for(int i = 0; i < nSize; i++)		//이전 공백 삭제 
	{
		if(hexData[0] == ' ')
		{
			hexData.Delete(0);
		}
		else
		{
			break;
		}
	}

	nSize = hexData.GetLength();
	for(i = nSize-1; i >= 0 ; i--)		//이후  공백 삭제 
	{
		if(hexData[i] == ' ')
		{
			hexData.Delete(i);
		}
		else
		{
			break;
		}
	}

	nSize = hexData.GetLength();
	for(i=nSize-1; i>=0; i--)
	{
		val = 0;
		if( hexData[i] >= '0' && hexData[i] <= '9')	//'0'~'1'
		{
			val = (hexData[i] - '0') << (4*count++);
		}
		else
		{
			val1 = hexData[i] - 'a';
			if(val1 >= 0 && val1 <= 5)	//'a'~'f', 'A'~'F'
			{
				val +=((10 + val1)<<(4*count++));
			}
			else	//	
			{
				return 0;
			}
		}
		data += val; 
	}
	return data;

}

void CIOTestView::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	OnDisconnect() ;
	CFormView::OnClose();
}

BOOL CIOTestView::ConnectModule(CString strIP)
{
	m_ctrlIPAddress.SetWindowText(strIP);
	m_ctrlIPAddress.SetFieldFocus(3);

	unlink(LOG_FILE_NAME);

	m_strIPAddress = strIP;

	ZeroMemory(m_inputData, sizeof(m_inputData));
	ZeroMemory(m_outputData, sizeof(m_outputData));
	RollBackOutState(0);
	RollBackOutState(1);
	RollBackOutState(2);
	RollBackOutState(3);
	
	if(m_pClientSock != NULL)
	{
		//if(m_pClientSock->m_hSocket)
		m_pClientSock->Close();
		delete m_pClientSock ;
		m_pClientSock = NULL;
	}

	m_pClientSock = new CTelnetSock(this->m_hWnd);
	BOOL bOK;
	if(m_pClientSock != NULL)
	{
		bOK = m_pClientSock->Create();
		if(bOK == TRUE)
		{
			m_pClientSock->AsyncSelect(FD_READ | FD_WRITE | FD_CLOSE | FD_CONNECT | FD_OOB);
			m_pClientSock->Connect(strIP, 23);
			GetDocument()->SetTitle(strIP);
			Sleep(90);
		}
		else
		{
			ASSERT(FALSE);  //Did you remember to call AfxSocketInit()?
			delete m_pClientSock;
			m_pClientSock = NULL;
		}
	}
	else
	{
		AfxMessageBox("Could not create new socket",MB_OK);	
	}
	GetDlgItem(IDC_CONNECT_STATIC)->SetWindowText("Try connect to "+strIP);
	
	return TRUE;
}

BOOL CIOTestView::SendRequest()
{
	CString cmd;
	
	static bPress = TRUE;
	static count = 0;

	m_txState.Depress(bPress);
	m_rxState.Depress(!bPress);

	switch(count)
	{
	case 0:	cmd.Format("get %x", m_nInputAddress[0]);		count++;	break;
	case 1:	cmd.Format("get %x", m_nInputAddress[1]);		count++;	break;
	case 2:	cmd.Format("get %x", m_nInputAddress[2]);		count++;	break;
	case 3:	cmd.Format("get %x", m_nInputAddress[3]);		count++;	break;
	case 4:	cmd.Format("get %x", m_nInputAddress[4]);		count = 0;	break;
	}
	bPress = !bPress;
	return SendData(cmd);		
}

void CIOTestView::OnAutoDemoStart() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
//	SetTimer(ID_DEMO_SEQUENCE_TIMER, 5000, NULL);
	m_nRepeatCount = 0;
	m_nTotalSqCount = 0;

	SetTimer(ID_AUTO_SEQUENCE_TIMER, m_AutoSequence[m_nTotalSqCount].interval, NULL);

	while(m_AutoSequence[m_nTotalSqCount].address > 0 && m_nTotalSqCount < MAX_AUTO_SEQUENCE)
	{
		m_nTotalSqCount++;
	}

	m_startTime = COleDateTime::GetCurrentTime();
	GetDlgItem(IDC_START_TIME_STATIC)->SetWindowText(m_startTime.Format());

	CString msg;
	if(m_nTotalCount > 0)
	{
		m_nRemainTime = 5*m_nTotalSqCount*m_nTotalCount;
		msg.Format("%02d:%02d:%02d", m_nRemainTime/3600, (m_nRemainTime%3600)/60, (m_nRemainTime%3600)%60);
		GetDlgItem(IDC_REMAIN_TIME_STATIC)->SetWindowText(msg);
	}
	else
	{
		m_nRemainTime = 0;
		GetDlgItem(IDC_REMAIN_TIME_STATIC)->SetWindowText("00:00:00");
	}

	GetDlgItem(IDC_AUTO_DEMO_START)->EnableWindow(FALSE);
	GetDlgItem(IDC_AUTO_DEMO_STOP)->EnableWindow(TRUE);

}

void CIOTestView::OnAutoDemoStop() 
{
	// TODO: Add your control notification handler code here
//	KillTimer(ID_DEMO_SEQUENCE_TIMER);
	KillTimer(ID_AUTO_SEQUENCE_TIMER);

	GetDlgItem(IDC_AUTO_DEMO_START)->EnableWindow(TRUE);
	GetDlgItem(IDC_AUTO_DEMO_STOP)->EnableWindow(FALSE);

}


void CIOTestView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
//	TRACE("Key Down\n");
	CFormView::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CIOTestView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(IDC_IPADDRESS1 == ::GetDlgCtrlID(pMsg->hwnd)
 			&& pMsg->wParam == VK_RETURN)
		{
			if(m_pClientSock == NULL)
				OnConnect();
		}
	}

	return CFormView::PreTranslateMessage(pMsg);
}


//실행 모드에 따라 Control 들을 재배치 한다.
// bSensorTest == TRUE => Simple Sensor Test Mode 
// 1) input 모든 Sensor 상태와 Jig Down/Latch/Stacker Out 이외에는 모드 Hide 시킴 
// 2) 창의 크기를 줄임

void CIOTestView::SetControlLayout(BOOL bSensorTest)
{
	//Simple Sensor Test Mode
	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
	CRect rect;

// 	if(bSensorTest)
// 	{
// 		for(int i =IDC_CHECK37; i<= IDC_CHECK64; i++)
// 			GetDlgItem(i)->ShowWindow(SW_HIDE);
// 
// 		GetDlgItem(IDC_CHECK73)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CHECK74)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CHECK75)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CHECK76)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CHECK77)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CHECK78)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CHECK79)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CHECK80)->ShowWindow(SW_HIDE);
// 
// 		
// 		GetDlgItem(IDC_OUTPUT2_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_OUTPUT3_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_OUTPUT4_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_OUTPUT5_STATIC)->ShowWindow(SW_HIDE);
// 
// 		GetDlgItem(IDC_DISCONNECT)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_IPADDRESS1)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_REPEAT_COUNT)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_AUTO_DEMO_START)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_AUTO_DEMO_STOP)->ShowWindow(SW_HIDE);
// 	
// 		GetDlgItem(IDC_CONNECT)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_COUNT_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_START_TIME_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_REMAIN_TIME_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_ELAPS_TIME_STATIC)->ShowWindow(SW_HIDE);
// 	
// 		GetDlgItem(IDC_CONNECTION_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_IP_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_REPEAT_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_TOTAL_COUNT__STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_CUT_COUNT_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_TIMES_STATIC)->ShowWindow(SW_HIDE);
// 
// 		GetDlgItem(IDC_START_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_REMAIN_STATIC)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_ELAPSE_STATIC)->ShowWindow(SW_HIDE);
// 
// 		GetDlgItem(IDC_OUT_STATIC)->ShowWindow(SW_HIDE);
// 
// 		GetDlgItem(IDC_OUTPUT1_STATIC)->MoveWindow(60, 115, 100, 30);
// 		GetDlgItem(IDC_CHECK33)->MoveWindow(60, 150, 100, 23);
// 		GetDlgItem(IDC_CHECK34)->MoveWindow(60, 180, 100, 23);
// 		GetDlgItem(IDC_CHECK35)->MoveWindow(60, 210, 100, 23);
// 		GetDlgItem(IDC_CHECK36)->MoveWindow(60, 240, 100, 23);
// 
// 		GetDlgItem(IDC_KILL_PROCESS_BUTTON)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_REBOOT_BUTTON)->ShowWindow(SW_HIDE);
// 		GetDlgItem(IDC_HALT_BUTTON)->ShowWindow(SW_HIDE);
// 
// 
// 		
// 
// 		rect.top = 145;
// 		rect.left = 110;
// 		rect.right = 1105;
// 		rect.bottom = 490;
// 		pMain->MoveWindow(rect);
// 		
// 		//최상위 Window로 유지
// 		::SetWindowPos(pMain->m_hWnd, HWND_TOPMOST, 110, 145, 1105, 490, SWP_NOMOVE|SWP_NOSIZE);
// 	}
/*	else
	{
		rect.top = 145;
		rect.left = 110;
		rect.right = 960;
		rect.bottom = 725;
	
	}
*/	
}

void CIOTestView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	CFormView::ShowScrollBar(SB_BOTH,FALSE);
	// TODO: Add your message handler code here
}


BOOL CIOTestView::ConnectTelnet()
{
	if(m_pTelnetSock != NULL)
	{
		DisConnectTelnet();
	}

	CString strData;	//m_strExecuteCmd
	m_pTelnetSock = new CClientSocket(this->m_hWnd);

	m_pTelnetSock->SetUser(m_strUserID, m_strUserPassword);
	m_pTelnetSock->SetRootPassword(m_strRootPassword);
//	strData.Format("/project/%s/current/hwTest/ver64v22/ver64v22", m_strProjectName);
	strData = m_strDir;
	m_pTelnetSock->SetIOServerName(strData);

	BOOL bOK;
	if(m_pTelnetSock != NULL)
	{
		bOK = m_pTelnetSock->Create();
		if(bOK == TRUE)
		{
			m_pTelnetSock->AsyncSelect(FD_READ | FD_WRITE | FD_CLOSE | FD_CONNECT | FD_OOB);
			if(m_pTelnetSock->Connect(m_strIPAddress, 23) == 0)
			{
			}
			Sleep(90);
		}
		else
		{
			delete m_pTelnetSock;
			m_pTelnetSock = NULL;
			ASSERT(FALSE);  //Did you remember to call AfxSocketInit()?
		}
	}
	else
	{
		AfxMessageBox("Could not create new telnet socket",MB_OK);	
		return FALSE;
	}
	
	m_bTelnetCmdPhase = 0;
	return TRUE;
}

void CIOTestView::DisConnectTelnet()
{
	if(m_pTelnetSock != NULL)
	{
		m_pTelnetSock->Close();
		delete m_pTelnetSock;
		m_pTelnetSock = NULL;
	}
	m_bTelnetCmdPhase = 0;
}

void CIOTestView::OnRebootButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString msg;

	BYTE address[4];
	m_ctrlIPAddress.GetAddress(address[0], address[1], address[2], address[3]);
	m_strIPAddress.Format("%d.%d.%d.%d", address[0], address[1], address[2], address[3]);

	UpdateModuleState();
	if(m_nModuleState == EP_STATE_RUN || m_nModuleState == EP_STATE_PAUSE)
	{
		msg.Format("설비[%s]는 Run 상태이거나 잠시멈춤 상태 입니다. 그래도 Rebooting 하시겠습니까?", m_strIPAddress);
		if(MessageBox(msg, "모듈 실행 종료", MB_YESNO|MB_ICONQUESTION) == IDNO)
			return;
	}

	msg.Format("설비[%s]를 재실행 시키시겠습니까?", m_strIPAddress);
	if(MessageBox(msg, "설비 재실행", MB_ICONQUESTION|MB_YESNO) == IDYES)
	{
		m_nTelnetMode = TELNET_CMD_MODE_REBOOT;	//Reboot Mode
		if(ConnectTelnet() == TRUE)
		{
		}
	}	
}

void CIOTestView::OnHaltButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString msg;

	BYTE address[4];
	m_ctrlIPAddress.GetAddress(address[0], address[1], address[2], address[3]);
	m_strIPAddress.Format("%d.%d.%d.%d", address[0], address[1], address[2], address[3]);

	UpdateModuleState();
	if(m_nModuleState == EP_STATE_RUN || m_nModuleState == EP_STATE_PAUSE)
	{
		msg.Format("설비[%s]는 Run 상태이거나 잠시멈춤 상태 입니다. 그래도 전원을 Off 하시겠습니까?", m_strIPAddress);
		if(MessageBox(msg, "모듈 실행 종료", MB_YESNO|MB_ICONQUESTION) == IDNO)
			return;
	}

	msg.Format("설비[%s] 전원을 Off 하시겠습니까?", m_strIPAddress);
	if(MessageBox(msg, "설비 전원 Off", MB_ICONQUESTION|MB_YESNO) == IDYES)
	{
		m_nTelnetMode = TELNET_CMD_MODE_HALT;	//halt Mode
		if(ConnectTelnet() == TRUE)
		{
		}
	}	
}

LONG CIOTestView::OnUpdateComplite(UINT wParam,LONG lParam)
{
	CString msg;
	if(m_nTelnetMode == TELNET_CMD_MODE_REBOOT)
	{
		msg.Format("설비[%s]를 재실행 시키는 동안 잠시(약 1분 30초) 기다려 주십시요.", m_strIPAddress);
	}
	else if(m_nTelnetMode == TELNET_CMD_MODE_HALT)
	{
		msg.Format("설비[%s]를 전원을 Off 시키는 동안 잠시(약 30초) 기다려 주십시요.", m_strIPAddress);
	}
	else if(m_nTelnetMode == TELENT_CMD_MODE_KILL_PROCESS)
	{
		msg.Format("설비[%s]를 모듈 동작을 종료 하였습니다.", m_strIPAddress);
	}
	AfxMessageBox(msg);		
	return 0;
}

LONG CIOTestView::OnCmdStandBy(UINT wParam, LONG lParam)
{
	ASSERT(m_pTelnetSock);

	if((CClientSocket *)lParam != m_pTelnetSock)	return 0;


	LPCSTR pText = (LPCSTR)wParam;
	CString cmdResult(pText);
	CString strCmdLine;

	//Reboot Mode
	//BTS가 돌고 있으면 먼저 Kill 하여야 함
	if(m_nTelnetMode == TELNET_CMD_MODE_REBOOT)
	{
		switch(m_bTelnetCmdPhase)
		{
		case 0:		//Move Direcotry 
			GetDlgItem(IDC_REBOOT_BUTTON)->EnableWindow(FALSE);
			OnDisconnect();
			GetDlgItem(IDC_CONNECT_STATIC)->SetWindowText("System Rebooting...");
			strCmdLine.Format("cd /project/%s/current/form/App/PSKill", m_strProjectName);
			m_pTelnetSock->SendData(strCmdLine);
			m_bTelnetCmdPhase++;
			break;
		case 1:		//Execute kill Process
			strCmdLine = "./kill";
			m_pTelnetSock->SendData(strCmdLine);
			m_bTelnetCmdPhase++;
			break;
		case 2:		//Send System Reboot Command
			m_pTelnetSock->SendSystemReboot();
			m_bTelnetCmdPhase++;
			PostMessage(WM_UPDATE_COMPLITE, 0, 0);
			GetDlgItem(IDC_REBOOT_BUTTON)->EnableWindow(TRUE);
			break;
		}
	}

	//Power Off Mode //
	//BTS가 돌고 있으면 먼저 Kill 하여야 함
	if(m_nTelnetMode == TELNET_CMD_MODE_HALT)
	{
		switch(m_bTelnetCmdPhase)
		{
		case 0:		//Move Direcoty
			GetDlgItem(IDC_CONNECT_STATIC)->SetWindowText("System Power Off");
			GetDlgItem(IDC_HALT_BUTTON)->EnableWindow(FALSE);
			OnDisconnect();
			strCmdLine.Format("cd /project/%s/current/form/App/PSKill", m_strProjectName);
			m_pTelnetSock->SendData(strCmdLine);
			m_bTelnetCmdPhase++;
			break;

		case 1:		//Execute kill Process
			strCmdLine = "./kill";
			m_pTelnetSock->SendData(strCmdLine);
			m_bTelnetCmdPhase++;
			break;
		case 2:		//Send System halt Command
			m_pTelnetSock->SendPowerOff();
			m_bTelnetCmdPhase++;
			PostMessage(WM_UPDATE_COMPLITE, 0, 0);
			GetDlgItem(IDC_HALT_BUTTON)->EnableWindow(TRUE);
			break;
		}
	}

	//Calibration Mode
	if(m_nTelnetMode == TELENT_CMD_MODE_KILL_PROCESS)
	{
		switch(m_bTelnetCmdPhase)
		{
		case 0: 
			GetDlgItem(IDC_KILL_PROCESS_BUTTON)->EnableWindow(FALSE);
			strCmdLine.Format("cd /project/%s/current/form/App/PSKill", m_strProjectName);
			m_pTelnetSock->SendData(strCmdLine);
			m_bTelnetCmdPhase++;
			break;

		case 1:
			strCmdLine = "./kill";
			m_pTelnetSock->SendData(strCmdLine);
			m_bTelnetCmdPhase++;
			break;
		case 2:
			m_pTelnetSock->SendPortData("513", "0");
			m_bTelnetCmdPhase++;
			PostMessage(WM_UPDATE_COMPLITE, 0, 0);
			GetDlgItem(IDC_KILL_PROCESS_BUTTON)->EnableWindow(TRUE);
			break;
		}
	}
	return TRUE;
}

void CIOTestView::OnKillProcessButton() 
{
	// TODO: Add your control notification handler code here
	
	UpdateData(TRUE);

	CString msg;
	BYTE address[4];

	m_ctrlIPAddress.GetAddress(address[0], address[1], address[2], address[3]);
	m_strIPAddress.Format("%d.%d.%d.%d", address[0], address[1], address[2], address[3]);

	UpdateModuleState();
	if(m_nModuleState == EP_STATE_RUN || m_nModuleState == EP_STATE_PAUSE)
	{
		msg.Format("설비[%s]는 Run 상태이거나 잠시멈춤 상태 입니다. 그래도 모듈 실행을 종료 하시겠습니까?", m_strIPAddress);
		if(MessageBox(msg, "모듈 실행 종료", MB_YESNO|MB_ICONQUESTION) == IDNO)
			return;
	}

	msg.Format("설비[%s] 모듈 동작을 종료 하시겠습니까?", m_strIPAddress);
	if(MessageBox(msg, "Module Quit", MB_ICONQUESTION|MB_YESNO) == IDYES)
	{
		m_nTelnetMode = TELENT_CMD_MODE_KILL_PROCESS;//Kill Process
		if(ConnectTelnet() == TRUE)
		{
		}
	}	
}

//Get Module State by FTP
WORD CIOTestView::UpdateModuleState()
{
	ASSERT(!m_strIPAddress.IsEmpty());

	WORD state = EP_STATE_LINE_OFF;		//default Line Off State
	char sz[1024];
	CString strRemoteFileName;
	strRemoteFileName.Format("/project/%s/config/tmp/ModuleState", m_strProjectName);
	CString strLocalFileName("md_state.txt");

//////////////////////////////////////////////
//Ftp 연결 
	CInternetSession ftpSession("_T(StateFileFTP)");
	CFtpConnection *pFtpConnection = NULL;
	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(m_strIPAddress, "root", m_strRootPassword);
	}
	catch(CInternetException *e)
	{
		e->GetErrorMessage(sz, 1024);
//		AfxMessageBox(sz);
		e->Delete();
		ftpSession.Close();
		return state;
	}
///////연결 완료 ////////////////
	
	try
	{
		if(pFtpConnection->GetFile(strRemoteFileName, strLocalFileName, FALSE) == 0)
		{

		}
	}
	catch(CInternetException *e)
	{
		e->GetErrorMessage(sz, 1024);
//		AfxMessageBox(sz);
		e->Delete();

		pFtpConnection->Close();
		ftpSession.Close();
		delete pFtpConnection;

		return state;
	}

	pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;


	FILE *fp = NULL;
	fp = fopen(strLocalFileName, "rt");
	if(fp != NULL)
	{
		if(fscanf(fp, "%s", sz) > 0)
		{
			state = WORD(atol(sz));
		}
		fclose(fp);
	}
	
	m_nModuleState = state;
	
	return state;
}

void CIOTestView::OnCheck73() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);
}

void CIOTestView::OnCheck74() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);	
}

void CIOTestView::OnCheck75() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);	
}

void CIOTestView::OnCheck76() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);	
}

void CIOTestView::OnCheck77() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);	
}

void CIOTestView::OnCheck78() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);

}

void CIOTestView::OnCheck79() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);

}

void CIOTestView::OnCheck80() 
{
	// TODO: Add your control notification handler code here
		WriteData(m_nOutputAddress[4]);

}
