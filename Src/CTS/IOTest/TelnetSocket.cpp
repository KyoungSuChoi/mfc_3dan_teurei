// ClientSocket.cpp : implementation file
//

#include "stdafx.h"
//#include "Telnet.h"
#include "TelnetSocket.h"

//#include "TelnetView.h"
//#include "TelnetDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTelnetSock

CTelnetSock::CTelnetSock(HWND hWnd)
{
	m_pHwnd = hWnd;
}

CTelnetSock::~CTelnetSock()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CTelnetSock, CAsyncSocket)
	//{{AFX_MSG_MAP(CTelnetSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CTelnetSock member functions

void CTelnetSock::OnClose(int nErrorCode) 
{
//	AfxMessageBox("Connection Closed",MB_OK);
	
	CAsyncSocket::OnClose(nErrorCode);
/*	if(!IsWindow(cView->m_hWnd)) return;
	if(!IsWindowVisible(cView->m_hWnd)) return;
	cView->GetDocument()->OnCloseDocument();
*/
	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_SOCKET_DISCONNECTED, 0, (LPARAM)this);
}

void CTelnetSock::OnConnect(int nErrorCode) 
{
	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_SOCKET_CONNECTED, 0, (LPARAM)this);

	CAsyncSocket::OnConnect(nErrorCode);
}

void CTelnetSock::OnOutOfBandData(int nErrorCode) 
{
	ASSERT(FALSE); //Telnet should not have OOB data
	CAsyncSocket::OnOutOfBandData(nErrorCode);
}

void CTelnetSock::OnReceive(int nErrorCode) 
{
//	cView->ProcessMessage(this);
	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_RECEIVE_DATA, 0, (LPARAM)this);
}

void CTelnetSock::OnSend(int nErrorCode) 
{
	CAsyncSocket::OnSend(nErrorCode);
}
