// IROCVPort.h: interface for the CIROCVPort class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IROCVPORT_H__08163312_210D_4224_9AF0_315AB9268E90__INCLUDED_)
#define AFX_IROCVPORT_H__08163312_210D_4224_9AF0_315AB9268E90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define IOCARD_INPUT_PORT1		1
#define IOCARD_INPUT_PORT2		2
#define IOCARD_INPUT_PORT3		3
#define IOCARD_INPUT_PORT4		4

#define IOCARD_OUTPUT_PORT1		1
#define IOCARD_OUTPUT_PORT2		2
#define IOCARD_OUTPUT_PORT3		3
#define IOCARD_OUTPUT_PORT4		4
	

#define IROCV_INPUT_PORT1		IOCARD_INPUT_PORT4
#define IROCV_INPUT_PORT2		IOCARD_INPUT_PORT3
#define IROCV_OUTPUT_PORT1		IOCARD_OUTPUT_PORT4
//#define IROCV_OUTPUT_PORT2		0x511

#define IROCV_RELAY_SEL1		IOCARD_OUTPUT_PORT1
#define IROCV_RELAY_SEL2		IOCARD_OUTPUT_PORT2
#define IROCV_RELAY_SEL3		IOCARD_OUTPUT_PORT3

#define SEL_RESET		0x00
#define SEL_DEFAULT		0x01
#define SEL_ALL			0xFF

#define MAX_CH_NO		128

#define CYLINDER_SENSOR_INPUT_PORT	IROCV_INPUT_PORT1
#define DOOR_SENSOR_PORT			IROCV_INPUT_PORT1	
#define POWER_LAMP_OUTPUT_PORT		IROCV_OUTPUT_PORT1	
#define BUZZER_OUPUT_PORT			IROCV_OUTPUT_PORT1
#define JIG_SOL_OUTPUT_PORT			IROCV_OUTPUT_PORT1
#define DOOR_LAMP_OUTPUT_PORT		IROCV_OUTPUT_PORT1
#define SIGN_LAMP_OUTPUT_PORT		IROCV_OUTPUT_PORT1

//Input Port 1
#define UPDOWN_CYLINDER_UP_SENSOR_BIT		0
#define UPDOWN_CYLINDER_DOWN_SENSOR_BIT		1
#define DOOR_SENSOR_BIT						2
#define TRAY_SENSOR_BIT						3

//Input Port 2
#define LEFT_CYLINDER_OPEN_SENSOR_BIT		0
#define LEFT_CYLINDER_CLOSE_SENSOR_BIT		1
#define RIGHT_CYLINDER_OPEN_SENSOR_BIT		2
#define RIGHT_CYLINDER_CLOSE_SENSOR_BIT		3


#define UPDOWN_CYLINDER_UP_SENSOR		(0x01<<UPDOWN_CYLINDER_UP_SENSOR_BIT)
#define UPDOWN_CYLINDER_DOWN_SENSOR		(0x01<<UPDOWN_CYLINDER_DOWN_SENSOR_BIT)
#define LEFT_CYLINDER_OPEN_SENSOR		(0x01<<LEFT_CYLINDER_OPEN_SENSOR_BIT)
#define LEFT_CYLINDER_CLOSE_SENSOR		(0x01<<LEFT_CYLINDER_CLOSE_SENSOR_BIT)
#define RIGHT_CYLINDER_OPEN_SENSOR		(0x01<<RIGHT_CYLINDER_OPEN_SENSOR_BIT)
#define RIGHT_CYLINDER_CLOSE_SENSOR		(0x01<<RIGHT_CYLINDER_CLOSE_SENSOR_BIT)
#define	DOOR_SENSOR						(0x01 << DOOR_SENSOR_BIT)
#define TRAY_SENSOR						(0x01 << TRAY_SENSOR_BIT)

//OupPut Port 1
#define POWER_ON_LAMP_BIT					0
#define BUZZER_ALARM_BIT					1
#define DOOR_LAMP_ON_BIT					2
#define SIGN_LAMP_ON_BIT					3
#define UPDOWN_CYLINDER_UP_SOL_BIT			4
#define UPDOWN_CYLINDER_DOWN_SOL_BIT		5
#define GRIPPER_OPEN_SOL_BIT				6	
#define GRIPPER_CLOSE_SOL_BIT				7


#define POWER_ON_LAMP					(0x01<<POWER_ON_LAMP_BIT)
#define BUZZER_ALARM					(0x01<<BUZZER_ALARM_BIT)
#define UPDOWN_CYLINDER_DOWN_SOL		(0x01<<UPDOWN_CYLINDER_DOWN_SOL_BIT)
#define UPDOWN_CYLINDER_UP_SOL			(0x01<<UPDOWN_CYLINDER_UP_SOL_BIT)
#define GRIPPER_CLOSE_SOL				(0x01<<GRIPPER_CLOSE_SOL_BIT)
#define GRIPPER_OPEN_SOL				(0x01<<GRIPPER_OPEN_SOL_BIT)	
#define DOOR_LAMP_ON					(0x01<<DOOR_LAMP_ON_BIT)
#define SIGN_LAMP_ON					(0x01<<SIGN_LAMP_ON_BIT)

#define JIG_UPDOWN_TIMEOUT				5000
#define GRIPPER_OPENCLOSE_TIMEOUT		5000
#define MAX_READ_COUNT					3
#define INPUT_READ_INTERVAL				200

#define JIG_UP			0
#define JIG_DOWN		1
#define JIG_ERROR		2

#define JIG_OPEN		0
#define JIG_CLOSE		1
#define JIG_ERROR		2

#define CYLINDER_OPEN	0
#define CYLINDER_CLOSE	1

#define DOOR_OPEN		0
#define DOOR_CLOSE		1

#define TRAY_UNLOAD		0
#define TRAY_LOAD		1

#define WM_DOOR_CLOSE	WM_USER+1
#define WM_DOOR_OPEN	WM_USER+2
#define WM_JIG_CLOSE	WM_USER+3
#define WM_JIG_OPEN		WM_USER+4
#define WM_TRAY_LOADED	WM_USER+5
#define WM_TRAY_UNLOADED	WM_USER+6

class CIROCVPort  
{
public:
	void TrayUnLoaded();
	void TrayLoaded();
	void DoorClose();
	void DoorOpen();
	BYTE ReadFromPort(int nPort);
	BOOL m_bInitedCard;
	BOOL InitIOCard();
	int TrayState();
	int m_nTrayState;
	WORD GetJigState();
	BOOL BuzzerAlarm(BOOL bAlarm, int mSec = 0);
	BOOL SignTower(BOOL bTurnOn);
	BYTE m_OutPutPort1Data;
//	BYTE m_OutPutPort2Data;
	BOOL JigUp();
	BOOL JigDown();
	BOOL WriteToPort(int nPort = 0, BYTE data = 0);
	BOOL ResetSelect();
	BOOL SelectCh(int nChIndex, BOOL bReset = FALSE);
	BOOL Stop();
	BOOL Start(CWnd *pPortOwner);
	CIROCVPort();
	virtual ~CIROCVPort();

	volatile    WORD	m_doorState;
	volatile    WORD	m_JigState;
	volatile	BOOL	m_bRunThread; 
	volatile    WORD	m_updownState;
	volatile    WORD	m_leftCylinderState;
	volatile    WORD	m_rightCylinderState;

protected:
	void CloseIOCard();
	HANDLE		m_hShutdownEvent;
	HANDLE		m_hJigUpEvent;
	HANDLE		m_hJigDownEvent;
	HANDLE		m_hLeftCylinderOpenCloseEvent;
	HANDLE		m_hRightCylinderOpenCloseEvent;

	WORD	m_wTotalBoard;
	WORD	m_wInitialCode;
	DWORD   m_wGetAddrBase;   // the base address assigned for board
	WORD    m_wGetIrqNo;      // the IRQ Level assigned for board
	WORD    m_wGetSubVendor, m_wGetSubDevice, m_wGetSubAux;
	WORD	m_wGetSlotBus, m_wGetSlotDevice;

	// owner window
	CWnd*				m_pOwner;

	CWinThread*			m_Thread;
	static UINT	PortThread(LPVOID pParam);
};

#endif // !defined(AFX_IROCVPORT_H__08163312_210D_4224_9AF0_315AB9268E90__INCLUDED_)
