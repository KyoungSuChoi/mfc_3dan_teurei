#if !defined(AFX_TELNETSOCKET_H__42C5C9C7_3102_11D2_9A30_00C04FB78B23__INCLUDED_)
#define AFX_TELNETSOCKET_H__42C5C9C7_3102_11D2_9A30_00C04FB78B23__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ClientSocket.h : header file
//

#include <afxsock.h>
#include "telnetdef.h"

/////////////////////////////////////////////////////////////////////////////
// CTelnetSock command target


class CTelnetSock : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CTelnetSock(HWND hWnd);
	virtual ~CTelnetSock();

// Overrides
public:
	HWND m_pHwnd;
//	CTelnetView * cView;
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTelnetSock)
	public:
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual void OnOutOfBandData(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CTelnetSock)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TELNETSOCKET_H__42C5C9C7_3102_11D2_9A30_00C04FB78B23__INCLUDED_)
