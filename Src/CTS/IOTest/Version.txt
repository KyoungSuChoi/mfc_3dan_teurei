1.1.0 : 입력 센서 Scan 간격이 빠름
1.1.5 : 연속적인 반복 동작을 지정 할 수 있음 
1.1.6 : 지그 동작을 DIO Card를 이용하여 PC에 연결 시킬수 있고 연속 동작 Sequence를 사용자가 수정 할 수 있음
1.1.7 : 지그 동작을 DIO Card를 이용하여 PC에 연결하여 읽어 들이고 출력까지 제공

1.2.0 : 
	1) Simple IO Test Mode(충방전 Program 실행시는 Simple Mode로 실시)
	2) ioconfig.cfg 파일에 [Directory] 항목 추가
	3) Module 종료/Reboot/Power Off 명령 추가
	4) 모듈 상태정보를 Ftp를 이용하여 Read하여 경고 표시

1.2.5 :
	1) Input Address 5Port로 확장

1.2.6 :
	1) ioconfig.cfg에서 [Auto_Sequence_Interval_1] 추가하여 각 동작별 시간 지정 가능