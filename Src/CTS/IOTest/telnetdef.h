#ifndef TELNET_DEFINE
#define TELNET_DEFINE

#define WM_RECEIVE_DATA			WM_USER+2
#define WM_SOCKET_CONNECTED		WM_USER+3
#define WM_SOCKET_DISCONNECTED	WM_USER+4
#define WM_CMD_STANDBY			WM_USER+5
#define WM_UPDATE_COMPLITE		WM_USER+6

#define LOG_FILE_NAME			"transdata.log"

const	unsigned char IAC		= 255;
const	unsigned char DO		= 253;
const	unsigned char DONT		= 254;
const	unsigned char WILL		= 251;
const	unsigned char WONT		= 252;
const	unsigned char SB		= 250;
const	unsigned char SE		= 240;
const	unsigned char IS		= '0';
const	unsigned char SEND		= '1';
const	unsigned char INFO		= '2';
const	unsigned char VAR		= '0';
const	unsigned char VALUE		= '1';
const	unsigned char ESC		= '2';
const	unsigned char USERVAR	= '3';

#define CMD_PHASE_INIT_STATE		0
#define CMD_PHASE_USER_PASSWORD		1
#define CMD_PHASE_CHANGE_ROOT		2
#define CMD_PHASE_ROOT_PASSWORD		3
#define CMD_PHASE_LOGIN_DONE		4


#define PHASE_INIT_STATE		0
#define PHASE_USER_PASSWORD		1
#define PHASE_CHANGE_ROOT		2
#define PHASE_ROOT_PASSWORD		3
#define PHASE_CHANGE_DIRECTORY	4
#define PHASE_EXECUTE			5
#define PHASE_LOGIN_DONE		6


#endif
