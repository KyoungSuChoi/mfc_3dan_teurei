// IOTest.h : main header file for the IOTEST application
//

#if !defined(AFX_IOTEST_H__86BFBE79_F21A_40CA_9D02_DF8C23BC55BA__INCLUDED_)
#define AFX_IOTEST_H__86BFBE79_F21A_40CA_9D02_DF8C23BC55BA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CIOTestApp:
// See IOTest.cpp for the implementation of this class
//

class CIOTestApp : public CWinApp
{
public:
	CIOTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIOTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CIOTestApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IOTEST_H__86BFBE79_F21A_40CA_9D02_DF8C23BC55BA__INCLUDED_)
