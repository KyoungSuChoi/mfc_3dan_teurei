// IOTestDoc.h : interface of the CIOTestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IOTESTDOC_H__0023B19B_63A0_4D88_A42C_3CD13755226A__INCLUDED_)
#define AFX_IOTESTDOC_H__0023B19B_63A0_4D88_A42C_3CD13755226A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CIOTestDoc : public CDocument
{
protected: // create from serialization only
	CIOTestDoc();
	DECLARE_DYNCREATE(CIOTestDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIOTestDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL m_bSimpleTestMode;
	BOOL m_bUseOutPut;
	virtual ~CIOTestDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CIOTestDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IOTESTDOC_H__0023B19B_63A0_4D88_A42C_3CD13755226A__INCLUDED_)
