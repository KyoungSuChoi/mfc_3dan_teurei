// ClientSocket.cpp : implementation file
//

#include "stdafx.h"
//#include "Telnet.h"
#include "ClientSocket.h"

//#include "TelnetView.h"
//#include "TelnetDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClientSocket

CClientSocket::CClientSocket(HWND hWnd)
{
	m_pHwnd = hWnd;
	m_nPhase = CMD_PHASE_INIT_STATE;
	m_strRootPassword = "dusrn1";
	m_strUserPassword = "dusrn1";
	m_strUserID = "sbc";

}

CClientSocket::~CClientSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClientSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CClientSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClientSocket member functions

void CClientSocket::OnClose(int nErrorCode) 
{
//	AfxMessageBox("Connection Closed",MB_OK);
	
	CAsyncSocket::OnClose(nErrorCode);

	m_nPhase = CMD_PHASE_INIT_STATE;

	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_SOCKET_DISCONNECTED, 0, (LPARAM)this);
}

void CClientSocket::OnConnect(int nErrorCode) 
{
	if (0 != nErrorCode)
	{
		AfxMessageBox(GetErrorMsg(nErrorCode));
		return;
	}

	unlink(LOG_FILE_NAME);
	m_nPhase = CMD_PHASE_INIT_STATE;

	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_SOCKET_CONNECTED, 0, (LPARAM)this);

	CAsyncSocket::OnConnect(nErrorCode);
}

void CClientSocket::OnOutOfBandData(int nErrorCode) 
{
	ASSERT(FALSE); //Telnet should not have OOB data
	CAsyncSocket::OnOutOfBandData(nErrorCode);
}

void CClientSocket::OnReceive(int nErrorCode) 
{
	static int count = 0;

	int nBytes = Receive(m_bBuf ,ioBuffSize );

//	m_bBuf[nBytes] = '\0';
//	TRACE("Data Received %03d ::: %s\n", count++, m_bBuf);

	if(nBytes != SOCKET_ERROR)
	{
		while(GetLine(m_bBuf, nBytes) != TRUE);
		ProcessOptions();
		ParsingMessage(m_strNormalText);
	}
	m_strLine.Empty();
	m_strResp.Empty();
}

//Parsing Data to Line String 
void CClientSocket::ParsingMessage(LPCSTR pText)
{
	CString buff(pText);
	if(buff.IsEmpty())	return;

	int nIndex;
	while((nIndex = buff.Find("\r\n")) >= 0)
	{
		m_strLineData += buff.Left(nIndex);
		if(nIndex+2 < buff.GetLength())
		{
			buff = buff.Mid(nIndex+3);
		}
		else
		{
			buff.Empty();
		}

		if(!m_strLineData.IsEmpty())
		{
			ParsingData(m_strLineData);
			m_strLineData.Empty();
		}
	}

//	if(m_nPhase < PHASE_LOGIN_DONE)
//	{
	if(buff.GetLength() > 0)
	{
		m_strLineData += buff;
		ParsingData(m_strLineData);
	}
//	}
}

BOOL CClientSocket::ParsingData(LPCSTR pText)
{
//	m_timeOutFlag = FALSE;

	CString msg(pText);

//	TRACE("%s\n", msg);
	FILE *fp = fopen(LOG_FILE_NAME, "at+");
	if(fp != NULL)
	{
		fprintf(fp, "%s\n", msg);
		fclose(fp);
	}
	
	if(m_pHwnd)
		SendMessage(m_pHwnd, WM_RECEIVE_DATA, (WPARAM)pText, (LPARAM)this);

	CString subMsg;

	switch(m_nPhase)
	{
	case CMD_PHASE_INIT_STATE:		//ID 입력
		subMsg = msg.Right(7);
		if(subMsg == "login: ")
		{
			SendData(m_strUserID);
			m_nPhase++;
//			m_wndProgress.SetPos(15);
		}
		break;
	case CMD_PHASE_USER_PASSWORD:		//Password 입력
		subMsg = msg.Right(10);
		if(subMsg == "Password: ")
		{
			SendData(m_strUserPassword);
			m_nPhase++;
//			m_wndProgress.SetPos(30);
		}
		break;
	case CMD_PHASE_CHANGE_ROOT:		//root 계정 변경 
		subMsg = msg.Right(m_strUserID.GetLength()+3);
		if(subMsg == m_strUserID+"]$ ")
		{
			SendData("su - root");
			m_nPhase++;
//			m_wndProgress.SetPos(45);
		}
		break;

	case CMD_PHASE_ROOT_PASSWORD:		//root 암호 입력  
		subMsg = msg.Right(10);
		if(subMsg == "Password: ")
		{
			SendData(m_strRootPassword);
			m_nPhase++;
//			m_wndProgress.SetPos(60);
		}
		break;

	case CMD_PHASE_LOGIN_DONE:
		subMsg = msg.Right(3);
		if(subMsg == "]# ")
		{
			if(m_pHwnd)
				SendMessage(m_pHwnd, WM_CMD_STANDBY, (WPARAM)pText, (LPARAM)this);
		}
		break;

	default:
		return FALSE;
	}

	return TRUE;
}

int CClientSocket::SendData(CString strData)
{
//	m_bTx = TRUE;

	ASSERT(m_hSocket);
	
	CString strTempData;
	int baseIndex = 0;
	int nSize = strData.GetLength();

	if(nSize > 0)
	{
		//경로명 길이가 길 경우 경로 이동 오류가 있어 경로 이동 명령을 쪼개서 여러번 실행
/*		if(strData.Left(2) == "cd")
		{
			strTempData = strData.Mid(2);	//경로만 추출 
			strTempData.TrimLeft(" ");		//경로명 이전의 Space 제거 
			strData = strTempData;

			if(strTempData.GetLength() > 0)
			{
				if(strTempData[0] == '/')	//root 이동 
				{
					if( 5 != Send("cd /\n", 5))
					{	
						TRACE("MessageSend Fail\n");
						return FALSE;
					}
					strData = strTempData.Mid(1);
				}
			}
			else
			{
				if( 3 != Send("cd\n", 3))		//Data is "cd"
				{	
					TRACE("MessageSend Fail\n");
					return FALSE;
				}
			}

			//다음 경로 검색 
			while((baseIndex = strData.Find('/')) > 0)
			{
				strTempData = strData.Left(baseIndex);		//	'/' 이전 경로명 
				strData = strData.Mid(baseIndex+1);			//	'/' 이후 나머지 경로명 

				strTempData = "cd "+ strTempData +"\n";
				nSize = strTempData.GetLength();
				if( nSize != Send(strTempData, nSize))
				{	
					TRACE("MessageSend Fail\n");
					return FALSE;
				}
				TRACE("SEND MESSAGE ==>> %s\n", strTempData);
			}

			if(!strData.IsEmpty())							//마지막 나머지 경로 전송 
			{
				strTempData = "cd " + strData + "\n";
				nSize = strTempData.GetLength();
				if(nSize != Send(strTempData, nSize))
				{	
					TRACE("MessageSend Fail\n");
					return FALSE;
				}
				TRACE("SEND MESSAGE ==>> %s\n", strTempData);
			}
		}
		else
		{
	*/	
			strTempData = strData;
			strTempData += "\n";
			nSize = strTempData.GetLength();
			Sleep(100);
			if( nSize != Send(strTempData, nSize))
			{	
				TRACE("MessageSend Fail => %s\n", strData);
				return FALSE;
			}
			TRACE("SEND MESSAGE ==>> %s\n", strData);
//		}
	}
	return TRUE;
}


void CClientSocket::OnSend(int nErrorCode) 
{
	CAsyncSocket::OnSend(nErrorCode);
}


BOOL CClientSocket::GetLine(unsigned char *bytes, int nBytes)
{
	BOOL bLine = FALSE;
	int ndx = 0;

	while ( bLine == FALSE && ndx < nBytes )
	{
		unsigned char ch = bytes[ndx];
		
		switch( ch )
		{
		case '\r': // ignore
			m_strLine += "\r\n"; //"CR";
			break;

		case '\n': // end-of-line
			m_strLine += '\n'; //"LF";
//			bLine = TRUE;
			break;

		default:   // other....
			m_strLine += ch;
			break;
		} 

		ndx ++;

		if (ndx == nBytes)
		{
			bLine = TRUE;
		}
	}
	return bLine;
}

void CClientSocket::ProcessOptions()
{
	CString strTemp;
	CString strOption;
	unsigned char ch;
	int ndx;
	int ldx;
	BOOL bScanDone = FALSE;

	strTemp = m_strLine;

//	TRACE("RX :: %s\n", strTemp);
	
	while(!strTemp.IsEmpty() && bScanDone != TRUE)
	{
		ndx = strTemp.Find(IAC);
		if(ndx != -1)
		{
			m_strNormalText += strTemp.Left(ndx);		//IAC 이전 
			ch = strTemp.GetAt(ndx + 1);				//IAC 다음 문자
			switch(ch)
			{
			case DO:
			case DONT:
			case WILL:
			case WONT:
				strOption		= strTemp.Mid(ndx, 3);	//IAC부터 3개의 문자 
				strTemp		= strTemp.Mid(ndx + 3);		//3개 이후의 문자 (새로운 문장)
				m_strNormalText	= strTemp.Left(ndx);	//IAC 이전 문자들 
				m_ListOptions.AddTail(strOption);
				break;
			case IAC:
				m_strNormalText	= strTemp.Left(ndx);
				strTemp		= strTemp.Mid(ndx + 1);
				break;
			case SB:
				m_strNormalText = strTemp.Left(ndx);
				ldx = strTemp.Find(SE);
				strOption	= strTemp.Mid(ndx, ldx);
				m_ListOptions.AddTail(strOption);
				strTemp		= strTemp.Mid(ldx);
				
				AfxMessageBox(strOption, MB_OK);
				break;
			}
		}
		else
		{
			m_strNormalText = strTemp;
			bScanDone = TRUE;
		}
	} 
	
	RespondToOptions();
}

void CClientSocket::RespondToOptions()
{
	CString strOption;
	
	while(!m_ListOptions.IsEmpty())
	{
		strOption = m_ListOptions.RemoveHead();

		ArrangeReply(strOption);
	}

	DispatchMessage(m_strResp);
	m_strResp.Empty();
}

void CClientSocket::ArrangeReply(CString strOption)
{

	unsigned char Verb;
	unsigned char Option;
	unsigned char Modifier;
	unsigned char ch;
	BOOL bDefined = FALSE;

	if(strOption.GetLength() < 3) return;

	Verb = strOption.GetAt(1);
	Option = strOption.GetAt(2);

	switch(Option)
	{
	case 1:	// Echo
	case 3: // Suppress Go-Ahead
		bDefined = TRUE;
		break;
	}

	m_strResp += IAC;

	if(bDefined == TRUE)
	{
		switch(Verb)
		{
		case DO:
			ch = WILL;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case DONT:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WILL:
			ch = DO;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WONT:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case SB:
			Modifier = strOption.GetAt(3);
			if(Modifier == SEND)
			{
				ch = SB;
				m_strResp += ch;
				m_strResp += Option;
				m_strResp += IS;
				m_strResp += IAC;
				m_strResp += SE;
			}
			break;
		}
	}

	else
	{
		switch(Verb)
		{
		case DO:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case DONT:
			ch = WONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WILL:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		case WONT:
			ch = DONT;
			m_strResp += ch;
			m_strResp += Option;
			break;
		}
	}
}

void CClientSocket::DispatchMessage(CString strText)
{
	ASSERT(m_hSocket);
	Send(strText, strText.GetLength());

//	TRACE("TX :: %s\n", strText);

}

CString CClientSocket::GetErrorMsg(int nCode)
{
	switch(nCode)
	{
	case WSAENAMETOOLONG	:   return "Winsock Error :: Name too long";
    case WSANOTINITIALISED	:	return "Winsock Error :: Not initialized";
    case WSASYSNOTREADY		:   return "Winsock Error :: System not ready";
    case WSAVERNOTSUPPORTED :	return "Winsock Error :: Version is not supported";
    case WSAESHUTDOWN		:	return "Winsock Error :: Can't send after socket shutdown";
    case WSAEINTR			:   return "Winsock Error :: Interrupted system call";
    case WSAHOST_NOT_FOUND	:	return "Winsock Error :: Host not found";
    case WSATRY_AGAIN		:   return "Winsock Error :: Try again";
    case WSANO_RECOVERY		:	return "Winsock Error :: Non-recoverable error";
    case WSANO_DATA			:   return "Winsock Error :: No data record available";
    case WSAEBADF			:   return "Winsock Error :: Bad file number";
    case WSAEWOULDBLOCK		:   return "Winsock Error :: Operation would block";
    case WSAEINPROGRESS		:   return "Winsock Error :: Operation now in progress";
    case WSAEALREADY		:   return "Winsock Error :: Operation already in progress";
    case WSAEFAULT			:   return "Winsock Error :: Bad address";
    case WSAEDESTADDRREQ	:   return "Winsock Error :: Destination address required";
    case WSAEMSGSIZE		:   return "Winsock Error :: Message too long";
    case WSAEPFNOSUPPORT	:	return "Winsock Error :: Protocol family not supported";
    case WSAENOTEMPTY		:   return "Winsock Error :: Directory not empty";
    case WSAEPROCLIM		:   return "Winsock Error :: EPROCLIM returned";
    case WSAEUSERS			:   return "Winsock Error :: EUSERS returned";
    case WSAEDQUOT			:   return "Winsock Error :: Disk quota exceeded";
    case WSAESTALE			:   return "Winsock Error :: ESTALE returned";
    case WSAEINVAL			:   return "Winsock Error :: Invalid argument";
    case WSAEMFILE			:   return "Winsock Error :: Too many open files";
    case WSAEACCES			:   return "Winsock Error :: Access denied";
    case WSAELOOP			:   return "Winsock Error :: Too many levels of symbolic links";
    case WSAEREMOTE			:   return "Winsock Error :: The object is remote";
    case WSAENOTSOCK		:   return "Winsock Error :: Socket operation on non-socket";
    case WSAEADDRNOTAVAIL	:   return "Winsock Error :: Can't assign requested address";
    case WSAEADDRINUSE		:   return "Winsock Error :: Address already in use";
    case WSAEAFNOSUPPORT	:   return "Winsock Error :: Address family not supported by protocol family";
    case WSAESOCKTNOSUPPORT :   return "Winsock Error :: Socket type not supported";
    case WSAEPROTONOSUPPORT :   return "Winsock Error :: Protocol not supported";
    case WSAENOBUFS			:   return "Winsock Error :: No buffer space is supported";
    case WSAETIMEDOUT		:   return "Winsock Error :: Connection timed out";
    case WSAEISCONN			:   return "Winsock Error :: Socket is already connected";
    case WSAENOTCONN		:   return "Winsock Error :: Socket is not connected";
    case WSAENOPROTOOPT		:   return "Winsock Error :: Bad protocol option";
    case WSAECONNRESET		:   return "Winsock Error :: Connection reset by peer";
    case WSAECONNABORTED	:   return "Winsock Error :: Software caused connection abort";
    case WSAENETDOWN		:   return "Winsock Error :: Network is down";
    case WSAENETRESET		:   return "Winsock Error :: Network was reset";
    case WSAECONNREFUSED	:   return "Winsock Error :: Connection refused";
    case WSAEHOSTDOWN		:   return "Winsock Error :: Host is down";
    case WSAEHOSTUNREACH	:   return "Winsock Error :: Host is unreachable";
    case WSAEPROTOTYPE		:   return "Winsock Error :: Protocol is wrong type for socket";
    case WSAEOPNOTSUPP		:   return "Winsock Error :: Operation not supported on socket";
    case WSAENETUNREACH		:   return "Winsock Error :: ICMP network unreachable";
    case WSAETOOMANYREFS	:   return "Winsock Error :: Too many references";
    default					:   return "Winsock Error :: Unknown";
    }
	return "";
}

void CClientSocket::SetUser(CString strUserID, CString strPassWnd)
{
	m_strUserPassword = strPassWnd;
	m_strUserID = strUserID;
}

void CClientSocket::SetRootPassword(CString strPassWord)
{
	m_strRootPassword = strPassWord;
}

BOOL CClientSocket::SendSystemReboot()
{
	ASSERT(m_hSocket);


	//먼저 Kill Process를 실시 하고 OutPut 해야 함
	if(SendPortData("513", "2") == FALSE)	return FALSE;
	Sleep(100);

	SendData("reboot");
	
	return TRUE;
}

BOOL CClientSocket::SendPowerOff()
{
	ASSERT(m_hSocket);

	//먼저 Kill Process를 실시 하고 OutPut 해야 함
	if(SendPortData("513", "a") == FALSE)	return FALSE;
	Sleep(100);
	SendData("halt");

	return TRUE;
}

void CClientSocket::SetIOServerName(CString strName)
{
	m_strIOServerName = strName;
}

BOOL CClientSocket::SendPortData(CString strPort, CString strData)
{
	ASSERT(m_hSocket);

	if(m_strIOServerName.IsEmpty() || strPort.IsEmpty() || strData.IsEmpty())	return FALSE;

	CString cmdString;
	cmdString.Format("%s %s %s", m_strIOServerName, strPort, strData);
	SendData(cmdString);

	return TRUE;
}
