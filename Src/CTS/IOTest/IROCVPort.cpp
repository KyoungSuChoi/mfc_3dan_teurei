// IROCVPort.cpp: implementation of the CIROCVPort class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "PISODIO.H"

#include "IROCVPort.h"
#include <conio.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIROCVPort::CIROCVPort()
{
	m_JigState = JIG_OPEN;
	m_nTrayState = TRAY_UNLOAD;
	m_bRunThread = FALSE;
	m_Thread = NULL;
	m_OutPutPort1Data = 0;
//	m_OutPutPort2Data = 0;
	m_OutPutPort1Data |= POWER_ON_LAMP;

	m_hJigUpEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(m_hJigUpEvent);
	m_hJigDownEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(m_hJigDownEvent);
	m_hLeftCylinderOpenCloseEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(m_hLeftCylinderOpenCloseEvent);
	m_hRightCylinderOpenCloseEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(m_hRightCylinderOpenCloseEvent);
	m_hShutdownEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(m_hShutdownEvent);
	
	m_wTotalBoard = 0;
	m_wInitialCode = 0;
	m_wGetAddrBase = 0;   // the base address assigned for board
	m_wGetIrqNo = 0;      // the IRQ Level assigned for board
	m_wGetSubVendor =0;
	m_wGetSubDevice = 0;
	m_wGetSubAux = 0;
	m_wGetSlotBus = 0;
	m_wGetSlotDevice = 0;

	m_bInitedCard = FALSE;

}

CIROCVPort::~CIROCVPort()
{
	m_OutPutPort1Data &= ~POWER_ON_LAMP;
	WriteToPort();
	Stop();


//	::WaitForSingleObject(m_hShutdownEvent, INFINITE);

	::CloseHandle(m_hJigUpEvent);
	::CloseHandle(m_hJigDownEvent);
	::CloseHandle(m_hLeftCylinderOpenCloseEvent);
	::CloseHandle(m_hRightCylinderOpenCloseEvent);
	::CloseHandle(m_hShutdownEvent);

	m_Thread = NULL;

	CloseIOCard();
	TRACE("IROCV Port Thread ended\n");
}

BOOL CIROCVPort::Start(CWnd *pPortOwner)
{
	ASSERT(pPortOwner != NULL);
	
	// if the thread is alive: Kill
	Stop();
	m_pOwner = pPortOwner;

	if(InitIOCard() == FALSE)
		return FALSE;

	if (!(m_Thread = AfxBeginThread(PortThread, this)))		return FALSE;
	
//	WriteToPort();

	return TRUE;
}

BOOL CIROCVPort::Stop()
{
	ResetSelect();

	if(m_bRunThread)
	{
		m_bRunThread = FALSE;
		::WaitForSingleObject(m_hShutdownEvent, INFINITE);
	}
	return TRUE;
}

//#define UPDOWN_CYLINDER_UP_SENSOR		(0x01<<UPDOWN_CYLINDER_UP_SENSOR_BIT)
//#define UPDOWN_CYLINDER_DOWN_SENSOR		(0x01<<UPDOWN_CYLINDER_DOWN_SENSOR_BIT)

UINT CIROCVPort::PortThread(LPVOID pParam)
{
	TRACE("IROCV Port Thread started\n");
	BYTE portInput;
	CIROCVPort *pParent = (CIROCVPort *)pParam;
	
	pParent->m_bRunThread = TRUE;
	
	BYTE oldState[8] = {0}, newState = 0, bChange[8] = {FALSE}, count[8]= {0};
	BYTE oldState1[8] = {0}, newState1 = 0, bChange1[8] = {FALSE}, count1[8]= {0};
	BYTE doorState = 0, bDoorChange = FALSE, doorCount = 0;
	BYTE trayState = 0, bTrayChange = FALSE, trayCount = 0;

	while(pParent->m_bRunThread)
	{
		portInput = pParent->ReadFromPort(IROCV_INPUT_PORT1);

//		TRACE("0x%x Port Input 0x%x\n", IROCV_INPUT_PORT1, portInput);
		
//Jig Down sensor Check
		newState = portInput & (BYTE)UPDOWN_CYLINDER_DOWN_SENSOR;
		if(oldState[UPDOWN_CYLINDER_DOWN_SENSOR_BIT] != newState)
		{
			oldState[UPDOWN_CYLINDER_DOWN_SENSOR_BIT] = newState;
			bChange[UPDOWN_CYLINDER_DOWN_SENSOR_BIT] = TRUE;
			count[UPDOWN_CYLINDER_DOWN_SENSOR_BIT] = 0;
		}
		else
		{
			if(bChange[UPDOWN_CYLINDER_DOWN_SENSOR_BIT])
				count[UPDOWN_CYLINDER_DOWN_SENSOR_BIT]++;
		}

		if(count[UPDOWN_CYLINDER_DOWN_SENSOR_BIT]> MAX_READ_COUNT)
		{
			if(newState)
			{
				::SetEvent(pParent->m_hJigDownEvent);
				TRACE("Jig Down Sensor Detected\n");
				pParent->m_updownState = JIG_DOWN;
			}
/*			else
			{
				TRACE("Jig Down Sensor UnDetected\n");
			}
*/			bChange[UPDOWN_CYLINDER_DOWN_SENSOR_BIT] = FALSE;
			count[UPDOWN_CYLINDER_DOWN_SENSOR_BIT] = 0;
		}
		
//Jig Up sensor Check
		newState = portInput & (BYTE)UPDOWN_CYLINDER_UP_SENSOR;
		if(oldState[UPDOWN_CYLINDER_UP_SENSOR_BIT] != newState)
		{
			oldState[UPDOWN_CYLINDER_UP_SENSOR_BIT] = newState;
			bChange[UPDOWN_CYLINDER_UP_SENSOR_BIT] = TRUE;
			count[UPDOWN_CYLINDER_UP_SENSOR_BIT] = 0;
		}
		else
		{
			if(bChange[UPDOWN_CYLINDER_UP_SENSOR_BIT])
				count[UPDOWN_CYLINDER_UP_SENSOR_BIT]++;
		}

		if(count[UPDOWN_CYLINDER_UP_SENSOR_BIT]> MAX_READ_COUNT)
		{
			if(newState)
			{
				::SetEvent(pParent->m_hJigUpEvent);
				TRACE("Jig Up Sensor Detected\n");
				pParent->m_updownState = JIG_UP;
			}
/*			else
			{
				TRACE("Jig Up Sensor UnDetected\n");
			}
*/			bChange[UPDOWN_CYLINDER_UP_SENSOR_BIT] = FALSE;
			count[UPDOWN_CYLINDER_UP_SENSOR_BIT] = 0;
		}

//Door Open Close Sensor Check
		newState = portInput & (BYTE)DOOR_SENSOR;
		if(doorState != newState)
		{
			doorState = newState;
			bDoorChange = TRUE;
			doorCount = 0;
		}
		else
		{
			if(bDoorChange)		doorCount++;
		}

		if(doorCount> MAX_READ_COUNT)
		{
			if(newState)
			{
				pParent->DoorClose();
			}
			else
			{
				pParent->DoorOpen();
			}
			bDoorChange = FALSE;
			doorCount = 0;
		}		

//Tray Sensor Check
		newState = portInput & (BYTE)TRAY_SENSOR;
		if(trayState != newState)
		{
			trayState = newState;
			bTrayChange = TRUE;
			trayCount = 0;
		}
		else
		{
			if(bTrayChange)		trayCount++;
		}

		if(trayCount> MAX_READ_COUNT)
		{
			if(newState)
			{
				pParent->TrayLoaded();
			}
			else
			{
				pParent->TrayUnLoaded();
			}
			bTrayChange = FALSE;
			trayCount = 0;
		}	

//Read Input Port State
		portInput = pParent->ReadFromPort(IROCV_INPUT_PORT2);
//		TRACE("0x%x Port Input 0x%x\n", IROCV_INPUT_PORT2, portInput);
	
//Left Clylinder sensor Open Check
		newState1 = portInput & (BYTE)LEFT_CYLINDER_OPEN_SENSOR;
		if(oldState1[LEFT_CYLINDER_OPEN_SENSOR_BIT] != newState1)
		{
			oldState1[LEFT_CYLINDER_OPEN_SENSOR_BIT] = newState1;
			bChange1[LEFT_CYLINDER_OPEN_SENSOR_BIT] = TRUE;
			count1[LEFT_CYLINDER_OPEN_SENSOR_BIT] = 0;
		}
		else
		{
			if(bChange1[LEFT_CYLINDER_OPEN_SENSOR_BIT])
				count1[LEFT_CYLINDER_OPEN_SENSOR_BIT]++;
		}

		if(count1[LEFT_CYLINDER_OPEN_SENSOR_BIT]> MAX_READ_COUNT)
		{
			if(newState1)
			{
				::SetEvent(pParent->m_hLeftCylinderOpenCloseEvent);
				pParent->m_leftCylinderState = CYLINDER_OPEN;
				TRACE("Left cylinder Open Sensor Detected\n");
			}
		/*	else
			{
			}
		*/	bChange1[LEFT_CYLINDER_OPEN_SENSOR_BIT] = FALSE;
			count1[LEFT_CYLINDER_OPEN_SENSOR_BIT] = 0;
		}

//Left Clylinder sensor Close Check
		newState1 = portInput & (BYTE)LEFT_CYLINDER_CLOSE_SENSOR;
		if(oldState1[LEFT_CYLINDER_CLOSE_SENSOR_BIT] != newState1)
		{
			oldState1[LEFT_CYLINDER_CLOSE_SENSOR_BIT] = newState1;
			bChange1[LEFT_CYLINDER_CLOSE_SENSOR_BIT] = TRUE;
			count1[LEFT_CYLINDER_CLOSE_SENSOR_BIT] = 0;
		}
		else
		{
			if(bChange1[LEFT_CYLINDER_CLOSE_SENSOR_BIT])
				count1[LEFT_CYLINDER_CLOSE_SENSOR_BIT]++;
		}

		if(count1[LEFT_CYLINDER_CLOSE_SENSOR_BIT]> MAX_READ_COUNT)
		{
			if(newState1)
			{
				 ::SetEvent(pParent->m_hLeftCylinderOpenCloseEvent);
				pParent->m_leftCylinderState = CYLINDER_CLOSE;
				TRACE("Left cylinder Close Sensor Detected\n");
			}
			bChange1[LEFT_CYLINDER_CLOSE_SENSOR_BIT] = FALSE;
			count1[LEFT_CYLINDER_CLOSE_SENSOR_BIT] = 0;
		}

//Right Clylinder sensor Open Check
		newState1 = portInput & (BYTE)RIGHT_CYLINDER_OPEN_SENSOR;
		if(oldState1[RIGHT_CYLINDER_OPEN_SENSOR_BIT] != newState1)
		{
			oldState1[RIGHT_CYLINDER_OPEN_SENSOR_BIT] = newState1;
			bChange1[RIGHT_CYLINDER_OPEN_SENSOR_BIT] = TRUE;
			count1[RIGHT_CYLINDER_OPEN_SENSOR_BIT] = 0;
		}
		else
		{
			if(bChange1[RIGHT_CYLINDER_OPEN_SENSOR_BIT])
				count1[RIGHT_CYLINDER_OPEN_SENSOR_BIT]++;
		}

		if(count1[RIGHT_CYLINDER_OPEN_SENSOR_BIT]> MAX_READ_COUNT)
		{
			if(newState1)
			{
				::SetEvent(pParent->m_hRightCylinderOpenCloseEvent);
				pParent->m_rightCylinderState = CYLINDER_OPEN;
				TRACE("Right cylinder Open Sensor Detected\n");
			}
			bChange1[RIGHT_CYLINDER_OPEN_SENSOR_BIT] = FALSE;
			count1[RIGHT_CYLINDER_OPEN_SENSOR_BIT] = 0;
		}

//Right Clylinder sensor Close Check
		newState1 = portInput & (BYTE)RIGHT_CYLINDER_CLOSE_SENSOR;
		if(oldState1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT] != newState1)
		{
			oldState1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT] = newState1;
			bChange1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT] = TRUE;
			count1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT] = 0;
		}
		else
		{
			if(bChange1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT])
				count1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT]++;
		}

		if(count1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT]> MAX_READ_COUNT)
		{
			if(newState1)
			{
				::SetEvent(pParent->m_hRightCylinderOpenCloseEvent);
				pParent->m_leftCylinderState = CYLINDER_CLOSE;
				TRACE("Right cylinder Close Sensor Detected\n");
			}
			bChange1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT] = FALSE;
			count1[RIGHT_CYLINDER_CLOSE_SENSOR_BIT] = 0;
		}

		

		if(pParent->m_updownState == JIG_DOWN && 
			pParent->m_leftCylinderState == CYLINDER_CLOSE	&& 
			pParent->m_rightCylinderState == CYLINDER_CLOSE
		)
		{
			pParent->m_JigState = JIG_CLOSE;
		}
		else if(pParent->m_updownState == JIG_UP &&  
			pParent->m_leftCylinderState == CYLINDER_OPEN	&& 
			pParent->m_rightCylinderState == CYLINDER_OPEN
		)
		{
			pParent->m_JigState = JIG_OPEN;
		}
		else
		{
			pParent->m_JigState = JIG_ERROR;
		}

		Sleep(INPUT_READ_INTERVAL);
	}

	::SetEvent(pParent->m_hShutdownEvent);
	return 0;
}

BOOL CIROCVPort::SelectCh(int nChIndex, BOOL bReset)
{
	if(bReset)
		ResetSelect();

	BYTE sel1, sel2, sel3;
	if(nChIndex >= MAX_CH_NO/2 && nChIndex < MAX_CH_NO)
	{
		sel1 = 0;
		sel2 = (nChIndex-MAX_CH_NO/2)/8;
		sel2 = SEL_DEFAULT << sel2;
		sel3 = nChIndex%8;
		sel3 = SEL_DEFAULT << sel3;
	}
	else if(nChIndex >=0  && nChIndex < MAX_CH_NO/2)
	{
		sel1 = nChIndex/8;
		sel1 = SEL_DEFAULT << sel1;
		sel2 = 0;
		sel3 = nChIndex%8;
		sel3 = SEL_DEFAULT << sel3;
	}
	else
	{
		return FALSE;
	}

	WriteToPort(IROCV_RELAY_SEL1, sel1);
	WriteToPort(IROCV_RELAY_SEL2, sel2);
	WriteToPort(IROCV_RELAY_SEL3, sel3);

	TRACE("Select Ch Port 0x%x = 0x%02x, 0x%x = 0x%02x, 0x%x = 0x%02x\n", IROCV_RELAY_SEL1, sel1, IROCV_RELAY_SEL2, sel2, IROCV_RELAY_SEL3, sel3);
	return TRUE;
}

BOOL CIROCVPort::ResetSelect()
{
	WriteToPort(IROCV_RELAY_SEL1, SEL_RESET);
	WriteToPort(IROCV_RELAY_SEL2, SEL_RESET);
	WriteToPort(IROCV_RELAY_SEL3, SEL_RESET);
	
	TRACE("Reset Ch Select. Port 0x%x = 0x%02x, 0x%x = 0x%02x, 0x%x = 0x%02x\n", IROCV_RELAY_SEL1, SEL_RESET, IROCV_RELAY_SEL2, SEL_RESET, IROCV_RELAY_SEL3, SEL_RESET);
	return TRUE;
}

BOOL CIROCVPort::WriteToPort(int nPort, BYTE data)
{
	if(m_bInitedCard == FALSE)	return FALSE;

//		TRACE("0x%x Port write 0x%x\n", nPort, data);
//		_outp(nPort, data);

	if(nPort == 0  && data == 0)
	{
		PISODIO_OutputByte(m_wGetAddrBase + 0xcc, m_OutPutPort1Data);		//OuptPut Port1;			
		return TRUE;
	}
	else
	{
		if(nPort >= IOCARD_OUTPUT_PORT1 && nPort <= IOCARD_OUTPUT_PORT4)
		{
			PISODIO_OutputByte(m_wGetAddrBase + 0xc0 + 4*(nPort-1), data);
		}
		return TRUE;
	}
	return FALSE;
}

BOOL CIROCVPort::JigDown()
{
	if(m_JigState == JIG_CLOSE)	return TRUE;

	::ResetEvent(m_hJigDownEvent);
//	m_OutPutPort1Data |= UPDOWN_CYLINDER_DOWN_SOL;
	m_OutPutPort1Data = (m_OutPutPort1Data | UPDOWN_CYLINDER_DOWN_SOL) & ~UPDOWN_CYLINDER_UP_SOL;
	WriteToPort();

	if(m_updownState == JIG_UP)
	{
		if(::WaitForSingleObject(m_hJigDownEvent, JIG_UPDOWN_TIMEOUT) != WAIT_OBJECT_0)	//WaitForJigDown, Down sensor Check;
		{
			::ResetEvent(m_hJigDownEvent);
			TRACE("Jig Down Time Out\n");
			return FALSE;
		}
	}
	::ResetEvent(m_hJigDownEvent);
//	m_OutPutPort1Data &= ~UPDOWN_CYLINDER_DOWN_SOL;
//	WriteToPort(IROCV_OUTPUT_PORT1, m_OutPutPort1Data);

	Sleep(1000);

	::ResetEvent(m_hLeftCylinderOpenCloseEvent);
	::ResetEvent(m_hRightCylinderOpenCloseEvent);
//	m_OutPutPort1Data |= GRIPPER_CLOSE_SOL;
	m_OutPutPort1Data = (m_OutPutPort1Data| GRIPPER_CLOSE_SOL) & ~GRIPPER_OPEN_SOL;
	WriteToPort();

	if(m_leftCylinderState == CYLINDER_OPEN)
	{
		if(::WaitForSingleObject(m_hLeftCylinderOpenCloseEvent, GRIPPER_OPENCLOSE_TIMEOUT) != WAIT_OBJECT_0)
		{
			TRACE("Left cyclinder Close Time Out\n");
			::ResetEvent(m_hLeftCylinderOpenCloseEvent);
			::ResetEvent(m_hRightCylinderOpenCloseEvent);
			return FALSE;
		}
	}

	if(m_rightCylinderState == CYLINDER_OPEN)
	{
		if(::WaitForSingleObject(m_hRightCylinderOpenCloseEvent, GRIPPER_OPENCLOSE_TIMEOUT) != WAIT_OBJECT_0)
		{
			TRACE("Right cyclinder Close Time Out\n");
			::ResetEvent(m_hLeftCylinderOpenCloseEvent);
			::ResetEvent(m_hRightCylinderOpenCloseEvent);
			return FALSE;
		}
	}
	::ResetEvent(m_hLeftCylinderOpenCloseEvent);
	::ResetEvent(m_hRightCylinderOpenCloseEvent);

//	m_OutPutPort1Data &= ~GRIPPER_CLOSE_SOL;
//	WriteToPort(IROCV_OUTPUT_PORT1, m_OutPutPort1Data);
	m_JigState = JIG_CLOSE;
	return TRUE;
}

BOOL CIROCVPort::JigUp()
{
	if(m_JigState == JIG_OPEN)	return TRUE;

	::ResetEvent(m_hLeftCylinderOpenCloseEvent);
	::ResetEvent(m_hRightCylinderOpenCloseEvent);

//	m_OutPutPort1Data |= GRIPPER_OPEN_SOL;
	m_OutPutPort1Data = (m_OutPutPort1Data | GRIPPER_OPEN_SOL ) & ~GRIPPER_CLOSE_SOL;
	WriteToPort();
		
	if(m_leftCylinderState == CYLINDER_CLOSE)
	{
		//Wait For Gripper Open, Open Sensor Check;
		if(::WaitForSingleObject(m_hLeftCylinderOpenCloseEvent, GRIPPER_OPENCLOSE_TIMEOUT) != WAIT_OBJECT_0)
		{
			TRACE("Left cyclinder Open Time Out\n");
			::ResetEvent(m_hLeftCylinderOpenCloseEvent);
			::ResetEvent(m_hRightCylinderOpenCloseEvent);
			return FALSE;
		}
	}

	if(m_rightCylinderState == CYLINDER_CLOSE)
	{
		if(::WaitForSingleObject(m_hRightCylinderOpenCloseEvent, GRIPPER_OPENCLOSE_TIMEOUT) != WAIT_OBJECT_0)
		{
			TRACE("Right cyclinder Open Time Out\n");
			::ResetEvent(m_hLeftCylinderOpenCloseEvent);
			::ResetEvent(m_hRightCylinderOpenCloseEvent);
			return FALSE;
		}
	}
	
	::ResetEvent(m_hLeftCylinderOpenCloseEvent);
	::ResetEvent(m_hRightCylinderOpenCloseEvent);
//	m_OutPutPort1Data &= ~GRIPPER_OPEN_SOL;
//	WriteToPort(IROCV_OUTPUT_PORT1, m_OutPutPort1Data);

	Sleep(1000);

	::ResetEvent(m_hJigUpEvent);
//	m_OutPutPort1Data |= UPDOWN_CYLINDER_UP_SOL;
	m_OutPutPort1Data = (m_OutPutPort1Data | UPDOWN_CYLINDER_UP_SOL) & ~UPDOWN_CYLINDER_DOWN_SOL;

	WriteToPort();

	//Wait For Jig Up, Up sensor Check;
	if(m_updownState == JIG_DOWN)
	{
		if(::WaitForSingleObject(m_hJigUpEvent, JIG_UPDOWN_TIMEOUT) != WAIT_OBJECT_0)
		{
			::ResetEvent(m_hJigUpEvent);
			TRACE("Jig Up Time Out\n");
			return FALSE;
		}
	}
	::ResetEvent(m_hJigUpEvent);

//	m_OutPutPort1Data &= ~UPDOWN_CYLINDER_UP_SOL;
//	WriteToPort(IROCV_OUTPUT_PORT1, m_OutPutPort1Data);
	m_JigState = JIG_OPEN;
	return TRUE;
}

BOOL CIROCVPort::SignTower(BOOL bTurnOn)
{
	if(bTurnOn)
	{
//		m_OutPutPort2Data |= SIGN_LAMP_ON;
		m_OutPutPort1Data |= SIGN_LAMP_ON;
	}
	else
	{
//		m_OutPutPort2Data &= ~SIGN_LAMP_ON;
		m_OutPutPort1Data &= ~SIGN_LAMP_ON;
	}

	WriteToPort();
	return TRUE;
}

BOOL CIROCVPort::BuzzerAlarm(BOOL bAlarm, int mSec)
{
	if(bAlarm)
	{
		m_OutPutPort1Data |= BUZZER_ALARM;
	}
	else
	{
		m_OutPutPort1Data &= ~BUZZER_ALARM;
	}
	WriteToPort();
	return TRUE;
}

WORD CIROCVPort::GetJigState()
{
	return m_JigState;
}

int CIROCVPort::TrayState()
{
	return m_nTrayState;
}

BOOL CIROCVPort::InitIOCard()
{
	if(m_bInitedCard)
	{
		TRACE("초기화 되어 있습니다.\n");
		return FALSE;
	}

	m_wInitialCode = PISODIO_DriverInit();
	if( m_wInitialCode != PISODIO_NoError )
    {
		return  FALSE;
//		MessageBox("No PIO or PISO card in this system !!!","PIO Card Error",MB_OK);
    }

    PISODIO_SearchCard(&m_wTotalBoard, PISO_P32C32);
    if( m_wTotalBoard==0 )
    {
	    Beep(100,100);
//		MessageBox("No PISO-P32C32 card in this system !!!","PISO Card Error",MB_OK);
        return FALSE;
	}

//	char str[32];
	// get the first board information and display  it
	WORD wRetVal = PISODIO_GetConfigAddressSpace(0, &m_wGetAddrBase, &m_wGetIrqNo,
                &m_wGetSubVendor,&m_wGetSubDevice,&m_wGetSubAux,&m_wGetSlotBus,&m_wGetSlotDevice);
	if( wRetVal == PISODIO_NoError )
	{
	   TRACE("Base Address :0x%04x\n", m_wGetAddrBase);
	   TRACE("IRQ : 0x%x", m_wGetIrqNo);
	}
    
	//************************************************************
	// Notice: To Enable all DI/DO port for this board
    //************************************************************
	PISODIO_OutputByte(m_wGetAddrBase, 1);   // enable DI/DO
	
	ResetSelect();

	m_bInitedCard = TRUE;
	return TRUE;
}

void CIROCVPort::CloseIOCard()
{
	if(m_bInitedCard == FALSE)	return;

	PISODIO_DriverClose();         // close the driver
	m_bInitedCard = FALSE;
}


BYTE CIROCVPort::ReadFromPort(int nPort)
{
	if(m_bInitedCard == FALSE)	return 0;
	if(nPort < IOCARD_INPUT_PORT1 || nPort <IOCARD_INPUT_PORT1)		return 0;

	return ~(BYTE)PISODIO_InputByte(m_wGetAddrBase+0xc0+(nPort-1)*4);
}

void CIROCVPort::DoorOpen()
{
	m_doorState = DOOR_OPEN;
	m_OutPutPort1Data &= ~DOOR_LAMP_ON;
	WriteToPort();

	if(m_pOwner->m_hWnd != NULL)
		::PostMessage(m_pOwner->m_hWnd, WM_DOOR_OPEN, 0, 0);
	TRACE("Door Open Detected\n");
}

void CIROCVPort::DoorClose()
{
	m_doorState = DOOR_CLOSE;
	m_OutPutPort1Data |= DOOR_LAMP_ON;
	WriteToPort();		
	if(m_pOwner->m_hWnd != NULL)
		::PostMessage(m_pOwner->m_hWnd, WM_DOOR_CLOSE, 0, 0);
	TRACE("Door Close Detected\n");
}

void CIROCVPort::TrayLoaded()
{
	m_nTrayState = TRAY_LOAD;
//	m_OutPutPort1Data |= TRAY_LAMP_ON;
//	WriteToPort();		
	if(m_pOwner->m_hWnd != NULL)
		::PostMessage(m_pOwner->m_hWnd, WM_TRAY_LOADED, 0, 0);
	TRACE("Tray In Detected\n");
}

void CIROCVPort::TrayUnLoaded()
{
	m_nTrayState = TRAY_UNLOAD;
//	m_OutPutPort1Data &= ~TRAY_LAMP_ON;
//	WriteToPort();
	if(m_pOwner->m_hWnd != NULL)
		::PostMessage(m_pOwner->m_hWnd, WM_TRAY_UNLOADED, 0, 0);
	TRACE("Tray Out Detected\n");
}
