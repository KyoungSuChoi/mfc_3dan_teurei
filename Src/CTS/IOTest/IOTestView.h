// IOTestView.h : interface of the CIOTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IOTESTVIEW_H__1EC28165_9370_4C12_B9A5_5774DC7EE3C7__INCLUDED_)
#define AFX_IOTESTVIEW_H__1EC28165_9370_4C12_B9A5_5774DC7EE3C7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "telnetSocket.h"
#include "ClientSocket.h"
#include "LedButton.h"
#include "telnetdef.h"


#define ID_RX_TIME_OUT_TIMER	100
#define ID_DEMO_SEQUENCE_TIMER	101
#define ID_LOCAL_IOCARD_SCAN_TIMER	102
#define ID_AUTO_SEQUENCE_TIMER	103


#define MAX_RX_ERR_COUNT	1
#define MAX_TX_ERR_COUNT	1

#define MAX_AUTO_SEQUENCE	100

typedef struct sequence_Data {
	WORD	address;
	BYTE	data;
	UINT	interval;
} Out_Data;
//#include "IROCVPort.h"

#define TELNET_CMD_MODE_REBOOT	0
#define TELNET_CMD_MODE_HALT	1
#define	TELENT_CMD_MODE_KILL_PROCESS 3


#define EP_STATE_LINE_OFF		0x0100
#define EP_STATE_LINE_ON		0x0101
#define EP_STATE_EMERGENCY		0x0200
#define EP_STATE_ERROR			0xFFFF

#define EP_STATE_IDLE			0x0000
#define EP_STATE_SELF_TEST		0x0001
#define EP_STATE_STANDBY		0x0002
#define EP_STATE_RUN			0x0003
#define EP_STATE_PAUSE			0x0004
#define EP_STATE_FAIL			0x0005
#define EP_STATE_MAINTENANCE	0x0006
#define EP_STATE_OCV			0x0007
#define EP_STATE_CHARGE			0x0008
#define EP_STATE_DISCHARGE		0x0009
#define EP_STATE_REST			0x000A
#define EP_STATE_IMPEDANCE		0x000B
#define EP_STATE_CHECK			0x000C
#define EP_STATE_STOP			0x000D
#define EP_STATE_END			0x000E
#define EP_STATE_FAULT			0x000F
#define EP_STATE_COMMON			0x0010
#define EP_STATE_NONCELL		0x0011
#define EP_STATE_READY			0x0012


class CIOTestView : public CFormView
{
protected: // create from serialization only
	CIOTestView();
	DECLARE_DYNCREATE(CIOTestView)

public:
	//{{AFX_DATA(CIOTestView)
	enum { IDD = IDD_IOTEST_FORM };
	CLedButton	m_rxState;
	CLedButton	m_txState;
	CIPAddressCtrl	m_ctrlIPAddress;
	CProgressCtrl	m_wndProgress;
	UINT	m_nTotalCount;
	//}}AFX_DATA

// Attributes
public:
	CIOTestDoc* GetDocument();
	

// Operations
public:
	unsigned char m_bBuf[ioBuffSize];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIOTestView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	CString m_strProjectName;
	WORD m_nModuleState;
	WORD UpdateModuleState();
	int m_nTelnetMode;
	int m_bTelnetCmdPhase;
	void DisConnectTelnet();
	BOOL ConnectTelnet();
	void SetControlLayout(BOOL bSensorTest);
//	CIROCVPort	m_IROCVPort;
	int m_nMaxAutoSequence;
	COleDateTime m_startTime;
	int m_nTotalSqCount;
	int m_nRemainTime;
	int m_nRepeatCount;
	INT m_nSeq;
	Out_Data	m_AutoSequence[MAX_AUTO_SEQUENCE];

	BOOL SendRequest();
	CString m_strCurdir;
	BOOL m_timeOutFlag;
	BOOL ConnectModule(CString strIP);
	BOOL m_bOutMask;
	int m_nTxCount;
	int m_nRxCount;
//	BOOL m_bRx;
//	BOOL m_bTx;
	int Hex2Dec(CString hexData);
	CString m_strLineData;
	void ParsingMessage(LPCSTR pText);
	void RollBackOutState(int nPort);
	BOOL StateCheck(int nPort, BYTE data);
	void ParsingConfig(CString strConfig);
	BOOL LoadSetting();
	CString m_strIPAddress;
	CLedButton m_stateButton[40];
	void Connected();
	void WriteData(UINT address);
	void UpdateState(UINT address, BYTE data);

	UINT	m_nInputAddress[5];
	UINT	m_nOutputAddress[5];
	BYTE	m_inputData[5];
	BYTE	m_outputData[5];
	BYTE	m_outputMask[40];


	CString	m_strInputName[40];
	CString	m_strOutputName[40];

	BYTE ParsingData(CString msg);
	CString m_strSendedData;
	CString m_strExecuteCmd;
	CString m_strDir;
	CString m_strRootPassword;
	CString m_strUserPassword;
	CString m_strUserID;
	int SendData(CString strData);
	int m_nPhase;
	void MessageReceived(LPCSTR pText);
	void DispatchMessage(CString strText);
	CString m_strResp;
	void ArrangeReply(CString strOption);
	void RespondToOptions();
	CStringList m_ListOptions;
	CString m_strNormalText;
	void ProcessOptions();
	CString m_strLine;
	BOOL GetLine( unsigned char * bytes, int nBytes);
	CTelnetSock *m_pClientSock;
	CClientSocket *m_pTelnetSock;	//for 

	virtual ~CIOTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CIOTestView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCheck33();
	afx_msg void OnCheck34();
	afx_msg void OnCheck35();
	afx_msg void OnCheck36();
	afx_msg void OnCheck37();
	afx_msg void OnCheck38();
	afx_msg void OnCheck39();
	afx_msg void OnCheck40();
	afx_msg void OnCheck41();
	afx_msg void OnCheck42();
	afx_msg void OnCheck43();
	afx_msg void OnCheck44();
	afx_msg void OnCheck45();
	afx_msg void OnCheck46();
	afx_msg void OnCheck47();
	afx_msg void OnCheck48();
	afx_msg void OnCheck49();
	afx_msg void OnCheck50();
	afx_msg void OnCheck51();
	afx_msg void OnCheck52();
	afx_msg void OnCheck53();
	afx_msg void OnCheck54();
	afx_msg void OnCheck55();
	afx_msg void OnCheck56();
	afx_msg void OnCheck57();
	afx_msg void OnCheck58();
	afx_msg void OnCheck59();
	afx_msg void OnCheck60();
	afx_msg void OnCheck61();
	afx_msg void OnCheck62();
	afx_msg void OnCheck63();
	afx_msg void OnCheck64();
	afx_msg void OnConnect();
	afx_msg void OnDisconnect();
	afx_msg void OnClose();
	afx_msg void OnAutoDemoStart();
	afx_msg void OnAutoDemoStop();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnRebootButton();
	afx_msg void OnHaltButton();
	afx_msg void OnKillProcessButton();
	afx_msg void OnCheck73();
	afx_msg void OnCheck74();
	afx_msg void OnCheck75();
	afx_msg void OnCheck76();
	afx_msg void OnCheck77();
	afx_msg void OnCheck78();
	afx_msg void OnCheck79();
	afx_msg void OnCheck80();
	//}}AFX_MSG
	afx_msg LONG OnReceive(UINT,LONG);
	afx_msg LONG OnServerConnected(UINT,LONG);
	afx_msg LONG OnServerDisConnected(UINT,LONG);
	afx_msg LONG OnCmdStandBy(UINT,LONG);
	afx_msg LONG OnUpdateComplite(UINT,LONG);
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bFlash;
};

#ifndef _DEBUG  // debug version in IOTestView.cpp
inline CIOTestDoc* CIOTestView::GetDocument()
   { return (CIOTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IOTESTVIEW_H__1EC28165_9370_4C12_B9A5_5774DC7EE3C7__INCLUDED_)
