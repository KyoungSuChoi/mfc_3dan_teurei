// IOTestDoc.cpp : implementation of the CIOTestDoc class
//

#include "stdafx.h"
#include "IOTest.h"

#include "IOTestDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIOTestDoc

IMPLEMENT_DYNCREATE(CIOTestDoc, CDocument)

BEGIN_MESSAGE_MAP(CIOTestDoc, CDocument)
	//{{AFX_MSG_MAP(CIOTestDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIOTestDoc construction/destruction

CIOTestDoc::CIOTestDoc()
{
	// TODO: add one-time construction code here
	m_bUseOutPut = TRUE;
	m_bSimpleTestMode = FALSE;
}

CIOTestDoc::~CIOTestDoc()
{
}

BOOL CIOTestDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CIOTestDoc serialization

void CIOTestDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CIOTestDoc diagnostics

#ifdef _DEBUG
void CIOTestDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CIOTestDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIOTestDoc commands

