// CalFile.h : main header file for the CALFILE application
//

#if !defined(AFX_CALFILE_H__E9670FE7_C560_48D1_9A6D_2516F3025E4D__INCLUDED_)
#define AFX_CALFILE_H__E9670FE7_C560_48D1_9A6D_2516F3025E4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCalFileApp:
// See CalFile.cpp for the implementation of this class
//

class CCalFileApp : public CWinApp
{
public:
	CCalFileApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalFileApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCalFileApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALFILE_H__E9670FE7_C560_48D1_9A6D_2516F3025E4D__INCLUDED_)
