//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CalFile.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CALFILE_DIALOG              102
#define IDS_TEXT_FILENOT                102
#define IDS_TEXT_TRANSMIT               103
#define IDS_TEXT_MISSPOSITION           104
#define IDS_FORMAT_FILE_NOT_FOUND       105
#define IDS_FORMAT_UPDATE               106
#define IDS_FORMAT_ALL_UPDATE           107
#define IDS_FORMAT_IP_NOT_FOUND         108
#define IDS_TEXT_UPDATE_FAIL            109
#define IDR_MAINFRAME                   128
#define IDI_MAIN                        129
#define IDD_CALIBRATION_DLG             243
#define IDD_IP_SET_DLG                  251
#define IDC_STATIC_PATH                 1000
#define IDC_IPADDRESS1                  1158
#define IDC_UPDATE_BUTTON               1200
#define IDC_SEL_MODULE_COMBO            1201
#define IDC_CAL_FILE_LIST               1202
#define IDC_ALL_UPDATE_BUTTON           1203
#define IDC_MODULE_FILE_LIST            1204
#define IDC_MODULE_DATA_LABEL           1243

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
