// CalFileDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CalFile.h"
#include "CalFileDlg.h"
#include "IPSetDlg.h"

#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalFileDlg dialog


CCalFileDlg::CCalFileDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCalFileDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCalFileDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nInstalledModuleNum = 0;

	m_strModuleName = "Rack";	
	m_strGroupName =  "Group";	
	m_nModulePerRack = 3;
	m_bUseRackIndex = TRUE;
	m_bUseGroupSet = FALSE;
	m_strCurPath = ".";
	m_strPassword = "dusrn1";
	m_strLoginID = "root";
	m_strLocation = "/";
}


void CCalFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCalFileDlg)
	DDX_Control(pDX, IDC_MODULE_FILE_LIST, m_ctrlModuleFileList);
	DDX_Control(pDX, IDC_CAL_FILE_LIST, m_ctrlFileList);
	DDX_Control(pDX, IDC_SEL_MODULE_COMBO, m_ctrlModuleSel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCalFileDlg, CDialog)
	//{{AFX_MSG_MAP(CCalFileDlg)
	ON_BN_CLICKED(IDC_UPDATE_BUTTON, OnUpdateButton)
	ON_CBN_SELCHANGE(IDC_SEL_MODULE_COMBO, OnSelchangeSelModuleCombo)
	ON_BN_CLICKED(IDC_ALL_UPDATE_BUTTON, OnAllUpdateButton)
	ON_NOTIFY(NM_DBLCLK, IDC_MODULE_FILE_LIST, OnDblclkModuleFileList)
	ON_NOTIFY(NM_DBLCLK, IDC_CAL_FILE_LIST, OnDblclkCalFileList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCalFileDlg message handlers

BOOL CCalFileDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	LoadSetting();
	
	m_ctrlModuleSel.AddString("All");
	m_ctrlModuleSel.SetItemData(0, 0);
	CString strTemp;

	CDaoDatabase  db;
	db.Open(m_strDataBaseName);

	CString strSQL("SELECT ModuleID, Data3 FROM SystemConfig ORDER BY ModuleID");
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	while(!rs.IsEOF())
	{
		COleVariant data = rs.GetFieldValue(0);
		COleVariant data1 = rs.GetFieldValue(1);
		strTemp = data1.pcVal;
		if(strTemp.IsEmpty())
		{
			strTemp = GetModuleName(data.lVal);
		}
		m_ctrlModuleSel.AddString(strTemp);
		m_ctrlModuleSel.SetItemData(++m_nInstalledModuleNum, data.lVal);
		rs.MoveNext();

		//폴더가 존재 하지 않을 경우 폴더 생성 
		strTemp =  m_strCurPath + "\\CalData\\" + strTemp;
		if(_access((LPCTSTR)strTemp , 0) == -1)
		{
			CreateDirectory((LPCTSTR)strTemp, NULL);
		}
	}
	rs.Close();
	db.Close();	
	
	m_ctrlModuleSel.SetCurSel(0);
	InitList();

	BeginWaitCursor();
	LoadFileList();
	EndWaitCursor();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCalFileDlg::OnUpdateButton() 
{
	// TODO: Add your control notification handler code here
	CString strTemp, strform;

	int nIndex = m_ctrlModuleSel.GetCurSel();
	if(nIndex < 1)
	{
		OnAllUpdateButton();
		return;
	}
	int nModuleID = m_ctrlModuleSel.GetItemData(nIndex);
	strform.LoadString(IDS_FORMAT_UPDATE);
	strTemp.Format(strform, GetModuleName(nModuleID));
	if(MessageBox(strTemp, "확인", MB_YESNO|MB_ICONQUESTION) == IDYES)
	{
		int nSelected = -1;
		CStringArray aFileName;

		while((nSelected = m_ctrlFileList.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
		{
			aFileName.Add(m_ctrlFileList.GetItemText(nSelected , 0 ));
		}
		if(aFileName.GetSize() <= 0)
		{
			AfxMessageBox(IDS_TEXT_FILENOT);
			return;
		}
		if(SendFileToModule(nModuleID, aFileName))
			UpdateModuleFileList(nModuleID);
	}
}

inline CString CCalFileDlg::GetModuleName(int nModuleID, int nGroupIndex)
{
	CString strName;

	for(int i=0; i<m_ctrlModuleSel.GetCount(); i++)
	{
		if(nModuleID == m_ctrlModuleSel.GetItemData(i))
		{
			m_ctrlModuleSel.GetLBText(i, strName);
			break;
		}
	}

	if(strName.IsEmpty() == FALSE)	return strName;
	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}


/*
BOOL CCalFileDlg::SetModuleNum(int nNum)
{
	if(nNum < 0)	return FALSE;

	m_nInstalledModuleNum = nNum;
	return TRUE;
}
*/
BOOL CCalFileDlg::InitList()
{
	CRect rect;
	CString strTemp;
	strTemp = m_strModuleName + " No.";
	m_ctrlFileList.GetClientRect(&rect);
	int nColInterval = rect.Width()/10;
	m_ctrlFileList.InsertColumn(0, _T("File Name"), LVCFMT_LEFT, nColInterval*3);
	m_ctrlFileList.InsertColumn(1, _T(strTemp), LVCFMT_LEFT, nColInterval*1);
	m_ctrlFileList.InsertColumn(2, _T("Board No."), LVCFMT_LEFT, nColInterval*1);
	m_ctrlFileList.InsertColumn(3, _T("Cali. Date"), LVCFMT_LEFT, nColInterval*5);
	m_ctrlFileList.SetRedraw(FALSE);
	m_ctrlFileList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlFileList.ModifyStyle(0, LVS_REPORT);
	m_ctrlFileList.SetRedraw(TRUE);
	m_ctrlFileList.DeleteAllItems();

	m_ctrlModuleFileList.GetClientRect(&rect);
	nColInterval = rect.Width()/10;
	m_ctrlModuleFileList.InsertColumn(0, _T("Board No."), LVCFMT_LEFT, nColInterval*3);
	m_ctrlModuleFileList.InsertColumn(1, _T("Cali. Date"), LVCFMT_LEFT, nColInterval*7);
	m_ctrlModuleFileList.SetRedraw(FALSE);
	m_ctrlModuleFileList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlModuleFileList.ModifyStyle(0, LVS_REPORT);
	m_ctrlModuleFileList.SetRedraw(TRUE);
	m_ctrlModuleFileList.DeleteAllItems();
	
	return TRUE;
}

BOOL CCalFileDlg::LoadFileList(int nModuleID)
{
	//Delete Real Time Save File
	CString strTemp;
//	LVITEM lvi;
	CString strItem;

	
	if(nModuleID > 0)
		strTemp.Format("%s\\CalData\\%s", m_strCurPath, GetModuleName(nModuleID));
	else
		strTemp.Format("%s\\CalData", m_strCurPath);

	GetDlgItem(IDC_STATIC_PATH)->SetWindowText(strTemp+"\\*.cff");

	m_ctrlFileList.DeleteAllItems();
	Recurse(strTemp, nModuleID);

/*	CFileFind 	finder;
	BOOL   bWorking = finder.FindFile(strTemp);

	int nCount = 0;
	while (bWorking)
	{
		bWorking	= finder.FindNextFile();
		strTemp = finder.GetFileName();
	
		lvi.mask =  LVIF_TEXT;
		lvi.iItem = nCount++;
		lvi.iSubItem = 0;
		lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
		m_ctrlFileList.InsertItem(&lvi);
	}
	finder.Close();
*/	return TRUE;
}


void CCalFileDlg::Recurse(CString strCurLocation, int nModuleID)
{
	CFileFind finder;

   // build a string with wildcards
	CString strWildcard(strCurLocation);
	strWildcard += _T("\\*.*");

   // start working for files
	BOOL bWorking = finder.FindFile(strWildcard);
	LVITEM lvi;

	int nCount = 0;
	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		// skip . and .. files; otherwise, we'd
		// recur infinitely!

		if (finder.IsDots())
			continue;

		// if it's a directory, recursively search it

		if (finder.IsDirectory())
		{
			CString str = finder.GetFilePath();
			Recurse(str, nModuleID);
		}
		else
		{
			CString strFileName = finder.GetFileName();
			if(strFileName.Mid(strFileName.ReverseFind('.')+1) == "cff")
			{
				lvi.mask =  LVIF_TEXT;
				lvi.iItem = nCount++;
				lvi.iSubItem = 0;
				lvi.pszText = (LPTSTR)(LPCTSTR)(strFileName);
				m_ctrlFileList.InsertItem(&lvi);
				
				strFileName = finder.GetFilePath();
				FILE *fp = fopen((LPSTR)(LPCTSTR)strFileName, "rt");
				if(fp != NULL)
				{
					char szBuff[128];
					fscanf(fp, "%s", szBuff);		//File serial
					fscanf(fp, "%s", szBuff);		//Board Serial


					fscanf(fp, "%s", szBuff);		//date Time					
					lvi.iSubItem = 3;
					lvi.pszText = szBuff;
					m_ctrlFileList.SetItem(&lvi);
					
					if(nModuleID > 0)
					{
						lvi.iSubItem = 1;
						sprintf(szBuff, "%d", nModuleID);
						lvi.pszText = szBuff;
						m_ctrlFileList.SetItem(&lvi);
					}

					int data;
					fscanf(fp, "%d", &data);		//Board Num
					lvi.iSubItem = 2;
					sprintf(szBuff, "%d", data);
					lvi.pszText = szBuff;
					m_ctrlFileList.SetItem(&lvi);
					
					fclose(fp);
				}
			}
		}	
	}
	finder.Close();
}

void CCalFileDlg::OnSelchangeSelModuleCombo() 
{
	// TODO: Add your control notification handler code here
	int nID = 0;
	int nIndex = m_ctrlModuleSel.GetCurSel();
	CString strTemp;

	if(nIndex > 0)
	{
		nID = m_ctrlModuleSel.GetItemData(nIndex);
		m_ctrlModuleSel.GetLBText(nIndex, strTemp);
		strTemp += " Calibration Data";
	}
	else
	{
		strTemp = "Calibration Data";
	}

	GetDlgItem(IDC_MODULE_DATA_LABEL)->SetWindowText(strTemp);
	
	LoadFileList(nID);

	if(nID > 0)
	{
		GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(UpdateModuleFileList(nID));
	}
}

void CCalFileDlg::OnAllUpdateButton() 
{
	// TODO: Add your control notification handler code here
	CString strTemp, strform;
	strform.LoadString(IDS_FORMAT_ALL_UPDATE);
	strTemp.Format(strform, m_nInstalledModuleNum, m_strModuleName);
	if(MessageBox(strTemp, "Confirm", MB_YESNO|MB_ICONQUESTION) == IDYES)
	{

	}
}

BOOL CCalFileDlg::SendFileToModule(int nModuleID, CStringArray &strArray)
{
	CString strIPAddress;
	BOOL nRtn = TRUE;
	strIPAddress = GetIPAddress(nModuleID);

	if(strArray.GetSize() <= 0 || strIPAddress.IsEmpty())	return FALSE;
 
	CString toFileName, fromFileName;
	CString path;
	CString source, destination;
	CString strTemp;



	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;
	int nBoardNo;

//	timeLocal = COleDateTime::GetCurrentTime();

	BeginWaitCursor();

	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(strIPAddress, m_strLoginID, m_strPassword);
		if(!pFtpConnection->SetCurrentDirectory(m_strLocation))
		{
			AfxMessageBox(IDS_TEXT_MISSPOSITION);
			nRtn = FALSE;
			goto _EXIT_;
		}

#ifdef _DEBUG
		CString strFile;
		CFtpFileFind pFileFind(pFtpConnection);
		BOOL bWorking = pFileFind.FindFile(_T("*"));
		while (bWorking)
		{
			bWorking = pFileFind.FindNextFile();
			strFile = pFileFind.GetFileName();
			TRACE("%s\n", strFile);
		}
#endif

	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();

		ftpSession.Close();
		EndWaitCursor();
		return FALSE;
	}
	
	if(nModuleID > 0)
		path.Format("%s\\CalData\\%s", m_strCurPath, GetModuleName(nModuleID));
	else
		path.Format("%s\\CalData", m_strCurPath);


	try
	{
		for(int i = 0; i<strArray.GetSize(); i++)
		{
			fromFileName = path + "\\"+ strArray.GetAt(i);
			nBoardNo = GetBoardNo(fromFileName);
			if(!fromFileName.IsEmpty() && nBoardNo > 0)
			{
				source.Format("L_CALI_C%d", nBoardNo);
//				destination.Format("L_CALI_C%d_%s", nBoardNo, timeLocal.Format("%Y%m%d%H%M%s"));
				destination.Format("L_CALI_C%d_Back", nBoardNo);
				pFtpConnection->Rename(source, destination);		//BackUp Org File

				toFileName = source;
				if(pFtpConnection->PutFile(fromFileName, toFileName) == FALSE)
				{
					strTemp.LoadString(IDS_TEXT_UPDATE_FAIL);
					MessageBox(fromFileName + strTemp, "Send Fail", MB_OK|MB_ICONSTOP);
					nRtn = FALSE;
				}
			}
		}
		EndWaitCursor();
		AfxMessageBox(IDS_TEXT_TRANSMIT);
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
		EndWaitCursor();
		nRtn = FALSE;
	}

_EXIT_:
	pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;
	
	return nRtn;
}

CString CCalFileDlg::GetIPAddress(int nModuleID)
{
	CString address;
	// 접속된 모듈 
/*	if(EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)
	{
		address = EPGetModuleIP(nModuleID);
	}
	if(!address.IsEmpty())	return address;
*/	
	//DataBase 검색 
	CDaoDatabase  db;

	if(!m_strDataBaseName.IsEmpty())
	{
		db.Open(m_strDataBaseName);

		CString strSQL;
		strSQL.Format("SELECT IP_Address FROM SystemConfig WHERE ModuleID = %d", nModuleID);
		
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data = rs.GetFieldValue(0);
			address = data.pbVal;
		}

		rs.Close();
		db.Close();
	}
	
	//수동 입력 
	if(address.IsEmpty())
	{
		CIPSetDlg dlg;
		if(dlg.DoModal() == IDOK)
			address = dlg.GetIPAddress();
	}

	return address;
}

int CCalFileDlg::GetBoardNo(CString strFileName)
{
	if(strFileName.IsEmpty())	return 0;
	int data = 0;

	FILE *fp = fopen((LPSTR)(LPCTSTR)strFileName, "rt");
	if(fp != NULL)
	{
		char szBuff[128];
		fscanf(fp, "%s", szBuff);		//File serial
		fscanf(fp, "%s", szBuff);		//Board Serial
		fscanf(fp, "%s", szBuff);		//date Time					
		fscanf(fp, "%d", &data);		//Board Num
		fclose(fp);
	}
	return data;
}

BOOL CCalFileDlg::UpdateModuleFileList(int nModuleID)
{
	m_ctrlModuleFileList.DeleteAllItems();


	CString strIPAddress, strform;
	BOOL nRtn = TRUE;
	strIPAddress = GetIPAddress(nModuleID);

	if(strIPAddress.IsEmpty())
	{
		strform.LoadString(IDS_FORMAT_IP_NOT_FOUND);
		strIPAddress.Format(strform, GetModuleName(nModuleID));
		AfxMessageBox(strIPAddress, MB_ICONSTOP|MB_OK);
		return FALSE;
	}
 
	CString toFileName, fromFileName;
	CString path;
	CString source, destination;
	CString strTemp;

	BeginWaitCursor();
	
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;

	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(strIPAddress, m_strLoginID, m_strPassword);
		if(pFtpConnection != NULL && !pFtpConnection->SetCurrentDirectory(m_strLocation))
		{
			strTemp.LoadString(IDS_TEXT_MISSPOSITION);
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			nRtn = FALSE;
			goto _EXIT_;
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();

		ftpSession.Close();
		EndWaitCursor();
		return FALSE;
	}
	
	
	try
	{
		path = m_strCurPath +"\\Temp\\";
		int nBoard = 8;

		for(int bd = 0; bd <nBoard; bd++)
		{
			fromFileName.Format("L_CALI_C%d", bd+1);
			toFileName.Format("L_CALI_C%d.txt", bd+1);
			toFileName = path + toFileName;
			_unlink(toFileName);			//파일이 있으면 삭제 
			
			if(pFtpConnection->GetFile(fromFileName, toFileName))
			{
				if(AddModuleFileList(toFileName)== FALSE)
				{
					TRACE("Fail Adding Module File List\n");
				}
			}
			else
			{
				
				TRACE("%s Calibration File Down Load Fail\n", fromFileName);
				break;
			}
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
//		AfxMessageBox(sz);
		e->Delete();
		nRtn = FALSE;
	}

_EXIT_:
	pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;
	
	EndWaitCursor();
	return nRtn;
}

BOOL CCalFileDlg::AddModuleFileList(CString strFileName)
{
	if(strFileName.IsEmpty())	return FALSE;

	FILE *fp = fopen((LPSTR)(LPCTSTR)strFileName, "rt");
	CString dateTime;
	char szBuff[128];
	int nBoardNo = 0;

	if(fp != NULL)
	{
		fscanf(fp, "%s", szBuff);		//File serial
		fscanf(fp, "%s", szBuff);		//Board Serial


		fscanf(fp, "%s", szBuff);		//date Time	
		dateTime = szBuff;
		fscanf(fp, "%d", &nBoardNo);		//date Time	
		fclose(fp);
	}

	if(nBoardNo < 1)	return FALSE;
	
	int nCount = m_ctrlModuleFileList.GetItemCount();

	LVITEM lvi;

	lvi.mask =  LVIF_TEXT;
	lvi.iItem = nCount;
	lvi.iSubItem = 0;
	sprintf(szBuff, "%d", nBoardNo);
	lvi.pszText = szBuff;
	m_ctrlModuleFileList.InsertItem(&lvi);
	
	lvi.iSubItem = 1;
	sprintf(szBuff, "%s", dateTime);
	lvi.pszText = szBuff;
	m_ctrlModuleFileList.SetItem(&lvi);
				
	return TRUE;
}

void CCalFileDlg::OnDblclkModuleFileList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nSelected = -1;
	CString path;
	path = m_strCurPath + "\\Temp";

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	char szWinDir[128];
	if(GetWindowsDirectory(szWinDir, 127) <=0 )		return;
	
	CString strTemp, strform;
	CString strFileName;

	while((nSelected = m_ctrlModuleFileList.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
	{
		strTemp = m_ctrlModuleFileList.GetItemText(nSelected , 0 );
		strFileName.Format("%s\\L_CALI_C%s.txt", path, strTemp);
		strTemp.Format("%s\\Notepad.exe %s", szWinDir, strFileName);
		
		BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		if(bFlag == FALSE)
		{
			strform.LoadString(IDS_FORMAT_FILE_NOT_FOUND);
			strTemp.Format(strform, strFileName);
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		}	
	}
	*pResult = 0;
}

void CCalFileDlg::OnDblclkCalFileList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nID = 0;
	int nIndex = m_ctrlModuleSel.GetCurSel();
	if(nIndex < 1)		return;

	nID = m_ctrlModuleSel.GetItemData(nIndex);
//	nID = EPGetModuleID(nIndex-1);

	int nSelected = -1;
	CString strTemp;
	CString path;
	path.Format("%s\\CalData\\%s", m_strCurPath, GetModuleName(nID));

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	char szWinDir[128];
	if(GetWindowsDirectory(szWinDir, 127) <=0 )		return;
	
	CString strFileName, strform;

	while((nSelected = m_ctrlFileList.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
	{
		strTemp = m_ctrlFileList.GetItemText(nSelected , 0 );
		strFileName.Format("%s\\%s", path, strTemp);
		strTemp.Format("%s\\Notepad.exe %s", szWinDir, strFileName);
		
		BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		if(bFlag == FALSE)
		{
			strform.LoadString(IDS_FORMAT_FILE_NOT_FOUND);
			strTemp.Format(strform, strFileName);
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		}	
	}
	*pResult = 0;
}


void CCalFileDlg::LoadSetting()
{
	long rtn;
	HKEY hKey = 0;
	BYTE buf[256], buf2[256];
	DWORD size = 255;
	DWORD type;
	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_READ, &hKey);
	
	//Tray Column Size를 Registry에서 읽어옴 
	if(ERROR_SUCCESS == rtn)
	{
		rtn = ::RegQueryValueEx(hKey, "Module Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strModuleName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Group Name", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strGroupName = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Module Per Rack", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_nModulePerRack = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Rack Index", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseRackIndex = atol((LPCTSTR)buf2);
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Use Group", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && type == REG_DWORD)
		{
			sprintf((char *)buf2, "%ld", *(long *)buf);
			m_bUseGroupSet = atol((LPCTSTR)buf2);
		}
		::RegCloseKey(hKey);
	}

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Path", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		//PowerFormation DataBase 경로를 읽어온다. 
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "CTSMon", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strCurPath = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strDataBaseName.Format("%s\\Schedule.mdb", buf);
		}
		::RegCloseKey(hKey);
	}

	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\Ftp Set", 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == rtn)
	{
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Login ID", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strLoginID = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Login Password", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strPassword = buf;
		}
		size = 255;
		rtn = ::RegQueryValueEx(hKey, "Cal Location", NULL, &type, buf, &size);
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
			m_strLocation = buf;
		}
		::RegCloseKey(hKey);
	}
}
