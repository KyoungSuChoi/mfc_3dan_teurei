#pragma once


// CNormalSensorMapDlg 대화 상자입니다.
#define INPUT_IO_START_ADDRESS	96	
#define INPUT_IO_END_ADDRESS	INPUT_IO_START_ADDRESS + 96 + 96
#define MAX_LED_BUTTON_COUNT	INPUT_IO_END_ADDRESS-INPUT_IO_START_ADDRESS


class CNormalSensorMapDlg : public CDialog
{
	DECLARE_DYNAMIC(CNormalSensorMapDlg)

public:
	CNormalSensorMapDlg(int nDeviceID, bool& bReadySetCmd, IO_Data& sendJigIOData, IO_Data& recvJigIOData, IO_Data& sendFanIOData, IO_Data& recvFanIOData, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNormalSensorMapDlg();

	int m_nDeviceID;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NORMAL_SENSOR_PIC_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
private :
	CString	m_strInputName[156];
	CLedButton m_stateButton[156];

	CLabel m_IOLabelArray[16];
	CColorButton2 m_OnBtnArray[16];
	CColorButton2 m_OffBtnArray[16];

	bool& m_bReadySetCmd;

	IO_Data& m_SendJigIOData;
	IO_Data& m_RecvJigIOData;
	IO_Data& m_SendFanIOData;
	IO_Data& m_RecvFanIOData;

	bool LoadIoConfig();
	void ParsingConfig(CString strConfig);
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	bool UpdateAdditionalSensorState(unsigned char* pSensorState);

	afx_msg void OnBtnClickedOn001();
	afx_msg void OnBtnClickedOn002();
	afx_msg void OnBtnClickedOn003();
	afx_msg void OnBtnClickedOn004();
	afx_msg void OnBtnClickedOn005();
	afx_msg void OnBtnClickedOn006();
	afx_msg void OnBtnClickedOn007();
	afx_msg void OnBtnClickedOn008();
	afx_msg void OnBtnClickedOn009();
	afx_msg void OnBtnClickedOn010();
	afx_msg void OnBtnClickedOn011();
	afx_msg void OnBtnClickedOn012();

	afx_msg void OnBtnClickedOff001();
	afx_msg void OnBtnClickedOff002();
	afx_msg void OnBtnClickedOff003();
	afx_msg void OnBtnClickedOff004();
	afx_msg void OnBtnClickedOff005();
	afx_msg void OnBtnClickedOff006();
	afx_msg void OnBtnClickedOff007();
	afx_msg void OnBtnClickedOff008();
	afx_msg void OnBtnClickedOff009();
	afx_msg void OnBtnClickedOff010();
	afx_msg void OnBtnClickedOff011();
	afx_msg void OnBtnClickedOff012();
};
