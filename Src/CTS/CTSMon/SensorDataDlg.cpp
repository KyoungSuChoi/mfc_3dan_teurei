// SensorDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "SensorDataDlg.h"

#include "MainFrm.h"
#include "PrntScreen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define TEMP_PROGRESS_COLOR	RGB(100, 100, 200)
#define GAS_PROGRESS_COLOR	RGB(240, 100, 0)

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg dialog


CSensorDataDlg::CSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSensorDataDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CSensorDataDlg"));

	m_nModuleID = 0;
	m_pDoc = pDoc;
	ASSERT(m_pDoc);
	m_bLockUpdate = FALSE;

	m_csTemperatureSpecialCharacter = CalcTemperatureChracterByLocale();

	ZeroMemory(m_pJigTempData, sizeof(float)*MAX_SBC_COUNT*EP_MAX_SENSOR_CH);
	ZeroMemory(m_pTempCaliData, sizeof(float)*MAX_SBC_COUNT*EP_MAX_SENSOR_CH);
	ZeroMemory(m_pTempCaliOffsetData, sizeof(float)*MAX_SBC_COUNT*EP_MAX_SENSOR_CH);
	m_pInformationDlg = NULL;
}

CSensorDataDlg::CSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleID)
	: CDialog(CSensorDataDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CSensorDataDlg"));

	m_nModuleID = iTargetModuleID;
	m_pDoc = pDoc;
	ASSERT(m_pDoc);
	m_bLockUpdate = FALSE;
	m_pSensorMap = NULL;
	m_csTemperatureSpecialCharacter = CalcTemperatureChracterByLocale();
}

CSensorDataDlg::~CSensorDataDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	this->DestroyWindow();

	if(m_pInformationDlg != NULL)
	{
		delete m_pInformationDlg;
		m_pInformationDlg = NULL;
	}
}


bool CSensorDataDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}
void CSensorDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSensorDataDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_EXCEL_SAVE_BUTTON, m_btnExcel);
	DDX_Control(pDX, IDC_PRINT_BUTTON, m_btnPrint);
	DDX_Control(pDX, IDC_MAP_BUTTON, m_btnMap);
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ctrlMDSelCombo);
	DDX_Control(pDX, IDC_GAS_MIN, m_ctrlGasMin);
	DDX_Control(pDX, IDC_GAS_MAX, m_ctrlGasMax);
	DDX_Control(pDX, IDC_TEMP_MIN, m_ctrlTempMin);
	DDX_Control(pDX, IDC_TEMP_MAX, m_ctrlTempMax);
	DDX_Control(pDX, IDC_STATIC_MAP1, m_cTemperaturePicture);
	DDX_Control(pDX, IDC_BTN_TEMPERATURE_CALIBRATION_STOP, m_btnStopTempCali);
	DDX_Control(pDX, IDC_BTN_TEMPERATURE_CALIBRATION_RUN, m_btnRunTempCali);
	DDX_Control(pDX, IDC_BTN_TEMPERATURE_CALIBRATION_OFFSET_APPLY, m_btnApplyCaliOffset);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSensorDataDlg, CDialog)
	//{{AFX_MSG_MAP(CSensorDataDlg)
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_MAP_BUTTON, OnMapButton)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	ON_BN_CLICKED(IDC_PRINT_BUTTON, OnPrintButton)
	ON_BN_CLICKED(IDC_EXCEL_SAVE_BUTTON, OnExcelSaveButton)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_TEMPERATURE_CALIBRATION_RUN, &CSensorDataDlg::OnBnClickedBtnTemperatureCalibrationRun)
	ON_BN_CLICKED(IDC_BTN_TEMPERATURE_CALIBRATION_OFFSET_APPLY, &CSensorDataDlg::OnBnClickedBtnTemperatureCalibrationOffsetApply)
	ON_BN_CLICKED(IDC_BTN_TEMPERATURE_CALIBRATION_STOP, &CSensorDataDlg::OnBnClickedBtnTemperatureCalibrationStop)	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg message handlers

BOOL CSensorDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_strModuleName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	//Get Current Folder(CTSMon Folde)

	CString strTemp;
	int nModuleID;
	
	m_ctrlMDSelCombo.AddString("== All ==");
	m_ctrlMDSelCombo.SetItemData(0, 0);

	int nDefaultIndex = 0;
	int nCount = 1;
	for(int i=0; i<m_nInstallModuleNum; i++)
	{
		nModuleID = EPGetModuleID(i);
		if(nModuleID == m_nModuleID)
		{
			nDefaultIndex = nCount;
		}
		m_ctrlMDSelCombo.AddString(::GetModuleName(nModuleID));
		m_ctrlMDSelCombo.SetItemData(nCount++, nModuleID);
	}
	m_ctrlMDSelCombo.SetCurSel(nDefaultIndex);
	
	InitTempGrid();
	UpdateGridData();
	UpdateButtonControl();

	SetTimer(500, 5000, NULL);		//Value Update Timer

	HBITMAP hBmp =(HBITMAP)::LoadImage(AfxGetInstanceHandle(),"C:\\Program Files (x86)\\PNE CTS\\IMG\\temper_sensor.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	m_cTemperaturePicture.SetBitmap(hBmp);

	return TRUE;
}

void CSensorDataDlg::InitTempGrid()
{
	m_wndTempGrid.SubclassDlgItem(IDC_TEMP_GRID, this);
	m_wndTempGrid.m_bSameColSize  = FALSE;
	m_wndTempGrid.m_bSameRowSize  = FALSE;
	m_wndTempGrid.m_bCustomWidth  = TRUE;

	m_wndTempGrid.Initialize();
	BOOL bLock = m_wndTempGrid.LockUpdate();
	m_wndTempGrid.SetColCount(SENSOR_COLUMN_SENSOR_ITEM);
	m_wndTempGrid.m_BackColor	= RGB(255,255,255);

	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_UNIT_NAME), m_strModuleName);	// Unit Name
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_SENSOR_TYPE), TEXT_LANG[0]);		// Sensor
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_CURRENT), TEXT_LANG[4]);		// Current Temperature Value
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_CURRENT_OFFSET),		"CUR Offset");
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_CALI_JIG_TEMP),		"CalJigTemp");
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_CALI_TRAY_TEMP),		"CalTrayTemp");

	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_CALI_OFFSET),			"CalOffset");
	m_wndTempGrid.SetStyleRange(CGXRange().SetCols(SENSOR_COLUMN_TEMP_CALI_OFFSET),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetMaxLength(16)
		);

	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_CALI_OFFSET_ENABLE),			"ApplyEnable");
	m_wndTempGrid.SetStyleRange(CGXRange().SetCols(SENSOR_COLUMN_TEMP_CALI_OFFSET_ENABLE), 
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
		);

	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_MAX),					TEXT_LANG[5]);
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_MIN),					TEXT_LANG[6]);
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_LIMIT_USE),			TEXT_LANG[7]);
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_TEMP_LIMIT_VALUE),			TEXT_LANG[8]);
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_SENSOR_NO),				TEXT_LANG[1]);
	m_wndTempGrid.SetValueRange(CGXRange(0,SENSOR_COLUMN_SENSOR_ITEM),				TEXT_LANG[2]);

	CRect rectGrid;
	float width;
	m_wndTempGrid.GetClientRect(rectGrid);
	width = (float)rectGrid.Width()/100.0f;
	
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_UNIT_NAME]					= int(width* 10.0f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_SENSOR_TYPE]				= int(width* 7.5f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CURRENT]				= int(width* 15.0f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CURRENT_OFFSET]		= int(width* 8.0f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_JIG_TEMP]		= int(width* 8.0f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_TRAY_TEMP]		= int(width* 10.0f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_OFFSET]			= int(width* 10.0f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_OFFSET_ENABLE]	= int(width* 11.5f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_MAX]					= int(width* 10.0f);
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_MIN]					= int(width* 10.0f);

	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_LIMIT_USE]			= 0;
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_LIMIT_VALUE]			= 0;
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_SENSOR_NO]					= 0;
	m_wndTempGrid.m_nWidth[SENSOR_COLUMN_SENSOR_ITEM]				= 0;

	m_wndTempGrid.SetDefaultRowHeight(20);
	m_wndTempGrid.GetParam()->GetProperties()->SetDisplayHorzLines(TRUE);
	m_wndTempGrid.GetParam()->GetProperties()->SetDisplayVertLines(TRUE);
	m_wndTempGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetDraw3dFrame(gxFrameNormal));

	ReDrawGrid();

	m_wndTempGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndTempGrid.SetScrollBarMode(SB_HORZ, gxnDisabled);
	m_wndTempGrid.EnableCellTips();
	
	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();
}

void CSensorDataDlg::OnTimer(UINT nIDEvent) 
{
	UpdateGridData();

	CDialog::OnTimer(nIDEvent);
}

void CSensorDataDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		float width;
		if(m_wndTempGrid.GetSafeHwnd())
		{
			m_wndTempGrid.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_wndTempGrid.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			
			m_wndTempGrid.GetClientRect(rectGrid);
			width = (float)rectGrid.Width()/100.0f;
			
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_UNIT_NAME]					= int(width* 10.0f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_SENSOR_TYPE]				= int(width* 7.5f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CURRENT]				= int(width* 15.0f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CURRENT_OFFSET]		= int(width* 8.0f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_JIG_TEMP]		= int(width* 8.0f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_TRAY_TEMP]		= int(width* 10.0f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_OFFSET]			= int(width* 10.0f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_CALI_OFFSET_ENABLE]	= int(width* 11.5f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_MAX]					= int(width* 10.0f);
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_MIN]					= int(width* 10.0f);

			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_LIMIT_USE]			= 0;
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_TEMP_LIMIT_VALUE]			= 0;
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_SENSOR_NO]					= 0;
			m_wndTempGrid.m_nWidth[SENSOR_COLUMN_SENSOR_ITEM]				= 0;

			Invalidate();
		}
	}
}	

void CSensorDataDlg::UpdateGridData()
{
	if(m_bLockUpdate)	return;

	EP_GP_DATA gpData;
	CString strTemp;
	long moduleID;

	long lTempMax = -10e8, lTempMin = 10e8, lTempMaxModuleID = 0, lTempMinModuleID = 0;
	long lGasMax = -10e8, lGasMin = 10e8, lGasMaxModuleID = 0, lGasMinModuleID = 0;

	int nCount = 0, nCout1 = 0;
	int nRow;
	int nRowCount = m_wndTempGrid.GetRowCount();
	int nSenorNo, nSensorItem;
	CFormModule *pModule;

	int i = 0;

	//최대 최소값을 구한다.
	for(i =0; i<nRowCount; i++)
	{
		moduleID = atol(m_wndTempGrid.GetValueRowCol(i+1, 0));
		nSenorNo = atol(m_wndTempGrid.GetValueRowCol(i+1, SENSOR_COLUMN_SENSOR_NO));
		nSensorItem = atol(m_wndTempGrid.GetValueRowCol(i+1, SENSOR_COLUMN_SENSOR_ITEM))-1;
		gpData = EPGetGroupData(moduleID, 0);

		if(nSenorNo<=0 || nSenorNo > EP_MAX_SENSOR_CH)	continue;
		int j = nSenorNo-1;
		if(gpData.gpState.state != EP_STATE_LINE_OFF)
		{
			if(nSensorItem == CFormModule::sensorType1)
			{
				if(lTempMax < gpData.sensorData.sensorData1[j].lData)	
				{
					lTempMaxModuleID =  moduleID;
					lTempMax = gpData.sensorData.sensorData1[j].lData;
				}

				if(lTempMin > gpData.sensorData.sensorData1[j].lData)
				{
					lTempMinModuleID = moduleID;
					lTempMin = gpData.sensorData.sensorData1[j].lData;
				}
				nCount++;
			}
			else if(nSensorItem == CFormModule::sensorType2)
			{
				if(lGasMax < gpData.sensorData.sensorData2[j].lData)
				{
					lGasMaxModuleID = moduleID;
					lGasMax = gpData.sensorData.sensorData2[j].lData;
				}

				if(lGasMin > gpData.sensorData.sensorData2[j].lData)
				{
					lGasMinModuleID = moduleID;
					lGasMin = gpData.sensorData.sensorData2[j].lData;
				}
				nCout1++;
			}
		}
	}

	//표시할 data가 없을 경우 
	if(nCount <= 0)
	{
		lTempMax = 0; lTempMin = 0;
	}

	if(nCout1 <= 0)
	{
		lGasMax = 0; lGasMin = 0;
	}

	//Progress Control의 범위 설정을 위해 
	CString strMin, strMax;
	long nRngeMin, nRangeMax;

	if(lTempMin > 0)
	{
		nRngeMin = 0;		//0~Max Range
	}
	else
	{
		nRngeMin = lTempMin;	// - 온도 표시 
	}

	nRangeMax = ((lTempMax/5000)+1)*5000;	//50도 단위로 표시 

	strMin.Format("%.1f" + m_csTemperatureSpecialCharacter, float(ETC_PRECISION(nRngeMin)));
	strMax.Format("%.1f" + m_csTemperatureSpecialCharacter, float(ETC_PRECISION(nRangeMax)));	

	strTemp.Format("%s[%s~%s]", TEXT_LANG[4], strMin, strMax);
	m_wndTempGrid.SetValueRange(CGXRange(0,3), strTemp);

	strTemp.Format("%s    %.1f " + m_csTemperatureSpecialCharacter, ::GetModuleName(lTempMaxModuleID), ETC_PRECISION(lTempMax));
	m_ctrlTempMax.SetText(strTemp);
	strTemp.Format("%s    %.1f " + m_csTemperatureSpecialCharacter, ::GetModuleName(lTempMinModuleID), ETC_PRECISION(lTempMin));
	m_ctrlTempMin.SetText(strTemp);
	strTemp.Format("%s    %s", ::GetModuleName(lGasMaxModuleID), m_pDoc->ValueString(lGasMax, EP_VOLTAGE, TRUE));
	m_ctrlGasMax.SetText(strTemp);
	strTemp.Format("%s    %s", ::GetModuleName(lGasMinModuleID), m_pDoc->ValueString(lGasMin, EP_VOLTAGE, TRUE));
	m_ctrlGasMin.SetText(strTemp);


	for(i =0; i<nRowCount; i++)
	{
		nRow = i+1;
		moduleID = atol(m_wndTempGrid.GetValueRowCol(nRow, 0));
		pModule = m_pDoc->GetModuleInfo(moduleID);
		nSenorNo = atol(m_wndTempGrid.GetValueRowCol(nRow, SENSOR_COLUMN_SENSOR_NO));
		nSensorItem = atol(m_wndTempGrid.GetValueRowCol(nRow, SENSOR_COLUMN_SENSOR_ITEM));
		gpData = EPGetGroupData(moduleID, 0);

		if(pModule == NULL)		continue;
		if(nSenorNo<=0 || nSenorNo > EP_MAX_SENSOR_CH)	continue;

		int j = nSenorNo-1;
		if(nSensorItem-1 == CFormModule::sensorType1)
		{
			strTemp.Format("%.1f" + m_csTemperatureSpecialCharacter, float(ETC_PRECISION(gpData.sensorData.sensorData1[j].lData)));

			//Progess 구간을 현재 표시 data의 Min/Max로 조정한다.
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CURRENT), 
				CGXStyle()
				.SetControl(GX_IDS_CTRL_PROGRESS)
				//.SetValue(ETC_PRECISION(gpData.sensorData.sensorData1[j].lData))
				.SetValue(strTemp)
				.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T(strMin))
				.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T(strMax))
				.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, strTemp)		// sprintf formatting for value				
				);

// 20210614 KSCHOI Add Temperature Calibration Function START
// 			strTemp.Format("%.1f" + m_csTemperatureSpecialCharacter + " / %s", ETC_PRECISION(gpData.sensorMinMax.sensorData1[j].lMax), gpData.sensorMinMax.sensorData1[j].szMaxDateTime);
// 			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 4), 
// 				CGXStyle()
// 				.SetValue(strTemp)
// 				.SetTextColor(RGB(0, 0, 255))      // blue text
// 				.SetInterior(RGB(255, 255, 255))   // on white b
// 				);
// 
// 			strTemp.Format("%.1f" + m_csTemperatureSpecialCharacter + " / %s", ETC_PRECISION(gpData.sensorMinMax.sensorData1[j].lMin), gpData.sensorMinMax.sensorData1[j].szMinDateTime);
// 			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 5), 
// 				CGXStyle()
// 				.SetValue(strTemp)
// 				.SetTextColor(RGB(0, 0, 255))      // blue text
// 				.SetInterior(RGB(255, 255, 255))   // on white b
// 				);

			EP_MD_SBC_PARAM *lpSbcParam = EPGetModuleSbcParam(EPGetModuleIndex(moduleID));

			if(lpSbcParam != NULL)
			{
				strTemp.Format("%.2f", ((float)lpSbcParam->pTemperatureOffset[j])/100);
				m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CURRENT_OFFSET), 
					CGXStyle()
					.SetValue(strTemp)
					.SetTextColor(RGB(0, 0, 255))      // blue text
					.SetInterior(RGB(255, 255, 255))   // on white b
					);	
			}

			strTemp.Format("%.2f" + m_csTemperatureSpecialCharacter, m_pJigTempData[moduleID-1][i]);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CALI_JIG_TEMP), 
				CGXStyle()
				.SetValue(strTemp)
				.SetTextColor(RGB(0, 0, 255))      // blue text
				.SetInterior(RGB(255, 255, 255))   // on white b
				);

			strTemp.Format("%.2f" + m_csTemperatureSpecialCharacter, m_pTempCaliData[moduleID-1][i]);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CALI_TRAY_TEMP), 
				CGXStyle()
				.SetValue(strTemp)
				.SetTextColor(RGB(0, 0, 255))      // blue text
				.SetInterior(RGB(255, 255, 255))   // on white b
				);

// 			strTemp.Format("%.2f", m_pTempCaliOffsetData[moduleID-1][i]);
// 			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CALI_OFFSET), 
// 				CGXStyle()
// 				.SetValue(strTemp)
// 				.SetTextColor(RGB(0, 0, 255))      // blue text
// 				.SetInterior(RGB(255, 255, 255))   // on white b
// 				);

			strTemp.Format("%.2f" + m_csTemperatureSpecialCharacter + " / %s", ETC_PRECISION(gpData.sensorMinMax.sensorData1[j].lMax), gpData.sensorMinMax.sensorData1[j].szMaxDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_MAX), 
				CGXStyle()
				.SetValue(strTemp)
				.SetTextColor(RGB(0, 0, 255))      // blue text
				.SetInterior(RGB(255, 255, 255))   // on white b
				);

			strTemp.Format("%.2f" + m_csTemperatureSpecialCharacter + " / %s", ETC_PRECISION(gpData.sensorMinMax.sensorData1[j].lMin), gpData.sensorMinMax.sensorData1[j].szMinDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_MIN), 
				CGXStyle()
				.SetValue(strTemp)
				.SetTextColor(RGB(0, 0, 255))      // blue text
				.SetInterior(RGB(255, 255, 255))   // on white b
				);
// 20210614 KSCHOI Add Temperature Calibration Function END
		}
		else if(nSensorItem-1 == CFormModule::sensorType2)
		{
			//			strTemp.Format("%s", m_pDoc->ValueString(gpData.sensorData.sensorData2[j].lData, EP_VOLTAGE));
			strTemp.Format("%d", (long)VTG_PRECISION(gpData.sensorData.sensorData2[j].lData));
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CURRENT), 
				CGXStyle()
				.SetValue(strTemp)				
				);

// 20210614 KSCHOI Add Temperature Calibration Function START
// 			//strTemp.Format("%s / %s", m_pDoc->ValueString(gpData.sensorMinMax.sensorData2[j].lMax, EP_VOLTAGE), gpData.sensorMinMax.sensorData2[j].szMaxDateTime);
// 			strTemp.Format("%d / %s", (long)VTG_PRECISION(gpData.sensorMinMax.sensorData2[j].lMax), gpData.sensorMinMax.sensorData2[j].szMaxDateTime);
// 			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 4), 
// 				CGXStyle()
// 				.SetValue(strTemp)
// 				.SetTextColor(RGB(255, 0, 0))      // red text
// 				.SetInterior(RGB(255, 255, 255))   // on white b
// 				);
// 
// 			//strTemp.Format("%s / %s", m_pDoc->ValueString(gpData.sensorMinMax.sensorData2[j].lMin, EP_VOLTAGE), gpData.sensorMinMax.sensorData2[j].szMinDateTime);
// 			strTemp.Format("%d / %s", (long)VTG_PRECISION(gpData.sensorMinMax.sensorData2[j].lMin), gpData.sensorMinMax.sensorData2[j].szMinDateTime);
// 			m_wndTempGrid.SetStyleRange(CGXRange(nRow, 5), 
// 				CGXStyle()
// 				.SetValue(strTemp)
// 				.SetTextColor(RGB(255, 0, 0))      // red text
// 				.SetInterior(RGB(255, 255, 255))   // on white b
// 				);			
			strTemp.Format("%d / %s", (long)VTG_PRECISION(gpData.sensorMinMax.sensorData2[j].lMax), gpData.sensorMinMax.sensorData2[j].szMaxDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_MAX), 
				CGXStyle()
				.SetValue(strTemp)
				.SetTextColor(RGB(255, 0, 0))      // red text
				.SetInterior(RGB(255, 255, 255))   // on white b
				);
			
			strTemp.Format("%d / %s", (long)VTG_PRECISION(gpData.sensorMinMax.sensorData2[j].lMin), gpData.sensorMinMax.sensorData2[j].szMinDateTime);
			m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_MIN), 
				CGXStyle()
				.SetValue(strTemp)
				.SetTextColor(RGB(255, 0, 0))      // red text
				.SetInterior(RGB(255, 255, 255))   // on white b
				);
// 20210614 KSCHOI Add Temperature Calibration Function END
		}
	}
}

void CSensorDataDlg::OnOK() 
{
	// TODO: Add extra validation here				
	CDialog::OnOK();
}

void CSensorDataDlg::OnMapButton() 
{
	// TODO: Add your control notification handler code here
	if(m_nModuleID < 1)	return;

	if(::PasswordCheck() == FALSE)	return;

	WORD state = EPGetGroupState(m_nModuleID,  0);

	m_pSensorMap = new CSensorMapDlg( m_pDoc, this );
	m_pSensorMap->m_nModuleID = m_nModuleID;
	if(m_pSensorMap->DoModal()==IDOK)
	{
		ReDrawGrid();
		UpdateGridData();

		delete m_pSensorMap;
		m_pSensorMap = NULL;
	}
	return;
}

void CSensorDataDlg::OnSelchangeModuleSelCombo() 
{
	int nCurSel = m_ctrlMDSelCombo.GetCurSel();
	if(nCurSel == LB_ERR)	return;

	m_nModuleID = m_ctrlMDSelCombo.GetItemData(nCurSel);

	UpdateButtonControl();

	m_bLockUpdate = TRUE;
	ReDrawGrid();
	m_bLockUpdate = FALSE;

	UpdateGridData();
}

void CSensorDataDlg::fnSelModuleNum( int nModuleID )
{
	m_nModuleID = nModuleID;

	if(nModuleID > 0)
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(FALSE);
	}

	m_bLockUpdate = TRUE;
	ReDrawGrid();
	m_bLockUpdate = FALSE;

	UpdateGridData();
}

void CSensorDataDlg::ReDrawGrid()
{
	int nDispModuleCount  = 1;
	CString strTemp;
	EP_SYSTEM_PARAM	*lpSysParam;
	int nModuleID =0;
	CFormModule *pModule;

	if(m_nModuleID < 1)	//Display All Module
	{
		nDispModuleCount = m_nInstallModuleNum;
	}

	BOOL bLock = m_wndTempGrid.LockUpdate();
	if(m_wndTempGrid.GetRowCount() > 0)
		m_wndTempGrid.RemoveRows(1, m_wndTempGrid.GetRowCount());

	_MAPPING_DATA *pMapData;
	int nRow = 0;
	for(int i=0; i<nDispModuleCount; i++)
	{
		if(m_nModuleID < 1)		
			nModuleID = EPGetModuleID(i);
		else					
			nModuleID = m_nModuleID;

		lpSysParam = EPGetSysParam(nModuleID);
		if(lpSysParam == NULL)	continue;

		pModule = m_pDoc->GetModuleInfo(nModuleID);
		for(int s =0; s<2; s++)
		{
			for(int j =0; j<EP_MAX_SENSOR_CH; j++)
			{
				pMapData = pModule->GetSensorMap(s, j);
				if(pMapData->nChannelNo >= 0)
				{
					m_wndTempGrid.InsertRows(++nRow, 1);					
					m_wndTempGrid.SetValueRange(CGXRange(nRow,SENSOR_COLUMN_MODULE_NO), (long)nModuleID);
					strTemp.Format("%s S%d-%02d", ::GetModuleName(nModuleID), s+1, j+1);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,SENSOR_COLUMN_UNIT_NAME), strTemp);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,SENSOR_COLUMN_SENSOR_TYPE), pMapData->szName);
					strTemp.Format("%d", j+1);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,SENSOR_COLUMN_SENSOR_NO), strTemp);
					strTemp.Format("%d", s+1);
					m_wndTempGrid.SetValueRange(CGXRange(nRow,SENSOR_COLUMN_SENSOR_ITEM), strTemp);
					
					if(s == CFormModule::sensorType1)
					{
						//Sensor Item1
						m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CURRENT), 
							CGXStyle()							
							.SetValue(0L)
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld" + m_csTemperatureSpecialCharacter))		// sprintf formatting for value
							.SetTextColor(TEMP_PROGRESS_COLOR)									// blue progress bar
							.SetInterior(RGB(255, 255, 255))									// on white background
							);

						m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CALI_JIG_TEMP), 
							CGXStyle()							
							.SetValue(0L)
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld" + m_csTemperatureSpecialCharacter))		// sprintf formatting for value
							.SetTextColor(TEMP_PROGRESS_COLOR)									// blue progress bar
							.SetInterior(RGB(255, 255, 255))									// on white background
							);

						m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CALI_TRAY_TEMP), 
							CGXStyle()							
							.SetValue(0L)
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld" + m_csTemperatureSpecialCharacter))		// sprintf formatting for value
							.SetTextColor(TEMP_PROGRESS_COLOR)									// blue progress bar
							.SetInterior(RGB(255, 255, 255))									// on white background
							);

						m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_LIMIT_VALUE), 
							CGXStyle()
							.SetControl(GX_IDS_CTRL_SPINEDIT)
							.SetUserAttribute(GX_IDS_UA_SPINBOUND_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_SPINBOUND_MAX, _T("100"))
							.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("100"))
							);

						m_wndTempGrid.SetValueRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_LIMIT_USE), (long)lpSysParam->bUseTempLimit);
						strTemp.Format("%.1f" + m_csTemperatureSpecialCharacter, ETC_PRECISION(lpSysParam->wJigTempErrorRefValue));
						m_wndTempGrid.SetValueRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_LIMIT_VALUE), strTemp);
					}
					else if(s == CFormModule::sensorType2)
					{
						//Sensor Item2
						m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_CURRENT), 
							CGXStyle()
							.SetControl(GX_IDS_CTRL_PROGRESS)
							.SetValue(0L)
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("5000"))
							.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%ld mV"))  // sprintf formatting for value
							.SetTextColor(GAS_PROGRESS_COLOR)      // Red progress bar
							.SetInterior(RGB(255, 255, 255))   // on white background
							);

						m_wndTempGrid.SetStyleRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_LIMIT_VALUE), 
							CGXStyle()
							.SetControl(GX_IDS_CTRL_SPINEDIT)
							.SetUserAttribute(GX_IDS_UA_SPINBOUND_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_SPINBOUND_MAX, _T("5000"))
							.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
							.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("5000"))
							);
						m_wndTempGrid.SetValueRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_LIMIT_USE), (long)lpSysParam->bUseGasLimit);
						strTemp.Format("%d", (long)ETC_PRECISION(lpSysParam->lMaxGas));
						m_wndTempGrid.SetValueRange(CGXRange(nRow, SENSOR_COLUMN_TEMP_LIMIT_VALUE), strTemp);
					}
				}	
			}
		}
		m_wndTempGrid.SetStyleRange(CGXRange(nRow, 1, nRow, 8), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(3).SetColor(RGB(0,0,0))));
	}

	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();
}

void CSensorDataDlg::OnPrintButton() 
{
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;		
}

void CSensorDataDlg::OnExcelSaveButton() 
{
	CString strFileName;
	strFileName = "temperature.csv";

	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[3]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();
			
		for(int i=0; i< m_wndTempGrid.GetRowCount()+1; i++)
		{
			for(int j=1; j< m_wndTempGrid.GetColCount()+1; j++)
			{
				if( j > 1)
					fprintf(fp, ",");

				fprintf(fp, "%s", m_wndTempGrid.GetValueRowCol(i, j));
			}
			fprintf(fp, "\n");
		}

		EndWaitCursor();
		
		fclose(fp);
	}		
}

CString CSensorDataDlg::CalcTemperatureChracterByLocale()
{
	char pTemperatureSpecialCharacter[32] = {};
	LANGID langID = 0xFF&GetSystemDefaultLangID();

	if(langID == LANG_HUNGARIAN || langID == LANG_ENGLISH)
	{
		// NO EXISTS TEMPERATURE CHARACTER IN CODE PAGE 1252 (ENGLISH, HUNGARIAN)
		pTemperatureSpecialCharacter[0] = TEMPERATURE_HUNGARIAN_SPECIAL_CHARACTER;
		pTemperatureSpecialCharacter[1] = 'C';

		return pTemperatureSpecialCharacter;
	}
	else
	{
		// FOR LANG_KOREAN, LANG_CHINESE
		CStringW cswTemper = "";
		cswTemper.AppendChar(UNICODE_VALUE_TEMPERATURE);

		int len = WideCharToMultiByte( CP_ACP, 0, cswTemper, -1, NULL, 0, NULL, NULL );
		WideCharToMultiByte(CP_ACP, 0, cswTemper, -1, pTemperatureSpecialCharacter, len, NULL, NULL);

		return pTemperatureSpecialCharacter;
	}
}

void CSensorDataDlg::OnBnClickedBtnTemperatureCalibrationRun()
{
	try
	{
		int nCurSel = m_ctrlMDSelCombo.GetCurSel();
		if(nCurSel == LB_ERR)	return;

		m_nModuleID = m_ctrlMDSelCombo.GetItemData(nCurSel);

		if(m_nModuleID > 0)
		{
			CString strTemp;
			CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(m_nModuleID);
			if(pModuleInfo == FALSE)
			{
				ShowInformation(m_nModuleID, "Cannot Find ModuleInfo", INFO_TYPE_FAULT);
				return;
			}

			EP_CMD_START_TEMPERATURE_CALIBRATION_REQ tempCaliStartInfo;
			ZeroMemory(&tempCaliStartInfo, sizeof(EP_CMD_START_TEMPERATURE_CALIBRATION_REQ));

			tempCaliStartInfo.cJigCommnad = 2; // 1 : STOP, 2 : START
			
			if(EPSendCommand(m_nModuleID, 1, 0, EP_CMD_START_TEMP_CALI_REQUEST, &tempCaliStartInfo, sizeof(EP_CMD_START_TEMPERATURE_CALIBRATION_REQ))!= EP_ACK)
			{
				strTemp.Format("%s :: TempCali Start Command Send Fail.", ::GetModuleName(m_nModuleID));
				m_pDoc->WriteLog( strTemp );

				ShowInformation(m_nModuleID, strTemp, INFO_TYPE_FAULT);
			}
			else
			{
				GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_RUN)->EnableWindow(FALSE);
				GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_STOP)->EnableWindow(TRUE);

				ShowInformation(m_nModuleID, "TempCali Start Command Send Success", INFO_TYPE_NORMAL);
			}
		}
		else
		{
			ShowInformation(m_nModuleID, "Cannot Start Temp Cali In All Module Selected", INFO_TYPE_WARNNING);
		}
	}
	catch (CMemoryException* e)
	{
	
	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
}

void CSensorDataDlg::OnBnClickedBtnTemperatureCalibrationOffsetApply()
{
	try
	{
		int nCurSel = m_ctrlMDSelCombo.GetCurSel();
		if(nCurSel == LB_ERR)	return;

		m_nModuleID = m_ctrlMDSelCombo.GetItemData(nCurSel);

		if(m_nModuleID > 0)
		{
			CString strTemp;
			CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(m_nModuleID);
			if(pModuleInfo == FALSE)
			{
				ShowInformation(m_nModuleID, "Cannot Find ModuleInfo", INFO_TYPE_FAULT);
				return;
			}

			EP_CMD_APPLY_TEMPERATURE_CALIBRATION_REQ offsetApplyData;
			ZeroMemory(&offsetApplyData, sizeof(EP_CMD_APPLY_TEMPERATURE_CALIBRATION_REQ));

			for(int i = 0; i < 16; i++)
			{
				CString csApplyEnable = m_wndTempGrid.GetValueRowCol(i +1,SENSOR_COLUMN_TEMP_CALI_OFFSET_ENABLE);

				if(csApplyEnable == "1")
				{
					offsetApplyData.pSelectedSensor[i] = TRUE;
					//offsetApplyData.pApplyValue[i] = m_pTempCaliOffsetData[m_nModuleID-1][i]*100;
					offsetApplyData.pApplyValue[i] = atof(m_wndTempGrid.GetValueRowCol(i+1, SENSOR_COLUMN_TEMP_CALI_OFFSET)) * 100;
				}
			}

			if(EPSendCommand(m_nModuleID, 1, 0, EP_CMD_APPLY_TEMP_CALI_REQUEST, &offsetApplyData, sizeof(EP_CMD_APPLY_TEMPERATURE_CALIBRATION_REQ))!= EP_ACK)
			{
				strTemp.Format("%s :: TempCali Apply Command Send Fail.", ::GetModuleName(m_nModuleID));
				m_pDoc->WriteLog( strTemp );

				ShowInformation(m_nModuleID, strTemp, INFO_TYPE_FAULT);
			}
			else
			{
				ShowInformation(m_nModuleID, "TempCali Offset Apply Command Send Success", INFO_TYPE_NORMAL);
			}
		}
		else
		{
			ShowInformation(m_nModuleID, "Cannot Apply Temp Cali In All Module Selected", INFO_TYPE_WARNNING);
		}
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
}

BOOL CSensorDataDlg::OnOneSensorTempCaliEndNotify(int iModuleID, EP_CMD_NOTIFY_ONE_SENSOR_TEMPERATURE_CALIBRATION_END pOneSensorTempCaliEndData)
{
	try
	{
		CString strLog;
		int nChannelNo = pOneSensorTempCaliEndData.cChannelNo;
		float fJigTemp = 0;
		float fCaliTemp = 0;
		float fDiff = 0;

		if(iModuleID <= 0 || iModuleID >= MAX_SBC_COUNT)
		{
			strLog.Format("OneSensorTempCaliEndNotify Invalid Module No Received %d", iModuleID);
			m_pDoc->WriteLog( strLog );	
			return FALSE;
		}

		fJigTemp = ETC_PRECISION(pOneSensorTempCaliEndData.iSensorJigTemp);
		fCaliTemp = ETC_PRECISION(pOneSensorTempCaliEndData.iSensorCaliTemp);
		fDiff = fCaliTemp - fJigTemp;

		strLog.Format("%s :: CH%02d TempCali End. Jig Temp %.2f Cali Temp %.2f Diff %.2f", 
			::GetModuleName(m_nModuleID),
			nChannelNo,
			fJigTemp,
			fCaliTemp,
			fJigTemp - fCaliTemp);
		
		m_pDoc->WriteLog( strLog );	

		m_pJigTempData[iModuleID - 1][nChannelNo] = fJigTemp;
		m_pTempCaliData[iModuleID - 1][nChannelNo] = fCaliTemp;
		m_pTempCaliOffsetData[iModuleID - 1][nChannelNo] = fDiff;

		CString strTemp;
		strTemp.Format("%.2f", fDiff);
		m_wndTempGrid.SetStyleRange(CGXRange(nChannelNo+1, SENSOR_COLUMN_TEMP_CALI_OFFSET), 
		CGXStyle()
		.SetValue(strTemp)
		.SetTextColor(RGB(0, 0, 255))      // blue text
		.SetInterior(RGB(255, 255, 255))   // on white
		);

		m_wndTempGrid.SetStyleRange(CGXRange(nChannelNo+1, SENSOR_COLUMN_TEMP_CALI_OFFSET_ENABLE), 
			CGXStyle()
			.SetValue("1")
			.SetTextColor(RGB(0, 0, 255))      // blue text
			.SetInterior(RGB(255, 255, 255))   // on white
			);
		
		UpdateGridData();

		return TRUE;
	}
	catch (CException* e)
	{
	}
	return FALSE;
}

BOOL CSensorDataDlg::OnAllSensorTempCaliEndNotify(int iModuleID)
{
	try
	{
		CString strLog;

		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_RUN)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_STOP)->EnableWindow(FALSE);

		if(iModuleID <= 0 || iModuleID >= MAX_SBC_COUNT)
		{
			strLog.Format("AllSensorTempCaliEndNotify Invalid Module No Received %d", iModuleID);
			m_pDoc->WriteLog( strLog );
			return FALSE;
		}

		strLog.Format("%s :: TempCali End Successfully.", ::GetModuleName(m_nModuleID));
		m_pDoc->WriteLog( strLog );

		ShowInformation(m_nModuleID, strLog, INFO_TYPE_NORMAL);

		return TRUE;
	}
	catch (CException* e)
	{
	}
	
	return FALSE;
}

void CSensorDataDlg::OnBnClickedBtnTemperatureCalibrationStop()
{
	try
	{
		int nCurSel = m_ctrlMDSelCombo.GetCurSel();
		if(nCurSel == LB_ERR)	return;

		m_nModuleID = m_ctrlMDSelCombo.GetItemData(nCurSel);

		if(m_nModuleID > 0)
		{
			CString strTemp;
			CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(m_nModuleID);
			if(pModuleInfo == FALSE)
			{
				ShowInformation(m_nModuleID, "Cannot Find ModuleInfo", INFO_TYPE_FAULT);
				return;
			}

			EP_CMD_START_TEMPERATURE_CALIBRATION_REQ tempCaliStartInfo;
			ZeroMemory(&tempCaliStartInfo, sizeof(EP_CMD_START_TEMPERATURE_CALIBRATION_REQ));

			tempCaliStartInfo.cJigCommnad = 1; // 1 : STOP, 2 : START

			if(EPSendCommand(m_nModuleID, 1, 0, EP_CMD_START_TEMP_CALI_REQUEST, &tempCaliStartInfo, sizeof(EP_CMD_START_TEMPERATURE_CALIBRATION_REQ))!= EP_ACK)
			{
				strTemp.Format("%s :: TempCali Stop Command Send Fail.", ::GetModuleName(m_nModuleID));
				m_pDoc->WriteLog( strTemp );
				ShowInformation(m_nModuleID, strTemp, INFO_TYPE_FAULT);
			}
			else
			{
				GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_STOP)->EnableWindow(FALSE);
				GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_RUN)->EnableWindow(TRUE);

				ShowInformation(m_nModuleID, "TempCali Stop Command Send Success", INFO_TYPE_NORMAL);
			}
		}
		else
		{
			ShowInformation(m_nModuleID, "Cannot Stop Temp Cali In All Module Selected", INFO_TYPE_WARNNING);			
		}
	}
	catch (CMemoryException* e)
	{
	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
}

void CSensorDataDlg::ShowInformation( int nUnitNum, CString strMsg, int nMsgType )
{
	if(m_pInformationDlg == NULL)
	{
		m_pInformationDlg = new CInformationDlg();
		m_pInformationDlg->Create(IDD_SHOW_WANNING_DLG,  this);		
		m_pInformationDlg->SetUnitNum( nUnitNum );
		m_pInformationDlg->SetResultMsg( strMsg, nMsgType );
		m_pInformationDlg->ShowWindow(SW_SHOW);		
	}
	else
	{
		m_pInformationDlg->SetUnitNum( nUnitNum );
		m_pInformationDlg->SetResultMsg( strMsg, nMsgType );
		m_pInformationDlg->ShowWindow(SW_SHOW);
	}
}

void CSensorDataDlg::UpdateButtonControl()
{
	int nCurSel = m_ctrlMDSelCombo.GetCurSel();
	if(nCurSel == LB_ERR)	return;

	if(nCurSel == 0)
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(FALSE);

		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_RUN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_OFFSET_APPLY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_STOP)->EnableWindow(FALSE);	
	}
	else
	{
		GetDlgItem(IDC_MAP_BUTTON)->EnableWindow(TRUE);

		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_RUN)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_OFFSET_APPLY)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_TEMPERATURE_CALIBRATION_STOP)->EnableWindow(FALSE);
	}
}
