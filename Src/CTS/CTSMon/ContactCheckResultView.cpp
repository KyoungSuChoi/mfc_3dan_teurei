// ContactCheckResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ContactCheckResultView.h"
#include "MainFrm.h"


// CContactCheckResultView

IMPLEMENT_DYNCREATE(CContactCheckResultView, CFormView)

CContactCheckResultView::CContactCheckResultView()
	: CFormView(CContactCheckResultView::IDD)
{
	LanguageinitMonConfig(_T("CContactCheckResultView"));
	m_pConfig = NULL;
	m_pRowCol2GpCh = NULL;

	m_nCurModuleID = 0;
	m_nDisplayTimer = 0;
	m_nCurGroup = 0;
	m_nCurChannel = 1;
	m_nCurTrayIndex = 0;
	m_nTopGridColCount = 1;
}

CContactCheckResultView::~CContactCheckResultView()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CContactCheckResultView::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CContactCheckResultView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
}

BEGIN_MESSAGE_MAP(CContactCheckResultView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CContactCheckResultView 진단입니다.

#ifdef _DEBUG
void CContactCheckResultView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CContactCheckResultView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CCTSMonDoc* CContactCheckResultView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG


// CContactCheckResultView 메시지 처리기입니다.
void CContactCheckResultView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	InitLabel();

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();						//Top Config

	// m_nTopGridColCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 16);
	m_nTopGridRowCount = EP_MAX_CH_PER_MD/m_nTopGridColCount;
	if(EP_MAX_CH_PER_MD % m_nTopGridColCount) 
		m_nTopGridRowCount++;

	InitSmallChGrid();
	SetTopGridFont();
	DrawChannel();			//Update Channel State Grid
}

void CContactCheckResultView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CFormView::ShowScrollBar(SB_HORZ,FALSE);
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		if(m_wndSmallChGrid.GetSafeHwnd())
		{
			m_wndSmallChGrid.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			m_wndSmallChGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-5, rect.bottom-ctrlRect.top-5, FALSE);
		}
		
		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
		
		if(::IsWindow(GetDlgItem(IDC_LIST_ERROR_CODE)->GetSafeHwnd()))
		{
			GetDlgItem(IDC_LIST_ERROR_CODE)->GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			GetDlgItem(IDC_LIST_ERROR_CODE)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), rect.bottom - ctrlRect.top-5 , FALSE);
		}
	}		
}

void CContactCheckResultView::InitLabel()
{
	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE);
/*		.SetFontName("HY헤드라인M");*/

	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText(TEXT_LANG[0]); //"Contact Check Result"
}

void CContactCheckResultView::GroupSetChanged(int nColCount)
{
	//Calculate Grid Info
	if(nColCount >0 && nColCount<= 64)
	{
		m_nTopGridColCount = nColCount;						//Update Top Grid Column Count
	}

	WORD nCount = 0, wChinGroup;

	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)	
	{
		return;
	}

	//Tray 단위 보기 
	wChinGroup = pModule->GetChInJig(m_nCurTrayIndex);

	//Module 단위 보기 
	//	wChinGroup = pModule->GetTotalInstallCh();
	nCount = (wChinGroup / m_nTopGridColCount);
	if(wChinGroup%m_nTopGridColCount >0)
	{
		nCount++;
	}

	m_nTopGridRowCount = (int)nCount;					//Update Top Gird Row Count

	//Make Group And Channel Index Table
	if(m_pRowCol2GpCh)
	{
		delete[] m_pRowCol2GpCh;
		m_pRowCol2GpCh = NULL;
	}

	nCount= m_nTopGridRowCount*m_nTopGridColCount;
	//	ASSERT(nCount);
	m_pRowCol2GpCh = new SHORT[nCount];
	memset(m_pRowCol2GpCh, -1, nCount);

	nCount = 0;
	WORD nRowIndex = 0, nCellCout = 0;

	//Grid의 특정 위치에 Group 번호와 Channel 번호를 쉽게 찾기 위해 Mapping Table을 생성한다. 

	WORD nTrayType = GetDocument()->m_nViewTrayType;
	WORD j = 0;
	switch (nTrayType)
	{
	case TRAY_TYPE_CUSTOMER:
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nRowIndex++;

			if(nRowIndex >= m_nTopGridRowCount)
			{
				nCount++;
				nRowIndex = 0;
			}
		}
		break;
	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nCount++;

			if(nCount >= m_nTopGridColCount)
			{
				nCount=0;
				nRowIndex++;
			}
		}
		break;
	}	

	//모둘에 할당된 지그(Tray) 숫자 가져오기
	int nCnt = pModule->GetTotalJig();

	//////////////////////////////////////////////////////////////////////////
	//Draw small channel display grid
	BOOL bLock = m_wndSmallChGrid.LockUpdate();
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);		//Delete Grid Attribute

	UINT nRow = 0, nCol = 1;
	int n = 0;
	for(n = 0; n<nCnt; n++)
	{
		nRow += pModule->GetChInJig(n);
	}
	
	if(m_wndSmallChGrid.GetRowCount() != nRow+1 || m_wndSmallChGrid.GetColCount() != nCol+1)
	{
		m_wndSmallChGrid.SetRowCount(nRow+1);		//ReDraw 
		m_wndSmallChGrid.SetColCount(nCol+1);
		m_wndSmallChGrid.SetCoveredCellsRowCol(0, 1, 1, 1);

		CString strTemp;		
		for( n=0; n<pModule->GetTotalJig(); n++)
		{
			strTemp.Format(TEXT_LANG[1], n+1); //"Connection"
			m_wndSmallChGrid.SetCoveredCellsRowCol(0, (n+1)*2, 1, (n+1)*2);
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2), 
				CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE))
				);
/*
			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2), 
				CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
				.SetHorizontalAlignment(DT_CENTER)
				.SetDraw3dFrame(gxFrameRaised)
				.SetInterior(GetSysColor(COLOR_3DFACE))
				.SetValue(1L)
				);
*/
			COLORREF boldColor = RGB(255, 192, 0);

			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2, 0, (n+1)*2), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetColor(boldColor)));			
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2, nRow+1, (n+1)*2), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2, nRow+1, (n+1)*2), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2, nRow+1, (n+1)*2), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(nRow+1, (n+1)*2, nRow+1, (n+1)*2), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(boldColor)));
		}

		m_wndSmallChGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue(TEXT_LANG[2]).SetFont(CGXFont().SetBold(TRUE)));//"CH(/)"

		for (int a = 1; a <=nRow; a++)
		{
			m_wndSmallChGrid.SetStyleRange(CGXRange(a+1, 1), CGXStyle().SetValue(ROWCOL(a)).SetDraw3dFrame(gxFrameRaised));
		}
	}
	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();
}

BOOL CContactCheckResultView::GetChRowCol(int nGroupIndex, int nChindex, ROWCOL &nRow, ROWCOL &nCol)
{
	WORD nTrayType = GetDocument()->m_nViewTrayType;
	switch (nTrayType)
	{
	case TRAY_TYPE_PB5_COL_ROW:	//각형 가로 번호 쳬계
		if(nChindex+2 < m_nTopGridColCount )	
		{	
			nRow =1; 
			nCol = nChindex+2; 
		}

		if( nChindex+2 >= m_nTopGridColCount)
		{
			nRow = (nChindex+2) / m_nTopGridColCount +1;	
			nCol = (nChindex +2) % m_nTopGridColCount+1;	
		}

		if(nRow == m_nTopGridRowCount)
		{
			nCol++;
		}

		if(nRow == m_nTopGridRowCount && nCol == m_nTopGridColCount)
		{
			nCol++;
		}
		break;
	case TRAY_TYPE_PB5_ROW_COL:	//각형 세로 번호 쳬계
		if(nChindex+2 < m_nTopGridRowCount )	
		{	
			nCol =1; 
			nRow = nChindex+2; 
		}

		if( nChindex+2 >= m_nTopGridRowCount)
		{
			nRow = (nChindex+2) % m_nTopGridRowCount+1;
			nCol = (nChindex +2) / m_nTopGridRowCount+1;	
		}

		if(nCol == m_nTopGridColCount)
		{
			nRow++;
		}

		if(nCol == m_nTopGridColCount && nRow == m_nTopGridRowCount)
		{
			nRow++;
		}
		break;
	case TRAY_TYPE_LG_COL_ROW:	//가로 번호 체계 
		nCol = nChindex % m_nTopGridColCount +1;
		nRow = nChindex / m_nTopGridColCount +1;	
		break;
	case TRAY_TYPE_CUSTOMER:
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계		
		nRow = nChindex % m_nTopGridRowCount +1;
		nCol = nChindex / m_nTopGridRowCount +1;
		break;
	}

	if(nCol < 0 || nCol > m_nTopGridColCount)	return FALSE;
	if(nRow < 0 || nRow > m_nTopGridRowCount)	return FALSE;

	return TRUE;
}

void CContactCheckResultView::DrawChannel()
{
	CCTSMonDoc *pDoc = GetDocument();
	BYTE colorFlag = 0;
	CString  strMsg;

	float	fTemp = 0;

	CStep *pStep = NULL;

	ROWCOL nRow = 0, nCol = 0;
	int nChannelNo = 0;
	int nTotalGroup = EPGetGroupCount(m_nCurModuleID);

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		
	{
		return;
	}

	STR_SAVE_CH_DATA chSaveData;	

	bool bLock = m_wndSmallChGrid.LockUpdate();
	for (INDEX nGroupIndex= 0; nGroupIndex < pModule->GetTotalJig(); nGroupIndex++)	// Group 수 만큼 
	{
		nChannelNo = pModule->GetChInJig(nGroupIndex);

		for (INDEX nChannelIndex = 0; nChannelIndex < nChannelNo; nChannelIndex++)					//Display Channel Ch Per Group
		{	
			if(GetChRowCol(nGroupIndex, nChannelIndex, nRow, nCol) == FALSE)		
			{
				break;
			}

			pModule->GetChannelStepData(chSaveData, nChannelIndex, nGroupIndex);

			strMsg = pDoc->GetStateMsg(chSaveData.state, colorFlag);				//Get State Msg

			// 1. 해당 탭의 조건에 해당하는 에러가 있을 경우
			// 2. 정보를 표시하며 없으면 XX로 표시한다.
			if(chSaveData.channelCode == EP_CODE_CELL_NONE)
			{	
				strMsg = pDoc->ChCodeMsg(chSaveData.channelCode);
				pDoc->GetStateMsg(EP_STATE_IDLE, colorFlag);
			}
			else
			{
				strMsg = pDoc->ChCodeMsg(chSaveData.channelCode);
				pDoc->GetStateMsg(EP_STATE_IDLE, colorFlag);
			}

			m_wndSmallChGrid.SetStyleRange(CGXRange(nRow+1, nCol+1), 
				CGXStyle().SetValue(strMsg).SetTextColor(m_pConfig->m_TStateColor[colorFlag]).SetInterior(m_pConfig->m_BStateColor[colorFlag]));
		}

		//1열 배열 표기시 Column Heaer에 Tray 번호 표시 
		strMsg = pModule->GetTrayNo(nGroupIndex);
		if(strMsg.IsEmpty())
		{
			strMsg.Format("TRAY %d", nGroupIndex+1);
		}

		m_wndSmallChGrid.SetValueRange(CGXRange(0, (nGroupIndex+1)*2), strMsg);	
	}

	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();	
}

void CContactCheckResultView::InitSmallChGrid()
{
	m_wndSmallChGrid.SubclassDlgItem(IDC_LARGE_GRID, this);
	m_wndSmallChGrid.m_bSameRowSize = TRUE;
	m_wndSmallChGrid.m_bSameColSize = TRUE;
	m_wndSmallChGrid.m_bCustomWidth = FALSE;
	m_wndSmallChGrid.m_bCustomColor = FALSE;

	m_wndSmallChGrid.Initialize();
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);
	m_wndSmallChGrid.SetDefaultRowHeight(26);
	m_wndSmallChGrid.SetFrozenRows(1);

	//Enable Multi Channel Select
	m_wndSmallChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndSmallChGrid.EnableGridToolTips();
	m_wndSmallChGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	//Use MemDc
	m_wndSmallChGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	//Row Header Setting
	m_wndSmallChGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));

}

void CContactCheckResultView::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	m_nCurModuleID = nModuleID;

	m_nCurGroup = 0;
	m_CmdTarget.SetText(GetTargetModuleName());	
}

void CContactCheckResultView::SetTopGridFont()
{
	//Font를 Registry에서 Laod
	UINT nSize;
	LPVOID* pData;
	LOGFONT lfGridFont;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(FORMSET_REG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nRtn && nSize > 0)
	{
		memcpy(&lfGridFont, pData, nSize);
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&lfGridFont);
		AfxGetApp()->WriteProfileBinary(FORMSET_REG_SECTION, "GridFont",(LPBYTE)&lfGridFont, sizeof(LOGFONT));
	}
	delete [] pData;

	m_wndSmallChGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont(lfGridFont)));
}

CString CContactCheckResultView::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}

void CContactCheckResultView::StopMonitoring()
{
	if(m_nDisplayTimer == 0)	
	{
		return;
	}

	KillTimer(m_nDisplayTimer);
	m_nDisplayTimer = 0;
}

void CContactCheckResultView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		StartMonitoring();	
	}
}

void CContactCheckResultView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	if( nModuleID != m_nCurModuleID )
	{	
		// 1. 현재 실행중인 모듈이 아니기에 상태값을 변경하지 않는다.
		return;
	}
}

void CContactCheckResultView::StartMonitoring()
{
	// 현재 모듈이 OffLine 상태이면 사용 안함
	if(m_nDisplayTimer)	
	{
		return;
	}
	
	GroupSetChanged();	

	int nTimer = 2000;
	if(m_pConfig)
	{
		nTimer = m_pConfig->m_ChannelInterval*1000;
	}	

	m_nDisplayTimer = SetTimer(102, nTimer, NULL);
}

void CContactCheckResultView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nIDEvent == 102 )
	{	
		DrawChannel();
	}

	CFormView::OnTimer(nIDEvent);
}
