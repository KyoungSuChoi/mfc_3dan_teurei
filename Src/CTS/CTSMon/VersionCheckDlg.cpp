// VersionCheckDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "VersionCheckDlg.h"


// CVersionCheckDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CVersionCheckDlg, CDialog)

CVersionCheckDlg::CVersionCheckDlg(CWnd* pParent , CCTSMonDoc* pDoc /*=NULL*/)
	: CDialog(CVersionCheckDlg::IDD, pParent)
{
	m_pDoc = pDoc;
	m_bHiddenData = FALSE;
	m_nTap = TAP_FW_VERSION;

}

CVersionCheckDlg::~CVersionCheckDlg()
{
}

void CVersionCheckDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
}


BEGIN_MESSAGE_MAP(CVersionCheckDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_SBC_VERSION, &CVersionCheckDlg::OnBnClickedBtnSbcVersion)
	ON_BN_CLICKED(IDC_BTN_FW_VERSION_HISTORY, &CVersionCheckDlg::OnBnClickedBtnVersionHistory)
	ON_STN_DBLCLK(ID_DLG_LABEL_NAME, &CVersionCheckDlg::OnStnDblclickDlgLabelName)
	ON_BN_CLICKED(IDC_BTN_MODIFY, &CVersionCheckDlg::OnBnClickedBtnModify)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CVersionCheckDlg::OnBnClickedBtnSave)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CVersionCheckDlg::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_REFRESH, &CVersionCheckDlg::OnBnClickedBtnRefresh)
	ON_BN_CLICKED(IDC_BTN_SW_VERSION_HISTORY, &CVersionCheckDlg::OnBnClickedBtnSwVersionHistory)
END_MESSAGE_MAP()


// CVersionCheckDlg 메시지 처리기입니다.

BOOL CVersionCheckDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_nTotalUnitNum = m_pDoc->LoadInstalledModuleNum();
	initCtrl();
	UpdateValue();
	UpdateView();
	UpdateFwHistory();
	UpdateSwHistory();

	GetDlgItem(IDC_GRID_FW_VERSION)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_GRID_FW_VERSION_DESC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_GRID_VERSION_HISTORY)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRID_SW_VERSION_HISTORY)->ShowWindow(SW_HIDE);
	
	return TRUE;
}

void CVersionCheckDlg::initCtrl()
{
	initGrid();
	initLabel();
	initFont();
}


void CVersionCheckDlg::initGrid()
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// F/W VERSION DESC
	m_gridFwVersionDesc.SubclassDlgItem(IDC_GRID_FW_VERSION_DESC, this);
	m_gridFwVersionDesc.m_bSameRowSize = FALSE;
	m_gridFwVersionDesc.m_bSameColSize = FALSE;
	m_gridFwVersionDesc.m_bCustomWidth = TRUE;
	m_gridFwVersionDesc.m_bCustomColor = FALSE;

	m_gridFwVersionDesc.Initialize();

	m_gridFwVersionDesc.SetColCount(3);     
	m_gridFwVersionDesc.SetRowCount(4);        
	m_gridFwVersionDesc.SetDefaultRowHeight(25);
	m_gridFwVersionDesc.SetRowHeight(0,0,0);

	m_gridFwVersionDesc.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_gridFwVersionDesc.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_gridFwVersionDesc.EnableGridToolTips();
	m_gridFwVersionDesc.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_gridFwVersionDesc.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_gridFwVersionDesc.m_nWidth[1] = 200;
	m_gridFwVersionDesc.m_nWidth[2] = 600;
	m_gridFwVersionDesc.m_nWidth[3] = 800;

	m_gridFwVersionDesc.SetStyleRange(CGXRange().SetCols(2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
	m_gridFwVersionDesc.SetStyleRange(CGXRange().SetCols(3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
	//Row Header Setting
	m_gridFwVersionDesc.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(12).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE).SetHorizontalAlignment(DT_LEFT));

	//Header
	m_gridFwVersionDesc.SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetInterior(COLOR_FW_MAIN_PROTOCAL));
	m_gridFwVersionDesc.SetStyleRange(CGXRange().SetRows(2), CGXStyle().SetInterior(COLOR_FW_SUB_PROTOCAL));
	m_gridFwVersionDesc.SetStyleRange(CGXRange().SetRows(3), CGXStyle().SetInterior(COLOR_FW_MAIN_REVISION));
	m_gridFwVersionDesc.SetStyleRange(CGXRange().SetRows(4), CGXStyle().SetInterior(COLOR_FW_SUB_REVISION));

	m_gridFwVersionDesc.SetValueRange(CGXRange( 1, 1), "MainProtocal"); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 2, 1), "SubProtocal"); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 3, 1), "MainRevision"); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 4, 1), "SubRevision"); 


	m_gridFwVersionDesc.SetValueRange(CGXRange( 1, 2), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_1", "-") ); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 2, 2), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_2", "-") ); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 3, 2), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_3", "-") ); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 4, 2), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_4", "-") ); 

	m_gridFwVersionDesc.SetValueRange(CGXRange( 1, 3), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_ex_1", "-") ); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 2, 3), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_ex_2", "-") ); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 3, 3), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_ex_3", "-") ); 
	m_gridFwVersionDesc.SetValueRange(CGXRange( 4, 3), AfxGetApp()->GetProfileString(VERSION_SECTION, "Version_Desc_ex_4", "-") ); 


	m_gridFwVersionDesc.Redraw();


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// F/W VERSION
	m_gridFwVersion.SubclassDlgItem(IDC_GRID_FW_VERSION, this);
	m_gridFwVersion.m_bSameRowSize = FALSE;
	m_gridFwVersion.m_bSameColSize = FALSE;
	m_gridFwVersion.m_bCustomWidth = TRUE;
	m_gridFwVersion.m_bCustomColor = FALSE;

	m_gridFwVersion.Initialize();

	m_gridFwVersion.SetColCount(6);     
	m_gridFwVersion.SetRowCount(m_nTotalUnitNum+1);        
	m_gridFwVersion.SetDefaultRowHeight(20);
	m_gridFwVersion.SetRowHeight(0,0,0);

	m_gridFwVersion.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_gridFwVersion.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_gridFwVersion.EnableGridToolTips();
	m_gridFwVersion.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_gridFwVersion.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_gridFwVersion.m_nWidth[1] = 150;
	m_gridFwVersion.m_nWidth[2] = 300;
	m_gridFwVersion.m_nWidth[3] = 300;
	m_gridFwVersion.m_nWidth[4] = 300;
	m_gridFwVersion.m_nWidth[5] = 300;
	m_gridFwVersion.m_nWidth[6] = 300;


	//Row Header Setting
	m_gridFwVersion.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(10).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));
	m_gridFwVersion.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(COLOR_FW_STAGE));
	m_gridFwVersion.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(COLOR_FW_MAIN_PROTOCAL));
	m_gridFwVersion.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(COLOR_FW_SUB_PROTOCAL));
	m_gridFwVersion.SetStyleRange(CGXRange().SetCols(4), CGXStyle().SetInterior(COLOR_FW_MAIN_REVISION));
	m_gridFwVersion.SetStyleRange(CGXRange().SetCols(5), CGXStyle().SetInterior(COLOR_FW_SUB_REVISION));
	m_gridFwVersion.SetStyleRange(CGXRange().SetCols(6), CGXStyle().SetInterior(RGB(220, 220, 240)));

	//Header
	m_gridFwVersion.SetStyleRange(CGXRange(1,1), CGXStyle().SetInterior(COLOR_FW_STAGE_B));
	m_gridFwVersion.SetStyleRange(CGXRange(1,2), CGXStyle().SetInterior(COLOR_FW_MAIN_PROTOCAL_B));
	m_gridFwVersion.SetStyleRange(CGXRange(1,3), CGXStyle().SetInterior(COLOR_FW_SUB_PROTOCAL_B));
	m_gridFwVersion.SetStyleRange(CGXRange(1,4), CGXStyle().SetInterior(COLOR_FW_MAIN_REVISION_B));
	m_gridFwVersion.SetStyleRange(CGXRange(1,5), CGXStyle().SetInterior(COLOR_FW_SUB_REVISION_B));
	m_gridFwVersion.SetStyleRange(CGXRange(1,6), CGXStyle().SetInterior(RGB(210, 210, 230)));


	m_gridFwVersion.SetValueRange(CGXRange( 1, 1), "Stage"); //"순서"
	m_gridFwVersion.SetValueRange(CGXRange( 1, 2), "MainProtocal"); 
	m_gridFwVersion.SetValueRange(CGXRange( 1, 3), "SubProtocal"); 
	m_gridFwVersion.SetValueRange(CGXRange( 1, 4), "MainRevision"); 
	m_gridFwVersion.SetValueRange(CGXRange( 1, 5), "SubRevision"); 
	m_gridFwVersion.SetValueRange(CGXRange( 1, 6), "F/W Bild"); 



	//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);

	CFormModule *pModule;
	for(int nIdx =0; nIdx<m_nTotalUnitNum; nIdx++)
	{

		pModule = m_pDoc->GetModuleInfo(nIdx+1);
		if(pModule)
		{
			CString strName = pModule->GetModuleName();
			m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,1), strName);
		}
	}

	m_gridFwVersion.Redraw();


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// F/W VERSION HISTORY

	m_gridFwVersionHistory.SubclassDlgItem(IDC_GRID_VERSION_HISTORY, this);
	m_gridFwVersionHistory.m_bSameRowSize = FALSE;
	m_gridFwVersionHistory.m_bSameColSize = FALSE;
	m_gridFwVersionHistory.m_bCustomWidth = TRUE;
	m_gridFwVersionHistory.m_bCustomColor = FALSE;

	m_gridFwVersionHistory.Initialize();

	m_gridFwVersionHistory.SetColCount(7);     
	m_gridFwVersionHistory.SetRowCount(MAX_FW_HISTORY_LOG+1);        
	m_gridFwVersionHistory.SetDefaultRowHeight(20);
	m_gridFwVersionHistory.SetRowHeight(0,0,0);

	m_gridFwVersionHistory.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_gridFwVersionHistory.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_gridFwVersionHistory.EnableGridToolTips();
	m_gridFwVersionHistory.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_gridFwVersionHistory.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_gridFwVersionHistory.m_nWidth[1] = 100;
	m_gridFwVersionHistory.m_nWidth[2] = 130;
	m_gridFwVersionHistory.m_nWidth[3] = 130;
	m_gridFwVersionHistory.m_nWidth[4] = 130;
	m_gridFwVersionHistory.m_nWidth[5] = 130;
	m_gridFwVersionHistory.m_nWidth[6] = 500;
	m_gridFwVersionHistory.m_nWidth[7] = 300;


	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(6,7),CGXStyle().SetHorizontalAlignment(DT_LEFT));


	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCells(2,2,MAX_FW_HISTORY_LOG+1,7),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	//Row Header Setting
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(10).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetInterior(COLOR_FW_STAGE));
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(COLOR_FW_MAIN_PROTOCAL));
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(COLOR_FW_SUB_PROTOCAL));
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(4), CGXStyle().SetInterior(COLOR_FW_MAIN_REVISION));
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(5), CGXStyle().SetInterior(COLOR_FW_SUB_REVISION));
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(6), CGXStyle().SetInterior(RGB(240, 240, 240)));
	m_gridFwVersionHistory.SetStyleRange(CGXRange().SetCols(7), CGXStyle().SetInterior(RGB(250, 250, 250)));

	//Header
	m_gridFwVersionHistory.SetStyleRange(CGXRange(1,1), CGXStyle().SetInterior(COLOR_FW_STAGE_B));
	m_gridFwVersionHistory.SetStyleRange(CGXRange(1,2), CGXStyle().SetInterior(COLOR_FW_MAIN_PROTOCAL_B));
	m_gridFwVersionHistory.SetStyleRange(CGXRange(1,3), CGXStyle().SetInterior(COLOR_FW_SUB_PROTOCAL_B));
	m_gridFwVersionHistory.SetStyleRange(CGXRange(1,4), CGXStyle().SetInterior(COLOR_FW_MAIN_REVISION_B));
	m_gridFwVersionHistory.SetStyleRange(CGXRange(1,5), CGXStyle().SetInterior(COLOR_FW_SUB_REVISION_B));
	m_gridFwVersionHistory.SetStyleRange(CGXRange(1,6), CGXStyle().SetInterior(RGB(230, 230, 230)));
	m_gridFwVersionHistory.SetStyleRange(CGXRange(1,7), CGXStyle().SetInterior(RGB(240, 240, 240)));


	m_gridFwVersionHistory.SetValueRange(CGXRange( 1, 1), "Stage"); //"순서"
	m_gridFwVersionHistory.SetValueRange(CGXRange( 1, 2), "MainProtocal"); 
	m_gridFwVersionHistory.SetValueRange(CGXRange( 1, 3), "SubProtocal"); 
	m_gridFwVersionHistory.SetValueRange(CGXRange( 1, 4), "MainRevision"); 
	m_gridFwVersionHistory.SetValueRange(CGXRange( 1, 5), "SubRevision"); 
	m_gridFwVersionHistory.SetValueRange(CGXRange( 1, 6), "Desc"); 
	m_gridFwVersionHistory.SetValueRange(CGXRange( 1, 7), "remarks"); 



	//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);

	CString strName;
	for(int nIdx =0; nIdx<MAX_FW_HISTORY_LOG; nIdx++)
	{
		strName.Format("%d",nIdx+1);
		m_gridFwVersionHistory.SetValueRange(CGXRange(nIdx+2,1), strName);

	}
	m_gridFwVersionHistory.Redraw();


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// S/W VERSION HISTORY

	m_gridSwVersionHistory.SubclassDlgItem(IDC_GRID_SW_VERSION_HISTORY, this);
	m_gridSwVersionHistory.m_bSameRowSize = FALSE;
	m_gridSwVersionHistory.m_bSameColSize = FALSE;
	m_gridSwVersionHistory.m_bCustomWidth = TRUE;
	m_gridSwVersionHistory.m_bCustomColor = FALSE;

	m_gridSwVersionHistory.Initialize();

	m_gridSwVersionHistory.SetColCount(6);     
	m_gridSwVersionHistory.SetRowCount(MAX_SW_HISTORY_LOG+1);        
	m_gridSwVersionHistory.SetDefaultRowHeight(20);
	m_gridSwVersionHistory.SetRowHeight(0,0,0);

	m_gridSwVersionHistory.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_gridSwVersionHistory.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_gridSwVersionHistory.EnableGridToolTips();
	m_gridSwVersionHistory.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetCols(6),CGXStyle().SetHorizontalAlignment(DT_LEFT));

	m_gridSwVersionHistory.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_gridSwVersionHistory.m_nWidth[1] = 150;
	m_gridSwVersionHistory.m_nWidth[2] = 150;
	m_gridSwVersionHistory.m_nWidth[3] = 150;
	m_gridSwVersionHistory.m_nWidth[4] = 150;
	m_gridSwVersionHistory.m_nWidth[5] = 150;
	m_gridSwVersionHistory.m_nWidth[6] = 600;

	//Row Header Setting
	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(10).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));
	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetInterior(COLOR_FW_STAGE));
	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(COLOR_FW_MAIN_PROTOCAL));
	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(COLOR_FW_SUB_PROTOCAL));
	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetCols(4), CGXStyle().SetInterior(COLOR_FW_MAIN_REVISION));
	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetCols(5), CGXStyle().SetInterior(COLOR_FW_SUB_REVISION));
	m_gridSwVersionHistory.SetStyleRange(CGXRange().SetCols(6), CGXStyle().SetInterior(RGB(240, 240, 240)));

	//Header
	m_gridSwVersionHistory.SetStyleRange(CGXRange(1,1), CGXStyle().SetInterior(COLOR_FW_STAGE_B));
	m_gridSwVersionHistory.SetStyleRange(CGXRange(1,2), CGXStyle().SetInterior(COLOR_FW_MAIN_PROTOCAL_B));
	m_gridSwVersionHistory.SetStyleRange(CGXRange(1,3), CGXStyle().SetInterior(COLOR_FW_SUB_PROTOCAL_B));
	m_gridSwVersionHistory.SetStyleRange(CGXRange(1,4), CGXStyle().SetInterior(COLOR_FW_MAIN_REVISION_B));
	m_gridSwVersionHistory.SetStyleRange(CGXRange(1,5), CGXStyle().SetInterior(COLOR_FW_SUB_REVISION_B));
	m_gridSwVersionHistory.SetStyleRange(CGXRange(1,6), CGXStyle().SetInterior(RGB(220, 220, 220)));


	m_gridSwVersionHistory.SetValueRange(CGXRange( 1, 1), "F/W"); //"순서"
	m_gridSwVersionHistory.SetValueRange(CGXRange( 1, 2), "IT"); 
	m_gridSwVersionHistory.SetValueRange(CGXRange( 1, 3), "UI"); 
	m_gridSwVersionHistory.SetValueRange(CGXRange( 1, 4), "Debug"); 
	m_gridSwVersionHistory.SetValueRange(CGXRange( 1, 5), "Site"); 
	m_gridSwVersionHistory.SetValueRange(CGXRange( 1, 6), "Desc"); 

	//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);
	m_gridSwVersionHistory.Redraw();

}


void CVersionCheckDlg::initLabel()
{
	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("F/W Version"); 
}

void CVersionCheckDlg::initFont()
{
	LOGFONT LogFont;

	GetDlgItem(IDC_BTN_SBC_VERSION)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 25;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_BTN_SBC_VERSION)->SetFont(&m_Font);
	GetDlgItem(IDC_BTN_FW_VERSION_HISTORY)->SetFont(&m_Font);
	GetDlgItem(IDC_BTN_SW_VERSION_HISTORY)->SetFont(&m_Font);
}

void CVersionCheckDlg::UpdateValue()
{
	EP_MD_SYSTEM_DATA *lpSysData;
	EP_MD_FW_VER_DATA *lpFwData;
	for(int nIdx =0; nIdx<m_nTotalUnitNum; nIdx++)
	{
		WORD gpState = EPGetGroupState(nIdx+1);
		if( gpState != EP_STATE_LINE_OFF )
		{
			lpSysData	= EPGetModuleSysData(nIdx);
			lpFwData	= EPGetModuleVersionData(nIdx);
			if(lpFwData != NULL)
			{
				CString strPro ="";
				CString strSubPro ="";
				CString strRev ="";
				CString strSubRev ="";
				CString strFwVerison ="";

				for(int i=0; i<4; i++)
				{
					strPro +=lpFwData->cMainPro[i];
				}
				for(int i=0; i<3; i++)
				{
					strSubPro +=lpFwData->cSubPro[i];
				}
				for(int i=0; i<4; i++)
				{
					strRev +=lpFwData->cMainRev[i];
				}
				for(int i=0; i<3; i++)
				{
					strSubRev +=lpFwData->cSubRev[i];
				}
				strFwVerison.Format("%d",lpSysData->nVersion);

				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,2), strPro);
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,3), strSubPro);
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,4), strRev);
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,5), strSubRev);
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,6), strFwVerison);
			}
			else
			{
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,2), "-");
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,3), "-");
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,4), "-");
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,5), "-");
				m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,6), "-");
			}
		}
		else
		{
			m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,2), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,3), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,4), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,5), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nIdx+2,6), "-");
		}
	}
}

void CVersionCheckDlg::UpdateValue(int nModuleID)
{
	EP_MD_SYSTEM_DATA *lpSysData;
	EP_MD_FW_VER_DATA *lpFwData;

	int nModuleIdx = nModuleID - 1;
	WORD gpState = EPGetGroupState(nModuleIdx+1);
	if( gpState != EP_STATE_LINE_OFF )
	{
		lpSysData	= EPGetModuleSysData(nModuleIdx);
		lpFwData	= EPGetModuleVersionData(nModuleIdx);
		if(lpFwData != NULL)
		{
			CString strPro ="";
			CString strSubPro ="";
			CString strRev ="";
			CString strSubRev ="";
			CString strFwVerison ="";

			for(int i=0; i<4; i++)
			{
				strPro +=lpFwData->cMainPro[i];
			}
			for(int i=0; i<3; i++)
			{
				strSubPro +=lpFwData->cSubPro[i];
			}
			for(int i=0; i<4; i++)
			{
				strRev +=lpFwData->cMainRev[i];
			}
			for(int i=0; i<3; i++)
			{
				strSubRev +=lpFwData->cSubRev[i];
			}
			strFwVerison.Format("%d",lpSysData->nVersion);

			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,2), strPro);
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,3), strSubPro);
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,4), strRev);
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,5), strSubRev);
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,6), strFwVerison);
		}
		else
		{
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,2), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,3), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,4), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,5), "-");
			m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,6), "-");
		}
	}
	else
	{
		m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,2), "-");
		m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,3), "-");
		m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,4), "-");
		m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,5), "-");
		m_gridFwVersion.SetValueRange(CGXRange(nModuleIdx+2,6), "-");
	}

}


void CVersionCheckDlg::UpdateView()
{
	if(m_bHiddenData == TRUE)
	{
		m_gridFwVersion.m_nWidth[1] = 150;
		m_gridFwVersion.m_nWidth[2] = 300;
		m_gridFwVersion.m_nWidth[3] = 300;
		m_gridFwVersion.m_nWidth[4] = 300;
		m_gridFwVersion.m_nWidth[5] = 300;
		m_gridFwVersion.m_nWidth[6] = 300;
		m_gridFwVersion.RedrawWindow();
		GetDlgItem(IDC_GRID_FW_VERSION_DESC)->EnableWindow(TRUE);
	}
	else
	{
		m_gridFwVersion.m_nWidth[1] = 150;
		m_gridFwVersion.m_nWidth[2] = 370;
		m_gridFwVersion.m_nWidth[3] = 370;
		m_gridFwVersion.m_nWidth[4] = 370;
		m_gridFwVersion.m_nWidth[5] = 370;
		m_gridFwVersion.m_nWidth[6] = 0;
		m_gridFwVersion.RedrawWindow();
		GetDlgItem(IDC_GRID_FW_VERSION_DESC)->EnableWindow(FALSE);
	}
}

void CVersionCheckDlg::UpdateFwHistory()
{
	CString strKey;
	CString strTemp;
	for(int i =0; i < MAX_FW_HISTORY_LOG; i++)
	{
		strKey.Format("History_MainProtocal_%d",i);
		m_gridFwVersionHistory.SetValueRange(CGXRange(i+2,2), AfxGetApp()->GetProfileString(VERSION_SECTION, strKey, ""));
		strKey.Format("History_SubProtocal_%d",i);
		m_gridFwVersionHistory.SetValueRange(CGXRange(i+2,3), AfxGetApp()->GetProfileString(VERSION_SECTION, strKey, ""));
		strKey.Format("History_MainRevision_%d",i);
		m_gridFwVersionHistory.SetValueRange(CGXRange(i+2,4), AfxGetApp()->GetProfileString(VERSION_SECTION, strKey, ""));
		strKey.Format("History_SubRevision_%d",i);
		m_gridFwVersionHistory.SetValueRange(CGXRange(i+2,5), AfxGetApp()->GetProfileString(VERSION_SECTION, strKey, ""));
		strKey.Format("History_Desc_%d",i);
		m_gridFwVersionHistory.SetValueRange(CGXRange(i+2,6), AfxGetApp()->GetProfileString(VERSION_SECTION, strKey, ""));
		strKey.Format("History_Remarks_%d",i);
		m_gridFwVersionHistory.SetValueRange(CGXRange(i+2,7), AfxGetApp()->GetProfileString(VERSION_SECTION, strKey, ""));
	}
}


void CVersionCheckDlg::UpdateSwHistory()
{

	m_gridSwVersionHistory.SetValueRange(CGXRange().SetCells(2,1,MAX_SW_HISTORY_LOG+1,6),"");
	CFile file;
	// 	CFileException* e;
	// 	e = new CFileException;

	CString strCurFolder = AfxGetApp()->GetProfileString(VERSION_SECTION ,"SwVersionFilePath","C:/Program Files (x86)/PNE CTS/swHistory");//20191001KSJ

	if(!file.Open(strCurFolder+"\\SwHistory.cfg", CFile::modeRead|CFile::typeBinary ))
	{
		return ;
	}

	WCHAR *buffer;
	buffer = new WCHAR[file.GetLength()];

	WCHAR buf[1];

	int i = 0;
	int nRength = 0;
	int nIdx = 0;

	CString strData;
	CString strTemp;

	file.Read(buf, sizeof(WCHAR));
	while (1)
	{
		file.Read(buf, sizeof(WCHAR));

		if( buf[0] == 0x0d )
		{
			buffer[i] = 0x00;
			CString strTemp2;
			strTemp2.Format("%s", buffer);
			int nLen = WideCharToMultiByte(CP_ACP, 0, buffer, -1, NULL, 0, NULL, NULL);			
			char* pMultibyte  = new char[nLen];
			memset(pMultibyte, 0x00, (nLen)*sizeof(char));
			WideCharToMultiByte(CP_ACP, 0, buffer, -1, pMultibyte, nLen, NULL, NULL);
			ZeroMemory( buffer, sizeof(WCHAR) * file.GetLength());			
			strData = pMultibyte;
			delete pMultibyte;
			i = 0;

			if(!strData.IsEmpty())
			{

				AfxExtractSubString(strTemp, strData, 0,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,1),strTemp);
				AfxExtractSubString(strTemp, strData, 1,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,2),strTemp);
				AfxExtractSubString(strTemp, strData, 2,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,3),strTemp);
				AfxExtractSubString(strTemp, strData, 3,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,4),strTemp);
				AfxExtractSubString(strTemp, strData, 4,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,5),strTemp.Left(strTemp.Find(" ")));
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,6),strTemp.Mid(strTemp.Find(" ")));
				nIdx++;
			}			
		}
		else
		{
			if(buf[0] != 0x0A)
			{
				buffer[i] = buf[0];		
				i++;
			}
		}
		nRength++;

		if( nRength*2 >= file.GetLength())
		{
			buffer[i] = 0x00;
			int nLen = WideCharToMultiByte(CP_ACP, 0, buffer, -1, NULL, 0, NULL, NULL);			
			char* pMultibyte  = new char[nLen];
			memset(pMultibyte, 0x00, (nLen)*sizeof(char));
			WideCharToMultiByte(CP_ACP, 0, buffer, -1, pMultibyte, nLen, NULL, NULL);
			ZeroMemory( buffer, sizeof(WCHAR) * file.GetLength());			

			strData = pMultibyte;
			delete pMultibyte;
			i = 0;

			if(!strData.IsEmpty())
			{

				AfxExtractSubString(strTemp, strData, 0,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,1),strTemp);
				AfxExtractSubString(strTemp, strData, 1,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,2),strTemp);
				AfxExtractSubString(strTemp, strData, 2,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,3),strTemp);
				AfxExtractSubString(strTemp, strData, 3,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,4),strTemp);
				AfxExtractSubString(strTemp, strData, 4,'.');
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,5),strTemp.Left(strTemp.Find(" ")));
				m_gridSwVersionHistory.SetValueRange(CGXRange(nIdx+2,6),strTemp.Mid(strTemp.Find(" ")));
				nIdx++;
			}			
			break;
		}
	}

	m_gridSwVersionHistory.Redraw();
	file.Close();
	delete buffer;

}



void CVersionCheckDlg::OnBnClickedBtnSbcVersion()
{
	GetDlgItem(IDC_GRID_FW_VERSION)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_GRID_FW_VERSION_DESC)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_GRID_VERSION_HISTORY)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRID_SW_VERSION_HISTORY)->ShowWindow(SW_HIDE);
	m_LabelViewName.SetText("F/W Version"); 

	GetDlgItem(IDC_BTN_REFRESH)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CANCEL)->EnableWindow(FALSE);

	UpdateFwHistory();

	m_nTap = TAP_FW_VERSION;
}

void CVersionCheckDlg::OnBnClickedBtnVersionHistory()
{
	GetDlgItem(IDC_GRID_FW_VERSION)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRID_FW_VERSION_DESC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRID_VERSION_HISTORY)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_GRID_SW_VERSION_HISTORY)->ShowWindow(SW_HIDE);
	m_LabelViewName.SetText("F/W Version History"); 

	GetDlgItem(IDC_BTN_REFRESH)->EnableWindow(FALSE);

	GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CANCEL)->EnableWindow(FALSE);

	UpdateFwHistory();
	m_nTap = TAP_FW_VERSION_HISTORY;

}

void CVersionCheckDlg::OnBnClickedBtnSwVersionHistory()
{
	GetDlgItem(IDC_GRID_FW_VERSION)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRID_FW_VERSION_DESC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRID_VERSION_HISTORY)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GRID_SW_VERSION_HISTORY)->ShowWindow(SW_SHOW);
	m_LabelViewName.SetText("S/W Version History"); 

	GetDlgItem(IDC_BTN_REFRESH)->EnableWindow(FALSE);

	GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CANCEL)->EnableWindow(FALSE);

	UpdateSwHistory();
	m_nTap = TAP_SW_VERSION_HISTORY;
}


void CVersionCheckDlg::OnStnDblclickDlgLabelName()
{
	if(m_nTap == TAP_FW_VERSION)
	{
		CString strKey;
		CString strTemp;
		if( m_bHiddenData == TRUE)
		{
			m_bHiddenData = FALSE;

			for(int i=0;i<4;i++)
			{
				strKey.Format("Version_Desc_%d",i+1);
				strTemp = m_gridFwVersionDesc.GetValueRowCol(i+1,2);
				AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);	

				strKey.Format("Version_Desc_ex_%d",i+1);
				strTemp = m_gridFwVersionDesc.GetValueRowCol(i+1,3);
				AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);	
			}
		}
		else
		{
			m_bHiddenData = TRUE;
		}
		UpdateView();
	}
}

void CVersionCheckDlg::OnBnClickedBtnModify()
{
	GetDlgItem(IDC_GRID_VERSION_HISTORY)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_CANCEL)->EnableWindow(TRUE);
}

void CVersionCheckDlg::OnBnClickedBtnSave()
{
	CString strKey;
	CString strTemp;

	GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CANCEL)->EnableWindow(FALSE);

	if(m_nTap == TAP_FW_VERSION_HISTORY)
	{
		for(int i =0; i < MAX_FW_HISTORY_LOG; i++)
		{
			strKey.Format("History_MainProtocal_%d",i);
			strTemp = m_gridFwVersionHistory.GetValueRowCol(i+2,2);
			AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);
			strKey.Format("History_SubProtocal_%d",i);
			strTemp = m_gridFwVersionHistory.GetValueRowCol(i+2,3);
			AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);
			strKey.Format("History_MainRevision_%d",i);
			strTemp = m_gridFwVersionHistory.GetValueRowCol(i+2,4);
			AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);
			strKey.Format("History_SubRevision_%d",i);
			strTemp = m_gridFwVersionHistory.GetValueRowCol(i+2,5);
			AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);
			strKey.Format("History_Desc_%d",i);
			strTemp = m_gridFwVersionHistory.GetValueRowCol(i+2,6);
			AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);
			strKey.Format("History_Remarks_%d",i);
			strTemp = m_gridFwVersionHistory.GetValueRowCol(i+2,7);
			AfxGetApp()->WriteProfileString(VERSION_SECTION, strKey, strTemp);
		}
		GetDlgItem(IDC_GRID_VERSION_HISTORY)->EnableWindow(FALSE);
		UpdateFwHistory();
	}
}

void CVersionCheckDlg::OnBnClickedBtnCancel()
{
	GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CANCEL)->EnableWindow(FALSE);

	if(m_nTap == TAP_FW_VERSION_HISTORY)
	{
		GetDlgItem(IDC_GRID_VERSION_HISTORY)->EnableWindow(FALSE);
		UpdateFwHistory();
	}

}

void CVersionCheckDlg::OnBnClickedBtnRefresh()
{
	for(int nIdx =0; nIdx<m_nTotalUnitNum; nIdx++)
	{
		WORD gpState = EPGetGroupState(nIdx+1);
		if( gpState == EP_STATE_IDLE )
		{
			if((EPSendCommand( nIdx+1, 0, 0, EP_CMD_SAFETY_VERSION_REQUEST)) != EP_ACK)
			{
				
			}
		}
	}

}

