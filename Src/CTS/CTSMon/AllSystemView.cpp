 // AllSystemView.cpp : implementation file
#include "stdafx.h"
#include "CTSMonDoc.h"
#include "CTSMon.h"
#include "AllSystemView.h"

#include "RebootDlg.h"
#include "MainFrm.h"
#include "ProcedureSelDlg.h"
#include "CellVoltageError.h"
#include "ErrorHistoryDlg.h"

#include "./Global/Mysql/PneApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAllSystemView

IMPLEMENT_DYNCREATE(CAllSystemView, CFormView)

#define MAX_USE_TEMP	16
#define TIMER_MAIN		603
#define TIMER_AUTOCALI  604


CAllSystemView::CAllSystemView()
	: CFormView(CAllSystemView::IDD)
	, m_bCaliDlgOn(false)
{ 		
	LanguageinitMonConfig(_T("CAllSystemView"));

	m_nCurModuleID = 0;
	m_nPrevModuleID = 0;
	m_nCurGroup = 0;

	//{{AFX_DATA_INIT(CAllSystemView)
	m_bLayoutView = FALSE;
	m_bCodeCount = FALSE;
	//}}AFX_DATA_INIT
	
	m_nLayOutCol = 10;
	m_nLayOutRow = 7;
	m_pImagelist = NULL;
	m_bFlash = FALSE;
	m_bSendMsgCmd = FALSE;
	m_bRackIndexUp = FALSE;

	m_nJigTempAvg = 0;
	m_nJigTempSendUnitIndex = 0;
	m_lUnitJigTempAvg = 0L;
	m_nTotalModule = 0;
	m_nSelectItem = STATUS_VIEW;
	m_bUpdateModule = false;
	m_bFmsConnectState = false;
	
	m_nTemp = 0;
	m_nPreDay = 0;

	m_pSensorMap = NULL;
	g_nCalistart = 0;   //KSH 20190814 자동교정 시작 플래그
	g_nCaliMode = 0;
	ZeroMemory( m_bLocalReservation, sizeof(m_bLocalReservation));	
}

CAllSystemView::~CAllSystemView()
{	
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

	if(m_pImagelist)
	{
		delete m_pImagelist;
		m_pImagelist = NULL;
	}
}

bool CAllSystemView::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
			else
			{
				TEXT_LANG[i].Replace("\\d","\d");
				TEXT_LANG[i].Replace("\\s","\s");
				TEXT_LANG[i].Replace("\\n","\n");
				TEXT_LANG[i].Replace("\\N","\N");
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CAllSystemView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAllSystemView)
	DDX_Control(pDX, IDC_ERRORCODE_SEL_COMBO, m_ctrlCodeCombo);
	DDX_Control(pDX, IDC_STAGE_SEL_COMBO, m_ctrlStateSelCombo);
	DDX_Control(pDX, IDC_RUN_MODULE, m_ctrlRunModule);
	DDX_Control(pDX, IDC_IDLE_MODULE, m_ctrlIdleModule);
	DDX_Control(pDX, IDC_LINE_OFF_MODULE, m_ctrlLineOffModule);
	DDX_Control(pDX, IDC_TOTAL_MODULE, m_ctrlTotalModule);
	DDX_Check(pDX, IDC_DISP_TYPE, m_bLayoutView);
	DDX_Check(pDX, IDC_CHECK_CODE, m_bCodeCount);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, ID_DLG_LABEL_SELECT_NAME, m_LabelSelectName);
	DDX_Control(pDX, IDC_LABEL_LOG, m_LabelLog);
	DDX_Control(pDX, IDC_LABEL_LOG_NUM, m_LabelLogNum);
	DDX_Control(pDX, IDC_LABEL_LOG_TIME, m_LabelLogTime);
	DDX_Control(pDX, IDC_BTN_STATUS, m_Btn_Status);
	DDX_Control(pDX, IDC_BTN_OPERATION_MODE, m_Btn_OperationMode);
	DDX_Control(pDX, IDC_BTN_INSPECTION_PASS_TIME, m_Btn_InspectionPassTime);
	DDX_Control(pDX, IDC_BTN_OPERATION_VIEW, m_Btn_OperationView);	
	DDX_Control(pDX, IDC_BTN_TYPE_SETTING_LOCAL, m_Btn_TypeSetting);
	DDX_Control(pDX, IDC_BTN_LOG_PREV, m_BtnLogPrev);
	DDX_Control(pDX, IDC_BTN_LOG_NEXT, m_BtnLogNext);
	DDX_Control(pDX, IDC_BTN_LOG_DELETE, m_BtnLogDelete);
	DDX_Control(pDX, IDC_BTN_ERROR_HISTORY, m_BtnErrorHistory);	
	DDX_Control(pDX, ID_LABEL_FMS_CONNECT_STATE, m_LabelFmsConnectState);
	DDX_Control(pDX, IDC_BTN_CHARGER_ALARMOFF, m_BtnAlarmOff);
	DDX_Control(pDX, ID_DLG_LABEL_DATE, m_LabelDate);		
	DDX_Control(pDX, IDC_BTN_TEMPERATURE, m_BtnTemperature);
	DDX_Control(pDX, ID_LABEL_TRAYTYPE_IMS, m_LabelTraytype_ims);
}

BEGIN_MESSAGE_MAP(CAllSystemView, CFormView)
	//{{AFX_MSG_MAP(CAllSystemView)
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_STAGE_SEL_COMBO, OnSelchangeStageSelCombo)
	ON_COMMAND(ID_RUN_MODULE, OnRunModule)
	ON_COMMAND(ID_STOP, OnStop)
	ON_COMMAND(ID_RECONNECT, OnReconnect)
	ON_UPDATE_COMMAND_UI(ID_RECONNECT, OnUpdateReconnect)
	ON_UPDATE_COMMAND_UI(ID_RUN_MODULE, OnUpdateRunModule)
	ON_UPDATE_COMMAND_UI(ID_STOP, OnUpdateStop)
	ON_COMMAND(ID_PAUSE, OnPause)
	ON_UPDATE_COMMAND_UI(ID_PAUSE, OnUpdatePause)
	ON_COMMAND(ID_CONTINUE_MODULE, OnContinueModule)
	ON_UPDATE_COMMAND_UI(ID_CONTINUE_MODULE, OnUpdateContinueModule)
	ON_COMMAND(ID_CLEAR, OnClear)
	ON_UPDATE_COMMAND_UI(ID_CLEAR, OnUpdateClear)
	ON_COMMAND(ID_STEPOVER, OnStepover)
	ON_UPDATE_COMMAND_UI(ID_STEPOVER, OnUpdateStepover)
	ON_BN_CLICKED(IDC_DISP_TYPE, OnDispType)
	ON_COMMAND(ID_ONLINE_MODE, OnOnlineMode)
	ON_UPDATE_COMMAND_UI(ID_ONLINE_MODE, OnUpdateOnlineMode)
	ON_COMMAND(ID_OFFLINE_MODE, OnOfflineMode)
	ON_UPDATE_COMMAND_UI(ID_OFFLINE_MODE, OnUpdateOfflineMode)
	ON_COMMAND(ID_MAINTENANCE_MODE, OnMaintenanceMode)
	ON_UPDATE_COMMAND_UI(ID_MAINTENANCE_MODE, OnUpdateMaintenanceMode)
	ON_UPDATE_COMMAND_UI(ID_CALI_MODE, OnUpdateCaliMode)
	ON_COMMAND(ID_CONTROL_MODE, OnControlMode)
	ON_COMMAND(ID_JIG_HEIGHT_MODE_HIGH, OnJigHeightModeHigh)
	ON_COMMAND(ID_JIG_HEIGHT_MODE_MIDDLE, OnJigHeightModeMiddle)
	ON_COMMAND(ID_JIG_HEIGHT_MODE_LOW, OnJigHeightModeLow)
	ON_UPDATE_COMMAND_UI(ID_CONTROL_MODE, OnUpdateControlMode)
	ON_COMMAND(ID_REBOOT, OnReboot)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_AUTO_PROC_ON, OnUpdateAutoProcOn)
	ON_UPDATE_COMMAND_UI(ID_AUTO_PROC_OFF, OnUpdateAutoProcOff)
	ON_COMMAND(ID_AUTO_PROC_ON, OnAutoProcOn)
	ON_COMMAND(ID_AUTO_PROC_OFF, OnAutoProcOff)
	ON_COMMAND(ID_REF_AD_VALUE_UPDATE, OnRefAdValueUpdate)
	ON_COMMAND(ID_TRAYNO_USER_INPUT, OnTraynoUserInput)
	ON_UPDATE_COMMAND_UI(ID_TRAYNO_USER_INPUT, OnUpdateTraynoUserInput)
	ON_COMMAND(ID_VIEW_RESULT, OnViewResult)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RESULT, OnUpdateViewResult)
	ON_COMMAND(ID_CONDITION_VIEW, OnConditionView)
	ON_UPDATE_COMMAND_UI(ID_CONDITION_VIEW, OnUpdateConditionView)
	ON_COMMAND(ID_DATA_RESTORE, OnDataRestore)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_UPDATE_COMMAND_UI(ID_DATA_RESTORE, OnUpdateDataRestore)
	ON_BN_CLICKED(IDC_CHECK_CODE, OnCheckCode)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_UPDATE_COMMAND_UI(ID_CHANGE_TRAY_TYPE1, OnUpdateChangeTrayType1)
	ON_UPDATE_COMMAND_UI(ID_CHANGE_TRAY_TYPE2, OnUpdateChangeTrayType2)
	ON_COMMAND(ID_CHANGE_TRAY_TYPE1, OnChangeTrayType1)
	ON_COMMAND(ID_CHANGE_TRAY_TYPE2, OnChangeTrayType2)
	ON_COMMAND(ID_LAYOUT_VIEW, OnLayoutView)
	ON_CBN_SELCHANGE(IDC_ERRORCODE_SEL_COMBO, OnSelchangeErrorcodeSelCombo)
	ON_COMMAND(ID_GET_PROFILE_DATA, OnGetProfileData)
	ON_COMMAND(ID_NEW_PROC, OnNewProc)
	ON_UPDATE_COMMAND_UI(ID_NEW_PROC, OnUpdateNewProc)
	//}}AFX_MSG_MAP

	ON_COMMAND(ID_FILE_PRINT, 			CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, 	CView::OnFilePrintPreview)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK,		OnRButtonClickedRowCol)
	ON_MESSAGE(WM_GRID_MOVECELL,		OnMovedCurrentCell)
	ON_MESSAGE(WM_SEL_CHANGED, 			OnGridSelChanged)
	ON_MESSAGE(WM_GRID_DOUBLECLICK,		OnGridDoubleClick)
	ON_MESSAGE(EPWM_FMS_PINGRECVED,		OnRecvPingStatus)

	ON_BN_CLICKED(IDC_BTN_STATUS, &CAllSystemView::OnBnClickedBtnStatus)
	ON_BN_CLICKED(IDC_BTN_OPERATION_MODE, &CAllSystemView::OnBnClickedBtnOperationMode)
	ON_BN_CLICKED(IDC_BTN_OPERATION_VIEW, &CAllSystemView::OnBnClickedBtnOperationView)
	ON_BN_CLICKED(IDC_BTN_INSPECTION_PASS_TIME, &CAllSystemView::OnBnClickedBtnInspectionPassTime)	
	ON_WM_ERASEBKGND()	
	ON_BN_CLICKED(IDC_BTN_TYPE_SETTING_LOCAL, &CAllSystemView::OnBnClickedBtnTypeSettingLocal)
	ON_BN_CLICKED(IDC_BTN_LOG_NEXT, &CAllSystemView::OnBnClickedBtnLogNext)
	ON_BN_CLICKED(IDC_BTN_LOG_PREV, &CAllSystemView::OnBnClickedBtnLogPrev)
	ON_BN_CLICKED(IDC_BTN_LOG_DELETE, &CAllSystemView::OnBnClickedBtnLogDelete)
	ON_BN_CLICKED(IDC_BTN_ERROR_HISTORY, &CAllSystemView::OnBnClickedBtnErrorHistory)
	ON_COMMAND(ID_AUTO_MODE, &CAllSystemView::OnAutoMode)
	ON_COMMAND(ID_CALI_MODE, &CAllSystemView::OnCaliMode)
	ON_UPDATE_COMMAND_UI(ID_AUTO_MODE, &CAllSystemView::OnUpdateAutoMode)
	ON_BN_CLICKED(IDC_BTN_CHARGER_ALARMOFF, &CAllSystemView::OnBnClickedBtnChargerAlarmoff)	
	ON_COMMAND(ID_FANTYPE_ALL, &CAllSystemView::OnFantypeAll)
	ON_COMMAND(ID_FANTYPE_MIDDLE, &CAllSystemView::OnFantypeMiddle)
	ON_COMMAND(ID_FANTYPE_SIDE, &CAllSystemView::OnFantypeSide)
	ON_COMMAND(ID_TRAYTYPE_MODE_0, &CAllSystemView::OnTraytypeMode0)
	ON_COMMAND(ID_TRAYTYPE_MODE_1, &CAllSystemView::OnTraytypeMode1)
	ON_UPDATE_COMMAND_UI(ID_TRAYTYPE_MODE_0, &CAllSystemView::OnUpdateTraytypeMode0)
	ON_UPDATE_COMMAND_UI(ID_TRAYTYPE_MODE_1, &CAllSystemView::OnUpdateTraytypeMode1)
	ON_BN_CLICKED(IDC_BTN_TEMPERATURE, &CAllSystemView::OnBnClickedBtnTemperature)
	ON_COMMAND(ID_SENSOR_MAP_VIEW, &CAllSystemView::OnSensorMapView)
	ON_BN_CLICKED(IDC_BUTTON1, &CAllSystemView::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON9, &CAllSystemView::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON5, &CAllSystemView::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON10, &CAllSystemView::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON8, &CAllSystemView::OnBnClickedButton8)
// 20201225 KSCHOI Open TemperatureData Folder When TemperatureButton DoubleClicked START
	ON_BN_DOUBLECLICKED(IDC_BTN_TEMPERATURE, &CAllSystemView::OnBnDoubleclickedBtnTemperature)
// 20201225 KSCHOI Open TemperatureData Folder When TemperatureButton DoubleClicked END
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
// CAllSystemView diagnostics
#ifdef _DEBUG
void CAllSystemView::AssertValid() const
{
	CFormView::AssertValid();
}

void CAllSystemView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSMonDoc* CAllSystemView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CAllSystemView message handlers
/**
@author 
@brief  
@bug    
@code  
*/
void CAllSystemView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class

	m_bRackIndexUp = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "RackIndexUp", TRUE);	

	InitColorBtn();
	
	InitLayoutGrid();	
	
	InitLabel();

	m_nCurModuleID = EPGetModuleID(0);			//Get Current Module ID

	UpdateModuleStateCount();
	
	Fun_PingChk();

	//////////////////////////////////////////////////////////////////////////
	//Layout Button state
/*	if(pDoc->m_nProjectType != LG_CHEMICAL && pDoc->m_nProjectType != LGC_PB5)
	{
		GetDlgItem(IDC_STATIC_LAYOUT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DISP_TYPE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_BAR1)->ShowWindow(SW_HIDE);
	}
*/
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	//////////////////////////////////////////////////////////////////////////
	
	
	// 1. EmgLog Setting	
	UpdateEmgLog();	

	SetTimer(TIMER_MAIN, 2000, NULL);

	SetTimer(TIMER_AUTOCALI, 500, NULL);
		
	int nConditionChk = 0;
	nConditionChk = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UseJigTargetTemp", 0);
	if( nConditionChk == 1 )
	{
		SetTimer( TIMERID_START_JIGTEMP_TARGET_DATA, TIMERINTERVAL_START_JIGTEMP_TARGET_DATA, NULL );
	}

	nConditionChk = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UseChSafety", 0);
	if( nConditionChk == 1 )
	{
		SetTimer( TIMERID_SAFECHK_CHVOLTAGE, TIMERINTERVAL_SAFECHK_CHVOLTAGE, NULL );
	}
}

void CAllSystemView::Fun_PingChk()
{
	// sk 네트워크 ping 확인	
	CString strAddress = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "FmsPingTestIP", "192.168.102.230");
	m_pingFms.StartPing( 20, strAddress, this->GetSafeHwnd(), 1 );

	/*
	strAddress = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "MonIP", "192.168.102.168");
	m_pingPneMonitoringsystme.StartPing( 30, strAddress, this->GetSafeHwnd(), 2 );
	*/
}

LRESULT CAllSystemView::OnRecvPingStatus(WPARAM wParam, LPARAM lParam)
{	
	UINT dwID = (UINT)wParam;

	char* pMsg = (char*)lParam;
	CString strRecv(pMsg);

	TRACE(strRecv);

// 	if( strRecv.)
/*
	// 1:FMS, 2:PNE
	if( dwID == 2 )
	{
		m_pingPneMonitoringsystme.StopPing();		
	}
	else
	{
		
	}
*/
	m_pingFms.StopPing();	

	/*
	CString strAddress = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "FmsPingTestIP", "192.168.102.230");
	m_pingFms.StartPing( 20, strAddress, this->GetSafeHwnd(), 1 );
	*/

	return 0;
}

void CAllSystemView::InitLabel()
{
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();	
	
	CString strTemp = _T("");
	
	m_nTotalUnitNum = pDoc->LoadInstalledModuleNum();

	strTemp.Format("%d %s", pDoc->GetInstalledModuleNum(), pDoc->m_strModuleName);
	m_ctrlTotalModule.SetText(strTemp);
	m_ctrlTotalModule.SetBkColor(RGB(255, 255, 220));
	//	m_ctrlTotalModule.SetTextColor(RGB(0, 0, 255));
	m_ctrlTotalModule.SetFontBold(TRUE);

	//	strTemp.Format("%d %s", m_nRunModuleNo, pDoc->m_strModuleName);
	//	m_ctrlRunModule.SetText(strTemp);
	m_ctrlRunModule.SetTextColor(RGB(0, 0, 255));
	m_ctrlRunModule.SetFontBold(TRUE);
	m_ctrlRunModule.SetBkColor(RGB(255, 255, 220));

	//	strTemp.Format("%d %s", m_nIdleModuleNo, pDoc->m_strModuleName);
	//	m_ctrlIdleModule.SetText(strTemp);
	m_ctrlIdleModule.SetTextColor(RGB(0, 200, 0));
	m_ctrlIdleModule.SetFontBold(TRUE);
	m_ctrlIdleModule.SetBkColor(RGB(255, 255, 220));

	//	strTemp.Format("%d %s", m_nTotalModuleNo, pDoc->m_strModuleName);
	//	m_ctrlLineOffModule.SetText(strTemp);
	m_ctrlLineOffModule.SetFontBold(TRUE);
	m_ctrlLineOffModule.SetTextColor(RGB(255, 0, 0));
	m_ctrlLineOffModule.SetBkColor(RGB(255, 255, 220));

	m_LabelSelectName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelSelectName.SetTextColor(RGB_WHITE);
	m_LabelSelectName.SetFontSize(24);
	m_LabelSelectName.SetFontBold(TRUE);
	m_LabelSelectName.SetText(TEXT_LANG[29]);//"Stage Status"	
	
	m_LabelDate.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelDate.SetTextColor(RGB_WHITE);
	m_LabelDate.SetFontSize(20);
	m_LabelDate.SetFontBold(TRUE);	

	m_LabelLogNum.SetBkColor(RGB_LTGRAY);
	m_LabelLogNum.SetTextColor(RGB_RED);
	m_LabelLogNum.SetFontSize(36);
	m_LabelLogNum.SetFontBold(TRUE);
	m_LabelLogNum.SetText("-");	

	m_LabelLog.SetBkColor(RGB_LTGRAY);
	m_LabelLog.SetTextColor(RGB_BLACK);
	m_LabelLog.SetFontSize(24);
	m_LabelLog.SetFontBold(TRUE);
	m_LabelLog.SetFontAlign(DT_LEFT);
	m_LabelLog.SetText("");		

	m_LabelLogTime.SetBkColor(RGB_LTGRAY);
	m_LabelLogTime.SetTextColor(RGB_BLACK);
	m_LabelLogTime.SetFontSize(24);
	m_LabelLogTime.SetFontBold(TRUE);
	m_LabelLogTime.SetText("-");
		
	m_LabelFmsConnectState.SetBkColor(RGB_RED)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[30]); //"FMS OFF"

	m_LabelTraytype_ims.SetBkColor(RGB_GREEN)
		.SetTextColor(RGB_MIDNIGHTBLUE)
		.SetFontSize(20)
		.SetFontBold(TRUE);

	m_LabelTraytype_ims.SetText(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Traytype_ims", "E370"));
		
	// 2. ImageList 표시
	
	m_pImagelist = new CImageList;
	ASSERT(m_pImagelist);
	m_pImagelist->Create(IDB_STATE_ICON1, 20, 18, RGB(255,255,255));
	m_ctrlStateSelCombo.SetImageList(m_pImagelist);
	m_ctrlStateSelCombo.AddString(TEXT_LANG[61]);
	m_ctrlStateSelCombo.SetItemData(0, EP_STATE_ERROR);
	//	m_ctrlStateSelCombo.SetItemIcon(0, pDoc->GetStateImgIndex(EP_STATE_ERROR));
	m_ctrlStateSelCombo.AddString(TEXT_LANG[62]);
	m_ctrlStateSelCombo.SetItemData(1, EP_STATE_LINE_OFF);
	m_ctrlStateSelCombo.SetItemIcon(1, pDoc->GetStateImgIndex(EP_STATE_LINE_OFF));
	m_ctrlStateSelCombo.AddString(TEXT_LANG[63]);
	m_ctrlStateSelCombo.SetItemData(2, EP_STATE_IDLE);
	m_ctrlStateSelCombo.SetItemIcon(2, pDoc->GetStateImgIndex(EP_STATE_IDLE));
	m_ctrlStateSelCombo.AddString(TEXT_LANG[64]);
	m_ctrlStateSelCombo.SetItemData(3, EP_STATE_STANDBY);
	m_ctrlStateSelCombo.SetItemIcon(3, pDoc->GetStateImgIndex(EP_STATE_STANDBY));
	m_ctrlStateSelCombo.AddString(TEXT_LANG[65]);
	m_ctrlStateSelCombo.SetItemData(4, EP_STATE_RUN);
	m_ctrlStateSelCombo.SetItemIcon(4, pDoc->GetStateImgIndex(EP_STATE_RUN));
	m_ctrlStateSelCombo.AddString(TEXT_LANG[66]);
	m_ctrlStateSelCombo.SetItemData(5, EP_STATE_PAUSE);
	m_ctrlStateSelCombo.SetItemIcon(5, pDoc->GetStateImgIndex(EP_STATE_PAUSE));
	m_ctrlStateSelCombo.AddString(TEXT_LANG[67]);
	m_ctrlStateSelCombo.SetItemData(6, EP_STATE_STOP);
	m_ctrlStateSelCombo.SetItemIcon(6, pDoc->GetStateImgIndex(EP_STATE_STOP));
	m_ctrlStateSelCombo.SetCurSel(0);

}

void CAllSystemView::InitSystemGrid()
{
	m_wndAllSystemGrid.SubclassDlgItem(IDC_ALL_SYSTEM_GRID, this);
//	m_wndAllSystemGrid.m_bRowSelection	= TRUE;
	m_wndAllSystemGrid.m_bSameRowSize = TRUE;
	// m_wndAllSystemGrid.m_bSameColSize = TRUE;
	// m_wndAllSystemGrid.m_bCustomWidth = TRUE;

	m_wndAllSystemGrid.Initialize();
	m_wndAllSystemGrid.EnableCellTips();

	BOOL bLock = m_wndAllSystemGrid.LockUpdate();
	
	m_wndAllSystemGrid.SetDefaultRowHeight(24);
//	m_wndAllSystemGrid.SetRowCount(1);
	m_wndAllSystemGrid.SetColCount(ALL_SYSTEM_GRID_COL);
	m_wndAllSystemGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertical Scroll Bar

//	m_wndAllSystemGrid.m_bCustomColor 	= TRUE;
	m_wndAllSystemGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Use MemDc
	m_wndAllSystemGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_MODULE_ID), "ID");
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_MODULE_NAME), GetDocument()->m_strModuleName);
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_GROUP_NAME), GetDocument()->m_strGroupName);
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_STATE), TEXT_LANG[68]);
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_TRAY_STATE), TEXT_LANG[69]);
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_TRAY_NAME), TEXT_LANG[70]);

	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_DOOR_STATE), TEXT_LANG[71]);
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_JIG_STATE), TEXT_LANG[72]);
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_PROC_NAME), TEXT_LANG[73]);
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_STEP_NO), "Step No");
	m_wndAllSystemGrid.SetValueRange(CGXRange(0, _GRID_COL_TEMP), "Temper.(℃)");

	m_wndAllSystemGrid.SetColWidth(0 ,0, 50);
	
	m_wndAllSystemGrid.SetStyleRange(CGXRange().SetCols(_GRID_COL_TEMP), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_PROGRESS)
								.SetValue(0L)
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%d ℃"))		// sprintf formatting for value
								.SetTextColor(RGB(255,128,64))									// blue progress bar
								.SetInterior(RGB(255, 255, 255))									// on white background
						  );
//	m_wndAllSystemGrid.SetStyleRange(CGXRange().SetCols(1),	CGXStyle()//.SetTextColor(RGB(0,0,255))
																//	.SetFont(CGXFont().SetSize(13))
//																	.SetVerticalAlignment(DT_VCENTER)
//																	.SetHorizontalAlignment(DT_CENTER)
//									);
	m_wndAllSystemGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[76]).SetBold(FALSE)));
//	m_wndAllSystemGrid.SetFrozenCols(1, 0);
//	m_wndAllSystemGrid.m_bSelectRange = TRUE;
//	m_wndAllSystemGrid.m_SelectRange.SetCols(2);
	m_wndAllSystemGrid.EnableGridToolTips();
    m_wndAllSystemGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


//	m_wndAllSystemGrid.SetStyleRange(CGXRange().SetCols(2, 7), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));

//	DrawGroupGrid();

//	m_wndAllSystemGrid.RegisterControl(IDS_CTRL_BITMAP, new CGXBitmapButton(&m_wndAllSystemGrid, IDB_MODULE_IMG));

	m_wndAllSystemGrid.LockUpdate(bLock);
	m_wndAllSystemGrid.Redraw();
}

void CAllSystemView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
		
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CFormView::ShowScrollBar(SB_HORZ,FALSE);
		CFormView::ShowScrollBar(SB_VERT,FALSE);

		CRect rect, rect2, ctrlRect;
		GetClientRect(rect);

		if( GetDlgItem(ID_DLG_LABEL_SELECT_NAME)->GetSafeHwnd() )
		{			
			GetDlgItem(ID_DLG_LABEL_SELECT_NAME)->GetWindowRect(ctrlRect);
			GetDlgItem(ID_DLG_LABEL_DATE)->GetWindowRect(rect2);
			
			ScreenToClient(ctrlRect);
			ScreenToClient(rect2);
			
			GetDlgItem(ID_DLG_LABEL_SELECT_NAME)->MoveWindow(ctrlRect.left, ctrlRect.top, rect.right - ctrlRect.left - rect2.Width(), ctrlRect.Height());
			GetDlgItem(ID_DLG_LABEL_DATE)->MoveWindow(rect.right - rect2.Width(), rect2.top, rect2.Width(), rect2.Height());
		}
		
		if(m_wndLayoutGrid.GetSafeHwnd())
		{
			m_wndLayoutGrid.GetWindowRect(ctrlRect);	//Step Grid Window Position
			ScreenToClient(ctrlRect);

			if( ctrlRect.top < 0 ||  rect.bottom - ctrlRect.top -10 < 0)
			{
				return;
			}
						
			m_wndLayoutGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.Width()-200, rect.bottom - ctrlRect.top - 70, FALSE);			
			
			m_wndLayoutGrid.GetClientRect(&ctrlRect);
			int nTotCol = m_wndLayoutGrid.GetColCount()/2;			
			float width = (float)(ctrlRect.Width())/nTotCol;				
			int height = (int)(ctrlRect.Height()-80)/(m_wndLayoutGrid.GetRowCount());

			if(  nTotCol <= 10 && nTotCol > 0 )
			{	
				for(int c = 0; c<nTotCol&&c<32; c++)
				{	
					m_wndLayoutGrid.m_nWidth[c*2+1]	= int(width* 0.3f);					
					m_wndLayoutGrid.m_nWidth[(c+1)*2] = int(width* 0.7f);
				}
				m_wndLayoutGrid.SetDefaultRowHeight( height );
				m_wndLayoutGrid.SetColWidth(0, 0, 0);
			}
			else if( nTotCol > 10 && nTotCol > 0 )
			{	
				for(int c = 0; c<nTotCol&&c<32; c++)
				{
					m_wndLayoutGrid.m_nWidth[c*2+1]	= int(width * 0.3f);					
					m_wndLayoutGrid.m_nWidth[(c+1)*2] = int(width* 0.7f);									
				}
				m_wndLayoutGrid.SetRowHeight(0,0,50);
				m_wndLayoutGrid.SetDefaultRowHeight( height );
 			}
 			m_wndLayoutGrid.Redraw();


			//버튼 크기 변경 ksh
			CRect rcDesktop, rct1, rct2;
			int width1, btnWidth;

			this->GetWindowRect(rcDesktop); 
			width1 = rcDesktop.right - rcDesktop.left;
// 20200612 KSCHOI DISABLE TEMPERATURE BUTTON START
			btnWidth = (width1 - 10) / 8;
//			btnWidth = (width1 - 10) / 7;
// 20200612 KSCHOI DISABLE TEMPERATURE BUTTON END

			GetDlgItem(IDC_BTN_STATUS)->GetWindowRect(rct1);
			ScreenToClient(rct1);

			GetDlgItem(IDC_BTN_STATUS)->MoveWindow(10, rct1.top, btnWidth, rct1.Height());
			GetDlgItem(IDC_BTN_OPERATION_MODE)->MoveWindow(btnWidth + 10, rct1.top, btnWidth, rct1.Height());
			GetDlgItem(IDC_BTN_INSPECTION_PASS_TIME)->MoveWindow(btnWidth * 2 + 10, rct1.top, btnWidth, rct1.Height());
			GetDlgItem(IDC_BTN_OPERATION_VIEW)->MoveWindow(btnWidth * 3 + 10, rct1.top, btnWidth, rct1.Height());
			GetDlgItem(IDC_BTN_TYPE_SETTING_LOCAL)->MoveWindow(btnWidth * 4 + 10, rct1.top, btnWidth, rct1.Height());
// 20200612 KSCHOI DISABLE TEMPERATURE BUTTON START
 			GetDlgItem(IDC_BTN_TEMPERATURE)->MoveWindow(btnWidth * 5 + 10, rct1.top, btnWidth, rct1.Height());
 			GetDlgItem(IDC_BTN_CHARGER_ALARMOFF)->MoveWindow(btnWidth * 6 + 10, rct1.top, btnWidth, rct1.Height());
 			GetDlgItem(IDC_BTN_ERROR_HISTORY)->MoveWindow(btnWidth * 7 + 10, rct1.top, btnWidth, rct1.Height());			
//			GetDlgItem(IDC_BTN_CHARGER_ALARMOFF)->MoveWindow(btnWidth * 5 + 10, rct1.top, btnWidth, rct1.Height());			
//			GetDlgItem(IDC_BTN_ERROR_HISTORY)->MoveWindow(btnWidth * 6 + 10, rct1.top, btnWidth, rct1.Height());
// 20200612 KSCHOI DISABLE TEMPERATURE BUTTON END

			if(g_nLanguage == 3) //KSJ 20190604 - 헝가리어가 너무길어서
			{
				width1 -=700;
			}

			m_Btn_Status.SetFontStyle(25 * width1 / 1916,1);
			m_Btn_Status.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
			m_Btn_OperationMode.SetFontStyle(25 * width1 / 1916, 1);
			m_Btn_OperationMode.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);		
			m_Btn_InspectionPassTime.SetFontStyle(25 * width1 / 1916, 1);
			m_Btn_InspectionPassTime.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
			m_Btn_OperationView.SetFontStyle(25 * width1 / 1916, 1);
			m_Btn_OperationView.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);		
			m_Btn_TypeSetting.SetFontStyle(25 * width1 / 1916, 1);
			m_Btn_TypeSetting.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
			m_BtnErrorHistory.SetFontStyle(25 * width1 / 1916, 1);
			m_BtnErrorHistory.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
			m_BtnAlarmOff.SetFontStyle(25 * width1 / 1916, 1);
			m_BtnAlarmOff.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
			m_BtnTemperature.SetFontStyle(25 * width1 / 1916, 1);
			m_BtnTemperature.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
			

			// 1. 로그 관련 컨트롤러 위치 조절

			//btnWidth = width1 / 20;

			//GetDlgItem(IDC_LABEL_LOG_NUM)->GetWindowRect(rct1);
			//ScreenToClient(rct1);

			//GetDlgItem(IDC_LABEL_LOG_NUM)->MoveWindow(0, rect.bottom-rct1.Height(), btnWidth, rct1.Height());
			//GetDlgItem(IDC_LABEL_LOG)->MoveWindow(btnWidth, rect.bottom-rct1.Height(), btnWidth * 16, rct1.Height());
			//GetDlgItem(IDC_LABEL_LOG_TIME)->MoveWindow(btnWidth * 17, rect.bottom-rct1.Height(), btnWidth, rct1.Height());
			//GetDlgItem(IDC_BTN_LOG_PREV)->MoveWindow(btnWidth * 18, rect.bottom-rct1.Height(), btnWidth / 2, rct1.Height());
			//GetDlgItem(IDC_BTN_LOG_NEXT)->MoveWindow(btnWidth * 18.5, rect.bottom-rct1.Height(), btnWidth / 2, rct1.Height());
			//GetDlgItem(IDC_BTN_LOG_DELETE)->MoveWindow(btnWidth * 19, rect.bottom-rct1.Height(), rect.Width() - btnWidth * 19, rct1.Height());

			//m_BtnLogPrev.SetFontStyle(40 * width1 / 1916, 1);
			//m_BtnLogPrev.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
			//m_BtnLogNext.SetFontStyle(40 * width1 / 1916, 1);
			//m_BtnLogNext.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
			//m_BtnLogDelete.SetFontStyle(40 * width1 / 1916, 1);
			//m_BtnLogDelete.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

			//if( GetDlgItem(IDC_LABEL_LOG_NUM)->GetSafeHwnd() )
			//{		
			//	GetDlgItem(IDC_LABEL_LOG_NUM)->GetWindowRect(ctrlRect);
			//	ScreenToClient(ctrlRect);
			//	GetDlgItem(IDC_LABEL_LOG_NUM)->MoveWindow(ctrlRect.left, rect.bottom-ctrlRect.Height(), ctrlRect.Width(), ctrlRect.Height(), FALSE);
			//}


			if( GetDlgItem(IDC_LABEL_LOG)->GetSafeHwnd() )
			{		
				CRect rect_LOG_NUM, rect_LOG, rect_LOG_TIME, rect_LOG_PREV, rect_LOG_NEXT, rect_LOG_DELETE;
				int Log_Width;

				GetDlgItem(IDC_LABEL_LOG_NUM)->GetWindowRect(rect_LOG_NUM);
				GetDlgItem(IDC_LABEL_LOG)->GetWindowRect(rect_LOG);
				GetDlgItem(IDC_LABEL_LOG_TIME)->GetWindowRect(rect_LOG_TIME);
				GetDlgItem(IDC_BTN_LOG_PREV)->GetWindowRect(rect_LOG_PREV);
				GetDlgItem(IDC_BTN_LOG_NEXT)->GetWindowRect(rect_LOG_NEXT);
				GetDlgItem(IDC_BTN_LOG_DELETE)->GetWindowRect(rect_LOG_DELETE);

				ScreenToClient(rect_LOG_NUM);
				ScreenToClient(rect_LOG);
				ScreenToClient(rect_LOG_TIME);
				ScreenToClient(rect_LOG_PREV);
				ScreenToClient(rect_LOG_NEXT);
				ScreenToClient(rect_LOG_DELETE);


				GetDlgItem(IDC_LABEL_LOG_NUM)->MoveWindow(0, rect.bottom-rect_LOG.Height(), rect_LOG_NUM.Width(), rect_LOG.Height(), FALSE);

				Log_Width = rect.right - (rect_LOG.left + rect_LOG_TIME.Width() + rect_LOG_PREV.Width() + rect_LOG_NEXT.Width() + rect_LOG_DELETE.Width());
				GetDlgItem(IDC_LABEL_LOG)->MoveWindow(rect_LOG.left, rect.bottom-rect_LOG.Height(), Log_Width, rect_LOG.Height(), FALSE);

				GetDlgItem(IDC_LABEL_LOG_TIME)->MoveWindow(rect_LOG.right, rect.bottom-rect_LOG.Height(), rect_LOG_TIME.Width(), rect_LOG_TIME.Height(), FALSE);
	
				GetDlgItem(IDC_BTN_LOG_PREV)->MoveWindow(rect_LOG_TIME.right, rect.bottom-rect_LOG.Height(), rect_LOG_PREV.Width(), rect_LOG_PREV.Height(), FALSE);
		
				GetDlgItem(IDC_BTN_LOG_NEXT)->MoveWindow(rect_LOG_PREV.right, rect.bottom-rect_LOG.Height(), rect_LOG_NEXT.Width(), rect_LOG_NEXT.Height(), FALSE);
	
				GetDlgItem(IDC_BTN_LOG_DELETE)->MoveWindow(rect_LOG_NEXT.right, rect.bottom-rect_LOG.Height(), rect_LOG_DELETE.Width(), rect_LOG_DELETE.Height(), FALSE);
			}
			
/*
			if( GetDlgItem(IDC_LABEL_LOG)->GetSafeHwnd() )
			{		
				GetDlgItem(IDC_LABEL_LOG)->GetWindowRect(ctrlRect);
				ScreenToClient(ctrlRect);
				GetDlgItem(IDC_LABEL_LOG)->MoveWindow(ctrlRect.left, rect.bottom-ctrlRect.Height(), ctrlRect.Width(), ctrlRect.Height(), FALSE);
			}

			if( GetDlgItem(IDC_LABEL_LOG_TIME)->GetSafeHwnd() )
			{		
				GetDlgItem(IDC_LABEL_LOG_TIME)->GetWindowRect(ctrlRect);
				ScreenToClient(ctrlRect);
				GetDlgItem(IDC_LABEL_LOG_TIME)->MoveWindow(ctrlRect.left, rect.bottom-ctrlRect.Height(), ctrlRect.Width(), ctrlRect.Height(), FALSE);
			}

			if( GetDlgItem(IDC_BTN_LOG_PREV)->GetSafeHwnd() )
			{		
				GetDlgItem(IDC_BTN_LOG_PREV)->GetWindowRect(ctrlRect);
				ScreenToClient(ctrlRect);
				GetDlgItem(IDC_BTN_LOG_PREV)->MoveWindow(ctrlRect.left, rect.bottom-ctrlRect.Height(), ctrlRect.Width(), ctrlRect.Height(), FALSE);
			}

			if( GetDlgItem(IDC_BTN_LOG_NEXT)->GetSafeHwnd() )
			{		
				GetDlgItem(IDC_BTN_LOG_NEXT)->GetWindowRect(ctrlRect);
				ScreenToClient(ctrlRect);
				GetDlgItem(IDC_BTN_LOG_NEXT)->MoveWindow(ctrlRect.left, rect.bottom-ctrlRect.Height(), ctrlRect.Width(), ctrlRect.Height(), FALSE);
			}

			if( GetDlgItem(IDC_BTN_LOG_DELETE)->GetSafeHwnd() )
			{		
				GetDlgItem(IDC_BTN_LOG_DELETE)->GetWindowRect(ctrlRect);
				ScreenToClient(ctrlRect);
				GetDlgItem(IDC_BTN_LOG_DELETE)->MoveWindow(ctrlRect.left, rect.bottom-ctrlRect.Height(), ctrlRect.Width(), ctrlRect.Height(), FALSE);
			}*/

		}
	}

}

void CAllSystemView::UpdateEmgLog(int nType)
{
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	
	CString strTemp = _T(""), strData = _T("");	 + 1;
	
	switch( nType )
	{
		case LOG_SELECT_TYPE_ALL:
		{
			pDoc->m_nEmgMaxCnt = pDoc->LoadEmgFromProductsMDB();
			pDoc->m_nEmgLogIndex = 0;
		}
		break;
		
		case LOG_SELECT_TYPE_DELETE:
		{
			if( pDoc->m_EmgList[pDoc->m_nEmgLogIndex].bUse == false )
			{
				return;			
			}			
			
			pDoc->m_nEmgMaxCnt = pDoc->DeleteEmgFromProductsMDB( pDoc->m_nEmgLogIndex );
			pDoc->m_nEmgLogIndex = 0;
		}
		break;
		
		case LOG_SELECT_TYPE_NEXT:
		{
			if( pDoc->m_nEmgLogIndex <= 0 )
			{
				return;		
			}
			
			pDoc->m_nEmgLogIndex--;
		}
		break;
		
		case LOG_SELECT_TYPE_PREV:
		{
			if( pDoc->m_nEmgLogIndex >= pDoc->m_nEmgMaxCnt )
			{
				return;		
			}

			pDoc->m_nEmgLogIndex++;		
		}
		break;	
	}
	
	if( pDoc->m_EmgList[pDoc->m_nEmgLogIndex].bUse == true )
	{
		strData.Format("%d", pDoc->m_nEmgMaxCnt - pDoc->m_nEmgLogIndex + 1);
		m_LabelLogNum.SetText(strData);
		
		strTemp.Format("[ %s ]", pDoc->m_EmgList[pDoc->m_nEmgLogIndex].szStage);
		strData = strTemp;

		if( pDoc->m_EmgList[pDoc->m_nEmgLogIndex].nOperationMode == EP_OPERATION_AUTO )
		{
			strTemp.Format("[ Auto ]");			
		}
		else if( pDoc->m_EmgList[pDoc->m_nEmgLogIndex].nOperationMode == EP_OPERATION_SAFETYLOCAL )
		{
			strTemp.Format("[ Safety Lo. ]");			
		}
		else if( pDoc->m_EmgList[pDoc->m_nEmgLogIndex].nOperationMode == EP_OPEARTION_MAINTENANCE )
		{
			strTemp.Format("[ Maint. ]");
		}
		else
		{
			strTemp.Format("[ Local ]");
		}

		strData += strTemp;		

		strTemp.Format(" :: %s(%s)", pDoc->m_EmgList[pDoc->m_nEmgLogIndex].szMessage, pDoc->m_EmgList[pDoc->m_nEmgLogIndex].szCode);
		strData += strTemp;	

		m_LabelLog.SetText(strData);

		strData.Format("%s", pDoc->m_EmgList[pDoc->m_nEmgLogIndex].curTime);
		m_LabelLogTime.SetText(strData);	
	}
	else
	{
		m_LabelLogNum.SetText("-");
		m_LabelLog.SetText("-");
		m_LabelLogTime.SetText("-");
	}
}

void CAllSystemView::DrawGrid()
{
	char	szTmp[20];
//	CCTSMonDoc *pDoc= GetDocument();

//	int nModule = EPGetInstalledModuleNum();	
//	int nTotalGroup = EPInstledlTotalGroup();

//	m_wndAllSystemGrid.SetRowCount(nTotalGroup);DrawGrid

//	m_wndAllSystemGrid.SetColCount(7);

//	m_wndAllSystemGrid.m_CustomColorRange = CGXRange(1, 2, nTotGroup, 2);	//Only Board state Cell is Custom color

//	if(m_pModuleColorFlag != NULL)
//	{
//		delete[] m_pModuleColorFlag;
//		m_pModuleColorFlag = NULL;
//	}

//	m_pModuleColorFlag = new char[nTotGroup];
//	ASSERT(m_pModuleColorFlag);
	
//	memset(m_pModuleColorFlag, 15, nTotGroup);				//Set Default color
//	m_wndAllSystemGrid.m_pCustomColorFlag = (char *)m_pModuleColorFlag;
	
	BOOL bLock = m_wndAllSystemGrid.LockUpdate();

	int nModuleID, nCount, nTotal = 0, nTemp;
	int nWidth = 1;
	COLORREF color = RGB(0, 0, 200);

	for(int i = 0; i< EPGetInstalledModuleNum(); i++)
	{
		nModuleID = EPGetModuleID(i);
		nCount = EPGetGroupCount(nModuleID);
		nTemp = nTotal+nCount;
		m_wndAllSystemGrid.InsertRows(nTotal+1, nCount);
		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, 1, nTotal+1, ALL_SYSTEM_GRID_COL), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(nWidth).SetColor(color)));
		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTemp, 1, nTemp, ALL_SYSTEM_GRID_COL), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(nWidth).SetColor(color)));
		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, 1, nTemp, 1), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(nWidth).SetColor(color)));
		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, ALL_SYSTEM_GRID_COL, nTemp, ALL_SYSTEM_GRID_COL), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(nWidth).SetColor(color)));
		m_wndAllSystemGrid.SetCoveredCellsRowCol(nTotal+1, 1, nTemp, 1);

/*		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, 3),
          CGXStyle()
    //         .SetControl(GX_IDS_CTRL_STATIC)
             .SetValue("#BMP(\"IDB_M_CLOSE\")")
        );
*/

		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, _GRID_COL_STATE),
          CGXStyle()
    //         .SetControl(GX_IDS_CTRL_STATIC)
             .SetValue(TEXT_LANG[0]) //TEXT - 연결끊김

        );

		
/*		sprintf(szTmp, "%s",  GetDocument()->ModuleName(nModuleID));
		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, 1), 
			CGXStyle()
				.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, szTmp)
		);

*/		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, _GRID_COL_MODULE_ID),
          CGXStyle()
    //         .SetControl(GX_IDS_CTRL_STATIC)
             .SetValue((long)nModuleID)
        );
		
		  m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, _GRID_COL_MODULE_NAME),
          CGXStyle()
    //         .SetControl(GX_IDS_CTRL_STATIC)
				.SetValue(::GetModuleName(nModuleID))
        );
		
		
		for(int j = 1; j<= nCount; j++)
		{
//			sprintf(szTmp, "%d", nModuleID);
//			m_wndAllSystemGrid.SetStyleRange(CGXRange(i*nCount+j, 1), szTmp);
			sprintf(szTmp, "Group %d", j);
			m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+j, _GRID_COL_GROUP_NAME), 
							CGXStyle().SetValue(szTmp)
									  .SetTextColor(RGB(255,255,255))
								      .SetInterior(RGB(128,128,128))
			);
		}
		nTotal = nTemp;
	}

/*	m_wndAllSystemGrid.SetStyleRange(CGXRange(1, 1, 4, 1), CGXStyle()
				.SetControl(IDS_CTRL_BITMAP)
				.SetVerticalAlignment(DT_VCENTER)
				.SetHorizontalAlignment(DT_CENTER)
			);
*/
      
	m_wndAllSystemGrid.LockUpdate(bLock);
	m_wndAllSystemGrid.Redraw();
}

void CAllSystemView::DrawModuleGrid()
{
	BOOL bLock = m_wndAllSystemGrid.LockUpdate();
	int nTotal = 0;
	for(int i = 0; i< EPGetInstalledModuleNum(); i++)
	{
		m_wndAllSystemGrid.InsertRows(nTotal+1, 1);
		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, _GRID_COL_STATE),
          CGXStyle()
             .SetValue(TEXT_LANG[0]) //TEXT - 연결끊김
        );
		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, _GRID_COL_MODULE_ID),
          CGXStyle()
             .SetValue((long)EPGetModuleID(i))
        );

		m_wndAllSystemGrid.SetStyleRange(CGXRange(nTotal+1, _GRID_COL_MODULE_NAME),
          CGXStyle()
		  .SetValue(::GetModuleName(EPGetModuleID(i)))
        );
		
		nTotal++;
	}
	m_wndAllSystemGrid.LockUpdate(bLock);
	m_wndAllSystemGrid.Redraw();
}

void CAllSystemView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	DWORD gpState;
	CFormModule *pModule = NULL;
	CCTSMonDoc *pDoc = GetDocument();
	pModule = pDoc->GetModuleInfo(nModuleID);
	gpState = pModule->GetState(TRUE);
	
	UpdateModule(nModuleID);
	UpdateModuleStateCount();
}

bool CAllSystemView::UpdateModule(int nModuleID)
{
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	CCTSMonDoc *pDoc = GetDocument();
	CString strLog = _T("");
	CString strData = _T("");
	
	int nRow =0, nCol=0;
	int nInstalledModuleNum = m_nTotalUnitNum;
	int i = 1;
	int state = 0;
	
	BOOL bLock = m_wndLayoutGrid.LockUpdate();

	for(i=0; i<=nInstalledModuleNum; i++ )
	{	
		DrawModule(i, FALSE);	
	}
	
	m_wndLayoutGrid.LockUpdate(bLock);
	m_wndLayoutGrid.Redraw();
	
	return true;
}

bool CAllSystemView::DrawModule(int nModuleID, BOOL bUseGroup )
{	
	WORD	gpState;
	BYTE 	colorFlag = 0;//, runColor = 0;
	CString strMsg, strTemp, strSubTemp, strTraytype;
	CString strSqlStatus = _T("");		// 통합 모니터링으로 보내주기 Stage 상태 정보
	CString strSqlDetail = _T("");		// 통합 모니터링으로 보내주기 Stage 상세 정보
	STR_TOP_CONFIG *pConfig = NULL;
	CFormModule *pModule = NULL;
	CCTSMonDoc *pDoc = GetDocument();
	int nRow=0, nCol=0, nTotalGroupNo=0;
	BOOL bTempErrFlag = FALSE;

	int nVersionDiffFlag = 0;  //19-01-10 엄륭 SBC Version 추가 관련

	pModule = pDoc->GetModuleInfo(nModuleID);
	if( pModule == NULL )
	{
		return false;
	}

	int nOperationMode = pModule->GetOperationMode();	
	pConfig = pDoc->GetTopConfig();
	
	if( GetModuleRowCol(nModuleID, nRow, nCol) == FALSE )
	{
		return false;
	}

	gpState = pModule->GetState(TRUE);
// 	if(nModuleID == 1)//TEST
// 	{
// 		CString strTemp;
// 		strTemp	= pDoc->GetStateMsg(gpState, colorFlag);
// 		TRACE(strTemp);
// 	}

	if( nOperationMode == EP_OPERATION_AUTO || nOperationMode == EP_OPERATION_CALIBRATION)
	{
		FMS_STATE_CODE FMSState = pDoc->m_fmst.fnGetFMSStateForAllSystemView(nModuleID);
		if( FMSState == FMS_ST_RUNNING)
		{	
			// 1. 강제로 Run 상태를 변경
			strMsg	= pDoc->GetStateMsg(gpState, colorFlag);
			strSqlStatus	= pDoc->GetStateSqlMsg(gpState, colorFlag);
			colorFlag = 2;
		}
		else if( FMSState == FMS_ST_ERROR )
		{
			strMsg	= pDoc->GetAutoStateMsg(FMSState, colorFlag);
			strSqlStatus	= pDoc->GetAutoStateSqlMsg(FMSState, colorFlag);

			if( gpState == EP_STATE_PAUSE )
			{
				if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES )
				{
					strMsg = "Res";
					strSqlStatus = "Res";
				}
			}
 		}
// 		else if(FMSState == FMS_ST_CALIBRATION)
// 		{
// 			strMsg	= pDoc->GetAutoStateMsg(FMSState, colorFlag);
// 		}
		else
		{
			// strMsg	= pDoc->GetAutoStateMsg(FMSState, colorFlag);
			if( FMSState == FMS_ST_OFF )
			{
				if( gpState == EP_STATE_STANDBY )
				{
					strMsg	= TEXT_LANG[88];
					strSqlStatus = "Tray In";
				}
				else
				{
					strMsg	= pDoc->GetStateMsg(gpState, colorFlag);
					strSqlStatus = pDoc->GetStateSqlMsg(gpState, colorFlag);
				}
			}
			else
			{
				strMsg	= pDoc->GetAutoStateMsg(FMSState, colorFlag);
				strSqlStatus = pDoc->GetAutoStateSqlMsg(FMSState, colorFlag);

				if( FMSState == FMS_ST_VACANCY )
				{
					if( m_bLocalReservation[nModuleID-1] == TRUE )
					{
						m_bLocalReservation[nModuleID-1] = FALSE;

						pDoc->SetOperationMode(nModuleID, 0, EP_OPERATION_LOCAL);
// 20210201 KSCHOI Add Command Button Log START
						CString csLog;
						csLog.Format("[Stage %s] AllSystemView LocalReservation Activated. LocalMode Change Request Send Completed.", ::GetModuleName(nModuleID));
						pDoc->WriteLog(csLog);
// 20210201 KSCHOI Add Command Button Log END
					}
				}
			}
		}
		
		// 1. 통합모니터링으로 전송하는 상태 정보
		if( pDoc->m_bUsePneMonitoringSystem == true )
		{
			if( FMSState == FMS_ST_RED_READY )
			{
				strSqlStatus = "Red Ready";
			}
			else if( FMSState == FMS_ST_RED_END )
			{
				strSqlStatus = "Red End";
			}
			else if( FMSState == FMS_ST_BLUE_END )
			{
				strSqlStatus = "Blue End";						
			}
			else if( FMSState == FMS_ST_RED_TRAY_IN )
			{
				strSqlStatus = "Red TrayIn";			
			}
			else
			{			
			}
		} 
	}
	else
	{ 
		// 1. Local의 status 를 확인
		strMsg	= pDoc->GetStateMsg(gpState, colorFlag);
		strSqlStatus	= pDoc->GetStateSqlMsg(gpState, colorFlag);
		
		if( gpState != EP_STATE_LINE_OFF )
		{
			if( gpState == EP_STATE_IDLE )
			{
				if( EPTrayState(nModuleID) == EP_TRAY_NONE )
				{
					strMsg = TEXT_LANG[63];			
					strSqlStatus = "Vacancy";		
				}
				else
				{
					strMsg = TEXT_LANG[88];
					strSqlStatus = "Tray In";	
				}			
			}
			else if( gpState == EP_STATE_READY )
			{
				strMsg = TEXT_LANG[63];	
				strSqlStatus = "Vacancy";	
			}	

			/*
			if( pModule->GetTrayInfo(0)->m_nResultFileType != EP_RESULTFILE_TYPE_NORMAL
				&&  pModule->GetTrayInfo(0)->m_nResultFileType != EP_RESULTFILE_TYPE_STP )
				*/
			if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_EMG
				|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES 
				|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_CONTACT )
			{
				colorFlag = 5;
				strMsg = TEXT_LANG[89];		
				strSqlStatus = "Error";

				if( gpState == EP_STATE_PAUSE )
				{
					if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES )
					{
						strMsg = "Res";		
						strSqlStatus = "Res";
					}
				}
			}			
		}	

//		strSqlStatus = strMsg;		
	}
// 	if( nOperationMode == EP_OPERATION_CALIBRATION && g_nCalistart == 1) //KSH Calibration 2(교정창 띄우기)
// 	{
// 		if( gpState != EP_STATE_LINE_OFF )
// 		{
// 			if( gpState == EP_STATE_IDLE )
// 			{
// 				if( EPTrayState(nModuleID) == EP_TRAY_NONE )
// 				{
// 
// 				}
// 				else
// 				{
// // 					int nTestCali = 0;
// // 					nTestCali = AfxGetApp()->GetProfileInt("FormSetting", "CaliTest", 1);
// // 					if(nTestCali == 1)
// // 					{
// // 						g_nCalistart = 1;
// // 					}
// 					//m_bCaliDlgOn = true;
// 					g_nCalistart = 0;
// 					CCalibratorDlg *pCaliDlg;
// 					CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
// 
// 					if (pFrame->m_apCaliDlg.GetSize() > 0)
// 					{
// 						for (int i=0 ; i< pFrame->m_apCaliDlg.GetSize() ; i++)
// 						{
// 							pCaliDlg = (CCalibratorDlg *)pFrame->m_apCaliDlg[i];
// 							if (pCaliDlg->m_nUnitNo == nModuleID)
// 							{
// 								//AfxMessageBox(TEXT_LANG[35]);
// 							}
// 						}
// 						pCaliDlg =  new CCalibratorDlg(nModuleID, this);
// 						ASSERT(pCaliDlg);
// 
// 						pCaliDlg->m_pdoc = (CCTSMonDoc *)pFrame->GetActiveDocument();
// 						pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
// 						pCaliDlg->ShowWindow(SW_SHOW);
// 						pFrame->m_apCaliDlg.Add(pCaliDlg);
// 					}
// 					else
// 					{
// 						pCaliDlg =  new CCalibratorDlg(nModuleID, this);
// 						ASSERT(pCaliDlg);
// 
// 						pCaliDlg->m_pdoc = (CCTSMonDoc *)pFrame->GetActiveDocument();
// 						pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
// 						pCaliDlg->ShowWindow(SW_SHOW);
// 						pFrame->m_apCaliDlg.Add(pCaliDlg);
// 					}
// 					//pCaliDlg->m_bStartCmd = TRUE;//KSH Calibration 20190830
// 					pCaliDlg->AutoCali();
// 				}			
// 			}
// 		}
// 	}

	// 1. 통합모니터링으로 전송하는 상태 정보
	if( pDoc->m_bUsePneMonitoringSystem == true && mainPneApp.GetSendFlag() == false )
	{	
		strSqlDetail = pModule->GetTrayNo();	

		strTemp.Format("UPDATE tbl_status set updatetime = now(), Op_Mode = %d, state = '%s', detail = '%s' where Device = %d AND unitnum = %d",
			nOperationMode, strSqlStatus, strSqlDetail, pDoc->m_nDeviceID , nModuleID + pDoc->m_nUnitIdx);

		mainPneApp.mesData.AddTail(strTemp);
	} 		
	
	switch( m_nSelectItem )
	{
	case STATUS_VIEW:
		{	
			INDEX nChannelIndex = 0;
			INDEX nChannelNo = pModule->GetChInJig(0);
			STR_SAVE_CH_DATA ChSaveData;
			int nFailCnt = 0;

			CString strCode;

			strTemp = pModule->GetTrayNo();
			if(!strTemp.IsEmpty())
			{
				if( pModule->GetTrayNo().GetLength() == 12 )
				{
					strTemp = "DN:";
					strTemp += pModule->GetTrayNo().Left(6).Trim();
					strTemp += "\nUP:";//20191004 ksj
					strTemp += pModule->GetTrayNo().Right(6).Trim();
				}
				else
				{
					strTemp = pModule->GetTrayNo();
				}


				strMsg = strMsg+"\n"+strTemp;
				if( pModule->GetCurStepNo() != 0 )
				{
					strTemp.Format("St %d", pModule->GetCurStepNo());
					strMsg = strMsg+"\n"+strTemp;
				}

				for( nChannelIndex = 0; nChannelIndex < nChannelNo; nChannelIndex++ )
				{
					pModule->GetChannelStepData(ChSaveData, nChannelIndex);

					strCode = pDoc->ChCodeMsg(ChSaveData.channelCode);

					if( strCode == _T("0000") || strCode == _T("CF03") || strCode == _T("CF05") )
						// || strCode == _T("U0") || strCode == _T("U1") || strCode == _T("U2") )
					{

					}
					else
					{
						nFailCnt++;
					}
				}

				if( nFailCnt != 0 )
				{
					strTemp.Format("- Err : %d", nFailCnt);
					strMsg = strMsg+strTemp;
				}
			}
			else
			{
				strMsg = strMsg+"\n"+"-";
			}
		}
		break;
	case OPERATION_MODE_VIEW:
		{
			STR_SAVE_CH_DATA ChSaveData;
			WORD gpState = EPGetGroupState(nModuleID);
			if( gpState == EP_STATE_LINE_OFF )
			{
				strMsg = "-";
			}
			else
			{	
				if( nOperationMode == EP_OPERATION_AUTO )
				{
					strMsg ="Auto";

					if( m_bLocalReservation[nModuleID-1] == TRUE )
					{
						strMsg += "\n[ Local ]";
					}
				}
				else if( nOperationMode == EP_OPEARTION_MAINTENANCE )
				{
					strMsg ="Maintenance";				
				}
				else if( nOperationMode == EP_OPERATION_CALIBRATION )
				{
					strMsg ="Calibration";				
				}
				else
				{
					if( EPGetAutoProcess(nModuleID) == TRUE )
					{
						strMsg ="Safety Mode";
					}
					else
					{
						strMsg ="Local";
					}
				}
				
				EP_MD_SYSTEM_DATA *lpSysData;
				lpSysData = EPGetModuleSysData(EPGetModuleIndex(nModuleID));
				//20201216ksj
				EP_MD_FW_VER_DATA *lpfwVerData;
				lpfwVerData = EPGetModuleVersionData(EPGetModuleIndex(nModuleID));
				if(lpfwVerData != NULL )
				{
					CString strPro ="";
					CString strSubPro ="";
					CString strRev ="";
					CString strSubRev ="";
					
					for(int i=0; i<4; i++)
					{
						strPro +=lpfwVerData->cMainPro[i];
					}
					for(int i=0; i<3; i++)
					{
						strSubPro +=lpfwVerData->cSubPro[i];
					}
					for(int i=0; i<4; i++)
					{
						strRev +=lpfwVerData->cMainRev[i];
					}
					for(int i=0; i<3; i++)
					{
						strSubRev +=lpfwVerData->cSubRev[i];
					}

//					strTemp.Format(".%d",lpSysData->nVersion);
					
					strTemp.Format("Pro:%s(%s)\nRev:%s(%s)\n", strPro,strSubPro,strRev,strSubRev);
					strMsg = strMsg+"\n"+strTemp;

					switch(pModule->GetJigHeightMode())
					{
					case EP_JIG_HEIGHT_HIGH :
						strTemp = "JIG HEIGHT : HIGH";
						break;
					case EP_JIG_HEIGHT_MIDDLE :
						strTemp = "JIG HEIGHT : MID";
						break;
					case EP_JIG_HEIGHT_LOW :
						strTemp = "JIG HEIGHT : LOW";
						break;
					default :
						strMsg = "JIG HEIGHT : UNKNOWN";
					}

					strMsg += strTemp;
				}
			}
		}
		break;
	case INSPECTION_PASS_TIME_VIEW:
		{	
			INDEX nChannelIndex = 0;
			INDEX nChannelNo = pModule->GetChInJig(0);
			float fStepTime = 0.0f;
			float fTotTime = 0.0f;
			STR_SAVE_CH_DATA ChSaveData;
			WORD gpState = EPGetGroupState(nModuleID);

			if( gpState == EP_STATE_LINE_OFF || gpState == EP_STATE_IDLE || gpState == EP_STATE_READY )
			{
				strMsg = "-";
			}
			else
			{
				for( nChannelIndex = 0; nChannelIndex < nChannelNo; nChannelIndex++ )
				{
					pModule->GetChannelStepData(ChSaveData, nChannelIndex);
					if( ChSaveData.fStepTime > fStepTime )
					{
						fStepTime = ChSaveData.fStepTime;
						fTotTime = ChSaveData.fTotalTime;
						
					}		
				}
				
				strMsg.Format("T : %s\nS : %s", pDoc->ValueString(fTotTime, EP_TOT_TIME), pDoc->ValueString(fStepTime, EP_STEP_TIME) );
			}
		}
		break;
	case OPERATION_VIEW:
		{
			if( gpState == EP_STATE_LINE_OFF || gpState == EP_STATE_IDLE || gpState == EP_STATE_READY )
			{
				strMsg = "-";
			}			
			else
			{
				CTestCondition *pTestConditon = pModule->GetCondition();

				if( pTestConditon != NULL )
				{
					strTemp.Format("%s", pTestConditon->GetModelInfo()->szProcesstype);

					if( strTemp != _T("") )
					{
						strMsg.Format("T %s - P %d", strTemp, pTestConditon->GetTestInfo()->lID );
					}
					else
					{
						strMsg.Format("T %d - P %d", pTestConditon->GetModelInfo()->lID, pTestConditon->GetTestInfo()->lID );
					}

					strMsg += "\n";

					strTraytype = _T("");
					if( pModule->GetTrayType() == EP_TRAYTYPE_0 )
					{
						strTraytype.Format(TEXT_LANG[1]);//TEXT - 양방향
					}
					else if( pModule->GetTrayType() == EP_TRAYTYPE_1 )
					{
						strTraytype.Format(TEXT_LANG[2]);//TEXT - 단방향
					}

					if( !strTraytype.IsEmpty() )
					{						
						strMsg += strTraytype;
					}

					strMsg += "\n";

					if( pModule->GetFanType() == EP_FANTYPE_ALL )
					{
						strTemp.Format(TEXT_LANG[3]+"-[OOOO]"); //"팬"
					}
					else if( pModule->GetFanType() == EP_FANTYPE_MIDDLE )
					{
						strTemp.Format(TEXT_LANG[3]+"-[XOOX]"); //"팬"
					}
					else if( pModule->GetFanType() == EP_FANTYPE_SIDE )
					{
						strTemp.Format(TEXT_LANG[3]+"-[OXXO]"); //"팬"
					}

					strMsg += strTemp;
				}	
			}
		}
		break;

	case TEMP_VIEW:
		{
			if( gpState == EP_STATE_LINE_OFF  )
			{
				strMsg = "-";
			}
			else
			{
				_MAPPING_DATA	*pMapData;

				long TempMax = 0, TempMin = 10000;
				EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);
// 20201211 KSCHOI Add Average Temperature START
				long TempAvg = 0;
				int iCount = 0;
// 20201211 KSCHOI Add Average Temperature END

				for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
				{
					pMapData = pModule->GetSensorMap(0, i);	

					// 1. Jig
					if( pMapData->nChannelNo == 1 )
					{
						if(TempMax < gpData.sensorData.sensorData1[i].lData)
						{
							TempMax = gpData.sensorData.sensorData1[i].lData;
						}

						if(TempMin > gpData.sensorData.sensorData1[i].lData)
						{
							TempMin = gpData.sensorData.sensorData1[i].lData;
						}
// 20201211 KSCHOI Add Average Temperature START
						TempAvg += gpData.sensorData.sensorData1[i].lData;
						iCount++;
// 20201211 KSCHOI Add Average Temperature END
					}		
				}

				if( TempMin < 0 || TempMin > 9999 )
				{
					TempMin = 0;
				}

// 20201211 KSCHOI Add Average Temperature START
//				strTemp.Format(TEXT_LANG[28], (float)TempMax/100.0f, (float)TempMin/100.0f ); //TEXT - Max : %.1f\nMin : %.1f
// 20201223 KSCHOI Add Process State In Temperature Section START
//				strTemp.Format(TEXT_LANG[28], (float)TempMax/100.0f, (float)TempMin/100.0f, iCount?(float)TempAvg/iCount/100.0f:0.00);
				CString csModuleState;
				BYTE byteTemp;
				
				if( nOperationMode == EP_OPERATION_AUTO || nOperationMode == EP_OPERATION_CALIBRATION)
				{
					// Auto, Calibration Mode
					if( pDoc->m_fmst.fnGetFMSStateForAllSystemView(nModuleID) == FMS_ST_RUNNING)
					{
						csModuleState	= pDoc->GetStateMsg(gpState, byteTemp);
					}
					else
					{
						csModuleState = "-";
					}
				}
				else
				{ 
					// Local Mode
					switch( gpState )
					{
					case EP_STATE_CHARGE :
					case EP_STATE_DISCHARGE :
					case EP_STATE_REST :
					case EP_STATE_OCV :
					case EP_STATE_IMPEDANCE :
						csModuleState = pDoc->GetStateMsg(gpState, byteTemp);
						break;
					default :
						csModuleState = "-";
					}					
				}
				
				strTemp.Format(csModuleState + "\n" +TEXT_LANG[28], (float)TempMax/100.0f, (float)TempMin/100.0f, iCount?(float)TempAvg/iCount/100.0f:0.00);
// 20201223 KSCHOI Add Process State In Temperature Section END
// 20201211 KSCHOI Add Average Temperature END

				strMsg = strTemp;

				EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(nModuleID);				
				if(lpSysParam != NULL)	
				{
					if( lpSysParam->wJigTempErrorRefValue < TempMax )
					{
						bTempErrFlag = TRUE;						
					}
				}
			}
		}
		break;
	}

	// 1. Auto 모드와 Local 모드의 상태에 따라 색상을 다르게 표시한다.	

	EP_SYSTEM_PARAM * sysParam = EPGetSysParam(nModuleID);
	if(sysParam->wModuleType == EP_ID_EMPTY)
	{
		strMsg = "BLANK";
		strSqlStatus = "BLANK";

		m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
			.SetTextColor(RGB(60,60,60))
			.SetInterior(RGB(255,251,240)).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));	
	}
// 20201208 KSCHOI Change Background Color For Empty Jig Stage START
	else if(sysParam->wModuleType == EP_ID_JIG_NO_EXIST)
	{
		m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
			.SetTextColor(pConfig->m_TStateColor[colorFlag])
			.SetInterior(RGB(192,192,192)).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));
	}
// 20201208 KSCHOI Change Background Color For Empty Jig Stage END
	else if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
	{
		m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
			.SetTextColor(pConfig->m_TStateColor[colorFlag])
			.SetInterior(RGB(160,160,160)).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));		
	}
	else
 	{
		if( m_nSelectItem == STATUS_VIEW )
		{
			if( nOperationMode == EP_OPERATION_AUTO )
			{
				m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
					.SetTextColor(pConfig->m_TAutoStateColor[colorFlag])
					.SetInterior(pConfig->m_BAutoStateColor[colorFlag]).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));
			}
			else
			{
				m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
					.SetTextColor(pConfig->m_TStateColor[colorFlag])
					.SetInterior(pConfig->m_BStateColor[colorFlag]).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));	
			}		
		}
		else
		{
			if( m_nSelectItem == TEMP_VIEW && bTempErrFlag == TRUE )
			{
				m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
					.SetTextColor(RGB_LTGRAY)
					.SetInterior(RGB_LIGHTRED).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));
			}
			else
			{
				if( nOperationMode == EP_OPERATION_LOCAL )
				{
					if( EPGetAutoProcess(nModuleID) == TRUE )
					{
						//SAFETY LOCAL
						m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
							.SetTextColor(RGB_PUPPLE)
							.SetInterior(pConfig->m_BStateColor[colorFlag]).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));			
					}
					else
					{
						//LOCAL
						m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
							.SetTextColor(RGB_RED)
							.SetInterior(pConfig->m_BStateColor[colorFlag]).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));	
					}
						
				}
				else if( nOperationMode == EP_OPERATION_AUTO )
				{
					m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
						.SetTextColor(RGB_BLUE)
						.SetInterior(pConfig->m_BAutoStateColor[colorFlag]).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));			
				}
				else
				{
					m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
						.SetTextColor(RGB_LTGRAY)
						.SetInterior(pConfig->m_BStateColor[colorFlag]).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));			
				}

				//19-01-10 엄륭 SBC Version 추가 관련////////////////////////////////////////////
				if( m_nSelectItem == OPERATION_MODE_VIEW )
				{
					if( nVersionDiffFlag == 1 )
					{
						m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
							.SetTextColor(RGB_BLACK)
							.SetInterior(RGB(255,120,120)).SetFont(CGXFont().SetSize(GRID_STAGE_DATA_FONT_SIZE)));			
					}
				}
				////////////////////////////////////////////////////////////////////////////////////////
			}			
		}	
	}	
	return true;
}

CString CAllSystemView::ProcUnitTempData( int nModuleID )			// nIndex => ModuleID
{	
	CCTSMonDoc *pDoc = GetDocument();
	long TempMax = 0, TempMin = 10000, lJigAvg = 0, lUnitAvg = 0, TempCh = 0;	
	CString strTemp = _T("");
	CString strRtnData = _T("");
	BOOL	m_bUnitSensor = FALSE;
	BOOL	m_bJigSensor = FALSE;	
	int nJigCnt = 0, nUnitCnt = 0;	
	float fTemp = 0.0f;	
	int nSensorNo;			// 저장된 SensorNum
	CFormModule		*pModule;
	_MAPPING_DATA	*pMapData;

	pModule = pDoc->GetModuleInfo(nModuleID);
	if( pModule == NULL )
	{
		return "-";
	}
	
	EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);	
	
	for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
	{
		pMapData = pModule->GetSensorMap(0, i);	

		if( pMapData->nChannelNo >= 0 )
		{
			if(pMapData->nChannelNo == 0)		// Unit
			{
				lUnitAvg += gpData.sensorData.sensorData1[i].lData;
				nUnitCnt++;
				m_bUnitSensor = TRUE;
			}

			if(pMapData->nChannelNo == 1)		// JIG
			{
				lJigAvg += gpData.sensorData.sensorData1[i].lData;
				nJigCnt++;
			}
		}			
	}

	strTemp = _T("-");

	if(nUnitCnt > 0)
	{
		strTemp.Format("Unit : %.1f℃", (float)lUnitAvg/nUnitCnt/100.0f);					
	}

	strRtnData = strTemp;
	strTemp = _T("-");	

	if( nJigCnt > 0)
	{
		strTemp.Format("Jig : %.1f℃", (float)lJigAvg/nJigCnt/100.0f);
	}

	strRtnData += "\n" + strTemp;
	return strRtnData; 
}


BOOL CAllSystemView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
	pInfo->SetMaxPage(0xffff);

	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	// default preparation
	return DoPreparePrinting(pInfo);
	
//	return CFormView::OnPreparePrinting(pInfo);
}

void CAllSystemView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndLayoutGrid.OnGridPrepareDC(pDC, pInfo);
//	CFormView::OnPrepareDC(pDC, pInfo);
}

void CAllSystemView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	CGXData& Header	= m_wndLayoutGrid.GetParam()->GetProperties()->GetDataHeader();
	Header.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(1, 2, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(2, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(2, 2, 
			CGXStyle().SetValue("All System View").SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxOverride);
	Header.StoreStyleRowCol(3, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(3, 2, CGXStyle().SetValue("━━━━━━━━━━━━━━━━━━"), gxOverride);
	CString strTemp1, strTemp2, strTemp3, strTemp4, strMsg;
	m_ctrlTotalModule.GetWindowText(strTemp1);
	m_ctrlLineOffModule.GetWindowText(strTemp2);
	m_ctrlRunModule.GetWindowText(strTemp3);
	m_ctrlIdleModule.GetWindowText(strTemp4);
	strMsg.Format("Total: %s    Line Off: %s    Idle : %s    Run: %s",
					strTemp1, strTemp2, strTemp3, strTemp4);

	Header.StoreStyleRowCol(4, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(4, 2, CGXStyle().SetValue(strMsg), gxOverride);
	m_wndLayoutGrid.OnGridPrint(pDC, pInfo);
	//	CFormView::OnPrint(pDC, pInfo);
}

void CAllSystemView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndLayoutGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);
	m_wndLayoutGrid.OnGridBeginPrinting(pDC, pInfo);
//	CFormView::OnBeginPrinting(pDC, pInfo);
} 

void CAllSystemView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndLayoutGrid.OnGridEndPrinting(pDC, pInfo);
	//CFormView::OnEndPrinting(pDC, pInfo);
}

void CAllSystemView::ModuleDisConnected(int nModuleID)
{		
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	for(int i =0; i<= nModuleIndex; i++)
	{
		if(EPGetGroupCount(EPGetModuleID(i)) != 1)
			return;
	}

	DrawModule(nModuleID);
}

void CAllSystemView::FnSetFmsCommState(bool bConnected)
{		
	CCTSMonDoc *pDoc = (CCTSMonDoc*)GetDocument();
	m_bFmsConnectState = bConnected;
	
	if( m_bFmsConnectState )
	{	
		m_LabelFmsConnectState.SetBkColor(RGB_GREEN)
			.SetTextColor(RGB_MIDNIGHTBLUE)
			.SetFontSize(22)
			.SetFontBold(TRUE)
			.SetText(TEXT_LANG[31]); //"HSMS ON"	
		pDoc->m_fmsConnectCheck = TRUE;
	}
	else
	{	
		m_LabelFmsConnectState.SetBkColor(RGB_RED)
			.SetTextColor(RGB_WHITE)
			.SetFontSize(22)
			.SetFontBold(TRUE)
			.SetText(TEXT_LANG[32]); //"HSMS OFF"	
		pDoc->m_fmsConnectCheck = FALSE;
	}
}

void CAllSystemView::OnSelchangeStageSelCombo() 
{
	// TODO: Add your control notification handler code here
	BOOL bLock = m_wndAllSystemGrid.LockUpdate();

	int nCount;
	int nModuleID;
	if((nCount = m_wndAllSystemGrid.GetRowCount()) > 0)
	{
		m_wndAllSystemGrid.RemoveRows(1, nCount);
	}

	int sel = m_ctrlStateSelCombo.GetCurSel();
	if(sel >= 0)	
	{

		WORD state = EP_STATE_LINE_OFF;
		state = (WORD)m_ctrlStateSelCombo.GetItemData(sel);

		for(int nModule = 0; nModule < EPGetInstalledModuleNum(); nModule++)
		{
			nModuleID = EPGetModuleID(nModule);
			if(sel <= 0)	//All Module or Error
			{
				AddModuleList(nModuleID);
			}
			else
			{
				if(EPGetGroupState(nModuleID, 0) == state)
				{
					AddModuleList(nModuleID);
				}
			}
		}
	}
	m_wndAllSystemGrid.LockUpdate(bLock);
	m_wndAllSystemGrid.Redraw();

	GetDlgItem(IDC_STAGE_SEL_COMBO)->SetWindowText("");
}

BOOL CAllSystemView::AddModuleList(int nModuleID)
{
	int nCount = m_wndAllSystemGrid.GetRowCount();
	m_wndAllSystemGrid.InsertRows(nCount+1, 1);

	m_wndAllSystemGrid.SetValueRange(CGXRange(nCount+1, _GRID_COL_MODULE_ID), long(nModuleID));		//Write Module ID to Column 0.
	m_wndAllSystemGrid.SetValueRange(CGXRange(nCount+1, _GRID_COL_MODULE_NAME), ::GetModuleName(nModuleID));

	UpdateModule(nModuleID);
	
	return TRUE;
}

//Contol Context Menu를 보여 준다.
LRESULT CAllSystemView::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(pGrid != (CMyGridWnd *)&m_wndAllSystemGrid && pGrid != (CMyGridWnd *)&m_wndLayoutGrid)	return 0;

	if(pGrid == (CMyGridWnd *)&m_wndLayoutGrid)
	{
		if(nCol%2 !=0 )		return 0;
	}

	ASSERT(pGrid);
	if(nCol<1 || nRow < 1)	return 0;

	if( m_nSelectItem == TEMP_VIEW )
	{
		int nCurModuleID = 0;

		if(pGrid == (CMyGridWnd *)&m_wndLayoutGrid)
		{
			nCurModuleID = GetModuleID(nRow, nCol);

			CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
			ASSERT(pDoc);

			CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

			if( pMainFrm->m_pSensorDlg != NULL )
			{
				delete pMainFrm->m_pSensorDlg;
				pMainFrm->m_pSensorDlg = NULL;
			}
			
			// 20200520 KSCHOI TEMPERATURE SENSOR BY MODULE START
			//pMainFrm->m_pSensorDlg = new CSensorDataDlg(pDoc, this);
			pMainFrm->m_pSensorDlg = new CSensorDataDlg(pDoc, this, nCurModuleID);
			// 20200520 KSCHOI TEMPERATURE SENSOR BY MODULE END
			ASSERT(pMainFrm->m_pSensorDlg);

			pMainFrm->m_pSensorDlg->m_nInstallModuleNum = pDoc->GetInstalledModuleNum();
			pMainFrm->m_pSensorDlg->Create(IDD_SENSOR_DATA_DLG, GetDesktopWindow());
			pMainFrm->m_pSensorDlg->ShowWindow(SW_SHOW);
		}
	}
	//else if( m_nSelectItem == STATUS_VIEW || m_nSelectItem == OPERATION_MODE_VIEW ) // 19-01-10 엄륭 SBC Version 추가 관련
	else if( m_nSelectItem == STATUS_VIEW)
	{
		if( AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Use Debug", 0) == 0 )
		{
			return 0;
		}

		CPoint point;
		::GetCursorPos(&point);

		if (point.x == -1 && point.y == -1)
		{
			//keystroke invocation
			CRect rect;
			pGrid->GetClientRect(rect);
			pGrid->ClientToScreen(rect);
			point = rect.TopLeft();
			point.Offset(5, 5);
		}

		CMenu menu, *pPopup;
		VERIFY(menu.LoadMenu(IDR_CONTEXT3));
		pPopup = menu.GetSubMenu(0);
		ASSERT(pPopup != NULL);

		CWnd* pWndPopupOwner = this;
		while (pWndPopupOwner->GetStyle() & WS_CHILD)
			pWndPopupOwner = pWndPopupOwner->GetParent();

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
	}

//19-01-10 엄륭 SBC Version 추가 관련/////////////////////////////////////////////////////////////
	else if( m_nSelectItem == OPERATION_MODE_VIEW )
	{
		m_nCurModuleID = GetModuleID(nRow, nCol);

		if(m_nCurModuleID > 0)
		{
			CString strData;
			CString strTemp;

			if( EPGetModuleState(m_nCurModuleID) != EP_STATE_LINE_OFF )
			{
				EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(EPGetModuleIndex(m_nCurModuleID));
				if(pSysData != NULL)	
				{
					int i=0;
					for(i=0; i<pSysData->wInstalledBoard; i++ )
					{
						pSysData->cVersionMain[i][6] = 0x00;
						strTemp.Format("[BD:%d]  %s", i+1, pSysData->cVersionMain[i] );

						strData += strTemp;

						strData += "\n";
					}			

					MessageBox( strData, "Main Board Version", MB_ICONINFORMATION );
				}
			}
			else
			{
				CPoint point;
				::GetCursorPos(&point);

				if (point.x == -1 && point.y == -1)
				{
					//keystroke invocation
					CRect rect;
					pGrid->GetClientRect(rect);
					pGrid->ClientToScreen(rect);
					point = rect.TopLeft();
					point.Offset(5, 5);
				}

				CMenu menu, *pPopup;
				VERIFY(menu.LoadMenu(IDR_CONTEXT3));
				pPopup = menu.GetSubMenu(0);
				ASSERT(pPopup != NULL);

				CWnd* pWndPopupOwner = this;
				while (pWndPopupOwner->GetStyle() & WS_CHILD)
					pWndPopupOwner = pWndPopupOwner->GetParent();

				pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
			
			}
		}	
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	return TRUE;
}

int CAllSystemView::GetModuleID(ROWCOL nRow)
{
	return atol(m_wndAllSystemGrid.GetValueRowCol(nRow, _GRID_COL_MODULE_ID));	//0 Column에서 모듈 ID를 구한다.
}

int CAllSystemView::GetCurModuleID()
{
	return m_nCurModuleID;
}

void CAllSystemView::OnRunModule() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + " [Start]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	int mID;
	WORD state;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)		//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));
			CFormModule *pModule = pDoc->GetModuleInfo(mID);
			
			if( state != EP_STATE_LINE_OFF )
			{
				//20200914 ksj
				if(pModule->m_bRackToRack == true) //20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[4],  ::GetModuleName(mID, j));//TEXT - %s는 작업시작을 전송할 수 없는 상태이므로 전송하지 않았습니다.	
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
				//2005/12/30 모듈이 Standby 상태(즉 PC에선 Standby, Ready, End)에서 시험조건이 전송가능하도록 수정됨
				else if(state == EP_STATE_STANDBY || state == EP_STATE_END || state == EP_STATE_IDLE || state == EP_STATE_READY)	
				{
					if(pDoc->SendRunCommand(mID, j) == 0)
					{
						strFailMD += strTemp;
					}
					else
					{
						pModule->m_bProcessStart = true;	//20200914 ksj
					}
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[4],  ::GetModuleName(mID, j));//TEXT - %s는 작업시작을 전송할 수 없는 상태이므로 전송하지 않았습니다.	
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");
				
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)		//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				strTemp.Format("[%s] ", ::GetModuleName(mID, j));
				CFormModule *pModule = pDoc->GetModuleInfo(mID);

				if( state != EP_STATE_LINE_OFF )
				{
					//20200914 ksj
					if(pModule->m_bRackToRack == true) //20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[4],  ::GetModuleName(mID, j));//TEXT - %s는 작업시작을 전송할 수 없는 상태이므로 전송하지 않았습니다.	
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
					//2005/12/30 모듈이 Standby 상태(즉 PC에선 Standby, Ready, End)에서 시험조건이 전송가능하도록 수정됨
					else if(state == EP_STATE_STANDBY || state == EP_STATE_END || state == EP_STATE_IDLE || state == EP_STATE_READY)	
					{
						if(pDoc->SendRunCommand(mID, j) == 0)
						{
							strFailMD += strTemp;
						}
						else
						{
							pModule->m_bProcessStart = true;	//20200914 ksj
						}
					}
					else
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[4],  ::GetModuleName(mID, j));//TEXT - %s는 작업시작을 전송할 수 없는 상태이므로 전송하지 않았습니다.
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
				}				
			}

			pDoc->SetProgressPos( i+1 );			
		}

		pDoc->HideProgressWnd();		
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}
}

void CAllSystemView::OnStop()
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+" [Stop]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	
	strTemp.Format(TEXT_LANG[74], nSelNo, pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Stop", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));
			if( state != EP_STATE_LINE_OFF )
			{
				if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
				{	
					if(pDoc->SendStopCmd(mID) == FALSE)
					{
						strFailMD += strTemp;
					}
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[6],  ::GetModuleName(mID, j));//TEXT - %s는 작업중지를 전송할 수 없는 상태이므로 전송하지 않았습니다.		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");		
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				strTemp.Format("[%s] ", ::GetModuleName(mID, j));

				if( state != EP_STATE_LINE_OFF )
				{
					if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
					{	
						if(pDoc->SendStopCmd(mID) == FALSE)
						{
							strFailMD += strTemp;
						}
					}
					else
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[6],  ::GetModuleName(mID, j));//TEXT - %s는 작업중지를 전송할 수 없는 상태이므로 전송하지 않았습니다.		
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
				}
			}
			
			pDoc->SetProgressPos( i+1 );
		}
		
		pDoc->HideProgressWnd();		
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView Stop");
// 20210201 KSCHOI Add Command Button Log END
}

void CAllSystemView::OnReconnect()
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+TEXT_LANG[33]); //" [System Initilize]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	strTemp.Format(TEXT_LANG[75], nSelNo, pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Initialize", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	int nRtn;
	CString strFailMD;
	
	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));								
			if( state != EP_STATE_LINE_OFF )
			{
				if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_SYSTEM_INIT)) != EP_ACK)
				{
					strFailMD += strTemp;
					strTemp.Format("%s (%s-%s)", TEXT_LANG[77], ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
				// EPSendCommand(mID);
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				strTemp.Format("[%s] ", ::GetModuleName(mID, j));
				state = EPGetGroupState(mID, j);
				if( state != EP_STATE_LINE_OFF )
				{
					if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_SYSTEM_INIT)) != EP_ACK)
					{
						strFailMD += strTemp;
						strTemp.Format("%s (%s-%s)",  TEXT_LANG[77], ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
					// EPSendCommand(mID);
				}
			}

			pDoc->SetProgressPos( i+1 );
		}
		
		pDoc->HideProgressWnd();		
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}
}

void CAllSystemView::OnPause()
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+ TEXT_LANG[34]); //" [Pause]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp = _T("");
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format(TEXT_LANG[78], nSelNo, pDoc->m_strModuleName);
	if(MessageBox(strTemp, TEXT_LANG[35], MB_ICONQUESTION|MB_YESNO) != IDYES)	return;//"Pause"

	int mID;
	WORD state;
	int nRtn;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));
			
			if(state == EP_STATE_RUN)	
			{
				if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_PAUSE)) != EP_ACK)
				{
					strFailMD += strTemp;
					strTemp.Format("%s (%s-%s)",  TEXT_LANG[77],  ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
				// EPSendCommand(mID);
			}
			else
			{
				strFailMD += strTemp;
				strTemp.Format(TEXT_LANG[7],  ::GetModuleName(mID, j));//TEXT - %s는 잠시멈춤을 전송할 수 없는 상태이므로 전송하지 않았습니다.
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");
				
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				strTemp.Format("[%s] ", ::GetModuleName(mID, j));
				
				if(state == EP_STATE_RUN)	
				{
					if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_PAUSE)) != EP_ACK)
					{
						strFailMD += strTemp;
						strTemp.Format("%s (%s-%s)",  TEXT_LANG[77],  ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
					// EPSendCommand(mID);
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[7],  ::GetModuleName(mID, j));//TEXT - %s는 잠시멈춤을 전송할 수 없는 상태이므로 전송하지 않았습니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
			
			pDoc->SetProgressPos( i+1 );
		}
		
		pDoc->HideProgressWnd();		
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.

		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView Pause");
// 20210201 KSCHOI Add Command Button Log END
}

void CAllSystemView::OnContinueModule() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+ TEXT_LANG[36]);//" [Continue]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	strTemp.Format(TEXT_LANG[79], nSelNo, pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Continue", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	int nRtn;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));

			CFormModule *pModule = pDoc->GetModuleInfo(mID);	
			if(pModule)
			{
				//20200914 ksj
				if(pModule->m_bRackToRack == true) //20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
				{
					strFailMD += strTemp;
				}
				else if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_EMG 
					|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES
					|| state == EP_STATE_PAUSE )
				{
					pModule->m_bProcessStart = true;	//20200914 ksj
				}
				else
				{
					strFailMD += strTemp;
				}
			}
			else
			{
				strFailMD += strTemp;
			}

			if(strFailMD.IsEmpty())
			{
				if(state == EP_STATE_PAUSE)	
				{
					CString strResultData = _T("");
					CString strTempResultFileName = _T("");

					char	drive[_MAX_PATH]; 
					char	dir[_MAX_PATH]; 
					char	fname[_MAX_PATH]; 
					char	ext[_MAX_PATH]; 

					strResultData = pModule->GetResultFileName(0);

					_splitpath((LPSTR)(LPCTSTR)strResultData, drive, dir, fname, ext);	

					strTempResultFileName.Format("%s\\%s\\%s_TEMP.fmt", drive, dir, fname);

					if( _access(strTempResultFileName, 0) != -1 )
					{
						if( _unlink(strResultData) != -1 )
						{
							CopyFile( strTempResultFileName, strResultData, false );			
						}
					}

					// Module 클래스의 공정 정보를 초기화 후 다시 불러온다.
					pModule->LoadResultData();

					((CMainFrame *)AfxGetMainWnd())->m_pTab->UpdateGroupState(m_nCurModuleID);			

					if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_CONTINUE)) != EP_ACK)
					{
						strFailMD += strTemp;
						strTemp.Format("%s (%s-%s)",  TEXT_LANG[77],  ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[8],  ::GetModuleName(mID, j));//TEXT - %s는 계속진행을 전송할 수 없는 상태이므로 전송하지 않았습니다.

					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}			
			}
			else
			{
				strTemp.Format(TEXT_LANG[8],  ::GetModuleName(mID, j));//TEXT - %s는 계속진행을 전송할 수 없는 상태이므로 전송하지 않았습니다.

				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}

			if( !strFailMD.IsEmpty() )
			{
				strTemp.Format("AllSystemView : Continue Fail ( %d )", mID);//
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}
			else
			{
				pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
				pModule->UpdateTestLogHeaderTempFile();
				pDoc->m_fmst.fnClearReset(mID);
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				CString strFailMDTemp = "";
				state = EPGetGroupState(mID, j);
				strTemp.Format("[%s] ", ::GetModuleName(mID, j));
	
				bool bImpossible = false;
				CFormModule *pModule = pDoc->GetModuleInfo(mID);	
				if(pModule)
				{
					//20200914 ksj
					if(pModule->m_bRackToRack == true) //20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
					{
						strFailMD += strTemp;
					}
					else if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_EMG 
						|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES
						|| state == EP_STATE_PAUSE )
					{
						pModule->m_bProcessStart = true;	//20200914 ksj
					}
					else
					{
						strFailMDTemp += strTemp;
					}
				}
				else
				{
					strFailMDTemp += strTemp;
				}

				if(strFailMDTemp.IsEmpty())
				{
					if(state == EP_STATE_PAUSE)	
					{
						CString strResultData = _T("");
						CString strTempResultFileName = _T("");

						char	drive[_MAX_PATH]; 
						char	dir[_MAX_PATH]; 
						char	fname[_MAX_PATH]; 
						char	ext[_MAX_PATH]; 

						strResultData = pModule->GetResultFileName(0);

						_splitpath((LPSTR)(LPCTSTR)strResultData, drive, dir, fname, ext);	

						strTempResultFileName.Format("%s\\%s\\%s_TEMP.fmt", drive, dir, fname);

						if( _access(strTempResultFileName, 0) != -1 )
						{
							if( _unlink(strResultData) != -1 )
							{
								CopyFile( strTempResultFileName, strResultData, false );			
							}
						}

						// Module 클래스의 공정 정보를 초기화 후 다시 불러온다.
						pModule->LoadResultData();

						((CMainFrame *)AfxGetMainWnd())->m_pTab->UpdateGroupState(m_nCurModuleID);			

						if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_CONTINUE)) != EP_ACK)
						{
							strFailMDTemp += strTemp;
							strTemp.Format("%s (%s-%s)",  TEXT_LANG[77],  ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
							pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
						}
					}
					else
					{
						strFailMDTemp += strTemp;
						strTemp.Format(TEXT_LANG[8],  ::GetModuleName(mID, j));//TEXT - %s는 계속진행을 전송할 수 없는 상태이므로 전송하지 않았습니다.

						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
				}

				if( !strFailMDTemp.IsEmpty() )
				{
					strTemp.Format("AllSystemView : Continue Fail ( %d )", mID);//
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
				else
				{
					pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
					pModule->UpdateTestLogHeaderTempFile();
					pDoc->m_fmst.fnClearReset(mID);
				}

				strFailMD+=strFailMDTemp;
			}
			
			pDoc->SetProgressPos( i+1 );
		}
		
		pDoc->HideProgressWnd();
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView Continue");
// 20210201 KSCHOI Add Command Button Log END
}


void CAllSystemView::OnClear() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] +TEXT_LANG[37]);//" [Initialize]"
		return ;
	}
	
	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;
	
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format(TEXT_LANG[58], nSelNo, pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Reset", MB_ICONQUESTION|MB_YESNO) != IDYES)	
	{
		return;
	}

	int mID;
	WORD state;
	CString strFailMD = _T("");
	
	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));
			//			if(state == EP_STATE_STANDBY || state == EP_STATE_END || state == EP_STATE_READY)
			if( state != EP_STATE_LINE_OFF )
			{
				if(state != EP_STATE_RUN)	
				{
					if(pDoc->SendInitCommand(mID, j) == FALSE)
					{
						strFailMD += strTemp;
					}
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[9],  ::GetModuleName(mID, j));	//TEXT - %s 는 초기화 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.
	
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}					
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				strTemp.Format("[%s] ", ::GetModuleName(mID, j));
				if( state != EP_STATE_LINE_OFF )
				{
					if(state != EP_STATE_RUN)	
					{
						if(pDoc->SendInitCommand(mID, j) == FALSE)
						{
							strFailMD += strTemp;
						}
					}
					else
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[9],  ::GetModuleName(mID, j));//TEXT - %s 는 초기화 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.	
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
				}			
			}

			pDoc->SetProgressPos( i+1 );
		}

		pDoc->HideProgressWnd();		
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView Clear");
// 20210201 KSCHOI Add Command Button Log END
}


void CAllSystemView::OnStepover() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] +TEXT_LANG[38]); //" [Next Step]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	strTemp.Format(TEXT_LANG[80], nSelNo, pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Next Step", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	int nRtn;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));

			if( state != EP_STATE_LINE_OFF )
			{
				if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
				{
					if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_NEXTSTEP)) != EP_ACK)
					{
						strFailMD += strTemp;
						strTemp.Format("%s (%s-%s)",  TEXT_LANG[77],  ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
					// EPSendCommand(mID);
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[10],  ::GetModuleName(mID, j));//TEXT - %s는 다음 Step을 전송할 수 없는 상태이므로 전송하지 않았습니다.		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}			
			}				
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				strTemp.Format("[%s] ", ::GetModuleName(mID, j));
				
				if( state != EP_STATE_LINE_OFF )
				{
					if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
					{
						if(( nRtn = EPSendCommand(mID, j+1, 0, EP_CMD_NEXTSTEP)) != EP_ACK)
						{
							strFailMD += strTemp;
							strTemp.Format("%s (%s-%s)",  TEXT_LANG[77],  ::GetModuleName(mID, j), pDoc->CmdFailMsg(nRtn));		
							pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
						}
						// EPSendCommand(mID);
					}
					else
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[10],  ::GetModuleName(mID, j));//TEXT - %s는 다음 Step을 전송할 수 없는 상태이므로 전송하지 않았습니다.	
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}			
				}
			}
			
			pDoc->SetProgressPos( i+1 );
		}		
		pDoc->HideProgressWnd();		
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView NextStep");
// 20210201 KSCHOI Add Command Button Log END
}

void CAllSystemView::OnUpdateRunModule(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);

	//2005/12/30 모듈이 Standby 상태(즉 PC에선 Standby, Ready, End)에서 시험조건이 전송가능하도록 수정됨	
	// 2009/05/04 EP_ONLINE_MODE 에서도 사용가능하게 변경
	if((gpState == EP_STATE_STANDBY || gpState == EP_STATE_END || gpState == EP_STATE_READY|| gpState == EP_STATE_IDLE) 
			&& !GetDocument()->m_bRunCmd
			&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
		{
			pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);

}

void CAllSystemView::OnUpdateStop(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	
// 	if((gpState == EP_STATE_RUN || gpState == EP_STATE_PAUSE) 
// 		&& !GetDocument()->m_bStopCmd
// 		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
	// 2009/05/04 EP_ONLINE_MODE 에서도 사용가능하게 변경
	if((gpState == EP_STATE_RUN || gpState == EP_STATE_PAUSE) 
		&& !GetDocument()->m_bStopCmd )
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);	
}

void CAllSystemView::OnUpdatePause(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if( gpState == EP_STATE_RUN 
		&& !GetDocument()->m_bPauseCmd
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);	
}

void CAllSystemView::OnUpdateContinueModule(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
// 	if( gpState == EP_STATE_PAUSE 
// 		&& !GetDocument()->m_bContinueCmd
// 		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
	// 2009/05/04 EP_ONLINE_MODE 에서도 사용가능하게 변경
	if( gpState == EP_STATE_PAUSE 
			&& !GetDocument()->m_bContinueCmd )
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
	
}

void CAllSystemView::OnUpdateClear(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(gpState != EP_STATE_RUN	 && gpState != EP_STATE_LINE_OFF && !GetDocument()->m_bInitCmd)
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
	
}

void CAllSystemView::OnUpdateStepover(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//자동 공정모드에서는 Next step 불가(for LSC)
	BOOL	bAutoState=		::EPGetAutoProcess(m_nCurModuleID);
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(gpState == EP_STATE_RUN 
		&& !GetDocument()->m_bNextStepCmd
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE
		&& bAutoState == FALSE)					
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);	

}

void CAllSystemView::OnUpdateReconnect(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

LONG CAllSystemView::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	CString strTemp;

	if(nRow < 1|| nCol < 1)		return 0;

	if(pGrid == (CMyGridWnd *)&m_wndLayoutGrid)
	{
		m_nCurModuleID = GetModuleID(nRow, nCol);
	}

	return 0;	
}

LONG CAllSystemView::OnGridSelChanged(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;	
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	int nSelNo = 0;
	CString strTemp;
	if(pGrid == (CMyGridWnd *)&m_wndAllSystemGrid)
	{
		CRowColArray	awRows;
		pGrid->GetSelectedRows(awRows);
		nSelNo = awRows.GetSize();				
	}	
	return 0;	
}

VOID CAllSystemView::InitColorBtn()
{
	m_Btn_Status.SetFontStyle(30,1);
	m_Btn_Status.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_OperationMode.SetFontStyle(30, 1);
	m_Btn_OperationMode.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);		
	m_Btn_InspectionPassTime.SetFontStyle(30, 1);
	m_Btn_InspectionPassTime.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_OperationView.SetFontStyle(30, 1);
	m_Btn_OperationView.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);		
	m_Btn_TypeSetting.SetFontStyle(30, 1);
	m_Btn_TypeSetting.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_BtnErrorHistory.SetFontStyle(30, 1);
	m_BtnErrorHistory.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_BtnAlarmOff.SetFontStyle(30, 1);
	m_BtnAlarmOff.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_BtnTemperature.SetFontStyle(30, 1);
	m_BtnTemperature.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_BtnLogPrev.SetFontStyle(40, 1);
	m_BtnLogPrev.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_BtnLogNext.SetFontStyle(40, 1);
	m_BtnLogNext.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_BtnLogDelete.SetFontStyle(40, 1);
	m_BtnLogDelete.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
}

void CAllSystemView::InitLayoutGrid()
{
	m_wndLayoutGrid.SubclassDlgItem(IDC_LAYOUT_GRID, this);
//	m_wndLayoutGrid.m_bRowSelection	= TRUE;
	m_wndLayoutGrid.m_bSameRowSize = FALSE;
	m_wndLayoutGrid.m_bSameColSize = FALSE;
	m_wndLayoutGrid.m_bCustomWidth = TRUE;

	m_wndLayoutGrid.Initialize();
	m_wndLayoutGrid.EnableCellTips();

	BOOL bLock = m_wndLayoutGrid.LockUpdate();
//	m_wndLayoutGrid.SetDefaultRowHeight(45);
	
	m_nLayOutRow = GetDocument()->m_nModulePerRack;
	m_wndLayoutGrid.SetRowCount(m_nLayOutRow);
	
	m_nLayOutCol =  GetDocument()->GetInstalledModuleNum()/m_nLayOutRow;
	if((GetDocument()->GetInstalledModuleNum()%m_nLayOutRow) > 0) 
		m_nLayOutCol++;

	//Use MemDc
	m_wndLayoutGrid.SetDrawingTechnique(gxDrawUsingMemDC);
	m_wndLayoutGrid.SetColCount(m_nLayOutCol*2);
//	m_wndLayoutGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar
//	m_wndLayoutGrid.m_bCustomColor 	= TRUE;	
	
	m_wndLayoutGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	CString strTemp = _T("");
	int i = 0;
	int nIndex = 0;
	STR_TOP_CONFIG *pCofig = GetDocument()->GetTopConfig();
	BYTE color;

	//New Version //20070807
	for(i =0; i<m_nLayOutCol; i++)
	{		
		m_wndLayoutGrid.SetValueRange(CGXRange().SetCols((i+1)*2), TEXT_LANG[39]); //"[ Empty ]"
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols((i+1)*2), CGXStyle().SetEnabled(FALSE));

		m_wndLayoutGrid.SetValueRange(CGXRange().SetCols((i+1)*2-1), "---");
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols((i+1)*2-1), CGXStyle().SetEnabled(FALSE).SetDraw3dFrame(gxFrameRaised));				
	}	
	
	for( i=0; i<m_nLayOutRow; i++ )
	{
		if( m_bRackIndexUp == TRUE )
		{	// 밑에서 위로
			nIndex = m_nLayOutRow - i;
			strTemp.Format("%d", nIndex);
			m_wndLayoutGrid.SetStyleRange(CGXRange(i+1, 0), CGXStyle().SetValue(strTemp));			
		}
		else
		{	// 위에서 아래로
			nIndex = i+1;
			strTemp.Format("%d",nIndex);
			m_wndLayoutGrid.SetStyleRange(CGXRange(i+1, 0), CGXStyle().SetValue(strTemp));
		}
	}
	
	int nRow, nCol;
	for( i =0; i<GetDocument()->GetInstalledModuleNum(); i++)
	{
		int nMD = ::EPGetModuleID(i);
		GetModuleRowCol(nMD, nRow, nCol);
		if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
		{
			GetDocument()->GetStateMsg(IDS_TEXT_LINE_OFF, color);	
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), 
													CGXStyle().SetValue(TEXT_LANG[62])
															  .SetInterior(pCofig->m_BStateColor[color])
															  .SetTextColor(pCofig->m_TStateColor[color])
															  .SetEnabled(TRUE));
			strTemp = ::GetModuleName(nMD);	//Module Name			
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol-1), CGXStyle().SetEnabled(FALSE).SetDraw3dFrame(gxFrameRaised).SetValue(strTemp));
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol-1), CGXStyle().SetFont(CGXFont().SetSize(13).SetBold(TRUE)));
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
		}
		//열단위 굵은 라인 표시 
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(nCol), 
												CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0)))
									);

		if( nCol == m_nLayOutCol*2 )
		{
			m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(nCol), 
				CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0)))
									);
		}
	}
	//열단위 굵은 라인 표시 
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(1), 
											CGXStyle()
													.SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetRows(1), 
											CGXStyle()
													.SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetRows(m_nLayOutRow), 
											CGXStyle()
													.SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));

	int nBayStartNum = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "BayStartNum", 1);

	// 1열/2열 표시 (상단)  
	for( i = 0; i<m_nLayOutCol; i++)
	{
		m_wndLayoutGrid.SetCoveredCellsRowCol(0, i*2+1, 0, (i+1)*2);			//
		strTemp.Format("%d %s", i+nBayStartNum, TEXT_LANG[81]);	//1열 2열 표시 
 		m_wndLayoutGrid.SetStyleRange(CGXRange(0, i*2+1), CGXStyle().SetValue(strTemp).SetTextColor(RGB_LIGHTBLUE));
	}
	
	m_wndLayoutGrid.EnableGridToolTips();

	/*
	if( GetDocument()->m_nDeviceID == DEVICE_FORMATION4 )
	{
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetTable(),
			CGXStyle().SetFont(CGXFont().SetSize(GRID_DATA_FONT_SIZE-2).SetBold(TRUE).SetFaceName(::GetStringTable(IDS_LANG_TEXT_FONT)))
			//SetBold(bFontBold)
			//.SetItalic(bFontItalic)
			//.SetUnderline(bFontUnderLine)
			//.SetStrikeOut(bStrikeOut))
			);
	}
	else
	{
	*/
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetTable(),
			CGXStyle().SetFont(CGXFont().SetSize(GRID_DATA_FONT_SIZE-2).SetBold(TRUE).SetFaceName(TEXT_LANG[76]))
			//SetBold(bFontBold)
			//.SetItalic(bFontItalic)
			//.SetUnderline(bFontUnderLine)
			//.SetStrikeOut(bStrikeOut))
			);
	// }
	
	m_wndLayoutGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	m_wndLayoutGrid.LockUpdate(bLock);
	m_wndLayoutGrid.Redraw();
}

/*
//LayOut Grid상의 State 모양을 Update 시킨다.
BOOL CAllSystemView::StateReDraw(int nModuleID)
{
	int nRow, nCol;
	if(GetModuleRowCol(nModuleID, nRow, nCol) == FALSE)	
		return FALSE;
	CCTSMonDoc *pDoc = GetDocument();
	
	WORD	state;
	BYTE 	colorFlag = 0;
	CString strMsg, strTemp;
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == NULL)		
		return FALSE;

	STR_TOP_CONFIG *pConfig = NULL;
	pConfig = pDoc->GetTopConfig();
	ASSERT(pConfig);
	state =  pModule->GetState(TRUE);
	strMsg = pDoc->GetStateMsg(state, colorFlag);			//Group State
	if(pModule->GetLineMode() == EP_ONLINE_MODE)
	{
		strMsg = "[ON] "+strMsg;
	}
	else
	{
		if(::EPGetAutoProcess(nModuleID) == TRUE)
		{
			strMsg ="[Auto] "+strMsg;
		}
	}

	if(EPTrayState(nModuleID, 0) == EP_TRAY_LOAD)
	{
		strTemp =pModule->GetTrayNo();
		if(!strTemp.IsEmpty())
		{
			strMsg = strMsg+"\n"+strTemp;
		}
		else
		{
			strMsg = strMsg+"\n"+"Unknown";
		}
	}

	if(m_bCodeCount)
	{
		EP_GROUP_INFO *pgpData =  EPGetGroupInfo(nModuleID);
		if(pgpData)
		{
			if(pgpData->gpData.gpState.codeState[1] > 0)
			{
				strMsg.Format("%d", pgpData->gpData.gpState.codeState[1]);
			}
			else
			{
				strMsg.Empty();
			}
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
																	   .SetTextColor(RGB(255, 0, 0))
																	   .SetInterior(RGB(255,255,220)));
		}
	}
	else
	{
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetValue(strMsg)
																		   .SetTextColor(pConfig->m_TStateColor[colorFlag])
																		   .SetInterior(pConfig->m_BStateColor[colorFlag]));
	}
	return TRUE;
}
*/

//LayOut Grid상의 Row/Col을 구한다.
BOOL CAllSystemView::GetModuleRowCol(int nModuleID, int &nRow, int &nCol)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if(nModuleIndex < 0)	return FALSE;

	if(m_bRackIndexUp)
	{
		nCol = (nModuleIndex / m_nLayOutRow+1)*2;						//0~11 => 2
		nRow = m_nLayOutRow - ((nModuleIndex) % m_nLayOutRow);			//12-0~11
	}
	else
	{
		nCol = (nModuleIndex / m_nLayOutRow+1)*2;						
		nRow = ((nModuleIndex) % m_nLayOutRow)+1;			
	}
	return TRUE;
}

//LayOut Grid에서 주어진 Row,Col에 해당하는 모듈 ID를 구한다.
int CAllSystemView::GetModuleID(ROWCOL nRow, ROWCOL nCol)
{
	if(nRow < 1 || nCol < 1)	return 0;

	int index = 0;

	if(m_bRackIndexUp)
	{
		index = int((nCol-1)/2)*m_nLayOutRow+(m_nLayOutRow - nRow);		//하단에서 부터 표시 
	}
	else
	{
		index = int((nCol-1)/2)*m_nLayOutRow+nRow-1;					//상단에서 부터 표시 
	}
	
	if(index < 0 )	return 0;

	return EPGetModuleID(index);
}

void CAllSystemView::OnDispType() 
{
	// TODO: Add your control notification handler code here
//	UpdateData(TRUE);
	GetDlgItem(IDC_ERRORCODE_SEL_COMBO)->EnableWindow(FALSE);
	GetDlgItem(IDC_ERRORCODE_SEL_COMBO)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STAGE_SEL_COMBO)->EnableWindow(FALSE);
	GetDlgItem(IDC_STAGE_SEL_COMBO)->ShowWindow(SW_HIDE);
	
	/*
	if(m_bLayoutView)
	{
		CRect rect;
		// GetDlgItem(IDC_DISP_TYPE)->SetWindowText(::GetStringTable(IDS_TEXT_LAYOUT_VIEW));
		m_wndAllSystemGrid.ShowWindow(SW_HIDE);
		m_wndLayoutGrid.ShowWindow(SW_SHOW);
		
		GetDlgItem(IDC_STAGE_SEL_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_STAGE_SEL_COMBO)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_ERRORCODE_SEL_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_ERRORCODE_SEL_COMBO)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_CODE)->ShowWindow(SW_SHOW);						
	}
	else
	{
		CRect rect;
		// GetDlgItem(IDC_DISP_TYPE)->SetWindowText(::GetStringTable(IDS_TEXT_LIST_VIEW));
		m_wndAllSystemGrid.ShowWindow(SW_SHOW);
		m_wndLayoutGrid.ShowWindow(SW_HIDE);
		
		GetDlgItem(IDC_ERRORCODE_SEL_COMBO)->EnableWindow(FALSE);		
		GetDlgItem(IDC_ERRORCODE_SEL_COMBO)->ShowWindow(SW_HIDE);		
		GetDlgItem(IDC_CHECK_CODE)->ShowWindow(SW_HIDE);				
		GetDlgItem(IDC_STAGE_SEL_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_STAGE_SEL_COMBO)->ShowWindow(SW_SHOW);
	}
	*/
	Invalidate();	
}

//On Line 으로 전환( 전환시 초기에 Control Mode로 된다.)
//On Line 시에는 반드시 Control Mode로 전환 하여야 한다.
void CAllSystemView::OnOnlineMode() 
{
	// TODO: Add your command handler code here
	if( LoginPremissionCheck(PMS_MODULE_MODE_CHANGE) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + TEXT_LANG[40]); //" [Change to OnLine]"
		return;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;

	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format(TEXT_LANG[82], nSelNo, pDoc->m_strModuleName);

	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			if( state != EP_STATE_LINE_OFF )
			{
				if( pDoc->SetLineMode(mID, j, EP_ONLINE_MODE) == false )
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[41],  ::GetModuleName(mID)); //"%s는 OnLine mode 변경에 실패 하였습니다."		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}

			DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, 100, "Sending initialization to module...");
		float fProgressStep = 100.0f/(float)nSelNo;
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				if( state != EP_STATE_LINE_OFF )
				{
					if( pDoc->SetLineMode(mID, j, EP_ONLINE_MODE) == false )
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[11],  ::GetModuleName(mID));//TEXT - %s는 OnLine mode 변경에 실패 하였습니다.
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}

					DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
				}
			}
		
			if( i == nSelNo )
			{
				pDoc->SetProgressPos( 100 );
			}
			else
			{
				pDoc->SetProgressPos( (int)(fProgressStep*(float)(i+1)) );
			}			
		}
		
		pDoc->HideProgressWnd();		
	}	
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}
}

void CAllSystemView::OnUpdateOnlineMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != EP_ID_FORM_OP_SYSTEM)
	{
		pCmdUI->Enable(FALSE);
		return;		
	}
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	
	if((gpState == EP_STATE_IDLE || gpState == EP_STATE_READY || gpState == EP_STATE_STANDBY) 
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) != EP_ONLINE_MODE)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

void CAllSystemView::OnUpdateOfflineMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != EP_ID_FORM_OP_SYSTEM)
	{
		pCmdUI->Enable(FALSE);
		return;		
	}

	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	
	if((gpState == EP_STATE_IDLE || gpState == EP_STATE_READY || gpState == EP_STATE_STANDBY) 
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) != EP_OFFLINE_MODE)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

//OffLine 으로 전환( 전환시 초기에 Control Mode로 된다.)
void CAllSystemView::OnOfflineMode() 
{
	// TODO: Add your command handler code here
/*	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR));
		return ;
	}
*/
	if( LoginPremissionCheck(PMS_MODULE_MODE_CHANGE) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] +" [Change to OffLine]");
		return;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	strTemp.Format((TEXT_LANG[83]), nSelNo, pDoc->m_strModuleName);

	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;
	
	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			if( state != EP_STATE_LINE_OFF )
			{
				if( pDoc->SetLineMode(mID, j, EP_OFFLINE_MODE) == false )
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[12],  ::GetModuleName(mID));//TEXT - %s는 OffLine mode 변경에 실패 하였습니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
			DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, 100, "Sending initialization to module...");
		float fProgressStep = 100.0f/(float)nSelNo;
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				if( pDoc->SetLineMode(mID, j, EP_OFFLINE_MODE) == false )
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[12],  ::GetModuleName(mID));// %s는 OffLine mode 변경에 실패 하였습니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}

				DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
			}
			
			if( i == nSelNo )
			{
				pDoc->SetProgressPos( 100 );
			}
			else
			{
				pDoc->SetProgressPos( (int)(fProgressStep*(float)(i+1)) );
			}			
		}
		
		pDoc->HideProgressWnd();		
	}	
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}
}

void CAllSystemView::OnMaintenanceMode() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + TEXT_LANG[42]); //" [Change to Maintenance]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	
	strTemp.Format(TEXT_LANG[84], nSelNo);
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;
	
	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			if( state != EP_STATE_LINE_OFF )
			{
				if( pDoc->SetOperationMode(mID, j, EP_OPEARTION_MAINTENANCE) == false )
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[11],  ::GetModuleName(mID));//TEXT - %s는 OnLine mode 변경에 실패 하였습니다.	
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
		}
		DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, 100, "Sending initialization to module...");
		float fProgressStep = 100.0f/(float)nSelNo;
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				if( state != EP_STATE_LINE_OFF )
				{
					if( pDoc->SetOperationMode(mID, j, EP_OPEARTION_MAINTENANCE) == false )
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[11],  ::GetModuleName(mID));//TEXT - %s는 OnLine mode 변경에 실패 하였습니다.
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
				}
			}

			DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
			
			if( i == nSelNo )
			{
				pDoc->SetProgressPos( 100 );
			}
			else
			{
				pDoc->SetProgressPos( (int)(fProgressStep*(float)(i+1)) );
			}			
		}
		
		pDoc->HideProgressWnd();		
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView MaintModeChange");
// 20210201 KSCHOI Add Command Button Log END
}

void CAllSystemView::OnUpdateMaintenanceMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//20200421 KSJ
	bool bEnable = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ModeChangeEnable", 0);
	if(bEnable == false)
	{
		pCmdUI->Enable(FALSE);
		return;
	}


	//Maintenance/Control Mode는 충방전 Program이 아니라 다른 외부 프로그램을 이용하도록 변경
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	int nOperationMode = GetDocument()->GetOperationMode(m_nCurModuleID, m_nCurGroup);

	if( nOperationMode == EP_OPERATION_LOCAL || nOperationMode == EP_OPERATION_AUTO || nOperationMode == EP_OPERATION_CALIBRATION)
	{	
		if( gpState == EP_STATE_IDLE || gpState == EP_STATE_STANDBY || gpState == EP_STATE_READY || gpState == EP_STATE_END )
		{
			pCmdUI->Enable(TRUE);		
			return;
		}
	}
	
	pCmdUI->Enable(FALSE);
	
/*
	if( gpState != EP_STATE_RUN && nLineMode != EP_ONLINE_MODE && gpState != EP_STATE_LINE_OFF && gpState != EP_STATE_MAINTENANCE )
	{
		if( gpState == EP_STATE_IDLE || gpState == EP_STATE_READY || gpState == EP_STATE_STANDBY )
		{
			pCmdUI->Enable(TRUE);
		}
	}
*/
}

void CAllSystemView::OnUpdateCaliMode(CCmdUI* pCmdUI) 
{

	//20200421 KSJ
	bool bEnable = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ModeChangeEnable", 0);
	if(bEnable == false)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	// TODO: Add your command update UI handler code here

	//Maintenance/Control Mode는 충방전 Program이 아니라 다른 외부 프로그램을 이용하도록 변경
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	int nOperationMode = GetDocument()->GetOperationMode(m_nCurModuleID, m_nCurGroup);


	if( nOperationMode == EP_OPERATION_CALIBRATION)
	{

	}
	else
	{
		if( gpState == EP_STATE_IDLE || gpState == EP_STATE_STANDBY || gpState == EP_STATE_READY || gpState == EP_STATE_END || gpState == EP_STATE_MAINTENANCE )
		{
			pCmdUI->Enable(TRUE);
			return;
		}
	}
	
// 	if( nOperationMode == EP_OPERATION_LOCAL )
// 	{	
// 		if( gpState == EP_STATE_IDLE || gpState == EP_STATE_STANDBY || gpState == EP_STATE_READY || gpState == EP_STATE_END )
// 		{
// 			pCmdUI->Enable(TRUE);		
// 			return;
// 		}
// 	}
	
	pCmdUI->Enable(FALSE);
	
/*
	if( gpState != EP_STATE_RUN && nLineMode != EP_ONLINE_MODE && gpState != EP_STATE_LINE_OFF && gpState != EP_STATE_MAINTENANCE )
	{
		if( gpState == EP_STATE_IDLE || gpState == EP_STATE_READY || gpState == EP_STATE_STANDBY )
		{
			pCmdUI->Enable(TRUE);
		}
	}
*/
}
void CAllSystemView::OnControlMode() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+ TEXT_LANG[43]); //" [Change to Control]"
		return ;
	}
	
	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;
	
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	strTemp.Format(TEXT_LANG[85], nSelNo);
	
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;
	
	int mID;
	WORD state;
	CString strFailMD;
	
	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.

		//Cali state init
		CFormModule* pModule = pDoc->GetModuleInfo(mID);
		pModule->GetTrayInfo()->SetTrayNo("");

		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			if( state != EP_STATE_LINE_OFF )
			{
				if( state == EP_STATE_IDLE || state == EP_STATE_STANDBY || state == EP_STATE_READY || state == EP_STATE_MAINTENANCE )
				{
					m_bLocalReservation[mID-1] = FALSE;

					if( pDoc->SetOperationMode(mID, j, EP_OPERATION_LOCAL) == false )
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[13],  ::GetModuleName(mID));//TEXT - %s는 작업모드 변경에 실패 하였습니다.
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}

					::EPSetAutoProcess(mID, FALSE);
					pDoc->SaveAutoProcSetToDB(mID, FALSE);

					DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.					
				}
				else
				{
					if( m_bLocalReservation[mID-1] == TRUE )
					{
						m_bLocalReservation[mID-1] = FALSE;
// 20210201 KSCHOI Add Command Button Log START
						CString csLog;
						csLog.Format("[Stage %s] AllSystemView LocalReservation FALSE", ::GetModuleName(mID));
						pDoc->WriteLog(csLog);
// 20210201 KSCHOI Add Command Button Log END
					}
					else
					{
						m_bLocalReservation[mID-1] = TRUE;
// 20210201 KSCHOI Add Command Button Log START
						CString csLog;
						csLog.Format("[Stage %s] AllSystemView LocalReservation TRUE", ::GetModuleName(mID));
						pDoc->WriteLog(csLog);
// 20210201 KSCHOI Add Command Button Log END
					}					
				}
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				if( state != EP_STATE_LINE_OFF )
				{
					if( pDoc->SetOperationMode(mID, j, EP_OPERATION_LOCAL) == false )
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[13],  ::GetModuleName(mID));	//%s는 작업모드 변경에 실패 하였습니다.	
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}

					::EPSetAutoProcess(mID, FALSE);
					pDoc->SaveAutoProcSetToDB(mID, FALSE);
					
					DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
				}
				else
				{
					if( m_bLocalReservation[mID-1] == TRUE )
					{
						m_bLocalReservation[mID-1] = FALSE;
// 20210201 KSCHOI Add Command Button Log START
						CString csLog;
						csLog.Format("[Stage %s] AllSystemView LocalReservation FALSE", ::GetModuleName(mID));
						pDoc->WriteLog(csLog);
// 20210201 KSCHOI Add Command Button Log END
					}
					else
					{
						m_bLocalReservation[mID-1] = TRUE;
// 20210201 KSCHOI Add Command Button Log START
						CString csLog;
						csLog.Format("[Stage %s] AllSystemView LocalReservation TRUE", ::GetModuleName(mID));
						pDoc->WriteLog(csLog);
// 20210201 KSCHOI Add Command Button Log END
					}
				}
			}
			
			pDoc->SetProgressPos( i+1 );
		}
		
		pDoc->HideProgressWnd();		
	}		
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView LocalMode Change");
// 20210201 KSCHOI Add Command Button Log END
}

void CAllSystemView::OnUpdateControlMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//Maintenance/Control Mode는 충방전 Program이 아니라 다른 외부 프로그램을 이용하도록 변경 2004/3/28
	//20200421 KSJ
	bool bEnable = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ModeChangeEnable", 0);
	if(bEnable == false)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
		
	int nOperationMode;
	nOperationMode = GetDocument()->GetOperationMode(m_nCurModuleID, m_nCurGroup);

	if( nOperationMode == EP_OPERATION_LOCAL)
	{

	}
	else
	{
 		if( gpState == EP_STATE_IDLE || gpState == EP_STATE_STANDBY || gpState == EP_STATE_READY || gpState == EP_STATE_END || gpState == EP_STATE_MAINTENANCE )
 		{
			pCmdUI->Enable(TRUE);
			return;
		}
	}

	pCmdUI->Enable(FALSE);
}

void CAllSystemView::UpdateModuleStateCount()
{
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;
	WORD state;
	int totMD, offMD = 0, runMD = 0;
	
	totMD = pDoc->GetInstalledModuleNum();

	for(int i=0; i<totMD; i++)
	{
		state = EPGetGroupState(EPGetModuleID(i), 0);
		if(	state == EP_STATE_LINE_OFF)		//통신 불능 상태
		{
			offMD++;
		}
		else if(state == EP_STATE_RUN)		//작업 중인 장비
		{
			runMD++;
		}
	}

	strTemp.Format("%d %s", runMD, pDoc->m_strModuleName);
	m_ctrlRunModule.SetText(strTemp);

	strTemp.Format("%d %s", (totMD - offMD) - runMD, pDoc->m_strModuleName);
	m_ctrlIdleModule.SetText(strTemp);

	strTemp.Format("%d %s", offMD, pDoc->m_strModuleName);
	m_ctrlLineOffModule.SetText(strTemp);
}

//shared Memory 삭제 후 rebooting Command
void CAllSystemView::OnReboot() 
{
	// TODO: Add your command handler code here
	CRebootDlg	*pDlg;
	pDlg = new CRebootDlg(this);
	ASSERT(pDlg);

	if(EPGetGroupState(m_nCurModuleID, 0) == EP_STATE_RUN)
	{
		CString strTemp;
		strTemp.Format(TEXT_LANG[86], ::GetModuleName(m_nCurModuleID));
		if(MessageBox(strTemp, TEXT_LANG[44], MB_ICONQUESTION|MB_YESNO) != IDYES) //"Running..."
			return;
	}

	pDlg->SetModuleName(::GetModuleName(m_nCurModuleID), GetDocument()->GetModuleIPAddress(m_nCurModuleID));
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}
// -----------------------------------------------------------------------------
//  [4/30/2009 PNE]
// Unit, State, Tray, Door, Jig, Procedure 정보를 (.csv) 파일로 저장
// -----------------------------------------------------------------------------
void CAllSystemView::OnFileSave() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = GetDocument();
	CString FileName;
	FileName = "sysmon.csv";
	CFileDialog pDlg(FALSE, "", FileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[45]); //"csv File(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return;
	}

	FILE *fp = fopen(FileName, "wt");
	if(fp == NULL)	return;

	fprintf(fp, "PNE Cell Test System\n\n");
	
	CString strMsg;
	int nRow = m_wndAllSystemGrid.GetRowCount();
	int nModuleID, nTotGroup;
	WORD state;
	BYTE colorFlag;
	CString strTemp;
	
	m_ctrlTotalModule.GetWindowText(strTemp);
	fprintf(fp, "Total,%s\n", strTemp);
	m_ctrlLineOffModule.GetWindowText(strTemp);
	fprintf(fp, "LineOff,%s\n", strTemp);
	m_ctrlRunModule.GetWindowText(strTemp);
	fprintf(fp, "Run,%s\n", strTemp);
	m_ctrlIdleModule.GetWindowText(strTemp);
	fprintf(fp, "Idle,%s\n\n", strTemp);

	strMsg.Format("%s,state,Tray,Door,Jig,Procedure", pDoc->m_strModuleName);
	fprintf(fp, "%s\n", strMsg);

	for(int row = 1; row <= nRow; row++)
	{
		nModuleID = GetModuleID(row);
		nTotGroup = EPGetGroupCount(nModuleID);

		for(int nGroup = 0; nGroup<nTotGroup; nGroup++)
		{
			//Module Name
			strMsg = ::GetModuleName(nModuleID, nGroup);
			fprintf(fp, "%s,", strMsg);

			//Module state
			state =  EPGetGroupState(nModuleID, nGroup);
			strMsg = pDoc->GetStateMsg(state, colorFlag);			//Group State
			if(pDoc->GetLineMode(nModuleID, nGroup) == EP_ONLINE_MODE)
			{
				strMsg = "[ON]"+strMsg;
			}
			fprintf(fp, "%s,", strMsg);

			//Tray State
			state = EPTrayState(nModuleID, nGroup);
			strMsg = TrayStateMsg(state);
			if(state == EP_TRAY_LOAD)
			{
				strTemp = pDoc->GetTrayNo(nModuleID, nGroup);
				if(!strTemp.IsEmpty()) strMsg = strTemp;
			}
			fprintf(fp, "%s,", strMsg);
					
			//Door State
			strMsg = DoorStateMsg(EPDoorState(nModuleID, nGroup));
			fprintf(fp, "%s,", strMsg);
			
			//Jig State
			strMsg = JigStateMsg(EPJigState(nModuleID, nGroup));
			fprintf(fp, "%s,", strMsg);

			//공정 
			strMsg = pDoc->GetTestName(nModuleID, nGroup);
			fprintf(fp, "%s,", strMsg);
			fprintf(fp, "\n");
		}
	}
	fclose(fp);	
}

LONG CAllSystemView::OnGridDoubleClick(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	CString strTemp;
	
	if(nRow < 1|| nCol < 1)		return 0;

	if(pGrid == (CMyGridWnd *)&m_wndLayoutGrid)
	{
		m_nCurModuleID = GetModuleID(nRow, nCol);
	}

	if(m_nCurModuleID <= 0)	
	{
		return 0;
	}	
	
	//TopView 다시 그리기	
	CCTSMonDoc *pDoc=  GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	if(pModule == NULL)		return 0; 

	LPEP_MD_SYSTEM_DATA	pSysData;
	ZeroMemory(&pSysData, sizeof(LPEP_MD_SYSTEM_DATA));
	pSysData = EPGetModuleSysData(EPGetModuleIndex(m_nCurModuleID));	

	int nColNum = 0; 	
	pMainFrm->m_wndSplitter.ShowRow(SELECT_SUB_ITEM);
	pMainFrm->m_uSelect = SELECT_SUB_ITEM;	
	
	// 1. 모듈이 선택되면 한번만 실행되는 지점
	if( m_nCurModuleID != m_nPrevModuleID )		
	{
		InitStageView();
	}
	
	if( pMainFrm->m_uSelect == SELECT_SUB_ITEM )
	{
		UpdateModule(m_nCurModuleID);			
	}
	
	pMainFrm->m_pTab->m_tabWnd.ActivateTab(1);
	
	/*	
	if( m_nCurModuleID != m_nPrevModuleID )	
	{
		m_nPrevModuleID = m_nCurModuleID;
		pDoc->SetAutoReport( m_nCurModuleID, m_nPrevModuleID );	
	}
	*/
	
	return 0;
}

void CAllSystemView::InitStageView()
{
	if(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pManualControlView) // Main Tab
	{
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pManualControlView->StopMonitoring();
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pManualControlView->SetCurrentModule(m_nCurModuleID);
	}

	if(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pTopView)			// Main Tab
	{
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pTopView->StopMonitoring();
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pTopView->SetCurrentModule(m_nCurModuleID);
	}

	if(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pConnectionView )
	{
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pConnectionView->StopMonitoring();
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pConnectionView->SetCurrentModule(m_nCurModuleID);
	}

	if(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pOperationView )
	{
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pOperationView->StopMonitoring();
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pOperationView->SetCurrentModule(m_nCurModuleID);
	}	
	if(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pAutoView)
	{
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pAutoView->StopMonitoring();
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pAutoView->SetCurrentModule(m_nCurModuleID);
	}	
}

void CAllSystemView::OnUpdateAutoProcOn(CCmdUI* pCmdUI) 
{
	int nSelNo;
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	int mID;
	BOOL bAutoState;
	WORD wdModuleState;
	for (int i = 0; i<nSelNo; i++)			//선택한 모든 Row
	{
		mID = awSelModule.GetAt(i);			//선택 Row의 ID를 구한다.

		wdModuleState=	::EPGetGroupState(mID, 0);
		bAutoState=		::EPGetAutoProcess(mID);

		if(wdModuleState == EP_STATE_LINE_OFF || wdModuleState==EP_STATE_RUN || wdModuleState==EP_STATE_PAUSE ||
			GetDocument()->GetLineMode(mID) != EP_OFFLINE_MODE || bAutoState==TRUE)
		{
			pCmdUI->Enable(FALSE);
			return;
		}
	}		
	pCmdUI->Enable(TRUE);
}

void CAllSystemView::OnUpdateAutoProcOff(CCmdUI* pCmdUI) 
{
	int nSelNo;
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	int mID;
	BOOL bAutoState;
	WORD wdModuleState;
	for (int i = 0; i<nSelNo; i++)			//선택한 모든 Row
	{
		mID = awSelModule.GetAt(i);			//선택 Row의 ID를 구한다.
		wdModuleState=	::EPGetGroupState(mID, 0);
		bAutoState=		::EPGetAutoProcess(mID);

		if( wdModuleState==EP_STATE_LINE_OFF|| wdModuleState==EP_STATE_RUN||
		    wdModuleState==EP_STATE_PAUSE||
			GetDocument()->GetLineMode(mID) != EP_OFFLINE_MODE || bAutoState==FALSE)
		{
			pCmdUI->Enable(FALSE);
			return;
		}
	}	
	pCmdUI->Enable(TRUE);		
}

void CAllSystemView::OnAutoProcOn() 
{
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	int nSelNo = awSelModule.GetSize();
		
	CCTSMonDoc *pDoc = GetDocument();
	pDoc->SendAutoProcessOn(awSelModule, TRUE);

	for (int i = 0; i<nSelNo; i++)							//선택한 모든 Row
	{
		DrawModule(awSelModule.GetAt(i), FALSE);			//선택 Row의 ID를 구한다.		
	}	
}

void CAllSystemView::OnAutoProcOff() 
{
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	int nSelNo = awSelModule.GetSize();
		
	CCTSMonDoc *pDoc = GetDocument();
	pDoc->SendAutoProcessOn(awSelModule, FALSE);

	for (int i = 0; i<nSelNo; i++)								//선택한 모든 Row
	{
		DrawModule(awSelModule.GetAt(i), FALSE);			//선택 Row의 ID를 구한다.		
	}	
}

void CAllSystemView::OnRefAdValueUpdate() 
{
	// TODO: Add your command handler code here
	
	//Dialog Permission Check
	CCTSMonDoc *pDoc = GetDocument();
	int i, nGroupNo, nSelNo;

	//선택 모듈 확인 
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	CString strTemp;
	if(nSelNo <=0 )
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	strTemp.Format(TEXT_LANG[87], nSelNo, pDoc->m_strModuleName);
	if(MessageBox(strTemp, TEXT_LANG[46], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Ref IC Update"

	if(LoginPremissionCheck(PMS_MODULE_CAL_UPDATE) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + TEXT_LANG[47]); //" [Reference IC Value Update]"
		return ;
	}

	int mID;
	WORD state;
	EP_MD_SYSTEM_DATA *lpSysData;

	//선택 모듈에 모두 전송 
	for (i = 0; i<nSelNo; i++)				//선택한 모든 Row
	{
		mID = awSelModule.GetAt(i);			//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);	//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<1; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			lpSysData = EPGetModuleSysData(EPGetModuleIndex(mID));
			
			if(lpSysData == NULL)	continue;

			//Version 검사
			//상태 검사
			if(state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_END)
			{
				int nRtn = EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_CAL_EXEC);
				if(nRtn != EP_ACK)
				{
					strTemp.Format("%s (%s-%s)",  TEXT_LANG[77], ::GetModuleName(m_nCurModuleID, 0), pDoc->CmdFailMsg(nRtn));		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					AfxMessageBox(strTemp);
				}
				// EPSendCommand(mID);
			}
		}
	}	
}

void CAllSystemView::OnTraynoUserInput() 
{
	// TODO: Add your command handler code here
	((CMainFrame *)AfxGetMainWnd())->UserInputTrayNo(m_nCurModuleID);
}

void CAllSystemView::OnUpdateTraynoUserInput(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);

	if( (gpState == EP_STATE_STANDBY || gpState == EP_STATE_END || gpState == EP_STATE_READY || gpState == EP_STATE_IDLE))
//		&& EPGetAutoProcess(m_nCurModuleID)
//		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)			
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{	pCmdUI->Enable(FALSE);

	}		
}

void CAllSystemView::OnViewResult() 
{
	// TODO: Add your command handler code here
	GetDocument()->ViewResult(m_nCurModuleID);
}

void CAllSystemView::OnUpdateViewResult(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	BOOL bEnable = FALSE;
	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		CString str;
		for(int i =0; i<pModule->GetTotalJig(); i++)
		{
			str = pModule->GetResultFileName(i);
			if(str.IsEmpty() == FALSE)
			{
				bEnable = TRUE;
				break;
			}
		}
	}
	pCmdUI->Enable(bEnable);
}

void CAllSystemView::OnConditionView() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);
}

void CAllSystemView::OnUpdateConditionView(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(	gpState == EP_STATE_LINE_OFF || gpState == EP_STATE_IDLE)
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
}

void CAllSystemView::OnDataRestore() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + TEXT_LANG[48]); //" [Data restore]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nSelNo;
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format("Restore lost data of %s?", pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Data restore", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;
	CFormModule *pModule;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.		
		pModule = pDoc->GetModuleInfo(mID);
		if(pModule)
		{
			state = EPGetGroupState(mID, 0);
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			if(state != EP_STATE_LINE_OFF && pModule->GetLineMode() != EP_ONLINE_MODE)
			{
				if(pModule->RestoreLostData() == FALSE)
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[14],  ::GetModuleName(mID));	//TEXT - %s는 Data 복구를 실패 하였습니다.	
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
			else
			{
				strFailMD += strTemp;
				strTemp.Format(TEXT_LANG[15],  ::GetModuleName(mID));//TEXT - %s는 복구할 수 없는 상태 입니다.
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, 100, "Sending initialization to module...");
		
		float fProgressStep = 100.0f/(float)nSelNo;
		
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);			//선택 Row의 ID를 구한다.
			pModule = pDoc->GetModuleInfo(mID);
			if(pModule)
			{
				state = EPGetGroupState(mID, 0);
				strTemp.Format("[%s] ", ::GetModuleName(mID));
				if(state != EP_STATE_LINE_OFF && pModule->GetLineMode() != EP_ONLINE_MODE)
				{
					if(pModule->RestoreLostData() == FALSE)
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[14],  ::GetModuleName(mID));//TEXT - %s는 Data 복구를 실패 하였습니다.	
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[15],  ::GetModuleName(mID));//TEXT - %s는 복구할 수 없는 상태 입니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
			
			if( i == nSelNo )
			{
				pDoc->SetProgressPos( 100 );
			}
			else
			{
				pDoc->SetProgressPos( (int)(fProgressStep*(float)(i+1)) );
			}			
		}
		
		pDoc->HideProgressWnd();		
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[14], strFailMD);//TEXT - %s는 Data 복구를 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[49], MB_OK|MB_ICONWARNING); //"Fail"
	}
	else
	{
		strTemp.Format(TEXT_LANG[16], nSelNo);// TEXT - %d 개 Unit data 복구를 완료 하였습니다.
		MessageBox(strTemp, TEXT_LANG[49], MB_OK|MB_ICONWARNING); //"Fail"
	}
}

void CAllSystemView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	switch( nIDEvent )
	{
	case TIMER_MAIN:
		{
			COleDateTime nowtime(COleDateTime::GetCurrentTime());			

			if( m_nPreDay != nowtime.GetDay() )
			{	
				m_nPreDay = nowtime.GetDay();
				
				int nErrorHistorySaveInterval = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ErrorHistorySavePeriod", 0);	
				
				if( nErrorHistorySaveInterval != 0 )
				{
					DeleteErrorHistory(nErrorHistorySaveInterval);
				}

// 				int nCalibrationNotifyAlarmPeriod = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "CalibrationNotifyAlarmPeriod", 0);
// 
// 				if( nCalibrationNotifyAlarmPeriod != 0 )
// 				{
// 					fnCalibrationNotifyAlarm(nCalibrationNotifyAlarmPeriod);
// 				}
			}

			m_LabelDate.SetText(nowtime.Format());
						
			if( m_bUpdateModule == false )
			{
				m_bUpdateModule = true;
			}
			else
			{
				return;
			}
			
			m_bFlash = !m_bFlash;

			UpdateModule();
			
			m_bUpdateModule = false;
		}

	case TIMER_AUTOCALI:
		for(int i=0; i<m_nTotalUnitNum; i++)
		{
			CFormModule *pModule = NULL;
			CCTSMonDoc *pDoc = GetDocument();

			pModule = pDoc->GetModuleInfo(i);
			if(pModule != NULL)
			{
				if(pModule->m_bAutoCaliRun == TRUE)
				{
					pModule->m_bAutoCaliRun = false;
					FnAutoCaliRun(pModule->GetModuleID());
				}
// 				if(pModule->m_bAutoCaliPointSet == TRUE)
// 				{
// 					pModule->m_bAutoCaliPointSet = false;
// 					FnAutoCaliPointSet(pModule->GetModuleID());
// 				}
			}
		}
	}
	
	/*
	switch( nIDEvent )
	{
	case TIMERID_START_JIGTEMP_TARGET_DATA:
		{
			// 1번 Unit의 EP_CMD_JIG_TEMP_SET 값 전송시 전체 UNIT의 평균값을 구한다.
			// 5분간격으로 1초에 1Unit의 상태값을 전송
			CCTSMonDoc *pDoc = GetDocument();
			int		nIndexModule=0, nIndexTemp=0;
			int		nModuleID = 0;		
			long	lTempAvg = 0L;
			int		nCnt = 0;
			int		nUnitCnt = 0;

			CFormModule		*pModule;	
			_MAPPING_DATA	*pMapData;

			m_nJigTempSendUnitIndex = 0;
			m_nTotalModule = EPGetInstalledModuleNum();
			m_lUnitJigTempAvg = 0L;
			nUnitCnt = 0;

			for( nIndexModule=0; nIndexModule<m_nTotalModule; nIndexModule++ )
			{
				nModuleID = EPGetModuleID( nIndexModule );
				pModule = pDoc->GetModuleInfo( nModuleID );	
				
				EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);
				nCnt = 0;

				if( pModule )
				{
					if( pModule->GetLineMode() == EP_ONLINE_MODE )
					{
						if( gpData.gpState.state != EP_STATE_LINE_OFF )
						{
							nUnitCnt++;
							
							for( nIndexTemp=0; nIndexTemp<EP_MAX_TEMP; nIndexTemp++ )
							{
								pMapData = pModule->GetSensorMap(0, nIndexTemp);
								
								if( pMapData->nChannelNo == 1 )		// Jig일 경우
								{
									lTempAvg += gpData.sensorData.sensorData1[nIndexTemp].lData;
									nCnt++;
								}
							}
							if( lTempAvg != 0 && nCnt != 0 ) {
								lTempAvg = lTempAvg/nCnt;				// 평균값을 구한다.
							}
							else {
								lTempAvg = 0;
							}
							
							m_lUnitJigTempAvg = m_lUnitJigTempAvg + lTempAvg;
						}
					}
				}						
			}

			if( m_lUnitJigTempAvg != 0 && nUnitCnt != 0 ) {
				m_lUnitJigTempAvg = m_lUnitJigTempAvg/nUnitCnt;
			}
			else {
				m_lUnitJigTempAvg = 0;
			}
			
			SetTimer( TIMERID_SEND_JIGTEMP_TARGET_DATA, TIMERINTERVAL_SEND_JIGTEMP_TARGET_DATA, NULL );			
		}
		break;

	case TIMERID_SEND_JIGTEMP_TARGET_DATA:
		{
			KillTimer(TIMERID_SEND_JIGTEMP_TARGET_DATA);

			if( m_bSendMsgCmd == true )
			{
				SetTimer( TIMERID_SEND_JIGTEMP_TARGET_DATA, TIMERINTERVAL_SEND_JIGTEMP_TARGET_DATA, NULL );
				return;
			}
			
			int		nModuleID = 0;
			CCTSMonDoc		*pDoc = GetDocument();
			CFormModule		*pModule;
			nModuleID = EPGetModuleID( m_nJigTempSendUnitIndex );
			pModule = pDoc->GetModuleInfo( nModuleID );	

			EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);
			EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(nModuleID);

			if( pModule )
			{
				if( pModule->GetLineMode() == EP_ONLINE_MODE )
				{
					if( gpData.gpState.state != EP_STATE_LINE_OFF )
					{
						EP_CMD_JIGTEMP_TARGET_DATA data;
						ZeroMemory( &data, sizeof( EP_CMD_JIGTEMP_TARGET_DATA ));
						data.wCurJigAvgTemp = (unsigned short)m_lUnitJigTempAvg;
						data.useJigTempSetDate = lpSysParam->bUseJigTargetTemp;
						data.wIntervalTime = lpSysParam->nJigTempInterval;
						data.wTargetTemp = lpSysParam->nJigTargetTemp*100;
						
						if(EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)
						{
							EPSendCommand(nModuleID, 0, 0, EP_CMD_JIGTEMP_TARGET_SET, &data, sizeof(EP_CMD_JIGTEMP_TARGET_DATA));
						}
					}
				}
			}

			m_nJigTempSendUnitIndex++;

			if( m_nTotalModule > m_nJigTempSendUnitIndex )
			{				
				SetTimer( TIMERID_SEND_JIGTEMP_TARGET_DATA, TIMERINTERVAL_SEND_JIGTEMP_TARGET_DATA, NULL );
			}
		}		
		break;
		
	case TIMERID_SAFECHK_CHVOLTAGE:
		{
			KillTimer(TIMERID_SAFECHK_CHVOLTAGE);

			CFormModule *pModule = NULL;
			CellVoltageError *m_pCellVoltageError;

			bool bFindWarnningCH = false;
			bool bFindDangerCH = false;
			
			int nChIndex = 0;
			CCTSMonDoc *pDoc = GetDocument();
			CString strUpErrorLog = _T("");
			CString strDownErrorLog = _T("");			
			CString strTemp = _T("");
			CString strEmgErrorLog = _T("");
			int nModuleID = 1;
			int i = 0;
	
			// 1. CTSMon에서 실행중인 UNIT에 대해 채널 전압 상한 안전조건을 검사
			// 2. 경고값: 경고창, 위험값: 조치방법 설정
			// 3. 모든 Unit 상태에 대해 확인.
			for( i = 0; i<m_nTotalUnitNum; i++ )
			{	
				nModuleID = EPGetModuleID(i);
				pModule = pDoc->GetModuleInfo(nModuleID);
				strEmgErrorLog = _T("");

				if( pModule )
				{
					bFindWarnningCH = false;
					bFindDangerCH = false;
					strEmgErrorLog = _T("");
					strUpErrorLog = _T("");

					WORD gpState = pModule->GetState();
					// if( gpState == EP_STATE_RUN || gpState == EP_STATE_IDLE )
					if( gpState != EP_STATE_FAULT && gpState != EP_STATE_LINE_OFF )
					{
						// 실행되는 부분
						for( nChIndex=0; nChIndex<pModule->GetTotalInstallCh(); nChIndex++ )
						{							
							STR_SAVE_CH_DATA chSaveData;
							pModule->GetChannelStepData(chSaveData, nChIndex, 0);	
							long lVoltage = (long)chSaveData.fVoltage;
							
							if( lVoltage > pDoc->m_lDangerChVoltage )
							{
								bFindDangerCH = TRUE;
								
								if( strEmgErrorLog.IsEmpty() )
								{
									strEmgErrorLog.Format("TrayId: %s, 안전조건: %d, [%d]_%d ",
										pModule->GetTrayNo(),
										pDoc->m_lDangerChVoltage,
										nChIndex+1,
										lVoltage
										);
								}
								else
								{										
									strTemp.Format(" [%d]_%d ",
										nChIndex+1,
										lVoltage										
										);
									strEmgErrorLog = strEmgErrorLog + strTemp;
								}
								
								if( strUpErrorLog.IsEmpty() )
								{
									strTemp.Format("%d,%d,", nModuleID, nChIndex+1);
									strUpErrorLog = strUpErrorLog + strTemp;
								}
								else
								{
									strTemp.Format("%d,", nChIndex+1);
									strUpErrorLog = strUpErrorLog + strTemp;
								}		
							}
							else if( lVoltage > pDoc->m_lWarnningChVoltage )
							{
								bFindWarnningCH = TRUE;
								
								if( strEmgErrorLog.IsEmpty() )
								{
									strEmgErrorLog.Format("TrayId: %s, 안전조건: %d, [%d]_%d ",
										pModule->GetTrayNo(),
										pDoc->m_lWarnningChVoltage,
										nChIndex+1,										
										lVoltage
										);
								}
								else
								{										
									strTemp.Format(" [%d]_%d ",
										nChIndex+1,
										lVoltage										
										);
									strEmgErrorLog = strEmgErrorLog + strTemp;
								}
								
								if( strUpErrorLog.IsEmpty() )
								{
									strTemp.Format("%d,%d,", nModuleID, nChIndex+1);
									strUpErrorLog = strUpErrorLog + strTemp;
								}
								else
								{
									strTemp.Format("%d,", nChIndex+1);
									strUpErrorLog = strUpErrorLog + strTemp;
								}								
							}
						}

						if( bFindDangerCH || bFindWarnningCH )
						{
							if( m_bErrorCellVoltageDlg[i] == true )
							{
								
							}
							else
							{
								m_bErrorCellVoltageDlg[i] = true;

								if( bFindDangerCH == true )
								{
									strTemp.Format(",%s,채널 전압위험 발생 - %s", ::GetModuleName(nModuleID), strEmgErrorLog);
									pDoc->WriteEMGLog( strTemp );
								}

								else
								{
									strTemp.Format(",%s,채널 전압경고 발생 - %s", ::GetModuleName(nModuleID), strEmgErrorLog);
									pDoc->WriteEMGLog( strTemp );
								}

								m_pCellVoltageError = new CellVoltageError(NULL, strUpErrorLog, strDownErrorLog);
								m_pCellVoltageError->Create(IDD_DLG_ERROR_CELLVOLTAGE, this);
								m_pCellVoltageError->ShowWindow(SW_SHOW);
								m_ptCellVoltageDlg.Add( m_pCellVoltageError );
							
								if( bFindDangerCH )
								{
									switch( pDoc->m_nAfterSafeVoltageError )		// Error 조치 관련 설정
									{
									case 1:
										{
											EPSendCommand(nModuleID, 1, 0, EP_CMD_PAUSE);
											
											m_pCellVoltageError = new CellVoltageError(NULL, strUpErrorLog, strDownErrorLog);
											m_pCellVoltageError->Create(IDD_DLG_ERROR_CELLVOLTAGE, this);
											m_pCellVoltageError->ShowWindow(SW_SHOW);
											m_ptCellVoltageDlg.Add( m_pCellVoltageError );
										}					
										break;
									case 2:
										{
											EPSendCommand(nModuleID, 1, 0, EP_CMD_STOP);
										
											m_pCellVoltageError = new CellVoltageError(NULL, strUpErrorLog, strDownErrorLog);
											m_pCellVoltageError->Create(IDD_DLG_ERROR_CELLVOLTAGE, this);
											m_pCellVoltageError->ShowWindow(SW_SHOW);
											m_ptCellVoltageDlg.Add( m_pCellVoltageError );
										}					
										break;
									}
								}
							}				
						}
						else
						{
							m_bErrorCellVoltageDlg[i] = false;
							
							if( m_ptCellVoltageDlg.GetSize() > 0 )
							{
								for (int i=0 ; i< m_ptCellVoltageDlg.GetSize() ; i++)
								{
									m_pCellVoltageError = (CellVoltageError *)m_ptCellVoltageDlg[i];
									if (m_pCellVoltageError->IsWindowVisible())
									{
										continue;
									}
									else
									{
										m_ptCellVoltageDlg.RemoveAt(i);
									}
								}
							}
						}
					}
				}
			}		
			SetTimer(TIMERID_SAFECHK_CHVOLTAGE, TIMERINTERVAL_SAFECHK_CHVOLTAGE, NULL);
		}		
		break;
	*/
	CFormView::OnTimer(nIDEvent);
}

void CAllSystemView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	KillTimer(603);	
}

BOOL CAllSystemView::GetSelectedModuleList(CWordArray &awSelModule)
{	
	CGXRangeList *rangeList = m_wndLayoutGrid.GetParam()->GetRangeList();
	int nSelCellCount = rangeList->GetCount();
	POSITION	pos;
	ROWCOL		nRow, nCol;
	BOOL		nRtn;
	CGXRange	*SelCell;
	
	if(nSelCellCount == 0) //Only One Cell is Selected
	{
		if(m_wndLayoutGrid.GetCurrentCell(nRow, nCol))
		{
			awSelModule.Add((WORD)GetModuleID(nRow, nCol));
		}
	}

	for(pos = rangeList->GetHeadPosition(); pos != NULL; )
	{
		SelCell = (CGXRange *)rangeList->GetAt(pos);
		ASSERT(SelCell);
		nRtn = SelCell->GetFirstCell(nRow, nCol);
		while(nRtn)
		{
			if( nCol%2 == 0 )
			{
				awSelModule.Add((WORD)GetModuleID(nRow, nCol));
			}				
			nRtn = SelCell->GetNextCell(nRow, nCol);
		}
	   rangeList->GetNext(pos); 
	}

	return TRUE;
}

void CAllSystemView::OnUpdateDataRestore(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CCTSMonDoc *pDoc = GetDocument();
	if (pDoc->m_bUseOnLine)
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}	
}

void CAllSystemView::OnCheckCode() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	UpdateModule();
}

void CAllSystemView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndAllSystemGrid == (CMyGridWnd *)pWnd)
	{
		m_wndAllSystemGrid.Copy();
	}
	else if(&m_wndLayoutGrid == (CMyGridWnd *)pWnd)
	{
		m_wndLayoutGrid.Copy();
	}
}

void CAllSystemView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndAllSystemGrid == (CMyGridWnd *)pWnd || &m_wndLayoutGrid == (CMyGridWnd *)pWnd)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
	
}

//DEL void CAllSystemView::OnButton1() 
//DEL {
//DEL 	LPEP_MD_SYSTEM_DATA	pSysData;
//DEL 	ZeroMemory(&pSysData,sizeof(LPEP_MD_SYSTEM_DATA));
//DEL 	
//DEL 	pSysData = EPGetModuleSysData(m_nCurModuleID-1);
//DEL 	CString	strTemp;
//DEL 	strTemp.Format("wTrayType=%d,Module ID = %d",pSysData->wTaryType,pSysData->nModuleID);
//DEL 	AfxMessageBox(strTemp);
//DEL 	
//DEL }

void CAllSystemView::OnUpdateChangeTrayType1(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);

	// TODO: Add your command update UI handler code here
	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);

	if(pModule)
	{
		if (pModule->m_iTrayTypeCount > 1) 
		{
			LPEP_MD_SYSTEM_DATA	pSysData;
			ZeroMemory(&pSysData,sizeof(LPEP_MD_SYSTEM_DATA));
			pSysData = EPGetModuleSysData(m_nCurModuleID-1);
//			TRACE("wTrayType=%d,Module ID = %d",pSysData->wTrayType,pSysData->nModuleID);
//			if(pModule->m_sTrayTypeData[pSysData->wTrayType].iTrayStyle==)
			if (pSysData->wTrayType == 3)
				pCmdUI->Enable(TRUE);
		}

	}
}

void CAllSystemView::OnUpdateChangeTrayType2(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);

	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);

	if(pModule)
	{
		if (pModule->m_iTrayTypeCount > 1) 
		{
			LPEP_MD_SYSTEM_DATA	pSysData;
			ZeroMemory(&pSysData,sizeof(LPEP_MD_SYSTEM_DATA));
			pSysData = EPGetModuleSysData(m_nCurModuleID-1);
			//			TRACE("wTrayType=%d,Module ID = %d",pSysData->wTrayType,pSysData->nModuleID);
			//			if(pModule->m_sTrayTypeData[pSysData->wTrayType].iTrayStyle==)
			if (pSysData->wTrayType == 2)
				pCmdUI->Enable(TRUE);
		}
		
	}
}

void CAllSystemView::OnChangeTrayType1() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo;
	WORD state;
	CString strTemp;

 	nGroupNo = EPGetGroupCount(m_nCurModuleID)-1;		//선택 모듈 ID의 Group 수를 구한다.
 	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
 	if(pModule)
	{
		if (pModule->m_iTrayTypeCount > 1) 
		{
			state = EPGetGroupState(m_nCurModuleID, nGroupNo);
			strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID, nGroupNo));
			if(state == EP_STATE_IDLE)// || state == EP_STATE_STANDBY || state == EP_STATE_STOP || state == EP_STATE_READY || state == EP_STATE_END)	
			{
				strTemp.Format(TEXT_LANG[17]);//TEXT - 전환 명령을 실행 하시겠습니까?
				if ( MessageBox(strTemp,TEXT_LANG[18],MB_ICONQUESTION|MB_YESNO) == IDNO)//TEXT - 확인
					return;
				if(pDoc->SendChangeTrayTypeCmd(m_nCurModuleID,0,EP_CHANGE_TRAY_MODE0) == FALSE)
				{
					strTemp.Format(TEXT_LANG[19],  ::GetModuleName(m_nCurModuleID, nGroupNo));//TEXT - %s는 형변환 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}
			else
			{
				strTemp.Format(TEXT_LANG[19],  ::GetModuleName(m_nCurModuleID, nGroupNo));//TEXT - %s는 형변환 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				AfxMessageBox(strTemp);
			}
		}		
	}
	else
	{
		strTemp.Format(TEXT_LANG[20],  ::GetModuleName(m_nCurModuleID, nGroupNo));//TEXT - %s -> 형변환 명령 전송 실패.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}
}

void CAllSystemView::OnChangeTrayType2() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo;
	WORD state;
	CString strTemp;
	
 	nGroupNo = EPGetGroupCount(m_nCurModuleID);		//선택 모듈 ID의 Group 수를 구한다.
 	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
 	if(pModule)
	{
		state = EPGetGroupState(m_nCurModuleID, nGroupNo);
		strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID, nGroupNo));
		if(state == EP_STATE_IDLE)// || state == EP_STATE_STANDBY || state == EP_STATE_STOP || state == EP_STATE_READY || state == EP_STATE_END)	
		{	
			strTemp.Format(TEXT_LANG[17]);//TEXT - 전환 명령을 실행 하시겠습니까?
			if ( MessageBox(strTemp,TEXT_LANG[18],MB_ICONQUESTION|MB_YESNO) == IDNO)//TEXT - 확인
				return;
			if(pDoc->SendChangeTrayTypeCmd(m_nCurModuleID,0,EP_CHANGE_TRAY_MODE1) == FALSE)
			{
				strTemp.Format(TEXT_LANG[19],  ::GetModuleName(m_nCurModuleID, nGroupNo));//TEXT - %s는 형변환 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.		
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}
		}
		else
		{
			strTemp.Format(TEXT_LANG[19],  ::GetModuleName(m_nCurModuleID, nGroupNo));	//TEXT - %s는 형변환 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			AfxMessageBox(strTemp);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[20],  ::GetModuleName(m_nCurModuleID, nGroupNo));	//TEXT - %s -> 형변환 명령 전송 실패
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);
	}
}


void CAllSystemView::OnLayoutView() 
{
	// TODO: Add your command handler code here
	m_bLayoutView = !m_bLayoutView;

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "LayoutView", m_bLayoutView);

	OnDispType();	
}

void CAllSystemView::OnSelchangeErrorcodeSelCombo() 
{
	// TODO: Add your control notification handler code here
	int nIndex = m_ctrlCodeCombo.GetCurSel();
	m_nCurErrorCode = m_ctrlCodeCombo.GetItemData(nIndex);
}

void CAllSystemView::OnGetProfileData() 
{
	// TODO: Add your command handler code here
	if(GetDocument()->DownLoadProfileData(m_nCurModuleID, GetDocument()->GetResultFileName(m_nCurModuleID, 0), TRUE) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[88]);
	}	
}

void CAllSystemView::OnNewProc() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + TEXT_LANG[34]);//" [Pause]"
		return ;
	}
	
	//선택 모듈 
	int nSelNo;
	CString strTemp;
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	ASSERT(pDoc);
	
	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();
	
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	
	STR_CONDITION_HEADER testHeader = pDoc->SelectTest();
	if(testHeader.lID == 0)		return;
	
	CTestCondition testCondition;
	if(testCondition.LoadTestCondition(testHeader.lID)==FALSE)
	{
		strTemp.Format(TEXT_LANG[21], testHeader.szName);	//선택 조건 [%s] Loading을 실패 하였습니다.	
		AfxMessageBox(strTemp);
		return;
	}
	
	// 1. 사용자 메세지 전송 준비
	m_bSendMsgCmd = true;
	
	int mID, nGroupNo;
	WORD state;
	CString strFailMD;
	for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
	{
		mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		strTemp.Format("=============> %d", nGroupNo);
		TRACE(strTemp);
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			strTemp.Format("[%s] ", ::GetModuleName(mID, j));
			
			if( state != EP_STATE_LINE_OFF )
			{
				//2005/12/30 모듈이 Standby 상태(즉 PC에선 Standby, Ready, End)에서 시험조건이 전송가능하도록 수정됨
				if(state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_STANDBY || state == EP_STATE_END)
				{
					if( state != EP_STATE_IDLE )
					{						
						CFormModule *pModule = pDoc->GetModuleInfo(mID);
						if(pModule != NULL)
						{
							pModule->ResetGroupData();
						}
					}

					if(pDoc->SendConditionToModule(mID, j, &testCondition) == FALSE)
					{
						strFailMD += strTemp;
						strTemp.Format("%s (%s)",  TEXT_LANG[77],  ::GetModuleName(mID, j));		
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}
									
					//Stanby => Standby 전송시 Update가 필요한 ModuleState change를 감지하여 Update하는 부분이 있어서 
					AfxGetMainWnd()->SendMessage(EPWM_MODULE_STATE_CHANGE, MAKELONG(j, mID), NULL);
				}
				else
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[22],  ::GetModuleName(mID, j));//TEXT - %s는 공정조건을 전송할 수 없는 상태이므로 전송하지 않았습니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
			}			
		}
	}
	
	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[22], strFailMD);//TEXT - %s는 공정조건을 전송할 수 없는 상태이므로 전송하지 않았습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}
	
	// 사용자 메세지 전송 완료
	m_bSendMsgCmd = false;
}

void CAllSystemView::OnUpdateNewProc(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if( (gpState == EP_STATE_STANDBY || gpState == EP_STATE_READY ||  gpState == EP_STATE_END || gpState == EP_STATE_IDLE)
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE && EPGetAutoProcess(m_nCurModuleID) == FALSE)
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
}

void CAllSystemView::OnBnClickedBtnStatus()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();	

	if( pMainFrm->m_uSelect != SELECT_MAIN_ITEM )
	{
		pMainFrm->m_pTab->StopGroupTimer();
		pMainFrm->m_uSelect = SELECT_MAIN_ITEM;
		pMainFrm->m_wndSplitter.HideRow(SELECT_SUB_ITEM);
	}
	
	m_LabelSelectName.SetText(TEXT_LANG[29]); //"Stage Status"
	
	m_nSelectItem = STATUS_VIEW;	

	UpdateModule();

	// ::PostMessage(pMainFrm->GetSafeHwnd(), EPWM_FMS_ERROR_INFO, 101, 0);
}

void CAllSystemView::OnBnClickedBtnOperationMode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	if( pMainFrm->m_uSelect != SELECT_MAIN_ITEM )
	{
		pMainFrm->m_pTab->StopGroupTimer();
		pMainFrm->m_uSelect = SELECT_MAIN_ITEM;
		pMainFrm->m_wndSplitter.HideRow(1);
	}	

	m_LabelSelectName.SetText(TEXT_LANG[50]); //"Operation Mode"

	m_nSelectItem = OPERATION_MODE_VIEW;

	UpdateModule();
	
}

void CAllSystemView::OnBnClickedBtnOperationView()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	if( pMainFrm->m_uSelect != SELECT_MAIN_ITEM )
	{
		pMainFrm->m_pTab->StopGroupTimer();
		pMainFrm->m_uSelect = SELECT_MAIN_ITEM;
		pMainFrm->m_wndSplitter.HideRow(1);	
	}

	m_LabelSelectName.SetText(TEXT_LANG[91]); //"Operation View

	m_nSelectItem = OPERATION_VIEW;

	UpdateModule();
}

void CAllSystemView::OnBnClickedBtnInspectionPassTime()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	if( pMainFrm->m_uSelect != SELECT_MAIN_ITEM )
	{
		pMainFrm->m_pTab->StopGroupTimer();
		pMainFrm->m_uSelect = SELECT_MAIN_ITEM;
		pMainFrm->m_wndSplitter.HideRow(1);
	}
	
	m_LabelSelectName.SetText(TEXT_LANG[51]);//"Inspection Pass Time"

	m_nSelectItem = INSPECTION_PASS_TIME_VIEW;

	UpdateModule();
}

void CAllSystemView::OnDraw(CDC* pDC)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	// CRect rect;
	// GetClientRect(rect);

	// pDC->FillRect(&rect, &m_wndbkBrush);
}

void CAllSystemView::OnBnClickedBtnTypeSettingLocal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strProgramName;
	strProgramName.Format("%s\\CTSEditor.exe", GetDocument()->m_strCurFolder);

	GetDocument()->ExecuteProgram(strProgramName, "", EDITOR_APP_NAME, "", FALSE, TRUE);
}

void CAllSystemView::OnBnClickedBtnLogNext()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateEmgLog(LOG_SELECT_TYPE_NEXT);
}

void CAllSystemView::OnBnClickedBtnLogPrev()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	UpdateEmgLog(LOG_SELECT_TYPE_PREV);
}

void CAllSystemView::OnBnClickedBtnLogDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateEmgLog(LOG_SELECT_TYPE_DELETE);
}

void CAllSystemView::OnBnClickedBtnErrorHistory()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CErrorHistoryDlg dlg(GetDocument());
	dlg.DoModal();
		

}
void CAllSystemView::OnAutoMode()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + TEXT_LANG[43]); //" [Change to Control]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	
	strTemp.Format(TEXT_LANG[60], nSelNo );

	if(MessageBox(strTemp, TEXT_LANG[52], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Mode Change"

	int mID;
	WORD state;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			if( state != EP_STATE_LINE_OFF )
			{
				if( pDoc->SetOperationMode(mID, j, EP_OPERATION_AUTO) == false )
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[13],  ::GetModuleName(mID));//TEXT - %s는 작업모드 변경에 실패 하였습니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}

				::EPSetAutoProcess(mID, FALSE);
				pDoc->SaveAutoProcSetToDB(mID, FALSE);

				DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, 100, "Sending initialization to module...");
		float fProgressStep = 100.0f/(float)nSelNo;

		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				if( state != EP_STATE_LINE_OFF )
				{
					if( pDoc->SetOperationMode(mID, j, EP_OPERATION_AUTO) == false )
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[13],  ::GetModuleName(mID));	//TEXT - %s는 작업모드 변경에 실패 하였습니다.
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}

					::EPSetAutoProcess(mID, FALSE);
					pDoc->SaveAutoProcSetToDB(mID, FALSE);

					DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
				}
			}

			if( i == nSelNo )
			{
				pDoc->SetProgressPos( 100 );
			}
			else
			{
				pDoc->SetProgressPos( (int)(fProgressStep*(float)(i+1)) );
			}			
		}

		pDoc->HideProgressWnd();		
	}		

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD); // "%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView AutoMode Change");
// 20210201 KSCHOI Add Command Button Log END
}
void CAllSystemView::OnCaliMode()
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + TEXT_LANG[43]); //" [Change to Control]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format(TEXT_LANG[60], nSelNo );

	if(MessageBox(strTemp, TEXT_LANG[52], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Mode Change"

	int mID;
	WORD state;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			state = EPGetGroupState(mID, j);
			if( state != EP_STATE_LINE_OFF )
			{
				if( pDoc->SetOperationMode(mID, j, EP_OPERATION_CALIBRATION) == false )
				{
					strFailMD += strTemp;
					strTemp.Format(TEXT_LANG[13],  ::GetModuleName(mID));//TEXT - %s는 작업모드 변경에 실패 하였습니다.
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}

				::EPSetAutoProcess(mID, FALSE);
				pDoc->SaveAutoProcSetToDB(mID, FALSE);

				DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		pDoc->SetProgressWnd(0, 100, "Sending initialization to module...");
		float fProgressStep = 100.0f/(float)nSelNo;

		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				state = EPGetGroupState(mID, j);
				if( state != EP_STATE_LINE_OFF )
				{
					if( pDoc->SetOperationMode(mID, j, EP_OPERATION_CALIBRATION) == false )
					{
						strFailMD += strTemp;
						strTemp.Format(TEXT_LANG[13],  ::GetModuleName(mID));	//TEXT - %s는 작업모드 변경에 실패 하였습니다.
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
					}

					::EPSetAutoProcess(mID, FALSE);
					pDoc->SaveAutoProcSetToDB(mID, FALSE);

					DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
				}
			}

			if( i == nSelNo )
			{
				pDoc->SetProgressPos( 100 );
			}
			else
			{
				pDoc->SetProgressPos( (int)(fProgressStep*(float)(i+1)) );
			}			
		}

		pDoc->HideProgressWnd();		
	}		

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD); // "%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

// 20210201 KSCHOI Add Command Button Log START
	WriteButtonLog(awSelModule, "AllSystemView CaliMode Change");
// 20210201 KSCHOI Add Command Button Log END
}
void CAllSystemView::OnUpdateAutoMode(CCmdUI *pCmdUI)
{

	//20200421 KSJ
	bool bEnable = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ModeChangeEnable", 0);
	if(bEnable == false)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	
	int nOperationMode;
	nOperationMode = GetDocument()->GetOperationMode(m_nCurModuleID, m_nCurGroup);
		
	if( nOperationMode == EP_OPERATION_AUTO)
	{

	}
	else
	{
		if( gpState == EP_STATE_IDLE || gpState == EP_STATE_STANDBY || gpState == EP_STATE_READY || gpState == EP_STATE_END || gpState == EP_STATE_MAINTENANCE )
		{
			pCmdUI->Enable(TRUE);
			return;
		}
	}
	
	pCmdUI->Enable(FALSE);	
}
INT CAllSystemView::strLinker(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totlink = 0;
	UINT totidx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iLink = mesmap[i++];
		memcpy(msg + totlink, stTemp + totidx, iLink); 

		totidx += iLink + 1;
		totlink += iLink;
	}

	return totidx;
}

void CAllSystemView::OnBnClickedBtnChargerAlarmoff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	pMainFrm->SetAlarmOff();
}

BOOL CAllSystemView::fnCalibrationNotifyAlarm(int nInterval)
{
	if( nInterval == 0 )
	{
		return TRUE;		
	}

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	CString strSQL = _T("");
	CString strTemp = _T("");;
	int nMouleIdx = 0;
	
	CDaoDatabase  db;

	try
	{	
		db.Open(pDoc->m_strDataBaseName);		
	}
	catch (CDaoException* e)
	{
		e->Delete();
		return false;
	}

	CTime nowtime(CTime::GetCurrentTime());
	CTimeSpan oneday( nInterval,0,0,0 );
	CTime endtime = nowtime - oneday;

	ZeroMemory( pDoc->m_bCalibrationNotify, sizeof(pDoc->m_bCalibrationNotify));

	try
	{		
		CDaoRecordset rs(&db);
		strSQL.Format("Select ModuleID From SystemConfig Where CalibrationUpdateTime < #%s# ", endtime.Format("%Y/%m/%d") );
			
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs.IsBOF())
		{
			COleVariant data = rs.GetFieldValue(0);
			
			nMouleIdx = data.lVal - 1;
			
			pDoc->m_bCalibrationNotify[nMouleIdx] = TRUE;
		}

		rs.Close();
	}
	catch (CDBException* e)
	{	
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin);		
		e->Delete();
	}
	
	db.Close();

	return true;	
}

BOOL CAllSystemView::DeleteErrorHistory(int nInterval)
{
	if( nInterval == 0 )
	{
		return TRUE;		
	}
	
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	CString strSQL = _T("");
	CString strPeriod = _T("");
	
	CDaoDatabase  db;
	
	try
	{
		db.Open(pDoc->m_strLogDataBaseName);
	}
	catch (CDaoException* e)
	{
		e->Delete();
		return false;
	}
	
	try
	{	
		CTime nowtime(CTime::GetCurrentTime());
				
		CTimeSpan oneday( nInterval,0,0,0 );
		CTime endtime = nowtime - oneday;
		
		strSQL.Format("Delete From EMG_Log Where DateTime < #%s#", endtime.Format("%Y/%m/%d") );
				
		db.Execute(strSQL);
	}
	catch (CDBException* e)
	{	
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin);		
		e->Delete();
	}
	
	db.Close();

	return true;	
}

void CAllSystemView::OnFantypeAll()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	fnSetFanType(EP_FANTYPE_ALL);
}

void CAllSystemView::OnFantypeMiddle()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	fnSetFanType(EP_FANTYPE_MIDDLE);
}

void CAllSystemView::OnFantypeSide()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	fnSetFanType(EP_FANTYPE_SIDE);
}

void CAllSystemView::fnSetFanType( int nFanType )
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56] + " [Change Fantype]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format(TEXT_LANG[23], nSelNo); //선택한 %d개 Stage 의 FanType을 변경하시겠습니까?

	if(MessageBox(strTemp, TEXT_LANG[53], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"FanType Change"

	int mID;
	WORD state;
	CString strFailMD;

	if( nSelNo == 1 )
	{
		// 1. 전송할 Unit이 1개일 경우
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{
			if( pDoc->SetFanType(mID, j, nFanType) == false )
			{
				strFailMD += strTemp;				
			}
			else
			{
				DrawModule(mID, FALSE);			//선택 Row의 ID를 구한다.
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		int nProgressStep = 0;		

		pDoc->SetProgressWnd(0, nSelNo, "Sending Command to module...");
		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{
				if( pDoc->SetFanType(mID, j, nFanType) == false )
				{
					strFailMD += strTemp;					
				}
				else
				{
					DrawModule(mID, FALSE);
				}			
			}

			nProgressStep++;
			pDoc->SetProgressPos( nProgressStep );			
		}

		pDoc->HideProgressWnd();		
	}		

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}
}

void CAllSystemView::OnTraytypeMode0()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;

	if(nSelNo <=0)
	{
		AfxMessageBox(TEXT_LANG[24], MB_ICONINFORMATION); //TEXT-선택된 모듈 정보가 없습니다
		return;
	}

	if(::PasswordCheck() == FALSE)	return;	//20200917 ksj

	strTemp.Format(TEXT_LANG[25], nSelNo);	//TEXT - 선택된 %d개 Stage 의 Traytype 을 전환 하시겠습니까?
	if(MessageBox(strTemp, TEXT_LANG[54], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Traytype Mode Change"

	int mID;

	CString strFailMD;

	if( nSelNo == 1 )
	{	
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{	
			if( pDoc->SetTrayType(mID, j, EP_TRAYTYPE_0) == false )
			{
				strFailMD += strTemp;				
			}
			else
			{
				DrawModule(mID, FALSE);
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		int nProgressStep = 0;
		pDoc->SetProgressWnd(0, nSelNo, "Sending Command to module...");

		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{	
				if( pDoc->SetTrayType(mID, j, EP_TRAYTYPE_0) == false )
				{
					strFailMD += strTemp;					
				}
				else
				{					
					DrawModule(mID, FALSE);
				}
			}

			nProgressStep++;
			pDoc->SetProgressPos( nProgressStep );
		}

		pDoc->HideProgressWnd();		
	}	

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[26], strFailMD); //TEXT - %s => 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[27], MB_OK|MB_ICONWARNING);//TEXT - 전송실패
	}
}

void CAllSystemView::OnTraytypeMode1()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;

	if(nSelNo <=0)
	{
		AfxMessageBox(TEXT_LANG[24], MB_ICONINFORMATION); //TEXT - 선택된 모듈 정보가 없습니다
		return;
	}

	if(::PasswordCheck() == FALSE)	return; //20200917 ksj

	strTemp.Format(TEXT_LANG[25], nSelNo);	//TEXT-선택된 %d개 Stage 의 Traytype 을 전환 하시겠습니까?
	if(MessageBox(strTemp, TEXT_LANG[54], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Traytype Mode Change"

	int mID;

	CString strFailMD;

	if( nSelNo == 1 )
	{	
		mID = awSelModule.GetAt(0);				//선택 Row의 ID를 구한다.
		strTemp.Format("[%s] ", ::GetModuleName(mID));
		nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
		for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
		{	
			if( pDoc->SetTrayType(mID, j, EP_TRAYTYPE_1) == false )
			{
				strFailMD += strTemp;				
			}
			else
			{
				DrawModule(mID, FALSE);
			}
		}
	}
	else
	{
		// 1. 전송할 Unit이 여러개일 경우
		int nProgressStep = 0;
		pDoc->SetProgressWnd(0, nSelNo, "Sending Command to module...");

		for (int i = 0; i<nSelNo; i++)				//선택한 모든 Row
		{
			mID = awSelModule.GetAt(i);				//선택 Row의 ID를 구한다.
			strTemp.Format("[%s] ", ::GetModuleName(mID));
			nGroupNo = EPGetGroupCount(mID);		//선택 모듈 ID의 Group 수를 구한다.
			for(int j = 0; j<nGroupNo; j++)			//모든 Group에 전송한다.
			{	
				if( pDoc->SetTrayType(mID, j, EP_TRAYTYPE_1) == false )
				{
					strFailMD += strTemp;					
				}
				else
				{					
					DrawModule(mID, FALSE);
				}
			}

			nProgressStep++;
			pDoc->SetProgressPos( nProgressStep );
		}

		pDoc->HideProgressWnd();		
	}	

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[26], strFailMD);//TEXT - %s => 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[27], MB_OK|MB_ICONWARNING);//TEXT - 전송실패
	}
}

void CAllSystemView::OnUpdateTraytypeMode0(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	int nOperationMode = GetDocument()->GetOperationMode(m_nCurModuleID, m_nCurGroup);

	if( nOperationMode == EP_OPERATION_LOCAL )
	{	
		if( gpState == EP_STATE_IDLE )
		{
			pCmdUI->Enable(TRUE);		
			return;
		}
	}

	pCmdUI->Enable(TRUE);
}

void CAllSystemView::OnUpdateTraytypeMode1(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	int nOperationMode = GetDocument()->GetOperationMode(m_nCurModuleID, m_nCurGroup);

	if( nOperationMode == EP_OPERATION_LOCAL )
	{	
		if( gpState == EP_STATE_IDLE )
		{
			pCmdUI->Enable(TRUE);		
			return;
		}
	}

	pCmdUI->Enable(FALSE);
}

void CAllSystemView::OnBnClickedBtnTemperature()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	if( pMainFrm->m_uSelect != SELECT_MAIN_ITEM )
	{
		pMainFrm->m_pTab->StopGroupTimer();
		pMainFrm->m_uSelect = SELECT_MAIN_ITEM;
		pMainFrm->m_wndSplitter.HideRow(1);	
	}

	m_LabelSelectName.SetText(TEXT_LANG[55]); //"Temperature View"

	m_nSelectItem = TEMP_VIEW;

	UpdateModule();
}

void CAllSystemView::OnSensorMapView()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	if( m_nCurModuleID == 0 )
	{
		return;
	}

	if( m_pSensorMap == NULL )
	{
		m_pSensorMap = new CSensorMapDlg( GetDocument(), AfxGetMainWnd() );
		m_pSensorMap->m_nModuleID = m_nCurModuleID;

		m_pSensorMap->Create(IDD_SENSOR_MAP_DIALOG, NULL );		
		m_pSensorMap->DisplayMapping(m_nCurModuleID);
		m_pSensorMap->CenterWindow();
		m_pSensorMap->ShowWindow(SW_SHOW);
	}
	else
	{
		m_pSensorMap->m_nModuleID = m_nCurModuleID;
		m_pSensorMap->DisplayMapping(m_nCurModuleID);
		m_pSensorMap->CenterWindow();
		m_pSensorMap->ShowWindow(SW_SHOW);
	}
}


void CAllSystemView::FnAutoCaliRun(int nModuleID)
{
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	CCTSMonDoc *pDoc = GetDocument();
	CFormModule *pModule = NULL;

	pModule = pDoc->GetModuleInfo(nModuleID);
	CString strTemp;

// 
// 	EP_GP_STATE_DATA prevState;
// 	memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
// 	prevState.state = EP_STATE_CALIBRATION;
// 	pModule->SetState(prevState);	


	CCalibratorDlg *pCaliDlg;
	// 캘리창 정리
	if (pFrame->m_apCaliDlg.GetSize() > 0)
	{
		for (int i=0 ; i< pFrame->m_apCaliDlg.GetSize() ; i++)
		{
			pCaliDlg = (CCalibratorDlg *)pFrame->m_apCaliDlg[i];

			if (pCaliDlg->IsWindowVisible())
				continue;
			else
			{
				delete pCaliDlg;
				pCaliDlg = NULL;			
				pFrame->m_apCaliDlg.RemoveAt(i);
			}
		}
	}

	//캘리창 확인
	if (pFrame->m_apCaliDlg.GetSize() > 0)
	{
		for (int i=0 ; i< pFrame->m_apCaliDlg.GetSize() ; i++)
		{
			pCaliDlg = (CCalibratorDlg *)pFrame->m_apCaliDlg[i];
			if (pCaliDlg->m_nUnitNo == nModuleID)	//이미 열려있음
			{
				pCaliDlg->m_bPointSet = TRUE;
				return;
			}
		}
		// 열린창중에는 없음
		pCaliDlg =  new CCalibratorDlg(nModuleID, this);
		ASSERT(pCaliDlg);

		pCaliDlg->m_pdoc = pDoc;
		pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
		pCaliDlg->ShowWindow(SW_SHOW);
		pFrame->m_apCaliDlg.Add(pCaliDlg);

		pCaliDlg->m_bPointSet = TRUE;

	}
	else
	{
		// 열린창이 없음
		pCaliDlg =  new CCalibratorDlg(nModuleID, this);
		ASSERT(pCaliDlg);

		pCaliDlg->m_pdoc = pDoc;
		pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
		pCaliDlg->ShowWindow(SW_SHOW);
		pFrame->m_apCaliDlg.Add(pCaliDlg);

		pCaliDlg->m_bPointSet = TRUE;

	}
}


void CAllSystemView::OnBnClickedButton1() //TEST END
{
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();

	CFormModule *pModule = NULL;
	CCTSMonDoc *pDoc = GetDocument();

	CString strTemp;
	GetDlgItemText(IDC_EDIT_TEST, strTemp);
	int nModuleID= atoi(strTemp);

	EP_GP_STATE_DATA prevState;
	memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
	prevState.state = EP_STATE_END;


	pModule = pDoc->GetModuleInfo(nModuleID);
	pModule->SetState(prevState);	



// 	ShowEmgDlg *pEmgDlg;
// 
// 
// 	pEmgDlg =  new ShowEmgDlg(this);
// 	ASSERT(pEmgDlg);
// 	pEmgDlg->Create(IDD_SHOW_EMG_DLG, this);
// 	pEmgDlg->SetModuleID(nModuleID);
// 	pEmgDlg->SetEmgData(::GetModuleName(nModuleID), "2, AC power fail detected.", 0);
// 	pEmgDlg->ShowWindow(SW_SHOW);
	//m_apEmgDlg.Add(pEmgDlg);

}

void CAllSystemView::OnBnClickedButton9() //TEST VA
{
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();

	CFormModule *pModule = NULL;
	CCTSMonDoc *pDoc = GetDocument();

	CString strTemp;
	GetDlgItemText(IDC_EDIT_TEST, strTemp);
	int nModuleID= atoi(strTemp);

	EP_GP_STATE_DATA prevState;
	memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
	prevState.state = EP_STATE_IDLE;
	prevState.sensorState[1] = 0x00;


	pModule = pDoc->GetModuleInfo(nModuleID);
	pModule->SetState(prevState);	
}

void CAllSystemView::OnBnClickedButton5() //TEST RAEDY
{
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	CFormModule *pModule = NULL;
	CCTSMonDoc *pDoc = GetDocument();

	CString strTemp;
	GetDlgItemText(IDC_EDIT_TEST, strTemp);
	int nModuleID= atoi(strTemp);

	EP_GP_STATE_DATA prevState;
	memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
	prevState.state = EP_STATE_READY;

	pModule = pDoc->GetModuleInfo(nModuleID);
	pModule->SetState(prevState);	
}

void CAllSystemView::OnBnClickedButton10() // TRAY IN
{
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	CFormModule *pModule = NULL;
	CCTSMonDoc *pDoc = GetDocument();

	CString strTemp;
	GetDlgItemText(IDC_EDIT_TEST, strTemp);
	int nModuleID= atoi(strTemp);

	EP_GP_STATE_DATA prevState;
	memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
	prevState.state = EP_STATE_IDLE;
	prevState.sensorState[1] = 0x03;

	pModule = pDoc->GetModuleInfo(nModuleID);
	pModule->SetState(prevState);	
}

void CAllSystemView::OnBnClickedButton8() //CAILI
{
	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	CFormModule *pModule = NULL;
	CCTSMonDoc *pDoc = GetDocument();

	CString strTemp;
	GetDlgItemText(IDC_EDIT_TEST, strTemp);
	int nModuleID= atoi(strTemp);

	EP_GP_STATE_DATA prevState;
	memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
	prevState.state = EP_STATE_CALIBRATION;

	pModule = pDoc->GetModuleInfo(nModuleID);
	pModule->SetState(prevState);	
}

// 20201225 KSCHOI Open TemperatureData Folder When TemperatureButton DoubleClicked START
void CAllSystemView::OnBnDoubleclickedBtnTemperature()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ShellExecute(NULL, "open", "explorer.exe",  _T("C:\\Program Files (x86)\\PNE CTS\\TemperatureData"), NULL, SW_SHOW);
}
// 20201225 KSCHOI Open TemperatureData Folder When TemperatureButton DoubleClicked END

// 20210201 KSCHOI Add Command Button Log START
void CAllSystemView::WriteButtonLog(CWordArray& awSelModule, CString csButtonName)
{
	CCTSMonDoc *pDoc = GetDocument();

	if(pDoc == NULL) return;

	CString csButtonLog = "[ButtonLog "+csButtonName+"] ";
	CString csButtonLogTemp;
	for(int i = 0; i < awSelModule.GetSize(); i++)
	{
		csButtonLogTemp.Format("%s, ", GetModuleName(awSelModule.GetAt(i), 0));
		csButtonLog += csButtonLogTemp;
	}

	pDoc->WriteLog(csButtonLog);
}
// 20210201 KSCHOI Add Command Button Log END

void CAllSystemView::OnJigHeightModeHigh() 
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+ TEXT_LANG[43]); //" [Change to Control]"
		return ;
	}

	if(PasswordCheck() == FALSE)
		return;

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format(TEXT_LANG[92], nSelNo);
	if(MessageBox(strTemp, "JigHeightMode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;

	pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");		
	for (int i = 0; i<nSelNo; i++)
	{
		mID = awSelModule.GetAt(i);

		CFormModule* pModule = pDoc->GetModuleInfo(mID);

		state = EPGetGroupState(mID, 0);
		if( state != EP_STATE_LINE_OFF )
		{
			if(state == EP_STATE_IDLE || state ==EP_STATE_STANDBY )
			{
				if(pModule->GetJigHeightMode() != JIG_HEIGHT_HIGH)
				{
					if(::EPSetLineMode(mID, 0, EP_JIG_HEIGHT_MODE, JIG_HEIGHT_HIGH) == false )
					{
						strTemp.Format("[Module %s] Jig Height Mode Change Failed.",  ::GetModuleName(mID));
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
						strFailMD += strTemp;
					}

					DrawModule(mID, FALSE);
				}
			}
			else
			{
				strTemp.Format("[Module %s] State Mismatch. State : %d",  ::GetModuleName(mID), state);
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				strFailMD += strTemp;
			}
		}

		pDoc->SetProgressPos( i+1 );
	}

	pDoc->HideProgressWnd();

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

	WriteButtonLog(awSelModule, "AllSystemView Jig Height Mode Change : HIGH");
}

void CAllSystemView::OnJigHeightModeMiddle() 
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+ TEXT_LANG[43]); //" [Change to Control]"
		return ;
	}

	if(PasswordCheck() == FALSE)
		return;

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}

	strTemp.Format(TEXT_LANG[92], nSelNo);
	if(MessageBox(strTemp, "JigHeightMode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;

	pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");		
	for (int i = 0; i<nSelNo; i++)
	{
		mID = awSelModule.GetAt(i);

		CFormModule* pModule = pDoc->GetModuleInfo(mID);

		state = EPGetGroupState(mID, 0);
		if( state != EP_STATE_LINE_OFF )
		{
			if(state == EP_STATE_IDLE || state ==EP_STATE_STANDBY )
			{
				if(pModule->GetJigHeightMode() != JIG_HEIGHT_MIDDLE)
				{
					if(::EPSetLineMode(mID, 0, EP_JIG_HEIGHT_MODE, JIG_HEIGHT_MIDDLE) == false )
					{
						strTemp.Format("[Module %s] Jig Height Mode Change Failed.",  ::GetModuleName(mID));
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
						strFailMD += strTemp;
					}

					DrawModule(mID, FALSE);
				}
			}
			else
			{
				strTemp.Format("[Module %s] State Mismatch. State : %d",  ::GetModuleName(mID), state);
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				strFailMD += strTemp;
			}
		}

		pDoc->SetProgressPos( i+1 );
	}

	pDoc->HideProgressWnd();

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

	WriteButtonLog(awSelModule, "AllSystemView Jig Height Mode Change : MIDDLE");
}

void CAllSystemView::OnJigHeightModeLow() 
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[56]+ TEXT_LANG[43]); //" [Change to Control]"
		return ;
	}

	if(PasswordCheck() == FALSE)
		return;

	CCTSMonDoc *pDoc = GetDocument();
	int nGroupNo, nSelNo;

	CWordArray awSelModule;
	GetSelectedModuleList(awSelModule);
	nSelNo = awSelModule.GetSize();

	CString strTemp;
	if(nSelNo <=0)
	{
		strTemp.Format(TEXT_LANG[57], pDoc->m_strModuleName);
		AfxMessageBox(strTemp);
		return;
	}
	
	strTemp.Format(TEXT_LANG[92], nSelNo);
	if(MessageBox(strTemp, "JigHeightMode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	int mID;
	WORD state;
	CString strFailMD;

	pDoc->SetProgressWnd(0, nSelNo, "Sending initialization to module...");		
	for (int i = 0; i<nSelNo; i++)
	{
		mID = awSelModule.GetAt(i);

		CFormModule* pModule = pDoc->GetModuleInfo(mID);

		state = EPGetGroupState(mID, 0);
		if( state != EP_STATE_LINE_OFF )
		{
			if(state == EP_STATE_IDLE || state ==EP_STATE_STANDBY )
			{
				if(pModule->GetJigHeightMode() != JIG_HEIGHT_LOW)
				{
					if(::EPSetLineMode(mID, 0, EP_JIG_HEIGHT_MODE, JIG_HEIGHT_LOW) == false )
					{
						strTemp.Format("[Module %s] Jig Height Mode Change Failed.",  ::GetModuleName(mID));
						pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
						strFailMD += strTemp;
					}

					DrawModule(mID, FALSE);
				}
			}
			else
			{
				strTemp.Format("[Module %s] State Mismatch. State : %d",  ::GetModuleName(mID), state);
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				strFailMD += strTemp;
			}
		}

		pDoc->SetProgressPos( i+1 );
	}

	pDoc->HideProgressWnd();

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[5], strFailMD);//TEXT - %s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

	WriteButtonLog(awSelModule, "AllSystemView Jig Height Mode Change : LOW");
}