// GradingResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CTSMonDoc.h"
#include "GradingResultDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGradingResultDlg dialog


CGradingResultDlg::CGradingResultDlg(CCTSMonDoc *pDoc, 
									 CFormResultFile &rConditionMain,									 
									 CWnd* pParent /*=NULL*/)
	: CDialog(CGradingResultDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGradingResultDlg)
	m_nDisplayMode = 0;
	m_nDirect = 0;
	//}}AFX_DATA_INIT

	//m_pConditionMain = pConditionMain;
	//m_aStepData = aStepData;
	m_ppResultData = NULL;
	m_pStepType = NULL;
	m_pTopColorFlag = NULL;
	m_nStepNum = 0;
	m_nChNum = 0;
	m_pDoc = pDoc;

	m_bExpand = false;

	m_aStepData = new CPtrArray;
	STR_STEP_RESULT* pStepResult = NULL;
	m_pConditionMain = rConditionMain.GetConditionMain();
	int nStepNum = rConditionMain.GetStepSize();
	for(int nI = 0; nI < nStepNum; nI++)
	{
		pStepResult = rConditionMain.GetStepData(nI);
		m_aStepData->Add(&pStepResult->stepData);
	}

	m_strFileName = rConditionMain.GetFileName();
}


void CGradingResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGradingResultDlg)
	DDX_Control(pDX, IDC_STEP_NO, m_wndStepNo);
	DDX_Control(pDX, IDC_GRID_SETTING, m_wndGridSetting);
	DDX_Radio(pDX, IDC_MODE_AND, m_nDisplayMode);
	DDX_Radio(pDX, IDC_DIRECT_L2R, m_nDirect);

	for(int i=0;i<BACK_COLOR_NUM;i++)
		DDX_Text(pDX,IDC_EDIT1+i, m_strGradeCode[i]);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGradingResultDlg, CDialog)
	//{{AFX_MSG_MAP(CGradingResultDlg)
	ON_CBN_SELCHANGE(IDC_STEP_NO, OnSelchangeStepNo)
	ON_CBN_SELCHANGE(IDC_GRID_SETTING, OnSelchangeGridSetting)
	ON_BN_CLICKED(IDC_MODE_AND, OnModeAnd)
	ON_BN_CLICKED(IDC_MODE_OR, OnModeOr)
	ON_BN_CLICKED(IDC_DIRECT_L2R, OnDirectL2R)
	ON_BN_CLICKED(IDC_DIRECT_R2L, OnDirectR2L)
	ON_BN_CLICKED(IDC_BTN_EXPAND, OnBtnExpand)
	ON_BN_CLICKED(IDC_BTN_UPDATE, OnBtnUpdate)
	ON_BN_CLICKED(IDC_BTN_PRINT_RESULT, OnPrintResult)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGradingResultDlg message handlers

BOOL CGradingResultDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	UpdateData(FALSE);

	{//Get Line Boundary Region
		GetWindowRect(m_rectTotal);
		GetDlgItem(IDC_LINE)->GetWindowRect(m_rectLine);
		MoveWindow(m_rectTotal.left, m_rectTotal.top, m_rectLine.right, m_rectTotal.bottom);
	}

	return LoadResultData();	
}

bool CGradingResultDlg::LoadResultData()
{
	if( m_pConditionMain == NULL || m_aStepData->GetSize() == 0 )
	{
		return false;
	}

	SetWindowText(m_strFileName);
	
	m_wndModuleGrid.SubclassDlgItem(IDC_CUSTOM1, this);	
	m_wndModuleGrid.Initialize();
	BOOL bLock = m_wndModuleGrid.LockUpdate();

	m_wndModuleGrid.EnableGridToolTips();
	m_wndModuleGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_wndModuleGrid.m_bCustomWidth = TRUE;

	int nRowCount = 32;
	int nColCount = 8;
	m_wndModuleGrid.SetRowCount(nRowCount);
	m_wndModuleGrid.SetColCount(nColCount+1);

	CRect rect;
	m_wndModuleGrid.GetClientRect(rect);
	m_wndModuleGrid.SetRowHeight(0, nRowCount, 45);
	for(int x=0;x<9;x++)
	{
		m_wndModuleGrid.SetColWidth(x,x,(rect.Width()/9)+7);
	}
	m_wndModuleGrid.SetColWidth(9,9,0);
	
	m_wndModuleGrid.m_bCustomColor 	= TRUE;
	
	if( m_pTopColorFlag == NULL )
		m_pTopColorFlag = new char[nRowCount*nColCount];

	memset(m_pTopColorFlag, 0, nRowCount*nColCount);	
	m_wndModuleGrid.m_pCustomColorFlag = m_pTopColorFlag;

	m_wndModuleGrid.m_CustomColorRange	= CGXRange(1, 1, nRowCount, nColCount);
	m_wndModuleGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	char szLabel[10];
	m_wndModuleGrid.SetValueRange(CGXRange(0,0), "No");
	for( int nI = 0;  nI < nColCount; nI++ )
	{
		::ZeroMemory(szLabel, 10);
		sprintf(szLabel, "%d", nI+1);
		m_wndModuleGrid.SetValueRange(CGXRange(0,nI+1), szLabel);
	}
	for( nI = 0; nI < nRowCount; nI++ )
	{
		sprintf(szLabel, "Tray%d-Ch%02d", (nI/8)+1, ((nI*8)+1)%64);
		m_wndModuleGrid.SetValueRange(CGXRange(nI+1,0), szLabel);
	}

	{//경계선 표시
		int nWidth = 3;
		COLORREF color = RGB(255,0,0);
		int nFromCh, nToCh;
		ROWCOL	nFromRow, nFromCol, nToRow, nToCol;
		for(nI = 0; nI < 4; nI++)
		{
			switch(nI){
			case 0:nFromCh=1;nToCh=64;break;
			case 1:nFromCh=65;nToCh=128;break;
			case 2:nFromCh=129;nToCh=192;break;
			case 3:nFromCh=193;nToCh=256;break;
			}
			nFromRow = (nFromCh-1) / nColCount + 1;
			nFromCol = (nFromCh-1) % nColCount + 1;
			nToRow = (nToCh - 1) / nColCount + 1;
			nToCol = (nToCh - 1) % nColCount + 1;

			m_wndModuleGrid.SetStyleRange(CGXRange(nFromRow, 
												   nFromCol, 
												   nFromRow, 
												   nColCount), 
										  CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(nWidth).SetColor(color)));
			m_wndModuleGrid.SetStyleRange(CGXRange(nFromRow, 
												   1, 
												   nToRow, 
												   1), 
										  CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(nWidth).SetColor(color)));
			m_wndModuleGrid.SetStyleRange(CGXRange(nToRow, 
												   1, 
												   nToRow, 
												   nToCol), 
										  CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(nWidth).SetColor(color)));
			m_wndModuleGrid.SetStyleRange(CGXRange(nToRow, nToCol), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(nWidth).SetColor(color)));
			m_wndModuleGrid.SetStyleRange(CGXRange(nFromRow, 
												   nColCount, 
												   nToRow-1, 
												   nColCount), 
										  CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(nWidth).SetColor(color)));
		}
	}

	m_wndModuleGrid.LockUpdate(bLock);
	m_wndModuleGrid.Redraw();

	LPEP_STEP_HEADER pStepHeader;
	LPVOID lpBuffer = NULL;
	CString strMsg;

	m_wndStepNo.ResetContent();

	if( m_pStepType )
	{
		delete [] m_pStepType;
	}

	int nStepNum = m_aStepData->GetSize();
	//int nStepNum = m_pConditionMain->apStepList.GetSize();
	m_pStepType = new BYTE[nStepNum];
	for( nI =0; nI < nStepNum; nI++)
	{
		pStepHeader = (LPEP_STEP_HEADER)m_pConditionMain->apStepList.GetAt(nI);
		m_pStepType[nI] = pStepHeader->type;
		strMsg = _T("");
		switch(pStepHeader->type)
		{
			case EP_TYPE_CHARGE	:
				strMsg.Format("Step %d::Charge", nI+1);
				break;
			case EP_TYPE_DISCHARGE	:
				strMsg.Format("Step %d::Discharge", nI+1);
				break;
			case EP_TYPE_REST	:
				strMsg.Format("Step %d::Rest", nI+1);
				break;
			case EP_TYPE_OCV	:
				strMsg.Format("Step %d::OCV", nI+1);
				break;
			case EP_TYPE_END	:
				strMsg.Format("Step %d::End", nI+1);
				break;
/*			default:
				strMsg.Format("Step %d::Unknown", nI+1);
				break;
*/		}
		if(strMsg.IsEmpty() == FALSE )
			m_wndStepNo.AddString(strMsg);
	}
//	strMsg.Format("Grading Result");
//	m_wndStepNo.AddString(strMsg);

	LPEP_GROUP_INFO	pStepData;
	STR_SAVE_CH_DATA *lpChData;
	nStepNum = m_aStepData->GetSize();

	if( m_ppResultData )
	{
		for( int nI = 0; nI < m_nStepNum; nI++ )
			delete [] m_ppResultData[nI];

		delete [] m_ppResultData;
	}
	m_nStepNum = nStepNum;
	m_ppResultData = new BYTE* [m_nStepNum];

	int nChNum;
	for( nI = 0; nI < nStepNum; nI++ )
	{
		pStepData = (LPEP_GROUP_INFO)m_aStepData->GetAt(nI);
		ASSERT(pStepData);

		nChNum = pStepData->chData.GetSize();
		if( nI == 0 )
			m_nChNum = nChNum;

		m_ppResultData[nI] = new BYTE [nChNum];
		ZeroMemory(m_ppResultData[nI], nChNum);
		
		//
		if( pStepData->chData.GetSize() <= 0 )
			continue;

		// End Step은 데이터 남기지 않음
		if( m_pStepType[nI] == EP_TYPE_END )
			continue;

		//
		bool bGrading=false;
		{
			EP_GRADE_HEADER* pGradeHeader=NULL;
			for(int nTmpIndex=0; nTmpIndex<m_pConditionMain->apGradeList.GetSize();nTmpIndex++)
			{
				pGradeHeader=(EP_GRADE_HEADER*)m_pConditionMain->apGradeList.GetAt(nTmpIndex);
				if(pGradeHeader->stepIndex==nI)
				{
					//Grading을 실시한 스텝
					bGrading=true;
					break;
				}
			}
			if(bGrading==false)	
			{
				::memset(m_ppResultData[nI], 0xFF, sizeof(BYTE)*nChNum);
				continue;
			}
		}

		for( int nChIndex = 0; nChIndex < nChNum; nChIndex++ )
		{
			lpChData = (LPSTR_SAVE_CH_DATA)pStepData->chData.GetAt(nChIndex);
			ASSERT(lpChData);

			if( lpChData->wChIndex >= nChNum || lpChData->wChIndex < 0 )
				continue;
			
			m_ppResultData[nI][lpChData->wChIndex] = lpChData->grade;
		}
	}

	m_nDisplayMode = 0;

	InitColorConfig();

	m_wndStepNo.SetCurSel(m_nStepNum-1);
	Display(m_nStepNum);
	return true;
}

void CGradingResultDlg::Display(int nStepNum, int nDisplayMode)
{
	CString strNoGradeText, strNoGradeSymbol, strGradeFailText;
	{
		strNoGradeText=AfxGetApp()->GetProfileString(SECTION, "NoGradeText");
		if(strNoGradeText.IsEmpty())
		{
			strNoGradeText="No Grade";
			AfxGetApp()->WriteProfileString(SECTION, "NoGradeText", strNoGradeText);
		}
		strNoGradeSymbol=AfxGetApp()->GetProfileString(SECTION, "NoGradeSymbol");
		if(strNoGradeSymbol.IsEmpty())
		{
			strNoGradeSymbol="♥";
			AfxGetApp()->WriteProfileString(SECTION, "NoGradeSymbol", strNoGradeSymbol);
		}
		strGradeFailText=AfxGetApp()->GetProfileString(SECTION, "GradeFailText");
		if(strGradeFailText.IsEmpty())
		{
			strGradeFailText="Fail";
			AfxGetApp()->WriteProfileString(SECTION, "GradeFailText", strGradeFailText);
		}
	}
	if( m_pConditionMain == NULL || m_aStepData->GetSize() == 0 )
		return;

	char szLabel[60];
	int nRowCount = 32;		
	int nColCount = 8;
	int nRow, nCol;

	m_wndModuleGrid.m_pCustomColorFlag = (char*)m_pTopColorFlag;
  	m_wndModuleGrid.m_CustomColorRange	= CGXRange(1, 1, nRowCount, nColCount);
	CString strToolTip, strDataMsg, strTemp;
	int nColorIndex = 0;
	int nStepIndex = nStepNum - 1;

	if( m_pStepType[nStepIndex] == EP_TYPE_END )
	{
		LPEP_GROUP_INFO	pStepData;
		STR_SAVE_CH_DATA *lpChData;

		CString strTmp, strCode;
		bool bFoundFault = false;
		for( int nI = 0; nI < m_nChNum; nI++ )
		{
			strDataMsg = "";
			nRow = nI / nColCount + 1;
			nCol = nI % nColCount + 1;
			nColorIndex = ((nI/nColCount)*nColCount)+(nI%nColCount);

			strCode = "";
			bFoundFault = false;
			for(int nX = 0; nX < m_nStepNum-1; nX++)
			{
				if(nX > 0)
					strCode += ", ";

				strTmp = CString(m_ppResultData[nX][nI]);
				if( m_ppResultData[nX][nI] == 0x00 )
				{
					bFoundFault = true;
					strTmp = strGradeFailText;
					strCode += strTmp;
					break;
				}
				else if( m_ppResultData[nX][nI] == 0xFF )
				{
					strTmp = strNoGradeSymbol;
					strCode += strTmp;
				}
				else
				{
					strCode += strTmp;
				}
			}
			
			m_pTopColorFlag[nColorIndex] = bFoundFault? BACK_COLOR_NUM : BACK_COLOR_NUM+2;

			ZeroMemory(szLabel, 60);
			sprintf(szLabel, "%s", strCode);
			m_wndModuleGrid.SetValueRange(CGXRange(nRow, nCol), szLabel);

			strToolTip.Format("Ch%03d - %s", nI+1, strCode);
			m_wndModuleGrid.SetStyleRange(CGXRange(nRow, nCol), 
				CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));
		}
	}
	else
	{
		CString strData, strCodeMsg;

		LPEP_GROUP_INFO	pStepData = (LPEP_GROUP_INFO)m_aStepData->GetAt(nStepIndex);
		STR_SAVE_CH_DATA *lpChData;
		for( int nI = 0; nI < m_nChNum; nI++ )
		{
			if( m_nDirect == 1 )
			{
				nRow = nI / nColCount + 1;
				nCol = nColCount - (nI % nColCount);
				nColorIndex = ((nI/nColCount)*nColCount)+(nColCount-(nI%nColCount)) - 1;
			}
			else
			{
				nRow = nI / nColCount + 1;
				nCol = nI % nColCount + 1;
				nColorIndex = ((nI/nColCount)*nColCount)+(nI%nColCount);
			}
			m_pTopColorFlag[nColorIndex] = GetColorIndex(m_ppResultData[nStepIndex][nI]);

			lpChData = (STR_SAVE_CH_DATA *)pStepData->chData[nI];
			strCodeMsg.Format("%s", m_pDoc->ChCodeMsg(lpChData->channelCode));

			ZeroMemory(szLabel, 60);
			if(m_ppResultData[nStepIndex][nI]==0x00)
			{
#ifdef _DEBUG
				CString strTmp = strCodeMsg.Mid(0,strCodeMsg.Find("("));
#else
				CString strTmp = strCodeMsg;
#endif
				sprintf(szLabel, "%s", strTmp);
			}
			else if(m_ppResultData[nStepIndex][nI]==0xFF)
			{
				sprintf(szLabel, "%s", strNoGradeText);
			}
			else
			{
				sprintf(szLabel, "%c", m_ppResultData[nStepIndex][nI]);
			}
			m_wndModuleGrid.SetValueRange(CGXRange(nRow, nCol), szLabel);
			
			strToolTip.Format("Ch%03d - 전압(%.4f) 전류(%.2f) 용량(%.2f) [%s]",
								nI+1, 
								lpChData->lVoltage/10000.0f,
								(lpChData->lCurrent<0)?
								lpChData->lCurrent/100.0f*-1:lpChData->lCurrent/100.0f,
								lpChData->lCapacity/100.0f,
								strCodeMsg);
/*
			switch( m_pStepType[nStepIndex] )
			{
			case EP_TYPE_DISCHARGE:
				strData.Format(" - Capacity (%.2f) [%s]", 
					(float)lpChData->lCapacity/100.0f, strCodeMsg);
				break;
			case EP_TYPE_CHARGE:
			case EP_TYPE_OCV:
				strData.Format(" - Voltage (%.4f) [%s]", 
					(float)lpChData->lVoltage/10000.0f, strCodeMsg);
				break;
			default:
				strData = "";
				break;
			}
			strToolTip.Format("Ch%03d%s", nI+1, strData);
*/			m_wndModuleGrid.SetStyleRange(CGXRange(nRow, nCol), 
				CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));
		}
	}
	m_wndModuleGrid.Redraw();
}

void CGradingResultDlg::OnSelchangeStepNo() 
{
	UpdateData();
	int nStepNo = m_wndStepNo.GetCurSel()+1;
	Display(nStepNo, m_nDisplayMode);	
}

void CGradingResultDlg::OnSelchangeGridSetting() 
{
	UpdateData();
	int nStepNo = m_wndStepNo.GetCurSel();
	if(nStepNo == 0)
		nStepNo = m_nStepNum;

	Display(nStepNo, m_nDisplayMode);	
}

void CGradingResultDlg::OnModeAnd() 
{
	UpdateData();
	int nStepNo = m_wndStepNo.GetCurSel();
	if(nStepNo == 0)
		nStepNo = m_nStepNum;

	Display(nStepNo, m_nDisplayMode);
}

void CGradingResultDlg::OnModeOr() 
{
	UpdateData();
	int nStepNo = m_wndStepNo.GetCurSel();
	if(nStepNo == 0)
		nStepNo = m_nStepNum;

	Display(nStepNo, m_nDisplayMode);	
}


void CGradingResultDlg::OnDirectL2R() 
{
	UpdateData();
	int nStepNo = m_wndStepNo.GetCurSel();
	if(nStepNo == 0)
		nStepNo = m_nStepNum;

	Display(nStepNo, m_nDisplayMode);	
}

void CGradingResultDlg::OnDirectR2L() 
{
	UpdateData();
	int nStepNo = m_wndStepNo.GetCurSel();
	if(nStepNo == 0)
		nStepNo = m_nStepNum;

	Display(nStepNo, m_nDisplayMode);		
}

BYTE CGradingResultDlg::GetColorIndex(BYTE byResult)
{
	if( byResult == 0x00 )
	{
		return BACK_COLOR_NUM; // Fault Color
	}
	else if( byResult == 0xFF )
	{
		return BACK_COLOR_NUM+2; //None Color
	}
	else
	{
		for(int nI = 0; nI < BACK_COLOR_NUM; nI++ )
		{
			if( !m_strGradeCode[nI].IsEmpty() && byResult == m_strGradeCode[nI].GetAt(0))
				return nI;
		}
		return BACK_COLOR_NUM+1; //Unknown Code (BLACK)
	}
}

BOOL CGradingResultDlg::DestroyWindow() 
{
	if( m_ppResultData )
	{
		for( int nI = 0; nI < m_nStepNum; nI++ )
			delete [] m_ppResultData[nI];

		delete [] m_ppResultData;
		m_ppResultData = NULL;
	}
	if( m_pStepType )
	{
		delete [] m_pStepType;
		m_pStepType = NULL;
	}
	if( m_pTopColorFlag )
	{
		delete [] m_pTopColorFlag;
		m_pTopColorFlag = NULL;
	}
	if( m_aStepData )
	{
		delete m_aStepData;
		m_aStepData = NULL;
	}
	
	return CDialog::DestroyWindow();
}

void CGradingResultDlg::InitColorConfig()
{
	char szEntry[10];
	
	UINT nSize = sizeof(COLORREF);
	CString strCode;
	COLORREF colorData;

	LPBYTE* pColorData=new LPBYTE[1];
	
	for (int i = 0; i < BACK_COLOR_NUM; i++)
	{
		sprintf(szEntry, "Code_%02d", i+1);
		strCode = AfxGetApp()->GetProfileString("Grade", szEntry);

		if( strCode.IsEmpty() )
		{
			strCode.Format("%c", 'A'+i);
			AfxGetApp()->WriteProfileString("Grade", szEntry, strCode);

			colorData = RGB(255,255,255);
			sprintf(szEntry, "Color_%02d", i+1);		
			AfxGetApp()->WriteProfileBinary("Grade", szEntry, (LPBYTE)&colorData, nSize);
		}
		else
		{
			sprintf(szEntry, "Color_%02d", i+1);			
			AfxGetApp()->GetProfileBinary("Grade", szEntry, pColorData, &nSize);
			if(nSize<=0)
			{
				colorData = RGB(255,255,255);
				sprintf(szEntry, "Color_%02d", i+1);		
				nSize = sizeof(COLORREF);
				AfxGetApp()->WriteProfileBinary("Grade", szEntry, (LPBYTE)&colorData, nSize);
				m_BStateColor[i] = colorData;
			}
			else
			{
				::CopyMemory(&colorData, pColorData[0], sizeof(COLORREF));
				m_BStateColor[i] = colorData;

				delete [] pColorData[0];
			}
		}
		m_strGradeCode[i]=strCode;
		m_BWellColor[i].AttachButton(IDC_COLOR1+i, this);		
	}

	{// Fault Color
		sprintf(szEntry, "Color_Fault");		
		AfxGetApp()->GetProfileBinary("Grade", szEntry, pColorData, &nSize);
		if(nSize<=0)
		{
			m_colorFault=RGB(255,0,0);
			sprintf(szEntry, "Color_Fault");	
			nSize = sizeof(COLORREF);
			AfxGetApp()->WriteProfileBinary("Grade", szEntry, (LPBYTE)&m_colorFault, nSize);
		}
		else
		{
			::CopyMemory(&m_colorFault, pColorData[0], sizeof(COLORREF));
			delete [] pColorData[0];
		}
		m_BWellFaultColor.AttachButton(IDC_COLOR_FAULT, this);
	}
	delete [] pColorData;

	DrawColor();
	SetColorArray();
}

void CGradingResultDlg::DrawColor()
{
	for (int i = 0; i < BACK_COLOR_NUM; i++)
	{
		m_BWellColor[i].SetColor(m_BStateColor[i]);
	}
	m_BWellFaultColor.SetColor(m_colorFault);

	UpdateData(FALSE);
}

void CGradingResultDlg::OnBtnExpand()
{
	if( m_bExpand == true )
	{
		GetDlgItem(IDC_BTN_EXPAND)->SetWindowText("☞");
		m_bExpand = false;
		MoveWindow(m_rectTotal.left, m_rectTotal.top, m_rectLine.right, m_rectTotal.bottom);
	}
	else
	{
		GetDlgItem(IDC_BTN_EXPAND)->SetWindowText("☜");
		m_bExpand = true;
		MoveWindow(m_rectTotal.left, m_rectTotal.top, m_rectTotal.right, m_rectTotal.bottom);
	}
}

void CGradingResultDlg::OnBtnUpdate()
{
	UpdateData();

	CString strEntry;
	COLORREF colorData;
	for (int i = 0; i < BACK_COLOR_NUM; i++)
	{		
		strEntry.Format("Code_%02d", i+1);
		AfxGetApp()->WriteProfileString("Grade", strEntry, m_strGradeCode[i]);

		m_BStateColor[i] = m_BWellColor[i].GetColor();
		colorData = m_BStateColor[i];
		strEntry.Format("Color_%02d", i+1);		
		AfxGetApp()->WriteProfileBinary("Grade", strEntry, (LPBYTE)&colorData, sizeof(COLORREF));
	}

	{//Fault Background Color
		m_colorFault = m_BWellFaultColor.GetColor();
		strEntry.Format("Color_Fault");		
		AfxGetApp()->WriteProfileBinary("Grade", strEntry, (LPBYTE)&m_colorFault, sizeof(COLORREF));
	}
	SetColorArray();
	Display(m_wndStepNo.GetCurSel()+1);
}

void CGradingResultDlg::SetColorArray()
{
	for (int nI = 0; nI < BACK_COLOR_NUM+1; nI++)
	{
		m_wndModuleGrid.m_ColorArray[nI].BackColor = m_BStateColor[nI];
		m_wndModuleGrid.m_ColorArray[nI].TextColor = RGB(0,0,0);
	}
	m_wndModuleGrid.m_ColorArray[BACK_COLOR_NUM].BackColor = m_colorFault;
	m_wndModuleGrid.m_ColorArray[BACK_COLOR_NUM].TextColor = RGB(0,0,0);

	m_wndModuleGrid.m_ColorArray[BACK_COLOR_NUM+1].BackColor = RGB(0,0,0);
	m_wndModuleGrid.m_ColorArray[BACK_COLOR_NUM+1].TextColor = RGB(255,255,255);

	m_wndModuleGrid.m_ColorArray[BACK_COLOR_NUM+2].BackColor = RGB(255,255,255);
	m_wndModuleGrid.m_ColorArray[BACK_COLOR_NUM+2].TextColor = RGB(0,0,255);
}

/////////////////////////////////////////////////////////////////////////////////

void CGradingResultDlg::OnPrintResult() 
{
	CDC dc;
    CPrintDialog printDlg(FALSE);

    if (printDlg.DoModal() == IDCANCEL)         // Get printer settings from user
        return;

    dc.Attach(printDlg.GetPrinterDC());         // Attach a printer DC
    dc.m_bPrinting = FALSE;

    CString strTitle;                           // Get the application title
	if(dc.m_hDC == NULL)
	{
		strTitle.Format("Device %s not support.", printDlg.GetPortName());
		AfxMessageBox(strTitle, MB_ICONSTOP|MB_OK);
		return;
	}
    strTitle.LoadString(AFX_IDS_APP_TITLE);

    DOCINFO di;                                 // Initialise print document details
    ::ZeroMemory (&di, sizeof (DOCINFO));
    di.cbSize = sizeof (DOCINFO);
    di.lpszDocName = strTitle;
	

    BOOL bPrintingOK = dc.StartDoc(&di);        // Begin a new print job

    // Get the printing extents and store in the m_rectDraw field of a 
    // CPrintInfo object
    CPrintInfo Info;
    Info.m_rectDraw.SetRect(0,0, 
                            dc.GetDeviceCaps(HORZRES), 
                         dc.GetDeviceCaps(VERTRES));
   	Info.SetMaxPage(0xffff);

	Info.m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	Info.m_pPD->m_pd.hInstance = AfxGetInstanceHandle();	
	
	
	CFont font;
	font.CreateFont(170, 80, 0, 0, FW_NORMAL, 0, 0, 0, 
		DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_CHARACTER_PRECIS, 
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Tahoma");
	//font.CreateFont(75, 45, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, 
	//	HANGUL_CHARSET, 0, 0, 0, VARIABLE_PITCH, "굴림");
	dc.SelectObject(&font);

    OnBeginPrinting(&dc, &Info);                // Call your "Init printing" funtion
    for (UINT page = Info.GetMinPage(); 
         page <= Info.GetMaxPage() && bPrintingOK; 
         page++)
    {
        dc.StartPage();                         // begin new page
        Info.m_nCurPage = page;
        OnPrint(&dc, &Info);                    // Call your "Print page" function
        bPrintingOK = (dc.EndPage() > 0);       // end page
    }
    OnEndPrinting(&dc, &Info);                  // Call your "Clean up" funtion

    if (bPrintingOK)
        dc.EndDoc();                            // end a print job
    else
        dc.AbortDoc();                          // abort job.

    dc.Detach();              	
}

void CGradingResultDlg::OnBeginPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	int nXMargin = 600;
	int nYMargin = 600;
	int nTextHeight = 180;
	int nLineCount = 0;
	CString strTemp;
	CRect strRect;
	
	strRect.SetRect(nXMargin, 
					nYMargin,
					10000, 
					nYMargin + nTextHeight);
	strTemp.Format("File Name: %s", m_strFileName);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

/*	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("Tray No: %s", m_pConditionMain->testHeader.);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("Lot No: %s", m_strLotNo);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);
*/

/*
	nLineCount = nLineCount+2;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("투입수량:  %d", EP_MAX_CH_PER_MD - m_nNonCellCount);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("양품(A~D):  %d", m_nNormalCount);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("OCV1 불량(S):  %d", m_cellStateCount[EP_SELECT_CODE_DELTA_OCV1]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("DeltaOCV(1-2)(E):  %d", m_cellStateCount[EP_SELECT_CODE_DELTA_OCV1]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("방전용량불량(F):  %d", m_cellStateCount[EP_SELECT_CODE_DISCHARGE_CAP]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("임피던스불량(I):  %d", m_cellStateCount[EP_SELECT_CODE_IMPEDANCE]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("OCV4불량(G):  %d", m_cellStateCount[EP_SELECT_CODE_OCV_FAIL4]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("DeltaOCV(3-4)(E1):  %d", m_cellStateCount[EP_SELECT_CODE_DELTA_OCV2]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);

	nLineCount++;
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	strTemp.Format("안전불량(N):  %d", m_cellStateCount[EP_SELECT_CODE_SAFETY_FAULT]);
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);
*/
	m_wndModuleGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);
	m_wndModuleGrid.GetParam()->GetProperties()->SetMargins(50,40,20,10);
	m_wndModuleGrid.SetColWidth(1, m_wndModuleGrid.GetColCount()-1, 70);
	m_wndModuleGrid.SetRowHeight(1, m_wndModuleGrid.GetRowCount(), 25);
	m_wndModuleGrid.OnGridBeginPrinting(pDC, pInfo);

}

void CGradingResultDlg::OnEndPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	m_wndModuleGrid.OnGridEndPrinting(pDC, pInfo);
	
}

void CGradingResultDlg::OnPrint(CDC *pDC, CPrintInfo *pInfo)
{
	m_wndModuleGrid.OnGridPrint(pDC, pInfo);
}
