// PasswordChkDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "PasswordChkDlg.h"


// CPasswordChkDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPasswordChkDlg, CDialog)

CPasswordChkDlg::CPasswordChkDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPasswordChkDlg::IDD, pParent)
	, m_strPassword(_T(""))
	, m_strNewPassword(_T(""))
{
	LanguageinitMonConfig(_T("CPasswordChkDlg"));
	m_bPasswordChagneChk = FALSE;
	m_bDiffPassword= false;
	m_strDiffPassword ="";
	m_strTitle = "";
}

CPasswordChkDlg::~CPasswordChkDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CPasswordChkDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CPasswordChkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PASSWORD, m_strPassword);
	DDX_Text(pDX, IDC_NEW_PASSWORD, m_strNewPassword);
}


BEGIN_MESSAGE_MAP(CPasswordChkDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPasswordChkDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_PASSWORD_CHANGE, &CPasswordChkDlg::OnBnClickedBtnPasswordChange)
END_MESSAGE_MAP()


// CPasswordChkDlg 메시지 처리기입니다.

void CPasswordChkDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_bPasswordChagneChk == TRUE )
	{
		UpdateData(TRUE);
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION, "Password", m_strNewPassword );
	}
	else
	{
		if( PasswordCheck() == FALSE )
		{
			AfxMessageBox(TEXT_LANG[0], MB_ICONINFORMATION);
			return;
		}
	}

	OnOK();
}

BOOL CPasswordChkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitFont();

	GetDlgItem(IDC_NEW_PASSWORD)->EnableWindow(FALSE);

	//20210105ksj
	if(m_bDiffPassword == TRUE)
	{
		SetWindowText(m_strTitle);
		GetDlgItem(IDC_BTN_PASSWORD_CHANGE)->EnableWindow(FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPasswordChkDlg::OnBnClickedBtnPasswordChange()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( PasswordCheck() == TRUE )
	{		
		GetDlgItem(IDOK)->SetWindowText("CONFIRM");

		GetDlgItem(IDC_BTN_PASSWORD_CHANGE)->EnableWindow(FALSE);
		GetDlgItem(IDC_PASSWORD)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEW_PASSWORD)->EnableWindow(TRUE);

		m_bPasswordChagneChk = TRUE;
	}
	else
	{
		AfxMessageBox(TEXT_LANG[0], MB_ICONINFORMATION );
	}
}

BOOL CPasswordChkDlg::PasswordCheck()
{
	UpdateData(TRUE);

	if(m_bDiffPassword == TRUE)
	{
		if( m_strPassword != m_strDiffPassword )
		{
			return FALSE;
		}
	}
	else
	{
		CString strTempPassword = _T("");
		strTempPassword = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "Password", "");

		if( m_strPassword != strTempPassword )
		{
			return FALSE;
		}
	}

	return TRUE;
}

void CPasswordChkDlg::InitFont()
{
	LOGFONT	LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 18;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_BTN_PASSWORD_CHANGE)->SetFont(&font);
	GetDlgItem(IDC_PASSWORD)->SetFont(&font);
	GetDlgItem(IDC_NEW_PASSWORD)->SetFont(&font);
}