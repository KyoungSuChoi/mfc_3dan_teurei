// CellVoltageError.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "CellVoltageError.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CellVoltageError dialog


CellVoltageError::CellVoltageError(CWnd* pParent, CString strUpErr, CString strDownErr)
	: CDialog(CellVoltageError::IDD, pParent)
{
	//{{AFX_DATA_INIT(CellVoltageError)
	m_strDownErrorChNum = _T("");
	m_strUpErrorChNum = _T("");
	//}}AFX_DATA_INIT
	m_strUpErr = strUpErr;	
	m_strDownErr = strDownErr;
	LanguageinitMonConfig(_T("CellVoltageError"));
}
CellVoltageError::~CellVoltageError(){
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CellVoltageError::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}


void CellVoltageError::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CellVoltageError)
	DDX_Control(pDX, IDC_LABEL_ERROR_UNITID, m_Label_ErrorUnitID);
	DDX_Text(pDX, IDC_EDIT_DOWNERROR_CHNUM, m_strDownErrorChNum);
	DDX_Text(pDX, IDC_EDIT_UPERROR_CHNUM, m_strUpErrorChNum);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CellVoltageError, CDialog)
	//{{AFX_MSG_MAP(CellVoltageError)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CellVoltageError message handlers

BOOL CellVoltageError::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here	
	InitLabel();
	ParsingErrMessage();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

VOID CellVoltageError::ParsingErrMessage()
{
	CString strData;
	CString strTemp;
	CString strTemp1;
	CString strUnitNum;
	int nCnt = GetFindCharCount( m_strUpErr, ',' );
	AfxExtractSubString(strUnitNum, m_strUpErr, 0,',');		// UnitID
	strData.Format(TEXT_LANG[0], strUnitNum);//"UNIT %s 채널 전압 경고"
	m_Label_ErrorUnitID.SetText(strData);
	int i = 1;
	
	strData = _T("");
	for( i=1; i<nCnt; i++ )
	{
		AfxExtractSubString(strTemp, m_strUpErr,i,',');
		if( i%10 == 0 )
		{
			strTemp1.Format("_%s_\r\n", strTemp);
		}
		else
		{
			strTemp1.Format("_%s_", strTemp);
		}	
		strData = strData + strTemp1;		
	}
	m_strUpErrorChNum.Format("%s", strData );

	nCnt = GetFindCharCount( m_strDownErr, ',' );
	if( strUnitNum.IsEmpty() )
	{
		AfxExtractSubString(strUnitNum, m_strDownErr, 0,',');		// UnitID
		strData.Format(TEXT_LANG[0], strUnitNum);//"UNIT %s 채널 전압 경고"
		m_Label_ErrorUnitID.SetText(strData);	
	}
	
	strData = _T("");
	for( i=1; i<nCnt; i++ )
	{
		AfxExtractSubString(strTemp, m_strDownErr,i,',');
		if( i%10 == 0 )
		{
			strTemp1.Format("_%s_\r\n", strTemp);
		}
		else
		{
			strTemp1.Format("_%s_", strTemp);
		}	
		strData = strData + strTemp1;		
	}
	m_strDownErrorChNum.Format("%s", strData );
	UpdateData(FALSE);
}

VOID CellVoltageError::InitLabel()
{
	m_Label_ErrorUnitID.SetBkColor(0);
	m_Label_ErrorUnitID.SetTextColor(RGB(255, 50, 50));
	m_Label_ErrorUnitID.SetFontSize(30);
	m_Label_ErrorUnitID.SetFontBold(TRUE);
	m_Label_ErrorUnitID.SetText(TEXT_LANG[1]);//"UNIT ID"
}

VOID CellVoltageError::OnOK() 
{
	// TODO: Add extra validation here	
	OnDestroy();
	CDialog::OnOK();
}

int CellVoltageError::GetFindCharCount(CString pString, char pChar) 
{ 
	int length = pString.GetLength();
	int find_count = 0; 
	
	for(int i = 0; i < length; i++)
	{ 
		if(pString[i] == pChar) 
			find_count++; 
	} 	
	return find_count; 
} 

void CellVoltageError::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;
	CDialog::PostNcDestroy();
}