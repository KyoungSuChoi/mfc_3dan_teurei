#pragma once
#include "afxwin.h"
#include "CTSMonDoc.h"
#include "NormalSensorMapDlg.h"
#include "colorbutton2.h"

// CManualControlView 폼 뷰입니다.

#define MAX_AUTO_SEQUENCE	100
#define MAX_WRITE_ADDR_CNT	48

#define JIG_IO_BYTE_START_INDEX	0
#define JIG_IO_BYTE_END_INDEX	8
#define JIG_IO_COUNT			(JIG_IO_BYTE_END_INDEX - JIG_IO_BYTE_START_INDEX + 1) * 8

#define MAX_WRITE_PORT		6
#define MAX_READ_PORT		10

#define IO_JC_UP				0
#define IO_JC_FRONT				1
#define IO_MAIN_SOL				2
#define IO_CLIP_OPEN			3
#define IO_STP_1				4
#define IO_STP_2				5
#define IO_STP_3				6
#define IO_STP_4				7
#define IO_JIG_FAN_ON_1			8
#define IO_JIG_FAN_ON_2			9
#define IO_JIG_FAN_ON_3			10
#define IO_JIG_FAN_ON_4			11
#define IO_EMG_LAMP				12
#define IO_CALI_TRAY_POWER		13

class CManualControlView : public CFormView
{
	DECLARE_DYNCREATE(CManualControlView)

protected:
	CManualControlView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CManualControlView();

public:
	enum { IDD = IDD_MANUAL_CONTROL_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:	
	CFont	m_font;
	CString *TEXT_LANG;
	bool LanguageinitMonConfig(CString strClassName);

	// Version Check
	CNormalSensorMapDlg *m_pNormalSensorMapDlg;

	int m_nCurModuleID;
	int		m_nLineMode;

	bool m_bReadySetCmd;
	unsigned short	m_nInputAddress[16];
	unsigned short	m_nOutputAddress[16];

	IO_Data m_SendJigIOData;
	IO_Data m_RecvJigIOData;
	IO_Data m_SendFanIOData;
	IO_Data m_RecvFanIOData;

	unsigned short m_nRecvJigIOData;
	unsigned short m_nRecvFanIOData;

	CString	m_strInputName[JIG_IO_COUNT];		
	CLedButton m_stateButton[JIG_IO_COUNT];

public:
	CCTSMonDoc* GetDocument();	
	void	WriteData();
	void	UpdateIoState(bool bForceUpdate = false);
	void	ClearIoState();
	int		Hex2Dec(CString hexData);
	void	ParsingConfig(CString strConfig);
	void	InitValue();
	void	InitLabel();
	void	InitFont();
	void	InitColorBtn();
	bool	LoadIoConfig();	

	void	SetBackgroundColor(COLORREF crBackground);
	CString GetTargetModuleName();
	void	ModuleConnected(int nModuleID);
	void	ModuleDisConnected(int nModuleID);
	void	SetCurrentModule(int nModuleID, int nGroupIndex = 0 );
	void	StopMonitoring();
	void	StartMonitoring();
	bool    UpdateGroupState(int nModuleID, int nGroupIndex=0);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	UINT m_nAutoIntervalTime;
	int	m_nDisplayTimer;

	CColorButton2 m_Btn_AutoUpdateStop;
	CColorButton2 m_Btn_ReStart;
	CColorButton2 m_Btn_Reset;
	CColorButton2 m_Btn_MaintenanceMode;
	CColorButton2 m_Btn_LocalMode;
	CColorButton2 m_BtnStopper1;
	CColorButton2 m_BtnStopper2;
	CColorButton2 m_BtnStopper3;
	CColorButton2 m_BtnStopper4;
	CColorButton2 m_BtnStopper5;
	CColorButton2 m_BtnStopper6;
	CColorButton2 m_Btn_BuzzerOff;
	CColorButton2 m_Btn_EmergencyStop;
	CColorButton2 m_Btn_FmsReset1;
	CColorButton2 m_Btn_FmsReset2;
	CColorButton2 m_Btn_FmsEndCommandSend;
	CColorButton2 m_Btn_FmsOutCommandSend;
	CColorButton2 m_Btn_Continue;
	CColorButton2 m_Btn_SafetyLocalMode;

	CColorButton2 m_OnBtnArray[32];
	CColorButton2 m_OffBtnArray[32];

	CLabel m_LabelViewName;
	CLabel m_CmdTarget;
	CLabel m_ctrlWorkMode;
	CLabel m_LabelLog;
	CLabel m_Label13;
	CLabel m_Label14;
	CLabel m_Label12;
	CLabel m_Label16;
	CLabel m_Label17;
	CLabel m_Label23;
	CLabel m_Label24;
	CLabel m_Label25;
	CLabel m_Label27;
	CLabel m_Label29;
	CLabel m_Label30;
	CLabel m_Label31;
	CLabel m_Label33;

	CLabel m_IOLabelArray[32];

	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);	
	afx_msg void OnBnClickedLocalModeBtn();
	afx_msg void OnBnClickedMaintenanceModeBtn();
	afx_msg void OnBnClickedBtnStopperSetting();
	afx_msg void OnBnClickedRestartBtn();
	afx_msg void OnBnClickedResetBtn();

	BOOL IsMainternanceMode();
	afx_msg void OnBtnClickedOn001();
	afx_msg void OnBtnClickedOn002();
	afx_msg void OnBtnClickedOn003();
	afx_msg void OnBtnClickedOn004();
	afx_msg void OnBtnClickedOn005();
	afx_msg void OnBtnClickedOn006();
	afx_msg void OnBtnClickedOn007();
	afx_msg void OnBtnClickedOn008();
	afx_msg void OnBtnClickedOn009();
	afx_msg void OnBtnClickedOn010();
	afx_msg void OnBtnClickedOn011();
	afx_msg void OnBtnClickedOn012();
	afx_msg void OnBtnClickedOn013();
	afx_msg void OnBtnClickedOn014();
	afx_msg void OnBtnClickedOn015();
	afx_msg void OnBtnClickedOn016();

	afx_msg void OnBtnClickedOff001();
	afx_msg void OnBtnClickedOff002();
	afx_msg void OnBtnClickedOff003();
	afx_msg void OnBtnClickedOff004();
	afx_msg void OnBtnClickedOff005();
	afx_msg void OnBtnClickedOff006();
	afx_msg void OnBtnClickedOff007();
	afx_msg void OnBtnClickedOff008();
	afx_msg void OnBtnClickedOff009();
	afx_msg void OnBtnClickedOff010();
	afx_msg void OnBtnClickedOff011();
	afx_msg void OnBtnClickedOff012();
	afx_msg void OnBtnClickedOff013();
	afx_msg void OnBtnClickedOff014();
	afx_msg void OnBtnClickedOff015();
	afx_msg void OnBtnClickedOff016();

	afx_msg void OnBnClickedBtnStopper1();
	afx_msg void OnBnClickedBtnStopper2();
	afx_msg void OnBnClickedBtnStopper3();
	afx_msg void OnBnClickedBtnStopper4();

	afx_msg void OnBnClickedBtnNormalSensor();
	afx_msg void OnBnClickedBuzzerOffBtn();
	afx_msg void OnBnClickedEmergencyStopBtn();
	afx_msg void OnBnClickedFmsEndCmdSendBtn();
	afx_msg void OnBnClickedFmsReset1Btn();
	afx_msg void OnBnClickedContinueBtn();

	afx_msg void OnBnClickedFmsReset2Btn();	
	afx_msg void OnBnClickedSafetylocalModeBtn();	
	afx_msg void OnBnClickedBtnStopper5();
	afx_msg void OnBnClickedBtnStopper6();
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CManualControlView::GetDocument()
{ return (CCTSMonDoc*)m_pDocument; }
#endif


