#include "StdAfx.h"
#include "CTSMon.h"

#include "FMSGlobal.h"
#include "FMSLog.h"
#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"
#include "FMSStaticSyncParent.h"
#include "FMSMemoryPool.h"
#include "FMSManagedBuf.h"
#include "FMSCircularQueue.h"

#include "FMSPacketBox.h"

#include "FMSIocp.h"
#include "FMSNetObj.h"

#include "FMSNetIOCP.h"
#include "FMSRawServer.h"

#include "FMSObj.h"
#include "FMSLog.h"

#include "FMSServer.h"
#include "XComProApi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CFMSServer::CFMSServer()
{
	mConnectCnt = 0;	
	m_HeartBeatCheck = 0;

	mKeepThreadHandle = NULL;
	mKeepThreadDestroyEvent = NULL;

	mObjDelete = NULL;
	m_bRunChk = FALSE;	
}

CFMSServer::~CFMSServer()
{

}

DWORD WINAPI KeepThreadCallback(LPVOID parameter)
{
	CFMSServer *Owner = (CFMSServer*) parameter;
	Owner->KeepThreadCallback();

	return 0;
}

VOID CFMSServer::SetHeartbeatClear()
{	
	m_HeartBeatCheck = 0;
}

BOOL CFMSServer::ChkHeartbeat()
{
	/*
	if( !m_bActive )
	{
		return FALSE;
	}
	*/

	m_HeartBeatCheck++;

	if( m_HeartBeatCheck > 10 * FMS_HEARTBEATCHECK )		
	{
		m_HeartBeatCheck = 0;
		return TRUE;
	}

	return FALSE;
}

BOOL CFMSServer::fnSendHsmeCmd( st_HSMS_PACKET pack )
{
	char    szMsg[255];
	BYTE	nBinary = 0;
	long    lMsgId, lSize;
	int     nReturn, i;
	CString sAscii;
	WORD    nU2 = 0;

	switch( pack.head.nStrm )
	{
	case 1:
		{
			switch( pack.head.nFunc )
			{
			case 2:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F2 data;
					ZeroMemory( &data, sizeof(st_S1F2));
					memcpy(&data, pack.data, sizeof(st_S1F2));

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.MDLN, 4 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.SOFTREV, 6 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						CFMSLog::WriteLog( "Reply S1F2 successfully" );
					}
					else {
						sprintf( szMsg, "Fail to reply S1F2 (%d)", nReturn );
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 6:
				{
					st_S1F6 data;
					ZeroMemory( &data, sizeof(st_S1F6));
					memcpy(&data, pack.data, sizeof(st_S1F6));

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
					nReturn = theApp.m_XComPro.SetBinaryItem( lMsgId, &data.SFCD, 1 );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, data.STAGETOTALCOUNT );

					for( i=0; i<data.STAGETOTALCOUNT; i++ )
					{
						nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );
						nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.EQ_State[i].STAGENUMBER, 3);
						nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.EQ_State[i].CTRLMODE, 1 );						
						nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.EQ_State[i].EQSTATUS, 1 );
					}

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						CFMSLog::WriteLog( "Reply S1F6 successfully" );		
					}
					else {
						sprintf( szMsg, "Fail to reply S1F6 (%d)", nReturn );
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 13:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F13 data;
					ZeroMemory( &data, sizeof(st_S1F13));
					memcpy(&data, pack.data, sizeof(st_S1F13));

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.MDLN, 4 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.SOFTREV, 6 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						CFMSLog::WriteLog( "Reply S1F13 successfully" );
					}
					else {
						sprintf( szMsg, "Fail to reply S1F13 (%d)", nReturn );
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 14:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F14 data;
					ZeroMemory( &data, sizeof(st_S1F14));
					memcpy(&data, pack.data, sizeof(st_S1F14));

					nReturn = theApp.m_XComPro.SetBinaryItem(lMsgId, &data.COMMACK, 1);
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.MDLN, 4 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.SOFTREV, 6 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						CFMSLog::WriteLog( "Reply S1F14 successfully" );
					}
					else {
						sprintf( szMsg, "Fail to reply S1F14 (%d)", nReturn );
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;			

			case 18:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F18 data;
					ZeroMemory( &data, sizeof(st_S1F18));
					memcpy(&data, pack.data, sizeof(st_S1F18));

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId,data.STAGENUMBER, 3);

					if( data.fmsERCode == FMS_ER_NONE )
					{
						nBinary = 0;
					}
					else
					{
						nBinary = 1;
					}

					nReturn = theApp.m_XComPro.SetBinaryItem( lMsgId, &nBinary, 1 );
					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						CFMSLog::WriteLog( "Reply S1F18 successfully" );
					}
					else {
						sprintf( szMsg, "Fail to reply S1F18 (%d)", nReturn );
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;
			}
		}
	case 2:
		{
			switch( pack.head.nFunc )
			{
			case 42:
				{
					st_S2F42 data;
					ZeroMemory( &data, sizeof(st_S2F42));
					memcpy(&data, pack.data, sizeof(st_S2F42));					

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );
					nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &data.ACK, 1 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 3);
// 					strData = data.STAGENUMBER;
// 					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, strData.GetBuffer(), strData.GetLength());
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.TRAYID_1, 10);
// 					strData = data.TRAYID_1;
// 					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, strData.GetBuffer(), strData.GetLength());
					nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
// 					strData = data.TRAYID_2;
// 					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, strData.GetBuffer(), strData.GetLength());
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.TRAYID_2, 10);
					nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						CFMSLog::WriteLog( "Reply S2F42 successfully" );
					}
					else {
						sprintf( szMsg, "Fail to reply S2F42 (%d)", nReturn );
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 2:
				break;
			}

		}
		break;
	
	case 6:
		{
			switch( pack.head.nFunc )
			{
			case 11:
				{
					switch( pack.head.nId )
					{
					case 1:
					case 5:
					case 6:
					case 7:
					case 12:
					case 13:
						{
							st_S6F11C1 data;
							ZeroMemory( &data, sizeof(st_S6F11C1));
							memcpy(&data, pack.data, sizeof(st_S6F11C1));					

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &pack.head.nId, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 3);
							
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.TRAYID_1, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );
							
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.TRAYID_2, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								CFMSLog::WriteLog( "Reply S6F11 successfully" );
							}
							else {
								sprintf( szMsg, "Fail to reply S6F11 (%d)", nReturn );
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 11:
						{
							st_S6F11C11 data;
							ZeroMemory( &data, sizeof(st_S6F11C11));
							memcpy(&data, pack.data, sizeof(st_S6F11C11));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem(lMsgId, 4);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &pack.head.nId, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 3);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.CTRLMODE, 1 );
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.EQSTATUS, 1 );			

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								CFMSLog::WriteLog( "Reply S6F11 successfully" );
							}
							else {
								sprintf( szMsg, "Fail to reply S6F11 (%d)", nReturn );
								CFMSLog::WriteLog( szMsg );
							}
						}
						break;
					}

				}
				break;
			}
		}
		break;
	}

	return TRUE;
}


VOID CFMSServer::KeepThreadCallback(VOID)
{
	while (TRUE)
	{
		SetEvent(mObjDelete);
		DWORD Result = WaitForSingleObject(mKeepThreadDestroyEvent, 100);

		if (Result == WAIT_OBJECT_0)
			return;

		// 1. 전송할 CMD 데이터가 없으면 heartbeat 체크 신호를 ON 한다.
		// 2. Heartbeat 신호를 전송한 후
		if( m_bRunChk == FALSE )
		{
			m_bRunChk = TRUE;

			if( theApp.m_HSMS_SendQ.GetIsEmpty() )
			{
				if( ChkHeartbeat() == TRUE )
				{
					// heartbeat 신호를 보낸다.
					/*
					CHAR cmd[5] = {0};
					sprintf(cmd, "%04s", "1001");
					fnMakeHead(m_Pack.head, cmd, 0, '0');
					memset(m_Pack.data, 0x20, sizeof(m_Pack.data));
					m_Pack.dataLen = 0;

					if( poFMSObj->Write(m_Pack.head, (BYTE*)m_Pack.data, m_Pack.dataLen) == FALSE )
					{
						if (poFMSObj->ForceClose())
						{
							OnDisconnected(poFMSObj);
						}
					}
					*/
				}
			}
			else
			{
				SetHeartbeatClear();

				while( !theApp.m_HSMS_SendQ.GetIsEmpty() )
				{
					st_HSMS_PACKET pack;
					theApp.m_HSMS_SendQ.Pop(pack);

					if( fnSendHsmeCmd(pack) == TRUE )
					{
						// 1. Log
					}
					else
					{
						// theApp.m_HSMS_SendQ.Push(pack);
					}
				}
			}
		}

		m_bRunChk = FALSE;
	}
}


BOOL CFMSServer::NetBegin(HWND mainframe)
{
	////////////////////////////////////////////////////////////////////////////
	////Test
	//st_FMSMSGHEAD* phead = new st_FMSMSGHEAD;
	//ZeroMemory(phead, sizeof(st_FMSMSGHEAD));
	//strCutter(g_Test_pMsg, phead, g_iHead_Map);
	//CHAR msg[1024] = {0,};
	//strLinker(msg, phead, g_iHead_Map);
	////////////////////////////////////////////////////////////////////////////
	INT idx = 0;
	INT lenidx = 0;
	UINT nPort = 7000;
	CHAR szhead[51] = {0};

	szhead[idx]  = '@';

	lenidx += g_iHead_Map[idx];//1
	memcpy(szhead + lenidx
		, AfxGetApp()->GetProfileString("FMS", "Line", "")
		, g_iHead_Map[++idx]);//4

	lenidx += g_iHead_Map[idx];
	memcpy(szhead + lenidx
		, AfxGetApp()->GetProfileString("FMS", "Sender", "")
		, g_iHead_Map[++idx]);//8

	lenidx += g_iHead_Map[idx];
	memcpy(szhead + lenidx
		, AfxGetApp()->GetProfileString("FMS", "Addressee", "")
		, g_iHead_Map[++idx]);//8

	szhead[50] = ':';

	ZeroMemory(&m_Pack, sizeof(st_FMS_PACKET));
	memset(&m_Pack.head, 0x20, sizeof(st_FMSMSGHEAD));		//Ox20 == Space
// 	strCutter(szhead, &m_Pack.head, g_iHead_Map);
		
	m_NotifyHwnd = mainframe;

	nPort = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "FMSPortNum", 7001);

	mKeepThreadHandle = NULL;
	mKeepThreadDestroyEvent = NULL;

	mKeepThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mKeepThreadDestroyEvent)
	{
//		CFMSRawServer::End();

		return FALSE;
	}

	mKeepThreadHandle = CreateThread(NULL, 0, ::KeepThreadCallback, this, 0, NULL);
	if (!mKeepThreadHandle)
	{
//		CFMSRawServer::End();

		return FALSE;
	}
	
	mObjDelete = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mObjDelete)
	{
//		CFMSRawServer::End();

		return FALSE;
	}	

	return TRUE;
}

BOOL CFMSServer::NetEnd(VOID)
{
	if (mKeepThreadDestroyEvent && mKeepThreadHandle)
	{
		SetEvent(mKeepThreadDestroyEvent);

		WaitForSingleObject(mKeepThreadHandle, INFINITE);

		CloseHandle(mKeepThreadDestroyEvent);
		CloseHandle(mKeepThreadHandle);
	}

	CloseHandle(mObjDelete);

	// CFMSRawServer::End();

	return TRUE;
}

VOID CFMSServer::fnSendResult(const CHAR* _Longdata)
{	
// 	for (std::list<CFMSObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
// 	{
// 		CFMSObj *poFMSObj = (CFMSObj*)(*it);
// 		poFMSObj->fnSendResult(_Longdata);
// 	}
}

VOID CFMSServer::fnKillNetwork()
{
// 	for (std::list<CFMSObj*>::iterator it=m_lstConnectedObj.begin();it!=m_lstConnectedObj.end();it++)
// 	{
// 		CFMSObj *poFMSObj = (CFMSObj*)(*it);
// 		poFMSObj->ForceClose();
// 	}
}

VOID CFMSServer::fnMakeHead(st_FMSMSGHEAD& _head, CHAR* _cmd, UINT _len, CHAR _result)
{
	strcpy_s(_head.Command, _cmd);

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(_head.SendTime, "%s", dateTime.Format("%Y%m%d%H%M%S"));

	sprintf(_head.DataLength, "%06d", _len);

	memset(_head.ResultcCode, 0x20, 5);
	_head.ResultcCode[3] = _result;

	_head.HeadEnd[0] = ':';
}