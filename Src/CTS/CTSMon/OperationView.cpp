// OperationView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "OperationView.h"
#include "MainFrm.h"

#define EDIT_FONT_SIZE 28
#define BTN_FONT_SIZE 30

IMPLEMENT_DYNCREATE(COperationView, CFormView)

COperationView::COperationView()
	: CFormView(COperationView::IDD)
	, m_strModelId(_T(""))
	, m_lProcess(0)
	, m_strBatch(_T(""))
	, m_str_1st_TrayId(_T(""))	
	, m_str_2nd_TrayId(_T(""))	
	, m_str_3rd_TrayId(_T(""))		
{
	LanguageinitMonConfig(_T("COperationView"));
	m_nCurModuleID = 1;
	m_nPrevStep = 0;
	m_pConfig = NULL;
	m_nDisplayTimer = 0;
	m_pChStsSmallImage = NULL;
	m_strPreTrayId = _T("");
	m_strPreUpTrayId = _T("");
	m_n1stCurrentChCnt = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "CellInTray", 56);
	m_n2ndCurrentChCnt = 0;
	m_n3rdCurrentChCnt = 0;
	m_bFanFlag = FALSE;
}

COperationView::~COperationView()
{
	if(m_pChStsSmallImage)
	{
		delete m_pChStsSmallImage;
	}
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool COperationView::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void COperationView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
	DDX_Control(pDX, IDC_STEP_LIST, m_ctrlStepList);
	DDX_Control(pDX, IDC_LABEL_LINEMODE, m_ctrlLineMode);		
	DDX_Control(pDX, ID_LABEL_PROCESS_TEST, m_LabelProcessTest);
	DDX_Control(pDX, ID_LABEL_PROCESS_MODEL, m_LabelProcessModel);	
	DDX_Control(pDX, ID_LABEL_BATCH, m_LabelBatch);
	DDX_Control(pDX, ID_LABEL_1ST_TRAYID, m_Label_1st_TrayId);
	DDX_Control(pDX, ID_LABEL_2ND_TRAYID, m_Label_2nd_TrayId);
	DDX_Control(pDX, ID_LABEL_3RD_TRAYID, m_Label_3rd_TrayId);

	DDX_Text(pDX, IDC_EDIT_1ST_TRAYID, m_str_1st_TrayId);
	DDX_Text(pDX, IDC_EDIT_2ND_TRAYID, m_str_2nd_TrayId);
	DDX_Text(pDX, IDC_EDIT_3RD_TRAYID, m_str_3rd_TrayId);

	DDX_Control(pDX, IDC_RUN_BTN, m_Btn_Run);
	DDX_Control(pDX, IDC_STOP_BTN, m_Btn_Stop);
	DDX_Control(pDX, IDC_CONTINUE_BTN, m_Btn_Continue);
	DDX_Control(pDX, IDC_STEPOVER_BTN, m_Btn_StepOver);
	DDX_Control(pDX, IDC_PAUSE_BTN, m_Btn_Pause);
	DDX_Control(pDX, IDC_SEND_PROCESS_BTN, m_Btn_SendProcess);
	DDX_Control(pDX, IDC_RESULTDATA_DISP_BTN, m_Btn_ResultDataDisplay);
	DDX_Control(pDX, IDC_CONTACTDATA_DISP_BTN, m_Btn_ContactResultDataDisplay);
	DDX_Control(pDX, IDC_OPERATION_AUTO_BTN, m_Btn_OperationAuto);
	DDX_Control(pDX, IDC_OPERATION_LOCAL_BTN, m_Btn_OperationLocal);	
	DDX_Text(pDX, IDC_EDIT_TYPE, m_strModelId);
	DDX_Text(pDX, IDC_EDIT_PROCESS, m_lProcess);
	DDX_Text(pDX, IDC_EDIT_BATCH, m_strBatch);
	
	DDX_Control(pDX, ID_LABEL1, m_Label1);
	DDX_Control(pDX, ID_LABEL2, m_Label2);
	DDX_Control(pDX, ID_LABEL3, m_Label3);
	DDX_Control(pDX, IDC_COMB_TABDEEPTH, m_ctrlTabDeepthCombo);
	DDX_Control(pDX, IDC_COMB_TRAYHEIGHT, m_ctrlTrayHeightCombo);
	DDX_Control(pDX, IDC_COMB_TRAYTYPE, m_ctrlTrayTypeCombo);
	
	DDX_Control(pDX, ID_LABEL_1ST_TRAY_COUNT, m_Label_1st_TrayCount);	
	DDX_Control(pDX, ID_LABEL_2ND_TRAY_COUNT, m_Label_2nd_TrayCount);
	DDX_Control(pDX, ID_LABEL_3RD_TRAY_COUNT, m_Label_3rd_TrayCount);

	DDX_Text(pDX, IDC_EDIT_1ST_CURRENT_CH_CNT, m_n1stCurrentChCnt);
	DDX_Text(pDX, IDC_EDIT_2ND_CURRENT_CH_CNT, m_n2ndCurrentChCnt);
	DDX_Text(pDX, IDC_EDIT_3RD_CURRENT_CH_CNT, m_n3rdCurrentChCnt);
	
	DDX_Check(pDX, IDC_CHECK_FANFLAG, m_bFanFlag);

	DDX_Control(pDX, IDC_LABEL_JIG_HEIGHT_MODE,	m_Label_Jig_Height_Mode);
	DDX_Control(pDX, IDC_BTN_HEIGHT_HIGH,	m_Btn_Height[JIG_HEIGHT_HIGH]);
	DDX_Control(pDX, IDC_BTN_HEIGHT_MIDDLE, m_Btn_Height[JIG_HEIGHT_MIDDLE]);
	DDX_Control(pDX, IDC_BTN_HEIGHT_LOW,	m_Btn_Height[JIG_HEIGHT_LOW]);
}

BEGIN_MESSAGE_MAP(COperationView, CFormView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_RUN_BTN, &COperationView::OnBnClickedRunBtn)
	ON_BN_CLICKED(IDC_STOP_BTN, &COperationView::OnBnClickedStopBtn)
	ON_BN_CLICKED(IDC_CONTINUE_BTN, &COperationView::OnBnClickedContinueBtn)
	ON_BN_CLICKED(IDC_PAUSE_BTN, &COperationView::OnBnClickedPauseBtn)
	ON_BN_CLICKED(IDC_STEPOVER_BTN, &COperationView::OnBnClickedStepoverBtn)
	ON_BN_CLICKED(IDC_OPERATION_AUTO_BTN, &COperationView::OnBnClickedAutoOperationBtn)
	ON_BN_CLICKED(IDC_OPERATION_LOCAL_BTN, &COperationView::OnBnClickedLocalOperationBtn)
	ON_NOTIFY(NM_DBLCLK, IDC_STEP_LIST, &COperationView::OnNMDblclkStepList)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_STEP_LIST, &COperationView::OnGetdispinfoStepList)
	ON_BN_CLICKED(IDC_SEND_PROCESS_BTN, &COperationView::OnBnClickedSendProcessBtn)
	ON_BN_CLICKED(IDC_RESULTDATA_DISP_BTN, &COperationView::OnBnClickedResultdataDispBtn)
	ON_BN_CLICKED(IDC_CONTACTDATA_DISP_BTN, &COperationView::OnBnClickedContactdataDispBtn)
	ON_EN_CHANGE(IDC_EDIT_1ST_TRAYID, &COperationView::OnEnChangeEdit1stTrayid)
	ON_EN_CHANGE(IDC_EDIT_2ND_TRAYID, &COperationView::OnEnChangeEdit2ndTrayid)
	ON_EN_CHANGE(IDC_EDIT_3RD_TRAYID, &COperationView::OnEnChangeEdit3rdTrayid)
	ON_CBN_SELCHANGE(IDC_COMB_TRAYTYPE, &COperationView::OnCbnSelchangeCombTraytype)
	ON_BN_CLICKED(IDC_CHECK_FANFLAG, &COperationView::OnBnClickedCheckFanflag)	
	ON_BN_CLICKED(IDC_BTN_HEIGHT_HIGH, &COperationView::OnBnClickedBtnHeightHigh)
	ON_BN_CLICKED(IDC_BTN_HEIGHT_MIDDLE, &COperationView::OnBnClickedBtnHeightMiddle)
	ON_BN_CLICKED(IDC_BTN_HEIGHT_LOW, &COperationView::OnBnClickedBtnHeightLow)
END_MESSAGE_MAP()


// COperationView 진단입니다.

#ifdef _DEBUG
void COperationView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void COperationView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CCTSMonDoc* COperationView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG


// COperationView 메시지 처리기입니다.
void COperationView::InitLabel()
{
	m_ctrlLineMode.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)		
		.SetFontBold(TRUE)
		.SetText("-");

	m_CmdTarget.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)			
		.SetFontBold(TRUE);

	m_LabelViewName.SetFontSize(24)
		.SetFontName("Arial")
		.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[0]);//"Operation"	
		
	m_LabelProcessTest.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[1]);//"Process"
		
	m_LabelProcessModel.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[2]);//"Type"
		
	
	m_LabelBatch.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)		
//		.SetFontName("HY헤드라인M")
		.SetText(TEXT_LANG[3]);//"Batch"
		
	m_Label_1st_TrayId.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[4])
		.SetFontName("Arial");//"1단 Tray ID"

	m_Label_2nd_TrayId.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[5]);//"2단 Tray ID"

	m_Label_3rd_TrayId.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[46]);//"3단 Tray ID"
		
	m_Label1.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE);
	m_Label2.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE);
	m_Label3.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE);
	m_Label_1st_TrayCount.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE);
	m_Label_2nd_TrayCount.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE);
	m_Label_3rd_TrayCount.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)			
		.SetFontBold(TRUE);

	m_Label_Jig_Height_Mode.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE);
}

void COperationView::InitCombo()
{	
	CString strData = _T("");
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "370");	
	m_ctrlTabDeepthCombo.AddString(strData);
	
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250");	
	m_ctrlTabDeepthCombo.AddString(strData);
	
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "200");
	m_ctrlTabDeepthCombo.AddString(strData);
	
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180");
	m_ctrlTabDeepthCombo.AddString(strData);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper5", "150");
	m_ctrlTabDeepthCombo.AddString(strData);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper6", "120");
	m_ctrlTabDeepthCombo.AddString(strData);

	m_ctrlTabDeepthCombo.SetCurSel(0);
	
	m_ctrlTrayHeightCombo.AddString("220");
	m_ctrlTrayHeightCombo.SetCurSel(0);
	
	m_ctrlTrayTypeCombo.AddString(TEXT_LANG[6]); // 1단
	m_ctrlTrayTypeCombo.AddString(TEXT_LANG[7]); // 2단
	m_ctrlTrayTypeCombo.AddString(TEXT_LANG[47]);// 3단
	m_ctrlTrayTypeCombo.SetCurSel(0);
}

void COperationView::InitFont()
{	
	LOGFONT LogFont;

	GetDlgItem(IDC_EDIT_TYPE)->GetFont()->GetLogFont(&LogFont);
	
	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_EDIT_TYPE)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_PROCESS)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_BATCH)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_1ST_TRAYID)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_2ND_TRAYID)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_3RD_TRAYID)->SetFont(&m_Font);	
	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);	
	GetDlgItem(IDC_EDIT_1ST_CURRENT_CH_CNT)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->SetFont(&m_Font);	
	GetDlgItem(IDC_COMB_TABDEEPTH)->SetFont(&m_Font);	
	GetDlgItem(IDC_COMB_TRAYHEIGHT)->SetFont(&m_Font);	
	GetDlgItem(IDC_COMB_TRAYTYPE)->SetFont(&m_Font);	
}

void COperationView::InitColorBtn()
{
	m_Btn_Run.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Run.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Stop.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Stop.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_Continue.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Continue.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_StepOver.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_StepOver.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Pause.SetFontStyle(BTN_FONT_SIZE,1);
	m_Btn_Pause.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_SendProcess.SetFontStyle(24,1);
	m_Btn_SendProcess.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_ResultDataDisplay.SetFontStyle(24,1);
	m_Btn_ResultDataDisplay.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_ContactResultDataDisplay.SetFontStyle(24,1);
	m_Btn_ContactResultDataDisplay.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_OperationAuto.SetFontStyle(24,1);
	m_Btn_OperationAuto.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_OperationLocal.SetFontStyle(24,1);
	m_Btn_OperationLocal.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	for(int i = 0; i < JIG_HEIGHT_SETTING_MAX; i++)
	{
		m_Btn_Height[i].SetFontStyle(24, 1);
		m_Btn_Height[i].SetColor(RGB_BLACK, RGB_GRAY);
	}
}

void COperationView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();						//Top Config

	InitLabel();
	InitFont();	
	InitColorBtn();
	InitCombo();
	InitStepList();
}

void COperationView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);
				
		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
		
		if(::IsWindow(GetDlgItem(IDC_STEP_LIST)->GetSafeHwnd()))
		{
			GetDlgItem(IDC_STEP_LIST)->GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			GetDlgItem(IDC_STEP_LIST)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), rect.bottom - ctrlRect.top-5 , FALSE);
		}		
	}	
}

void COperationView::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());	
	
	m_lProcess = 0;
	m_strModelId = _T("");
	m_strBatch = _T("");
	m_str_1st_TrayId = _T("");
	m_str_2nd_TrayId = _T("");
	m_str_3rd_TrayId = _T("");	
	
	m_ctrlTabDeepthCombo.ResetContent();
		
	CString strData = _T("");

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "370");
	m_ctrlTabDeepthCombo.AddString(strData);

	
	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250");
	m_ctrlTabDeepthCombo.AddString(strData);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "200");
	m_ctrlTabDeepthCombo.AddString(strData);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180");
	m_ctrlTabDeepthCombo.AddString(strData);
	
	
	m_ctrlTabDeepthCombo.SetCurSel(0);
	m_ctrlTrayHeightCombo.SetCurSel(0);	
	m_ctrlTrayTypeCombo.SetCurSel(0);
	
	UpdateGroupState(nModuleID);
	
	UpdateData(false);
}

CString COperationView::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}

void COperationView::InitStepList()
{
	m_pChStsSmallImage = new CImageList;
	//상태별 표시할 이미지 로딩
	m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON,19,24,RGB(255,255,255));
	m_ctrlStepList.SetImageList(m_pChStsSmallImage, LVSIL_SMALL);

	//
	DWORD style = 	m_ctrlStepList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;
	m_ctrlStepList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

	// Column 삽입
	m_ctrlStepList.InsertColumn(0, TEXT_LANG[8], LVCFMT_CENTER, 0);	//center 정렬을 0 column을 사용하지 않음   //"Step"
	m_ctrlStepList.InsertColumn(1, TEXT_LANG[8], LVCFMT_CENTER, 40); //"Step"
	m_ctrlStepList.InsertColumn(2, TEXT_LANG[9], LVCFMT_LEFT, 100); //"Type"
	m_ctrlStepList.InsertColumn(3, TEXT_LANG[10], LVCFMT_LEFT, 80); //"Status"
	m_ctrlStepList.InsertColumn(4, TEXT_LANG[11], LVCFMT_LEFT, 100); //"Referance"
}

void COperationView::DrawStepList()
{
	m_ctrlStepList.DeleteAllItems();

	CCTSMonDoc *pDoc=  GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	
	if(pModule == NULL)		return;
	
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;

	CTestCondition *pCon = pModule->GetCondition();
	CStep *pStep = NULL;
	char szName[32];
	int nI = 0;
	for(nI = 0; nI <pCon->GetTotalStepNo(); nI++ )
	{
		pStep = pCon->GetStep(nI);
		if(pStep == NULL)	break;
		
		sprintf(szName, "%d", pStep->m_StepIndex+1);
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		lvItem.iImage = I_IMAGECALLBACK;
		m_ctrlStepList.InsertItem(&lvItem);
		m_ctrlStepList.SetItemData(lvItem.iItem, pStep->m_type);		//==>LVN_ITEMCHANGED 를 발생 기킴 

		m_ctrlStepList.SetItemText(nI, 1, szName);

		sprintf(szName, "%s", ::StepTypeMsg(pStep->m_type));
		m_ctrlStepList.SetItemText(nI, 2, szName);
		if(pStep->m_type == EP_TYPE_CHARGE || pStep->m_type == EP_TYPE_DISCHARGE || pStep->m_type == EP_TYPE_IMPEDANCE)
		{
			sprintf(szName, "%s/%s", pDoc->ValueString(pStep->m_fVref, EP_VOLTAGE, TRUE), pDoc->ValueString(pStep->m_fIref, EP_CURRENT, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
		else if(pStep->m_type == EP_TYPE_REST)
		{
			sprintf(szName, "%s", pDoc->ValueString(pStep->m_fEndTime, EP_STEP_TIME, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
	}	

	int nCurStep = pModule->GetCurStepNo();
	for(nI = 0; nI < m_ctrlStepList.GetItemCount(); nI++)
	{
		if(pModule->IsComplitedStep(nI))
		{
			m_ctrlStepList.SetItemText(nI, 3, TEXT_LANG[37]);
		}
		else
		{
			if(nCurStep == nI+1)
			{
				//진행중인 data는 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, TEXT_LANG[38]);

				break;
			}
			else
			{
				//아직 진행하지 않은 step은 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, TEXT_LANG[39]);
			}
		}
	}
	m_ctrlStepList.Invalidate();
}

void COperationView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);
	
		StartMonitoring();	
	}
}

void COperationView::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							// 현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);

		StopMonitoring();
	}
}

void COperationView::StopMonitoring()
{
	if(m_nDisplayTimer != 0)	
	{
		KillTimer(m_nDisplayTimer);
		m_nDisplayTimer = 0;
	}
	
	m_str_1st_TrayId = _T("");
	m_str_2nd_TrayId = _T("");
	m_str_3rd_TrayId = _T("");	
	m_strBatch = _T("");
	m_lProcess = 0;
	m_strModelId = _T("");	
	m_n1stCurrentChCnt = 0;
	m_n2ndCurrentChCnt = 0;
	m_n3rdCurrentChCnt = 0;
	m_ctrlTabDeepthCombo.SetCurSel(0);
	m_ctrlTrayHeightCombo.SetCurSel(0);
	m_ctrlTrayTypeCombo.SetCurSel(0);
}

void COperationView::StartMonitoring()
{
	m_nPrevStep = 0;
	
	DrawStepList();
	
	DrawProcessInfo();
}

void COperationView::DrawProcessInfo()
{
	CCTSMonDoc *pDoc = GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	CString strTemp = _T("");
	
	if(pModule != NULL )
	{
		WORD state;
		state = EPGetGroupState(m_nCurModuleID);

		if( state != EP_STATE_LINE_OFF && pModule->GetOperationMode() != EP_OPERATION_AUTO)
		{
			if(pModule->GetTrayNo(0).GetLength() == 18)
			{
				m_str_1st_TrayId = pModule->GetTrayNo(0).Left(6);
				m_str_2nd_TrayId = pModule->GetTrayNo(0).Mid(6, 5);
				m_str_3rd_TrayId = pModule->GetTrayNo(0).Right(6);
			}
			else if( pModule->GetTrayNo(0).GetLength() == 12 )
			{
				m_str_1st_TrayId = pModule->GetTrayNo(0).Left(6);
				m_str_2nd_TrayId = pModule->GetTrayNo(0).Right(6);
				m_str_3rd_TrayId = _T("");
			}
			else
			{
				m_str_1st_TrayId = pModule->GetTrayNo(0).Left(6);
				m_str_2nd_TrayId = _T("");
				m_str_3rd_TrayId = _T("");
			}

			m_ctrlTabDeepthCombo.SetCurSel( pModule->GetTrayInfo(0)->m_nTabDeepth );
			m_ctrlTrayHeightCombo.SetCurSel( pModule->GetTrayInfo(0)->m_nTrayHeight );
			m_ctrlTrayTypeCombo.SetCurSel( pModule->GetTrayInfo(0)->m_nTrayType );

			if( pModule->GetTrayInfo(0)->m_nTrayType == 2 )
			{
				GetDlgItem(IDC_EDIT_2ND_TRAYID)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->EnableWindow(TRUE);

				GetDlgItem(IDC_EDIT_3RD_TRAYID)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->EnableWindow(TRUE);
			}
			else if(pModule->GetTrayInfo(0)->m_nTrayType == 1 )
			{
				GetDlgItem(IDC_EDIT_2ND_TRAYID)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->EnableWindow(TRUE);

				GetDlgItem(IDC_EDIT_3RD_TRAYID)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->EnableWindow(FALSE);
			}
			else
			{
				GetDlgItem(IDC_EDIT_2ND_TRAYID)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->EnableWindow(FALSE);

				GetDlgItem(IDC_EDIT_3RD_TRAYID)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->EnableWindow(FALSE);
			}

			m_n1stCurrentChCnt = pModule->GetTrayInfo(0)->GetInputCellCnt_1st();
			m_n2ndCurrentChCnt = pModule->GetTrayInfo(0)->GetInputCellCnt_2nd();
			m_n3rdCurrentChCnt = pModule->GetTrayInfo(0)->GetInputCellCnt_3rd();

			m_lProcess = pModule->GetCondition()->GetTestInfo()->lID;

			strTemp.Format("%s", pModule->GetCondition()->GetModelInfo()->szProcesstype );
			if( strTemp == _T("") )
			{
				strTemp.Format("%ld", pModule->GetCondition()->GetModelInfo()->lID);		
			}

			m_strModelId = strTemp;
			m_strBatch = pModule->GetTrayInfo(0)->strLotNo;
		}
		else
		{
			UpdateBtnEnable(false);
		}		

		UpdateData(FALSE);
	}
}

void COperationView::OnBnClickedRunBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

 	CString strTemp = _T(""), strFailMD = _T(""), strLoadFailMD = _T(""), strTrayNo = _T("");
	CString strTemp_Batch = _T(""), strTemp_Trim =_T("");

	UpdateData(true);

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);

	GetDlgItem(IDC_EDIT_BATCH)->GetWindowText(strTemp_Trim);
	strTemp_Batch = strTemp_Trim.Left(1);

	if(strTemp_Batch == "P")
	{
		pDoc->m_ProcessNo = 1;
	}
	else
	{
		pDoc->m_ProcessNo = 0;
	}
	
	if( state == EP_STATE_LINE_OFF || pMainFrm->nModuleReadyToStart[m_nCurModuleID] == 1 || state == EP_STATE_RUN )
	{
		strTemp.Format(TEXT_LANG[12]);//"충방전 진행이 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
		return;		
	}

	if( m_n1stCurrentChCnt == 0 || m_strBatch.IsEmpty() || m_str_1st_TrayId.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[13]);
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_WARNNING);		
		return;	
	}

	strTemp.Format(TEXT_LANG[40], pDoc->m_strModuleName);
	if(MessageBox(strTemp, TEXT_LANG[14], MB_ICONQUESTION|MB_YESNO) != IDYES)
		return;//"Start"

	if( state == EP_STATE_IDLE || state == EP_STATE_STANDBY || state == EP_STATE_END || state == EP_STATE_READY )
	{
		if( state != EP_STATE_IDLE )
		{
			pDoc->SendInitCommand(m_nCurModuleID);
			Sleep(200);
		}

		// 1. Idle 상태 체크 후 공정 전송
		STR_CONDITION_HEADER testHeader;
		ZeroMemory( &testHeader, sizeof(STR_CONDITION_HEADER));
		testHeader.lID = m_lProcess;
		testHeader.lNo = atoi(m_strModelId);

		CTestCondition testCondition;
		CString strTestSerialNo = _T("");

		if(testCondition.LoadTestCondition(testHeader.lID, testHeader.lNo) == true)
		{
			CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
			
			//20200914 ksj
			if( pModule->m_bRackToRack == TRUE ) //20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
			{
				strTemp.Format("Command transmission is failed.\n[ Tray position Change ]");//
				pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
				return;	
			}
			
			if(pModule != NULL)
			{
				COleDateTime curTime = COleDateTime::GetCurrentTime();
				strTestSerialNo.Format("%s%02d%07d", curTime.Format("%Y%m%d%H%M%S"), 1, rand()%EP_TESTSERIAL_EXP);

				if( !m_str_3rd_TrayId.IsEmpty())
				{
					strTrayNo.Format("% 6s% 6s% 6s", m_str_1st_TrayId, m_str_2nd_TrayId, m_str_3rd_TrayId );
				}
				else if( !m_str_2nd_TrayId.IsEmpty() )
				{
					strTrayNo.Format("% 6s% 6s", m_str_1st_TrayId, m_str_2nd_TrayId );
				}
				else
				{
					strTrayNo.Format("% 6s", m_str_1st_TrayId);
				}				

				pModule->GetTrayInfo(0)->SetTrayNo(strTrayNo);
				pModule->GetTrayInfo(0)->strLotNo = m_strBatch;
				pModule->GetTrayInfo(0)->strTestSerialNo = strTestSerialNo;								
				
				// pModule->GetTrayInfo(0)->SetInputCellCnt(m_nCurrentChCnt);
				pModule->GetTrayInfo(0)->SetInputCellCnt_1st(m_n1stCurrentChCnt);
				pModule->GetTrayInfo(0)->SetInputCellCnt_2nd(m_n2ndCurrentChCnt);
				pModule->GetTrayInfo(0)->SetInputCellCnt_3rd(m_n3rdCurrentChCnt);
				
				pModule->GetTrayInfo(0)->m_nTabDeepth = m_ctrlTabDeepthCombo.GetCurSel();
				pModule->GetTrayInfo(0)->m_nTrayHeight = m_ctrlTrayHeightCombo.GetCurSel();
				pModule->GetTrayInfo(0)->m_nTraykind = m_ctrlTrayTypeCombo.GetCurSel();
				pModule->GetTrayInfo(0)->m_nTrayType = 0; // 0 : 양방향 1 : 단방향

				if( pModule->GetTrayInfo(0)->m_nTrayType == 2 )
				{
					pModule->GetTrayInfo(0)->m_nChannelInsertType = 40;
				}
				else if(pModule->GetTrayInfo(0)->m_nTrayType == 1 )
				{
					pModule->GetTrayInfo(0)->m_nChannelInsertType = 80;
				}
				else
				{
					pModule->GetTrayInfo(0)->m_nChannelInsertType = 120;
				}

				pModule->GetTrayInfo(0)->m_nContactErrlimit = pDoc->m_nContactErrlimit;
				pModule->GetTrayInfo(0)->m_nCapaErrlimit = pDoc->m_nCapaErrlimit;
				pModule->GetTrayInfo(0)->m_nChErrlimit = pDoc->m_nChErrlimit;				

				m_ctrlTabDeepthCombo.GetLBText(m_ctrlTabDeepthCombo.GetCurSel(), strTemp);
				pModule->GetTrayInfo(0)->m_nAutoTabDeepth = atoi(strTemp);
				m_ctrlTrayHeightCombo.GetLBText(m_ctrlTrayHeightCombo.GetCurSel(), strTemp);
				pModule->GetTrayInfo(0)->m_nAutoTrayHeight = atoi(strTemp);
				
				if(pDoc->SendConditionToModule(m_nCurModuleID, 0, &testCondition) == FALSE)
				{
					strFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
					strTemp.Format("%s (%s)", TEXT_LANG[41],  ::GetModuleName(m_nCurModuleID));		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);					
				}
				else
				{
					if(pDoc->SendLastTrayStateData(m_nCurModuleID, 0) == FALSE)	//현재 Tray의 최종 상태를 Module로 전송
					{						
						strFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
					}
					else
					{
						pMainFrm->nModuleReadyToStart[m_nCurModuleID] = 1;
						pMainFrm->m_pTab->m_tabWnd.ActivateTab(1);
						pModule->m_bProcessStart = true;	//20200914 ksj
					}
				}
			}
			else
			{
				strFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
			}					
		}
		else
		{
			strLoadFailMD.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
		}				

		if( !strLoadFailMD.IsEmpty())
		{	
			strTemp.Format(TEXT_LANG[15], strFailMD, testCondition.GetModelInfo()->lID, testCondition.GetTestInfo()->lID );	//"%s 의 선택 조건 [M:%d - P:%d] Loading 을 실패 하였습니다."
			MessageBox(strTemp, TEXT_LANG[34], MB_OK|MB_ICONWARNING);							
			return;										
		}

		if( !strFailMD.IsEmpty())
		{
			strTemp.Format(TEXT_LANG[16]); //"명령을 전송할 수 없는 상태이거나 전송에 실패 하였습니다."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[12]);//"충방전 진행이 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
	}
}

void COperationView::OnBnClickedStopBtn()
{
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T("");

	pMainFrm->nModuleReadyToStart[m_nCurModuleID] = 0;
	
	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{	
		strTemp.Format(TEXT_LANG[17]);//"명령이 전송 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}

	if(state == EP_STATE_RUN || state == EP_STATE_PAUSE )
	{
		strTemp.Format(TEXT_LANG[42], ::GetModuleName(m_nCurModuleID));
		if(MessageBox(strTemp, TEXT_LANG[18], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Stop"

		strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
		state = EPGetGroupState(m_nCurModuleID);

		if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
		{	
			if(pDoc->SendStopCmd(m_nCurModuleID) == FALSE)
			{
				strFailMD += strTemp;
			}
		}
		else
		{
			strFailMD += strTemp;
		}

		if(!strFailMD.IsEmpty())
		{
			strTemp.Format(TEXT_LANG[16]);//"명령을 전송할 수 없는 상태이거나 전송에 실패 하였습니다."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		}
		else
		{
			strTemp.Format(TEXT_LANG[19]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[17]);//"명령이 전송 가능한 상태가 아닙니다"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
	}	
}

void COperationView::OnBnClickedContinueBtn()
{
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");
	
	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		return;		
	}	

	strTemp.Format(TEXT_LANG[43], pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Continue", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	
	if(state == EP_STATE_PAUSE )	
	{
		int nRtn = EP_NACK;
		if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_CONTINUE)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  TEXT_LANG[41],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
		// EPSendCommand(m_nCurModuleID);
	}
	else
	{
		strFailMD += strTemp;
		strTemp.Format(TEXT_LANG[20],  ::GetModuleName(m_nCurModuleID));//"%s는 계속진행을 전송할 수 없는 상태이므로 전송하지 않았습니다."		
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[21], strFailMD); //"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, TEXT_LANG[34], MB_OK|MB_ICONWARNING);
	}
}

void COperationView::OnBnClickedPauseBtn()
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[33] + " [Start]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(TEXT_LANG[44], pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Pause", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	WORD state;
	
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if(state == EP_STATE_RUN)	
		{
			int nRtn = EP_NACK;
			if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_PAUSE)) != EP_ACK)
			{
				strFailMD += strTemp;
				strTemp.Format("%s (%s-%s)",  TEXT_LANG[41],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}
		}
		else
		{
			strFailMD += strTemp;
			strTemp.Format(TEXT_LANG[21],  ::GetModuleName(m_nCurModuleID));
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[22], strFailMD);
		MessageBox(strTemp, TEXT_LANG[34], MB_OK|MB_ICONWARNING);
	}
}

void COperationView::OnBnClickedStepoverBtn()
{
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(TEXT_LANG[45], pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Next Step", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	WORD state;
	
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if( state != EP_STATE_LINE_OFF )
		{
			if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)	
			{
				int nRtn = EP_NACK;
				if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_NEXTSTEP)) != EP_ACK)
				{
					strFailMD += strTemp;
					strTemp.Format("%s (%s-%s)",  TEXT_LANG[41],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
					pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
				}
				// EPSendCommand(m_nCurModuleID);
			}
			else
			{
				strFailMD += strTemp;
				strTemp.Format(TEXT_LANG[23],  ::GetModuleName(m_nCurModuleID)); //"%s 는 다음 Step을 전송할 수 없는 상태이므로 전송하지 않았습니다."
				pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
			}			
		}	
	}

	if(!strFailMD.IsEmpty())
	{		
		strTemp.Format(TEXT_LANG[22], strFailMD);//"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, TEXT_LANG[34], MB_OK|MB_ICONWARNING);
	}
}

void COperationView::DrawChannel()
{
	BYTE colorFlag = 0;
	CString  strMsg, strOld, strToolTip, strUnit;

	CCTSMonDoc *pDoc = GetDocument();
	float	fTemp = 0;

	CTestCondition *pProc;
	CStep *pStep = NULL;

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		return;

	int nCurStep = pModule->GetCurStepNo();
	pProc = pModule->GetCondition();
	if(pProc != NULL)
	{
		//		TRACE("STEP No : %d\r\n", nCurStep);
		pStep = pProc->GetStep(nCurStep-1);		//Step 번호가 1 Base
	}

	// step 이 변했으면 자동으로 현재 step을 이동한다.
	if(m_nPrevStep != nCurStep)
	{
		m_nPrevStep = nCurStep;
		
		//현재 진행 Step에 맞게 Display 한다.
		for(int item = 0; item < m_ctrlStepList.GetItemCount(); item++)
		{
			if(pModule->IsComplitedStep(item))
			{
				//완료된 data는 파일에서 loading한다.
				m_ctrlStepList.SetItemText(item, 3, TEXT_LANG[37]);
			}
			else
			{
				// 작업을 완료한 후 결과 파일이 삭제 되었거나 존재 하지 않을 경우
				// 진행중과 대기중으로 표기된다.
				if(nCurStep == item+1)
				{
					m_ctrlStepList.SetItemText(item, 3, TEXT_LANG[38]);
				}
				else
				{
					m_ctrlStepList.SetItemText(item, 3, TEXT_LANG[39]);
				}
			}
		}
		m_ctrlStepList.Invalidate();
	}
}

void COperationView::OnBnClickedAutoOperationBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( EPGetGroupState(m_nCurModuleID) == EP_STATE_LINE_OFF )
	{
		return;
	}

	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[33] + " [Change to OnLine]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(TEXT_LANG[36], m_nCurModuleID);
	if(MessageBox(strTemp, TEXT_LANG[24], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Mode Change"

	WORD state;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{
		if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPERATION_AUTO) == false )
		{
			strFailMD += strTemp;
			strTemp.Format(TEXT_LANG[25],  ::GetModuleName(m_nCurModuleID));	//"%s는 OnLine mode 변경에 실패 하였습니다."	
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[26], strFailMD); //"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, TEXT_LANG[34], MB_OK|MB_ICONWARNING);		
	}
}

void COperationView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	if( nModuleID != m_nCurModuleID )
	{	
		// 1. 현재 실행중인 모듈이 아니기에 상태값을 변경하지 않는다.
		return;
	}
	
	UpdateBtnEnable(false);
	
	if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
	{
		m_ctrlLineMode.SetText(TEXT_LANG[27]);//"Line Off"
	}
	else
	{
		CCTSMonDoc *pDoc =  GetDocument();
		CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
		if(pModule != NULL)
		{
			if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
			{
				m_ctrlLineMode.SetText(TEXT_LANG[27]);//"Line Off"			
			}
			else
			{
				DrawStepList();

				DrawChannel();

				int nLineMode = EP_OFFLINE_MODE;		
				nLineMode = pModule->GetOperationMode();

				switch( nLineMode )
				{
				case EP_OPERATION_LOCAL:
					{
						m_ctrlLineMode.SetText(TEXT_LANG[28]);//"Local Mode"
						UpdateBtnEnable(true);		
					}
					break;
				case EP_OPEARTION_MAINTENANCE:
					{
						m_ctrlLineMode.SetText(TEXT_LANG[29]);//"Maintenance Mode"
					}
					break;
				case EP_OPERATION_AUTO:
					{
						m_ctrlLineMode.SetText(TEXT_LANG[30]); //"Automatic Mode"
						DrawProcessInfo();
					}
					break;
				}

				int nJigHeightMode = pModule->GetJigHeightMode();

				if(nJigHeightMode >= 0 && nJigHeightMode < JIG_HEIGHT_SETTING_MAX)
				{
					for(int i = 0; i < JIG_HEIGHT_SETTING_MAX; i++)
						m_Btn_Height[i].SetColor(RGB_BLACK, RGB_GRAY);	// OFF	

					m_Btn_Height[nJigHeightMode].SetColor(RGB_BLACK, RGB_LIGHTGREEN);	// ON
				}
			}
		}
		else
		{
			m_ctrlLineMode.SetText(TEXT_LANG[27]);//"Line Off"
		}	
	}
}

void COperationView::UpdateBtnEnable( bool bShow )
{
	GetDlgItem(IDC_EDIT_TYPE)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_PROCESS)->EnableWindow(bShow);
	
	GetDlgItem(IDC_EDIT_BATCH)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_1ST_TRAYID)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_2ND_TRAYID)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_3RD_TRAYID)->EnableWindow(bShow);
	
	GetDlgItem(IDC_COMB_TABDEEPTH)->EnableWindow(bShow);
	GetDlgItem(IDC_COMB_TRAYHEIGHT)->EnableWindow(bShow);
	GetDlgItem(IDC_COMB_TRAYTYPE)->EnableWindow(bShow);

	GetDlgItem(IDC_EDIT_1ST_CURRENT_CH_CNT)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->EnableWindow(bShow);
	GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->EnableWindow(bShow);	
	
	GetDlgItem(IDC_RUN_BTN)->EnableWindow(bShow);
	GetDlgItem(IDC_STOP_BTN)->EnableWindow(bShow);
	GetDlgItem(IDC_STEPOVER_BTN)->EnableWindow(bShow);

	GetDlgItem(IDC_BTN_HEIGHT_HIGH)->EnableWindow(bShow);
	GetDlgItem(IDC_BTN_HEIGHT_MIDDLE)->EnableWindow(bShow);
	GetDlgItem(IDC_BTN_HEIGHT_LOW)->EnableWindow(bShow);
}


void COperationView::OnBnClickedLocalOperationBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( EPGetGroupState(m_nCurModuleID) == EP_STATE_LINE_OFF )
	{
		return;
	}

	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[33] + TEXT_LANG[31]);//" [Change to OnLine]"
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(TEXT_LANG[35], m_nCurModuleID);
	if(MessageBox(strTemp, TEXT_LANG[24], MB_ICONQUESTION|MB_YESNO) != IDYES)	return; //"Mode Change"

	WORD state;

	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	
	if( EPGetGroupState(m_nCurModuleID) != EP_STATE_LINE_OFF )
	{
		if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPERATION_LOCAL) == false )
		{
			strFailMD += strTemp;
			strTemp.Format(TEXT_LANG[32],  ::GetModuleName(m_nCurModuleID)); //"%s는 local mode 변경에 실패 하였습니다."
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[26], strFailMD); //"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, TEXT_LANG[34], MB_OK|MB_ICONWARNING);		
	}
}

void COperationView::OnNMDblclkStepList(NMHDR *pNMHDR, LRESULT *pResult)
{
	pNMHDR = NULL;
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);
	
	*pResult = 0;
}

void COperationView::OnGetdispinfoStepList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;

	UINT nType, nCurStep = 0;
	DWORD dwImgIndex = 0;	

	if(pDispInfo->item.mask & LVIF_IMAGE)
	{
		CFormModule *pMD = GetDocument()->GetModuleInfo(m_nCurModuleID);

		if(pDispInfo->item.iSubItem == 2)
		{
			nType = m_ctrlStepList.GetItemData(pDispInfo->item.iItem);	//step type

			switch(nType)
			{
			case EP_TYPE_CHARGE	:		dwImgIndex = 2;		break;
			case EP_TYPE_DISCHARGE:		dwImgIndex = 1;		break;
			case EP_TYPE_REST	:		dwImgIndex = 11;	break;
			case EP_TYPE_OCV	:		dwImgIndex = 7;		break;
			case EP_TYPE_IMPEDANCE:		dwImgIndex = 10;	break;
			case EP_TYPE_END	:		dwImgIndex = 4;		break;
			default				:		dwImgIndex = 0;		break;
			}

			pDispInfo->item.iImage = dwImgIndex + 12;

			if(pMD)		
			{
				if(pMD->IsComplitedStep(pDispInfo->item.iItem))
				{
					pDispInfo->item.iImage = dwImgIndex;
				}
			}
		}
		else if(pDispInfo->item.iSubItem == 1)
		{
			if(pMD)		
			{

				nCurStep = pMD->GetCurStepNo();
				if((UINT)pDispInfo->item.iItem + 1 == nCurStep)
				{
					pDispInfo->item.iImage = 8;				
				}
				else
				{
					pDispInfo->item.iImage = -1;
				}
			}
		}
	}
	*pResult = 0;
}

void COperationView::OnBnClickedSendProcessBtn()
{
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);	
}

void COperationView::OnBnClickedResultdataDispBtn()
{
	GetDocument()->ViewResult(m_nCurModuleID, 0);
}

void COperationView::OnBnClickedContactdataDispBtn()
{
	GetDocument()->ViewResult(m_nCurModuleID, 0, 1);
}

void COperationView::OnEnChangeEdit1stTrayid()
{
	UpdateData(true);
	
	if( m_str_1st_TrayId.GetLength() > 6 )
	{
		m_str_1st_TrayId = m_strPreTrayId;			
	}
	else
	{
		m_strPreTrayId = m_str_1st_TrayId;
	}
	
	UpdateData(false);
}

void COperationView::OnEnChangeEdit2ndTrayid()
{
	UpdateData(true);

	if( m_str_2nd_TrayId.GetLength() > 6 )
	{
		m_str_2nd_TrayId = m_strPreUpTrayId;			
	}
	else
	{
		m_strPreUpTrayId = m_str_2nd_TrayId;
	}

	UpdateData(false);
}

void COperationView::OnEnChangeEdit3rdTrayid()
{
	UpdateData(true);

	if( m_str_3rd_TrayId.GetLength() > 6 )
	{
		m_str_3rd_TrayId = m_strPreUpTrayId;			
	}
	else
	{
		m_strPreUpTrayId = m_str_3rd_TrayId;
	}

	UpdateData(false);
}

void COperationView::OnCbnSelchangeCombTraytype()
{
	if( m_ctrlTrayTypeCombo.GetCurSel() == 2 )
	{
		GetDlgItem(IDC_EDIT_2ND_TRAYID)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->EnableWindow(TRUE);

		GetDlgItem(IDC_EDIT_3RD_TRAYID)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->EnableWindow(TRUE);

		m_n1stCurrentChCnt = 40;
		m_n2ndCurrentChCnt = 40;
		m_n3rdCurrentChCnt = 40;
	}
	else if( m_ctrlTrayTypeCombo.GetCurSel() == 1 )
	{
		GetDlgItem(IDC_EDIT_2ND_TRAYID)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->EnableWindow(TRUE);

		GetDlgItem(IDC_EDIT_3RD_TRAYID)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->EnableWindow(FALSE);

		m_n1stCurrentChCnt = 40;
		m_n2ndCurrentChCnt = 40;
		m_n3rdCurrentChCnt = 0;
	}
	else
	{
		GetDlgItem(IDC_EDIT_2ND_TRAYID)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_2ND_CURRENT_CH_CNT)->EnableWindow(FALSE);

		GetDlgItem(IDC_EDIT_3RD_TRAYID)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_3RD_CURRENT_CH_CNT)->EnableWindow(FALSE);

		m_n1stCurrentChCnt = 40;
		m_n2ndCurrentChCnt = 0;
		m_n3rdCurrentChCnt = 0;
	}

	UpdateData(FALSE);
}

void COperationView::OnBnClickedCheckFanflag()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_FANFLAG))->GetCheck()==1)
	{
		g_nFanFlag = 1;
	}
	else
	{
		g_nFanFlag = 0;
	}	
}

void COperationView::OnBnClickedBtnHeightHigh()
{
	if(PasswordCheck() == false)
		return;

	EPSetLineMode(m_nCurModuleID, 0, EP_JIG_HEIGHT_MODE, JIG_HEIGHT_HIGH);

	UpdateGroupState(m_nCurModuleID);
}

void COperationView::OnBnClickedBtnHeightMiddle()
{
	if(PasswordCheck() == false)
		return;

	EPSetLineMode(m_nCurModuleID, 0, EP_JIG_HEIGHT_MODE, JIG_HEIGHT_MIDDLE);

	UpdateGroupState(m_nCurModuleID);
}

void COperationView::OnBnClickedBtnHeightLow()
{
	if(PasswordCheck() == false)
		return;

	EPSetLineMode(m_nCurModuleID, 0, EP_JIG_HEIGHT_MODE, JIG_HEIGHT_LOW);

	UpdateGroupState(m_nCurModuleID);
}