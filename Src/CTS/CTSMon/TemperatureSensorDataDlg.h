#if !defined(AFX_TEMPERATURESENSORDATA_H__39D330CE_700F_4763_A88A_67064C69C696__INCLUDED_)
#define AFX_TEMPERATURESENSORDATA_H__39D330CE_700F_4763_A88A_67064C69C696__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sen
#include "MyGridWnd.h"
#include "CTSMonDoc.h"

#define TIMER_TEMPERATURE_REFRESH_ID		1
#define TIMER_TEMPERATURE_REFRESH_INTERVAL	500
// CTemperatureSensorDataDlg 대화 상자입니다.

#define TEMPERATURE_ENGLISH_SPECIAL_CHARACTER	-80
#define TEMPERATURE_HUNGARIAN_SPECIAL_CHARACTER	-80	// Central Europe Code Page 1250

#define UNICODE_VALUE_DEGREE		0x00B0
#define UNICODE_VALUE_TEMPERATURE	0x2103

class CTemperatureSensorDataDlg : public CDialog
{
	DECLARE_DYNAMIC(CTemperatureSensorDataDlg)

public:	
	CTemperatureSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleID);
	virtual ~CTemperatureSensorDataDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TEMPERATURE_SENSOR_DATA_DLG };

	CMyGridWnd m_wndTempGrid;

	int		GetModuleID();
	void	ChangeModuleID(int nModuleID);
	void	SetModuleID(int nModuleID);
	afx_msg	BOOL DestroyWindow();

private:
	CStatic		m_cTemperaturePicture;
	CColorButton2 m_cPictureBtn;
	CCTSMonDoc* m_pDoc;
	int			m_nModuleID;
	CString		m_csTemperatureStringFormat;
	BOOL		m_bWindowSize;

	CString	CalcTemperatureChracterByLocale();
	CString	MakeTemperatureStringFormat(CString csTemperatureChar);

	void	InitializeGrid();
	BOOL	InitFirstRow();
	BOOL	InitDefaultRows();

	void	UpdateGridData();
	void	ResizeWindow(BOOL bUp=TRUE);

protected:	
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnPictureButton();

	DECLARE_MESSAGE_MAP()	
};
#endif // !defined(AFX_TEMPERATURESENSORDATA_H__39D330CE_700F_4763_A88A_67064C69C696__INCLUDED_)