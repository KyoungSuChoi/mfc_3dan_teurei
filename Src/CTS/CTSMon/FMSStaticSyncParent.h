#pragma once

template <class T>
class CFMSStaticSyncParent
{
	friend class CFMSStaticSyncObj;
public:
	class CFMSStaticSyncObj
	{
	public:
		CFMSStaticSyncObj(VOID) {T::m_scsSyncObj.Enter();}
		~CFMSStaticSyncObj(VOID) {T::m_scsSyncObj.Leave();}
	};

private:
	static CFMSCriticalSection m_scsSyncObj;
};

template <class T>
CFMSCriticalSection CFMSStaticSyncParent<T>::m_scsSyncObj;