// ChCaliData.cpp: implementation of the CChCaliData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ChCaliData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChCaliData::CChCaliData()
{
	m_nEdited = CAL_DATA_EMPTY;
	m_nBoardNo = 0;	//1Base
	m_nChannelNo = 0;	//1Base

	ZeroMemory(m_VCalData, sizeof(m_VCalData));
	ZeroMemory(m_VCheckData, sizeof(m_VCheckData));
	ZeroMemory(m_ICalData, sizeof(m_ICalData));
	ZeroMemory(m_ICheckData, sizeof(m_ICheckData));
// 20200622 KSCHOI Add Temperature Data In Calibration START
	ZeroMemory(m_VCalTemperatatureData, sizeof(m_VCalTemperatatureData));
	ZeroMemory(m_VCheckTemperatatureData, sizeof(m_VCheckTemperatatureData));
	ZeroMemory(m_ICalTemperatatureData, sizeof(m_ICalTemperatatureData));	
	ZeroMemory(m_ICheckTemperatatureData, sizeof(m_ICheckTemperatatureData));
// 20200622 KSCHOI Add Temperature Data In Calibration END
}

CChCaliData::~CChCaliData()
{

}

void CChCaliData::GetVCalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_VOLTAGE_RANGE || wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCalData[wRange][wPoint].dAdData;
	dMeterData = m_VCalData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetVCheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_VOLTAGE_RANGE || wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCheckData[wRange][wPoint].dAdData;
	dMeterData = m_VCheckData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_CURRENT_RANGE || wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICalData[wRange][wPoint].dAdData;
	dMeterData = m_ICalData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= CAL_MAX_CURRENT_RANGE || wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICheckData[wRange][wPoint].dAdData;
	dMeterData = m_ICheckData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetVCalData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCalData[0][wPoint].dAdData;
	dMeterData = m_VCalData[0][wPoint].dMeterData;
}

void CChCaliData::GetVCheckData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCheckData[0][wPoint].dAdData;
	dMeterData = m_VCheckData[0][wPoint].dMeterData;
}

void CChCaliData::GetICalData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICalData[0][wPoint].dAdData;
	dMeterData = m_ICalData[0][wPoint].dMeterData;
}

void CChCaliData::GetICheckData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= CAL_MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICheckData[0][wPoint].dAdData;
	dMeterData = m_ICheckData[0][wPoint].dMeterData;
}	 


void CChCaliData::SetVCalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_VCalData[wRange][wPoint].dAdData = dAdData;
	m_VCalData[wRange][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetVCheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_VCheckData[wRange][wPoint].dAdData = dAdData;
	m_VCheckData[wRange][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_ICalData[wRange][wPoint].dAdData = dAdData;
	m_ICalData[wRange][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_ICheckData[wRange][wPoint].dAdData = dAdData;
	m_ICheckData[wRange][wPoint].dMeterData = dMeterData;
}

	 

BOOL CChCaliData::ReadDataFromFile(FILE *fp, int iFIleSize)
{
	if(fp == NULL)	return FALSE;

	if(fread(&m_nBoardNo, sizeof(m_nBoardNo), 1, fp) < 1)
	{
		return FALSE;
	}

	if(fread(&m_nChannelNo, sizeof(m_nChannelNo), 1, fp) < 1)
	{
		return FALSE;
	}

	//여유공간 확보
	int nBuff[64];
	if(fread(nBuff, sizeof(nBuff), 1, fp) < 1)
	{
		return FALSE;
	}

	if(iFIleSize == 8547852)
	{
		// 방전교정기능 없을 때 만들어진 것.
		if(fread(m_VCalData, sizeof(m_VCalData)/2, 1, fp) < 1)
		{
			return FALSE;
		}
		if(fread(m_VCheckData, sizeof(m_VCheckData)/2, 1, fp) < 1)
		{
			return FALSE;
		}
		if(fread(m_ICalData, sizeof(m_ICalData), 1, fp) < 1)
		{
			return FALSE;
		}
		if(fread(m_ICheckData, sizeof(m_ICheckData), 1, fp) < 1)
		{
			return FALSE;
		}
	}
	else
	{
		if(fread(m_VCalData, sizeof(m_VCalData), 1, fp) < 1)
		{
			return FALSE;
		}
		if(fread(m_VCheckData, sizeof(m_VCheckData), 1, fp) < 1)
		{
			return FALSE;
		}
		if(fread(m_ICalData, sizeof(m_ICalData), 1, fp) < 1)
		{
			return FALSE;
		}
		if(fread(m_ICheckData, sizeof(m_ICheckData), 1, fp) < 1)
		{
			return FALSE;
		}
	}

	m_nEdited = CAL_DATA_SAVED;

	return TRUE;
}

BOOL CChCaliData::WriteDataToFile(FILE *fp)
{
	if(fp == NULL)	return FALSE;

	if(fwrite(&m_nBoardNo, sizeof(m_nBoardNo), 1, fp) < 1)
	{
		return FALSE;
	}

	if(fwrite(&m_nChannelNo, sizeof(m_nChannelNo), 1, fp) < 1)
	{
		return FALSE;
	}
	
	//여유공간 확보
	int nBuff[64];
	ZeroMemory(nBuff, sizeof(nBuff));
	if(fwrite(nBuff, sizeof(nBuff), 1, fp) < 1)
	{
		return FALSE;
	}

	if(fwrite(m_VCalData, sizeof(m_VCalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_VCheckData, sizeof(m_VCheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICalData, sizeof(m_ICalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICheckData, sizeof(m_ICheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	m_nEdited = CAL_DATA_SAVED;

	return TRUE;
}

// 20200622 KSCHOI Add Temperature Data In Calibration START
void CChCaliData::GetVCalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pSensorDataArray)
{
	this->GetVCalData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		pSensorDataArray[i] = m_VCalTemperatatureData[wRange][wPoint][i];
	}
}

void CChCaliData::GetVCheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pSensorDataArray)
{
	this->GetVCheckData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		pSensorDataArray[i] = m_VCheckTemperatatureData[wRange][wPoint][i];
	}
}

void CChCaliData::GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pSensorDataArray)
{
	this->GetICalData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		pSensorDataArray[i] = m_ICalTemperatatureData[wRange][wPoint][i];
	}
}

void CChCaliData::GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pSensorDataArray)
{
	this->GetICheckData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		pSensorDataArray[i] = m_ICheckTemperatatureData[wRange][wPoint][i];
	}
}


void CChCaliData::SetVCalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray)
{
	this->SetVCalData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		m_VCalTemperatatureData[wRange][wPoint][i] = TransDataValue(pSensorDataArray[i].lData, -2);
	}
}

void CChCaliData::SetVCheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray)
{
	this->SetVCheckData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		m_VCheckTemperatatureData[wRange][wPoint][i] =TransDataValue(pSensorDataArray[i].lData, -2);
	}
}

void CChCaliData::SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray)
{
	this->SetICalData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		m_ICalTemperatatureData[wRange][wPoint][i] = TransDataValue(pSensorDataArray[i].lData, -2);
	}
}

void CChCaliData::SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray)
{
	this->SetICheckData(wRange, wPoint, dAdData, dMeterData);

	if(pSensorDataArray == NULL || wRange >= 1) return;

	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		m_ICheckTemperatatureData[wRange][wPoint][i] = TransDataValue(pSensorDataArray[i].lData, -2);
	}
}

double CChCaliData::TransDataValue(LONG lValue, int iPrecision)
{
	double returnValue = (double)lValue;

	if(iPrecision < 0)
	{
		for(int i = 0; i < abs(iPrecision); i++)
			returnValue *= 0.1f;

		return returnValue;
	}
	else if(iPrecision > 0)
	{
		for(int i = 0; i < abs(iPrecision); i++)
			returnValue *= 10.0f;

		return returnValue;
	}
	else
	{
		return (double)lValue;
	}
}

BOOL CChCaliData::ReadDataFromFile(FILE *fp, UINT nVersion, int iFileSize)
{
	try
	{
		if(ReadDataFromFile(fp, iFileSize))
		{
			if(nVersion == CAL_FILE_VERSION_SENSOR_DATA)
			{
				if(fread(m_VCalTemperatatureData,	sizeof(m_VCalTemperatatureData), 1, fp) < 1)	return FALSE;
				if(fread(m_VCheckTemperatatureData, sizeof(m_VCheckTemperatatureData), 1, fp) < 1)	return FALSE;
				if(fread(m_ICalTemperatatureData,	sizeof(m_ICalTemperatatureData), 1, fp) < 1)	return FALSE;
				if(fread(m_ICheckTemperatatureData, sizeof(m_ICheckTemperatatureData), 1, fp) < 1)	return FALSE;
			}

			m_nEdited = CAL_DATA_SAVED;

			return TRUE;
		}
		else
		{
			m_nEdited = CAL_DATA_ERROR;
			return FALSE;
		}
	}	
	catch (CException* e)
	{
		m_nEdited = CAL_DATA_ERROR;
		return FALSE;
	}
}

BOOL CChCaliData::WriteDataToFile(FILE *fp, UINT nVersion)
{
	try
	{
		if(WriteDataToFile(fp))
		{
			if(nVersion == CAL_FILE_VERSION_SENSOR_DATA)
			{
				if(fwrite(m_VCalTemperatatureData, sizeof(m_VCalTemperatatureData), 1, fp) < 1) return FALSE;
				if(fwrite(m_VCheckTemperatatureData, sizeof(m_VCheckTemperatatureData), 1, fp) < 1) return FALSE;
				if(fwrite(m_ICalTemperatatureData, sizeof(m_ICalTemperatatureData), 1, fp) < 1) return FALSE;
				if(fwrite(m_ICheckTemperatatureData, sizeof(m_ICheckTemperatatureData), 1, fp) < 1) return FALSE;
			}

			m_nEdited = CAL_DATA_SAVED;
			
			return TRUE;
		}
		else
		{
			m_nEdited = CAL_DATA_ERROR;
			return FALSE;
		}
	}	
	catch (CException* e)
	{
		m_nEdited = CAL_DATA_ERROR;
		return FALSE;
	}
}
// 20200622 KSCHOI Add Temperature Data In Calibration END