#include "stdafx.h"
#include "CTSMon.h"
#include "FMS.h"
#include "MainFrm.h"

CFMS::CFMS(void)
: m_totStep(0)
, m_pStep(NULL)
, m_resultfilePathNName("")
{
	LanguageinitMonConfig(_T("CFMS"));
	ZeroMemory( m_sStepResult, sizeof(st_Result_Step_Result)*100);
	ZeroMemory( &m_sResultFile, sizeof(st_RESULT_FILE) );
	ZeroMemory(&m_inreserv, sizeof(st_CHARGER_INRESEVE));	
	
	ZeroMemory(&m_HsmsPacket, sizeof(st_HSMS_PACKET));

	m_Mode = EQUIP_ST_OFF;
	m_FMSStateCode = FMS_ST_OFF;

	m_MisSendState = 0;
	m_SendMisIdx = 0;

	m_LastState = 0;

	m_pModule = NULL;

	m_Result_FIle_Type = '0';	
	
	m_bError = FALSE;	

	m_strTraytype_ims = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Traytype_ims", "E370");

	m_LastInit = FALSE;
	m_OverCCheck = FALSE;  //20200411 엄륭 용량 상한 관련 (화재관련)
	m_nChgChkF = 0; //20200807 엄륭
}

CFMS::~CFMS(void)
{	
	m_TestCondition.RemoveStep();
	m_TestCondition.ResetCondition();

	if(m_pModule)
		m_pModule->GetCondition()->ResetCondition();

	fnResetChannelCode();
	
	if(m_pStep)
	{
		delete [] m_pStep;
	}
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CFMS::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


VOID CFMS::fnInit(CFormModule* _Module)
{
	if( _Module == NULL )
	{
		return;
	}

	m_pModule = _Module;
	m_nModuleID = _Module->GetModuleID();	
	//////////////////////////////////////////////////////////////////////////	
	CString strTemp;
	strTemp.Format("StageLastState_%02d", m_nModuleID);
	m_LastState = AfxGetApp()->GetProfileInt(FMS_REG, strTemp, 0);
	strTemp.Format("StageLastMode_%02d", m_nModuleID);
	m_LastMode = AfxGetApp()->GetProfileInt(FMS_REG, strTemp, 0);
	if(m_LastState == 0)
	{
		m_LastState = 1;
	}

	CString strTrayID;
	strTemp.Format("StageLastTray1_%02d", m_nModuleID);
	strTrayID = AfxGetApp()->GetProfileString(FMS_REG, strTemp, "");
	if(strTrayID.GetLength() == 6)
	{
		m_LastTrayID[0] = strTrayID;
	}
	strTemp.Format("StageLastTray2_%02d",m_nModuleID);
	strTrayID = AfxGetApp()->GetProfileString(FMS_REG, strTemp, "");
	if(strTrayID.GetLength() == 6)
	{
		m_LastTrayID[1] = strTrayID;
	}


// 	if(strTrayID[0].GetLength() == 6 && strTrayID[1].GetLength() == 6)
// 	{
// 		strTemp = strTrayID[0] + strTrayID[1];
// 		pModule->GetTrayInfo()->SetTrayNo(strTemp);
// 	}
// 	else if(strTrayID[0].GetLength() == 6)
// 	{
// 		strTemp = strTrayID[0];
// 		pModule->GetTrayInfo()->SetTrayNo(strTemp);
// 	}

	if(m_LastState == FMS_ST_ERROR)
	{
		m_bError = TRUE;
	}

	if(m_LastMode == EP_OPERATION_CALIBRATION)
	{
		
	}
	
	//////////////////////////////////////////////////////////////////////////
	//Get Data Folder
	m_strDataFolder = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"Data");		
	if(m_strDataFolder.IsEmpty())
	{
		m_strDataFolder.Format("%s\\Data", theApp.m_strCurFolder);
		theApp.WriteProfileString(FORM_PATH_REG_SECTION, "Data", m_strDataFolder);
	}

	m_bFolderModuleName = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "ModuleFolder", FALSE);
	m_bFolderTime = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TimeFolder", FALSE);
	m_nTimeFolderInterval = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "FolderTimeInterval", 30);
	m_bFolderLot = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "LotFolder", FALSE);
	m_bFolderTray = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayFolder", FALSE);
	//////////////////////////////////////////////////////////////////////////
	fnLoadChannelCode();
	fnLoadTabDeepth();
	fnLoadTrayHeight();
}

BOOL CFMS::fnLoadTabDeepth()
{	
	st_FMS_TabDeepth *pCode;
	CString strData;

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "300");
	pCode = new st_FMS_TabDeepth;
	pCode->nValue = atoi(strData);
	m_vTabDeepth.push_back(pCode);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250");
	pCode = new st_FMS_TabDeepth;
	pCode->nValue = atoi(strData);
	m_vTabDeepth.push_back(pCode);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "240");
	pCode = new st_FMS_TabDeepth;
	pCode->nValue = atoi(strData);
	m_vTabDeepth.push_back(pCode);

	strData = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180");
	pCode = new st_FMS_TabDeepth;
	pCode->nValue = atoi(strData);
	m_vTabDeepth.push_back(pCode);


	return TRUE;
}

INT CFMS::fnGetTabDeepth( WORD _code )
{
	for (UINT i = 0; i < m_vTabDeepth.size(); i++)
	{
		if(m_vTabDeepth[i]->nValue == _code)
		{
			return i;
		}
	}

	// 셋팅된 TabDeepth 값이 없는 경우 0으로 전송
	return -1;
}

CString CFMS::fnGetTrayType()
{
	return m_strTraytype_ims;
}

BOOL CFMS::fnLoadTrayHeight()
{	
	st_FMS_TrayHeight *pCode;	

	pCode = new st_FMS_TrayHeight;
	pCode->nValue = 220;
	m_vTrayHeight.push_back( pCode );

	pCode = new st_FMS_TrayHeight;
	pCode->nValue = 455;
	m_vTrayHeight.push_back( pCode );

	return TRUE;
}

INT CFMS::fnGetTrayHeight( WORD _code )
{
	for (UINT i = 0; i < m_vTrayHeight.size(); i++)
	{
		if(m_vTrayHeight[i]->nValue == _code)
		{
			return i;
		}
	}
	return -1;
}

BOOL CFMS::fnLoadChannelCode()
{
	CDaoDatabase  db;

	CString dbpath = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString m_strDataBaseName = dbpath +"\\"+FORM_SET_DATABASE_NAME;
	
	try
	{
		db.Open(m_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		CString strSQL;
		strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode1 ORDER BY Code");
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		st_FMS_ChannelCode *pCode;

		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue(0);		//code
			INT code = data.lVal;

			data = rs.GetFieldValue(1);		//message
			if(VT_NULL != data.vt)
			{
				if(strlen((CHAR*)data.pbVal) == 4)
				{
					pCode = new st_FMS_ChannelCode;
					pCode->nCode = code;
					sprintf(pCode->szMessage, "%s", data.pbVal);

					m_vChannelCode.push_back(pCode);
				}
			}
			
			rs.MoveNext();
		}
	}
	rs.Close();
	db.Close();
	return TRUE;
}

CHAR* CFMS::fnGetChannelCode(INT _code)
{
	for (UINT i = 0; i < m_vChannelCode.size(); i++)
	{
		if(m_vChannelCode[i]->nCode == _code)
		{
			return m_vChannelCode[i]->szMessage;
		}
	}

	return "XXXX";
}

VOID CFMS::fnResetChannelCode()
{
	UINT i;

	for (i = 0; i < m_vChannelCode.size(); i++)
	{
		st_FMS_ChannelCode* pCode = m_vChannelCode[i];

		delete pCode;
	}
	m_vChannelCode.clear();

	for (i = 0; i < m_vTabDeepth.size(); i++)
	{
		st_FMS_TabDeepth* pCode = m_vTabDeepth[i];

		delete pCode;
	}

	m_vTabDeepth.clear();

	for (i = 0; i < m_vTrayHeight.size(); i++)
	{
		st_FMS_TrayHeight* pCode = m_vTrayHeight[i];

		delete pCode;
	}

	m_vTrayHeight.clear();
}

INT CFMS::strLinker(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totlink = 0;
	UINT totidx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iLink = mesmap[i++];
		memcpy(msg + totlink, stTemp + totidx, iLink); 

		totidx += iLink + 1;
		totlink += iLink;
	}

	return totlink;
}

INT CFMS::strCutter(CHAR *msg, VOID * _dest, INT* mesmap)
{
	CHAR *stTemp = (CHAR *)_dest;

	UINT totcut = 0;
	UINT totidx = 0;
	UINT szEndIdx = 0;
	UINT i = 0;
	while(mesmap[i])
	{
		INT iCut = mesmap[i++];
		memcpy(stTemp + totcut, msg + totidx, iCut);

		totidx += iCut;
		totcut += iCut;
		stTemp[totcut] = 0;
		totcut++;
	}

	return totidx;
}

FMS_ERRORCODE CFMS::fnSetReserve(st_CHARGER_INRESEVE inreserv)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;	

	m_pModule->ResetGroupData();
	
	m_inreserv = inreserv;

	return _error;
}

FMS_ERRORCODE CFMS::fnReserveProc()
{
	FMS_ERRORCODE _error = FMS_ER_NONE;
	INT nRtn = 0;

	if(fnTransFMSWorkInfoToCTSWorkInfo() == FALSE)
	{
		if(m_OverCCheck == TRUE)
		{
			m_OverCCheck = FALSE;
			return _error = ER_fnRecipeOverC_Error;  //20200411 엄륭 용량 상한 관련 에러 처리
		}
		else
		{
			return _error = ER_fnTransFMSWorkInfoToCTSWorkInfo;
		}
		
	}

	nRtn = fnSendConditionToModule();
	if( nRtn != EP_ACK )
	{	
		if( nRtn == EP_NACK ) {
			_error = ER_fnTransFMSWorkInfoToCTSWorkInfo;
			CFMSLog::WriteErrorLog("[Module %d] error : EP_NACK, ER_fnTransFMSWorkInfoToCTSWorkInfo", m_pModule->GetModuleID());
		}
		else if( nRtn == EP_TIMEOUT) {
			_error = ER_SBC_NetworkError;
			CFMSLog::WriteErrorLog("[Module %d] error : ER_SBC_NetworkError", m_pModule->GetModuleID());
		}
		else
		{
			_error = ER_SBC_ConditionError;
			CFMSLog::WriteErrorLog("[Module %d] error : ER_SBC_ConditionError", m_pModule->GetModuleID());
		}

		return _error;
	}

	if(fnMakeTrayInfo() == FALSE)
	{
		return _error = ER_fnMakeTrayInfo;
		CFMSLog::WriteErrorLog("[Module %d] error : ER_fnMakeTrayInfo", m_pModule->GetModuleID());
	}

	return _error;
}

BOOL CFMS::fnLoadWorkInfo(INT64 _code)
{
	/*
	if( m_pModule == NULL )
	{
		CFMSLog::WriteErrorLog("fnLoadWorkInfo : m_pModule == NULL");
	}
	
	INT loadOK = 0;
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetStageNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;
	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);
 
	sqlite3_stmt * stmt = 0;
	
	CString strQuery;
	if(_code)
	{
		strQuery.Format("select idx\
			  , moduleID\
			  , F_Info \
			  , StepInfo \
			  , B_Info \
			  from FMSWorkInfo where moduleID = %d AND idx = %I64d"
			  , fnGetStageNo()
			  , _code);
	}
	else
	{
		strQuery.Format("select MAX(idx)\
				   , moduleID\
				   , F_Info \
				   , StepInfo \
				   , B_Info \
				   , Result_File_Name \
				   , Send_Complet \
				   from FMSWorkInfo where moduleID = %d"
				    , fnGetStageNo());
	}
	
	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	if(m_inreserv.Step_Info)
	{
		delete [] m_inreserv.Step_Info;
		m_inreserv.Step_Info = NULL;
	}

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		m_WorkCode = sqlite3_column_int64(stmt, c++);
		UINT64 idx = m_WorkCode;
		INT year = idx / 10000000000;
		idx -= year * 10000000000;
		INT month = idx / 100000000;
		idx -= month * 100000000;
		INT day = idx / 1000000;
		idx -= day * 1000000;

		INT hour = idx / 10000;
		idx -= hour * 10000;
		INT mint = idx / 100;
		idx -= mint * 100;
		INT sec = idx;
		m_OleRunStartTime.SetDateTime(year
			, month
			, day
			, hour
			, mint
			, sec);

		INT id = sqlite3_column_int(stmt, c++);

		INT blobsize = 0;
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_F))
		{
			ZeroMemory(&m_inreserv.F_Value, sizeof(st_CHARGER_INRESEVE_F));
			memcpy(&m_inreserv.F_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_F));
		}
		else 
		{
			loadOK++;
		}
		
		c++;

		if(m_inreserv.Step_Info)
		{
			delete [] m_inreserv.Step_Info;
			m_inreserv.Step_Info = NULL;
			loadOK++;
			break;
		}

		INT totstep = atoi(m_inreserv.Total_Step);
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_Step_Set) * totstep)
		{
			st_Step_Set* pStepInfo = new st_Step_Set[totstep];
			ZeroMemory(pStepInfo, sizeof(st_Step_Set) * totstep);
			memcpy(pStepInfo, sqlite3_column_blob(stmt, c), sizeof(st_Step_Set) * totstep);
			m_inreserv.Step_Info = pStepInfo;
		}
		else 
		{
			loadOK++;		
		}
		
		c++;

		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_B))
		{
			ZeroMemory(&m_inreserv.B_Value, sizeof(st_CHARGER_INRESEVE_B));
			memcpy(&m_inreserv.B_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_B));
		}
		else loadOK++;

		if(_code)
		{

		}
		else
		{
			c++;
			m_resultfilePathNName = (CHAR*)sqlite3_column_text(stmt, c++);
			m_PreState = sqlite3_column_int(stmt, c++);
		}
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
		else loadOK++;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);
	
	if(loadOK)
		return FALSE;
	else
		return TRUE;
		*/
	return TRUE;
}

BOOL CFMS::fnRangeCheck(STEP_VALUE_RANGE _range, CHAR* _szValue, FLOAT& _fValue, FMS_Step_Type fmsStepType)
{
	FLOAT _result = atof(_szValue);
	
	switch(_range)
	{
	case Range_S_I:		//반드시 사이값 입력
		{
			if(_MIN_CC_I <= _result && _MAX_CC_I >= _result)
			{			
				_fValue = _result;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
	case Range_S_V:
		{
			if( _result <= _MAX_CV_V )
			{
				_fValue = _result;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
	case Range_I:		//0입력 허용(선택입력)
		{
			if(_result == 0.0f || (_MIN_CC_I <= _result && _MAX_CC_I >= _result))
			{
				_fValue = _result;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
	case Range_V:		//0입력 허용(선택입력)
		{
			if( 0 <= _result && _MAX_CV_V >= _result )
			{
				_fValue = _result;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
	case Range_End_V:
		{
			if( _MIN_CV_V <= _result && _MAX_CV_V >= _result )
			{
				_fValue = _result;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
	case Range_SOC:		//0입력 허용(선택입력)
		{
			if(0 <= _result && 100 >= _result)
			{
				_fValue = _result;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
	case Range_S_Min_Time:	//반드시 0 이상값 입력
		{
			if(0 < _result)
			{
				_fValue = _result * 60.0f;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
    case Range_Min_Time:	//0입력 허용(선택입력)
        {
            if(0 <= _result)
            {
                _fValue = _result * 60.0f;
                return FALSE;
            }
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
        }
        break;		
    case Range_S_Sec_Time:    //반드시 0 이상값 입력
        {
            if(0 < _result)
            {
                _fValue = _result;
                return FALSE;
            }
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
        }
        break;		
	case Range_Capa:		//0입력 허용(선택입력)
	case Range_Sec_Time:
		{
			if(0 <= _result)
			{
				_fValue = _result;
				return FALSE;
			}
			else
			{
				if( fmsStepType != 0 )
				{
					CFMSLog::WriteErrorLog("[Module %d] Step Value Error : Item %d, szValue %s, Value %f, Step %d", m_pModule->GetModuleID(),  _range, _szValue, _result, fmsStepType);
				}
			}
		}
		break;
	}
	
	return TRUE;
}

// 1. 충전기 동작 Spec 범위 확인
BOOL CFMS::fnTransFMSWorkInfoToCTSWorkInfo()
{
	if( m_pModule == NULL )
	{
		CFMSLog::WriteErrorLog("fnTransFMSWorkInfoToCTSWorkInfo : m_pModule == NULL");
	}

	CString strtemp;
	int nTabDeepth = fnGetTabDeepth( m_inreserv.Tap_Deepth );
	if( nTabDeepth < 0 )
	{
		CFMSLog::WriteErrorLog("[Module %d] Tap_Deepth code ERROR : [%d]", m_pModule->GetModuleID(), m_inreserv.Tap_Deepth );
		return FALSE;
	}
// 	else
// 	{
// 		m_inreserv.Tap_Deepth = nTabDeepth;
// 	}
// 20200830 KSCHOI Add Invalid Tray Height Condition START
//	int nTrayHeight = 0;
	int nTrayHeight = -1;
// 20200830 KSCHOI Add Invalid Tray Height Condition END
	if(m_inreserv.Tray_Height == 220)
	{
		nTrayHeight = 0;
	}
	else if(m_inreserv.Tray_Height == 455)
	{
		nTrayHeight = 1;
	}
	//int nTrayHeight = fnGetTrayHeight( m_inreserv.Tray_Height );

	m_pModule->GetTrayInfo(0)->m_nTrayHeight = nTrayHeight;
	if( nTrayHeight < 0 )
	{
		CFMSLog::WriteErrorLog("[Module %d] Tray_Height code ERROR : [%d]", m_pModule->GetModuleID(), m_inreserv.Tray_Height );
		return FALSE;
	}
	else
	{
// 20200803 KSCHOI S2F41 RCMD 1 TRAY HEIGHT BUG FIXED START
//		m_inreserv.Tray_Height = nTrayHeight;
// 20200803 KSCHOI S2F41 RCMD 1 TRAY HEIGHT BUG FIXED END
	}

    if(m_inreserv.Tray_Type < 0 || m_inreserv.Tray_Type > 1)
    {
        CFMSLog::WriteErrorLog("[Module %d] Tray_Type code ERROR(0 - 1) : [%d]", m_pModule->GetModuleID(), m_inreserv.Tray_Type );
        return FALSE;
    }

	if(m_inreserv.nTraykind < 1  || m_inreserv.nTraykind > 2)
	{
		CFMSLog::WriteErrorLog("[Module %d] Traykind code ERROR(0 - 1) : [%d]", m_pModule->GetModuleID(), m_inreserv.nTraykind );
		return FALSE;
	}

	UINT InputCellCount = 0;

	INT i = 0;
	for (i = 0; i < MAX_CELL_COUNT; i++)
	{		
		if( m_inreserv.arCellInfo[i].Cell_Info[3] == '0' )	//OK
		{
			InputCellCount++;
		}
	}

	if( InputCellCount == 0 )
	{
		return FALSE;
	}

	STR_CONDITION Condition;
	ZeroMemory(Condition.szTestSerialNo, EP_TEST_SERIAL_LENGTH+1);
	ZeroMemory(&Condition.modelHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.conditionHeader, sizeof(STR_CONDITION_HEADER));
	ZeroMemory(&Condition.testHeader, sizeof(EP_TEST_HEADER));
	ZeroMemory(&Condition.checkParam, sizeof(STR_CHECK_PARAM));	

	sprintf(m_runInfo.szTestSerialNo, m_pModule->GetSyncCode());

	sprintf(Condition.modelHeader.szProcesstype, "%4s", m_inreserv.Process_Type);

	Condition.conditionHeader.lID = m_inreserv.Process_No;
	Condition.conditionHeader.lNo = m_inreserv.Process_No;
	
	strtemp = m_inreserv.Batch_No;	
	strtemp = strtemp.Trim();
	strcpy_s(Condition.conditionHeader.szName, strtemp.GetBuffer());
	
	FLOAT ftemp = 0;
	if(fnRangeCheck(Range_I
		, m_inreserv.Contact.Charge_Cur
		, Condition.checkParam.fIRef))
		return FALSE;

	if(fnRangeCheck(Range_Sec_Time
		, m_inreserv.Contact.Charge_Time
		, Condition.checkParam.fTime))
		return FALSE;	

	Condition.checkParam.fVRef = atof(m_inreserv.Contact.Inverse_Vol);
	Condition.checkParam.fOCVUpperValue = atof(m_inreserv.Contact.Upper_Vol_Check);
	Condition.checkParam.fOCVLowerValue = atof(m_inreserv.Contact.Lower_Vol_Check);
	Condition.checkParam.fDeltaVoltage = atof(m_inreserv.Contact.Upper_Cur_Check);
	Condition.checkParam.fMaxFaultBattery = atof(m_inreserv.Contact.Lower_Cur_Check);
	Condition.checkParam.fDeltaVoltageLimit = atof(m_inreserv.Contact.Delta_Vol_Limit);

	if( Condition.checkParam.fTime < 5 )
	{
		Condition.checkParam.fTime = 5;
	}

	if( Condition.checkParam.fIRef < 100 )
	{
		Condition.checkParam.fIRef = 250;
	}

	Condition.checkParam.compFlag = 1;
	Condition.checkParam.autoProcessingYN = 0;	

	st_Step_Set *pFMSStep = m_inreserv.Step_Info;

	INT totStep = m_inreserv.Total_Step;

    if(totStep < 1 || totStep > 100)
    {
		CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[1], m_pModule->GetModuleID(), totStep); //"[Module %d] 적용할 수 없는 Step수입니다.(1-100) [%d]"
        return FALSE;
    }

	float fRecordTime = AfxGetApp()->GetProfileInt("FMS", "IMSRecordConditionTime", 1);

	int nStepCnt = 0;

	bool bChkVolGetTime = false;			// 1. 첫번재 충전스텝에만 적용한다.
    
	for(i = 0; i < totStep; i++)
	{
		STR_COMMON_STEP _step;
		ZeroMemory(&_step, sizeof(STR_COMMON_STEP));		

		FMS_Step_Type fmsStepType = (FMS_Step_Type)pFMSStep[i].Step_Type;

		if( fmsStepType == FMS_SKIP )
		{
			continue;
		}

		_step.stepHeader.stepIndex = nStepCnt;		

		_step.fmsType = pFMSStep[i].Step_Type;
	
		_step.fRecDeltaTime = 10.0f;		//5sec

		switch(fmsStepType)
		{
		case FMS_CHG:
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CCCV;

				if(fnRangeCheck(Range_S_V	//정전압 반드시 입력
					, pFMSStep[i].Volt
					, _step.fVref
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_I	//정전류 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//종지 시간 반드시 입력
					, pFMSStep[i].Time
					, _step.fEndTime
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_I		//종지 전류 반드시 입력
					, pFMSStep[i].CutOff_Current
					, _step.fEndI
					, fmsStepType))
					return FALSE;
					
				if(fnRangeCheck(Range_SOC			//SOC 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_Capa			//CAPA 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC
					, fmsStepType))
					return FALSE;

                if(fnRangeCheck(Range_Min_Time
					, pFMSStep[i].GetTime_Setting
					, _step.fRecVIGetTime
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_V	
					, pFMSStep[i].Step_Start_Volt
					, _step.fLimitV
					, fmsStepType))
					return FALSE;

					
				_step.fOverV = atof(m_inreserv.Protect.Charge_LIMIT_Vol);
				// _step.fLimitV = atof(m_inreserv.Protect.Charge_LIMIT_Cur);
				_step.fOverI = atof(m_inreserv.Protect.Charge_LIMIT_Cur);
				//_step.fLimitI = atof(m_inreserv.Protect.Charge_L_Cur);

				_step.fOverC = atof(pFMSStep[i].Cap_H);
				if(_step.fOverC == 0) //20200411 엄륭 용량 상한 관련
				{
					m_OverCCheck = TRUE;
					return FALSE;
				}
				_step.fLimitC = atof(pFMSStep[i].Cap_L);

// 20201109 KSCHOI Disable Capacity Fitting Option At Charging Step START
// 				//19-01-09 엄륭 DCIR 관련 MES 수정
// 				_step.lCalc_Cap_Type_Data = atoi(m_inreserv.Protect.Calc_Cap_Type);
// 				_step.lConst_A_Data = atof(m_inreserv.Protect.Const_A);
// 				_step.lConst_B_Data = atof(m_inreserv.Protect.Const_B);
// 				_step.lConst_C_Data = atof(m_inreserv.Protect.Const_C);
// 				_step.lConst_D_Data = atof(m_inreserv.Protect.Const_D);
// 				_step.lConst_E_Data = atof(m_inreserv.Protect.Const_E);
// 20201109 KSCHOI Disable Capacity Fitting Option At Charging Step END

				if( bChkVolGetTime == false )
				{
					bChkVolGetTime = true;

					fnRangeCheck(Range_Min_Time
						, m_inreserv.Protect.Charge_Vol_Get_Time
						, _step.fCompTimeV[1]
					, fmsStepType);

					_step.fCompVHigh[1] = atof(m_inreserv.Protect.Charge_H_Time_Vol);
					_step.fCompVLow[1] = atof(m_inreserv.Protect.Charge_L_Time_Vol);
				}
				
				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;
			}
			break;

		case FMS_DCHG:
			{
				_step.stepHeader.type
					= EP_TYPE_DISCHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				if(fnRangeCheck(Range_S_I		//정전류 Ref 반드시 입력
					, pFMSStep[i].Current
					, _step.fIref
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_V			//정전압 Ref 선택 입력 (미입력시 SBC default 값 적용)
					, pFMSStep[i].Volt
					, _step.fVref
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//End Time 필수입력
					, pFMSStep[i].Time
					, _step.fEndTime
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_End_V		//종지 전압 반드시 입력
					, pFMSStep[i].CutOff_Volt
					, _step.fEndV
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_SOC		//Soc 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate
					, fmsStepType))
					return FALSE;





				if(fnRangeCheck(Range_Capa		//Capa 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_V	
					, pFMSStep[i].Step_Start_Volt
					, _step.fLimitV
					, fmsStepType))
					return FALSE;

				if(_step.fVref <= 0.0f)
					_step.fVref = _step.fEndV - 50.0f;	//default Ref 
				
				if(_step.fVref <= 0.0f)
					_step.fVref = 0.0f;					//default Ref

                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 

				_step.fOverV = atof(m_inreserv.Protect.DisCharge_LIMIT_Vol);				
				_step.fOverI = atof(m_inreserv.Protect.DisCharge_LIMIT_Cur);
				_step.fOverC = atof(pFMSStep[i].Cap_H);
				_step.fLimitC = atof(pFMSStep[i].Cap_L);


				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;

				//19-01-09 엄륭 DCIR 관련 MES 수정
				_step.lCalc_Cap_Type_Data = atoi(m_inreserv.Protect.Calc_Cap_Type);
				_step.lConst_A_Data = atof(m_inreserv.Protect.Const_A);
				_step.lConst_B_Data = atof(m_inreserv.Protect.Const_B);
				_step.lConst_C_Data = atof(m_inreserv.Protect.Const_C);
				_step.lConst_D_Data = atof(m_inreserv.Protect.Const_D);
				_step.lConst_E_Data = atof(m_inreserv.Protect.Const_E);

			}
			break;
		case FMS_REST:
			{
				_step.stepHeader.type
					= EP_TYPE_REST;
				_step.stepHeader.mode
					= 0;

				if(fnRangeCheck(Range_S_Min_Time		//필수 입력
					, pFMSStep[i].Time
					, _step.fEndTime
					, fmsStepType))
					return FALSE;

                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
			}
			break;
		case FMS_OCV:
			{
				_step.stepHeader.type
					= EP_TYPE_OCV;
				_step.stepHeader.mode
					= 0;
			}
			break;

		case FMS_CC_CHARGE_FANOFF:
			{
				// 팬 동작을 멈추는 스텝
				_step.m_nFanOffFlag = 1;
			}			
		case FMS_CC_CHARGE:		
			{
				_step.stepHeader.type
					= EP_TYPE_CHARGE;
				_step.stepHeader.mode
					= EP_MODE_CC;

				if(fnRangeCheck(Range_S_I		//CC 전류 필수
					, pFMSStep[i].Current
					, _step.fIref
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_V			//CV 전압 선택 입력(미입력시 SBC Default)
					, pFMSStep[i].Volt
					, _step.fVref
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_V		//종지 전압 필수입력
					, pFMSStep[i].CutOff_Volt
					, _step.fEndV
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_Min_Time	//종료 시간 필수
					, pFMSStep[i].Time
					, _step.fEndTime
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_SOC		//Soc 선택입력
					, pFMSStep[i].CutOff_SOC
					, _step.fSocRate
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_Capa		//Capa 선택입력
					, pFMSStep[i].CutOff_A
					, _step.fEndC
					, fmsStepType))
					return FALSE;

				if(fnRangeCheck(Range_S_V	
					, pFMSStep[i].Step_Start_Volt
					, _step.fLimitV
					, fmsStepType))
					return FALSE;

				if(_step.fVref <= 0.0f)
					_step.fVref = _step.fEndV + 50.0f;	//default Ref 
				if(_step.fVref >= _OVP_V)
					_step.fVref = _OVP_V-10.0f;				//default Ref 

                fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 

				_step.fOverV = atof(m_inreserv.Protect.Charge_LIMIT_Vol);				
				_step.fOverI = atof(m_inreserv.Protect.Charge_LIMIT_Cur);
				_step.fOverC = atof(pFMSStep[i].Cap_H);
				if(_step.fOverC == 0) //20200411 엄륭 용량 상한 관련
				{
					m_OverCCheck = TRUE;
					return FALSE;
				}

				_step.fLimitC = atof(pFMSStep[i].Cap_L);

				if( bChkVolGetTime == false )
				{
					bChkVolGetTime = true;
                
					fnRangeCheck(Range_Min_Time
						, m_inreserv.Protect.Charge_Vol_Get_Time
						, _step.fCompTimeV[1]);

					_step.fCompVHigh[1] = atof(m_inreserv.Protect.Charge_H_Time_Vol);
					_step.fCompVLow[1] = atof(m_inreserv.Protect.Charge_L_Time_Vol);
				}

				// _step.fRecDeltaTime = 1.0f;		//5sec
				_step.fRecDeltaTime = fRecordTime;
			}
			break;

 		case FMS_DCIR:				//19-01-09 엄륭 DCIR 관련 MES 수정
 				_step.stepHeader.type
 					= EP_TYPE_IMPEDANCE;
 				_step.stepHeader.mode
 					= EP_MODE_DC_IMP;
 
 				if(fnRangeCheck(Range_S_I		//정전류 Ref 반드시 입력
 					, pFMSStep[i].Current
 					, _step.fIref))
 					return FALSE;
 
 				//if(fnRangeCheck(Range_V			//정전압 Ref 선택 입력 (미입력시 SBC default 값 적용)
 				//	, pFMSStep[i].Volt
 				//	, _step.fVref))
 				//	return FALSE;
// 
// 20201103 KSCHOI DCIR EndTime Not Accept Zero START
// 				if(fnRangeCheck(Range_Sec_Time	//End Time 필수입력   //19-01-18 엄륭 Impedance End Time  Sec 단위로 변경
// 					, pFMSStep[i].Time
//					, _step.fEndTime))
// 					return FALSE;
				if(fnRangeCheck(Range_S_Sec_Time // 0 허용하지 않음.
					, pFMSStep[i].Time
					, _step.fEndTime))
					return FALSE;
// 20201103 KSCHOI DCIR EndTime Not Accept Zero END
 				//if(fnRangeCheck(Range_V			//종지 전압 선택입력
 				//	, pFMSStep[i].CutOff_Volt
 				//	, _step.fEndV))
 				//	return FALSE;
 
 
 				//default V Ref.
 				if(_step.fEndV <= 0.0f)		//endV 미설정시
 				{
 					_step.fVref = _MIN_CV_V - 50.0f;
 				}
 				else						//End V 설정시
 				{
 					_step.fVref = _step.fEndV-50.0f;	//default Ref 
 					if(_step.fVref <= 0.0f)
 						_step.fVref = 0.0f;				//default Ref 
 				}
// 				
                 fnRangeCheck(Range_Min_Time, pFMSStep[i].GetTime_Setting, _step.fRecVIGetTime); 
// 
// 				_step.fOverV = atof(m_inreserv.Protect.DisCharge_LIMIT_Vol);				
// 				_step.fOverI = atof(m_inreserv.Protect.DisCharge_LIMIT_Cur);
// 				_step.fOverC = atof(m_inreserv.Protect.DisCharge_H_Cap);
// 				_step.fLimitC = atof(m_inreserv.Protect.DisCharge_L_Cap);
// 
// 				_step.fRecDeltaTime = 0.1f;		//5sec

				 //19-01-09 엄륭 DCIR 관련 MES 추가
				_step.lDcir_Enable_Data = atoi(m_inreserv.Protect.DCIR);
				_step.lDcir_A_Data = atof(m_inreserv.Protect.DCIR_A);
				_step.lDcir_B_Data = atof(m_inreserv.Protect.DCIR_B);
				_step.lDcir_C_Data = atof(m_inreserv.Protect.DCIR_C);
				_step.fOverImp = atof(m_inreserv.Protect.DCIR_MAX);
				_step.fLimitImp = atof(m_inreserv.Protect.DCIR_MIN);


 			break;
		}
		
		_step.stepHeader.gradeSize = 0;
		_step.stepHeader.nProcType;

		_step.fEndDV;
		_step.fEndDI;

		_step.bUseActucalCap;
		_step.bReserved;

		_step.fSocRate;

		_step.fOverV;
		_step.fLimitV;
		_step.fOverI;
		_step.fLimitI;
		_step.fOverC;
		_step.fLimitC;
		_step.fOverImp;
		_step.fLimitImp;

		//_step.fCompTimeV;
		//_step.fCompVLow;
		//_step.fCompVHigh;

		//_step.fCompTimeI;
		//_step.fCompILow;
		//_step.fCompIHigh;

		_step.fDeltaTimeV;
		_step.fDeltaV;
		_step.fDeltaTimeI;
		_step.fDeltaI;

		_step.grade;

		// _step.fRecDeltaTime = 5.0f;		//5sec
		_step.fRecDeltaV;
		_step.fRecDeltaI;
		_step.fRecDeltaT;

		_step.fParam1;
		_step.fParam2;

		_step.UseStepContinue;
		//_step.fmsType;
		_step.Reserved2;

		_step.fReserved;

		STR_COMMON_STEP* pStep = new STR_COMMON_STEP;
		CopyMemory(pStep, &_step, sizeof(STR_COMMON_STEP));
		Condition.apStepList.Add(pStep);

		nStepCnt++;
	}

	//end step 추가
	STR_COMMON_STEP* pstepEnd = new STR_COMMON_STEP;
	ZeroMemory(pstepEnd, sizeof(STR_COMMON_STEP));
	pstepEnd->stepHeader.type = EP_TYPE_END; // nType;
	pstepEnd->stepHeader.stepIndex = nStepCnt;
	pstepEnd->stepHeader.mode = 0; //StepMode 1 : CC-CV, 2: CC, 3: CV, 4: OCV

	Condition.apStepList.Add(pstepEnd);

	Condition.testHeader.totalStep = nStepCnt + 1;

	m_TestCondition.RemoveStep();
	m_TestCondition.ResetCondition();
	if(m_TestCondition.SetProcedure(&Condition) == FALSE)
	{
		return FALSE;
	}

	m_pModule->GetCondition()->ResetCondition();
	m_pModule->GetCondition()->SetTestConditon(&m_TestCondition);

	return TRUE;
}

INT CFMS::fnSendConditionToModule()
{
	if( m_pModule == NULL )
	{
		CFMSLog::WriteErrorLog("fnSendConditionToModule : m_pModule == NULL");
		return EP_FAIL;
	}

	STR_CONDITION Condition; //20200411 엄륭 프로세스 넘버
		
	CStep* pstep = (CStep*)m_TestCondition.GetStep(0);
	pstep = (CStep*)m_TestCondition.GetStep(1);

	CString strTemp;
	int nRtn = EP_NACK;
	int nDataSize = 0;

	INT nModuleID = m_pModule->GetModuleID();
	INT nGroupIndex = 0;
	CTestCondition *lpProc = m_pModule->GetCondition();
		
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	EP_TEST_HEADER testHeader;
	ZeroMemory(&testHeader, sizeof(EP_TEST_HEADER));
	testHeader.totalStep = (BYTE)lpProc->GetTotalStepNo();
	testHeader.totalGrade = 0;

// 20201013 KSCHOI S2F41 RCMD1 TIMEOUTSEC SET 9 SECOND START
//	if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_TEST_HEADERDATA, &testHeader, sizeof(EP_TEST_HEADER)))!= EP_ACK)	
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_TEST_HEADERDATA, &testHeader, sizeof(EP_TEST_HEADER), _FMS_MSG_TIMEOUT))!= EP_ACK)	
// 20201013 KSCHOI S2F41 RCMD1 TIMEOUTSEC SET 9 SECOND END
	{
		CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_TEST_HEADERDATA(0x2005) != EP_ACK", m_pModule->GetModuleName());
		return nRtn;
	}
	
	CFMSLog::WriteLog("[Stage %s] Send EP_CMD_TEST_HEADERDATA(0x2005)", m_pModule->GetModuleName());

	EP_PRETEST_PARAM checkParam;
	ZeroMemory( &checkParam, sizeof( EP_PRETEST_PARAM));

	checkParam = lpProc->GetNetCheckParam();	

	if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_CHECK_PARAM, &checkParam, sizeof(EP_PRETEST_PARAM), _FMS_MSG_TIMEOUT)) != EP_ACK)
	{
		CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_CHECK_PARAM(0x2006) != EP_ACK", m_pModule->GetModuleName());		
		return nRtn;
	}

	CFMSLog::WriteLog("[Stage %s] Send EP_CMD_CHECK_PARAM(0x2006)", m_pModule->GetModuleName());

	//int nProcessType = 1; // 0 : 안전조건 무시, 1 : 안전조건 적용 인듯?
	m_nChgChkF = 0;
	m_ProcessNo = 0;
	
	if(m_inreserv.Process_Type[0] == 'P')
	{
		m_ProcessNo = 1;
	}

	CStep *pStep;
	LPVOID lpData = NULL;
	
	int j = 0;

	for(j=0; j< testHeader.totalStep; j++)
	{
		pStep = lpProc->GetStep(j);

		if(!pStep) return EP_NACK;

		if(pStep->m_type == 3 && m_nChgChkF == 0)  // 20200807 엄륭
		{
			m_nChgChkF = 1;
		}
		else if(pStep->m_type == 3 && m_nChgChkF != 0)
		{
			m_ProcessNo = 0;
		}
//20200512 최경수 프로세스 타입 관련 (화재관련) START
		if(pStep->GetNetStepData(lpData, nDataSize, 0, m_ProcessNo) == FALSE)	//ljb Step Send //20200411 엄륭 프로세스 넘버 수동테스트는 완료 MES에서 정보 들어올때 값을 Condition.conditionHeader.lID 문자열 비교 해야 함 P, R, T 그래서 P일때 Condition.conditionHeader.lID 값에 1을 넣어줘야함 R,T일때는 0
		//if(pStep->GetNetStepData(lpData, nDataSize, 0,m_inreserv.Process_Type) == FALSE)
//20200512 최경수 프로세스 타입 관련 (화재관련) END
		{
			return EP_FAIL;
		}

		if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_STEPDATA, lpData, nDataSize, _FMS_MSG_TIMEOUT)) != EP_ACK)	
		{
			delete lpData;
			CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_STEPDATA(0x2003) != EP_ACK", m_pModule->GetModuleName());			
			return nRtn;
		}
		else
		{
			CFMSLog::WriteLog("[Stage %s] EPSendCommand EP_CMD_STEPDATA(0x2003_%02d)", m_pModule->GetModuleName(), j+1);
		}
		
		delete lpData;
		lpData = NULL;
	}
	m_nChgChkF = 0; // 20200807 엄륭

	for(j=0; j< testHeader.totalStep; j++)
	{
		pStep = lpProc->GetStep(j);
		if(pStep->GetNetStepGradeData(lpData, nDataSize))
		{
			if((nRtn = EPSendCommand(nModuleID, nGroupIndex, 0, EP_CMD_GRADEDATA, lpData, nDataSize, _FMS_MSG_TIMEOUT)) != EP_ACK)	
			{
				delete lpData;
				CFMSLog::WriteErrorLog("[Stage %s] EPSendCommand EP_CMD_GRADEDATA(0x2004) != EP_ACK", m_pModule->GetModuleName());
				return nRtn;
			}			
			
			delete lpData;
			lpData = NULL;
		}	
	}	

	if(m_pModule->WriteTestLogTempFile(lpProc) == FALSE)	//전송 공정 조건 Log 기록 파일 
	{
		CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[2], m_pModule->GetModuleName());//"[Stage %s] WriteTestLogTempFile( 공정 조건 log 파일 생성 )"
		return EP_FAIL;
	}

	return EP_ACK;
}

BOOL CFMS::fnCheckProcAndSystemType(long lProcTypeID)
{
	//장비 종류만 확인 (ex.Formation인데 Aging으로 진입)
	//TestType Tabel에서 검색

	int nSysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();

	if(nSysType == EP_ID_ALL_SYSTEM)	return TRUE;

	CString str;
	str.Format("SELECT TestTypeName, ExeStep FROM TestType WHERE TestType = %d", lProcTypeID);	
	CDaoDatabase  db;
	try
	{
		db.Open(::GetDataBaseName());
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, str, dbReadOnly);	

		str.Empty();
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			data = rs.GetFieldValue(1);
			str = data.pcVal;
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
		e->Delete();
	}

	if(str.GetLength() > 2)
	{
		if(str.Left(2) == EP_PGS_AGING_CODE)	//Aging 장비 
		{
			if(nSysType != EP_ID_AGIGN_SYSTEM)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[3], str.Left(2), nSysType); //"Aging System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else if(str.Left(2) == EP_PGS_PREIMPEDANCE_CODE || str.Left(2) == EP_PGS_IROCV_CODE || str.Left(2) == EP_PGS_OCV_CODE )			//IROCV 측정기 
		{
			if(nSysType != EP_ID_IROCV_SYSTEM)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[4], str.Left(2), nSysType); //"IR/OCV System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else if(str.Left(2) == EP_PGS_GRADING_CODE || str.Left(2) == EP_PGS_SELECTOR_CODE)								//Selecting 설비 
		{
			if(nSysType != EP_ID_GRADING_SYSTEM)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[5], str.Left(2), nSysType); //"Grading/Selecting System에서 진행할 수 없습니다.[Code : %s, System : %d]"
				return FALSE;
			}
		}
		else //if(strProcess == "PC" || strProcess == "FO" || strProcess == "FC" )	//충방전기 설비 
		{
			if(nSysType != EP_ID_FORM_OP_SYSTEM)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[6], str.Left(2), nSysType);
				return FALSE;
			}
		}
	}
	return TRUE;
}


INT CFMS::fnStepValidityCheck()
{
	int nTotStepNum = m_TestCondition.GetTotalStepNo();
	CStep* pStep, *pTempStep;
	if(nTotStepNum <0 || nTotStepNum >  EP_MAX_STEP)
	{
		CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[7]); //"입력된 Step수가 범위를 벗어났습니다."
		return -1;
	}
	
	STR_CHECK_PARAM* pcheck = m_TestCondition.GetCheckParam();
	if(pcheck->compFlag)
	{
		//if(m_sPreTestParam.lTrickleTime < 500 || m_sPreTestParam.lTrickleTime > 6000)
		if(pcheck->fTime < 5 || pcheck->fTime > 60)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[8]); //"Cell Check Parameter의 시간 설정값을 5초~60초 범위에서 입력하십시요."
			return -5;
		}
	}

	if(nTotStepNum < 1)
	{
		CFMSLog::WriteErrorLog("nTotStepNum < 0 .");
		return 0;
	}

	pStep = (CStep*)m_TestCondition.GetStep(nTotStepNum - 1); // Get element End
	if(pStep->m_type != EP_TYPE_END)
	{
		CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[10]); //"마지막 Step이 [완료] Step이 아닙니다."
		return -1;
	}
	
	int nAdvCycleCount = 0;
	int nNormalStepCount = 0;
	int nCurrentAdvCycleIndex = 0;
	int nMaxEndVGotoStepNo = 0;		//EndV goto를 가지고 있는 Step번호 
	int nMaxEndVGotoStepIndex = 0;	//EndV goto가 가리키고 있는 Step번호

	int nMaxEndTGotoStepNo = 0;		//EndT goto를 가지고 있는 Step번호 
	int nMaxEndTGotoStepIndex = 0;	//EndT goto가 가리키고 있는 Step번호

	int nMaxEndCGotoStepNo = 0;		//EndC goto를 가지고 있는 Step번호 
	int nMaxEndCGotoStepIndex = 0;	//EndC goto가 가리키고 있는 Step번호
	CString strTemp;

	for(int i = 0; i< nTotStepNum; i++)		//Save All Step
	{
		pStep = (CStep*)m_TestCondition.GetStep(i); // Get element 0
		ASSERT(pStep);

		if(pStep->m_type < 0 )
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[9], i+1); //"Step %d의 Type이 설정되지 않았습니다."
			return -2;
		}
		
		//종료 시간이 설정되어 있고 Record time이 1Sec이하가 설정되어 있을 경우 
/*		if(pStep->m_fEndTime > 0 && ((INT)pStep->m_fRecDeltaTime) % 100 > 0)
		{
			//경고 MessageBox
			if(pStep->m_fEndTime > 600 * (((INT)pStep->m_fRecDeltaTime)%100))
			{
				CFMSLog::WriteErrorLog("Step %d의 저장 data는 %d초 이후는 1초 간격으로 저장됩니다.(Step 종료 시간이 최대 저장 시간 보다 깁니다.)"
				,i+1, (600*(((INT)pStep->m_fRecDeltaTime)%100))/100);
			}
		}
*/
		
		//cwm//Grading이 설정되었지만 Step이 없을 경우 
		//cwm//if(pStep->bGrade && pStep->sGrading_Val.chTotalGrade <= 0)
		//cwm//{	
		//cwm//	pStep->bGrade = 0;
		//cwm//}
		//LONG lMaxCurrent = AfxGetApp()->GetProfileInt("Config" , "Max Current", 5000);		//최대 CC 전류
		//LONG lMaxVoltage = AfxGetApp()->GetProfileInt("Config" , "Max Voltage", 5000);
		//LONG lMaxCurrent1= AfxGetApp()->GetProfileInt("Config" , "Max Aging Current", 500);
		//LONG nMinRefI = AfxGetApp()->GetProfileInt("Config" , "Min Ref Current", 25);		//최소 CC 전류입력값
		//LONG lVRefLow = AfxGetApp()->GetProfileInt("Config" , "CCLowRef", 0);				//최소 입력 Ref V
		//LONG lVRefHigh = AfxGetApp()->GetProfileInt("Config" , "CCHighRef", lMaxVoltage);	//최대 입력 Ref V

		//DC Impedance 측정일 경우 설정값을 입력 하여야 한다.


		if(pStep->m_type == EP_TYPE_IMPEDANCE && pStep->m_mode == EP_MODE_DC_IMP)
		{
			if(pStep->m_fIref <= _MIN_CC_I)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[11], i+1);	//"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다."							
				return -1;
			}

			if(pStep->m_fIref > _MAX_CC_I)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[12], i+1); //"Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다."
				return -1;
			}

			if(pStep->m_fEndTime == 0)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[13], i+1); //"Step %d의 종료시간이 설정되지 않았습니다."
				return -1;
			}

		} //Impedance 

		if(pStep->m_type == EP_TYPE_CHARGE || pStep->m_type == EP_TYPE_DISCHARGE)
		{
			//formation에서는 CC나 CV에 
			//if(m_lLoadedProcType != PS_PGS_AGING)
			{
				if(pStep->m_mode == EP_MODE_CC && pStep->m_fEndV <= 0.0f)
				{
					CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[14], i+1); //"Step %d의 종료 전압이 설정되어 있지 않습니다."
					return -1;
				}
				if(pStep->m_mode == EP_MODE_CV && pStep->m_fEndI <= 0.0f)
				{
					CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[15], i+1); //"Step %d의 종료 전류가 설정되어 있지 않습니다."
					return -1;
				}

				if(pStep->m_mode == EP_MODE_CCCV )
				{
					if(pStep->m_fVref > _MAX_CV_V )
					{
						CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[16] //"Step %d의 전압값이 최대 설정 전압값(%dmV) 보다 높습니다."
							, i+1, _MAX_CV_V );
						return -1;
					}

					if( pStep->m_fIref > _MAX_CC_I )
					{
						CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[17] //"Step %d의 전류값이 최대 설정 전압값(%dmA) 보다 높습니다."
							, i+1, _MAX_CC_I );
						return -1;
					}
				}
			}

			//전류 설정값은 반드시 입력해야 한다.
			if(pStep->m_mode != EP_MODE_CP && pStep->m_mode != EP_MODE_CR)
			{
				//if(m_lLoadedProcType == PS_PGS_AGING )
				//{
				//	if(pStep->m_fIref > m_lMaxCurrent1)
				//	{
				//		CFMSLog::WriteErrorLog("Step %d의 설정전류가 최대 전류값을 초과하였습니다.(Aging 공정 최대 전류 %dmA)", i+1, m_lMaxCurrent1);			
				//		return -1;
				//	}
				//}
				//else
				{
					if( pStep->m_fIref < _MIN_CC_I)
					{
						CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)(TEXT_LANG[11]+"(%dmA)")
							, i+1, _MIN_CC_I);			
						return -1;
					}

					if( pStep->m_fIref > _MAX_CC_I )
					{
						CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[17]
							, i+1, _MAX_CC_I );
						return -1;
					}
				}

				if(pStep->m_fIref <= pStep->m_fEndI || pStep->m_fIref <= pStep->m_fLowLimitI )
				{
					CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[12], i+1);			
					return -1;
				}
			}
			else	//fIRef에 CP, CR제어값이 들어 있다.
			{
				if(EP_MODE_CR == pStep->m_mode && pStep->m_fIref <= 0.0f)
				{
					CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[18], i+1);			//"Step %d의 CR 저항값 입력 범위가 벗어났습니다.(CR > 0)"
					return -1;
				}
			}

			//최소 한가지 이상의 종료조건이 설정 되어야 한다.
			if( pStep->m_fEndC <= 0 && pStep->m_fEndDI <= 0 &&
				pStep->m_fEndDV <=0 && pStep->m_fEndI <=0 && 
				pStep->m_fEndV <=0 && pStep->m_fEndTime == 0
				//cwm//&& pStep->nUseDataStepNo == 0
				)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[19], i+1); //"Step %d의 종료 조건이 설정되어 있지 않았습니다."			
				return -1;
			}

			
			int k = 0;
			for( k=0; k<3; k++ )
			{
				//전압 상하한 크기 비교
				if(pStep->m_fCompVLow[k] > 0.0 && pStep->m_fCompVHigh[k] > 0.0)
				{
					if(pStep->m_fCompVLow[k] > pStep->m_fCompVHigh[k])
					{
						CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[20], i+1, k+1 ); //"Step %d 전압 변화 비교 Point %d 전압상한값이 하한값보다 작습니다."			
						return -1;
					}
				}

				//전류 상하한 크기 비교
				if( pStep->m_fCompILow[k] > 0.0 && pStep->m_fCompIHigh[k] > 0.0)
				{
					if( pStep->m_fCompILow[k] > pStep->m_fCompIHigh[k])
					{
						CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[21], i+1, k+1 ); //"Step %d 전류 변화 비교 Point %d 전류상한값이 하한값보다 작습니다."			
						return -1;
					}
				}
			}
			
			/*
			//Soc rate 설정
			if(pStep->m_fSocRate>0.0f)
			{
				STEP *pCapStep = GetStepData(pStep->nUseDataStepNo);
				if(pCapStep == NULL || pCapStep->bUseActualCapa == FALSE)
				{
					CFMSLog::WriteErrorLog("Step %d의 기준용량 종료로 사용할 Step이 설정되지 않았거나 존재하지 않는 Step을 참조하고 있습니다.(참조 Step %d)", i+1, pStep->nUseDataStepNo);			
					return -1;
				}
			}
			*/

		}	//Charge Discharge


		if(pStep->m_type == EP_TYPE_CHARGE)
		{
			//End 전압을 정격전압보다 최소 10mV이하 낮게 설정하여야 한다.
			//End 전압은 CC모드 입력에서만 가능한데 CC동작을 위해서는 CV 전압이 더 높게 설정되어야 하므로 
//			if(pStep->m_fEndV >= m_lMaxVoltage-10)
//			{
//				CFMSLog::WriteErrorLog("Step %d의 종료 전압이 정격 전압(%dmV)에 비해 높게 설정 되었습니다. 정격전압보다 최소 10mV이하 범위값으로 설정하십시오.", i+1, m_lMaxVoltage);			
//				return -1;				
//			}

			if(pStep->m_fVref > 0 && pStep->m_mode == EP_MODE_CC && pStep->m_fEndV > 0)		//Vref가 입력된 경우 
			{
				if(pStep->m_fVref <= pStep->m_fEndV)
				{
					CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[22], i+1); //"Step %d의 전압 설정값이 종료 전압보다 낮습니다."
					return -1;
				}
			}


			if(pStep->m_fVref <= 0.0f)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[23], i+1);	//"Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.	(범위:0mV)"		
				return -1;
			}

			if(pStep->m_fVref > _OVP_V )
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[24], i+1, _OVP_V ); //"Step %d의 전압값이 설정 전압값(%dmV) 보다 높습니다."
				return -1;
			}

			if(pStep->m_fVref <= pStep->m_fEndDV)	
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[25], i+1); //"Step %d의 전압변화 설정값이 너무 큽니다."			
				return -1;				
			}
			
			if(pStep->m_fVref <= pStep->m_fEndV)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[26], i+1); //"Step %d의 종료 전압이 설정 전압보다 높습니다."			
				return -1;
			}
		} //charge
			

		//CC 방전일 경우 종료 전압은 Ref 전압 보다 커야 한다. 
		if(pStep->m_type == EP_TYPE_DISCHARGE)
		{
			if(pStep->m_fVref > 0 && pStep->m_mode == EP_MODE_CC && pStep->m_fEndV > 0)		//Vref가 입력된 경우 
			{
				if(pStep->m_fVref >= pStep->m_fEndV)
				{
					CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[27], i+1); //"Step %d의 전압 설정값이 종료 전압보다 높습니다."
					return -1;
				}
			}

			if(pStep->m_fEndV > _MAX_CV_V )
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[28], i+1); //"Step %d의 종료 전압값이 설정 전압보다 보다 높습니다."
				return -1;
			}
		} //Discharge

		//Rest는 반드시 종료 시간이 입력 되어야 한다.
		if(pStep->m_type == EP_TYPE_REST)
		{
			if(pStep->m_fEndTime == 0)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[13], i+1); //"Step %d의 종료시간이 설정되지 않았습니다."
				return -1;
			}
		} //reset
	
		//cwm//if(pStep->m_fTref == 0.0f && (pStep->m_fStartT !=0.0f || pStep->m_fEndTemp != 0.0f))
		//cwm//{
		//cwm//	CFMSLog::WriteErrorLog("Step %d의 온도 설정값을 입력하지 않고 시작온도나 종료온도가 설정되었습니다.", i+1);			
		//cwm//	return -1;
		//cwm//}
			
		//전압 설정값 입력 범위 검사
		if(  pStep->m_fVref < 0.0f)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[29], i+1); //"Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV)"
			return -1;
		}
		//전압 종료값 입력 범위 검사
		if(pStep->m_fEndV < 0.0f)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[30], i+1); //"Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV)"
			return -7;
		}
		//전류 종료값 입력 범위 검사
		if(pStep->m_fEndI < 0.0f)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[31], i+1); //"Step %d의 종료 전류값이 입력 범위를 벗어났습니다.(범위:0mA)"
			return -8;
		}

		//전압 제한값 입력 범위 검사
		if(pStep->m_fLowLimitV < 0)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[32], i+1); //"Step %d의 안전 전압 상하한값이 입력 범위를 벗어났습니다.(범위:0mV)"
			return -1;
		}

		//전류 제한값 입력 검사 
		if(pStep->m_fLowLimitI < 0)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[33], i+1); //"Step %d의 안전 전류 상하한값이 입력 범위를 벗어났습니다."
			return -1;
		}
		
		//전압 제한 상한값을 입력하였을 경우 전압 설정값보다 반드시 커야 한다.
		if(pStep->m_fHighLimitV > 0)
		{
			if(pStep->m_fHighLimitV <= pStep->m_fVref)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[34], i+1); //"Step %d의 안전 전압 상한값이 설정 전압보다 작습니다."
				return -1;
			}
		}

		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		if(pStep->m_fHighLimitI > 0 )
		{
			if(pStep->m_fHighLimitI <= pStep->m_fIref)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[35], i+1); //"Step %d의 안전 전류 상한값이 설정 전류보다 작습니다."
				return -1;
			}
		}

		//종료 전압값은 전압 제한 하한값보다 커야 한다.
		if(pStep->m_fLowLimitV > 0 && pStep->m_fEndV > 0)
		{
			if(pStep->m_fLowLimitV > pStep->m_fEndV)
			{
				CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[36], i+1); //"Step %d의 종료 전압값이 안전 전압값 보다 작습니다."
				return -1;
			}
		}

		//용량 상한값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitC > 0.0f && pStep->m_fLowLimitC > 0.0f && pStep->m_fHighLimitC < pStep->m_fLowLimitC)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[37], i+1); //"Step %d의 안전 용량 상한값이 하한값보다 작습니다."
			return -1;
		}

		//전압 상한 값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitV > 0.0f && pStep->m_fLowLimitV > 0.0f && pStep->m_fHighLimitV < pStep->m_fLowLimitV)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[38], i+1); //"Step %d의 안전 전압 상한값이 하한값보다 작습니다."
			return -1;
		}

		//전류 상한 값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitI > 0 && pStep->m_fLowLimitI > 0 && pStep->m_fHighLimitI < pStep->m_fLowLimitI)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[39], i+1); //"Step %d의 안전 전류 상한값이 하한값보다 작습니다."
			return -1;
		}

		//임피던스 상한 값이 하한값보다 커야 한다.
		if(pStep->m_fHighLimitImp > 0 && pStep->m_fLowLimitImp > 0 && pStep->m_fHighLimitImp < pStep->m_fLowLimitImp)
		{
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[40], i+1); //"Step %d의 안전 저항 상한값이 하한값보다 작습니다."
			return -1;
		}
	}
	return 1;
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
BOOL CFMS::fnMakeTrayInfo( bool bNew )
{
	CFormResultFile* pResultFile = &m_pModule->m_aResultData[0];

	CTray* pTray = m_pModule->GetTrayInfo(0);

	if( bNew == TRUE )
	{
		pTray->InitData(TRUE);
	}
	else
	{
		return TRUE;
	}
	
	pTray->strLotNo = m_inreserv.Batch_No;	
	pTray->strLotNo = pTray->strLotNo.Trim();

	CString TrayNo = _T("");
	if( m_inreserv.nTraykind == 2 )
	{
		m_inreserv.Tray_ID_1[6] = NULL;
		m_inreserv.Tray_ID_2[6] = NULL;
		TrayNo.Format("%-6s%-6s", m_inreserv.Tray_ID_1, m_inreserv.Tray_ID_2 );
	}
	else
	{
		TrayNo.Format("%-6s", m_inreserv.Tray_ID_1 );
	}
	
	TrayNo = TrayNo.Trim();
	pTray->SetTrayNo(TrayNo);

	CString strTestSerialNo = _T("");
	COleDateTime curTime = COleDateTime::GetCurrentTime();
	strTestSerialNo.Format("%s%02d%07d", curTime.Format("%Y%m%d%H%M%S"), 1, rand()%EP_TESTSERIAL_EXP);
	pTray->strTestSerialNo = strTestSerialNo;

	pTray->m_nAutoTabDeepth = m_inreserv.Tap_Deepth;
	pTray->m_nTabDeepth = fnGetTabDeepth( m_inreserv.Tap_Deepth );

	pTray->m_nAutoTrayHeight = m_inreserv.Tray_Height;
	pTray->m_nTrayHeight = fnGetTrayHeight( m_inreserv.Tray_Height );
	
	pTray->m_nTrayType = m_inreserv.Tray_Type;
	pTray->m_nContactErrlimit = m_inreserv.Contact_Error_Setting;
	pTray->m_nChErrlimit = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "ChErrorlimit", 1);
	pTray->m_nCapaErrlimit = m_inreserv.Capa_Error_Limit;

	pTray->m_nTraykind = m_inreserv.nTraykind;
	pTray->m_nChannelInsertType = m_inreserv.Channel_Insert_Type;

	//////////////////////////////////////////////////////////////////////////
	st_CELL_INFO arcellInfo[MAX_CELL_COUNT];
	memcpy(arcellInfo, m_inreserv.arCellInfo, sizeof(st_CELL_INFO) * MAX_CELL_COUNT);
	UINT InputCellCount = 0;
	INT i = 0;

	if( pTray->m_nTrayType == 0 )
	{
		for (i = 0; i < MAX_TRAYCELL_COUNT; i++)
		{
			if(arcellInfo[i].Cell_Info[3] == '0')//OK
			{
				InputCellCount++;
				pTray->cellCode[i] = EP_CODE_NORMAL;
			}
			else if(arcellInfo[i].Cell_Info[3] == '5') //이전 공정 불량	
			{
				pTray->cellCode[i] = EP_CODE_CELL_FAIL;
			}
			else if(arcellInfo[i].Cell_Info[3] == ' ') //No Cell
			{
				pTray->cellCode[i] = EP_CODE_NONCELL;
			}
			else
			{
				pTray->cellCode[i] = EP_CODE_NONCELL;
			}
		}

		pTray->lTrayCellCount_1st = InputCellCount;

		InputCellCount = 0;

		if( pTray->m_nTraykind == 2 )
		{
			for (i = MAX_TRAYCELL_COUNT; i < MAX_CELL_COUNT; i++)
			{
				if(arcellInfo[i].Cell_Info[3] == '0')//OK
				{
					InputCellCount++;
					pTray->cellCode[i] = EP_CODE_NORMAL;
				}
				else if(arcellInfo[i].Cell_Info[3] == '5') //이전 공정 불량	
				{
					pTray->cellCode[i] = EP_CODE_CELL_FAIL;
				}
				else if(arcellInfo[i].Cell_Info[3] == ' ') //No Cell
				{
					pTray->cellCode[i] = EP_CODE_NONCELL;
				}
				else
				{
					pTray->cellCode[i] = EP_CODE_NONCELL;
				}
			}

			pTray->lTrayCellCount_2nd = InputCellCount;
		}
		else
		{
			for (i = MAX_TRAYCELL_COUNT; i < MAX_CELL_COUNT; i++)
			{
				pTray->cellCode[i] = EP_CODE_NONCELL;
			}

			pTray->lTrayCellCount_2nd = InputCellCount;
		}
	}
	else
	{
		for (i = 0; i < MAX_CELL_COUNT; i++)
		{
			if(arcellInfo[i].Cell_Info[3] == '0')//OK
			{
				InputCellCount++;
				pTray->cellCode[i] = EP_CODE_NORMAL;
			}
			else if(arcellInfo[i].Cell_Info[3] == '5') //이전 공정 불량	
			{
				pTray->cellCode[i] = EP_CODE_CELL_FAIL;
			}
			else if(arcellInfo[i].Cell_Info[3] == ' ') //No Cell
			{
				pTray->cellCode[i] = EP_CODE_NONCELL;
			}
			else
			{
				pTray->cellCode[i] = EP_CODE_NONCELL;
			}
		}

		pTray->lTrayCellCount_1st = InputCellCount;
	}
	
	//////////////////////////////////////////////////////////////////////////
	
	for (i = 0; i < MAX_CELL_COUNT; i++)
	{
		pTray->SetCellSerial(i, m_inreserv.arCell_ID[i].Cell_ID);
	}

	//////////////////////////////////////////////////////////////////////////
	ZeroMemory(&m_runInfo, sizeof(EP_RUN_CMD_INFO));
	
	m_runInfo.nCurrentCellCnt = InputCellCount;							

	m_runInfo.nTabDeepth = pTray->m_nTabDeepth;			//m_inreserv.Tap_Deepth;
	m_runInfo.nTrayHeight = pTray->m_nTrayHeight;		//m_inreserv.Tray_Height;
	m_runInfo.nTrayType = pTray->m_nTrayType;			//m_inreserv.Tray_Type;	
	//m_runInfo.nTrayKind = pTray->m_nTraykind;			//m_inreserv.Tray_Kind;  KSH 20190812	  
	if(pTray->m_nTraykind == 1)
	{
		m_runInfo.nTrayKind = 0;
	}
	else if(pTray->m_nTraykind == 2)
	{
		m_runInfo.nTrayKind = 1;
	}
// 	if( pTray->m_nContactErrlimit < 1 )
// 	{
// 		pTray->m_nContactErrlimit = 1;		
// 	}

	m_runInfo.nContactErrlimit = pTray->m_nContactErrlimit;	//m_inreserv.Contact_Error_Setting;	

	m_runInfo.nCappErrlimit = pTray->m_nCapaErrlimit;	//m_inreserv.Capa_Error_Limit;	

	CFMSLog::WriteLog("[Stage %s] nTabDeepth %d, nTrayHeight %d, nTrayType %d, nCurrentCellCnt %d, nContactErrorSetting %d, nCappErrlimit %d "
		, m_pModule->GetModuleName()
		, m_runInfo.nTabDeepth
		, m_runInfo.nTrayHeight
		, m_runInfo.nTrayType
		, m_runInfo.nCurrentCellCnt
		, m_runInfo.nContactErrlimit
		, m_runInfo.nCappErrlimit
		);

	sprintf(m_runInfo.szTestSerialNo, m_pModule->GetSyncCode());

	for(INT nTrayIndex=0; nTrayIndex < INSTALL_TRAY_PER_MD; nTrayIndex++)
	{
		pTray = m_pModule->GetTrayInfo(nTrayIndex);
		if(pTray)
		{
			m_runInfo.nInputCell[nTrayIndex] = pTray->GetInputCellCnt();
		}
	}

	if(SendLastTrayStateData(m_pModule->GetModuleID(), 0) == FALSE)	//현재 Tray의 최종 상태를 Module로 전송
	{
		return FALSE;
	}

	// 1. 입고 예약 정보를 저장하기 위해서..
	if(m_pModule->UpdateTestLogTempFile())
	{

	}

	return TRUE;
}

BOOL CFMS::FileNameCheck(char *szfileName)
{
	//현재 파일명이 사용 가능한 파일명인지 확인(특수 문자등의 입력으로 파일생성 안되는것을 방지) 
	CString strTemp;
	FILE *fp = NULL;
	if(strlen(szfileName) <= 0)	return  FALSE;

	if((fp = fopen(szfileName, "wb")) != NULL)
	{
		fclose(fp);
		fp = NULL;
		_unlink(szfileName);	//확인 됐으면 삭제(불 필요한 파일 증가 막기 위해) 
		return TRUE;
	}
	else
	{
		CFMSLog::WriteErrorLog("%S, %S", TEXT_LANG[0], szfileName); //%s 는 파일명이 올바르지 않습니다. 다시 입력 하십시요.
	}
	return FALSE;
}
BOOL CFMS::CheckFileNameValidate(CString strFileName, CString &strNewFileName, int nModuleID, int nGroupIndex )
{
	//다른 Module에서 사용중인 파일이지 검사
	CString strTemp;
	//int ID;
	//for(int i =0; i<GetInstalledModuleNum(); i++)
	//{
	//	ID = EPGetModuleID(i);
	//	for(int j=0; j<EPGetGroupCount(ID); j++)
	//	{
	//		if(nModuleID  != ID || nGroupIndex != j)	//자기 자신은 제외
	//		{
	//			if(GetResultFileName(ID, j) == strFileName && !strFileName.IsEmpty())
	//			{
	//				m_strLastErrorString.Format(GetStringTable(IDS_MSG_FILE_NAME_USED), strFileName, ::GetModuleName(ID, j));
	//				AfxMessageBox(m_strLastErrorString);
	//				return FALSE;
	//			}
	//		}
	//	}
	//}

	//파일이 중복되는지 검사 
	CFileFind fileFinder;
	if(fileFinder.FindFile(strFileName))			//Find Board Test Result File
	{
		if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "NoOverWrite", FALSE))
		{
			//Over write 방지 (for LSC)
			CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[41], strFileName); //"%s는 중복된 이름입니다. 다른 이름을 입력하거나 이전 결과를 삭제 후 시작하십시요."
			//AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
			return FALSE;
		}
		else
		{
			//사용자에게 Overwite여부를 물음
			//strTemp.Format(GetStringTable(IDS_MSG_FILE_OVER_WIRTE_CONFIRM), strFileName);
			//if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDNO)
			{
				// 1. 결과파일이 있을경우 파일 이름을 변경해서 저장한다.
				CString strTemp = _T("");
				CString strNewFileName = _T("");

				AfxExtractSubString(strTemp, strFileName, 1,'.');

				if( strTemp == "fmt" )
				{					
					COleDateTime curTime = COleDateTime::GetCurrentTime();
					AfxExtractSubString(strTemp, strFileName, 0,'.');					
					strFileName.Format("%s_%s", strTemp, curTime.Format("%H%M%S"));					
				}
				else
				{
					return false;
				}
			}

		}		
	}

	m_resultfilePathNName = strNewFileName = strFileName;
	//파일명으로 유효 여부 검사 
	return FileNameCheck((LPSTR)(LPCTSTR)strNewFileName);
}

CString CFMS::CreateResultFilePath(int nModuleID, CString strDataFolder, CString strLot, CString strTray)
{
	//모듈별로 분리해서 저장 	
	CFileFind aDirChecker;
	CString strTempFolderName(strDataFolder);

	//1. Lot 구별 폴더 
	if(m_bFolderLot && !strLot.IsEmpty())
	{
		strTempFolderName =  strTempFolderName + "\\" + strLot;
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//2. Tray별 구별 폴더 
	if(m_bFolderTray && !strTray.IsEmpty())
	{
		strTempFolderName =  strTempFolderName + "\\" + strTray;
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//3. 폴더 구별
	if(m_bFolderModuleName)
	{
		strTempFolderName =  strTempFolderName + "\\" + m_pModule->GetModuleName();
		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	//4. 시간 별로 폴더 분리
	if(m_bFolderTime)
	{
		COleDateTime oleTime = COleDateTime::GetCurrentTime();
		if(m_nTimeFolderInterval <= 1)			//1일 단위 생성 
		{
			strTempFolderName += oleTime.Format("\\%Y%m%d");
		}
		else if(m_nTimeFolderInterval <= 10)	//10일 단위 생성
		{
			if(oleTime.GetDay() <= 10)
			{
				strTempFolderName += oleTime.Format("\\%Y%m01");
			}
			else if(oleTime.GetDay() <= 20)
			{
				strTempFolderName += oleTime.Format("\\%Y%m11");
			}
			else
			{
				strTempFolderName += oleTime.Format("\\%Y%m21");				
			}
		}
		else if(m_nTimeFolderInterval <= 20)		//15일 간격 생성 
		{
			if(oleTime.GetDay() <= 15)
			{
				strTempFolderName += oleTime.Format("\\%Y%m01");
			}
			else
			{
				strTempFolderName += oleTime.Format("\\%Y%m16");				
			}

		}
		else
		{
			strTempFolderName += oleTime.Format("\\%Y%m");				
		}

		if(aDirChecker.FindFile(strTempFolderName) == FALSE)
		{
			if(::CreateDirectory(strTempFolderName, NULL) == FALSE)
			{
				return "";
			}
		}
		aDirChecker.Close();
	}

	return strTempFolderName;
}
INT CFMS::ShowRunInformation(int nModuleID, int nGroupIndex, BOOL bNewFileName, long lProcPK = 0)
{
	CString strTemp;
	CFormModule *pModule = m_pModule;
	if(pModule == FALSE)	
		return 0;
	
	CTray *pTray;
	BOOL bAuto = EPGetAutoProcess(nModuleID);

	///폴더와 기본 파일명 생성 ///
	CString strDataPath;
	if(bNewFileName)
	{
		for(int j=0; j<pModule->GetTotalJig(); j++)
		{
			pTray = pModule->GetTrayInfo();
			if(pTray && bAuto == FALSE)	//수동 공정이면 모두 처음 공정처럼 처리 	
			{
				pTray->SetFirstProcess(TRUE);
			}
			
			COleDateTime nowtime(COleDateTime::GetCurrentTime());

			strDataPath = CreateResultFilePath(nModuleID, m_strDataFolder, pModule->GetLotNo(j), pModule->GetTrayNo(j));			
			
			// SKI kky 결과파일 형식 변경( ProcessNo(00)_TrayId(000000)_일시(130826134333).fmt
			strTemp.Format("%s\\%02d_%s_%s.fmt", 
				strDataPath,
				pModule->GetCondition()->GetModelID(),
				pModule->GetTrayNo(j),
				nowtime.Format("%y%m%d%H%M%S")				
				);
			/*
			strTemp.Format("%s\\%d_%s_%s_%s_T%02d.fmt", 
							strDataPath,
							pModule->GetTestSequenceNo(),
							pModule->GetTestName(),
							pModule->GetLotNo(j),
							pModule->GetTrayNo(j),
							j+1
							);	
			*/
			
			pModule->SetResultFileName(strTemp, j);
			CFMSLog::WriteLog("[Stage %s] CreateResultFileName => %s", pModule->GetModuleName(), m_pModule->GetResultFileName() );
		}
	}

	//////////////////////////////////////////////////////////////////////////	
	//자동 진행에서 자동 시작 설정시는 새로운 Tray가 아니고 Group이 Standby 상태 이면 바로 진행 
	//모두 자동 진행 설정시에는 공정 조건이 자동으로 보내지고 Module이 Stanby 상태가 되기전에
	//Start Command가 전송되는 경우가 있어서 상태를 검사 한다.
	//BOOL bAutoRun = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoRun", 0);
	
	//STR_CONDITION_HEADER testInfo = GetPrevTest(lProcPK);
	//BOOL bNewTray = testInfo.lID <=0 ? TRUE: FALSE;	//pTray->IsNewProcedureTray();

	// if((bAutoRun && bAuto && !bNewTray && EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_STANDBY)) 
	if(EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_STANDBY
		|| EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_END
		|| EPGetGroupState(nModuleID, nGroupIndex) == EP_STATE_READY)
	{
		BOOL bAllOk = TRUE;
		for(int j=0; j<pModule->GetTotalJig(); j++)
		{
			CString strTemp;
			if(CheckFileNameValidate(pModule->GetResultFileName(j), strTemp, nModuleID) == FALSE)
			{
				bAllOk = FALSE;
			}
		}
		if(bAllOk)	return 1;	//파일명이 모두 정상이면 자동 실행하고 
	}

	CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[43], pModule->GetModuleName() ); //"[Stage %s] 파일명에 문제가 발생함."
	return 0;
}
BOOL CFMS::SendProcedureLogToDataBase(int nModuleID, int nTrayIndex)
{
	CFormModule *pModule = m_pModule;
	if(pModule == NULL)		return FALSE;

	EP_MD_SYSTEM_DATA *pSysData = EPGetModuleSysData(EPGetModuleIndex(nModuleID));

	int nRtn;
	//CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	//if(pFrame->m_bDBSvrConnect)	//자동 공정 모드이고 DB가 연결되어 있으면 전송 
	//{
	//	CTestCondition *pCondition = pModule->GetCondition();
	//	EP_PROCEDURE_DATA	procData;
	//	ZeroMemory(&procData, sizeof(procData));

	//	//////////////////////////////////////////////////////////////////////////
	//	sprintf(procData.szModuleName, "%s", pModule->GetModuleName());
	//	procData.nModuleID = pModule->GetModuleID();
	//	procData.wModuleGroupNo = pSysData->nModuleGroupNo;
	//	procData.wJigIndex = nTrayIndex;
	//	sprintf(procData.szDateTime,	"%s", pModule->GetRunStartTime().Format());			
	//	sprintf(procData.szModuleIP,	"%s", pModule->GetIPAddress());	
	//	sprintf(procData.szTestName,	"%s", pModule->GetTestName());

	//	sprintf(procData.szTrayNo,		"%s", pModule->GetTrayNo(nTrayIndex));			
	//	sprintf(procData.szLotNo,		"%s", pModule->GetLotNo(nTrayIndex));				
	//	sprintf(procData.szOperatorID,  "%s", pModule->GetOperatorID(nTrayIndex));
	//	sprintf(procData.szTestSerialNo, "%s",  pModule->GetTestSerialNo(nTrayIndex));
	//	sprintf(procData.szResultFileName, "%s", pModule->GetResultFileName(nTrayIndex));
	//	procData.nCellNo		= pModule->GetCellNo(nTrayIndex);
	//	procData.lTestSeqenceNo	=  pModule->GetTestSequenceNo();
	//	procData.lTestID		=  pCondition->GetTestInfo()->lID;
	//	procData.lModelID		= pCondition->GetModelID();
	//	strcpy(procData.szModelName, pCondition->GetModelInfo()->szName);
	//	procData.procedureType	= (BYTE)pCondition->GetTestProcType();
	//	//		procData.installTotalChCount = 	pModule->GetChInJig(nTrayIndex);	//BYTE)::EPGetChInGroup(nModuleID, 0);
	//	procData.wCellInTrayCount = pModule->GetCellCountInTray();
	//	procData.nInputCellCount = pModule->GetInputCellCount(nTrayIndex);
	//	procData.bAutoProcess = EPGetAutoProcess(nModuleID);

	//	memcpy(&procData.mdSysData, ::EPGetModuleSysData(EPGetModuleIndex(nModuleID)), sizeof(EP_MD_SYSTEM_DATA));
	//	memcpy(&procData.sensorMap1, pModule->GetSensorMap(CFormModule::sensorType1), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);
	//	memcpy(&procData.sensorMap2, pModule->GetSensorMap(CFormModule::sensorType2), sizeof(_MAPPING_DATA)*EP_MAX_SENSOR_CH);

	//	//////////////////////////////////////////////////////////////////////////

	//	nRtn = ::dcSendData(DB_CMD_PROCEDURE_DATA, &procData, sizeof(EP_PROCEDURE_DATA));

	//	if(nRtn != EP_ACK)
	//	{
	//		CFMSLog::WriteErrorLog("Procedure Information Send to Data Server Fail. ");
	//		return FALSE;
	//	}
	//}
	return TRUE;
}

BOOL CFMS::SendLastTrayStateData(int nModuleID, int nGroupIndex)
{
	CFormModule *pModule = m_pModule;
	
	if(pModule == NULL)	return FALSE;

	CTray* pTray = pModule->GetTrayInfo(0);
	
	ZeroMemory(&m_runInfo, sizeof(EP_RUN_CMD_INFO));
	
	int nTotalHWCh = EPGetChInGroup(nModuleID, nGroupIndex);	//전송해야할 채널수 
	int nTraychSum = 0;
	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		nTraychSum += pModule->GetChInJig(t);
	}

	CString strTemp;	
	EP_TRAY_STATE	trayState;
	ZeroMemory(&trayState, sizeof(trayState));	

	int nInputCell = 0;
	for (INT i = 0; i < nTotalHWCh; i++)
	{	
		trayState.cellCode[i] = pTray->cellCode[i];
		if( trayState.cellCode[i] == EP_CODE_NORMAL )
		{
			nInputCell++;
		}
	}

	sprintf(m_runInfo.szTestSerialNo, "%s",  pTray->strTestSerialNo );

	if( pTray->GetTrayNo().GetLength() == 18 )
	{
		sprintf(m_runInfo.szTrayID_1st, "%s", pTray->GetTrayNo().Left(6) );
		sprintf(m_runInfo.szTrayID_2nd, "%s", pTray->GetTrayNo().Mid(6, 6));
		sprintf(m_runInfo.szTrayID_3rd, "%s", pTray->GetTrayNo().Right(6) );
	}
	else if( pTray->GetTrayNo().GetLength() == 12 )
	{
		sprintf(m_runInfo.szTrayID_1st, "%s", pTray->GetTrayNo().Left(6) );
		sprintf(m_runInfo.szTrayID_2nd, "%s", pTray->GetTrayNo().Right(6) );
	}
	else
	{
		sprintf(m_runInfo.szTrayID_1st, "%s", pTray->GetTrayNo().Left(6) );
	}

	m_runInfo.nTabDeepth = pTray->m_nTabDeepth;
	m_runInfo.nTrayHeight = pTray->m_nTrayHeight;
	m_runInfo.nTrayType = pTray->m_nTrayType;
	m_runInfo.nCurrentCellCnt = pTray->GetInputCellCnt();
	m_runInfo.nContactErrlimit = pTray->m_nContactErrlimit;
	m_runInfo.nCappErrlimit = pTray->m_nCapaErrlimit;
	m_runInfo.nChErrlimit = pTray->m_nChErrlimit;
	m_runInfo.nInputCell[0] = pTray->GetInputCellCnt();
	//m_runInfo.nTrayKind = pTray->m_nTraykind;   //KSH 20190812 TrayKind

	if(pTray->m_nTraykind == 1)
	{
		m_runInfo.nTrayKind = 0;
	}
	else if(pTray->m_nTraykind == 2)
	{
		m_runInfo.nTrayKind = 1;
	}

	int nRtn;
	if((nRtn = EPSendCommand(nModuleID, nGroupIndex+1, 0, EP_CMD_TRAY_DATA, &trayState, sizeof(trayState))) != EP_ACK)
	{
		CFMSLog::WriteErrorLog("%s :: Grade Data Send Fail");
		return FALSE;
	}
	
	return TRUE;
}

DWORD CFMS::CheckEnableRun(int nModuleID)
{
	CString strTemp;
	DWORD bStateFlag = 0;

	//TrayLoad Check & Crate file 
	CFormModule *pModule = m_pModule;
	if(pModule == NULL)		return 0;

	CTray *pTrayInfo;
	for(int t =0; t<pModule->GetTotalJig(); t++)
	{
		//준비된 Tray
		pTrayInfo = pModule->GetTrayInfo(t);
		if(pTrayInfo->IsWorkingTray())
		{
			////Tray 감지 Sensor Check;
			// 무조건 Run 보내기 - Tray 감지 제외 [2013/9/23 cwm]
			//if(EPTrayState(nModuleID, t) != EP_TRAY_LOAD)
			//{
			//	CFMSLog::WriteErrorLog("%s의 Jig %d번에 Tray[%s]가 감지되지 않아 시작할 수 없습니다.", 
			//		m_pModule->GetModuleName(), t+1, pTrayInfo->GetTrayNo());
			//	return 0;
			//}
			//else
			//{

			//}

			//Create result file
			//20060523
			//기존에 최소 1개의 step이 종료되면 결과 파일이 생성되던 방식을 시험 시작시 결과 파일을 생성하도록 수정
			//실시간 DataDown시 Step1번의 진행 data 경과를 보기 위해  			
			if(pModule->CreateTrayResultFile(t) == FALSE)
			{
				CFMSLog::WriteErrorLog("%s %s", pModule->GetResultFileName(), " file create fail!!!");
				return 0;
			}


			// void CMainFrame::OnSaveDataReceive(WPARAM wParam, LPARAM lParam) 에서 현재 위치로 변경 //2007/02/13
			// DataBase 구조를 파일 구조로 바꾸면서 Step1이 완료되기 이전에도 Step진행 이력을 조회할 수 있도록
			// Step1이 완료는 시점에서 보고하던 부분을 시작과 동시에 보고 하도록 수정(중복시 자동 Overwrite됨)
			// 1. DataServer 로 공정 시작 정보를 보내는 부분
			//SendProcedureLogToDataBase(nModuleID, t);

			bStateFlag |= (0x01<<t);
		}
	}	

	if(bStateFlag == 0)
	{
		CFMSLog::WriteErrorLog((LPSTR)(LPCTSTR)TEXT_LANG[42] //"%s에 작업가능한 Tray번호나 수량이 입력되지 않았습니다.\n작업할 Tray를 삽입한 후 Tray번호와 수량을 정확히 입력 후 재시도 하십시요."
			,	m_pModule->GetModuleName());
	}
	return bStateFlag;
}
BOOL CFMS::fnCalRun() //KSH Calibration 1(SBC에 지그 전진 명령)
{
	CString	strTempM;
		
	//g_nCalistart = 1;

	m_nModuleID = m_pModule->GetModuleID();

	if(m_nModuleID == NULL)	
	{
		return FALSE;
	}
	

	WORD state = m_pModule->GetState();

// 	if(state != EP_STATE_IDLE && state != EP_STATE_STANDBY && state != EP_STATE_MAINTENANCE && state != EP_STATE_READY)
// 	{
// 		CString strTemp;
// 		strTemp.Format(TEXT_LANG[1], GetModuleName(m_nModuleID)); //"%s는 교정 가능한 상태가 아닙니다."
// 		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
// 		return FALSE; 
// 	}
// 	
	ZeroMemory(&m_SelInfo, sizeof(EP_CAL_SELECT_INFO));  //KSH 20190821
// 	m_SelInfo.nMain = 1;
// 	m_SelInfo.nType = 0;
// 	m_SelInfo.nRange = 0;  
// 	m_SelInfo.nBoardNo = 0;
// 
// 	int chCnt = GetHWChCnt(m_SelInfo.nBoardNo);
// 	memset(m_SelInfo.byCnSel, 1, chCnt);
// 	
// 	m_SelInfo.bMutiMode = TRUE;
// 	m_SelInfo.bTrayType = 0;
// 	m_SelInfo.bForceJig = 0;

// 	CCalibratorDlg *pCaliDlg;
// 	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
// 	pCaliDlg =  new CCalibratorDlg(m_nModuleID, pFrame->m_pAllSystemView);
// 	pCaliDlg->m_bStartCmd = TRUE;

// 	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
// 	pFrame->SendMessage(1234,0,0);

	m_pModule->m_bAutoCaliRun = TRUE;

	EP_CAL_SELECT_INFO selInfo;
	ZeroMemory(&selInfo, sizeof(EP_CAL_SELECT_INFO));

	selInfo.bForceJig =2;

	if(EPSendCommand(m_nModuleID, 0, 0, EP_CMD_CAL_START, &selInfo, sizeof(EP_CAL_SELECT_INFO)) != EP_ACK)
	{
		AfxMessageBox(TEXT_LANG[64]); //"JIG CLOSE cmd send failed!"
		return FALSE;
	}

// 	else  //KSH 교정창 Open
// 	{
// 		CCalibratorDlg *pCaliDlg;
// 		CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
// 
// 		if (pFrame->m_apCaliDlg.GetSize() > 0)
// 		{
// 			for (int i=0 ; i< pFrame->m_apCaliDlg.GetSize() ; i++)
// 			{
// 				pCaliDlg = (CCalibratorDlg *)pFrame->m_apCaliDlg[i];
// 				if (pCaliDlg->m_nUnitNo == m_nModuleID)
// 				{
// 					AfxMessageBox(TEXT_LANG[35]);
// 				}
// 			}
// 			pCaliDlg =  new CCalibratorDlg(m_nModuleID, this);
// 			ASSERT(pCaliDlg);
// 
// 			pCaliDlg->m_pdoc = (CCTSMonDoc *)pFrame->GetActiveDocument();
// 			pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
// 			pCaliDlg->ShowWindow(SW_SHOW);
// 			pFrame->m_apCaliDlg.Add(pCaliDlg);
// 		}
// 		else
// 		{
// 			pCaliDlg =  new CCalibratorDlg(m_nModuleID, this);
// 			ASSERT(pCaliDlg);
// 
// 			pCaliDlg->m_pdoc = (CCTSMonDoc *)pFrame->GetActiveDocument();
// 			pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
// 			pCaliDlg->ShowWindow(SW_SHOW);
// 			pFrame->m_apCaliDlg.Add(pCaliDlg);
// 		}
// 	}
		//GetDlgItem(IDC_STATIC_PROGRESS_TITLE)->SetWindowText("AUTO CALIBRATION START");
	
// 	strTempM.Format("AUTO CALIBRATION START", GetModuleName(m_nModuleID));
// 	AfxMessageBox(strTempM);


	return TRUE;
}

// 20201013 KSCHOI Critical Memory Issue Bug Fixed. START
//mapping 정보에서 선택 Board의 Channel 수를 Count 한다.
// int CFMS::GetHWChCnt(int nBoardNo)
// {
// 	//default
// 	EP_MD_SYSTEM_DATA *pSysData = ::EPGetModuleSysData(EPGetModuleIndex(m_nModuleID));
// 	int nTotch = 0;
// 
// 	if(nBoardNo == 0)
// 	{
// 		nTotch = pSysData->wChannelPerBoard;
// 	}
// 	else
// 	{
// //		nTotch = m_calibrtaionData.GetHWChCount(nBoardNo);
// 	}
// 
// 	if(nTotch < 0 || nTotch > EP_MAX_CH_PER_BD)
// 		nTotch = EP_MAX_CH_PER_BD;
// 
// 	return nTotch;
// }
// 20201013 KSCHOI Critical Memory Issue Bug Fixed. END

BOOL CFMS::fnRun()
{
	INT nModuleID = m_pModule->GetModuleID();
	//이전 정보를 Reset하고 보여줄지 여부 
	if(ShowRunInformation(nModuleID, 0, TRUE) < 1)
	{
		return FALSE;
	}

	//Module에 전송 정보 저장 (파일생성 이전에 )
	m_pModule->UpdateStartTime();

	m_pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.	

	//작업 가능상태인지 검사 
	//결과 파일도 생성한다.
	DWORD dwJigFlag = CheckEnableRun(nModuleID);
	if(dwJigFlag == 0)
	{
		return FALSE;
	}

	sprintf(m_runInfo.szTestSerialNo, m_pModule->GetSyncCode());
	
	m_runInfo.dwUseFlag = dwJigFlag;

	m_runInfo.nTabDeepth = m_pModule->GetTrayInfo(0)->m_nTabDeepth;
	m_runInfo.nTrayHeight = m_pModule->GetTrayInfo(0)->m_nTrayHeight;

// 20200803 KSCHOI S2F41 RCMD 1 TRAY HEIGHT BUG FIXED START
//	m_runInfo.nTrayHeight = m_inreserv.Tray_Height;
// 20200803 KSCHOI S2F41 RCMD 1 TRAY HEIGHT BUG FIXED END
	//kveby
	//m_runInfo.nTrayHeight = 0;
	m_runInfo.nTrayType = m_pModule->GetTrayInfo(0)->m_nTrayType;

	// 20200415 KSJ
	if(m_pModule->GetTrayInfo(0)->m_nTraykind == 1)
	{
		m_runInfo.nTrayKind = 0;
	}
	else if(m_pModule->GetTrayInfo(0)->m_nTraykind == 2)
	{
		m_runInfo.nTrayKind = 1;
	}

	m_runInfo.nCurrentCellCnt = m_pModule->GetTrayInfo(0)->lTrayCellCount_1st + m_pModule->GetTrayInfo(0)->lTrayCellCount_2nd;							
	m_runInfo.nContactErrlimit = m_pModule->GetTrayInfo(0)->m_nContactErrlimit;	
	m_runInfo.nCappErrlimit = m_pModule->GetTrayInfo(0)->m_nCapaErrlimit;
	m_runInfo.nJigHeightMode = m_pModule->GetJigHeightMode();

	int nRtn = EP_NACK;
	//ljb Send Run Command
	if((nRtn = EPSendCommand(nModuleID, 0, 0, EP_CMD_RUN, &m_runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)
	{
// 		if( nRtn == EP_TIMEOUT )
// 		{
// 			// 20131205 시작 신호 전송을 실패 했을 경우는 그냥 무시한다.
// 			//
// 		}
// 		else
// 		{
// 			CFMSLog::WriteErrorLog("%s (%s-??)",  TEXT_LANG[46], m_pModule->GetModuleName());
// 			return FALSE;
// 		}
	}

	//2006/03/22 KBH
	//완료된 Step의 결과 data를 표시할 수 있도록 최종 data file reset 한다.
	m_pModule->LoadResultData();

	return TRUE;
}

// 1. 상태가 변경되면 CHARGER_STATE 메세지를 전송
VOID CFMS::fnSetFMSStateCode(UINT mode, FMS_STATE_CODE _code)
{	
	if(m_Mode != mode || m_FMSStateCode != _code)
	{
		BOOL bCtrlModeChk = FALSE;

		if( m_Mode != mode )
		{
			bCtrlModeChk = TRUE;
		}

		if( m_FMSStateCode == FMS_ST_ERROR )
		{
// 20201118 KSCHOI Add Break Fire Report START
			if(this->GetFireAlarmStatus())
			{
				this->SetFireAlarmStatus(FALSE);
				fnSendToHost_S5F3(FALSE);
			}
// 20201118 KSCHOI Add Break Fire Report END
			fnSendToHost_S5F1_Reset();
		}

		m_Mode = mode;
		m_FMSStateCode = _code;
		m_LastState = _code;

		if(_code != EP_STATE_LINE_OFF)
		{
			CString strTemp;
			strTemp.Format("StageLastState_%02d", m_nModuleID);
			AfxGetApp()->WriteProfileInt(FMS_REG, strTemp, m_LastState);
		}

		//////////////////////////////////////////////////////////////////////////
		st_HSMS_PACKET* pPack = fnGetHsmsPack();
		memset(&pPack->head, 0, sizeof(pPack->head));
		memset(pPack->data, 0x20, sizeof(pPack->data));

		st_S6F11C11 data;
		ZeroMemory( &data, sizeof(st_S6F11C11));

		pPack->head.nDevId = 1;
		pPack->head.lSysByte = 0;
		pPack->head.nStrm = 6;
		pPack->head.nFunc = 11;
		pPack->head.nId = 11;		

		sprintf(data.STAGENUMBER, "%05d", fnGetStageNo() );

		st_IMPOSSBLE_STATE impossble_response;
		memset(&impossble_response, 0, sizeof(st_IMPOSSBLE_STATE));
		fnGetFMSImpossbleCode(impossble_response);

		data.CTRLMODE = impossble_response.CTRLMODE;
		data.EQSTATUS = impossble_response.EQSTATUS;
		
		int nOperationMode = m_pModule->GetOperationMode();	

		if(nOperationMode == EP_OPERATION_CALIBRATION)
		{
			data.CTRLMODE = 2;
		}
		
// 		if(data.EQSTATUS == FMS_ST_TRAY_IN && data.CTRLMODE == 2) // 20190926 KSJ CALI
// 		{
// // 			g_nCalistart = 1;
// // 			INT nModuleID = m_pModule->GetModuleID();
// // 			CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
// // 			pFrame->m_pAllSystemView->DrawModule(nModuleID, FALSE);
// 
// 		}

		memcpy(pPack->data, &data, sizeof(data));	

		if( bCtrlModeChk == TRUE )
		{
			pPack->head.nId = 100;
			theApp.m_HSMS_SendQ.Push(*pPack);
		}
		else
		{
			theApp.m_HSMS_SendQ.Push(*pPack);
		}
	}	
}

FMS_STATE_CODE CFMS::fnGetFMSStateCode()
{
	return m_FMSStateCode;
}

VOID CFMS::fnGetFMSImpossbleCode(st_IMPOSSBLE_STATE& _iState)
{
	switch(m_Mode)
	{
	case EQUIP_ST_OFF:
	case EQUIP_ST_LOCAL:
	case EQUIP_ST_MAINT:
		_iState.CTRLMODE = 1;
		_iState.EQSTATUS = m_FMSStateCode;
		break;
	case EQUIP_ST_AUTO:
		_iState.CTRLMODE = 0;
		_iState.EQSTATUS = m_FMSStateCode;
		break;
//KSH Calibration 20190830
	case EQUIP_ST_CALI:
		_iState.CTRLMODE = 2;
		_iState.EQSTATUS = m_FMSStateCode;
		break;
	}
}
VOID CFMS::fnGetFMSNewImpossbleCode(st_NEW_IMPOSSBLE_STATE& _iState)
{
	switch(m_Mode)
	{
	case EQUIP_ST_OFF:
	case EQUIP_ST_LOCAL:
	case EQUIP_ST_MAINT:
		_iState.CTRLMODE = 1;
		_iState.EQSTATUS = m_FMSStateCode;
		break;
	case EQUIP_ST_AUTO:
		_iState.CTRLMODE = 0;
		_iState.EQSTATUS = m_FMSStateCode;
// KSCHOI 20200526 S1F102 CTRLMODE Bug Fix START
		break;
// KSCHOI 20200526 S1F102 CTRLMODE Bug Fix END
	case EQUIP_ST_CALI:
		_iState.CTRLMODE = 2;
		_iState.EQSTATUS = m_FMSStateCode;


		break;
	}
}
INT CFMS::fnGetStageNo()
{
	return atoi(m_pModule->GetModuleName().Right(5));
	// return (m_pModule->GetModuleID()) + m_LineNo;
}


bool CFMS::fnSetTrayID(CString trayID)
{
	if(trayID.GetLength() == 12)
	{
		m_pModule->GetTrayInfo()->SetTrayNo(trayID);
		return TRUE;
	}
	return false;
}


CString CFMS::fnGetTrayID_1()
{
	CString strTrayNo = _T("      ");

	if( m_pModule->GetTrayInfo()->GetTrayNo().GetLength() != 0 )
	{
		strTrayNo = m_pModule->GetTrayInfo()->GetTrayNo().Left(6);
	}

// 	if(strlen(m_inreserv.Tray_ID_1) == 0)
// 		strcpy_s(m_inreserv.Tray_ID_1, "      ");
	return strTrayNo;
}

CString CFMS::fnGetTrayID_2()
{
	CString strTrayNo = _T("      ");
	if(m_pModule->GetTrayNo().GetLength() == 12)
	{
		strTrayNo = m_pModule->GetTrayInfo()->GetTrayNo().Right(6);
	}

// 	if(strlen(m_inreserv.Tray_ID_2) == 0)
// 		strcpy_s(m_inreserv.Tray_ID_2, "      ");
	return strTrayNo;
}

CString CFMS::Time_Conversion(CString str_Time)
{
// 	CString Mon[7], str_Mon_Time;
// 	
// 	const char *pdest;
// 	bool bChkMorningTime = false;		// 오전, 오후 확인
// 	
// 	pdest = strstr(str_Time, TEXT_LANG[44]); //"오전"
// 	
// 	if( pdest != NULL )
// 	{
// 		// 오전
// 		bChkMorningTime = true;
// 	}
// 	else
// 	{
// 		// 오후
// 		bChkMorningTime = false;
// 	}
// 	
// 	str_Time.Replace(" "+TEXT_LANG[44],"-00-");
// 	str_Time.Replace(" "+TEXT_LANG[45],"-12-");//"오후"
// 	str_Time.Replace(":","-");
// 
// 	for(int i = 0; i < 7; i++)
// 	{
// 		AfxExtractSubString(Mon[i], str_Time, i, '-');
// 	}
// 
// 	// 오후 12시일 경우 예외처리
// 	int Mon_Time = 0;
// 	
// 	if( bChkMorningTime == true )
// 	{
// 		// 오전 12시일 경우 0시로 처리
// 		if( atoi(Mon[4]) == 12 )
// 		{
// 			Mon_Time = 0;
// 		}
// 		else
// 		{
// 			Mon_Time = atoi(Mon[4]);
// 		}	
// 
// 		str_Mon_Time.Format("%02d", Mon_Time);		
// 	}
// 	else
// 	{
// 		if( atoi(Mon[4]) == 12 )
// 		{
// 			// 오후 12시일 경우 처리하지 않음
// 			Mon_Time = atoi(Mon[4]);
// 		}
// 		else
// 		{
// 			Mon_Time = atoi(Mon[3]) + atoi(Mon[4]);
// 		}	
// 
// 		str_Mon_Time.Format("%02d", Mon_Time);	
// 	}
// 
// 	str_Time = "";
// 	str_Time += Mon[0];
// 	str_Time += Mon[1];
// 	str_Time += Mon[2];
// 	str_Time += str_Mon_Time;
// 	str_Time += Mon[5];
// 	str_Time += Mon[6];
// 
// 	return str_Time;


	CString Mon[7], str_Mon_Time;	

	str_Time.Replace(" ","-");
	str_Time.Replace(":","-");

	for(int i = 0; i < 7; i++)
	{
		AfxExtractSubString(Mon[i], str_Time, i, '-');
		Mon[i].Trim();
	}

	str_Time = "";
	str_Time += Mon[0];
	str_Time += Mon[1];
	str_Time += Mon[2];
	str_Time += Mon[3];
	str_Time += Mon[4];
	str_Time += Mon[5];
	str_Time += Mon[6];

	return str_Time;
}

// ok				0
// file error		1
// Contact Error	2
// Capa Error		3
// Over Charge		4
// FMS 결과데이터 작성
FMS_ERRORCODE CFMS::fnMakeResult()
{	
 	FMS_ERRORCODE _error = FMS_ER_NONE;
	TCHAR errorMessage[1024] = {0};
	try
	{
		CFormModule *pModule = m_pModule;
		if(pModule != NULL)
		{
			m_resultfilePathNName = pModule->GetResultFileName();		
		}
		else
		{
			return ER_File_Not_Found;
		}

		CString strFile = m_resultfilePathNName;
		CString strConFile = strFile.Left( strFile.ReverseFind('.'));
		strConFile = strConFile + "_CON.Fmt";

		CFMSLog::WriteLog("[Stage %s] fnMakeResult Start For FMS => FileName:%s, ConFileName:%s", m_pModule->GetModuleName(), strFile, strConFile);

		// 1. 셀체크 데이터 확인 후 
		// 2. 결과데이터를 가져온다.
		if( pModule->m_resultFile.ReadConFile(strConFile) != 1 )
		{
			CFMSLog::WriteLog("[Stage %s] File Read Fail %s", m_pModule->GetModuleName(), strConFile);
			return ER_File_Not_Found;
		}

		if(pModule->m_resultFile.ReadFile(strFile, 0) != 1)
		{
			CFMSLog::WriteLog("[Stage %s] File Read Fail %s", m_pModule->GetModuleName(), strFile);
			return ER_File_Not_Found;
		}

		CString strTmp;
		EP_FILE_HEADER		*lpFileHeader = NULL;
		STR_SAVE_CH_DATA	*lpChannel1 = NULL;
		STR_STEP_RESULT		*lpStepData1 = NULL;    
		STR_CONDITION_HEADER *lpModelData = NULL;

		int nStepCnt = pModule->m_resultFile.GetLastIndexStepNo();	//2020-02-19 ksj

		INT nStepIndex = 0;
		BOOL bCheckfileloss = FALSE;
		for( nStepIndex=0; nStepIndex < nStepCnt; nStepIndex++)
		{
			lpStepData1 = pModule->m_resultFile.GetStepData(nStepIndex);
			if( lpStepData1 == NULL )
			{
				CFMSLog::WriteLog("[Stage %s] Step Data Loss Step : %d", m_pModule->GetModuleName(), nStepIndex);
				bCheckfileloss = TRUE;
				break;
			}
		}

		if( bCheckfileloss == TRUE )
		{
			// 1. 손실 복구 실행
			// 2. 결과 파일 다시 열기
			// 		if( pModule->RestoreLostData_new() == TRUE )
			// 		{	
			// 			if(pModule->m_resultFile.ReadFile(strFile, 0) != 1)
			// 			{
			// 				return ER_File_Not_Found;
			// 			}
			// 
			// 			pModule->LoadResultData();
			// 		}
			// 		else
			{
				return ER_File_Not_Found;
			}
		}

		lpFileHeader = pModule->m_resultFile.GetFileHeader();
		if(lpFileHeader == NULL) return ER_File_Open_error;    

		memset(m_sStepResult, 0x00, sizeof(st_Result_Step_Result)*100);
		memset(&m_sResultFile, 0x00, sizeof(st_RESULT_FILE));

		sprintf_s(m_sResultFile.Stage_No, "%05d", fnGetStageNo() );
		m_sResultFile.RESULTTYPE = lpFileHeader->nResultFileType;
		m_sResultFile.TRAYTYPE = lpFileHeader->nTrayType;

		CTestCondition *pTestConditon = pModule->GetCondition();

		sprintf_s(m_sResultFile.PROCESSTYPE, "%4s",  pTestConditon->GetModelInfo()->szProcesstype);

		m_sResultFile.PROCESSNO = pTestConditon->GetTestInfo()->lID;

		sprintf_s(m_sResultFile.BACHNO, "%s",  pModule->GetLotNo());

		m_sResultFile.TotalStep = nStepCnt-1;		// END 스텝 제외

		CString strTrayNo = pModule->m_resultFile.GetTrayNo();
		if( strTrayNo.GetLength() == 12 )
		{
			m_sResultFile.nTraykind = 2;

			sprintf_s(m_sResultFile.TRAYID_1, "%s", strTrayNo.Left(6).Trim() );
			m_sResultFile.TRAYID_1;
			m_sResultFile.TRAYPOSITION_1 = 1;

			sprintf_s(m_sResultFile.TRAYID_2, "%s", strTrayNo.Right(6).Trim() );
			m_sResultFile.TRAYID_2;
			m_sResultFile.TRAYPOSITION_2 = 2;
		}
		else
		{		
			m_sResultFile.nTraykind = 1;

			sprintf_s(m_sResultFile.TRAYID_1, "%s", strTrayNo.Left(6).Trim() );
			m_sResultFile.TRAYID_1;
			m_sResultFile.TRAYPOSITION_1 = 1;
		}


		// 1. ContackCheck 데이터확인
		lpStepData1 = pModule->m_resultFile.GetConStepData();
		if( lpStepData1 != NULL )
		{
			for(int i = 0; i<MAX_CELL_COUNT ; i++)
			{
				if(i < lpStepData1->aChData.GetSize())
				{
					lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(i);
					m_sResultFile.sContactResult[i].Initial_Vol = lpChannel1->fStartVoltage;
					m_sResultFile.sContactResult[i].Contact_Vol = lpChannel1->fVoltage;
					m_sResultFile.sContactResult[i].Contact_Cur = lpChannel1->fCurrent;
					m_sResultFile.sContactResult[i].Delta_OCV = lpChannel1->fDeltaOCV;
				}
			}
		}

		INT a = 0;
		INT t = 0;
		INT stepCnt = 0;
		for(a=0; a < nStepCnt; a++)
		{
			lpStepData1 = pModule->m_resultFile.GetStepData(a);

			if( lpStepData1->stepCondition.stepHeader.type == EP_TYPE_END )
			{
				break;
			}

			stepCnt++;

			m_sStepResult[a].StepNo = a+1;
			if( lpStepData1 == NULL )
			{
				// 1. 중간에 빠진 결과 데이터가 존재할 경우
				continue;
			}

			m_sStepResult[a].Action = lpStepData1->stepCondition.fmsType;		

			for(int i = 0; i<MAX_CELL_COUNT; i++)
			{
				if(i < lpStepData1->aChData.GetSize())
				{
					lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(i);
					CHAR* pconCode = fnGetChannelCode(lpChannel1->channelCode);
					// 				if(strcmp(pconCode, "02") != 0)
					// 				{
					m_sStepResult[a].Voltage[i] = lpChannel1->fVoltage;
					m_sStepResult[a].Capacity[i] = lpChannel1->fCapacity;
					m_sStepResult[a].CutCurrent[i] = lpChannel1->fCurrent;
					m_sStepResult[a].StepEndTime[i] = lpChannel1->fStepTime;

					m_sStepResult[a].CC_Time[i] = lpChannel1->fCcRunTime;
					m_sStepResult[a].CC_Capacity[i] = lpChannel1->fCcCapacity;
					m_sStepResult[a].CV_Time[i] = lpChannel1->fCvRunTime;
					m_sStepResult[a].CV_Capacity[i] = lpChannel1->fCvCapacity;

					m_sStepResult[a].Delta_Capacity[i] = lpChannel1->fDeltaCapacity;
					m_sStepResult[a].StartVoltage[i] = lpChannel1->fStartVoltage;
					m_sStepResult[a].ChecktimeVoltage[i] = lpChannel1->fTimeGetChargeVoltage;

					for( t=0; t<16; t++ )
					{
						m_sStepResult[a].CheckTimeTemp[i][t] = lpChannel1->fJigTemp[t];
						//19-01-09 엄륭 DCIR 관련 MES 수정
						m_sStepResult[a].AverageTimeTemp[i][t] = lpChannel1->fAvrJigTemp[t];
					}

					//19-01-09 엄륭 DCIR 관련 MES 수정 : DCIR 측정값, 평균 온도값				
					m_sStepResult[a].DCIR_Measure[i] = lpChannel1->fImpedance;
					//				}				
				}
			}
		}

		//저장한 스텝의 제일 마지막
		lpStepData1 = pModule->m_resultFile.GetStepData(pModule->m_resultFile.GetLastIndexStepNo() - 1);	//2020-02-19 ksj

		if(lpStepData1 != NULL)
		{
			INT ContactErrorCnt = 0;
			int h = 0;

			for(h = 0; h <MAX_CELL_COUNT; h++)
			{
				if(h < lpStepData1->aChData.GetSize())
				{
					lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(h);

					strTmp = fnGetChannelCode(lpChannel1->channelCode);

					if( strTmp == _T("CF03") )
					{
						strTmp = _T("");					
					}

					sprintf(m_sResultFile.arCellInfo[h].Cell_Info, "%4s", strTmp);
					sprintf(m_sResultFile.arCell_ID[h].Cell_ID, "%20s", pModule->m_resultFile.GetCellSerial(h));
				}
			}		

			CString StartTime, EndTime, strTime;
			EndTime.Format("%s", lpStepData1->stepHead.szEndDateTime);
			strTime.Format("%14s",EndTime.Left(14)); // 20190925 ksj
			sprintf(m_sResultFile.ETIME, "%s", strTime);


			lpStepData1 = pModule->m_resultFile.GetFirstStepData();
			StartTime.Format("%s", lpStepData1->stepHead.szStartDateTime);
			strTime.Format("%14s",StartTime.Left(14));	// 20190925 ksj
			sprintf(m_sResultFile.STIME, "%s", strTime);
		}
	}
	catch (CMemoryException* e)
	{
		e->GetErrorMessage(errorMessage, 1024);
		CFMSLog::WriteLog("[Stage %s] fnMakeResult Memory Exception %s", m_pModule->GetModuleName(), errorMessage);
	}
	catch (CFileException* e)
	{
		e->GetErrorMessage(errorMessage, 1024);
		CFMSLog::WriteLog("[Stage %s] fnMakeResult File Exception %s", m_pModule->GetModuleName(), errorMessage);
	}
	catch (CException* e)
	{
		e->GetErrorMessage(errorMessage, 1024);
		CFMSLog::WriteLog("[Stage %s] fnMakeResult Exception %s", m_pModule->GetModuleName(), errorMessage);
	}

	CFMSLog::WriteLog("[Stage %s] fnMakeResult End For FMS", m_pModule->GetModuleName());
	return _error;	
}

FMS_ERRORCODE CFMS::fnMakeContactResult()
{	
	FMS_ERRORCODE _error = FMS_ER_NONE;

	CFormModule *pModule = m_pModule;
	if(pModule != NULL)
	{
		m_resultfilePathNName = pModule->GetResultFileName();		
	}
	else
	{
		return ER_File_Not_Found;
	}

	CString strFile = m_resultfilePathNName;
	CString strConFile = strFile.Left( strFile.ReverseFind('.'));
	strConFile = strConFile + "_CON.Fmt";

	CFMSLog::WriteLog("[Stage %s] fnMakeResult Start For FMS => FileName:%s, ConFileName:%s", m_pModule->GetModuleName(), strFile, strConFile);

	if( pModule->m_resultFile.ReadConFile(strConFile) != 1 )
	{
		return ER_File_Not_Found;
	}
	
	CString strTmp;
	STR_SAVE_CH_DATA	*lpChannel1 = NULL;
	STR_STEP_RESULT		*lpStepData1 = NULL;
	EP_FILE_HEADER		*lpFileHeader = NULL;

	lpFileHeader = pModule->m_resultFile.GetFileHeader();
	if(lpFileHeader == NULL) return ER_File_Open_error;
	
	memset(&m_sResultFile, 0x00, sizeof(st_RESULT_FILE));

	sprintf_s(m_sResultFile.Stage_No, "%05d", fnGetStageNo() );
	m_sResultFile.RESULTTYPE = lpFileHeader->nResultFileType;
	m_sResultFile.TRAYTYPE = lpFileHeader->nTrayType;
	m_sResultFile.nTraykind = lpFileHeader->nTraykind;

	CTestCondition *pTestConditon = pModule->GetCondition();
	sprintf_s(m_sResultFile.PROCESSTYPE, "%4s",  pTestConditon->GetModelInfo()->szProcesstype);
	m_sResultFile.PROCESSNO = pTestConditon->GetTestInfo()->lID;
	sprintf_s(m_sResultFile.BACHNO, "%s",  pModule->GetLotNo());

	CString strTrayNo = pModule->m_resultFile.GetTrayNo();
	if( strTrayNo.GetLength() == 12 )
	{
		sprintf_s(m_sResultFile.TRAYID_1, "%s", strTrayNo.Left(6).Trim() );		
		m_sResultFile.TRAYPOSITION_1 = 1;

		sprintf_s(m_sResultFile.TRAYID_2, "%s", strTrayNo.Right(6).Trim() );		
		m_sResultFile.TRAYPOSITION_2 = 2;

		m_sResultFile.nTraykind = 2;
	}
	else
	{		
		sprintf_s(m_sResultFile.TRAYID_1, "%s", strTrayNo.Left(6).Trim() );		
		m_sResultFile.TRAYPOSITION_1 = 1;

		m_sResultFile.nTraykind = 1;
	}

	// 1. ContackCheck 데이터확인
	lpStepData1 = pModule->m_resultFile.GetConStepData();
	if( lpStepData1 != NULL )
	{
		for(int i = 0; i<MAX_CELL_COUNT ; i++)
		{
			if(i < lpStepData1->aChData.GetSize())
			{
				lpChannel1 = (STR_SAVE_CH_DATA *)lpStepData1->aChData.GetAt(i);
				m_sResultFile.sContactResult[i].Initial_Vol = lpChannel1->fStartVoltage;
				m_sResultFile.sContactResult[i].Contact_Vol = lpChannel1->fVoltage;
				m_sResultFile.sContactResult[i].Contact_Cur = lpChannel1->fCurrent;
				m_sResultFile.sContactResult[i].Delta_OCV = lpChannel1->fDeltaOCV;

				strTmp = fnGetChannelCode(lpChannel1->channelCode);

				if( strTmp == _T("CF03") )
				{
					strTmp = _T("");					
				}

				sprintf(m_sResultFile.arCellInfo[i].Cell_Info, "%4s", strTmp);
				//sprintf(m_sResultFile.arCellInfo[i].Cell_Info, "%4s", fnGetChannelCode(lpChannel1->channelCode));
				sprintf(m_sResultFile.arCell_ID[i].Cell_ID, "%20s", pModule->m_resultFile.GetCellSerial(i));				
			}
		}
	}		

	CString StartTime, EndTime, strTime;
	EndTime.Format("%s", lpStepData1->stepHead.szEndDateTime);
	strTime.Format("%14s",EndTime);
	sprintf(m_sResultFile.ETIME, "%s", strTime);

	// lpStepData1 = pModule->m_resultFile.GetFirstStepData();
	StartTime.Format("%s", lpStepData1->stepHead.szStartDateTime);
	strTime.Format("%14s",StartTime);
	sprintf(m_sResultFile.STIME, "%s", strTime);

	return _error;	
}

//0 정상변경
//1 변경실패
//2 작업 없음
FMS_ERRORCODE CFMS::fnSetOperationMode(WORD _code)
{
	if(m_pModule->GetOperationMode() != _code)
	{
		switch(_code)
		{
		case EP_OPERATION_AUTO:
			{
				if( EPGetAutoProcess( m_pModule->GetModuleID()) == TRUE )
				{
					return ER_SBC_ConditionError;
				}

				if(EPSetLineMode(m_pModule->GetModuleID(), 0+1, EP_OPERATION_MODE, _code) == FALSE)
				{
					CFMSLog::WriteLog("[Stage %s] Control Mode Setting Fail.", m_pModule->GetModuleName());		
					return ER_SBC_ConditionError;
				}
			}
			break;

		case EP_OPERATION_LOCAL:
			{
				if(EPSetLineMode(m_pModule->GetModuleID(), 0+1, EP_OPERATION_MODE, _code) == FALSE)
				{
					CFMSLog::WriteLog("[Stage %s] Control Mode Setting Fail.", m_pModule->GetModuleName());		
					return ER_SBC_ConditionError;
				}	
			}
			break;

		case EP_OPERATION_CALIBRATION: //19-02-15 엄륭 MES 관련 사양변경
			{
				if( EPGetAutoProcess( m_pModule->GetModuleID()) == TRUE )
				{
					return ER_SBC_ConditionError;
				}

				if(EPSetLineMode(m_pModule->GetModuleID(), 0+1, EP_OPERATION_MODE, _code) == FALSE)
				{
					CFMSLog::WriteLog("[Stage %s] Control Mode Setting Fail.", m_pModule->GetModuleName());		
					return ER_SBC_ConditionError;
				}	
			}
			break;

		default:
			return ER_Settiing_Data_error;
		}
	}

	return FMS_ER_NONE;
}

FMS_ERRORCODE CFMS::SendSBCInitCommand()
{
	CFormModule *pModule = m_pModule;
	if(pModule == NULL)
	{
		//return ER_Stage_Setting_Error;
		return ER_Status_Error;
	}

	WORD state = EPGetGroupState(pModule->GetModuleID());
	BYTE byTemp;
	//	if(state != EP_STATE_STANDBY && state != EP_STATE_READY && state != EP_STATE_END)
	if(state == EP_STATE_RUN)	
	{
		//CFMSLog::WriteErrorLog("%s :: [초기화] 명령 전송가능 상태가 아닙니다.(state : %s)", ::GetModuleName(nModuleID, nGroupIndex), GetStateMsg(state, byTemp));
		//return ER_Stage_Setting_Error;
		return ER_Status_Error;
	}

	int nRtn;
	if((nRtn = EPSendCommand(pModule->GetModuleID(), 0+1, 0, EP_CMD_CLEAR)) != EP_ACK)
	{
		//m_strLastErrorString.Format(GetStringTable(IDS_MSG_COMMAND_SEND_FAILE), ::GetModuleName(nModuleID, nGroupIndex));
		//m_strLastErrorString = m_strLastErrorString + " ("+ CmdFailMsg(nRtn) +")";
		//AfxMessageBox(m_strLastErrorString);
		//return ER_Stage_Setting_Error;
	}

	ZeroMemory( m_sStepResult, sizeof(st_Result_Step_Result)*100);
	ZeroMemory( &m_sResultFile, sizeof(st_RESULT_FILE) );
	ZeroMemory(&m_inreserv, sizeof(st_CHARGER_INRESEVE));
	
	// Sleep(100);

	//예약된 공정을 Reset 시킨다.
	// 종료후 정보 유지 [2013/9/23 cwm]
	//pModule->ResetGroupData();

	return FMS_ER_NONE;
}

FMS_ERRORCODE CFMS::SendGUIInitCommand()
{
	m_pModule->ResetGroupData();

	return FMS_ER_NONE;
}

VOID CFMS::fnSendToHost_S5F1_Reset()
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = 1;
	pPack->head.lSysByte = 0;
	pPack->head.nStrm = 5;
	pPack->head.nFunc = 1;
	pPack->head.nId = 0;	

	st_S5F1 data;
	ZeroMemory( &data, sizeof(st_S5F1));

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));

	data.ALST = 0;
	data.ALID = 0;
	
	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S5F1( CString alarmCode, CString alarmText )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = 1;
	pPack->head.lSysByte = 0;
	pPack->head.nStrm = 5;
	pPack->head.nFunc = 1;
	pPack->head.nId = 0;	

	st_S5F1 data;
	ZeroMemory( &data, sizeof(st_S5F1));

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));

	data.ALST = 1;
	data.ALID = atoi(alarmCode);
		
	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);

	m_bError = TRUE;
}

// 20201118 KSCHOI Add Break Fire Report START
VOID CFMS::fnSendToHost_S5F3( BOOL bSet, short nDevId, BOOL bTrayState, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 5;
	pPack->head.nFunc = 3;
	pPack->head.nId = 0;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S5F3 data;
	ZeroMemory( &data, sizeof(st_S5F3));

	if(bSet)
	{
		data.STATUS = 9;
	}
	else
	{
		data.STATUS = 0;
	}
	
	if(bTrayState)
	{
		sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
		data.TRAYPOSITION_1 = 1;

		sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
		data.TRAYPOSITION_2 = 2;
	}

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}
// 20201118 KSCHOI Add Break Fire Report END

VOID CFMS::fnSendToHost_S5F102( short nDevId, long lSysByte, FMS_ERRORCODE _rec )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 5;
	pPack->head.nFunc = 102;
	pPack->head.nId = 0;	

	st_S5F102 data;
	ZeroMemory( &data, sizeof(st_S5F102));

	if( _rec == FMS_ER_NONE )
	{
		data.ACK = 0;
	}
	else
	{
		data.ACK = 1;
	}

	memcpy(pPack->data, &data, sizeof(data));
	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C4( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 4;
		
	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C4 data;
	ZeroMemory( &data, sizeof(st_S6F11C4));

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));

	data.fmsERCode = fnMakeResult();
	
	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C1( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 1;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C1 data;
	ZeroMemory( &data, sizeof(st_S6F11C1));

	sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;
	
	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C5( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 5;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C5 data;
	ZeroMemory( &data, sizeof(st_S6F11C5));

	sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C12( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 12;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C12 data;
	ZeroMemory( &data, sizeof(st_S6F11C12));

	sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C13( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 13;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C13 data;
	ZeroMemory( &data, sizeof(st_S6F11C13));

	sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C14( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 14;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C14 data;
	ZeroMemory( &data, sizeof(st_S6F11C14));

	sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}


VOID CFMS::fnSendToHost_S6F11C6( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 6;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C6 data;
	ZeroMemory( &data, sizeof(st_S6F11C6));

	sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C7( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 7;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C7 data;
	ZeroMemory( &data, sizeof(st_S6F11C7));

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));

	data.fmsERCode = fnMakeContactResult();

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C15( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 15;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C15 data;
	ZeroMemory( &data, sizeof(st_S6F11C15));

	sprintf(data.TRAYID_1, "%10s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%10s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}


VOID CFMS::fnSendToHost_S6F11C16( short nDevId, long lSysByte) //19-01-24 엄륭 MES 사양 변경
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 16;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C16 data;
	ZeroMemory( &data, sizeof(st_S6F11C16));

	if(m_pModule->GetTrayNo().GetLength() == 12)
	{
		sprintf(data.TRAYID_1, "%10s", fnGetTrayID_1());//KSH Calibration 20190830
		data.TRAYPOSITION_1 = 1;

		sprintf(data.TRAYID_2, "%10s", fnGetTrayID_2());//KSH Calibration 20190830
		data.TRAYPOSITION_2 = 2;
	}
	else
	{
		sprintf(data.TRAYID_1, "%10s", fnGetTrayID_1());//KSH Calibration 20190830
		data.TRAYPOSITION_1 = 1;
	}
	//READY
// 	EP_GP_STATE_DATA prevState;
// 	
// 	memcpy(&prevState, (const void *)&EPGetGroupData(m_pModule->GetModuleID(), NULL).gpState, sizeof(EP_GP_STATE_DATA));
// 	prevState.state = EP_STATE_READY;
// 	
// 	CCTSMonDoc pDoc = 
// 	m_pModule = pDoc->GetModuleInfo(nModuleID);
// 	m_pModule->SetState(prevState);


	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C17( short nDevId, long lSysByte ) //19-01-24 엄륭 MES 사양 변경
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 17;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C17 data;
	ZeroMemory( &data, sizeof(st_S6F11C17));

	sprintf(data.TRAYID_1, "%10s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%10s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C18( short nDevId, long lSysByte ) //19-01-24 엄륭 MES 사양 변경
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 18;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C18 data;
	ZeroMemory( &data, sizeof(st_S6F11C18));

	sprintf(data.TRAYID_1, "%10s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%10s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S6F11C19( short nDevId, long lSysByte ) //19-01-24 엄륭 MES 사양 변경
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 19;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C19 data;
	ZeroMemory( &data, sizeof(st_S6F11C19));

	sprintf(data.TRAYID_1, "%10s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%10s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}



VOID CFMS::fnSendToHost_S6F11C200( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 6;
	pPack->head.nFunc = 11;
	pPack->head.nId = 200;

	FMS_ERRORCODE _rec = FMS_ER_NONE;

	st_S6F11C200 data;
	ZeroMemory( &data, sizeof(st_S6F11C200));

	sprintf(data.TRAYID_1, "%s", fnGetTrayID_1() );
	data.TRAYPOSITION_1 = 1;

	sprintf(data.TRAYID_2, "%s", fnGetTrayID_2());
	data.TRAYPOSITION_2 = 2;

	sprintf(data.STAGENUMBER, "%s", m_pModule->GetModuleName().Right(5));	

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S2F42( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 2;
	pPack->head.nFunc = 42;

	st_S2F42 data;
	ZeroMemory( &data, sizeof(st_S2F42));

	if( fmsERCode == FMS_ER_NONE )
	{
		data.ACK = 0;
	}
	else if( fmsERCode == ER_NO_Process )
	{
		data.ACK = 2;
	}
	else
	{
		data.ACK = 1;
	}

	sprintf(data.STAGENUMBER, "%05d", fnGetStageNo() );

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS::fnSendToHost_S1F18( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode )
{
	st_HSMS_PACKET* pPack = fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 1;
	pPack->head.nFunc = 18;

	st_S1F18 data;
	ZeroMemory( &data, sizeof(st_S1F18));

	sprintf(data.STAGENUMBER, "%05d", fnGetStageNo() );

	if( fmsERCode != FMS_ER_NONE )
	{
		data.ACK = 1;
	}
	else
	{
		data.ACK = 0;
	}
	
	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

// 20201118 KSCHOI Add Break Fire Report START
VOID CFMS::SetFireAlarmStatus(BOOL bStatus)
{
	CString csBreakFireReportStatus = AfxGetApp()->GetProfileString(FMS_REG , "BreakFireReportStatus", "");

	if(m_nModuleID-1 < csBreakFireReportStatus.GetLength())
	{
		csBreakFireReportStatus.SetAt(m_nModuleID-1, bStatus?'1':'0');
		AfxGetApp()->WriteProfileString(FMS_REG, "BreakFireReportStatus", csBreakFireReportStatus);
	}
}

BOOL CFMS::GetFireAlarmStatus()
{
	CString csBreakFireReportStatus = AfxGetApp()->GetProfileString(FMS_REG , "BreakFireReportStatus", "");

	if(m_nModuleID-1 < csBreakFireReportStatus.GetLength())
	{
		return csBreakFireReportStatus.GetAt(m_nModuleID-1) == '1'?TRUE:FALSE;
	}

	return FALSE;
}
// 20201118 KSCHOI Add Break Fire Report END

// 20210105 KSCHOI Add SBCNetworkCheck START
BOOL CFMS::SendSBCNetworkCheck()
{
	TCHAR pErrorMsg[256];

	try
	{
		BOOL bResult = EPUpdateModuleLineMode(m_pModule->GetModuleID());;

		if(!bResult)
		{
			CFMSLog::WriteLog("[Stage %s] SendSBCNetworkCheck Error.", m_pModule->GetModuleName());
		}

		return bResult;
	}	
	catch (CException* e)
	{
		e->GetErrorMessage(pErrorMsg, 256);
		CFMSLog::WriteLog("[Stage %s] SendSBCNetworkCheck Exception %s.", m_pModule->GetModuleName(), pErrorMsg);
	}

	return FALSE;
}
// 20210105 KSCHOI Add SBCNetworkCheck END

/*
FMS_ERRORCODE CFMS::fnSend_E_HEAET_BEAT_P(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	return _error;
}
FMS_ERRORCODE CFMS::fnSend_E_TIME_SET_RESPONSE(FMS_ERRORCODE _rec)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;


	return _error;
}
FMS_ERRORCODE CFMS::fnSend_E_ERROR_NOTICE_RESPONSE(FMS_ERRORCODE _rec)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;


	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_MODE_CHANGE_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_MODE_CHANGE_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_MODE_CHANGE_RESPONSE));

	sprintf(data.Equipment_Num, "%03d", fnGetStageNo());

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_MODE_CHANGE_RESPONSE_Map);

	pPack->dataLen = dataLen;

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	case ER_Data_Error:
		_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_Status_Error:
		_result = RESULT_CODE_STATUS_ERROR;
		break;
	case ER_Settiing_Data_error:
		//_result = RESULT_CODE_SETTING_DATA_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_Stage_Setting_Error:
		//_result = RESULT_CODE_STAGE_SETTING_ERROR;
		_result = RESULT_CODE_OK;
		break;
	}

	fnMakeHead(pPack->head
		, CHARGER_MODE_CHANGE_RESPONSE, dataLen, _result);

//	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

// 1. 미전송 파일 업데이트 완료 후 Database에서 정보 삭제
BOOL CFMS::fnMisSendResultDataStateUpdate()
{
	if(m_vSendMis.size() <= 0)
		return FALSE;

	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetStageNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	strQuery.Format("Update FMSWorkInfo \
					Set \
					Send_Complet = %d \
					where idx = %I64d;"
					, RS_REPORT
					, m_vSendMis[m_SendMisIdx]->idxKey);

	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);
	
	if(SQLITE_OK != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return TRUE;
}

BOOL CFMS::fnMisSendResultDataStateUpdate2()
{
	if(m_vSendMis.size() <= 0)
		return FALSE;

	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetStageNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	strQuery.Format("Update FMSWorkInfo \
					Set \
					Send_Complet = %d \
					where idx = %I64d;"
					, RS_ERROR
					, m_vSendMis[m_SendMisIdx]->idxKey);


	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);
	
	if(SQLITE_OK != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return TRUE;
}



FMS_ERRORCODE CFMS::fnSend_E_CHARGER_WORKEND(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	//종료 메시지
	st_CHARGER_WORKEND data;
	memset(&data, 0x20, sizeof(st_CHARGER_WORKEND));

	sprintf(data.Equipment_Num, "%d", fnGetStageNo());
	strcpy_s(data.Tray_ID, fnGetTrayID_1());

	CHAR _result = RESULT_UNKNOW_ERROR;

	// 1. 결과 생성
	_rec = fnMakeResult();

	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	default:
		{
			_result = RESULT_CODE_OK;
		}
		break;
		//case ER_File_Not_Found:
		//	_result = RESULT_CODE_FILE_NOT_FOUND;
		//	break;
		//case ER_File_Open_error:
		//	_result = RESULT_CODE_FILE_OPEN_ERROR;
		//	break;
		//case ER_Contact_Error:
		//	_result = RESULT_CODE_CONTACT_ERROR;
		//	break;
		//case ER_Capa_Error:
		//	_result = RESULT_CODE_CAPA_ERROR;
		//	break;
		//case ER_Over_Charge:
		//	_result = RESULT_CODE_OVER_CHARGE;
		//	break;
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetStageNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_WORKEND_Map);
	pPack->dataLen = dataLen;

	fnMakeHead(pPack->head, CHARGER_WORKEND, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_RESULT_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	CHAR _result = RESULT_UNKNOW_ERROR;

	INT dataLen = 0;

	switch(_rec)
	{
	case ER_TrayID:
		_result = RESULT_CODE_TRAY_ID_ERROR;
		//_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_NO_Process:
		_result = RESULT_CODE_FILE_OPEN_ERROR;
		break;
	default:
		dataLen = fnGetResultData().GetLength();
		if(dataLen <= 0)
		{
			_result = RESULT_CODE_FILE_OPEN_ERROR;
			CFMSLog::WriteErrorLog("Result DataLen error[%03d][%s]%s"
				, fnGetStageNo()
				, g_str_FMS_State[m_FMSStateCode]
			, g_str_FMS_ERROR[_rec]);
		}
		else
		{
			_result = RESULT_CODE_OK;
		}
		break;	
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetStageNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	fnMakeHead(pPack->head, CHARGER_RESULT_RESPONSE, dataLen, _result);

	CHAR head[51];
	memset(&head, 0x20, 50);
	head[50] = 0;
	strLinker(head, &pPack->head, g_iHead_Map);

	CString strResult;
	if(_result == RESULT_CODE_OK)
	{		
		strResult = head + fnGetResultData() + "*;";
		const CHAR *temp = strResult.GetBuffer();
		//		theApp.m_FMSserver.fnSendResult(temp);
	}
	else
	{
		CHAR dumy[134 + 1] = {0};
		memset(dumy, 0x20, 134);
		CString	strDumy = dumy;
		strResult = head + strDumy + "*;";
		const CHAR *temp = strResult.GetBuffer();
		//		theApp.m_FMSserver.fnSendResult(temp);
	}

	CFMSLog::SendFMSLog(strResult.GetBuffer(), E_CHARGER_RESULT_RESPONSE);		// CHARGER_RESULT_RESPONSE 메세지를 로그로 저장

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_RESULT_RECV_RESPONSE(FMS_ERRORCODE _rec)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_RESULT_RECV_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_RESULT_RECV_RESPONSE));

	sprintf(data.Equipment_Num, "%d", fnGetStageNo());
	strcpy_s(data.Tray_ID, fnGetTrayID_1());

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	case ER_TrayID:
		//_result = RESULT_CODE_TRAY_ID_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	case ER_NO_Process:
		//_result = RESULT_CODE_STATUS_ERROR;
		_result = RESULT_CODE_DATA_ERROR;
		break;
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetStageNo()
			, g_str_FMS_State[m_FMSStateCode]
		, g_str_FMS_ERROR[_rec]);
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_RESULT_RECV_RESPONSE_Map);

	pPack->dataLen = dataLen;
	fnMakeHead(pPack->head
		, CHARGER_RESULT_RECV_RESPONSE
		, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_OUT_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	SendSBCInitCommand();

	st_CHARGER_OUT_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_OUT_RESPONSE));

	sprintf(data.Equipment_Num, "%d", fnGetStageNo());

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
		//case ER_Data_Error:
		//	_result = RESULT_CODE_DATA_ERROR;
		//	break;un
	case ER_NO_Process:
		//_result = RESULT_CODE_STATUS_ERROR;
		_result = RESULT_CODE_OK;
		break;
		//case ER_Tray_In:
		//	_result = RESULT_CODE_TRAY_IN_ERROR;
		//	break;
	}

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_OUT_RESPONSE_Map);
	pPack->dataLen = dataLen;

	fnMakeHead(pPack->head
		, CHARGER_OUT_RESPONSE, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_EQ_ERROR(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	if(m_ErrorData.Equipment_Num[0] != 0x20)
	{
		st_FMS_PACKET* pPack = fnGetPack();
		memset(pPack->data, 0x20, sizeof(pPack->data));
		UINT dataLen = strLinker(pPack->data
			, &m_ErrorData, g_iEQ_ERROR_Map);

		pPack->dataLen = dataLen;

		fnMakeHead(pPack->head
			, EQ_ERROR, dataLen, RESULT_CODE_OK);

		theApp.m_FMS_SendQ.Push(*pPack);
		memset(&m_ErrorData, 0x20, sizeof(st_EQ_ERROR));
	}

	return _error;
}


FMS_ERRORCODE CFMS::fnSend_E_CHARGER_IMPOSSBLE_RESPONSE(FMS_ERRORCODE _rec)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;


	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_INRESEVE_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_INRESEVE_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_INRESEVE_RESPONSE));

	sprintf(data.Equipment_Num, "%d", fnGetStageNo());

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_INRESEVE_RESPONSE_Map);
	pPack->dataLen = dataLen;

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	//case ER_Already_Reserve:
	//	_result = RESULT_CODE_ALREADY_RESERVED;
	//	break;	
	case ER_fnTransFMSWorkInfoToCTSWorkInfo:	
		_result = RESULT_CODE_INSPECTION;
		break;
	
	case ER_fnSendConditionToModule:
	case ER_SBC_NetworkError:	
		_result = RESULT_CODE_OK;
		break;
	case ER_fnMakeTrayInfo:
	case ER_NO_Process:
	case ER_Run_Fail:
	case ER_fnLoadWorkInfo:
	case ER_FMS_Recv_Error_State:		
		_result = RESULT_CODE_STATUS_ERROR;
		break;

	}
	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetStageNo()
			, g_str_FMS_State[m_FMSStateCode]
			, g_str_FMS_ERROR[_rec]);
	}
	fnMakeHead(pPack->head, CHARGER_INRESEVE_RESPONSE , dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}
FMS_ERRORCODE CFMS::fnSend_E_CHARGER_IN_RESPONSE(FMS_ERRORCODE _rec)
{
	FMS_ERRORCODE _error = FMS_ER_NONE;

	st_CHARGER_IN_RESPONSE data;
	memset(&data, 0x20, sizeof(st_CHARGER_IN_RESPONSE));

	sprintf(data.Equipment_Num
		, "%d", fnGetStageNo());

	st_FMS_PACKET* pPack = fnGetPack();
	memset(pPack->data, 0x20, sizeof(pPack->data));
	UINT dataLen = strLinker(pPack->data
		, &data, g_iCHARGER_IN_RESPONSE_Map);
	pPack->dataLen = dataLen;

	CHAR _result = RESULT_UNKNOW_ERROR;
	switch(_rec)
	{
	case FMS_ER_NONE:
		_result = RESULT_CODE_OK;
		break;
	case ER_TrayID:
		_result = RESULT_CODE_DATA_ERROR;
		break;
	
	case ER_SBC_NetworkError:
	case ER_fnMakeTrayInfo:
		//_result = RESULT_CODE_STEP_SEND;
		_result = RESULT_CODE_OK;
	//	//에러 발생
	//	//fnSetLastState(FMS_ST_ERROR);
	//	fnSet_E_EQ_ERROR_Dtat("24");
		break;
	case ER_NO_Process:
	case ER_Run_Fail:
		_result = RESULT_CODE_STATUS_ERROR;
		break;
	default:
		break;
	}

	if(_rec)
	{
		CFMSLog::WriteErrorLog("[%03d][%s]%s"
			, fnGetStageNo()
			, g_str_FMS_State[m_FMSStateCode]
			, g_str_FMS_ERROR[_rec]);
	}

	fnMakeHead(pPack->head, CHARGER_IN_RESPONSE, dataLen, _result);

	theApp.m_FMS_SendQ.Push(*pPack);

	return _error;
}

FMS_ERRORCODE CFMS::fnSend_E_CHARGER_STATE(FMS_ERRORCODE _rec)
{

	FMS_ERRORCODE _error = FMS_ER_NONE;


	return _error;
}

INT CFMS::fnLoadSendMisVector()
{
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetStageNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	COleDateTime OleCurrentTime = COleDateTime::GetCurrentTime();
	COleDateTimeSpan savePeriod;
	savePeriod.SetDateTimeSpan( -30, 0, 0, 0 );		// 저장기간은 30일
	OleCurrentTime += savePeriod;

	INT64 idx = OleCurrentTime.GetYear()	* 10000000000;
	idx += OleCurrentTime.GetMonth()		* 100000000;
	idx += OleCurrentTime.GetDay()			* 1000000;
	idx += OleCurrentTime.GetHour()			* 10000;
	idx += OleCurrentTime.GetMinute()		* 100;
	idx += OleCurrentTime.GetSecond();

	strQuery.Format("delete \
					from FMSWorkInfo where idx < %I64d \
					AND moduleID = %d"
					, idx, m_pModule->GetModuleID());

	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);

	strQuery.Format("select idx, Result_File_Name\
					from FMSWorkInfo where Send_Complet = 2 \
					AND moduleID = %d ORDER BY idx desc limit 1"					
					, m_pModule->GetModuleID());

	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	m_SendMisIdx = 0;
	m_SendMisCnt = 0;

	fnResetSendMisVector();

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		st_SEND_MIS_INFO* pMisInfo = new st_SEND_MIS_INFO;

		pMisInfo->idxKey = sqlite3_column_int64(stmt, c++);

		CHAR* ans = (CHAR*)sqlite3_column_text(stmt, c++);

		if( ans != NULL )
		{
			// strcpy_s(pMisInfo->szFMTFilePath, (CHAR*)sqlite3_column_text(stmt, c++));		
			strcpy_s(pMisInfo->szFMTFilePath, ans);		
			m_vSendMis.push_back(pMisInfo);
			m_SendMisCnt++;

			if(m_SendMisCnt > 10) break;
		}
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return m_SendMisCnt;
}

VOID CFMS::fnResetSendMisVector()
{
	m_MisSendState = 0;

	for (UINT i = 0; i < m_vSendMis.size(); i++)
	{
		st_SEND_MIS_INFO* pMisInfo = m_vSendMis[i];
		delete pMisInfo;
	}
	m_vSendMis.clear();
}

BOOL CFMS::fnResultDataStateUpdate(CHAR* _resultFileName, RESULTDATA_STATE _rState)
{
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");
	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetStageNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	if(_resultFileName == NULL)
	{
		strQuery.Format("Update FMSWorkInfo \
						Set \
						Send_Complet = %d \
						where moduleID = %d AND idx = %I64d;"
						, _rState
						, m_pModule->GetModuleID()
						, fnGetWorkCode());
	}
	else
	{
		strQuery.Format("Update FMSWorkInfo \
						Set \
						Result_File_Name = '%s' \
						, Send_Complet = %d \
						where moduleID = %d AND idx = %I64d;"
						, _resultFileName
						, _rState
						, m_pModule->GetModuleID()
						, fnGetWorkCode());
	}

	CHAR *errorMsg;
	sqlerr = sqlite3_exec(mSqlite, strQuery, NULL, NULL, &errorMsg);
	
	if(SQLITE_OK != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	return TRUE;
}

//미전송 파일
//Send_Complet
//0 사용한적 없음 - reset
//1 정상 시작 - 공정 정상 시작되면 업데이트
//2 결과데이터 전송 - 결과확인 메시지 수신시 업데이트
BOOL CFMS::fnLoadWorkInfo_CHARGER(INT64 _code, st_CHARGER_INRESEVE& _reserveInfo)
{
	INT loadOK = 0;
	INT sqlerr = 0;

	CString strSqliteDBPathNName = theApp.GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");

	CString dbname;
	dbname.Format("\\FMS_DB\\CTSMonDB_%03d.db3", fnGetStageNo());
	strSqliteDBPathNName += dbname;

	sqlite3* mSqlite;

	sqlerr = sqlite3_open(strSqliteDBPathNName, &mSqlite);

	sqlite3_stmt * stmt = 0;

	CString strQuery;

	strQuery.Format("select idx\
					, moduleID\
					, F_Info \
					, StepInfo \
					, B_Info \
					from FMSWorkInfo where idx = %I64d"
					, _code);

	sqlerr = sqlite3_prepare( mSqlite,  strQuery, -1, &stmt, 0 );

	if(_reserveInfo.Step_Info)
	{
		delete [] _reserveInfo.Step_Info;
		_reserveInfo.Step_Info = NULL;
	}

	while((sqlerr = sqlite3_step( stmt )) == SQLITE_ROW)
	{
		INT c = 0;
		m_WorkCode = sqlite3_column_int64(stmt, c++);
		UINT64 idx = m_WorkCode;
		INT year = idx / 10000000000;
		idx -= year * 10000000000;
		INT month = idx / 100000000;
		idx -= month * 100000000;
		INT day = idx / 1000000;
		idx -= day * 1000000;

		INT hour = idx / 10000;
		idx -= hour * 10000;
		INT mint = idx / 100;
		idx -= mint * 100;
		INT sec = idx;
		m_OleRunStartTime.SetDateTime(year
			, month
			, day
			, hour
			, mint
			, sec);

		INT id = sqlite3_column_int(stmt, c++);

		INT blobsize = 0;
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_F))
		{
			ZeroMemory(&_reserveInfo.F_Value, sizeof(st_CHARGER_INRESEVE_F));
			memcpy(&_reserveInfo.F_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_F));
		}
		else loadOK++;
		c++;

		if(_reserveInfo.Step_Info)
		{
			delete [] _reserveInfo.Step_Info;
			_reserveInfo.Step_Info = NULL;
			loadOK++;
			break;
		}

		INT totstep = atoi(_reserveInfo.Total_Step);
		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_Step_Set) * totstep)
		{
			st_Step_Set* pStepInfo = new st_Step_Set[totstep];
			ZeroMemory(pStepInfo, sizeof(st_Step_Set) * totstep);
			memcpy(pStepInfo, sqlite3_column_blob(stmt, c), sizeof(st_Step_Set) * totstep);
			_reserveInfo.Step_Info = pStepInfo;
		}
		else loadOK++;
		c++;

		blobsize = sqlite3_column_bytes(stmt, c);
		if(blobsize == sizeof(st_CHARGER_INRESEVE_B))
		{
			ZeroMemory(&_reserveInfo.B_Value, sizeof(st_CHARGER_INRESEVE_B));
			memcpy(&_reserveInfo.B_Value, sqlite3_column_blob(stmt, c), sizeof(st_CHARGER_INRESEVE_B));
		}
		else loadOK++;
	}

	if(SQLITE_DONE != sqlerr)
	{
		LPSTR szError = (LPSTR)sqlite3_errmsg(mSqlite);
		CFMSLog::WriteLog(szError);

		int nRowsChanged = sqlite3_changes(mSqlite);

		sqlerr = sqlite3_reset(stmt);

		if (sqlerr != SQLITE_OK)
		{
			szError = (LPSTR)sqlite3_errmsg(mSqlite);
			CFMSLog::WriteLog(szError);
		}
		else loadOK++;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(mSqlite);

	if(loadOK)
		return FALSE;
	else
		return TRUE;	
	return 1;
}
*/