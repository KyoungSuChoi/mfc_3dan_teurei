#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"
#include "FM_STATE.h"


CFM_Unit::CFM_Unit(void)
: m_pstOff(NULL)
, m_pstAuto(NULL)
, m_pstLocal(NULL)
, m_pstMaint(NULL)
, m_pstCali(NULL)
, m_pState(NULL)
, m_pPreState(NULL)
, m_ModuleIdx(-1)
, m_ModuleID(-1)
{
}

CFM_Unit::~CFM_Unit(void)
{
	
}
VOID CFM_Unit::fnInit(INT moduleIdx, CFormModule *pModule)
{
	m_ModuleIdx = moduleIdx;
	//	m_ModuleID = moduleIdx + 1;
	m_ModuleID = EPGetModuleID(m_ModuleIdx);	
	m_ModuleLineNo = EPGetModuleLineNo(m_ModuleIdx);	
	CString strTemp;
	strTemp.Format("%c",m_ModuleLineNo);
	m_pModule = pModule;
	m_ModuleName = GetModuleName(m_ModuleID);

	m_nMachineId = atoi(m_pModule->GetModuleName().Right(5));

	m_FMS.fnInit(m_pModule);

	m_pstOff = new CFM_ST_OFF(EQUIP_ST_OFF, EQUIP_ST_OFF, this);
	m_pstAuto = new CFM_ST_AUTO(EQUIP_ST_OFF, EQUIP_ST_OFF, this);

	CFM_STATE* pForAutoTemp = new CFM_ST_AT_VACANCY(EQUIP_ST_AUTO, AUTO_ST_VACANCY, this);
	((CFM_ST_AUTO *)m_pstAuto)->fnSetVacancy(pForAutoTemp);

	pForAutoTemp = new CFM_ST_AT_READY(EQUIP_ST_AUTO, AUTO_ST_READY, this);
	((CFM_ST_AUTO *)m_pstAuto)->fnSetReady(pForAutoTemp);

	pForAutoTemp = new CFM_ST_AT_TRAY_IN(EQUIP_ST_AUTO, AUTO_ST_TRAY_IN, this);
	((CFM_ST_AUTO *)m_pstAuto)->fnSetTrayIn(pForAutoTemp);

	pForAutoTemp = new CFM_ST_AT_CONTACT_CHECK(EQUIP_ST_AUTO, AUTO_ST_CONTACT_CHECK, this);
	((CFM_ST_AUTO *)m_pstAuto)->fnSetContactCheck(pForAutoTemp);

	pForAutoTemp = new CFM_ST_AT_RUN(EQUIP_ST_AUTO, AUTO_ST_RUN, this);
	((CFM_ST_AUTO *)m_pstAuto)->fnSetRun(pForAutoTemp);

	pForAutoTemp = new CFM_ST_AT_ERROR(EQUIP_ST_AUTO, AUTO_ST_ERROR, this);
	((CFM_ST_AUTO *)m_pstAuto)->fnSetError(pForAutoTemp);

	pForAutoTemp = new CFM_ST_AT_END(EQUIP_ST_AUTO, AUTO_ST_END, this);
	((CFM_ST_AUTO *)m_pstAuto)->fnSetEnd(pForAutoTemp);

	m_pstLocal = new CFM_ST_LOCAL(EQUIP_ST_LOCAL, EQUIP_ST_LOCAL, this);

	CFM_STATE* pForLocalTemp = new CFM_ST_LO_LOCAL(EQUIP_ST_LOCAL, LOCAL_ST_LOCAL, this);
	((CFM_ST_LOCAL *)m_pstLocal)->fnSetLocal(pForLocalTemp);

	pForLocalTemp = new CFM_ST_LO_ERROR(EQUIP_ST_LOCAL, LOCAL_ST_ERROR, this);
	((CFM_ST_LOCAL *)m_pstLocal)->fnSetError(pForLocalTemp);

	m_pstMaint = new CFM_ST_MAINT(EQUIP_ST_MAINT, EQUIP_ST_MAINT, this);

	CFM_STATE* pForMaintTemp = new CFM_ST_MA_MAINT(EQUIP_ST_MAINT, MAINT_ST_MAINT, this);
	((CFM_ST_MAINT *)m_pstMaint)->fnSetMaint(pForMaintTemp);

	pForMaintTemp = new CFM_ST_MA_ERROR(EQUIP_ST_MAINT, MAINT_ST_ERROR, this);
	((CFM_ST_MAINT *)m_pstMaint)->fnSetError(pForMaintTemp);

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//CALIBRATION 20190902
	m_pstCali = new CFM_ST_CALI(EQUIP_ST_CALI, EQUIP_ST_CALI, this); //20190902

	CFM_STATE* pForCalITemp = new CFM_ST_CA_VACANCY(EQUIP_ST_CALI, CALI_ST_VACANCY, this);
	((CFM_ST_CALI *)m_pstCali)->fnSetVacancy(pForCalITemp);

	pForCalITemp = new CFM_ST_CA_READY(EQUIP_ST_CALI, CALI_ST_READY, this);
	((CFM_ST_CALI *)m_pstCali)->fnSetReady(pForCalITemp);

	pForCalITemp = new CFM_ST_CA_TRAY_IN(EQUIP_ST_CALI, CALI_ST_TRAY_IN, this);
	((CFM_ST_CALI *)m_pstCali)->fnSetCaliTrayin(pForCalITemp);

	pForCalITemp = new CFM_ST_CA_CALI(EQUIP_ST_CALI, CALI_ST_CALI, this);
	((CFM_ST_CALI *)m_pstCali)->fnSetCali(pForCalITemp);

	pForCalITemp = new CFM_ST_CA_ERROR(EQUIP_ST_CALI, CALI_ST_ERROR, this);
	((CFM_ST_CALI *)m_pstCali)->fnSetError(pForCalITemp);

	pForCalITemp = new CFM_ST_CA_END(EQUIP_ST_CALI, CALI_ST_END, this);
	((CFM_ST_CALI *)m_pstCali)->fnSetEnd(pForCalITemp);
	////////////////////////////////////////////////////////////////////////////////////////////////////////


	m_pPreState = m_pstOff;
	m_pState = m_pstOff;

	m_bPreTrayIn = false;
}
VOID CFM_Unit::fnUnInit()
{
	if(m_pstOff)
	{
		delete m_pstOff;
		m_pstOff = NULL;
	}

	if(m_pstAuto)
	{
		delete m_pstAuto;
		m_pstAuto = NULL;
	}

	if(m_pstLocal)
	{
		delete m_pstLocal;
		m_pstLocal = NULL;
	}

	if(m_pstMaint)
	{
		delete m_pstMaint;
		m_pstMaint = NULL;
	}

	if(m_pstCali)
	{
		delete m_pstCali;
		m_pstCali = NULL;
	}
}
CFM_STATE* CFM_Unit::fnProcessing()
{
	CFMSSyncObj FMSSync;

	m_pState->fnPorcessing();

	return m_pState;
}

CFM_STATE* CFM_Unit::fnGet_AUTO_ST_VACANCY()
{
	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetVacancy();
}

CFM_STATE* CFM_Unit::fnGet_AUTO_ST_READY()
{
	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetReady();
}
CFM_STATE* CFM_Unit::fnGet_AUTO_ST_TRAY_IN()
{
	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetTrayIn();
}
CFM_STATE* CFM_Unit::fnGet_AUTO_ST_CONTACT_CHECK()
{
	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetContactCheck();
}
// 
// CFM_STATE* CFM_Unit::fnGet_AUTO_ST_CALIBRATION()
// {
// 	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetCalibration();
// }


CFM_STATE* CFM_Unit::fnGet_AUTO_ST_RUN()
{
	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetRun();
}

CFM_STATE* CFM_Unit::fnGet_AUTO_ST_END()
{
	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetEnd();
}

CFM_STATE* CFM_Unit::fnGet_AUTO_ST_ERROR()
{
	return ((CFM_ST_AUTO*)m_pstAuto)->fnGetError();
}

CFM_STATE* CFM_Unit::fnGet_LOCAL_ST_LOCAL()
{
	return ((CFM_ST_LOCAL*)m_pstLocal)->fnGetLocal();
}

CFM_STATE* CFM_Unit::fnGet_LOCAL_ST_ERROR()
{
	return ((CFM_ST_LOCAL*)m_pstLocal)->fnGetError();
}

CFM_STATE* CFM_Unit::fnGet_MAINT_ST_MAINT()
{
	return ((CFM_ST_MAINT*)m_pstMaint)->fnGetMaint();
}

CFM_STATE* CFM_Unit::fnGet_MAINT_ST_ERROR()
{
	return ((CFM_ST_MAINT*)m_pstMaint)->fnGetError();
}

CFM_STATE* CFM_Unit::fnGet_CALI_ST_VACANCY()
{
	return ((CFM_ST_CALI*)m_pstCali)->fnGetCaliVacancy();
}

CFM_STATE* CFM_Unit::fnGet_CALI_ST_READY()
{
	return ((CFM_ST_CALI*)m_pstCali)->fnGetCaliReady();
}

CFM_STATE* CFM_Unit::fnGet_CALI_ST_TRAY_IN()
{
	return ((CFM_ST_CALI*)m_pstCali)->fnGetCaliTrayIn();
}

CFM_STATE* CFM_Unit::fnGet_CALI_ST_CALI()
{
	return ((CFM_ST_CALI*)m_pstCali)->fnGetCali();
}

CFM_STATE* CFM_Unit::fnGet_CALI_ST_ERROR()
{
	return ((CFM_ST_CALI*)m_pstCali)->fnGetError();
}

CFM_STATE* CFM_Unit::fnGet_CALI_ST_END()
{
	return ((CFM_ST_CALI*)m_pstCali)->fnGetEnd();
}

VOID CFM_Unit::fnSetState(CFM_STATE* _st)
{
	if(m_pState != _st)
	{
		TRACE("STATE %s ------> %s \n", g_str_State[m_pState->fnGetStateID()]
		, g_str_State[_st->fnGetStateID()]);

		m_pPreState = m_pState;
		m_pPreState->fnInit();
		m_pPreState->fnWaitInit();
		m_pState = _st;
		m_pState->fnInit();
		m_pState->fnWaitInit();
		m_pState->fnSetProcID(FNID_ENTER);
	}
}
BOOL CFM_Unit::fnGetModuleTrayState()
{
	DWORD nCount = 0;	//������ Tray ���� 
	for(int j =0; j<m_pModule->GetTotalJig(); j++)
	{
		if(EPTrayState(m_ModuleID, j) == EP_TRAY_LOAD)
		{
			nCount++;
		}
	}

	if(nCount) return TRUE;
	return FALSE;
}

BOOL CFM_Unit::fnGetModuleCheckState()
{
	EP_CH_DATA netChData;
	for (INT i = 0; i < 32; i++)
	{
		netChData = ::EPGetChannelData(fnGetModuleID(), 0, i);
		if(netChData.state == EP_STATE_CHECK)
			return TRUE;
	}

	return FALSE;
}

void CFM_Unit::fnSetTrayInStateRecv( BOOL bTrayInState )
{
	m_bRecvCmd_S2F41 = bTrayInState;
}

VOID CFM_Unit::fnSendEmg(HWND _hwnd, INT code)
{
	m_EmgData.nCode = code;

	PostMessage(_hwnd,
		EPWM_MODULE_EMG, 
		(WPARAM)MAKELONG(0, m_ModuleID), 
		(LPARAM)&m_EmgData);
}