#if !defined(AFX_CALIBRATORDLG_H__E9556045_6B86_44B0_AF52_288181258156__INCLUDED_)
#define AFX_CALIBRATORDLG_H__E9556045_6B86_44B0_AF52_288181258156__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalibratorDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCalibratorDlg dialog
#include "MyGridWnd.h"
#include "CalibrationData.h"
#include "CTSMonDoc.h"
#include "CaliStartDlg.h"
#include "afxwin.h"


#define _MON_CH_COL_	1
#define _HW_CH_COL		2
#define _FROZEN_COL_CNT_	2
#define _FROZEN_ROW_CNT_	4

#define CAL_TYPE_CALCHECK	0
#define CAL_TYPE_CALIB		1	
#define CAL_TYPE_CHECK		2

#define CAL_ADC_DAC			0
#define CAL_CH_BOARD		1

#define CAL_VOLTAGE_CURRENT	-1
#define CAL_VOLTAGE			0
#define CAL_CURRENT			1

#define CAL_DATA_TYPE_VTG			0
#define CAL_DATA_TYPE_CRT			1
// 20200622 KSCHOI Add Temperature Data In Calibration START
#define CAL_DATA_TYPE_TEMPERATURE	2
// 20200622 KSCHOI Add Temperature Data In Calibration END

#define CAL_DATA_RANGE1		0
#define CAL_DATA_RANGE2		1
#define CAL_DATA_RANGE3		2
#define CAL_DATA_RANGE4		3

#define CAL_DATA_MODE_BEFORE	0
#define CAL_DATA_MODE_AFTER		1

#define CAL_TIME_OUT_SEC	20

#define CAL_STATE_LINEOFF	0
#define CAL_STATE_IDLE		1
#define CAL_STATE_RUN		2
#define CAL_STATE_PAUSE		3

#define ONE_POINT_COL_COUNT	6	//set, ad, dmm, ad-err, da-err

#define TIMER_CALI			150
#define TIMER_POINT_SET		151

typedef struct str_CaliStartInfo
{
	int nMain;		// Main DA=0 / CH=1
	int nCalType;
	int nTrayType;	// 교정 tray = 0, 실측 tray =1		//ljb 2011519 
	bool bMultiMode;
	int nBoardNo;	// 1base, Board No
	int nRange;		// 1base, 0:Allbase
} STR_CALISTARTINFO;

class CCalibratorDlg : public CDialog
{
// Construction
public:
	virtual ~CCalibratorDlg();
	CString *TEXT_LANG;
	bool LanguageinitMonConfig(CString strClassName);

	STR_CALISTARTINFO m_CaliStartInfo;
	EP_CAL_SELECT_INFO m_SelInfo;

	BOOL	m_bCheckConnectLine;
	int		m_nOnePointElapsedTime;
	int		GetTotalCalCount(EP_CAL_SELECT_INFO &selInfo);
	void	SetCalState(int nState);
	int		m_nState;
	int		m_nPreJigState;
	BOOL	CheckEditedCh();
	BOOL	m_nTimeOutCnt;									// 일정 시간동안 교정 응답이 없으면 통신두절 처리한다.
	BOOL	CheckOverSpec(BOOL bAllData = TRUE);
	COLORREF m_boardColor[EP_MAX_BD_PER_MD];
	int		m_nTotBoardNo;
	void	ReDrawGrid(int nMode, int nType);
	// int m_nCalType;
	// int m_nBoardNo;	
	// BOOL	m_bMultiMode;
	BOOL	EndResultData(long nModuleID, WORD nMode, EP_CAL_SELECT_INFO calEndInfo);
	BOOL	UpdateResultData(long nModuleID, WORD nMode, EP_CALIHCK_RESULT_DATA calData);
	void	InitCalGrid(); 
	int		m_nTotCh;
	CCalibratorDlg(int nModuleID, CWnd* pParent = NULL);   // standard constructor
	int		m_nUnitNo;
	CCTSMonDoc* m_pdoc;
	void	CalPointInit();
	void	SaveUpdateResultData(CWordArray *pawSelMonCh = NULL);		// PC -> SBC Update 후 파일로 결과 파일을 저장
	BOOL	UpdateCalibrationInfo();
	void	DisplayData( int nDataMode, int nDataType, int nRange=0 );	// 결과 데이터 저장을 위해 그리드를 이용
	void	fnUpdateJigState();
// 20200622 KSCHOI Add Temperature Data In Calibration START
	CString ReplaceStringEndCharacter(CString csStr);
// 20200622 KSCHOI Add Temperature Data In Calibration END

	BOOL	m_bStartCmd;

	CFont	m_Font;
	void	InitFont();
	void	AutoCali();

	BOOL	m_bPointSet; //20190927
	BOOL	m_bCaliPointSet;	//20191027 //onPointSet진입시 오토시퀀스를 타면 자동진행하는 플래그
	
// Dialog Data
	//{{AFX_DATA(CCalibratorDlg)
	enum { IDD = IDD_CALIBRATION_DLG };
	CLabel	m_wndCalState;
	int		m_nDataMode;
	int		m_nDataType;
	int		m_nOptTrayType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalibratorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
	CCalibrationData	m_calibrtaionData;
protected:	
	WORD m_CaliModuleState[256];
	// int m_nRange;
	void DisplayChNoColumn();
	int GetHWChCnt(int nBoardNo);
	CMyGridWnd m_wndCalData;
	// Generated message map functions
	//{{AFX_MSG(CCalibratorDlg)
	afx_msg void OnBtnSetPoint();
	virtual BOOL OnInitDialog();
	afx_msg void OnCaliStartBtn();
	afx_msg void OnStopBtn();
	afx_msg void OnPauseBtn();
	afx_msg void OnCaliDataSearch();
	afx_msg void OnResumeBtn();
	afx_msg void OnCaliDataUpdate();
	afx_msg void OnSelchangeComboMode();
	afx_msg void OnSelchangeComboType();
	afx_msg void OnSelStart();
	afx_msg void OnButtonAccu();
	afx_msg void OnUpdateSelStart(CCmdUI* pCmdUI);
	virtual void OnCancel();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCalStop();
	afx_msg void OnCalDataUpdate();
	afx_msg void OnOptTrayType1();
	afx_msg void OnOptTrayType2();
	//}}AFX_MSG
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CLabel m_LabelJig;
	afx_msg void OnBnClickedJigClose();
	afx_msg void OnBnClickedJigOpen();	
	afx_msg void OnCbnSelchangeCombStoppertype();
	CComboBox m_ctrlStopper;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonResetCmd();
	afx_msg void OnBnClickedButtonEndCmd();
	afx_msg void OnBnClickedBtnTemporaryContinue();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALIBRATORDLG_H__E9556045_6B86_44B0_AF52_288181258156__INCLUDED_)
