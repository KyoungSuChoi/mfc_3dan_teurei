// ModuleStepEndData.h: interface for the CModuleStepEndData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODULESTEPENDDATA_H__CD115863_CC4A_4F6D_B3CD_F6DA53E2CF4D__INCLUDED_)
#define AFX_MODULESTEPENDDATA_H__CD115863_CC4A_4F6D_B3CD_F6DA53E2CF4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CModuleStepEndData  
{
public:
	LPEP_CH_DATA GetStepData(int nStepNo);
	int GetDataSize();
	int LoadData(int nStepIndex, CString strEndFile);
	CModuleStepEndData();
	virtual ~CModuleStepEndData();

protected:
	CTypedPtrList <CPtrList, EP_CH_DATA*> m_ChDataList;

};

#endif // !defined(AFX_MODULESTEPENDDATA_H__CD115863_CC4A_4F6D_B3CD_F6DA53E2CF4D__INCLUDED_)
