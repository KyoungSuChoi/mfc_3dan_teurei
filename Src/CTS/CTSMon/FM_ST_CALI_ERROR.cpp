#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_CA_ERROR::CFM_ST_CA_ERROR(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_CALI(_Eqstid, _stid, _unit)
{
}


CFM_ST_CA_ERROR::~CFM_ST_CA_ERROR(void)
{
}

VOID CFM_ST_CA_ERROR::fnEnter()
{
	// m_Unit->fnGetFMS()->fnSend_E_EQ_ERROR(FMS_ER_NONE);
	CHANGE_FNID(FNID_PROC);
}

VOID CFM_ST_CA_ERROR::fnProc()
{
	fnSetFMSStateCode(FMS_ST_ERROR);
}

VOID CFM_ST_CA_ERROR::fnExit()
{
}

VOID CFM_ST_CA_ERROR::fnSBCPorcess(WORD _state)
{
	CFM_ST_CALI::fnSBCPorcess(_state);
}

FMS_ERRORCODE CFM_ST_CA_ERROR::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

// 	switch(_msgId)
// 	{
// 	default:
// 		{
// 		}
// 		break;
// 	}

	return _error;

}