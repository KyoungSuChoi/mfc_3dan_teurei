// CalFileDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CalFileDlg.h"
#include "IPSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalFileDlg dialog


CCalFileDlg::CCalFileDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCalFileDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCalFileDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nInstalledModuleNum = 0;
}


void CCalFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCalFileDlg)
	DDX_Control(pDX, IDC_MODULE_FILE_LIST, m_ctrlModuleFileList);
	DDX_Control(pDX, IDC_CAL_FILE_LIST, m_ctrlFileList);
	DDX_Control(pDX, IDC_SEL_MODULE_COMBO, m_ctrlModuleSel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCalFileDlg, CDialog)
	//{{AFX_MSG_MAP(CCalFileDlg)
	ON_BN_CLICKED(IDC_UPDATE_BUTTON, OnUpdateButton)
	ON_CBN_SELCHANGE(IDC_SEL_MODULE_COMBO, OnSelchangeSelModuleCombo)
	ON_BN_CLICKED(IDC_ALL_UPDATE_BUTTON, OnAllUpdateButton)
	ON_NOTIFY(NM_DBLCLK, IDC_MODULE_FILE_LIST, OnDblclkModuleFileList)
	ON_NOTIFY(NM_DBLCLK, IDC_CAL_FILE_LIST, OnDblclkCalFileList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCalFileDlg message handlers

BOOL CCalFileDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_strModuleName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	//Get Current Folder(CTSMon Folde)
//	m_strGroupName = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Group Name", "Group");	//Get Current Folder(CTSMon Folde)
//	m_nModulePerRack = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Module Per Rack", 3);
//	m_bUseRackIndex = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Rack Index", TRUE);
//	m_bUseGroupSet = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Use Group", FALSE);
	
	m_ctrlModuleSel.AddString("All");
	CString strTemp;
	// TODO: Add extra initialization here
	for(int i = 0; i<m_nInstalledModuleNum; i++)
	{
		strTemp.Format("%s", GetModuleName(EPGetModuleID(i)));
		m_ctrlModuleSel.AddString(strTemp);
	}
	m_ctrlModuleSel.SetCurSel(0);
	InitList();

	BeginWaitCursor();
	LoadFileList();
	EndWaitCursor();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCalFileDlg::OnUpdateButton() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;

/*	if(PermissionCheck(PMS_MODULE_CAL_UPDATE) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR));
		return ;
	}
*/
	if( LoginPremissionCheck(PMS_MODULE_MODE_CHANGE) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Calibration Data Update]");
		return;
	}

	int nIndex = m_ctrlModuleSel.GetCurSel() -1;
	if(nIndex < 0)
	{
		OnAllUpdateButton();
		return;
	}

	int nModuleID = EPGetModuleID(nIndex);

	strTemp.Format(GetStringTable(IDS_MSG_CAL_UPDATE_CONFIRM), GetModuleName(nModuleID));
	if(MessageBox(strTemp, "UpLoad File", MB_YESNO|MB_ICONQUESTION) == IDYES)
	{
		int nSelected = -1;
		CStringArray aFileName;

		while((nSelected = m_ctrlFileList.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
		{
			aFileName.Add(m_ctrlFileList.GetItemText(nSelected , 0 ));
		}
		if(aFileName.GetSize() <= 0)
		{
			AfxMessageBox(GetStringTable(IDS_LANG_MSG_FILE_NOT_FOUND));
			return;
		}
		if(SendFileToModule(nModuleID, aFileName))
			UpdateModuleFileList(nModuleID);
	}
}

/*
inline CString CCalFileDlg::GetModuleName(int nModuleID, int nGroupIndex)
{
	CString strName;
	
	if(m_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, m_nModulePerRack );

		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d-%d, %s %d", m_strModuleName, div_result.quot+1, div_result.rem+1, m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d-%d", m_strModuleName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(m_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %d, %s %d", m_strModuleName, nModuleID,  m_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %d", m_strModuleName, nModuleID);
		}
	}
	return strName;
}
*/


BOOL CCalFileDlg::SetModuleNum(int nNum)
{
	if(nNum < 0)	return FALSE;

	m_nInstalledModuleNum = nNum;
	return TRUE;
}

BOOL CCalFileDlg::InitList()
{
	CRect rect;
	CString strTemp;
	strTemp = m_strModuleName + " 번호";
	m_ctrlFileList.GetClientRect(&rect);
	int nColInterval = rect.Width()/10;
	m_ctrlFileList.InsertColumn(0, _T("File Name"), LVCFMT_LEFT, nColInterval*3);
	m_ctrlFileList.InsertColumn(1, _T(strTemp), LVCFMT_LEFT, nColInterval*1);
	m_ctrlFileList.InsertColumn(2, _T("보드 번호"), LVCFMT_LEFT, nColInterval*1);
	m_ctrlFileList.InsertColumn(3, _T("교정 날짜"), LVCFMT_LEFT, nColInterval*5);
	m_ctrlFileList.SetRedraw(FALSE);
	m_ctrlFileList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlFileList.ModifyStyle(0, LVS_REPORT);
	m_ctrlFileList.SetRedraw(TRUE);
	m_ctrlFileList.DeleteAllItems();

	m_ctrlModuleFileList.GetClientRect(&rect);
	nColInterval = rect.Width()/10;
	m_ctrlModuleFileList.InsertColumn(0, _T("보드 번호"), LVCFMT_LEFT, nColInterval*3);
	m_ctrlModuleFileList.InsertColumn(1, _T("교정 날짜"), LVCFMT_LEFT, nColInterval*7);
	m_ctrlModuleFileList.SetRedraw(FALSE);
	m_ctrlModuleFileList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlModuleFileList.ModifyStyle(0, LVS_REPORT);
	m_ctrlModuleFileList.SetRedraw(TRUE);
	m_ctrlModuleFileList.DeleteAllItems();
	
	return TRUE;
}

BOOL CCalFileDlg::LoadFileList(int nModuleID)
{
	//Delete Real Time Save File
	CString strTemp;
//	LVITEM lvi;
	CString strItem;
	
	strItem = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folde)
	if(nModuleID > 0)
		strTemp.Format("%s\\CalData\\Module %d", strItem, nModuleID);
	else
		strTemp.Format("%s\\CalData", strItem);

	m_ctrlFileList.DeleteAllItems();
	Recurse(strTemp, nModuleID);

/*	CFileFind 	finder;
	BOOL   bWorking = finder.FindFile(strTemp);

	int nCount = 0;
	while (bWorking)
	{
		bWorking	= finder.FindNextFile();
		strTemp = finder.GetFileName();
	
		lvi.mask =  LVIF_TEXT;
		lvi.iItem = nCount++;
		lvi.iSubItem = 0;
		lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
		m_ctrlFileList.InsertItem(&lvi);
	}
	finder.Close();
*/	return TRUE;
}


void CCalFileDlg::Recurse(CString strCurLocation, int nModuleID)
{
	CFileFind finder;

   // build a string with wildcards
	CString strWildcard(strCurLocation);
	strWildcard += _T("\\*.*");

   // start working for files
	BOOL bWorking = finder.FindFile(strWildcard);
	LVITEM lvi;

	int nCount = 0;
	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		// skip . and .. files; otherwise, we'd
		// recur infinitely!

		if (finder.IsDots())
			continue;

		// if it's a directory, recursively search it

		if (finder.IsDirectory())
		{
			CString str = finder.GetFilePath();
			Recurse(str, nModuleID);
		}
		else
		{
			CString strFileName = finder.GetFileName();
			if(strFileName.Mid(strFileName.ReverseFind('.')+1) == "cff")
			{
				lvi.mask =  LVIF_TEXT;
				lvi.iItem = nCount++;
				lvi.iSubItem = 0;
				lvi.pszText = (LPTSTR)(LPCTSTR)(strFileName);
				m_ctrlFileList.InsertItem(&lvi);
				
				strFileName = finder.GetFilePath();
				FILE *fp = fopen((LPSTR)(LPCTSTR)strFileName, "rt");
				if(fp != NULL)
				{
					char szBuff[128];
					fscanf(fp, "%s", szBuff);		//File serial
					fscanf(fp, "%s", szBuff);		//Board Serial


					fscanf(fp, "%s", szBuff);		//date Time					
					lvi.iSubItem = 3;
					lvi.pszText = szBuff;
					m_ctrlFileList.SetItem(&lvi);
					
					if(nModuleID > 0)
					{
						lvi.iSubItem = 1;
						sprintf(szBuff, "%d", nModuleID);
						lvi.pszText = szBuff;
						m_ctrlFileList.SetItem(&lvi);
					}

					int data;
					fscanf(fp, "%d", &data);		//Board Num
					lvi.iSubItem = 2;
					sprintf(szBuff, "%d", data);
					lvi.pszText = szBuff;
					m_ctrlFileList.SetItem(&lvi);
					
					fclose(fp);
				}
			}
		}	
	}
	finder.Close();
}

void CCalFileDlg::OnSelchangeSelModuleCombo() 
{
	// TODO: Add your control notification handler code here
	int nID = 0;
	int nIndex = m_ctrlModuleSel.GetCurSel();
	CString strTemp;

	if(nIndex > 0)
	{
		nID = EPGetModuleID(nIndex-1);
		strTemp.Format("%s calibration data", GetModuleName(nID));
	}
	else
	{
		strTemp = "Calibration data";
	}

	GetDlgItem(IDC_MODULE_DATA_LABEL)->SetWindowText(strTemp);
	
	LoadFileList(nID);

	if(nID > 0)
	{
		GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(UpdateModuleFileList(nID));
	}
}

void CCalFileDlg::OnAllUpdateButton() 
{
	// TODO: Add your control notification handler code here

}

BOOL CCalFileDlg::SendFileToModule(int nModuleID, CStringArray &strArray)
{
	CString strIPAddress;
	BOOL nRtn = TRUE;
	strIPAddress = GetIPAddress(nModuleID);

	if(strArray.GetSize() <= 0 || strIPAddress.IsEmpty())	return FALSE;
 
	CString strLoginID;
	CString strPassword;
	CString strLocation;
	CString toFileName, fromFileName;
	CString path;
	CString source, destination;

	strLoginID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	strPassword = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrnth");
	strLocation = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Cal Location", "/Project/SKC/current/form/App/Client/cali");

	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;
//	COleDateTime timeLocal;
	int nBoardNo;

//	timeLocal = COleDateTime::GetCurrentTime();

	BeginWaitCursor();

	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(strIPAddress, strLoginID, strPassword);
		if(!pFtpConnection->SetCurrentDirectory(strLocation))
		{
			AfxMessageBox(GetStringTable(IDS_TEXT_SAVE_DIR_ERROR));
			nRtn = FALSE;
			goto _EXIT_;
		}

#ifdef _DEBUG
		CString strFile;
		CFtpFileFind pFileFind(pFtpConnection);
		BOOL bWorking = pFileFind.FindFile(_T("*"));
		while (bWorking)
		{
			bWorking = pFileFind.FindNextFile();
			strFile = pFileFind.GetFileName();
			TRACE("%s\n", strFile);
		}
		pFileFind.Close();
#endif

	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();

		ftpSession.Close();
		EndWaitCursor();
		return FALSE;
	}
	
	fromFileName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folde)
	if(nModuleID > 0)
		path.Format("%s\\CalData\\Module %d", fromFileName, nModuleID);
	else
		path.Format("%s\\CalData", fromFileName);


	try
	{
		for(int i = 0; i<strArray.GetSize(); i++)
		{
			fromFileName = path + "\\"+ strArray.GetAt(i);
			nBoardNo = GetBoardNo(fromFileName);
			if(!fromFileName.IsEmpty() && nBoardNo > 0)
			{
				source.Format("L_CALI_C%d", nBoardNo);
//				destination.Format("L_CALI_C%d_%s", nBoardNo, timeLocal.Format("%Y%m%d%H%M%s"));
				destination.Format("L_CALI_C%d_Back", nBoardNo);
				pFtpConnection->Rename(source, destination);		//BackUp Org File

				toFileName = source;
				if(pFtpConnection->PutFile(fromFileName, toFileName) == FALSE)
				{
					MessageBox(fromFileName +" Update Fail", "UpLoad Fail", MB_OK|MB_ICONSTOP);
					nRtn = FALSE;
				}
			}
		}
		EndWaitCursor();
		AfxMessageBox(GetStringTable(IDS_TEXT_TRANS_COMPLITE));
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
		nRtn = FALSE;
	}

_EXIT_:
	EndWaitCursor();
	pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;
	
	return nRtn;
}

CString CCalFileDlg::GetIPAddress(int nModuleID)
{
	CString address;
	// 접속된 모듈 
	if(EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)
	{
		address = EPGetModuleIP(nModuleID);
	}
	if(!address.IsEmpty())	return address;
	
	//DataBase 검색 
	CDaoDatabase  db;

	CString strDataBaseName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "database");
	if(strDataBaseName.IsEmpty())	return  address;

	strDataBaseName = strDataBaseName+ "\\"+FORM_SET_DATABASE_NAME;
	db.Open(strDataBaseName);

	CString strSQL;
//	strSQL.Format("SELECT IP_Address FROM SystemConfig WHERE ModuleID = %d AND ModuleType = %d", nModuleID, ((CCTSMonApp*)AfxGetApp())->GetSystemType());
	if(((CCTSMonApp*)AfxGetApp())->GetSystemType() != 0)
		strSQL.Format("SELECT IP_Address FROM SystemConfig WHERE ModuleID = %d AND ModuleType = %d", nModuleID, ((CCTSMonApp*)AfxGetApp())->GetSystemType());
	else
		strSQL.Format("SELECT IP_Address FROM SystemConfig WHERE ModuleID = %d", nModuleID);

	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	if(!rs.IsBOF())
	{
		COleVariant data = rs.GetFieldValue(0);
		address = data.pbVal;
	}

	rs.Close();
	db.Close();

	//수동 입력 
	if(address.IsEmpty())
	{
		CIPSetDlg dlg;
		if(dlg.DoModal() == IDOK)
			address = dlg.GetIPAddress();
	}

	return address;
}

int CCalFileDlg::GetBoardNo(CString strFileName)
{
	if(strFileName.IsEmpty())	return 0;
	int data = 0;

	FILE *fp = fopen((LPSTR)(LPCTSTR)strFileName, "rt");
	if(fp != NULL)
	{
		char szBuff[128];
		fscanf(fp, "%s", szBuff);		//File serial
		fscanf(fp, "%s", szBuff);		//Board Serial
		fscanf(fp, "%s", szBuff);		//date Time					
		fscanf(fp, "%d", &data);		//Board Num
		fclose(fp);
	}
	return data;
}

BOOL CCalFileDlg::UpdateModuleFileList(int nModuleID)
{
	m_ctrlModuleFileList.DeleteAllItems();


	CString strIPAddress;
	BOOL nRtn = TRUE;
	strIPAddress = GetIPAddress(nModuleID);

	if(strIPAddress.IsEmpty())
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_ERROR_FOUND_IP), MB_ICONSTOP|MB_OK);
		return FALSE;
	}
 
	CString strLoginID;
	CString strPassword;
	CString strLocation;
	CString toFileName, fromFileName;
	CString path;
	CString source, destination;

	BeginWaitCursor();
	strLoginID = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login ID", "root");
	strPassword = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Login Password", "dusrnth");
	strLocation = AfxGetApp()->GetProfileString(FORM_FTP_SECTION, "Cal Location", "formation_data/config/caliData");
	path = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "CTSMon");
	
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;

	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(strIPAddress, strLoginID, strPassword);
		if(pFtpConnection != NULL && !pFtpConnection->SetCurrentDirectory(strLocation))
		{
			MessageBox(GetStringTable(IDS_TEXT_SAVE_DIR_ERROR), "Direcotry Error", MB_OK|MB_ICONSTOP);
			nRtn = FALSE;
			goto _EXIT_;
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();

		ftpSession.Close();
		EndWaitCursor();
		return FALSE;
	}
	
	
	try
	{
		path = path +"\\Temp\\";
		EP_MD_SYSTEM_DATA *sysData = EPGetModuleSysData(EPGetModuleIndex(nModuleID));
		int nBoard;
		if(sysData == NULL)
		{
			nBoard = EP_MAX_BD_PER_MD;
		}
		else
		{
			nBoard =  sysData->wInstalledBoard;
		}

		for(int bd = 0; bd <nBoard; bd++)
		{
			fromFileName.Format("L_CALI_C%d", bd+1);
			toFileName.Format("L_CALI_C%d.txt", bd+1);
			toFileName = path + toFileName;
			_unlink(toFileName);			//파일이 있으면 삭제 
			
			if(pFtpConnection->GetFile(fromFileName, toFileName, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_ASCII))
			{
				if(AddModuleFileList(toFileName)== FALSE)
				{
					TRACE("Fail Adding Module File List\n");
				}
			}
			else
			{
				TRACE("%s Calibration File Down Load Fail\n", fromFileName);
			}
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
//		AfxMessageBox(sz);
		e->Delete();
		nRtn = FALSE;
	}

_EXIT_:
	EndWaitCursor();
	pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;
	
	EndWaitCursor();
	return nRtn;
}

BOOL CCalFileDlg::AddModuleFileList(CString strFileName)
{
	if(strFileName.IsEmpty())	return FALSE;

	FILE *fp = fopen((LPSTR)(LPCTSTR)strFileName, "rt");
	CString dateTime;
	char szBuff[128];
	int nBoardNo = 0;

	if(fp != NULL)
	{
		fscanf(fp, "%s", szBuff);		//File serial
		fscanf(fp, "%s", szBuff);		//Board Serial


		fscanf(fp, "%s", szBuff);		//date Time	
		dateTime = szBuff;
		fscanf(fp, "%d", &nBoardNo);		//date Time	
		fclose(fp);
	}
	
	int nCount = m_ctrlModuleFileList.GetItemCount();

	LVITEM lvi;

	lvi.mask =  LVIF_TEXT;
	lvi.iItem = nCount;
	lvi.iSubItem = 0;
	sprintf(szBuff, "%d", nBoardNo);
	lvi.pszText = szBuff;
	m_ctrlModuleFileList.InsertItem(&lvi);
	
	lvi.iSubItem = 1;
	sprintf(szBuff, "%s", dateTime);
	lvi.pszText = szBuff;
	m_ctrlModuleFileList.SetItem(&lvi);
				
	return TRUE;
}

void CCalFileDlg::OnDblclkModuleFileList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nSelected = -1;
	CString path = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "CTSMon");
	path += "\\Temp";

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	char szWinDir[128];
	if(GetWindowsDirectory(szWinDir, 127) <=0 )		return;
	
	CString strTemp;
	CString strFileName;

	while((nSelected = m_ctrlModuleFileList.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
	{
		strTemp = m_ctrlModuleFileList.GetItemText(nSelected , 0 );
		strFileName.Format("%s\\L_CALI_C%s.txt", path, strTemp);
		strTemp.Format("%s\\Notepad.exe %s", szWinDir, strFileName);
		
		BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		if(bFlag == FALSE)
		{
			strTemp.Format(GetStringTable(IDS_MSG_NOT_FOUND), strFileName);
			MessageBox(strTemp, "File not found", MB_OK|MB_ICONSTOP);
		}	
	}
	*pResult = 0;
}

void CCalFileDlg::OnDblclkCalFileList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nID = 0;
	int nIndex = m_ctrlModuleSel.GetCurSel();
	if(nIndex < 1)		return;

	nID = EPGetModuleID(nIndex-1);

	int nSelected = -1;
	CString path = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "CTSMon");
	CString strTemp;

	strTemp.Format("\\CalData\\Module %d", nID);
	path = path + strTemp;

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	char szWinDir[128];
	if(GetWindowsDirectory(szWinDir, 127) <=0 )		return;
	
	CString strFileName;

	while((nSelected = m_ctrlFileList.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
	{
		strTemp = m_ctrlFileList.GetItemText(nSelected , 0 );
		strFileName.Format("%s\\%s", path, strTemp);
		strTemp.Format("%s\\Notepad.exe %s", szWinDir, strFileName);
		
		BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		if(bFlag == FALSE)
		{
			strTemp.Format(GetStringTable(IDS_MSG_NOT_FOUND), strFileName);
			MessageBox(strTemp, "File not found", MB_OK|MB_ICONSTOP);
		}	
	}
	*pResult = 0;
}
