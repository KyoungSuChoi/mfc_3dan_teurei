// ChCaliData.h: interface for the CChCaliData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_)
#define AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CaliPoint.h"

#define CAL_DATA_EMPTY	0
#define CAL_DATA_SAVED	1
#define CAL_DATA_MODIFY	2
#define CAL_DATA_ERROR	3	

// 20200622 KSCHOI Add Temperature Data In Calibration START
#define CAL_FILE_VERSION				0x1000				// Not Including Sensor Data
#define CAL_FILE_VERSION_SENSOR_DATA	CAL_FILE_VERSION+1	// Including Sensor Data
// 20200622 KSCHOI Add Temperature Data In Calibration START

typedef struct s_Calibraton_Data 
{
	double	dAdData;
	double	dMeterData;
}	CAL_DATA;

class CChCaliData : public CCaliPoint 
{
public:
	void SetHWChNo(int nBoardNo, int nChNo)		{	m_nBoardNo = nBoardNo;  m_nChannelNo =nChNo ;	}
	void GetHWChNo(int &nBoardNo, int &nChNo)	{	nBoardNo = m_nBoardNo;  nChNo = m_nChannelNo;	}
	void SetState(int nState = CAL_DATA_MODIFY)	{	 m_nEdited = nState;	}
	BOOL GetState()	{	return m_nEdited;	}
	BOOL WriteDataToFile(FILE *fp);
	BOOL ReadDataFromFile(FILE *fp, int iFileSize);
	//Multi Range FTN
	void GetVCalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetVCheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);

	//1 Range FTN
	void GetVCalData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetVCheckData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetICalData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetICheckData(WORD wPoint, double &dAdData, double &dMeterData);

	//Multi Range FTN
	void SetVCalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetVCheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);

// 20200622 KSCHOI Add Temperature Data In Calibration START
	void GetVCalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pTemperatureArray);
	void GetVCheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pTemperatureArray);
	void GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pTemperatureArray);
	void GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData, double* pTemperatureArray);

	void SetVCalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray);
	void SetVCheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray);
	void SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray);
	void SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData, _SENSOR_SENSING_DATA* pSensorDataArray);

	BOOL WriteDataToFile(FILE *fp, UINT nFileVersion);
	BOOL ReadDataFromFile(FILE *fp, UINT nFileVersion, int iFIleSize);

private :
	double TransDataValue(LONG lValue, int iPrecision);
public:
// 20200622 KSCHOI Add Temperature Data In Calibration END

	CChCaliData();
	virtual ~CChCaliData();

protected:
	int			m_nBoardNo;		// 1Base
	int			m_nChannelNo;	// 1Base
	int			m_nEdited;
	CAL_DATA	m_VCalData[CAL_MAX_VOLTAGE_RANGE][CAL_MAX_CALIB_SET_POINT];
	CAL_DATA	m_VCheckData[CAL_MAX_VOLTAGE_RANGE][CAL_MAX_CALIB_CHECK_POINT];
	CAL_DATA	m_ICalData[CAL_MAX_CURRENT_RANGE][CAL_MAX_CALIB_SET_POINT];
	CAL_DATA	m_ICheckData[CAL_MAX_CURRENT_RANGE][CAL_MAX_CALIB_CHECK_POINT];
// 20200622 KSCHOI Add Temperature Data In Calibration START
	double		m_VCalTemperatatureData[1][CAL_MAX_CALIB_SET_POINT][EP_MAX_SENSOR_CH];
	double		m_VCheckTemperatatureData[1][CAL_MAX_CALIB_SET_POINT][EP_MAX_SENSOR_CH];
	double		m_ICalTemperatatureData[1][CAL_MAX_CALIB_SET_POINT][EP_MAX_SENSOR_CH];
	double		m_ICheckTemperatatureData[1][CAL_MAX_CALIB_SET_POINT][EP_MAX_SENSOR_CH];
// 20200622 KSCHOI Add Temperature Data In Calibration END
};

#endif // !defined(AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_)
