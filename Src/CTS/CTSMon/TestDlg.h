#pragma once
#include "afxwin.h"


// CTestDlg 대화 상자입니다.

class CTestDlg : public CDialog
{
	DECLARE_DYNAMIC(CTestDlg)

public:
	CTestDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTestDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_TEST };
	
	INT m_curId;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnState1();
	afx_msg void OnBnClickedBtnState2();
	afx_msg void OnBnClickedBtnState3();
	afx_msg void OnBnClickedBtnState4();
	afx_msg void OnBnClickedBtnState5();
	afx_msg void OnBnClickedBtnState6();
	afx_msg void OnBnClickedBtnState7();
	afx_msg void OnBnClickedBtnState8();
	afx_msg void OnBnClickedBtnState9();
	CComboBox m_ctlCB_ID;
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeCbId();
	afx_msg void OnBnClickedBtnState10();
	afx_msg void OnBnClickedBtnState11();
	afx_msg void OnBnClickedBtnState12();
	afx_msg void OnBnClickedBtnState13();
	afx_msg void OnBnClickedBtnState14();
	afx_msg void OnBnClickedBtnState15();
};
