#pragma once

#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "afxwin.h"
#include "InformationDlg.h"
// FanModeSettingDlg 대화 상자입니다.

#define FAN_MODE_SETTING_FAN_TYPE				1
#define FAN_MODE_SETTING_FAN_NAME				2
#define FAN_MODE_CURRENT_FAN_SETTING			3

class FanModeSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(FanModeSettingDlg)

public:
	FanModeSettingDlg(CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleNo);
	virtual ~FanModeSettingDlg();

	CCTSMonDoc* m_pDoc;
	int			m_nCurModuleNo;

	int m_nCurrentJigFanCount;
	int m_nCurrentClipFanCount;

	CMyGridWnd m_wndFanModeSettingGrid;
	CColorButton2 m_cApplyBtn;

	CComboBox m_combo_Stage_List;

	void	InitializeGrid();
	BOOL	InitFirstRow();
	BOOL	InitDefaultRows(int nJigFanCount = 6, int nClipFanCount = 6);
	BOOL	ClearGrid();

	void SetCurrentModuleNo(int nModuleNo);
	void OnFanModeSettingResponse(int nModuleNo, EP_FAN_INFORMATION_RESPONSE response);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FAN_MODE_SETTING };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnCbnSelchangeComboStageList();
	void InitializeComboList();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnFanModeSettingApply();
	afx_msg void OnBnClickedBtnFanModeSettingRefresh();
};
