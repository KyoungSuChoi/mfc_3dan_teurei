// ViewSet.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ViewSet.h"


// CViewSet 대화 상자입니다.

IMPLEMENT_DYNAMIC(CViewSet, CDialog)

CViewSet::CViewSet(CWnd* pParent /*=NULL*/)
	: CDialog(CViewSet::IDD, pParent)
	, m_bLayoutView(FALSE)
	, m_bDataDownMessage(FALSE)
	, m_bRackIndexUp(FALSE)
	, m_bSmallChLayout(FALSE)
	, m_bShowChannelMonitoringView(FALSE)
	, m_bShowChannelResultView(FALSE)
	, m_bChkKorea(FALSE)
	, m_bChkEnglish(FALSE)
	, m_bChkChinese(FALSE)
	, m_nModulePerRack(0)
	, m_nCellInTray(0)
	, m_nTopGridColCount(0)
{
	m_Lang =0;
}

CViewSet::~CViewSet()
{
}

void CViewSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_LAYOUTVIEW, m_bLayoutView);
	DDX_Check(pDX, IDC_CHECK_DATADOWN_MESSAGE, m_bDataDownMessage);
	DDX_Check(pDX, IDC_CHECK_RACKINDEXUP, m_bRackIndexUp);
	DDX_Check(pDX, IDC_CHECK_SMALL_CH_LAYOUT, m_bSmallChLayout);
	DDX_Check(pDX, IDC_CHECK_SHOW_MONITORING, m_bShowChannelMonitoringView);
	DDX_Check(pDX, IDC_CHECK_SHOW_RESULT, m_bShowChannelResultView);
	DDX_Check(pDX, IDC_CHECK_KOR, m_bChkKorea);
	DDX_Check(pDX, IDC_CHECK_ENG, m_bChkEnglish);
	DDX_Check(pDX, IDC_CHECK_CHIN, m_bChkChinese);
	DDX_Check(pDX, IDC_CHECK_HUNG, m_bChkHungarian);
	DDX_Text(pDX, IDC_EDIT_MODULE_PER_RACK, m_nModulePerRack);
	DDX_Text(pDX, IDC_EDIT_CELLINTARY, m_nCellInTray);
	DDX_Text(pDX, IDC_EDIT_TOPGRIDCOLCOUT, m_nTopGridColCount);
}


BEGIN_MESSAGE_MAP(CViewSet, CDialog)
	ON_BN_CLICKED(IDOK, &CViewSet::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECK_KOR, &CViewSet::OnBnClickedCheckKor)
	ON_BN_CLICKED(IDC_CHECK_ENG, &CViewSet::OnBnClickedCheckEng)
	ON_BN_CLICKED(IDC_CHECK_CHIN, &CViewSet::OnBnClickedCheckChin)
	ON_BN_CLICKED(IDC_CHECK_HUNG, &CViewSet::OnBnClickedCheckHung)
END_MESSAGE_MAP()


// CViewSet 메시지 처리기입니다.

void CViewSet::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	LangSet();
	RegSet();

	OnOK();
}

void CViewSet::LangSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	HKEY hKey;
	DWORD pBuf=0;

	pBuf = m_Lang;

	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSEditor\\Config", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSAnalyzer\\FormSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSGraphAnal\\Config", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\DataDown\\SaveSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);
	RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\ExcelTrans\\SaveSetting", 0, KEY_ALL_ACCESS, &hKey);
	RegSetValueEx(hKey, TEXT("LANGUAGE"), 0, REG_DWORD, (BYTE*)&pBuf, sizeof(DWORD));
	RegCloseKey(hKey);	
}

void CViewSet::RegSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "LayoutView", m_bLayoutView);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "DataDown Message", m_bDataDownMessage);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "RackIndexUp", m_bRackIndexUp);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Small_Ch_Layout", m_bSmallChLayout);
	
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Show ChannelMonitoringView", m_bShowChannelMonitoringView);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Show ChannelResultView", m_bShowChannelResultView);
	
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "Module Per Rack", m_nModulePerRack);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "CellInTray", m_nCellInTray);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TopGridColCout", m_nTopGridColCount);
}

void CViewSet::Reginit()
{
	m_bLayoutView = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "LayoutView", FALSE);
	m_bDataDownMessage = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataDown Message", FALSE);
	m_bRackIndexUp = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "RackIndexUp", FALSE);
	m_bSmallChLayout = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Small_Ch_Layout", FALSE);

	m_bShowChannelMonitoringView = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Show ChannelMonitoringView", FALSE);
	m_bShowChannelResultView = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Show ChannelResultView", FALSE);
	
	m_nModulePerRack = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION , "Module Per Rack", 3);
	m_nCellInTray = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "CellInTray", 128);
	m_nTopGridColCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 16);
}

void CViewSet::Langinit()
{
	long rtn1, rtn2, rtn3, rtn4,  rtn5,  rtn6;
	DWORD type;
	HKEY hKey = 0;
	DWORD size = 511;
	BYTE buf[512], buf2[512];

	rtn1 = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSMon\\FormSetting", 0, KEY_READ, &hKey);
	rtn2 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSEditor\\Config", 0, KEY_READ, &hKey);
	rtn3 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSAnalyzer\\FormSetting", 0, KEY_READ, &hKey);
	rtn4 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\CTSGraphAnal\\Config", 0, KEY_READ, &hKey);
	rtn5 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\DataDown\\SaveSetting", 0, KEY_READ, &hKey);
	rtn6 = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTS\\ExcelTrans\\SaveSetting", 0, KEY_READ, &hKey);
	if (rtn1 == ERROR_SUCCESS && rtn2 == ERROR_SUCCESS && rtn3 == ERROR_SUCCESS 
		&& rtn4 == ERROR_SUCCESS && rtn5 == ERROR_SUCCESS && rtn6 == ERROR_SUCCESS)
	{
		size = 255;
		RegQueryValueEx(hKey, "LANGUAGE", NULL, &type, buf, &size);
		sprintf((char *)buf2, "%ld", *(long *)buf);
		m_Lang = atol((LPCTSTR)buf2);

		if (m_Lang==0)
		{
			m_bChkKorea = TRUE;			
		}
		else if (m_Lang==1)
		{
			m_bChkEnglish = TRUE;			
		}
		else if (m_Lang==2)
		{
			m_bChkChinese = TRUE;
		}
		else if (m_Lang==3)
		{
			m_bChkHungarian = TRUE;
		}
		UpdateData(FALSE);
	}
}
BOOL CViewSet::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Langinit();
	Reginit();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CViewSet::OnBnClickedCheckKor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Lang =0;
	m_bChkKorea = TRUE;
	m_bChkEnglish = FALSE;
	m_bChkChinese = FALSE;
	m_bChkHungarian = FALSE;
	UpdateData(FALSE);
}

void CViewSet::OnBnClickedCheckEng()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Lang =1;
	m_bChkKorea = FALSE;
	m_bChkEnglish = TRUE;
	m_bChkChinese = FALSE;
	m_bChkHungarian = FALSE;
	UpdateData(FALSE);
}

void CViewSet::OnBnClickedCheckChin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Lang =2;
	m_bChkKorea = FALSE;
	m_bChkEnglish = FALSE;
	m_bChkChinese = TRUE;
	m_bChkHungarian = FALSE;
	UpdateData(FALSE);
}

void CViewSet::OnBnClickedCheckHung()
{
	m_Lang =3;
	m_bChkKorea = FALSE;
	m_bChkEnglish = FALSE;
	m_bChkChinese = FALSE;
	m_bChkHungarian = TRUE;
	UpdateData(FALSE);
}
