#pragma once
#include "afxwin.h"

#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "MatrixStatic.h"

// CMonitoringView 폼 뷰입니다.

class CMonitoringView : public CFormView
{
	DECLARE_DYNCREATE(CMonitoringView)

protected:
	CMonitoringView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CMonitoringView();

public:
	enum { IDD = IDD_MONITORING_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CCTSMonDoc* GetDocument();

	void InitLabel();
	void InitSmallChGrid();
	void SetTopGridFont();
	void DataUnitChanged();
	void GroupSetChanged( int nColCount = -1 );
	void DrawChannel();
	void StartMonitoring();
	void StopMonitoring();

public:
	CMyGridWnd	m_wndSmallChGrid;
	int m_nCurModuleID;
	int m_nTopGridRowCount;
	int m_nTopGridColCount;
	int m_nCurTrayIndex;
	UINT m_nDisplayTimer;

	STR_TOP_CONFIG		*m_pConfig;

	SHORT	*m_pRowCol2GpCh;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CLabel m_LabelViewName;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnInitialUpdate();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CMonitoringView::GetDocument()
{ return (CCTSMonDoc*)m_pDocument; }
#endif


