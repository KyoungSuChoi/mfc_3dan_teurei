#pragma once


// CViewSet 대화 상자입니다.

class CViewSet : public CDialog
{
	DECLARE_DYNAMIC(CViewSet)

public:
	CViewSet(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CViewSet();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CTSMON_REGSETTING };

	void Langinit();
	void Reginit();
	void LangSet();
	void RegSet();

	int m_Lang;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	BOOL m_bLayoutView;
	BOOL m_bDataDownMessage;
	BOOL m_bRackIndexUp;
	BOOL m_bSmallChLayout;
	BOOL m_bShowChannelMonitoringView;
	BOOL m_bShowChannelResultView;
	BOOL m_bChkKorea;
	BOOL m_bChkEnglish;
	BOOL m_bChkChinese;
	BOOL m_bChkHungarian;
	UINT m_nModulePerRack;
	UINT m_nCellInTray;
	UINT m_nTopGridColCount;
	afx_msg void OnBnClickedCheckKor();
	afx_msg void OnBnClickedCheckEng();
	afx_msg void OnBnClickedCheckChin();
	afx_msg void OnBnClickedCheckHung();
};
