#pragma once


#include "afxwin.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"


// CStageVersion 대화 상자입니다.

class CStageVersion : public CDialog
{
	DECLARE_DYNAMIC(CStageVersion)

public:
	CStageVersion(CWnd* pParent,CCTSMonDoc* pDoc);   // 표준 생성자입니다.
	virtual ~CStageVersion();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_STAGE_VERSION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:

	void initCtrl();
	void initGrid();
	void initLabel();
	void initFont();
	void initCombo();
	void initValue();

/*	void ModuleConnected(int nModuleID);*/
	void ModuleDisConnected(int nModuleID);
	void SetCurrentModule(int nModuleID);
	void RecvData(int nModuleID);
	void UpdateAllData(int nModuleID);
	CString GetTargetModuleName();


	CCTSMonDoc* m_pDoc;
	int m_nCurModuleID;
	int m_nMaxStageCnt;

	CMyGridWnd      m_grid_SW_Version;
	CMyGridWnd      m_grid_FW_Version;
	CMyGridWnd      m_grid_Board_Version;
	CMyGridWnd      m_grid_Inverter_Version;

	CLabel			m_LabelViewName;
	CLabel			m_LabelViewSW;
	CLabel			m_LabelViewFW;
	CLabel			m_LabelViewBoard;
	CLabel			m_LabelViewInverter;
	CLabel			m_CmdTarget;

	CFont			m_Font;


	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnRefresh();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CComboBox m_comboStageID;
	afx_msg void OnCbnSelchangeComboStageChange();
	afx_msg void OnBnClickedBtnRefresh2();
	afx_msg void OnBnClickedBtnSaveExcel();


};
