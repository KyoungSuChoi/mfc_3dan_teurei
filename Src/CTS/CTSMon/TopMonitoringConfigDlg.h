#if !defined(AFX_TOPMONITORINGCONFIGDLG_H__5558E097_40D7_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_TOPMONITORINGCONFIGDLG_H__5558E097_40D7_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TopMonitoringConfigDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTopMonitoringConfigDlg dialog

class CTopMonitoringConfigDlg : public CDialog
{
// Construction
public:
	void DrawColor();
	CTopMonitoringConfigDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTopMonitoringConfigDlg)
	enum { IDD = IDD_TOP_CONFIG_DLG };
	CSpinButtonCtrl	m_ChannelSpin;
	CSpinButtonCtrl	m_ModuleSpin;
	UINT			m_ModuleInterval;
	UINT			m_ChannelInterval;
	UINT			m_ChannelSaveInterval;
	BOOL			m_DisplayText;
	BOOL			m_DisplayColor;
	BOOL			m_bLevelColor;
	
	float	m_fMaxLevel1;
	float	m_fMaxLevel2;
	float	m_fMaxLevel3;
	
	float	m_fMinLevel1;
	float	m_fMinLevel2;
	float	m_fMinLevel3;
	//}}AFX_DATA
	STR_TOP_CONFIG 	m_TopCfg;
	
	BOOL			m_Flash[STATE_COLOR];
	CLabel			m_Result[STATE_COLOR];
	
	stingray::foundation::SECWellButton m_TWellColor[STATE_COLOR];
	stingray::foundation::SECWellButton m_BWellColor[STATE_COLOR];
	
	BOOL			m_AutoMode_Flash[STATE_AUTO_COLOR];
	CLabel			m_AutoMode_Result[STATE_AUTO_COLOR];
	stingray::foundation::SECWellButton m_AutoMode_TWellColor[STATE_AUTO_COLOR];
	stingray::foundation::SECWellButton m_AutoMode_BWellColor[STATE_AUTO_COLOR];

	BOOL			m_bOverColor;
	stingray::foundation::SECWellButton m_IOverColor;
	stingray::foundation::SECWellButton m_VOverColor;
	// -----------------------------------------------------------------------
	// 전압 Level 별 색상 설정
	// -----------------------------------------------------------------------
	stingray::foundation::SECWellButton	m_DefaultColor;
	stingray::foundation::SECWellButton	m_LevelColor1;
	stingray::foundation::SECWellButton	m_LevelColor2;
	stingray::foundation::SECWellButton	m_LevelColor3;
	

	float			m_fVRange;
	float			m_fIRange;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTopMonitoringConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTopMonitoringConfigDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnTopcfgSetdefault();
	afx_msg void OnDeltaposTopcfgModuleSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposTopcfgChannelSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnChangeTopcfgModuleTimer();
	afx_msg void OnChangeTopcfgChannelTimer();
	//}}AFX_MSG
	afx_msg LONG OnColorChange(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnStnClickedTopcfgeResult20();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOPMONITORINGCONFIGDLG_H__5558E097_40D7_11D2_80A6_0020741517CE__INCLUDED_)
