#if !defined(AFX_SERIALCONFIGDLG_H__51674942_3258_11D2_975A_444553540000__INCLUDED_)
#define AFX_SERIALCONFIGDLG_H__51674942_3258_11D2_975A_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
//SerialConfigDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSerialConfigDlg dialog
////#include "EPIROCV.h"

class CSerialConfigDlg : public CDialog
{
// Construction
public:
	CSerialConfigDlg(CWnd* pParent = NULL);   // standard constructor
	SERIAL_CONFIG	m_SerialConfig[2];
	CString m_ParityStr;
// Dialog Data
	//{{AFX_DATA(CSerialConfigDlg)
	enum { IDD = IDD_SERIAL_DLG };
	CString	m_strBaudRate;
	CString	m_strDataBits;
	CString	m_strStopBits;
	CString	m_strPortNum;
	CString	m_strSendBuffer;
	int		m_nParity;
	CString		m_strPortNum1;
	CString		m_strBaudRate1;
	int		m_nParity1;
	CString		m_strDataBits1;
	CString		m_strStopBits1;
	CString		m_strSendBuffer1;
	BOOL	m_bUsePort1;
	//}}AFX_DATA
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSerialConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnOk();
	afx_msg void OnUseCheck();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCONFIGDLG_H__51674942_3258_11D2_975A_444553540000__INCLUDED_)
