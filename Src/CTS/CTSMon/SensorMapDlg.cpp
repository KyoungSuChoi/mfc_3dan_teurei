// SensorMapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "SensorMapDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSensorMapDlg dialog


CSensorMapDlg::CSensorMapDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSensorMapDlg::IDD, pParent),
	m_nModuleID(0),
	m_pDoc(pDoc),
	m_bUseTempWarning(FALSE),
	m_nWaringRed(0),
	m_nWaringYellow(0),
	m_sDangerTemp(0),
	m_nJigTargetTemp(0),
	m_iJigTempWarningRefValue(0),
	m_iJigTempErrorRefValue(0),
	m_iPowerTempWarningRefValue(0),
	m_iPowerTempErrorRefValue(0),
	m_iJigFanFaultErrorCount(0),
	m_iDCFanFaultErrorCount(0),
	m_lastApplyModuleID(0)
{
	LanguageinitMonConfig(_T("CSensorMapDlg"));
	
	ZeroMemory(&m_sendSensorSetData, sizeof(EP_SENSOR_SET_DATA));	
}
CSensorMapDlg::~CSensorMapDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSensorMapDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

void CSensorMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSensorMapDlg)
	DDX_Control(pDX, IDC_BUTTON_APPLY, m_Apply_Btn);
	DDX_Control(pDX, IDCANCEL, m_Cancel_Btn);
	DDX_Control(pDX, IDC_APPLY_ALL_BUTTON, m_btnApplyAll);
	DDX_Control(pDX, IDC_MD_SEL_COMBO, m_ctrlModuleSel);
	DDX_Control(pDX, IDC_LAMP_GREEN_STATIC, m_ctrlGreenLamp);
	DDX_Control(pDX, IDC_LAMP_YELLOW_STATIC, m_ctrlYellowLamp);
	DDX_Control(pDX, IDC_LAMP_RED_STATIC, m_ctrlRedLamp);
	DDX_Check(pDX, IDC_USE_TEMP_WARNING_CHECK, m_bUseTempWarning);
	DDX_Text(pDX, IDC_LAMP_RED_EDIT, m_nWaringRed);
	DDX_Text(pDX, IDC_LAMP_YELLOW_EDIT, m_nWaringYellow);	
	DDX_Text(pDX, IDC_DANGER_TEMP_EDIT, m_sDangerTemp);	
	DDX_Text(pDX, IDC_JIG_TARGET_TEMP_EDIT, m_nJigTargetTemp);	
	
	// Jig Temp Warning/Error Setting
	DDX_Text(pDX, IDC_EDIT_JIG_TEMP_WARNING_MAX, m_iJigTempWarningRefValue);
	DDV_MinMaxInt(pDX, m_iJigTempWarningRefValue, 40, 80);
	DDX_Text(pDX, IDC_EDIT_JIG_TEMP_ERROR_MAX, m_iJigTempErrorRefValue);
	DDV_MinMaxInt(pDX, m_iJigTempErrorRefValue, 55, 80);

	// Power Temp Warning/Error Setting
	DDX_Text(pDX, IDC_EDIT_POWER_TEMP_WARNING_MAX, m_iPowerTempWarningRefValue);
	DDV_MinMaxInt(pDX, m_iPowerTempWarningRefValue, 40, 80);
	DDX_Text(pDX, IDC_EDIT_POWER_TEMP_ERROR_MAX, m_iPowerTempErrorRefValue);
	DDV_MinMaxInt(pDX, m_iPowerTempErrorRefValue, 55, 80);

	// Fan Fault Error Count Setting
	DDX_Text(pDX, IDC_EDIT_JIG_FAN_FAULT_ERROR_COUNT, m_iJigFanFaultErrorCount);
	DDV_MinMaxInt(pDX, m_iJigFanFaultErrorCount, 0, 32);
	DDX_Text(pDX, IDC_EDIT_DC_FAN_FAULT_ERROR_COUNT, m_iDCFanFaultErrorCount);
	DDV_MinMaxInt(pDX, m_iDCFanFaultErrorCount, 0, 60);
	//}}AFX_DATA_MAP	
}


BEGIN_MESSAGE_MAP(CSensorMapDlg, CDialog)
	//{{AFX_MSG_MAP(CSensorMapDlg)
	ON_CBN_SELCHANGE(IDC_MD_SEL_COMBO, OnSelchangeMdSelCombo)
	ON_BN_CLICKED(IDC_APPLY_ALL_BUTTON, OnApplyAllButton)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CSensorMapDlg::OnBnClickedButtonApply)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSensorMapDlg message handlers

void CSensorMapDlg::InitMappingGrid()
{
	m_wndMapGrid.SubclassDlgItem(IDC_SENSOR_MAP_GRID, this);
	m_wndMapGrid.m_bSameRowSize = FALSE;
	m_wndMapGrid.m_bSameColSize = FALSE;
	m_wndMapGrid.m_bCustomWidth = TRUE;

	m_wndMapGrid.m_nWidth[1]	= 60;
	m_wndMapGrid.m_nWidth[2]	= 60;
	m_wndMapGrid.m_nWidth[3]	= 100;
	m_wndMapGrid.Initialize();
	BOOL bLock = m_wndMapGrid.LockUpdate();

	m_wndMapGrid.SetColCount(3);
	m_wndMapGrid.SetRowCount(EP_MAX_SENSOR_CH);
	m_wndMapGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

//	m_wndMapGrid.m_bCustomColor 	= TRUE;
	m_wndMapGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	m_wndMapGrid.SetValueRange(CGXRange(0,1), TEXT_LANG[0]);//"센서번호"
	m_wndMapGrid.SetValueRange(CGXRange(0,2), TEXT_LANG[1]);//"모듈채널"
	m_wndMapGrid.SetValueRange(CGXRange(0,3), TEXT_LANG[2]);//"이름"

	m_wndMapGrid.SetDefaultRowHeight(18);
	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255, 255, 255)));

	//Mapping Table display
	CString strType, strTemp;
	strType = "None\n";	
	int i = 0;
 	for(i=0; i<EP_DEFAULT_CH_PER_MD; i++)
 	{
 		strTemp.Format("%d\n", i+1);
 		strType += strTemp;
 	}
	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(2),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
				.SetHorizontalAlignment(DT_CENTER)
				.SetChoiceList(strType)
	);

	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(3),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_EDIT)
				.SetMaxLength(16)
	);

	for(i=0; i<EP_MAX_SENSOR_CH; i++)
	{
		strTemp.Format("%d\n", i+1);
		m_wndMapGrid.SetValueRange(CGXRange(i+1, 1), strTemp);
		strTemp.Format("T %d", i+1);
		m_wndMapGrid.SetValueRange(CGXRange(i+1, 3), strTemp);
	}

	((CButton*)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE); 

	m_wndMapGrid.LockUpdate(bLock);
	m_wndMapGrid.Redraw();
}

BOOL CSensorMapDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here

	m_ctrlGreenLamp.SetBkColor(RGB(0, 220, 0));
	m_ctrlYellowLamp.SetBkColor(RGB(220, 200, 0));
	m_ctrlRedLamp.SetBkColor(RGB(220, 0, 0));

//	SetWindowTheme(GetDlgItem(IDC_USE_TEMP_WARNING_CHECK)->m_hWnd, L"", L"");
//	SetWindowTheme(GetDlgItem(IDC_RADIO1)->m_hWnd, L"", L"");

	int nModID, nDefaultID = 0;
	for(int i=0; i<m_pDoc->GetInstalledModuleNum(); i++)
	{	
		nModID = EPGetModuleID(i);
		m_ctrlModuleSel.AddString(::GetModuleName(nModID));
		m_ctrlModuleSel.SetItemData(i, nModID);
		if(nModID == m_nModuleID)	nDefaultID = i;
	}
	
	m_ctrlModuleSel.SetCurSel(nDefaultID);

/*	EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(m_nModuleID);
	if(lpSysParam != NULL)	
	{
		m_bUseTempWarning = lpSysParam->bUseTempLimit;
		m_lTempMax = ETC_PRECISION(lpSysParam->lMaxTemp);

		m_nWaringRed = lpSysParam->wWarningNo2;
		m_nWaringYellow = lpSysParam->wWarningNo1;
	}
*/
	InitMappingGrid();

	DisplayMapping(m_nModuleID);	
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSensorMapDlg::OnSelchangeMdSelCombo() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_ctrlModuleSel.GetCurSel();
	if(nSel == LB_ERR)	return;

	//이전 Module Update
	UpdateData(TRUE);
	if(ApplyToModule(m_nModuleID) == FALSE)
	{
		CString strTemp;
		strTemp.Format(TEXT_LANG[3], ::GetModuleName(m_nModuleID));//"%s Sensor mapping save fail!!!"
		AfxMessageBox(strTemp);

	}
	m_nModuleID = m_ctrlModuleSel.GetItemData(nSel);
	DisplayMapping(m_nModuleID);
}

void CSensorMapDlg::OnOK() 
{
	// TODO: Add extra validation here


	CDialog::OnOK();
}

void CSensorMapDlg::OnApplyAllButton() 
{
	if(UpdateData(TRUE))
	{
		if(MessageBox(TEXT_LANG[4], TEXT_LANG[5], MB_ICONQUESTION|MB_YESNO) == IDYES) //"현재 설정을 모든 모듈에 적용 하시겠습니까?" //"적용"
		{
			//20200831 ksj
			m_arraySendError.RemoveAll();
			for(int i=1; i<= m_pDoc->GetInstalledModuleNum(); i++)
			{
				if(EPGetModuleState(i) != EP_STATE_LINE_OFF)
				{
					m_lastApplyModuleID = i;
				}
			}

			CString strTemp;
			m_pDoc->SetProgressWnd(0, m_pDoc->GetInstalledModuleNum(), "Sending sensormap data to module...");
			for(int i=1; i<= m_pDoc->GetInstalledModuleNum(); i++)
			{
				//if(ApplyToModule(EPGetModuleID(i)) == FALSE)
				if(ApplyToModule(i) == FALSE)		//2008-12-01 ljb
				{
					strTemp.Format("%s Sensor mapping save fail!!!", ::GetModuleName(EPGetModuleID(i)));
					AfxMessageBox(strTemp);
				}

				m_pDoc->SetProgressPos(i);
			}
		}

		m_pDoc->HideProgressWnd();
	}
}

BOOL CSensorMapDlg::ApplyToModule(int nModuleID)
{
		UpdateData(TRUE);
	CString strTemp;
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(nModuleID);
	if(pModuleInfo == FALSE)	
		return FALSE;

	int nS = SENSOR1;
	if(((CButton*)GetDlgItem(IDC_RADIO2))->GetCheck())	
		nS = SENSOR2;

	UpdateMappingCh(nModuleID, nS);		// Mapping file 에 저장

	_MAPPING_DATA *pMapData;
	EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(nModuleID);
	if(lpSysParam != NULL)	
	{
		lpSysParam->bUseTempLimit = m_bUseTempWarning;			

		lpSysParam->wJigTempWarningRefValue		= (unsigned short)ExpFloattoLong((float)m_iJigTempWarningRefValue, EP_ETC_FLOAT);
		lpSysParam->wJigTempErrorRefValue		= (unsigned short)ExpFloattoLong((float)m_iJigTempErrorRefValue, EP_ETC_FLOAT);
		lpSysParam->wPowerTempWarningRefValue	= (unsigned short)ExpFloattoLong((float)m_iPowerTempWarningRefValue, EP_ETC_FLOAT);
		lpSysParam->wPowerTempErrorRefValue		= (unsigned short)ExpFloattoLong((float)m_iPowerTempErrorRefValue, EP_ETC_FLOAT);
		lpSysParam->cJigFanFaultErrorCount		= m_iJigFanFaultErrorCount;
		lpSysParam->cDCFanFaultErrorCount		= m_iDCFanFaultErrorCount;

		// lpSysParam->sCutTemp = (unsigned short)ExpFloattoLong((float)m_sDangerTemp, EP_ETC_FLOAT);		
		lpSysParam->lCutTemp = m_sDangerTemp;
		lpSysParam->wWarningNo1 = (unsigned short)m_nWaringYellow;

		if( m_nJigTargetTemp > 30 )
		{
			m_nJigTargetTemp = 30;
		}
		lpSysParam->nJigTargetTemp = m_nJigTargetTemp;

		if( m_nJigTargetTemp < m_nWaringRed )
		{
			m_nWaringRed = m_nJigTargetTemp;
		}
		lpSysParam->wWarningNo2 = (unsigned short)m_nWaringRed;

		//Send setting to module make network body
		EP_SENSOR_SET_DATA data;
		ZeroMemory(&data, sizeof(EP_SENSOR_SET_DATA));			

		if(pModuleInfo)
		{
			for(int j = 0; j<EP_MAX_SENSOR_CH; j++)
			{
				pMapData = pModuleInfo->GetSensorMap(SENSOR1, j);
				if(pMapData->nChannelNo >= 0)
				{
					data.tempSensorParam.bUseSensorFlag[j] = TRUE;
				}
				pMapData = pModuleInfo->GetSensorMap(SENSOR2, j);
				if(pMapData->nChannelNo >= 0)
				{
					data.gasSensorParam.bUseSensorFlag[j] = TRUE;
				}				
			}
		}
				
		data.tempSensorParam.wJigTempErrorRefValue = lpSysParam->wJigTempErrorRefValue;			//Temperature Warnning
		data.tempSensorParam.lCutLimit = lpSysParam->lCutTemp;							//Temperature CutTemp
		data.tempSensorParam.wPowerTempErrorRefValue = lpSysParam->wPowerTempErrorRefValue;		//Temperature Low Warnning
		data.tempSensorParam.bUseLimit = (unsigned char)lpSysParam->bUseTempLimit;		//use limit data

		data.gasSensorParam.wJigTempErrorRefValue = (unsigned short)lpSysParam->lMaxGas;		//Gas Limit Value Use		
		data.gasSensorParam.bUseLimit = (unsigned char)lpSysParam->bUseGasLimit;		//use limit data

		data.bUseError = (unsigned char)lpSysParam->bUseCutLimit;					//use CutTemp -> 수동모드전환
		data.wWarningNo1 = lpSysParam->wWarningNo1;
		data.wWarningNo2 = lpSysParam->wWarningNo2;
		data.sErrorCumulativeBuffer = lpSysParam->nJigTargetTemp;
		data.wJigTempWarningRefValue = lpSysParam->wJigTempWarningRefValue;
		data.wPowerTempWarningRefValue = lpSysParam->wPowerTempWarningRefValue;
		data.cJigFanFaultErrorCount = lpSysParam->cJigFanFaultErrorCount;
		data.cDCFanFaultErrorCount	= lpSysParam->cDCFanFaultErrorCount;
				
		memcpy(&m_sendSensorSetData, &data, sizeof(EP_SENSOR_SET_DATA));
		if(EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)
		{
			if(EPSendCommand(nModuleID, 1, 0, EP_CMD_SENSOR_LIMIT_SET, &data, sizeof(EP_SENSOR_SET_DATA))!= EP_ACK)
			{				
				strTemp.Format("%s :: Sensor Limit Data Send Fail.", ::GetModuleName(nModuleID));
				m_pDoc->WriteLog( strTemp );
				m_arraySendError.Add(GetModuleName(nModuleID));//20200831ksj
				
				//20200831 ksj
				if(m_lastApplyModuleID == nModuleID)
				{
					int nSize = m_arraySendError.GetCount();
					if(nSize>0)
					{
						CString strMsg = "Failure to apply some modules : ";
						for(int i=0; i<nSize ;i++ )
						{
							strMsg += m_arraySendError.GetAt(i) + ", ";
						}
						MessageBox(strMsg);
					}
					m_pDoc->ReLoadSensorSetting();
					DisplayMapping(nModuleID);

					if(nSize == 0)
					{
						MessageBox("success all");
					}
				}
			}
		}						
	}
		
	//Save configuration to file.
	int nTempID = 0;
	FILE *fp = fopen(AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon") + "\\" + BF_SENSOR_MAPPING_FILE_NAME, "wb" );

	if(fp)
	{
		COleDateTime curT = COleDateTime::GetCurrentTime();
		EP_FILE_HEADER *pHeader= new EP_FILE_HEADER;
		strcpy(pHeader->szFileID, "20061017");
		strcpy(pHeader->szDescrition, "PNE sensor mapping file\n");
		strcpy(pHeader->szFileVersion, "1");
		sprintf(pHeader->szCreateDateTime, "%s", curT.Format());
		fwrite(pHeader, sizeof(EP_FILE_HEADER), 1, fp);
		delete pHeader;
		
		SENSOR_MAPPING_SAVE_TABLE *pData = new SENSOR_MAPPING_SAVE_TABLE;
		for(int i=0; i<m_pDoc->GetInstalledModuleNum(); i++)
		{
			ZeroMemory(pData, sizeof(SENSOR_MAPPING_SAVE_TABLE));
			nTempID = EPGetModuleID(i);
			pData->nModuleID = nTempID;
			
			pModuleInfo = m_pDoc->GetModuleInfo(nTempID);
			if(pModuleInfo)
			{
				for(int m=0; m<EP_MAX_SENSOR_CH; m++)
				{
					pMapData = pModuleInfo->GetSensorMap(SENSOR1, m);
					memcpy(&pData->mapData1[m], pMapData, sizeof(_MAPPING_DATA));
					pMapData = pModuleInfo->GetSensorMap(SENSOR2, m);
					memcpy(&pData->mapData2[m], pMapData, sizeof(_MAPPING_DATA));
				}
				fwrite(pData,  sizeof(SENSOR_MAPPING_SAVE_TABLE), 1, fp);
			}
		}
		delete pData;
		fclose(fp);
	}
	else
		return FALSE;
	return TRUE;
}

void CSensorMapDlg::DisplayMapping(int nModuleID)
{
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(nModuleID);
	
	//Mapping Table display
	CString strType, strTemp, strTemp1;
	int i = 0;
	strTemp = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Module Name", "Rack");	
	strType = "None\n";	
	strType += (strTemp + "\n");	
	strType += "Jig\n";
	for(i = 0; i<EPGetChInGroup(nModuleID); i++)
	{
		strTemp.Format("%d\n", i+1);
		strType += strTemp;
	}
	m_wndMapGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetChoiceList(strType));

	_MAPPING_DATA *pMapData;
	
	int nS = SENSOR1;
	if(((CButton*)GetDlgItem(IDC_RADIO2))->GetCheck())
		nS = SENSOR2; 

	for(i= 0; i<EP_MAX_SENSOR_CH; i++)
	{
		pMapData = pModuleInfo->GetSensorMap(nS, i);
		// strTemp.Format("%d", pMapData->nChannelNo+1);
		strTemp.Format("%d", pMapData->nChannelNo+1);	//ljb 온도 설정 수정 왜 2냐 0:None, 1:Module,2:Jig,3:channel(1 base)
		m_wndMapGrid.SetStyleRange(CGXRange(i+1, 2), CGXStyle().SetValue(strTemp));	
		m_wndMapGrid.SetStyleRange(CGXRange(i+1, 3), CGXStyle().SetValue(pMapData->szName));	
	}

	EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(nModuleID);
	if(lpSysParam != NULL)	
	{
		m_bUseTempWarning = lpSysParam->bUseTempLimit;	
		m_iJigTempWarningRefValue = (unsigned int)ETC_PRECISION(lpSysParam->wJigTempWarningRefValue);
		m_iJigTempErrorRefValue = (unsigned int)ETC_PRECISION(lpSysParam->wJigTempErrorRefValue);
		m_iPowerTempWarningRefValue = (unsigned int)ETC_PRECISION(lpSysParam->wPowerTempWarningRefValue);
		m_iPowerTempErrorRefValue = (unsigned int)ETC_PRECISION(lpSysParam->wPowerTempErrorRefValue);
		m_iJigFanFaultErrorCount = (unsigned int)lpSysParam->cJigFanFaultErrorCount;
		m_iDCFanFaultErrorCount = (unsigned int)lpSysParam->cDCFanFaultErrorCount;

		m_sDangerTemp = (unsigned int)lpSysParam->lCutTemp;
		m_nWaringYellow = lpSysParam->wWarningNo1;
		m_nWaringRed = lpSysParam->wWarningNo2;
		m_nJigTargetTemp = lpSysParam->nJigTargetTemp;		
	}

	UpdateData(FALSE);
}

void CSensorMapDlg::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	UpdateMappingCh(m_nModuleID, SENSOR2);
	DisplayMapping(m_nModuleID);	
}

void CSensorMapDlg::UpdateMappingCh(int nModuleID, int nSensor)
{
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(nModuleID);
	if(pModuleInfo == FALSE)	
		return;

	CString strTemp;
	_MAPPING_DATA *pMapData;
	int nTemp;

	for(DWORD i=0; i<m_wndMapGrid.GetRowCount() && i<EP_MAX_SENSOR_CH; i++)
	{
		pMapData = pModuleInfo->GetSensorMap(nSensor, i);
		// nTemp = atol(m_wndMapGrid.GetValueRowCol(i+1, 2))-1;
		nTemp = atol(m_wndMapGrid.GetValueRowCol(i+1, 2))-1;
		pMapData->nChannelNo = (unsigned short)nTemp;		//-1 : Not Use, 0: Module,  1~ : 1 Base channel no
		sprintf(pMapData->szName, "%s", m_wndMapGrid.GetValueRowCol(i+1, 3));		
	}	
}

void CSensorMapDlg::OnRadio2()
{
	// TODO: Add your control notification handler code here
	UpdateMappingCh(m_nModuleID, SENSOR1);
	DisplayMapping(m_nModuleID);		
}

void CSensorMapDlg::OnBnClickedButtonApply()
{
	if(UpdateData(TRUE))
	{
		m_arraySendError.RemoveAll();
		m_lastApplyModuleID = m_nModuleID;
		if(ApplyToModule(m_nModuleID) == FALSE)
		{
			CString strTemp;
			strTemp.Format(TEXT_LANG[3], ::GetModuleName(m_nModuleID)); //"%s Sensor mapping save fail!!!"
			AfxMessageBox(strTemp);
		}
	}
}

BOOL CSensorMapDlg::OnEraseBkgnd( CDC* pDC )
{
	CRect rc;
	GetClientRect(&rc);

	CBrush newBrush(RGB_PURELIGHTBLUE);
	CBrush* oldBrush = pDC->SelectObject(&newBrush);

	pDC->PatBlt(rc.left, rc.top, rc.Width(), rc.Height(), PATCOPY);

	pDC->SelectObject(oldBrush);

	return TRUE;
}

HBRUSH CSensorMapDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	UINT nID = pWnd->GetDlgCtrlID();

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		pDC->SetBkMode(TRANSPARENT);		
		hbr = (HBRUSH)GetStockObject(NULL_BRUSH);;		
	}

	return hbr;
}