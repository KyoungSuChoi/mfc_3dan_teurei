#include "stdafx.h"
#include "MisWiringPoint.h"

CMiswiringPoint::CMiswiringPoint(void)
{

}

CMiswiringPoint::~CMiswiringPoint(void)
{

}


void CMiswiringPoint::SetVData( WORD wPoint, double dValue)
{
	if( wPoint < 1 )
	{
		m_VMeasureData[wPoint] = dValue;
	}
}

void CMiswiringPoint::SetIData( WORD wPoint, double dValue)
{
	if( wPoint < 3 )
	{
		m_IMeasureData[wPoint] = dValue;
	}
}