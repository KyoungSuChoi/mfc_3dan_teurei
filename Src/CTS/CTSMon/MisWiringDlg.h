#pragma once

#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "MiswiringPoint.h"

#define GRID_COL	8
//#define GRID_COL	7

// CMisWiringDlg 대화 상자입니다.

class CMisWiringDlg : public CDialog
{
	DECLARE_DYNAMIC(CMisWiringDlg)

public:
	CMisWiringDlg(int nModuleID, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMisWiringDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MISWIRING_CHECK_DLG };

	typedef enum {MIS_COL_NO= 1, MIS_COL_USE, MIS_COL_PV, MIS_COL_VOL, MIS_COL_CUR, MIS_COL_DIV, MIS_COL_CASE, MIS_COL_DTAIL};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	CCTSMonDoc* m_pdoc;
	int	m_nModuleID;
	int m_nTotCh;

	int nCh;					//20200224 엄륭  채널 선택 관련 기능 변경함

	CList<LONG, LONG&> m_ChList; //20200224 엄륭  채널 선택 관련 기능 변경함

	CString	m_strChSelect;  //20200224 엄륭  채널 선택 관련 기능 변경함
	int		m_nOptSelectChannel; //20200224 엄륭  채널 선택 관련 기능 변경함
	BOOL	GetSelectdChNum(); //20200224 엄륭  채널 선택 관련 기능 변경함

	CMyGridWnd m_wndGrid;
	bool m_bRun;
	bool* m_bCheckChannel;	//채널별 사용여부 (미사용)

	CMiswiringPoint m_pMisPoint;

	bool m_bReceiveDataFlag; //하트비트

	CComboBox m_comboType;
	void InitGrid();
	void UpdateGrid();

	BOOL SendChDataToSbc();

	void UpdateResultData(EP_MISWIRING_CHECK_RESULT_DATA pCodeData);
	void EndResultData(int nModuleID, int nMode);



	CString *TEXT_CASE;
	bool LanguageinitCase(CString strKey);

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnAllSelect();
	afx_msg void OnBnClickedBtnAllRelease();
	afx_msg LRESULT OnClickedGrid(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModifyGrid(WPARAM wParam, LPARAM lParam);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	BOOL GetSelectedModuleList(CWordArray &awSelModule);
	afx_msg void OnBnClickedBtnRun();
	afx_msg void OnBnClickedBtnStop();
	afx_msg void OnBnClickedButtonPointSet();
	afx_msg void OnBnClickedCancel();

	
	afx_msg void OnBnClickedOptAllChannel();
	afx_msg void OnBnClickedOptPartChannel();
};
