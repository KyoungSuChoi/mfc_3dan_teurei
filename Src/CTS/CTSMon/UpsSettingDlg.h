#if !defined(AFX_UPSSETTINGDLG_H__3D7DF8CB_A1D5_437A_A250_5D891B8B85B0__INCLUDED_)
#define AFX_UPSSETTINGDLG_H__3D7DF8CB_A1D5_437A_A250_5D891B8B85B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UpsSettingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UpsSettingDlg dialog

class UpsSettingDlg : public CDialog
{
// Construction
public:
	UpsSettingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(UpsSettingDlg)
	enum { IDD = IDD_UPS_SETTING_DIALOG };
	CComboBox	m_CtrlUpsSelect;
	BOOL	m_bUseUPS;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UpsSettingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	void LoadSetting();
	void SaveSetting();

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(UpsSettingDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UPSSETTINGDLG_H__3D7DF8CB_A1D5_437A_A250_5D891B8B85B0__INCLUDED_)
