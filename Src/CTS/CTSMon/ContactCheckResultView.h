#pragma once
#include "afxwin.h"

// CContactCheckResultView 폼 뷰입니다.
#include "MyGridWnd.h"
#include "CTSMonDoc.h"

class CContactCheckResultView : public CFormView
{
	DECLARE_DYNCREATE(CContactCheckResultView)

protected:
	CContactCheckResultView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CContactCheckResultView();
	CString *TEXT_LANG;
	bool LanguageinitMonConfig(CString strClassName);

public:
	enum { IDD = IDD_CONTACT_CHECK_RESULT_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	void InitLabel();

	CCTSMonDoc* GetDocument();
	void UpdateGroupState( int nModuleID, int nGroupIndex=0 );	
	void ModuleConnected(int nModuleID);
	void GroupSetChanged(int nColCount = -1);
	void TopConfigChanged(); 
	void InitSmallChGrid();
	void StopMonitoring();
	void StartMonitoring();
	void DrawChannel();
	void SetTopGridFont();

	BOOL GetChRowCol(int nGroupIndex, int nChindex, ROWCOL &nRow, ROWCOL &nCol);
	void SetCurrentModule(int nModuleID, int nGroupIndex = 0);
	CString GetTargetModuleName();

public:
	STR_TOP_CONFIG		*m_pConfig;
	CMyGridWnd	m_wndSmallChGrid;

	int m_nTopGridRowCount;
	int m_nTopGridColCount;
	UINT m_nDisplayTimer;
	int m_nCurModuleID;	
	int m_nCurGroup;
	int m_nCurChannel;
	int m_nCurTrayIndex;

	SHORT	*m_pRowCol2GpCh;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CLabel m_LabelViewName;
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CLabel m_CmdTarget;
};

#ifndef _DEBUG 
inline CCTSMonDoc* CContactCheckResultView::GetDocument()
   { return (CCTSMonDoc*)m_pDocument; }
#endif


