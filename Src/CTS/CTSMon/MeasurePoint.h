#pragma once

#define MEAS_MAX_VOLTAGE_CNT 10
#define MEAS_MAX_CURRENT_CNT 10

#define REG_CAL_SECTION	"Calibration"

class CMeasurePoint
{
public:
	void SetVMeasureCnt(int nCnt = 1, RangeType nType=High_Range)	{	m_nVMeasureCnt[nType] = nCnt; }
	void SetIMeasureCnt(int nCnt = 1, RangeType nType=High_Range)	{	m_nIMeasureCnt[nType] = nCnt; }
	int	GetVMeasureCnt(RangeType nType=High_Range)	{	return  m_nVMeasureCnt[nType];	}
	int	GetIMeasureCnt(RangeType nType=High_Range)	{	return	m_nIMeasureCnt[nType];	}

	void SetVData( WORD wPoint, double dValue, RangeType nType=High_Range );
	void SetIData( WORD wPoint, double dValue, RangeType nType=High_Range );
	double GetVData( WORD wPoint, RangeType nType=High_Range )	{ if( wPoint < MEAS_MAX_VOLTAGE_CNT ) return m_VMeasureData[nType][wPoint]; }
	double GetIData( WORD wPoint, RangeType nType=High_Range )	{ if( wPoint < MEAS_MAX_CURRENT_CNT ) return m_IMeasureData[nType][wPoint]; }

	BOOL SetShuntT( double *pdData );
	BOOL SetShuntR( double *pdData );
	BOOL SetShuntSerial( int nBoardIndexNum, CHAR *pdData );
	double GetShuntT( int nBoardIndexNum );
	double GetShuntR( int nBoardIndexNum );
	CString GetShuntSerial( int nBoardIndexNum );

	CMeasurePoint(void);
	~CMeasurePoint(void);

protected:
	int m_nVMeasureCnt[3];
	int	m_nIMeasureCnt[3];

	double	m_VMeasureData[3][MEAS_MAX_VOLTAGE_CNT];
	double	m_IMeasureData[3][MEAS_MAX_CURRENT_CNT];

	double  m_ShuntT[CAL_MAX_BOARD_NUM];
	double	m_ShuntR[CAL_MAX_BOARD_NUM];
	CHAR	m_ShuntSerial[CAL_MAX_BOARD_NUM][CAL_MAX_SHUNT_SERIAL_SIZE];

};
