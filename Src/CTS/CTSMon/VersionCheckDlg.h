#pragma once

#include "afxwin.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"


#define MAX_FW_HISTORY_LOG		50
#define MAX_SW_HISTORY_LOG		50

#define COLOR_FW_STAGE					RGB(192, 221, 162)
#define COLOR_FW_MAIN_PROTOCAL			RGB(143,  213, 239)
#define COLOR_FW_SUB_PROTOCAL			RGB(242, 209, 183)
#define COLOR_FW_MAIN_REVISION			RGB(232, 249, 143)
#define COLOR_FW_SUB_REVISION			RGB(196, 190, 159)

#define COLOR_FW_STAGE_B				RGB(172, 201, 142)
#define COLOR_FW_MAIN_PROTOCAL_B		RGB(123,  193, 219)
#define COLOR_FW_SUB_PROTOCAL_B			RGB(222, 189, 163)
#define COLOR_FW_MAIN_REVISION_B		RGB(212, 229, 123)
#define COLOR_FW_SUB_REVISION_B			RGB(176, 170, 139)

// CVersionCheckDlg 대화 상자입니다.


class CVersionCheckDlg : public CDialog
{
	DECLARE_DYNAMIC(CVersionCheckDlg)

public:
	CVersionCheckDlg(CWnd* pParent = NULL, CCTSMonDoc* pDoc = NULL);   // 표준 생성자입니다.
	virtual ~CVersionCheckDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VERSION_CHECK };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	CCTSMonDoc* m_pDoc;

	int m_nTotalUnitNum;

	int m_nTap;
	enum{TAP_FW_VERSION, TAP_FW_VERSION_HISTORY, TAP_SW_VERSION_HISTORY};

public:
	CMyGridWnd      m_gridFwVersionDesc;
	CMyGridWnd      m_gridFwVersion;
	CMyGridWnd      m_gridFwVersionHistory;
	CMyGridWnd      m_gridSwVersionHistory;
	CLabel			m_LabelViewName;
	CFont			m_Font;

	BOOL			m_bHiddenData;

	void initCtrl();
	void initGrid();
	void initLabel();
	void initFont();
	void initCombo();

	void UpdateValue();
	void UpdateValue(int nModuleID);
	void UpdateView();
	void UpdateFwHistory();
	void UpdateSwHistory();

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnSbcVersion();
	afx_msg void OnBnClickedBtnVersionHistory();
	afx_msg void OnStnDblclickDlgLabelName();
	afx_msg void OnBnClickedBtnModify();
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnBnClickedBtnRefresh();
	afx_msg void OnBnClickedBtnSwVersionHistory();
};
