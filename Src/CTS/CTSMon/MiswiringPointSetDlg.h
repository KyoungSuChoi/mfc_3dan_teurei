#pragma once

#include "MiswiringPoint.h"
#include "listeditctrl.h"

// CMiswiringPointSetDlg 대화 상자입니다.

class CMiswiringPointSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CMiswiringPointSetDlg)

public:
	CMiswiringPointSetDlg(CMiswiringPoint* pMissPoint, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMiswiringPointSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MISWIRING_POINT_SET_DLG };

	CMiswiringPoint* m_pMissPoint;
	void LoadMeasureData();
	void SaveMeasureData();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListEditCtrl m_wndListPointV;
	CListEditCtrl m_wndListPointI;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg LRESULT OnChangeListEdit(WPARAM wParam, LPARAM lParam);
};
