#include "stdafx.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_LO_MAINT::CFM_ST_LO_MAINT(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_LOCAL(_Eqstid, _stid, _unit)
{
}


CFM_ST_LO_MAINT::~CFM_ST_LO_MAINT(void)
{
}

VOID CFM_ST_LO_MAINT::fnEnter()
{
}

VOID CFM_ST_LO_MAINT::fnProc()
{
}

VOID CFM_ST_LO_MAINT::fnExit()
{
}

VOID CFM_ST_LO_MAINT::fnSBCPorcess(WORD _state)
{
}

VOID CFM_ST_LO_MAINT::fnFMSPorcess(FMS_COMMAND _msgId, CHAR* _recvData)
{
}