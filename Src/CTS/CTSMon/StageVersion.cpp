// StageVersion.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "StageVersion.h"
#include "MainFrm.h"

// CStageVersion 대화 상자입니다.


IMPLEMENT_DYNAMIC(CStageVersion, CDialog)

CStageVersion::CStageVersion(CWnd* pParent /*=NULL*/,CCTSMonDoc* pDoc)
	: CDialog(CStageVersion::IDD, pParent)
{
	m_nCurModuleID = 0;
	m_pDoc = pDoc;
	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();
}

CStageVersion::~CStageVersion()
{
}

void CStageVersion::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_LABEL_SW, m_LabelViewSW);
	DDX_Control(pDX, IDC_LABEL_FW, m_LabelViewFW);
	DDX_Control(pDX, IDC_LABEL_BOARD, m_LabelViewBoard);
	DDX_Control(pDX, IDC_LABEL_INVERTER, m_LabelViewInverter);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);



	DDX_Control(pDX, IDC_COMBO_STAGE_CHANGE, m_comboStageID);
}


BEGIN_MESSAGE_MAP(CStageVersion, CDialog)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_BN_CLICKED(IDC_BTN_REFRESH, &CStageVersion::OnBnClickedBtnRefresh)
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO_STAGE_CHANGE, &CStageVersion::OnCbnSelchangeComboStageChange)
	ON_BN_CLICKED(IDC_BTN_SAVE_EXCEL, &CStageVersion::OnBnClickedBtnSaveExcel)
END_MESSAGE_MAP()


// CStageVersion 메시지 처리기입니다.

BOOL CStageVersion::OnInitDialog()
{
	CDialog::OnInitDialog();

	initCtrl();
	initValue();

	return TRUE;
}

void CStageVersion::initCtrl()
{
	initGrid();
	initLabel();
	initFont();
	initCombo();
}

void CStageVersion::initCombo()
{
	int nModuleID = 1;	

	for(int i=0; i<m_nMaxStageCnt; i++ )
	{
		nModuleID = EPGetModuleID(i);

		m_comboStageID.AddString(::GetModuleName(nModuleID));
		m_comboStageID.SetItemData(i, nModuleID);
	}
}
void CStageVersion::initFont()
{
	LOGFONT LogFont;

	m_comboStageID.GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 35;

	m_Font.CreateFontIndirect( &LogFont );

	m_comboStageID.SetFont(&m_Font);
}


void CStageVersion::initGrid()
{

	/////////////////////////////////////////////////////////////////////////////////F/W 버전

	CString strToolTip;
	m_grid_SW_Version.SubclassDlgItem(IDC_GRID_SW_VERSION, this);
	m_grid_SW_Version.m_bSameRowSize = FALSE;
	m_grid_SW_Version.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_grid_SW_Version.m_bCustomWidth = TRUE;
	m_grid_SW_Version.m_bCustomColor = FALSE;

	m_grid_SW_Version.Initialize();

	m_grid_SW_Version.SetColCount(6);     
	m_grid_SW_Version.SetRowCount(2);        
	m_grid_SW_Version.SetDefaultRowHeight(20);

	m_grid_SW_Version.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_grid_SW_Version.SetScrollBarMode(SB_VERT, gxnDisabled | gxnDisabled);	//Only Vertial Scroll Bar

	m_grid_SW_Version.EnableGridToolTips();
	m_grid_SW_Version.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_grid_SW_Version.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_grid_SW_Version.m_nWidth[1] = 150;
	m_grid_SW_Version.m_nWidth[2] = 150;
	m_grid_SW_Version.m_nWidth[3] = 150;
	m_grid_SW_Version.m_nWidth[4] = 150;
	m_grid_SW_Version.m_nWidth[5] = 150;
	m_grid_SW_Version.m_nWidth[6] = 150;

	m_grid_SW_Version.SetRowHeight(0,0,0);	

	//Row Header Setting
	m_grid_SW_Version.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));

	m_grid_SW_Version.SetStyleRange(CGXRange().SetRows(2), CGXStyle().SetInterior(RGB(240, 240, 240)));
	m_grid_SW_Version.SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_grid_SW_Version.SetStyleRange(CGXRange(2, 1), CGXStyle().SetInterior(RGB(220, 220, 220)));

	m_grid_SW_Version.SetValueRange(CGXRange( 2, 1), "Value");
	m_grid_SW_Version.SetValueRange(CGXRange( 1, 2), "F/W");
	m_grid_SW_Version.SetValueRange(CGXRange( 1, 3), "IT");
	m_grid_SW_Version.SetValueRange(CGXRange( 1, 4), "UI");
	m_grid_SW_Version.SetValueRange(CGXRange( 1, 5), "Debug");
	m_grid_SW_Version.SetValueRange(CGXRange( 1, 6), "Site");
	

	for (int i=0;i<5;i++)   ////툴팁 레지스트리에서 읽어서 관할 정보 써주기
	{
		CString strRegisty;

		strRegisty.Format(_T("Version_SW_Desc_%d"),i+1);
		strToolTip.Format("%s", AfxGetApp()->GetProfileString(VERSION_SECTION, strRegisty, "-")  );
		m_grid_SW_Version.SetStyleRange(CGXRange().SetCols(i+2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));

	}
	m_grid_SW_Version.Redraw();
	//////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////F/W 버전


	m_grid_FW_Version.SubclassDlgItem(IDC_GRID_FW_VERSION, this);
	m_grid_FW_Version.m_bSameRowSize = FALSE;
	m_grid_FW_Version.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_grid_FW_Version.m_bCustomWidth = TRUE;
	m_grid_FW_Version.m_bCustomColor = FALSE;

	m_grid_FW_Version.Initialize();

	m_grid_FW_Version.SetColCount(6);     
	m_grid_FW_Version.SetRowCount(2);        
	m_grid_FW_Version.SetDefaultRowHeight(20);

	m_grid_FW_Version.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_grid_FW_Version.SetScrollBarMode(SB_VERT, gxnDisabled | gxnDisabled);	//Only Vertial Scroll Bar

	m_grid_FW_Version.EnableGridToolTips();
	m_grid_FW_Version.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_grid_FW_Version.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_grid_FW_Version.m_nWidth[1] = 150;
	m_grid_FW_Version.m_nWidth[2] = 150;
	m_grid_FW_Version.m_nWidth[3] = 150;
	m_grid_FW_Version.m_nWidth[4] = 150;
	m_grid_FW_Version.m_nWidth[5] = 150;
	m_grid_FW_Version.m_nWidth[6] = 150;

	m_grid_FW_Version.SetRowHeight(0,0,0);	

	//Row Header Setting
	m_grid_FW_Version.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));

 	m_grid_FW_Version.SetStyleRange(CGXRange().SetRows(2), CGXStyle().SetInterior(RGB(240, 240, 240)));
 	m_grid_FW_Version.SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_grid_FW_Version.SetStyleRange(CGXRange(2, 1), CGXStyle().SetInterior(RGB(220, 220, 220)));

	m_grid_FW_Version.SetValueRange(CGXRange( 2, 1), "Value");
	m_grid_FW_Version.SetValueRange(CGXRange( 1, 2), "MainProtocol");
	m_grid_FW_Version.SetValueRange(CGXRange( 1, 3), "SubProtocol");
	m_grid_FW_Version.SetValueRange(CGXRange( 1, 4), "MainRevision");
	m_grid_FW_Version.SetValueRange(CGXRange( 1, 5), "SubRevision");
	m_grid_FW_Version.SetValueRange(CGXRange( 1, 6), "F/W Bild");

	for (int i=0;i<5;i++)   ////툴팁 레지스트리에서 읽어서 관할 정보 써주기
	{
		CString strRegisty;
		
		strRegisty.Format(_T("Version_FW_Desc_%d"),i+1);
		strToolTip.Format("%s", AfxGetApp()->GetProfileString(VERSION_SECTION, strRegisty, "-")  );
		m_grid_FW_Version.SetStyleRange(CGXRange().SetCols(i+2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));

	}
	m_grid_FW_Version.Redraw();
//////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////Main Board Version

	m_grid_Board_Version.SubclassDlgItem(IDC_GRID_BOARD_VERSION, this);
	m_grid_Board_Version.m_bSameRowSize = FALSE;
	m_grid_Board_Version.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_grid_Board_Version.m_bCustomWidth = TRUE;
	m_grid_Board_Version.m_bCustomColor = FALSE;

	m_grid_Board_Version.Initialize();

	m_grid_Board_Version.SetColCount(2);     
	m_grid_Board_Version.SetRowCount(1);        
	m_grid_Board_Version.SetDefaultRowHeight(20);

	m_grid_Board_Version.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_grid_Board_Version.SetScrollBarMode(SB_VERT, gxnEnabled | gxnDisabled);	//Only Vertial Scroll Bar

	m_grid_Board_Version.EnableGridToolTips();
	m_grid_Board_Version.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_grid_Board_Version.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_grid_Board_Version.m_nWidth[1] = 100;
	m_grid_Board_Version.m_nWidth[2] = 150;


	m_grid_Board_Version.SetRowHeight(0,0,0);	

	//Row Header Setting
	m_grid_Board_Version.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));

	m_grid_Board_Version.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(RGB(240, 240, 240)));
	m_grid_Board_Version.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(RGB(220, 220, 220)));
//	m_grid_Board_Version.SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetInterior(RGB(240, 240, 240)));
	m_grid_Board_Version.SetStyleRange(CGXRange(1, 2), CGXStyle().SetInterior(RGB(220, 220, 220)));

	m_grid_Board_Version.SetValueRange(CGXRange( 1, 2), "Value");

// 	for (int i=0;i<5;i++)   ////툴팁 레지스트리에서 읽어서 관할 정보 써주기
// 	{
// 		CString strRegisty;
// 
// 		strRegisty.Format(_T("Version_Desc_%d"),i+1);
// 		strToolTip.Format("%s", AfxGetApp()->GetProfileString(VERSION_SECTION, strRegisty, "-")  );
// 		m_grid_Board_Version.SetStyleRange(CGXRange().SetCols(i+2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));
// 
// 	}
	m_grid_Board_Version.Redraw();
	/////////////////////////////////////////////////////////////////////////////////Main Board Version

	/////////////////////////////////////////////////////////////////////////////////Inverter Version

	m_grid_Inverter_Version.SubclassDlgItem(IDC_GRID_INVERTER_VERSION, this);
	m_grid_Inverter_Version.m_bSameRowSize = FALSE;
	m_grid_Inverter_Version.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_grid_Inverter_Version.m_bCustomWidth = TRUE;
	m_grid_Inverter_Version.m_bCustomColor = FALSE;

	m_grid_Inverter_Version.Initialize();

	m_grid_Inverter_Version.SetColCount(2);     
	m_grid_Inverter_Version.SetRowCount(1);        
	m_grid_Inverter_Version.SetDefaultRowHeight(20);

	m_grid_Inverter_Version.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_grid_Inverter_Version.SetScrollBarMode(SB_VERT, gxnEnabled | gxnDisabled);	//Only Vertial Scroll Bar

	m_grid_Inverter_Version.EnableGridToolTips();
	m_grid_Inverter_Version.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_grid_Inverter_Version.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_grid_Inverter_Version.m_nWidth[1] = 100;
	m_grid_Inverter_Version.m_nWidth[2] = 150;


	m_grid_Inverter_Version.SetRowHeight(0,0,0);	

	//Row Header Setting
	m_grid_Inverter_Version.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));

	m_grid_Inverter_Version.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(RGB(240, 240, 240)));
	m_grid_Inverter_Version.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(RGB(220, 220, 220)));
	//	m_grid_Board_Version.SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetInterior(RGB(240, 240, 240)));
	m_grid_Inverter_Version.SetStyleRange(CGXRange(1, 2), CGXStyle().SetInterior(RGB(220, 220, 220)));

	m_grid_Inverter_Version.SetValueRange(CGXRange( 1, 2), "Value");

	// 	for (int i=0;i<5;i++)   ////툴팁 레지스트리에서 읽어서 관할 정보 써주기
	// 	{
	// 		CString strRegisty;
	// 
	// 		strRegisty.Format(_T("Version_Desc_%d"),i+1);
	// 		strToolTip.Format("%s", AfxGetApp()->GetProfileString(VERSION_SECTION, strRegisty, "-")  );
	// 		m_grid_Inverter_Version.SetStyleRange(CGXRange().SetCols(i+2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strToolTip)));
	// 
	// 	}
	m_grid_Inverter_Version.Redraw();
	/////////////////////////////////////////////////////////////////////////////////Main Board Version
}


void CStageVersion::initLabel()
{
	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE);

	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("STAGE Version Data Check"); //"Monitoring"

	m_LabelViewSW.SetBkColor(RGB_GRAY);
	m_LabelViewSW.SetTextColor(RGB_WHITE);
	m_LabelViewSW.SetFontSize(24);
	m_LabelViewSW.SetFontBold(TRUE);
	m_LabelViewSW.SetText("S/W Version"); //"Monitoring"

	m_LabelViewFW.SetBkColor(RGB_GRAY);
	m_LabelViewFW.SetTextColor(RGB_WHITE);
	m_LabelViewFW.SetFontSize(24);
	m_LabelViewFW.SetFontBold(TRUE);
	m_LabelViewFW.SetText("F/W Version"); //"Monitoring"

	m_LabelViewBoard.SetBkColor(RGB_GRAY);
	m_LabelViewBoard.SetTextColor(RGB_WHITE);
	m_LabelViewBoard.SetFontSize(24);
	m_LabelViewBoard.SetFontBold(TRUE);
	m_LabelViewBoard.SetText("Board Version"); //"Monitoring"

	m_LabelViewInverter.SetBkColor(RGB_GRAY);
	m_LabelViewInverter.SetTextColor(RGB_WHITE);
	m_LabelViewInverter.SetFontSize(24);
	m_LabelViewInverter.SetFontBold(TRUE);
	m_LabelViewInverter.SetText("Inverter Version"); //"Monitoring"
}


void CStageVersion::initValue()
{

}


void CStageVersion::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		initValue();
	}
}

void CStageVersion::SetCurrentModule(int nModuleID)
{
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());
	m_comboStageID.SetCurSel(nModuleID-1);

	EPJustSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_ALL_VERSION_REQUEST);
}

CString CStageVersion::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}


void CStageVersion::RecvData(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateAllData(nModuleID);
	}	
}
void CStageVersion::UpdateAllData( int nModuleID) //bjy 업데이트
{
	EP_MD_SBC_ALL_VERSION *lpSbcAllVersionData;

	int nModuleIdx = nModuleID - 1;
	WORD gpState = EPGetGroupState(nModuleIdx+1);
	if( gpState != EP_STATE_LINE_OFF )
	{	
		int nBoardCount = 0;		//메인보드 COUNT	
		int nInvertercount=0;		//인버터 COUNT
		lpSbcAllVersionData	= EPGetModuleVersionAllData(nModuleIdx);
		if(lpSbcAllVersionData != NULL)
		{
			CString strTemp =_T("");
			CString strSw_FW(_T("")),strSw_IT(_T("")),strSw_UI(_T("")),strSw_Debug(_T("")),strSw_Site(_T(""));	//s/w 버전
			CString strPro(_T("")),strSubPro(_T("")),strRev(_T("")),strSubRev(_T("")),strFwVerison(_T(""));		//f/w 버전
			strTemp = SW_VERSION;
			int i=0;

			///////////////////////////////////////////s/w version start
			AfxExtractSubString(strSw_FW,	 strTemp, i++, '.');
			strSw_FW = strSw_FW.Right(2);
			AfxExtractSubString(strSw_IT,	 strTemp, i++, '.');
			AfxExtractSubString(strSw_UI,	 strTemp, i++, '.');
			AfxExtractSubString(strSw_Debug, strTemp, i++, '.');
			AfxExtractSubString(strSw_Site,  strTemp, i++, '.');
			
			m_grid_SW_Version.SetValueRange(CGXRange(2,2), strSw_FW);
			m_grid_SW_Version.SetValueRange(CGXRange(2,3), strSw_IT);
			m_grid_SW_Version.SetValueRange(CGXRange(2,4), strSw_UI);
			m_grid_SW_Version.SetValueRange(CGXRange(2,5), strSw_Debug);
			m_grid_SW_Version.SetValueRange(CGXRange(2,6), strSw_Site);
			///////////////////////////////////////////s/w version end
			
			///////////////////////////////////////////f/w version start
			for(int i=0; i<4; i++)
			{
				strPro +=lpSbcAllVersionData->cMainPro[i];
			}
			for(int i=0; i<3; i++)
			{
				strSubPro +=lpSbcAllVersionData->cSubPro[i];
			}
			for(int i=0; i<4; i++)
			{
				strRev +=lpSbcAllVersionData->cMainRev[i];
			}
			for(int i=0; i<3; i++)
			{
				strSubRev +=lpSbcAllVersionData->cSubRev[i];
			}
			strFwVerison.Format("%d",lpSbcAllVersionData->nVersionNo);

			m_grid_FW_Version.SetValueRange(CGXRange(2,2), strPro);
			m_grid_FW_Version.SetValueRange(CGXRange(2,3), strSubPro);
			m_grid_FW_Version.SetValueRange(CGXRange(2,4), strRev);
			m_grid_FW_Version.SetValueRange(CGXRange(2,5), strSubRev);
			m_grid_FW_Version.SetValueRange(CGXRange(2,6), strFwVerison);
			///////////////////////////////////////////f/w version end

			///////////////////////////////////////////Board version start
			
			nBoardCount = lpSbcAllVersionData->nBoardCount;
			//////// 그리드 row COUTN SET START
			m_grid_Board_Version.SetRowCount(nBoardCount+1); 

			for(int i=0; i < nBoardCount; i++)
			{
				CString str;
				str.Format(_T("Board : %d"),i+1);
				m_grid_Board_Version.SetValueRange(CGXRange( i+2,1), str);
			}
			//////// 그리드 row COUTN SET end
			
			for(int i=0; i<nBoardCount; i++)
			{
				strTemp= _T("");
				for(int j=0; j<EP_BOARD_LENGTH; j++)
				{

					strTemp += lpSbcAllVersionData->cVersionMain[i][j];
				}
				strTemp.Trim();
				m_grid_Board_Version.SetValueRange(CGXRange(i+2,2), strTemp);
			}
			///////////////////////////////////////////Board version END
			///////////////////////////////////////////Inverter version start
		
			nInvertercount =  lpSbcAllVersionData->nInvCount;
			//////// 그리드 row COUTN SET START
			m_grid_Inverter_Version.SetRowCount(nInvertercount+1); 
			for(int i=0;i < nInvertercount;i++)
			{
				CString str;
				str.Format(_T("Inverter : %d"),i+1);
				m_grid_Inverter_Version.SetValueRange(CGXRange( i+2,1), str);
			}
			//////// 그리드 row COUTN SET END

			for(int i=0; i<nInvertercount; i++)
			{
				strTemp= _T("");
				for(int j=0; j<EP_INV_LENGTH; j++)
				{
					strTemp += lpSbcAllVersionData->cVersionInv[i][j];
				}
				strTemp.Trim();
				m_grid_Inverter_Version.SetValueRange(CGXRange(i+2,2), strTemp);
			}
			///////////////////////////////////////////Inverter version END
		}
// 		else
// 		{
// 			///////////////////////////////////////////s/w version start
// 			m_grid_SW_Version.SetValueRange(CGXRange(2,2), "-");
// 			m_grid_SW_Version.SetValueRange(CGXRange(2,3), "-");
// 			m_grid_SW_Version.SetValueRange(CGXRange(2,4), "-");
// 			m_grid_SW_Version.SetValueRange(CGXRange(2,5), "-");
// 			m_grid_SW_Version.SetValueRange(CGXRange(2,6), "-");
// 			///////////////////////////////////////////s/w version END
// 			//////////////////////////////////////////////////////////f/w version start
// 			m_grid_FW_Version.SetValueRange(CGXRange(2,2), "-");
// 			m_grid_FW_Version.SetValueRange(CGXRange(2,3), "-");
// 			m_grid_FW_Version.SetValueRange(CGXRange(2,4), "-");
// 			m_grid_FW_Version.SetValueRange(CGXRange(2,5), "-");
// 			m_grid_FW_Version.SetValueRange(CGXRange(2,6), "-");
// 			//////////////////////////////////////////////////////////f/w version end
// 			///////////////////////////////////////////Board version start
// 			for(int i=0; i<nBoardCount; i++)
// 			{
// 				m_grid_Board_Version.SetValueRange(CGXRange(i+2,2), "-");
// 			}
// 			///////////////////////////////////////////Board version END
// 			///////////////////////////////////////////Inverter version start
// 			for(int i=0; i<nInvertercount; i++)
// 			{
// 				m_grid_Inverter_Version.SetValueRange(CGXRange(i+2,2), "-");
// 			}
// 			///////////////////////////////////////////Inverter version END
// 		}
	
	}


}


void CStageVersion::OnBnClickedBtnRefresh()
{
	WORD state = EPGetGroupState(m_nCurModuleID);
	initValue();		

	if( state == EP_STATE_LINE_OFF )
	{
		AfxMessageBox("STAGE Disconnect");
		return;
	}

	int nRtn = 0;

	EPJustSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_ALL_VERSION_REQUEST);


}


void CStageVersion::OnEditCopy() 
{
	ROWCOL		nRow, nCol;
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();

	if(&m_grid_FW_Version == (CMyGridWnd *)pWnd)
	{
		m_grid_FW_Version.Copy();
	}
}




void CStageVersion::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(::IsWindow(this->GetSafeHwnd()))
	{

		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		int width = rect.right/100;

		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
	}
}

void CStageVersion::OnCbnSelchangeComboStageChange()
{

	initValue();
	int nIndex = m_comboStageID.GetCurSel();
	if(nIndex >= 0)
	{
		m_nCurModuleID = nIndex+1;
		m_CmdTarget.SetText(GetTargetModuleName());
		OnBnClickedBtnRefresh();
	}
}


void CStageVersion::OnBnClickedBtnSaveExcel()
{

	WORD state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		initValue();
		AfxMessageBox("Save fail !!");
		return;
	}

	EP_MD_SBC_ALL_VERSION *lpSbcALLVersionData;
	lpSbcALLVersionData = EPGetModuleVersionAllData(EPGetModuleIndex(m_nCurModuleID));

	CStdioFile csvFile;			
	CFileException ex;
	CString strTemp;
	CString strName;
	CString strKey;
	CString strDefault;

	CFileDialog fileDlg(FALSE,NULL,NULL, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"csv File(*.csv) | *.csv; *.csv");

	if(fileDlg.DoModal() == IDOK)
	{ 
		strTemp=fileDlg.GetPathName();  
		CString strExt=fileDlg.GetFileExt();

		if(strExt.IsEmpty())
		{
			strTemp += ".csv";
		}
	}
	else
	{
		return;
	}





	if(csvFile.Open( (LPSTR)(LPCTSTR)strTemp, CFile::modeWrite | CFile::modeNoTruncate | CFile::modeCreate, &ex ))
	{
		COleDateTime oleTime = COleDateTime::GetCurrentTime();
		csvFile.WriteString(oleTime.Format("%Y/%m/%d %H:%M:%S"));
		csvFile.WriteString(", , Version Data Check\n");

		strTemp = GetModuleName(m_nCurModuleID);	
		csvFile.WriteString(strTemp+"\n");


		csvFile.WriteString(", , ※S/W Version※\n"); 
		csvFile.WriteString(", , F/W, IT , UI , Debug , Site \n");
		strTemp.Format(_T(", , %s , %s , %s , %s , %s \n"), 
			CString(m_grid_SW_Version.GetValueRowCol(2, 2)),CString(m_grid_SW_Version.GetValueRowCol(2, 3)),CString(m_grid_SW_Version.GetValueRowCol(2, 4)),CString(m_grid_SW_Version.GetValueRowCol(2, 5)),CString(m_grid_SW_Version.GetValueRowCol(2, 6)));
		csvFile.WriteString(strTemp);
		csvFile.WriteString("\n");
		csvFile.WriteString(", , ※F/W Version※\n");
		csvFile.WriteString(", ,MainProtocol,SubProtocol,MainRevision,SubReviseion,F/W Blid \n");
		strTemp.Format(_T(", , %s , %s , %s , %s , %s \n"), 
			CString(m_grid_FW_Version.GetValueRowCol(2, 2)),CString(m_grid_FW_Version.GetValueRowCol(2, 3)),CString(m_grid_FW_Version.GetValueRowCol(2, 4)),CString(m_grid_FW_Version.GetValueRowCol(2, 5)),CString(m_grid_FW_Version.GetValueRowCol(2, 6)));
		csvFile.WriteString(strTemp);
		csvFile.WriteString("\n");
		csvFile.WriteString("\n");

		csvFile.WriteString(", , ※Board Version※, , , ,※Inverter Version※\n");

		/////////////////////////////////////////////////////////////메인보드 갯수와 인버터 갯수중 무엇이 더 큰지 확인 start
		int	 ncount = 0;
		ncount = lpSbcALLVersionData->nBoardCount;
		if (ncount < lpSbcALLVersionData->nInvCount)
		{
			ncount =lpSbcALLVersionData->nInvCount;
		}
		/////////////////////////////////////////////////////////////메인보드 갯수와 인버터 갯수중 무엇이 더 큰지 확인 END

		for(int i =0; i < ncount; i++)// 보드 갯수와 인버터 갯수에 따라서 저장
		{
			if( i >= lpSbcALLVersionData->nBoardCount)
			{
				strTemp.Format(_T(",,, , ,Inverter: %d, %s\n"),i+1,CString(m_grid_Inverter_Version.GetValueRowCol(i+2,2)));
				csvFile.WriteString(strTemp);
			}
			else if (i >= lpSbcALLVersionData->nInvCount)
			{
				strTemp.Format(_T(",Board : %d, %s\n"),i+1,CString(m_grid_Board_Version.GetValueRowCol(i+2,2)));
				csvFile.WriteString(strTemp);
			}
			else
			{
				strTemp.Format(_T(",Board : %d, %s, , ,Inverter: %d, %s\n"),i+1,CString(m_grid_Board_Version.GetValueRowCol(i+2,2)),i+1,CString(m_grid_Inverter_Version.GetValueRowCol(i+2,2)));
				csvFile.WriteString(strTemp);
			}
		}


		csvFile.Close();
		AfxMessageBox("Save succeed");
	}
	else
	{
		AfxMessageBox("Save fail !!");
		return ;
	}


}
