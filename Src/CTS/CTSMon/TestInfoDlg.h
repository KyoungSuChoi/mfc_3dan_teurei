#if !defined(AFX_TESTINFODLG_H__19ED532A_0641_44C2_809D_38C60EA564F8__INCLUDED_)
#define AFX_TESTINFODLG_H__19ED532A_0641_44C2_809D_38C60EA564F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestInfoDlg dialog
class CTestInfoDlg : public CDialog
{
// Construction
public:
	virtual ~CTestInfoDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
	void UpdateInformation();
	STR_CONDITION_HEADER m_curTestHeader;
	STR_CONDITION_HEADER m_nextTestHeader;
	STR_CONDITION_HEADER	m_testHeader;
	int nModuleID;
	int nGroupIndex;
	int m_nType;
	CTray		*m_pTestedTrayData;
//	CTray		m_NextTestData;
	CCTSMonDoc	*m_pDoc;
	CTestInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTestInfoDlg)
	enum { IDD = IDD_PROC_INFO_DLG };
	CLabel	m_ctrlNextTestDescrip;
	CLabel	m_ctrlTitle;
	CLabel	m_ctrlNextTestName;
	CLabel	m_ctrlTestedCellState;
	CLabel	m_ctrlTestedDate;
	CLabel	m_ctrlTestedName;
	CLabel	m_ctrlCellNo;
	CLabel	m_ctrlLot;
	BOOL	m_bNotShowTestInfoDlg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTestInfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnOk();
	afx_msg void OnAbort();
	virtual void OnCancel();
	afx_msg void OnPrevProc();
	afx_msg void OnNextProc();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTINFODLG_H__19ED532A_0641_44C2_809D_38C60EA564F8__INCLUDED_)
