#if !defined(AFX_PARAMSETDLG_H__73FEDDEC_1666_4E6E_AC1F_0DD1C3278D61__INCLUDED_)
#define AFX_PARAMSETDLG_H__73FEDDEC_1666_4E6E_AC1F_0DD1C3278D61__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParamSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParamSetDlg dialog

class CParamSetDlg : public CDialog
{
// Construction
public:
	virtual ~CParamSetDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

	BOOL SendConfigDataToModule(int nModuleID);
	void UpdateText();
	int m_nSelModuleID;
	void EnableAllControl();
	void UpdateConfigData(EP_MD_CONFIG_DATA &configData);
	void DisableAllControl();
	CParamSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CParamSetDlg)
	enum { IDD = IDD_PARAM_SET_DIALOG };
	CComboBox	m_ctrlModuleList;
	BOOL	m_bVAutoCal;
	BOOL	m_bIAutoCal;
	UINT	m_nHuttingCount;
	UINT	m_nVhuttingLevel;
	UINT	m_nIHuttingLevel;
	UINT	m_nStableTime;
	UINT	m_nDeltaStableTime;
	float	m_fVRefErrLevel;
	float	m_fIRefErrLevel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParamSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParamSetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeModuleSelCombo();
	afx_msg void OnChange5vErrLevelEdit();
	afx_msg void OnChange2aErrLevelEdit();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMSETDLG_H__73FEDDEC_1666_4E6E_AC1F_0DD1C3278D61__INCLUDED_)
