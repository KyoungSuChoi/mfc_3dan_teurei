// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CTSMonDoc.h"
#include "ProcedureSelDlg.h"

#include "MainFrm.h"

#include "GroupSetDlg.h"
#include "TopMonitoringConfigDlg.h"
#include "ChangeUserInfoDlg.h"
#include "UserAdminDlg.h"
#include "ModuleSetDlg.h"
#include "RegTrayDlg.h"
#include "SerialConfigDlg.h"

//#include "TestLotInputDlg.h"

#include "CalFileDlg.h"
#include "ModuleAddDlg.h"
#include "JigIDRegDlg.h"
//#include "TextInputDlg.h"

#include "LoginDlg.h"
#include "FailMsgDlg.h"
#include "ParamSetDlg.h"
#include "JigIDInputDlg.h"
#include "TrayInfoDlg.h"
#include "SelEmgLogDlg.h"
//#include "Mmsystem.h"
#include "SelUnitDlg.h"
#include "UnitComLogDlg.h"
#include "UpsSettingDlg.h"
//#include "ShowEmgDlg.h"
#include <TlHelp32.h>
#include "ViewSet.h"
#include "PasswordChkDlg.h"

#include "./Global/Mysql/PneApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern STR_LOGIN g_LoginData;
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

DWORD WINAPI SBCPROCThreadCallback(LPVOID parameter)
{
	CMainFrame *Owner = (CMainFrame*) parameter;
	Owner->SBCPROCThreadCallback();

	return 0;
}

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_CONEDITOR_RUN, OnConeditorRun)
	ON_COMMAND(ID_LOGDLG_CFG, OnLogdlgCfg)
	ON_COMMAND(ID_BOARD_GROUP_SET, OnBoardGroupSet)
	ON_COMMAND(ID_CFG_TOP_CONFIG, OnCfgTopConfig)
	ON_COMMAND(ID_CHANGE_USER, OnChangeUser)
	ON_COMMAND(ID_USER_SETTING, OnUserSetting)
	ON_COMMAND(ID_ADMINISTRATION, OnAdministration)
	ON_COMMAND(ID_MODULE_SETTING, OnModuleSetting)
	ON_WM_CLOSE()
	ON_UPDATE_COMMAND_UI(ID_MODULE_SETTING, OnUpdateModuleSetting)
	ON_UPDATE_COMMAND_UI(ID_ADMINISTRATION, OnUpdateAdministration)
	ON_WM_TIMER()
	ON_COMMAND(ID_CONNECT_DBSVR, OnConnectDbsvr)
	ON_COMMAND(ID_DISCONNECT_DBSVR, OnDisconnectDbsvr)
	ON_UPDATE_COMMAND_UI(ID_DISCONNECT_DBSVR, OnUpdateDisconnectDbsvr)
	ON_UPDATE_COMMAND_UI(ID_CONNECT_DBSVR, OnUpdateConnectDbsvr)
	ON_COMMAND(ID_TRAY_REG, OnTrayReg)
	ON_UPDATE_COMMAND_UI(ID_TRAY_REG, OnUpdateTrayReg)
	ON_COMMAND(ID_SENSOR_DATA_VIEW, OnSensorDataView)
	ON_COMMAND(ID_FAN_MODE_SETTING, OnFanModeSetting)
	ON_COMMAND(ID_SERIAL_CONFIG, OnSerialConfig)
	ON_UPDATE_COMMAND_UI(ID_SERIAL_CONFIG, OnUpdateSerialConfig)
	ON_COMMAND(ID_IO_TEST, OnIoTest)
	ON_COMMAND(ID_ACCURACY_TEST, OnAccuracyTest)
	ON_COMMAND(ID_CALIBRATION_SET, OnCalibrationSet)
	ON_COMMAND(ID_REF_AD_DATA, OnRefAdData)
	ON_UPDATE_COMMAND_UI(ID_REF_AD_DATA, OnUpdateRefAdData)
	ON_UPDATE_COMMAND_UI(ID_CALIBRATION_SET, OnUpdateCalibrationSet)
	ON_COMMAND(ID_CAL_TYPE_SELECT, OnCalTypeSelect)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_COMMAND(ID_TRAY_SERIAL_INIT, OnTraySerialInit)
	ON_UPDATE_COMMAND_UI(ID_TRAY_SERIAL_INIT, OnUpdateTraySerialInit)
	ON_UPDATE_COMMAND_UI(ID_IO_TEST, OnUpdateIoTest)
	ON_COMMAND(ID_JIG_ADD, OnJigAdd)
	ON_COMMAND(ID_JIG_DELETE, OnJigDelete)
	ON_UPDATE_COMMAND_UI(ID_USER_SETTING, OnUpdateUserSetting)
	ON_UPDATE_COMMAND_UI(ID_SENSOR_DATA_VIEW, OnUpdateSensorDataView)
	ON_COMMAND(ID_VIEW_EMG_LOG, OnViewEmgLog)
	ON_COMMAND(ID_UNIT_LOG, OnUnitLog)
	ON_COMMAND(ID_TENLET_COM, OnTenletCom)
	ON_COMMAND(ID_NET_CHECK, OnNetCheck)
	ON_COMMAND(ID_MODULE_BACKUP, OnModuleBackup)
	ON_COMMAND(ID_BFCALI, OnBfcali)
	ON_UPDATE_COMMAND_UI(ID_BFCALI, OnUpdateBfcali)
	ON_UPDATE_COMMAND_UI(ID_ACCURACY_TEST, OnUpdateAccuracyTest)
	ON_COMMAND(ID_UPS_CONFIG, OnUpsConfig)
	//}}AFX_MSG_MAP
	ON_MESSAGE(EPWM_MODULE_CHANGE_INFO, OnModuleInfoChange) //모듈상태 변경
	ON_MESSAGE(EPWM_MODULE_STATE_CHANGE, OnModuleStateChange)
	ON_MESSAGE(EPWM_MODULE_CONNECTED, OnModuleConnected)
	ON_MESSAGE(EPWM_MODULE_CLOSED, OnModuleDisConnected)
	ON_MESSAGE(EPWM_STEP_ENDDATA_RECEIVE, OnSaveDataReceive)
	ON_MESSAGE(EPWM_MODULE_ONLINE_STEPLIST, OnOnlineStepDataReceive)
	
	ON_MESSAGE(EPWM_DB_SYS_CONNECTED, OnDBServerConnected)
	ON_MESSAGE(EPWM_DB_SYS_DISCONNECTED, OnDBServerDisConnected)
	
	ON_MESSAGE(EPWM_CHECK_RESULT, OnCheckEnded)
	ON_MESSAGE(EPWM_NVRAM_HEADER, OnBarCodeReceive)
	ON_MESSAGE(WM_BCR_READED, OnTraySerialReceive)
	ON_MESSAGE(EPWM_MODULE_EMG, OnModuleEmg)

	ON_MESSAGE(EPWM_FMS_ERROR_INFO, OnFmsErrorInfo )

	ON_MESSAGE(EPWM_USER_CMD, OnUserCmdReceive)
	ON_MESSAGE(EPWM_REALTIME_DATA_RECEIVED, OnRealTimeDataReceive)
	
	ON_MESSAGE(WM_COMM_RXCHAR, OnCommunication)	
//	ON_MESSAGE(TCM_TABSEL, OnTabSelected)
	ON_MESSAGE(EPWM_CAL_DATA_RECEIVED, OnCalResultReceive)
	ON_MESSAGE(EPWM_CAL_END_RECEIVED, OnCalEndReceive)
	ON_MESSAGE(EPWM_RM_END_RECEIVED, OnRealMeasEndReceive)
	ON_MESSAGE(EPWM_RM_WORK_END_RECEIVED, OnRealMeasWorkEndReceive)		//ljb 2011519 
	ON_MESSAGE(EPWM_FMS_CONNECTED, OnFmsConnected)
	ON_MESSAGE(EPWM_FMS_CLOSED, OnFmsClosed)

	ON_COMMAND(ID_SYSTEM_SETTING, &CMainFrame::OnSystemSetting)
	ON_COMMAND(ID_PRECISION, &CMainFrame::OnPrecision)
	ON_COMMAND(ID_TEST_DLG, &CMainFrame::OnTestDlg)

	//20200130 KSJ// MISSWIRING
	ON_MESSAGE(EPWM_MISWIRING_DATA_RECV, OnMiswiringDataReceive)
	ON_MESSAGE(EPWM_MISWIRING_END_RECV, OnMiswiringEndReceive)
	ON_COMMAND(ID_MISWIRING_CHECK, OnMiswiringCheck)
	//ON_COMMAND(ID_MISWIRING_CHECK, &CMainFrame::OnMiswiringCheck)

	ON_MESSAGE(UWM_CHANGE_STATE, OnStateChange)
	ON_COMMAND(ID_SBC_LOGCHK, &CMainFrame::OnSbcLogchk)	
	ON_COMMAND(ID_CLAMP_CNTCHK, &CMainFrame::OnClampCntchk)
	ON_COMMAND(ID_VIEW_SET, &CMainFrame::OnViewSet)
	ON_COMMAND(ID_CALIBRATION_UPDATE_INFO, &CMainFrame::OnCalibrationUpdateInfo)
	ON_COMMAND(ID_SBC_SETTING_CHECK, &CMainFrame::OnSbcSettingCheck)
	ON_COMMAND(ID_SBC_CALI_DATA_CHECK, &CMainFrame::OnSbcCaliDataCheck)
	ON_COMMAND(ID_VERISON_CHECK, &CMainFrame::OnVersionCheck)
	ON_COMMAND(ID_STAGE_VERISON, &CMainFrame::OnStageVersion)
	ON_COMMAND(ID_WRITE_ERCD, &CMainFrame::OnWriteErcdCode)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_UPS_RUN_STATE,
	ID_DB_CONNECT_STATE,
	ID_COM_PORT_STATE,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	LanguageinitMonConfig(_T("CMainFrame"));
	// TODO: add member initialization code here
	m_pAllSystemView= NULL;
	m_pTopView		= NULL;			// 전체 Module & Channel Monitoring
//	m_pConditionView= NULL;
//	m_pResultView= NULL;

	m_bDBSvrConnect = FALSE;
	m_bSerialConnected = FALSE;

	m_SerialConfig.nPortNum = 1;
	m_SerialConfig.nPortBaudRate = 9600;
	m_SerialConfig.nPortDataBits = 8;
	m_SerialConfig.nPortStopBits = 1;
	m_SerialConfig.nPortBuffer = 512;
	m_SerialConfig.portParity = 'N';
	m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

//	m_bWorking = FALSE;
//	m_nCurTabIndex = 0;

	m_pAccurayDlg = NULL;
	m_pSensorDlg = NULL;
//	m_pCalDlg = NULL;

	m_bSplitterCreated = FALSE;
	m_pTab = NULL;

	m_nTimerArguModuleID = 0;

	m_pTrayInputDlg = NULL;
	
	m_nUpsDetectCnt = 0;
	m_iUPSFactory = 0;
	m_uSelect = 0;

	mSBCPROCThreadHandle = NULL;
	mSBCPROCThreadDestroyEvent = NULL;
	m_pTestDlg = NULL;
	m_pPrecisionDlg = NULL;
	m_pSbcLogChkDlg = NULL;
	m_pClampCountChkDlg = NULL;
	m_pSystemSettingdlg = NULL;
	m_bAlarmPlaying = FALSE;
	m_pCalibrationUpdateTimeDlg = NULL;
	m_pSettingCheckDlg = NULL;
	m_pSbcCaliDataDlg = NULL;
	m_pVersionCheckDlg	= NULL;
	m_pStageVersion		= NULL; //2021-03-15 BJY 각 STAGE 별 버전 정보 추가
	m_pErcdWriteDlg				= NULL; //2021-04-06 강신중 D ERCD 코드 복사 

	m_bUpsErrorChk = FALSE;

	ZeroMemory( nModuleReadyToStart, sizeof(nModuleReadyToStart));
	m_pCaliDlg = NULL;
	m_pMissDlg = NULL; //2020-02-20 엄륭 오배선 관련 
	//m_TempErrorOn = FALSE; //20200411 엄륭 화재감지 관련
	//m_SmokeErrorOn = FALSE;//20200411 엄륭 화재감지 관련
	//m_ErrDlgVislble = FALSE;//20200411 엄륭 화재감지 관련
	m_pMiswringUnwiringDlg = NULL;
	m_pFanModeSettingDlg = NULL;
}

void CMainFrame::ClearFrmDlg()
{
	int i = 0;
	int nSize = 0;


////////////////////////////////////////////

	nSize = m_apMisWiringDlg.GetSize();
	if(  nSize > 0 )
	{
		CMisWiringDlg* apMisWiringDlg;
		for( i=0; i<nSize; i++ )
		{
			apMisWiringDlg = (CMisWiringDlg *)m_apMisWiringDlg[i];

			delete apMisWiringDlg;
			apMisWiringDlg = NULL;	
		}	
		m_apMisWiringDlg.RemoveAll();
	}

////////////////////////////////////////////




	
	nSize = m_apCaliDlg.GetSize();
	if(  nSize > 0 )
	{
		CCalibratorDlg* pCalibDlg;
		for( i=0; i<nSize; i++ )
		{
			pCalibDlg = (CCalibratorDlg *)m_apCaliDlg[i];

			delete pCalibDlg;
			pCalibDlg = NULL;	
		}	
		m_apCaliDlg.RemoveAll();
	}	
	
	nSize = m_apEmgDlg.GetSize();
	if( nSize > 0 )
	{
		ShowEmgDlg* pEmpgDlg;
		for( i=0; i<nSize; i++ )
		{
			pEmpgDlg = (ShowEmgDlg *)m_apEmgDlg[i];
			delete pEmpgDlg;
			pEmpgDlg = NULL;	
		}
		m_apEmgDlg.RemoveAll();	
	}

	nSize = m_apHSMSEmgDlg.GetSize();
	if( nSize > 0 )
	{
		ShowEmgDlg* pEmpgDlg;
		for( i=0; i<nSize; i++ )
		{
			pEmpgDlg = (ShowEmgDlg *)m_apHSMSEmgDlg[i];
			delete pEmpgDlg;
			pEmpgDlg = NULL;	
		}
		m_apHSMSEmgDlg.RemoveAll();	
	}

	
	nSize = m_apContactCheckResultDlg.GetSize();
	if( nSize > 0 )
	{	
		ContactCheckResultDlg* pContactCheckResultDlg;
		for( i=0; i<nSize; i++ )
		{
			pContactCheckResultDlg = (ContactCheckResultDlg *)m_apContactCheckResultDlg[i];
			delete pContactCheckResultDlg;
			pContactCheckResultDlg = NULL;
		}    
		m_apContactCheckResultDlg.RemoveAll();	
	}
	
	nSize = m_apInformationDlg.GetSize();
	if( nSize > 0 )
	{
		CInformationDlg* pInformationDlg;
		for( i=0; i<nSize; i++ )
		{
			pInformationDlg = (CInformationDlg *)m_apInformationDlg[i];
			delete pInformationDlg;
			pInformationDlg = NULL;
		}    
		m_apInformationDlg.RemoveAll();	
	
	}

	nSize = m_apMisWiringDlg.GetSize();
	if( nSize > 0 )
	{
		CMisWiringDlg* pMisWiringDlg;
		for( i=0; i<nSize; i++ )
		{
			pMisWiringDlg = (CMisWiringDlg *)m_apMisWiringDlg[i];
			delete pMisWiringDlg;
			pMisWiringDlg = NULL;
		}    
		m_apMisWiringDlg.RemoveAll();	
	}

	
	
	if(m_pAccurayDlg != NULL)
	{
		delete m_pAccurayDlg;
		m_pAccurayDlg = NULL;
	}

	if(m_pSensorDlg != NULL)
	{
		delete m_pSensorDlg;
		m_pSensorDlg = NULL;
	}

	if( m_pClampCountChkDlg )
	{
		m_pClampCountChkDlg->DestroyWindow();
		delete m_pClampCountChkDlg;
	}

	if(m_pTrayInputDlg)
	{
		m_pTrayInputDlg->DestroyWindow();
		delete m_pTrayInputDlg;
	}
	
	if( m_pPrecisionDlg )
	{
		m_pPrecisionDlg->DestroyWindow();
		delete m_pPrecisionDlg;
	}
	
	if( m_pSbcLogChkDlg )
	{
		m_pSbcLogChkDlg->DestroyWindow();
		delete m_pSbcLogChkDlg;
	}

	if( m_pCalibrationUpdateTimeDlg )
	{
		m_pCalibrationUpdateTimeDlg->DestroyWindow();
		delete m_pCalibrationUpdateTimeDlg;
	}
	
	if( m_pSystemSettingdlg )
	{
		m_pSystemSettingdlg->DestroyWindow();
		delete m_pSystemSettingdlg;
	}
}

CMainFrame::~CMainFrame()
{	
	if (mSBCPROCThreadDestroyEvent && mSBCPROCThreadHandle)
	{
		SetEvent(mSBCPROCThreadDestroyEvent);

		WaitForSingleObject(mSBCPROCThreadHandle, INFINITE);

		CloseHandle(mSBCPROCThreadDestroyEvent);
		CloseHandle(mSBCPROCThreadHandle);
	}
	
// 	if(m_pCalDlg != NULL)
// 	{
// 		delete m_pCalDlg;
// 		m_pCalDlg = NULL;
// 	}
// 	if(m_pLogViewDlg)
// 	{
// 		delete m_pLogViewDlg;
// 		m_pLogViewDlg = NULL;
// 	}
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}

// 20201203 KSCHOI Delete SBCCaliDataDlg At Destroyer START
	if(m_pSbcCaliDataDlg != NULL)
		delete m_pSbcCaliDataDlg;

	if(m_pSettingCheckDlg != NULL)
		delete m_pSettingCheckDlg;
// 20201203 KSCHOI Delete SBCCaliDataDlg At Destroyer END
	if(m_pVersionCheckDlg != NULL)
		delete m_pVersionCheckDlg;


// 2021-03-15 BJY STAGE 별 버전 정보 At Destroyer START
	if(m_pStageVersion != NULL)
		delete m_pStageVersion;
// 2021-03-15 BJY STAGE 별 버전 정보 At Destroyer END

// 2021-04-06 BJY -> 강신중 D ERCD 코드복사 At Destroyer START
	if(m_pErcdWriteDlg != NULL)
		delete m_pErcdWriteDlg;
// 2021-04-06 BJY -> 강신중 D ERCD 코드복사 At Destroyer END
}


bool CMainFrame::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
			else
			{
				TEXT_LANG[i].Replace("\\d","\d");
				TEXT_LANG[i].Replace("\\s","\s");
				TEXT_LANG[i].Replace("\\n","\n");
				TEXT_LANG[i].Replace("\\N","\N");
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
// 	if (!m_wndReBar.Create(this, RBS_AUTOSIZE))
// 	{
// 		TRACE0("Failed to create rebar\n");
// 		return -1;      // fail to create
// 	}
	/*	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		AfxMessageBox("Failed to create toolbar");
		return -1;      // fail to create
	}
	*/

	/*
#ifdef USE_STATUS_BAR
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		CString str;
		str.Format("%d/%d", sizeof(indicators), sizeof(UINT));
		AfxMessageBox(str);
		return -1;      // fail to create
	}	

	int index = m_wndStatusBar.CommandToIndex(ID_UPS_RUN_STATE);
	m_wndStatusBar.SetPaneInfo(index, ID_UPS_RUN_STATE, SBPS_NORMAL, 70);
#endif
	*/

	// create the animation control
// 	m_wndAnimate.Create(WS_CHILD | WS_VISIBLE, CRect(0, 0, 10, 10), this, AFX_IDW_TOOLBAR + 2);
// 	m_wndAnimate.Open(IDR_AVI);
// 	m_wndReBar.AddBar(&m_wndToolBar);
// 	m_wndReBar.AddBar(&m_wndAnimate, NULL, NULL, RBBS_FIXEDSIZE | RBBS_FIXEDBMP);
// 	m_wndAnimate.Play(0, -1, -1);

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	
	/*
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	*/

//	BOOL ret = GetPrivateProfileStruct("CONFIG", "TOPCONFIG", 
//						&m_TopConfig, sizeof(STR_TOP_CONFIG), INI_FILE);

	if(LoadSerialSetting() == FALSE)
	{
		AfxMessageBox("Serial Setting Load Fail");
		return -1;
	}
	//Check Bar Code Reader
	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Bar Code Reader", FALSE))
	{
		InitSerialPort();
	}	

	OnConnectDbsvr();

/*	int data;
	if((data = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataSave", 0)) == TRUE)
	{
//		CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
//		pMain->SaveChData(bUseLogin, AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "SaveInterval", 2));
		SaveChData(data, AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "SaveInterval", 2));
	}
*/

// 20201103 KSCHOI Add Build Date Display By KSJ START
	CString strVer;
	strVer.Format("%s (%s_%s)", SW_VERSION, MacroDateToCString(__DATE__), __TIME__);
	SetWindowText(strVer);
// 20201103 KSCHOI Add Build Date Display By KSJ END
	return 0;
}

BOOL CMainFrame::LoadSerialSetting()
{
	m_SerialConfig.nPortNum = AfxGetApp()->GetProfileInt(SERIAL_REG_SECTION, "PortNo", 1);
	m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(SERIAL_REG_SECTION, "BaudRate", 9600);
	m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(SERIAL_REG_SECTION, "DataBit", 8);
	m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(SERIAL_REG_SECTION, "StopBit", 1);
	m_SerialConfig.nPortBuffer = AfxGetApp()->GetProfileInt(SERIAL_REG_SECTION, "Buffer", 512);
	m_SerialConfig.portParity = (char)AfxGetApp()->GetProfileInt(SERIAL_REG_SECTION ,"Parity", 'N');
	m_SerialConfig.nPortEvents = AfxGetApp()->GetProfileInt(SERIAL_REG_SECTION ,"PortEvent", EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG);
	
	return TRUE;
}

BOOL CMainFrame::WriteSerialSetting()
{
	AfxGetApp()->WriteProfileInt(SERIAL_REG_SECTION, "PortNo", m_SerialConfig.nPortNum);
	AfxGetApp()->WriteProfileInt(SERIAL_REG_SECTION, "BaudRate", m_SerialConfig.nPortBaudRate);
	AfxGetApp()->WriteProfileInt(SERIAL_REG_SECTION, "DataBit", m_SerialConfig.nPortDataBits);
	AfxGetApp()->WriteProfileInt(SERIAL_REG_SECTION, "StopBit", m_SerialConfig.nPortStopBits);
	AfxGetApp()->WriteProfileInt(SERIAL_REG_SECTION, "Buffer", m_SerialConfig.nPortBuffer);
	AfxGetApp()->WriteProfileInt(SERIAL_REG_SECTION ,"Parity", m_SerialConfig.portParity);
	AfxGetApp()->WriteProfileInt(SERIAL_REG_SECTION ,"PortEvent", m_SerialConfig.nPortEvents);

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		| WS_THICKFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;

 	cs.style &= ~FWS_ADDTOTITLE;

	WNDCLASS wc;
	::GetClassInfo( AfxGetInstanceHandle(), cs.lpszClass, &wc );
    wc.lpszClassName = CTSMON_CLASS_NAME; 
	cs.lpszClass = CTSMON_CLASS_NAME; 
	wc.hIcon = ::LoadIcon(AfxGetInstanceHandle(), "IDR_MAINFRAME");
    ::RegisterClass( &wc );

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class

	RECT rt;
	GetWindowRect(&rt);	
	BOOL bRC = m_wndSplitter.CreateStatic( this, 2, 1 );
	// m_wndSplitter.LockBar();

	// pane 0
	m_wndSplitter.CreateView ( 0, 0, RUNTIME_CLASS(CAllSystemView), CSize(0, 0), pContext );
	// pane 1
	m_wndSplitter.CreateView ( 1, 0, RUNTIME_CLASS(CTabbedWnd), CSize(0, 0), pContext );
	m_wndSplitter.SetRowInfo(0, 120, 0);
	m_wndSplitter.SetRowInfo(1, 0, 600);
	m_wndSplitter.RecalcLayout();

	// 1. 호환을 위해 일단 설정 한다.
	// 2. 향후 MainFrame에서 멤버변수 필요 없음 
	m_pAllSystemView = (CAllSystemView*)m_wndSplitter.GetPane( 0, 0 );
	m_pTab = (CTabbedWnd*)m_wndSplitter.GetPane( 1, 0 );					// save for dealloc	

	m_pManualControlView = m_pTab->m_pManualControlView;
	m_pTopView = m_pTab->m_pTopView;										// 전체 Module & Channel Monitoring
	m_pConnectionView = m_pTab->m_pConnectionView;
	m_pOperationView = m_pTab->m_pOperationView;
	m_pAutoView	= m_pTab->m_pAutoView;   //20201206 BJY AUTO VIEW 추가
	// m_pContactCheckResultView = m_pTab->m_pContactCheckResultView;

	UINT	m_nTabIndex_ManualControlView;
	UINT	m_nTabIndex_TopView;
	UINT	m_nTabIndex_ConditionView;
	UINT	m_nTabIndex_DetailView;
	UINT	m_nTabIndex_ResultView;
	UINT	m_nTabIndex_ConnectionView;
	UINT	m_nTabIndex_OperationView;
	UINT    m_nTabIndex_ContactCheckResultView;
	UINT	m_nTabIndex_MonitoringView;

	m_bSplitterCreated = TRUE;

	m_uSelect = 0;
	m_wndSplitter.HideRow(1);

	m_bChkUpsThread = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UPS_RUN", 0);
	m_iUPSFactory = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UPS_FACTORY", 0);
	
	if( m_bChkUpsThread == true )
	{
		// 1. UPS thread 시작
		m_pPCM_AliveProcessThread = AfxBeginThread(Fun_UPSChkThread, this);
	}

	//mSBCPROCThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	//if (!mSBCPROCThreadDestroyEvent)
	//{
	//}

	//mSBCPROCThreadHandle		= CreateThread(NULL, 0, ::SBCPROCThreadCallback, this, 0, NULL);
	//if (!mSBCPROCThreadHandle)
	//{
	//}

	WSADATA WsaData;
	WSAStartup(MAKEWORD(2, 2), &WsaData);

	 //theApp.m_server.NetBegin(this->GetSafeHwnd());
	 SetTimer(TIMER_FMS_NET_ON, 3000, NULL);
	 SetTimer(TIMER_500MSEC_DEFAULT, 500, NULL);
	 SetTimer(TIMER_1SEC_DEFAULT, 10000, NULL);
// 20201210 KSCHOI Add Temperature Data CSV File START
	m_lTemperatureSaveFunctionPeriod = ((CCTSMonDoc*)GetActiveDocument())->GetTemperatureDataSaveFunctionPeriod() * 1000 * 60;
	SetTimer(TIMER_TEMPERATURE_SAVE_FUNCTION, m_lTemperatureSaveFunctionPeriod, NULL);
// 20201210 KSCHOI Add Temperature Data CSV File END
/*	
	if (!m_tabWnd.Create(this, WS_CHILD | WS_VISIBLE | TWS_TABS_ON_TOP))
	{
		return FALSE;
	}
	m_tabWnd.SetNotifyWnd(this);

//	m_tabWnd.SetTabStyle( TCS_TABS_ON_LEFT );

	DWORD bySystemType;
	//Formation과 IROCV 구분
	bySystemType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();
	if(bySystemType == EP_ID_IROCV_SYSTEM)
	{
		SetWindowText("IR/OCV System");
	}
	else
	{
		SetWindowText("CTSMon");
	}
	
	m_ActiveFont.CreateFont (12, 0, 0, 0, FW_BOLD, 0, 0, 0, 
			DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "굴림");
	m_InactiveFont.CreateFont (12, 0, 0, 0, FW_NORMAL, 0, 0, 0, 
			DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, 
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "굴림");

	SECTab* pTab = NULL;
	int index = 0;

	if(bySystemType != EP_ID_IROCV_SYSTEM)
	{
		pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CAllSystemView), _T("All System"), pContext );
		ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
		if (pTab == NULL)	return FALSE;
		m_pAllSystemView = (CAllSystemView*)pTab->m_pClient;
		m_tabWnd.SetTabIcon( index++, IDI_ALL_SYSTEM_VIEW );
		m_nTabIndex_AllSystemView = index-1;
	}
	
	pTab = m_tabWnd.AddTab(RUNTIME_CLASS(CTopView), _T("Top Monitoring"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pTopView = (CTopView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_TOP );
	m_nTabIndex_TopView = index-1;



	pTab = m_tabWnd.AddTab( RUNTIME_CLASS(CDetailChannelView), _T("Channel Monitoring"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pDetailView = (CDetailChannelView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_CHANNELVIEW );
	m_nTabIndex_DetailView = index-1;

	pTab = m_tabWnd.AddTab( RUNTIME_CLASS(CProcesureFormView), _T("Test Condition"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pConditionView = (CProcesureFormView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_SENDCONDITION );
	m_nTabIndex_ConditionView = index-1;

	pTab = m_tabWnd.AddTab( RUNTIME_CLASS(CResultView), _T("Result View"), pContext );
	ASSERT(pTab->IsKindOf(RUNTIME_CLASS(SEC3DTab)));
	if (pTab == NULL)	return FALSE;
	m_pResultView = (CResultView*)pTab->m_pClient;
	m_tabWnd.SetTabIcon( index++, IDI_RESULTVIEW );
	m_nTabIndex_ResultView = index-1;
	
	m_tabWnd.SetWindowPos(&wndTop, 0, 0, 
				lpcs->cx, lpcs->cy, SWP_SHOWWINDOW );

	m_tabWnd.SetFontActiveTab(&m_ActiveFont);
	m_tabWnd.SetFontInactiveTab(&m_InactiveFont);

//	m_tabWnd.EnableTab(0, FALSE);
	m_tabWnd.ActivateTab(m_nCurTabIndex);
*/			
	return bRC;
}


void CMainFrame::OnConeditorRun() 
{
	// TODO: Add your command handler code here

	HWND FirsthWnd;
	FirsthWnd = ::FindWindow(NULL, EDITOR_APP_NAME);
	if (FirsthWnd)
	{
		::SetActiveWindow(FirsthWnd);	
		::SetForegroundWindow(FirsthWnd);
		return;
	}
	
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);	


	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");	//Get Current Folder(CTSMon Folder)
	if( strTemp.IsEmpty() )
	{
		return;
	}

	strTemp = strTemp + "\\" + EDITOR_APP_NAME + ".exe";

	BOOL bFlag = CreateProcess((LPCTSTR)strTemp, NULL, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		AfxMessageBox(TEXT_LANG[41], MB_OK|MB_ICONSTOP);
	}
}

/**
@author  
@brief Tools -> Server(PC) 로그보기
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  
@return  CTSMon 로그를 불러온다.
@see  
@todo    
*/
void CMainFrame::OnLogdlgCfg() 
{
	// TODO: Add your command handler code here
// 	m_pLogViewDlg = new CLogViewDlg;
// 	m_pLogViewDlg->Create(IDD_LOGVIEW_DLG, NULL);
// 	m_pLogViewDlg->ShowWindow(SW_SHOW);

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	char szWinDir[128];
	if(GetWindowsDirectory(szWinDir, 127) <=0 )		return;

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	CString strTemp;
	strTemp.Format("%s\\Notepad.exe %s", szWinDir, pDoc->m_szLogFileName);

	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		MessageBox(TEXT_LANG[42], "Excute Error", MB_OK|MB_ICONSTOP);
	}
}

//lParam : 이전 상태 정보 
//  [5/19/2009 kky ]
// for lParam 과 현재의 상태를 비교해서 파일을 생성
LRESULT CMainFrame::OnModuleStateChange(WPARAM wParam, LPARAM lParam)
{
	CString strErrorCode;			// 에러코드 저장
	UINT nNormalCnt = 0;			// 양품
	UINT nFailCnt = 0;				// 불량
	int nModuleID = HIWORD(wParam);
	int nGroupIndex = LOWORD(wParam);
	CString strTemp;

	//이전 Group Fail Code와 비교 해서 변했으면 기록 한다.
	EP_GP_STATE_DATA prevState;
	EP_GP_STATE_DATA _State;
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == NULL)	
		return 0;
		
	bool bChangeState = false;
	
	if(lParam != 0)
	{
		// 1. 현재 GUI 모듈의 정보
		memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
		
		// 2. SBC에서 전송된 모듈 정보
		memcpy(&_State, (const void *)lParam, sizeof(EP_GP_STATE_DATA));

					
// 		if( _State.state == EP_STATE_EMERGENCY )
// 		{	
// 			if( prevState.state != EP_STATE_EMERGENCY )
// 			{	
// 				bChangeState = true;					
// 			}
// 		}
// 		else if( _State.state == EP_STATE_PAUSE )
// 		{	
// 			if( prevState.state != EP_STATE_PAUSE )
// 			{	
// 				bChangeState = true;					
// 			}
// 		}
// 		else if( _State.state == EP_STATE_MAINTENANCE )
// 		{
// 			if( prevState.state != EP_STATE_MAINTENANCE )
// 			{	
// 				bChangeState = true;					
// 			}		
// 		}
// 		else
// 		{	
			switch( prevState.state )
			{
				case EP_STATE_IDLE:
					{	
						if( _State.state == EP_STATE_STANDBY )
						{
							bChangeState = true;
							
							if( _State.sensorState[1] == 0 )
							{
								_State.state = EP_STATE_READY;		
							}
						}
						else
						{
							if( _State.state != EP_STATE_IDLE )
							{
								bChangeState = true;															
							}						
						}	
					}
					break;
					
				case EP_STATE_READY:
					{
						if( _State.state == EP_STATE_STANDBY )
						{
							if( _State.sensorState[1] != 0 )
							{
								bChangeState = true;
							}
						}
						else
						{
							bChangeState = true;																					
						}										
					}
					break;			
				
				case EP_STATE_STANDBY:
					{
						if( _State.state == EP_STATE_STANDBY )
						{
							if( _State.sensorState[1] == 0 )
							{
								bChangeState = true;
								_State.state = EP_STATE_READY;
							}
						}
						else
						{
							bChangeState = true;
						}								
					}
					break;
				
				case EP_STATE_RUN:
					{
						if( _State.state != EP_STATE_RUN )
						{
							bChangeState = true;
						}						
					}
					break;
				
				case EP_STATE_END:
					{
						if( _State.state != EP_STATE_END )
						{
							bChangeState = true;
						}
						/*						
						if( _State.state == EP_STATE_STANDBY )
						{
						
						}
						else
						{	
							if( _State.sensorState[1] == 0 )
							{
								bChangeState = true;
								_State.state = EP_STATE_READY;		
							}	
							else if( _State.state == EP_STATE_IDLE )
							{
								bChangeState = true;							
							}					
						}
						*/
					}
					break;
					
				case EP_STATE_LINE_ON:
					{
						if( _State.state != EP_STATE_LINE_ON )
						{
							bChangeState = true;
						}					
					}
					break;

				case EP_STATE_LINE_OFF:
					{
						if( _State.state != EP_STATE_LINE_OFF )
						{
							bChangeState = true;							
						}					
					}
					break;

				case EP_STATE_PAUSE:
					{
						if( _State.state != EP_STATE_PAUSE )
						{
							bChangeState = true;
						}					
					}
					break;				

				case EP_STATE_MAINTENANCE:
					{
						if( _State.state != EP_STATE_MAINTENANCE )
						{
							bChangeState = true;							
						}					
					}
					break;		
				case EP_STATE_CALIBRATION:
					{
						if( _State.state != EP_STATE_CALIBRATION )
						{
							bChangeState = true;							
						}					
					}
					break;	
				default:
					{
						bChangeState = true;
						strTemp.Format("[Unknown ModeInfo] %d ====> %d From [ %d ]\n ", prevState.state, _State.state, nModuleID);
						pDoc->WriteLog(strTemp);										
					}
					break;	
						
					/*
					//시험을 완료 하고 Jig가 Up 되더라도 상태를 End 상태 유지(이후 상태가 바뀌면 Stanby나 Idle로 전환)
					if(prevState.state == EP_STATE_END	&& (prevState.sensorState[0] != _State.sensorState[0]))
					{
						_State.state = EP_STATE_END;
					}
					else
					{
						//시험조건이 전송된 상태에서 Tray가 없거나 Door가 Open된 경우
						if( _State.sensorState[1] == 0 || _State.sensorState[3] == 0)			//Tray가 1개도 Loading 안되거나 Door가 Open되어 있을 경우 
						{
							_State.state = EP_STATE_READY;
						}
					}*/
				}			
//		}
		
		/*
		WORD failCode = EPGetFailCode(nModuleID, nGroupIndex);		
		//갱신 Code가 이전 코드와 다를 경우(단 정상으로 전환은 제외)
		if(prevState.failCode != failCode && failCode != 0)
		{
			CString strMsg, strDescript,strTemp;
			//Error Code 검색 
			CFailMsgDlg *pDlg = new CFailMsgDlg();
			ASSERT(pDlg);
			pDlg->SetErrorCode(failCode, pDoc->m_strDataBaseName);
			pDlg->GetFailMsg(strMsg, strDescript);
			delete pDlg;
			pDlg = NULL;
			
			//Trouble Code 기록
			strErrorCode = strMsg;			
			strTemp.Format(",%s,FailCode,%d, %s", ::GetModuleName(nModuleID), failCode, strMsg);
			pDoc->WriteEMGLog(strTemp);
		}
		else
		{
			CString strMsg, strDescript, strTemp;
			//Error Code 검색 
			CFailMsgDlg *pDlg = new CFailMsgDlg();
			ASSERT(pDlg);
			pDlg->SetErrorCode(failCode, pDoc->m_strDataBaseName);
			pDlg->GetFailMsg(strMsg, strDescript);
			delete pDlg;
			pDlg = NULL;			
			//Trouble Code 기록
			strErrorCode = strMsg;
		}
		*/
		
		if( prevState.sensorState[4] != _State.sensorState[4] )
		{	
			// 1. Linemode 데이터가 변경 되었으면 Stage의 상태를 변경한다.			
			bChangeState = true;
			EPSetOperationMode(nModuleID, nGroupIndex, _State.sensorState[4]);
		}

		/*
		////최초  Group State가 변경되었음 
		//WORD state = pModule->GetState();		
		if(prevState.state == EP_STATE_LINE_ON && _State.state != EP_STATE_LINE_ON)
		{
			TRACE("Group state first recevied\n");
			
			pDoc->CheckAndLoadPrevTestInfo(nModuleID);				

			strTemp.Format("Group state first recevied ====> Read TempFile From [ %d ]\n ", nModuleID);
			pDoc->WriteLog(strTemp);										

			bChangeState = true;
		}
		*/
		int nMode = pModule->GetOperationMode();
		if( nMode == EP_OPERATION_CALIBRATION)
		{			

			memcpy( prevState.sensorState, _State.sensorState, sizeof(_State.sensorState));	
			memcpy( prevState.IOSensorState, _State.IOSensorState, sizeof(_State.IOSensorState));
			memcpy( &prevState.ManualFunc1, &_State.ManualFunc1, sizeof(_State.ManualFunc1));
			memcpy( &prevState.ManualFunc2, &_State.ManualFunc2, sizeof(_State.ManualFunc2));
			pModule->SetState(prevState);
			
		}
		else if( bChangeState == true)
		{
			pModule->SetState(_State);	
			m_pTab->UpdateGroupState(nModuleID);
		}
		else
		{
			memcpy( prevState.sensorState, _State.sensorState, sizeof(_State.sensorState));			
			memcpy( prevState.IOSensorState, _State.IOSensorState, sizeof(_State.IOSensorState));
			memcpy( &prevState.ManualFunc1, &_State.ManualFunc1, sizeof(_State.ManualFunc1));
			memcpy( &prevState.ManualFunc2, &_State.ManualFunc2, sizeof(_State.ManualFunc2));
			pModule->SetState(prevState);
		}	
	}
		
	return 0;
}

/*
LRESULT CMainFrame::OnModuleStateChange(WPARAM wParam, LPARAM lParam)
{
	CString strErrorCode;			// 에러코드 저장
	UINT nNormalCnt = 0;			// 양품
	UINT nFailCnt = 0;				// 불량
	int nModuleID = HIWORD(wParam);
	int nGroupIndex = LOWORD(wParam);

	//이전 Group Fail Code와 비교 해서 변했으면 기록 한다.
	EP_GP_STATE_DATA prevState;
	EP_GP_STATE_DATA _State;
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == NULL)	
		return 0;

	if(lParam != 0)
	{
		// 1. 현재 모듈의 정보
		memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
		// 2. SBC에서 전송된 모듈 정보
		memcpy(&_State, (const void *)lParam, sizeof(EP_GP_STATE_DATA));

		switch( prevState.state )
		{
		case EP_STATE_IDLE:
			{

			}
			break;

		case EP_STATE_READY:
			{

			}
			break;

		case EP_STATE_STANDBY:
			{

			}
			break;

		case EP_STATE_RUN:
			{
				if( _State.state == EP_STATE_STANDBY )
				{
					_State.state = EP_STATE_END;			
				}
				else if()
				{

				}			
			}
			break;

		case EP_STATE_END:
			{

			}
		default:
			{

			}

			break;			

		}

		prevState.state 

			if(_State.state == EP_STATE_STANDBY)	
			{
				if(prevState.state == EP_STATE_RUN)		//Run에서 Stanby로 가면 End 상태로 보고 
				{
					_State.state = EP_STATE_END;
				}
				else
				{
					//시험을 완료 하고 Jig가 Up 되더라도 상태를 End 상태 유지(이후 상태가 바뀌면 Stanby나 Idle로 전환)
					if(prevState.state == EP_STATE_END	&& (prevState.sensorState[0] != _State.sensorState[0]))
					{
						_State.state = EP_STATE_END;
					}
					else
					{
						//시험조건이 전송된 상태에서 Tray가 없거나 Door가 Open된 경우
						if( _State.sensorState[1] == 0 || _State.sensorState[3] == 0)			//Tray가 1개도 Loading 안되거나 Door가 Open되어 있을 경우 
						{
							_State.state = EP_STATE_READY;
						}
					}
				}
			}

			WORD failCode = EPGetFailCode(nModuleID, nGroupIndex);		
			//갱신 Code가 이전 코드와 다를 경우(단 정상으로 전환은 제외)
			if(prevState.failCode != failCode && failCode != 0)
			{
				CString strMsg, strDescript,strTemp;
				//Error Code 검색 
				CFailMsgDlg *pDlg = new CFailMsgDlg();
				ASSERT(pDlg);
				pDlg->SetErrorCode(failCode, pDoc->m_strDataBaseName);
				pDlg->GetFailMsg(strMsg, strDescript);
				delete pDlg;
				pDlg = NULL;

				//Trouble Code 기록
				strErrorCode = strMsg;			
				strTemp.Format(",%s,FailCode,%d, %s", ::GetModuleName(nModuleID), failCode, strMsg);
				pDoc->WriteEMGLog(strTemp);
			}
			else
			{
				CString strMsg, strDescript, strTemp;
				//Error Code 검색 
				CFailMsgDlg *pDlg = new CFailMsgDlg();
				ASSERT(pDlg);
				pDlg->SetErrorCode(failCode, pDoc->m_strDataBaseName);
				pDlg->GetFailMsg(strMsg, strDescript);
				delete pDlg;
				pDlg = NULL;			
				//Trouble Code 기록
				strErrorCode = strMsg;
			}

			////최초  Group State가 변경되었음 
			//WORD state = pModule->GetState();		

			if(prevState.state == EP_STATE_LINE_ON && _State.state != EP_STATE_LINE_ON)
			{
				TRACE("Group state first recevied\n");
				if(_State.state != EP_STATE_IDLE && _State.state != EP_STATE_MAINTENANCE)
				{
					pDoc->CheckAndLoadPrevTestInfo(nModuleID);
				}
			}

			pModule->SetState(_State);	
	}
	//Update All View Controls
	m_pTab->UpdateGroupState(nModuleID, nGroupIndex);

	return 0;
}


*/

BOOL	g_bUseRackIndex;
int		g_nModulePerRack;
BOOL	g_bUseGroupSet;
CString	g_strModuleName;
CString	g_strGroupName;

CString GetDefaultModuleName(int nModuleID, int nGroupIndex)
{
	CString strName, strMDName;	

	strMDName = g_strModuleName;

	EP_SYSTEM_PARAM *pParam = EPGetSysParam(nModuleID);
	if(pParam)
	{
		if(pParam->wModuleType == EP_ID_IROCV_SYSTEM)
		{
			strMDName = "IROCV";
		}
	}

	if(g_bUseRackIndex)
	{
		div_t div_result;
		div_result = div( nModuleID-1, g_nModulePerRack );

		if(g_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %02d-%02d, %s %d", strMDName, div_result.quot+1, div_result.rem+1, g_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %02d-%02d", strMDName, div_result.quot+1, div_result.rem+1);
		}
	}
	else
	{
		if(g_bUseGroupSet && nGroupIndex >= 0)
		{
			strName.Format("%s %03d, %s %02d", strMDName, nModuleID,  g_strGroupName, nGroupIndex+1);
		}
		else
		{
			strName.Format("%s %03d", strMDName, nModuleID);
		}
	}
	return strName;
}

CString GetModuleName(int nModuleID, int nGroupIndex)
{
	CString strName, strMDName;
	CFormModule *pModule = ((CCTSMonDoc *)((CMainFrame *)AfxGetMainWnd())->GetActiveDocument())->GetModuleInfo(nModuleID);
	if(pModule)
	{
		strName = pModule->GetModuleName();
	}
	else
	{
		//strName = ::GetDefaultModuleName(nModuleID, nGroupIndex);
	}
	return strName;
}

BOOL LoginPremissionCheck(int nAction)
{
	CLoginDlg *pDlg;
	pDlg = new CLoginDlg;
	ASSERT(pDlg);

	if(pDlg->DoModal() != IDOK)
	{
		delete pDlg;
		pDlg = NULL;
		return FALSE;
	}

	//현재 입력 ID가 권한을 갖았는지만 검사 하고 return 실시  
	long lPermission = g_LoginData.nPermission;
	delete pDlg;
	pDlg = NULL;

	return (nAction & lPermission);
}

BOOL PermissionCheck(int nAction)
{
	return (nAction & g_LoginData.nPermission);
}

CString GetDataBaseName()
{
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");		//Get DataBase Folder
	if(strTemp.IsEmpty())
	{
		return strTemp;
	}

	strTemp = strTemp+ "\\" + FORM_SET_DATABASE_NAME;
	return strTemp;
/*	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	ASSERT(pFrame);
	CCTSMonDoc *pDoc = (CCTSMonDoc *)pFrame->GetActiveDocument();
	ASSERT(pDoc);
	return pDoc->m_strDataBaseName;
//	return _T("C:\\My Documents\\Condition.mdb");
*/
}

CString GetLogDataBaseName()
{
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");		//Get DataBase Folder
	if(strTemp.IsEmpty())
	{
		return strTemp;
	}
	
	strTemp = strTemp+ "\\" + LOG_DATABASE_NAME;
	return strTemp;
// 	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
// 	ASSERT(pFrame);
// 
// 	CCTSMonDoc *pDoc = (CCTSMonDoc *)pFrame->GetActiveDocument();
// 	ASSERT(pDoc);
// 	return pDoc->m_strLogDataBaseName;
}


void CMainFrame::OnBoardGroupSet()
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	CGroupSetDlg *pDlg;

	pDlg = new CGroupSetDlg;
	ASSERT(pDlg);
	pDlg->SetDoc(pDoc);

	if(IDOK == pDlg->DoModal())
	{		
		//Unit 정보를 Update 시킴 
		pDoc->UpdateUnitSetting();
		if(m_pTopView)
		{
			m_pTopView->DataUnitChanged();
			if(m_pTopView->m_nTopGridColCount != pDlg->m_nTopGridCol)
			{
				if(m_pTopView->m_nTopGridColCount != pDlg->m_nTopGridCol)
				{
					m_pTopView->GroupSetChanged(pDlg->m_nTopGridCol);				
				}
			}
		}
	
		pDoc->m_bHideNonCellData = pDlg->m_bAutoProcDlg;
		pDoc->m_strDataFolder = pDlg->m_strDataPath;
		pDoc->m_strOnlineDataPath = pDlg->m_strOnlineDataPath;
		pDoc->m_bFolderTime = pDlg->m_bTimeFolder;
		pDoc->m_bFolderModuleName = pDlg->m_bModuleFolder;
		pDoc->m_nTimeFolderInterval = pDlg->m_nTimeFolderInterval;
		pDoc->m_bFolderLot = pDlg->m_bLotFolder;
		pDoc->m_bFolderTray = pDlg->m_bTrayFolder;
		pDoc->m_lWarnningChVoltage = pDlg->m_lWarnningChVoltage;
		pDoc->m_lDangerChVoltage = pDlg->m_lDangerChVoltage;
		pDoc->m_nAfterSafeVoltageError = pDlg->m_nAfterSafeVoltageError;
		
		if(m_pTopView) m_pTopView->SetTopGridFont();
		if(m_pConnectionView) m_pConnectionView->SetTopGridFont();

//		if(m_pResultView)	m_pResultView->m_bDispCapSum = pDlg->m_bCapaSumDisplay;
		//SaveChData(pDlg->m_bSaveData, pDlg->m_nDataSaveInterval);
		OnConnectDbsvr();
	}
	delete pDlg;
	pDlg = NULL;
}

LRESULT CMainFrame::OnModuleConnected(WPARAM wParam, LPARAM /*lParam*/)
{
	int nModuleID = HIWORD(wParam);
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();	
	int nGroupNo = EPGetGroupCount(nModuleID);
	ASSERT( nGroupNo>0 );

	//2005/12/23 수정 
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == NULL)
		return 0;
	
	CString strLog;
	strLog.Format("[Stage %s] Connected!", GetModuleName(nModuleID));
	pDoc->WriteLog(strLog);

	pModule->MakeGroupStruct(nGroupNo);

	pDoc->CheckAndLoadPrevTestInfo(nModuleID);

	//Write connected module information to database
	pDoc->WriteModuleIPToDataBase(nModuleID, EPGetModuleIP(nModuleID));

	//Cell No Index를 Update한다. 20090920
	pDoc->UpdateTrayCellStartNo(nModuleID);

	if(m_pTopView)					m_pTopView->ModuleConnected(nModuleID);
	if(m_pManualControlView)		m_pManualControlView->ModuleConnected(nModuleID);
	if(m_pConnectionView)			m_pConnectionView->ModuleConnected(nModuleID);
	if(m_pOperationView)			m_pOperationView->ModuleConnected(nModuleID);
	if(m_pAutoView)					m_pAutoView->ModuleConnected(nModuleID);	//2020 BJY AUTO VIEW 추가
	// if(m_pContactCheckResultView)	m_pContactCheckResultView->ModuleConnected(nModuleID);
	
	if( EPUpdateModuleLineMode(nModuleID) == FALSE )
	{	
		// strLog.Format("[Stage %d] 의 line mode request 를 실패했습니다!", nModuleID);
		pDoc->WriteLog(GetModuleName(nModuleID)+" line mode request fail!!!");
	}

	if((EPJustSendCommand( nModuleID, 0, 0, EP_CMD_SAFETY_VERSION_REQUEST)) != EP_ACK)
	{

	}


	
	return 1;
}

LRESULT CMainFrame::OnModuleDisConnected(WPARAM wParam, LPARAM /*lParam*/)
{
//#ifndef _DEBUG	
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);		
	
	int nModuleID = HIWORD(wParam);
	if(m_pAllSystemView)			{ m_pAllSystemView->ModuleDisConnected(nModuleID); }
	if(m_pTopView)					{ m_pTopView->ModuleDisConnected(nModuleID); }
	if(m_pManualControlView)		{ m_pManualControlView->ModuleDisConnected(nModuleID); }
	if(m_pConnectionView)			{ m_pConnectionView->ModuleDisConnected(nModuleID); }
	if(m_pOperationView)			{ m_pOperationView->ModuleDisConnected(nModuleID); }
	if(m_pAutoView)					{ m_pAutoView->ModuleDisConnected(nModuleID); }
	
	return 1;
}

void CMainFrame::OnCfgTopConfig() 
{
	// TODO: Add your command handler code here
	CTopMonitoringConfigDlg *pDlg;
	pDlg = new CTopMonitoringConfigDlg;
	if(pDlg == NULL)	return;
	
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);
	pDlg->m_TopCfg = pDoc->m_TopConfig;
	if (pDlg->DoModal() == IDOK)
	{
		pDoc->m_TopConfig = pDlg->m_TopCfg;
		::WriteTopConfig(pDoc->m_TopConfig);
		if(m_pTopView)	
		{	
			m_pTopView->TopConfigChanged();
		}
	}
	delete pDlg;
	pDlg = NULL;
}

void CMainFrame::OnChangeUser() 
{
	// TODO: Add your command handler code here
	::LoginCheck();
}

void CMainFrame::OnUserSetting() 
{
	// TODO: Add your command handler code here
	CChangeUserInfoDlg *pDlg;
	pDlg = new CChangeUserInfoDlg(&g_LoginData, &g_LoginData, this);
	ASSERT(pDlg);
	pDlg->m_bNewUser = FALSE;
	if(IDOK == pDlg->DoModal())
	{
		memcpy(&g_LoginData, pDlg->GetUserInfo(), sizeof(STR_LOGIN));
	}
	delete pDlg;
	pDlg = NULL;
}


void CMainFrame::OnUpdateUserSetting(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(g_LoginData.nPermission);
}

void CMainFrame::OnAdministration() 
{
	// TODO: Add your command handler code here
	CUserAdminDlg	*pDlg;
	pDlg = new CUserAdminDlg;
	pDlg->m_pLoginData = &g_LoginData;
	pDlg->DoModal();
	delete pDlg;
	pDlg = NULL;
}

////////////////////////////////////////////////////////////////////////////////////
//
//	lParam  구조 : [EP_TEST_RESULT * EP_RESULT_ITEM_NO]+[EP_CH_DATA * Channel 수]
//
/////////////////////////////////////////////////////////////////////////////////////
LRESULT CMainFrame::OnSaveDataReceive(WPARAM wParam, LPARAM lParam)
{
	int nModuleID = HIWORD(wParam);
	int nGroupIndex = LOWORD(wParam);
	LPVOID lpData = (LPVOID)lParam;
	ASSERT(lParam);

	char msg[128];
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == NULL)
	{
		sprintf(msg, "%s:: Module information error.\n", ::GetModuleName(nModuleID));
		pDoc->WriteLog(msg);
		return 0;
	}
	else
	{
		CString strResultFile = pDoc->GetResultFileName(nModuleID, 0);
		if( strResultFile.IsEmpty() )
		{
			sprintf(msg, "%s:: Module ResultFileName information error.\n", ::GetModuleName(nModuleID));
			pDoc->WriteLog(msg);
			return 0;
		}
	}

	CString strErrorCode;			// 에러코드 저장

	int nHWChCount = EPGetChInGroup(nModuleID);	
//	int nTrayCellCount = pModule->GetCellCountInTray();
//	if(nTrayCellCount <= 0 || nHWChCount <= 0)
//	{
//		sprintf(msg, "%s:: Step end data save fail.(Channel number in tray fail)\n", ::GetModuleName(nModuleID));
//		pDoc->WriteLog(msg);
//		return;
//	}
		
	//2006/01/03 KBH
	//비정상적인 shutdown에의해 프로그램이 재실행된 경우 
	//즉 현재 Run이거나 Pause인데 시험 정보가 없으면 이전 최종 시험 정보를 Loading한다.
//	pDoc->CheckAndLoadPrevTestInfo(nModuleID, nGroupIndex);
	//not use
	EP_STEP_END_HEADER stepHeader;
	memcpy(&stepHeader, (char *)lpData, sizeof(EP_STEP_END_HEADER));
	
	EP_CH_DATA *lpchData = new EP_CH_DATA[nHWChCount];
	ASSERT(lpchData);
	memcpy(lpchData, (char *)lpData+sizeof(EP_STEP_END_HEADER), sizeof(EP_CH_DATA)*nHWChCount);		//Test 결과 Channel 정보

	//20081208 BFServer에서 Release 함
//	delete [] lpData;
//	lpData = NULL;
	if( lpchData[0].nStepNo == 0 )
	{
		UINT nNormalCnt = 0;			// 양품
		UINT nFailCnt = 0;				// 불량
		
		CString strFailTray;
		CString strTemp;
		CString strNormalTray;
		CTray *pTray;
		
// 20201030 KSCHOI Divide Contact Check Result By Six START
//		int nTrayColCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 16);
//		nTrayColCount = pModule->GetCellCountInTray()/nTrayColCount;
		int nTrayColCount = pModule->GetCellCountInTray();
// 20201030 KSCHOI Divide Contact Check Result By Six END

		for(int t=0; t<pModule->GetTotalJig(); t++)
		{
			pTray = pModule->GetTrayInfo(t);
			if(pTray->IsWorkingTray())
			{
				CString strTemp = _T("");
				CString strCellPosition = _T("");				

				int nChCount = pModule->GetChInJig(t);				
				
				//정상 수량 계산 
				int nNormalCnt = 0;
				int nStartIndex = pModule->GetStartChIndex(t);
				for(int c = nStartIndex; c<EP_MAX_CH_PER_MD && c<nStartIndex+nChCount; c++)
				{
					if(lpchData[c].channelCode == EP_CODE_NORMAL || lpchData[c].channelCode == EP_CODE_CELL_CHECK_OK )
					{
						nNormalCnt++;
						strCellPosition += "O";
					}
					else
					{
						strCellPosition += "X";					
					}

// 20201030 KSCHOI Divide Contact Check Result By Eight START
					if( (c+1)%8 == 0)
					{
						strCellPosition += " ";
					}
// 20201030 KSCHOI Divide Contact Check Result By Eight END

					if( (c+1)%nTrayColCount == 0 )
					{
						strCellPosition += "\n";
					}					 
				}
				
				if(nNormalCnt != pTray->GetInputCellCnt() )
				{
					strFailTray = TEXT_LANG[0]; //"이상"
				}	
				else
				{
					strFailTray = TEXT_LANG[1]; //"정상"
				}

				strTemp.Format(TEXT_LANG[2]+"\n%s", pTray->GetTrayNo(), strFailTray, nNormalCnt, pTray->GetInputCellCnt(), strCellPosition); //"TrayID [%s] ====> %s \n[현재 Cell수량: %d Cells/ 입력된 Cell수량: %d Cells]\n\n%s"
				strNormalTray += strTemp;
								
				// 1. CellCheck 관련 데이터 저장 kky
				STR_STEP_RESULT rStepResult;
				
				//전송되어온 Data를 Tray단위로 분리하여 별도의 파일로 저장한다.	
				STR_DB_REPORT_HEADER rptHeader;
				int nDataHeaderSize = sizeof(STR_DB_REPORT_HEADER);	//1
				int nGroupStateSize = sizeof(GROUP_STATE_SAVE);		//2
				int nStepConSize = sizeof(STR_COMMON_STEP);			//3
				
				int nChSize = nChCount*sizeof(STR_SAVE_CH_DATA);	//4
				int nBuffSize = nDataHeaderSize + nGroupStateSize+ nStepConSize + nChSize ;		//DataBase로 전송할 Data Buffer
				LPVOID lpRxBuffer = new char[nBuffSize];
				LPSTR_SAVE_CH_DATA lpSaveChData = new STR_SAVE_CH_DATA[nChCount];
								
				if(MakeCellCheckFileStructure(nModuleID, t, (LPVOID)lpchData, lpSaveChData))
				{
					//Tray의 결과 Data를 파일로 저장 
					if(pDoc->SaveCellChkResultData(nModuleID, t, 0, (LPVOID)lpSaveChData, rStepResult) == FALSE)
					{
						sprintf(msg, "[%s] Step %d contect check end data save fail.", ::GetModuleName(nModuleID), 0);
						pDoc->WriteLog(msg);
					}
				}
				
				// 1. Contact 체크 진행시 셋팅된 수량이 0이 아닐 경우에만 체크한다.
				// 2. Local 모드와 Auto 모드에서의 셋팅값이 다르다.
				int nLineMode = pModule->GetOperationMode();
				int nContactErrorCnt = 0;
				
				// Local, Auto 모드 모두 공통 값을 참조.
				nContactErrorCnt = pModule->GetTrayInfo(0)->m_nContactErrlimit;
								
				int nNGCellCnt = 0;
				nNGCellCnt = pModule->GetTrayInfo(0)->GetInputCellCnt() - nNormalCnt;
				
				if(nNGCellCnt != 0)				
				{				
					sprintf(msg, TEXT_LANG[3], ::GetModuleName(nModuleID), pModule->GetTrayNo(), pModule->GetTrayInfo(0)->GetInputCellCnt(), nNGCellCnt, nNGCellCnt, nContactErrorCnt);								
					pDoc->WriteLog(msg);				  				
				}
				
				if( nContactErrorCnt != 0 )
				{
					if( nNGCellCnt == pModule->GetTrayInfo(0)->GetInputCellCnt() )
					{
						// 1. All Cell NG					
					}
					else
					{
						if( nNGCellCnt < 0)
						{
							nNGCellCnt = nNGCellCnt * -1;				
						}

						if( nNGCellCnt >= nContactErrorCnt )
						{
							CString strMsg;
							CString strMsg2;
							strMsg2.Format(TEXT_LANG[43], ::GetModuleName(nModuleID, nGroupIndex));
							strMsg.Format(TEXT_LANG[44], ::GetModuleName(nModuleID, nGroupIndex));
							strTemp.Format("%s\n\n%s\n\n%s", strMsg, strNormalTray, strMsg2);

							ContactCheckResultDlg *pContactCheckResultDlg;
							bool	bFindDlg = false;
							if (m_apContactCheckResultDlg.GetSize() > 0)
							{
								int i = 0;
								for ( i=0 ; i< m_apContactCheckResultDlg.GetSize(); i++ )
								{
									pContactCheckResultDlg = (ContactCheckResultDlg *)m_apContactCheckResultDlg[i];

									if (pContactCheckResultDlg->GetModuleID() == nModuleID)
									{
										if (pContactCheckResultDlg->IsWindowVisible())
										{
											bFindDlg = true;
											pContactCheckResultDlg->SetModuleID(nModuleID);
											pContactCheckResultDlg->SetResultMsg(strTemp);						
										}
										else
										{
											bFindDlg = true;
											pContactCheckResultDlg->SetModuleID(nModuleID);
											pContactCheckResultDlg->SetResultMsg(strTemp);	
											pContactCheckResultDlg->ShowWindow(SW_SHOW);				
										}			 
									}
								}
							}

							if( bFindDlg == false )
							{
								pContactCheckResultDlg =  new ContactCheckResultDlg(pDoc, this);
								ASSERT(pContactCheckResultDlg);
								pContactCheckResultDlg->Create(IDD_CONTACT_CHECK_RESULT_DLG, this);
								pContactCheckResultDlg->SetModuleID(nModuleID);
								pContactCheckResultDlg->SetResultMsg(strTemp);	
								pContactCheckResultDlg->ShowWindow(SW_SHOW);
								m_apContactCheckResultDlg.Add(pContactCheckResultDlg);	
							}

							// 1. Cell Contact 데이터로 따로 저장 관리 한다.
							sprintf(msg, "%s :: Cell Contact result data recv.", ::GetModuleName(nModuleID));
							pDoc->WriteLog(msg);

							pDoc->m_fmst.fnSendContactResultData(nModuleID);
						}
					}
				}
				
				delete[] lpRxBuffer;	lpRxBuffer = NULL;
				delete[] lpSaveChData;	lpSaveChData = NULL;
				delete[] lpchData;		lpchData = NULL;
			}
		}
		
		return 1;
	}
	
	CString strResultData = _T("");
	CString strDescFileName = _T("");
	CString strTempResultFileName = _T("");
	
	// 1. EMG 상황의 경우 결과데이터에 _EMG 를 붙인 결과데이터를 생성한다.
	if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_EMG
		|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES 
		|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_CONTACT )
/*
	if( pModule->GetTrayInfo(0)->m_nResultFileType != EP_RESULTFILE_TYPE_NORMAL && 
		pModule->GetTrayInfo(0)->m_nResultFileType != EP_RESULTFILE_TYPE_CONTACT)
*/
	{	
		strResultData = pModule->GetResultFileName(0);
		if( !strResultData.IsEmpty() )
		{
			EP_FILE_HEADER	fileHeader;
			FILE *fp = fopen((LPCTSTR)strResultData, "rb+");
			if(fp != NULL)
			{
				//File Header Read
				fread(&fileHeader, sizeof(EP_FILE_HEADER), 1, fp);

				fileHeader.nResultFileType = pModule->GetTrayInfo(0)->m_nResultFileType;			

				fseek(fp, 0, SEEK_SET);

				fwrite(&fileHeader, sizeof(EP_FILE_HEADER), 1, fp);

				fclose(fp);	
				fp = NULL;
			}

			pModule->UpdateTestLogHeaderTempFile();

			char	drive[_MAX_PATH]; 
			char	dir[_MAX_PATH]; 
			char	fname[_MAX_PATH]; 
			char	ext[_MAX_PATH]; 
			_splitpath((LPSTR)(LPCTSTR)strResultData, drive, dir, fname, ext);	

			strDescFileName.Format("%s\\%s\\%s_TEMP.fmt", drive, dir, fname);		// 1. 현재 저장되고 있는 파일을 임시 파일로 저장한 후 EMG 데이터를 저장한다.

			_unlink(strDescFileName);

			CopyFile( strResultData, strDescFileName, false );	
		}
	}
		
	int nStepIndex = lpchData[0].nStepNo-1;								//Step 번호 1번 채널을 대표값으로 사용	
	int nStepType = EP_TYPE_NONE;
	int nStepProcType = EP_PROC_TYPE_NONE;	
	CTestCondition *pCondition = pModule->GetCondition();
	STR_COMMON_STEP stepData;
	ZeroMemory(&stepData, sizeof(STR_COMMON_STEP));

	CStep *pStep = pCondition->GetStep(nStepIndex);
#ifdef _DEBUG
	if(pStep == NULL)
	{
		sprintf(msg, "%s:: Step index error %d.", ::GetModuleName(nModuleID), nStepIndex);
//		AfxMessageBox(msg);
	}
#endif	
	if(pStep)
	{
		nStepType = pStep->m_type;
		nStepProcType = pStep->m_lProcType;
		stepData = pStep->GetStepData();
	}

	//전송되어온 Data를 Tray단위로 분리하여 별도의 파일로 저장한다.	
	STR_DB_REPORT_HEADER rptHeader;
	int nDataHeaderSize = sizeof(STR_DB_REPORT_HEADER);		//1
	int nGroupStateSize = sizeof(GROUP_STATE_SAVE);			//2
	int nStepConSize = sizeof(STR_COMMON_STEP);				//3

	STR_STEP_RESULT rStepResult;
	BOOL bAutoProcess = EPGetAutoProcess(nModuleID);
	CTray *pTrayInfo;

	for(int t=0; t<pModule->GetTotalJig(); t++)
	{
		pTrayInfo = pModule->GetTrayInfo(t);

		if(pTrayInfo->IsWorkingTray())
		{
			int nChCount = pModule->GetChInJig(t);
			int nChSize = nChCount*sizeof(STR_SAVE_CH_DATA);	//4
			int nBuffSize = nDataHeaderSize + nGroupStateSize+ nStepConSize + nChSize ;		//DataBase로 전송할 Data Buffer
			LPVOID lpRxBuffer = new char[nBuffSize];
			LPSTR_SAVE_CH_DATA lpSaveChData = new STR_SAVE_CH_DATA[nChCount];

//			DWORD dwStart = GetTickCount();
			BOOL	bDBReport = TRUE;
			if(MakeFileStructure(nModuleID, t, nStepIndex, (LPVOID)lpchData, lpSaveChData))
			{
				//Tray의 결과 Data를 파일로 저장 
				if(pDoc->SaveResultData(nModuleID, t, nStepIndex, (LPVOID)lpSaveChData, rStepResult) == FALSE)
				{
					sprintf(msg, "%s :: Step %d end data save fail.", ::GetModuleName(nModuleID), nStepIndex+1);
					pDoc->WriteLog(msg);
				}

				//2006/09/20 모든 data를 전송하고 Server에서 선택하여 저장한다.
				if(m_bDBSvrConnect && bDBReport && nStepIndex >= 0)
				{
					//20070213
					//data 저장을 파일로 처리하고 중복될 경우 overwrite되므로 파일명 생성시 전송 하도록 수정(주석)
/*					//제일 첫번째 Step이 정상적으로 처리 되었을시 시작 정보 기록 (Step 1번이 Skip 되었을시 처리 필요)
					if(nStepIndex == 0)
					{
						//한개의 Tray 작업에 대해 1번만 전송해야 한다.
						pDoc->SendProcedureLogToDataBase(nModuleID, t);
					}
*/
					//result header + step condition + channel data
					strcpy(rptHeader.szTestSerialNo, pTrayInfo->strTestSerialNo);
					rptHeader.wInputCellCount = pTrayInfo->GetInputCellCnt();		//양품수량 Updat후 
					rptHeader.wTotCh = nChCount;							//전송할 총 Ch 수 
					rptHeader.lTestID = pCondition->GetTestInfo()->lID;
					sprintf(rptHeader.szFileName, "%s", pTrayInfo->m_strFileName);
					sprintf(rptHeader.szLot, "%s", pTrayInfo->strLotNo);
					sprintf(rptHeader.szTray, "%s", pTrayInfo->GetTrayNo());
					sprintf(rptHeader.szModuleName, "%s", GetModuleName(nModuleID));
					rptHeader.bAutoProcess = bAutoProcess;
				
					int offset = 0;
					memcpy(lpRxBuffer, &rptHeader, nDataHeaderSize);
					offset += nDataHeaderSize;
					memcpy(LPVOID((char *)lpRxBuffer+offset), &rStepResult.gpStateSave, nGroupStateSize);
					offset += nGroupStateSize;
					memcpy(LPVOID((char *)lpRxBuffer+offset), &stepData, nStepConSize);
					offset += nStepConSize;
					memcpy(LPVOID((char *)lpRxBuffer+offset), lpSaveChData, nChSize);

					offset = ::dcSendData(DB_CMD_STEP_RESULT, lpRxBuffer, nBuffSize);
					if(offset != EP_ACK)
					{
						sprintf(msg, TEXT_LANG[4], ::GetModuleName(nModuleID), pTrayInfo->GetTrayNo(),  nStepIndex+1, offset);//"%s tray [%s] data base server에 step %d result data 전송 실패!!! (Code %d)"
					}
					else
					{
						sprintf(msg, TEXT_LANG[5], ::GetModuleName(nModuleID), pTrayInfo->GetTrayNo(),  nStepIndex+1);//"%s tray [%s] data base server에 step %d result data 전송"
					}
					pDoc->WriteLog(msg);
				}				
			}

			//20201013KSJ
			delete[] lpRxBuffer;	lpRxBuffer = NULL;
			delete[] lpSaveChData;	lpSaveChData = NULL;

			//여러 Step 중간에 사용자가 Stop을 누르게 되면 공정을 처음부터 다시 시작 하도록 한다.(Tray에 현재 공정 완료를 기록하지 않는다.)
			//자동 공정에서 시험의 마지막이면 현재 Tray의 상태를 Update 시킨다.
			if(bDBReport && nStepType == EP_TYPE_END && bAutoProcess == TRUE)	//Test End Data
			{
				if(pDoc->UpdateTrayTestData(nModuleID, t) == FALSE)		//최종 Tray의 결과 Data를 DataBase 에 전송
				{
					sprintf(msg, "Tray [%s] test data update fail.", pTrayInfo->GetTrayNo());
					pDoc->WriteLog(msg);
				}
			}

			//2006/02/07 DownData에 실시간 저장파일을 자동 DownLoad하도록 Message 전송 
			//사용자 Step에대한 정보 저장 필요 
			if(nStepType == EP_TYPE_END && AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "DataSave", 0))
			{
//				if(pDoc->DownLoadProfileData(nModuleID, TRUE) == FALSE)
				if(pDoc->DownLoadProfileData(nModuleID, pTrayInfo->m_strFileName, false) == FALSE)
				{
					sprintf(msg, "%s real time data down load fail!!", ::GetModuleName(nModuleID));
					pDoc->WriteLog(msg);
				}
			}
			
			//20130910 Procision 데이터 저장			
			if(nStepType == EP_TYPE_END && AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "UsePrecisionData", 0))
			{
				if(pDoc->Fun_UpdatePrecisionData(nModuleID) == FALSE)
				{
					sprintf(msg, "%s precision data save failed!", ::GetModuleName(nModuleID));
					pDoc->WriteLog(msg);
				}
			}
			
			//2006/02/07 DownData에 실시간 저장파일을 자동 DownLoad하도록 Message 전송 
			//사용자 Step에대한 정보 저장 필요 
			if(nStepType == EP_TYPE_END && AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoExcelConvert", 0))
			{
				if(pDoc->ConvertToExcel(pTrayInfo->m_strFileName) == FALSE)
				{
					sprintf(msg, "%s result file auto converting fail!!", ::GetModuleName(nModuleID));
					pDoc->WriteLog(msg);
				}
			}

			if(nStepType == EP_TYPE_END && AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoRestoreFmtData", 0))
			{
				if(pModule->m_resultFile.ReadFile(pTrayInfo->m_strFileName, 0) == 1)
				{
					STR_STEP_RESULT	*lpStepData1 = NULL;			

					int nStepCnt = pModule->m_resultFile.GetTestCondition()->GetTotalStepNo();

					INT nStepIndex = 0;
					BOOL bCheckfileloss = FALSE;
					for( nStepIndex=0; nStepIndex < nStepCnt; nStepIndex++)
					{
						lpStepData1 = pModule->m_resultFile.GetStepData(nStepIndex);
						if( lpStepData1 == NULL )
						{
							bCheckfileloss = TRUE;
							break;
						}
						//온도데이터 손실 확인 
						if( lpStepData1->stepCondition.fRecVIGetTime != 0)
						{
							STR_SAVE_CH_DATA *lpChannel = (STR_SAVE_CH_DATA *)lpStepData1->aChData[0];
							if(lpChannel->fJigTemp[0] == 0)
							{
								bCheckfileloss = TRUE;	//온도값 저장 안됬을때
								break;
							}
						}
					}

					if( bCheckfileloss == TRUE )
					{
						// 1. 손실 복구 실행
						// 2. 결과 파일 다시 열기
						if( pModule->RestoreLostData_new() == TRUE )
						{
							sprintf(msg, "[%s] Result file restore success!", ::GetModuleName(nModuleID));
							pDoc->WriteLog(msg);
						}
						else
						{
							sprintf(msg, "[%s] Result file restore failed!", ::GetModuleName(nModuleID));
							pDoc->WriteLog(msg);
						}						
					}
				}
			}			
		}
	}
	delete[] lpchData;		lpchData = NULL;

	//2006/03/22 KBH
	//완료된 Step의 결과 data를 표시할 수 있도록 최종 data file을 Loading 한다.
	//파일 결과가 길면 Loading하는데 오래 걸리므로 수정 필요
//	pModule->LoadResultData();
	pModule->UpdateLoadResultData();

	//다음 Step 시작 시간을 갱신 한다.
	pModule->UpdateStepStartTime();
	
	// 1. EMG 상황의 경우 결과데이터에 _EMG 를 붙인 결과데이터를 생성한다.
	if( pModule->GetTrayInfo(0)->m_nResultFileType != EP_RESULTFILE_TYPE_NORMAL )	
	{
		if( pModule->GetTrayInfo(0)->m_nResultFileType != EP_RESULTFILE_TYPE_CONTACT )
		{	
			if( !strResultData.IsEmpty() )
			{
				char	drive[_MAX_PATH]; 
				char	dir[_MAX_PATH]; 
				char	fname[_MAX_PATH]; 
				char	ext[_MAX_PATH]; 
				_splitpath((LPSTR)(LPCTSTR)strResultData, drive, dir, fname, ext);	

				// 1. EMG, STOP END 를 저장한 원본 파일을 형식에 맞게 변환하고	
				switch( pModule->GetTrayInfo(0)->m_nResultFileType )
				{
				case EP_RESULTFILE_TYPE_EMG:
					{
						strDescFileName.Format("%s\\%s\\%s_EMG.fmt", drive, dir, fname);		
						_unlink(strDescFileName);												
					}
					break;

				case EP_RESULTFILE_TYPE_STP:
					{
						strDescFileName.Format("%s\\%s\\%s_STP.fmt", drive, dir, fname);		
						_unlink(strDescFileName);												
					}
					break;	

				case EP_RESULTFILE_TYPE_RES:
					{
						strDescFileName.Format("%s\\%s\\%s_RES.fmt", drive, dir, fname);		
						_unlink(strDescFileName);												
					}		
				}

				CopyFile( strResultData, strDescFileName, false );
			}
		}		
	}
	
	return 1;
}

void CMainFrame::OnModuleSetting() 
{
	// TODO: Add your command handler code here
	CModuleSetDlg *pDlg = new CModuleSetDlg(this);
	ASSERT(pDlg);

	pDlg->m_pDoc = (CCTSMonDoc *)GetActiveDocument();
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}

void CMainFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	if(!PermissionCheck(PMS_FORM_CLOSE))	
	{
		AfxMessageBox(TEXT_LANG[40]+" [Exit Power Formation]");
		return;	
	}

	int nModuleID = 0;
	int nRunning = 0;
	for(int i =0; i< pDoc->GetInstalledModuleNum(); i++)
	{
		nModuleID = EPGetModuleID(i);
		for(int j = 0; j<EPGetGroupCount(nModuleID); j++)
		{
			if(EPGetGroupState(nModuleID , j) == EP_STATE_RUN)
			{
				nRunning++;
			}
		}
	}
	
	CString strTemp;
	if(nRunning > 0)
	{
		strTemp.Format("%s [%d %s is Running...]",  TEXT_LANG[38], nRunning, pDoc->m_strModuleName);
	}
	else
	{
		strTemp = TEXT_LANG[38];
	}

	if(MessageBox(strTemp, "Exit", MB_ICONQUESTION|MB_YESNO) == IDNO)
		return;

	// theApp.m_FMSserver.End();

	pDoc->m_fmst.fnEnd();

	// EPNewServerStop();
	
	CFrameWnd::OnClose();
}

void CMainFrame::OnUpdateModuleSetting(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	//IR/OCV는 사용 불가 
/*	if(((CCTSMonApp *)AfxGetApp())->GetSystemType() == EP_ID_FORM_OP_SYSTEM
		|| ((CCTSMonApp *)AfxGetApp())->GetSystemType() == EP_ID_ALL_SYSTEM)
	{
		pCmdUI->Enable(g_LoginData.nPermission & PMS_MODULE_ADD_DELETE);
	}
	else
		pCmdUI->Enable(FALSE);
*/
}

void CMainFrame::OnUpdateAdministration(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(g_LoginData.nPermission & PMS_USER_SETTING_CHANGE);
}

/*
void CMainFrame::SaveChData(int bSave, UINT nInterval)
{
	KillTimer(EP_TIMER_SAVE_CH_DATA);
	
	if(bSave == TRUE && nInterval > 0)	
	{
		SetTimer(EP_TIMER_SAVE_CH_DATA, nInterval*1000, NULL);
	}
}
*/
void CMainFrame::OnTimer(UINT nIDEvent) 
{
	switch(nIDEvent)
	{
	case TIMER_1SEC_DEFAULT:
		if( m_bSerialConnected == TRUE )
		{	
			if( m_SerialPort.GetThreadAlive() == TRUE )
			{
				TRACE("TRUE");

				char szDelimat[4];
				ZeroMemory( szDelimat, sizeof(szDelimat) );
				szDelimat[0] = 0x51;
				szDelimat[1] = 0x31;
				szDelimat[2] = 0x0D;			
				m_SerialPort.WriteToPort(szDelimat);
			}
			else
			{
				m_bSerialConnected = FALSE;				
			}		
		}
		break;

//	case EP_TIMER_SAVE_CH_DATA:
//		((CCTSMonDoc *)GetActiveDocument())->SaveRealTimeData();
//		break;

	//DataBase 접속을 위한 Timer
	case TIMER_TRY_TO_CONNECT_TO_DBSERVER:
		OnTimerFuncTryToConnectToDBServer();
		break;

	//Cell Check 이후 모듈쪽 상태전이 시간을 대기하기 위한 Timer
	case TIMER_CHECK_RESULT:
		KillTimer(TIMER_CHECK_RESULT);
		if(	((CCTSMonDoc *)GetActiveDocument())->SendContinueCommand(m_nTimerArguModuleID) == FALSE)
		{
			CString strTemp;
			strTemp.Format(TEXT_LANG[6],  ::GetModuleName(m_nTimerArguModuleID), TEXT_LANG[45]);//"%s [작업 계속] %s"
			AfxMessageBox(strTemp, MB_ICONERROR);
		}
		break;

	case TIMER_500MSEC_DEFAULT:
		{
			int i=0;
			WORD state;
			CString strMsg;
			CCTSMonDoc  *pDoc = (CCTSMonDoc *)GetActiveDocument();

			CString strTrayNo1 = _T("");
			CString strTrayNo2 = _T("");

			char msg[128];

			for( i=1; i <= pDoc->GetInstalledModuleNum(); i++ )
			{
				if( nModuleReadyToStart[i] == 1 )
				{
					state = EPGetGroupState(i);
					if( state == EP_STATE_STANDBY || state == EP_STATE_READY )
					{
						nModuleReadyToStart[i] = 0;



						if( pDoc->SendRunCommand(i, 0) > 0 )
						{
							// strMsg.Format("공정 진행 준비 중...\n[%s] 에서 공정이 준비중입니다.", GetModuleName(i));
							// ShowInformation(i, strMsg, INFO_TYPE_NORMAL);

							sprintf(msg, "[%s] Send Run Command! [Standby] => [Running]", ::GetModuleName(i) );
							pDoc->WriteLog(msg);
						}
						else
						{
							strMsg.Format(TEXT_LANG[7]);//"명령을 전송할 수 없는 상태이거나 전송에 실패 하였습니다."
							ShowInformation( i, strMsg, INFO_TYPE_FAULT);
						}
					}
				}
			}			
		}
		break;

	case TIMER_FMS_NET_ON:
		{
			KillTimer(TIMER_FMS_NET_ON);

			CCTSMonDoc  *pDoc = (CCTSMonDoc *)GetActiveDocument();		
			
			CString msg = _T("");		
			
			if( pDoc->m_fmst.fnBegin(((CMainFrame *)AfxGetMainWnd())->GetSafeHwnd(), pDoc->m_apModuleInfo, pDoc->m_nDeviceID ))
			{
				msg.Format("CTSMon FMS Network start succeed.");
			}
			else
			{
				msg.Format("CTSMon FMS Network start failed.");
			}

			pDoc->WriteLog(msg);
			
//			1. 단일 쓰레드로 작동하기에 Module과 통신시 맞지 않음
//			2. IO_DISCONNECT 에 대한 처리가 되어 있지 않음. 1:다 통신에 적합
// 			if(EPNewServerStart(AfxGetMainWnd()->m_hWnd) == FALSE)
// 			{
// 				pDoc->WriteLog("Formation Server Start Fail");
// 			}
		}

		break;
// 20201210 KSCHOI Add Temperature Data CSV File START
	case TIMER_TEMPERATURE_SAVE_FUNCTION:
		{
			ULONG lTemperatureSaveFunctionPeriod = ((CCTSMonDoc*)GetActiveDocument())->GetTemperatureDataSaveFunctionPeriod() * 1000 * 60;
			BOOL bUseTemperatureSaveFunction = ((CCTSMonDoc*)GetActiveDocument())->GetUseTemperatureDataSaveFunction();
			
			if(bUseTemperatureSaveFunction)
			{
				((CCTSMonDoc*)GetActiveDocument())->SetTemperatureDataSaveEvent();
				
				if(m_lTemperatureSaveFunctionPeriod != lTemperatureSaveFunctionPeriod)
				{
					m_lTemperatureSaveFunctionPeriod = lTemperatureSaveFunctionPeriod;
					
					KillTimer(TIMER_TEMPERATURE_SAVE_FUNCTION);
					SetTimer(TIMER_TEMPERATURE_SAVE_FUNCTION, lTemperatureSaveFunctionPeriod, NULL);
				}
			}
		}
		break;
// 20201210 KSCHOI Add Temperature Data CSV File END
	}
	CFrameWnd::OnTimer(nIDEvent);
}

LRESULT CMainFrame::OnDBServerConnected(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	m_bDBSvrConnect = TRUE;
	KillTimer(TIMER_TRY_TO_CONNECT_TO_DBSERVER);

#ifdef USE_STATUS_BAR
	int index = m_wndStatusBar.CommandToIndex(ID_DB_CONNECT_STATE);
	CString str("Server ON");
	if(index >=0 )
	{
		m_wndStatusBar.SetPaneText(index, str);
	}
#endif
	return 1;
}

LRESULT CMainFrame::OnDBServerDisConnected(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	m_bDBSvrConnect = FALSE;
	SetTimer(TIMER_TRY_TO_CONNECT_TO_DBSERVER, 10000, NULL);

#ifdef USE_STATUS_BAR
	int index = m_wndStatusBar.CommandToIndex(ID_DB_CONNECT_STATE);
	CString str("Server OFF");
	if(index >=0 )
	{
		m_wndStatusBar.SetPaneText(index, str);
	}
#endif
	return 1;
}

void CMainFrame::OnConnectDbsvr() 
{
	// TODO: Add your command handler code here
	CString strTemp;
	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION ,"Use Data Server", FALSE))
	{
		if(m_bDBSvrConnect == FALSE)
		{
			strTemp = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION ,"Data Server IP", "");
			UINT lSystemID = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "System ID", 1);
			UINT lSystemType = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "System Type", 1);
			int nRtn = ::dcConnectToServer((LPSTR)(LPCTSTR)strTemp, AfxGetMainWnd()->m_hWnd, MAKELONG(lSystemID, lSystemType));
			if(nRtn < 0)
			{
				CString strTemp;
				strTemp.Format("Client Connecotin Fail. %d", nRtn);
				AfxMessageBox(strTemp);
			}
		}
	}
	else
	{
		if(m_bDBSvrConnect)
		{
			::dcDisConnect();
		}
	}
}

void CMainFrame::OnDisconnectDbsvr() 
{
	if(m_bDBSvrConnect)
	{
		dcDisConnect();
	}
}

void CMainFrame::OnUpdateDisconnectDbsvr(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_bDBSvrConnect);
	
}

void CMainFrame::OnUpdateConnectDbsvr(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_bDBSvrConnect);
}

//Cell Check 결과에 의한 작업자 작업 진행 여부 결정
LRESULT CMainFrame::OnCheckEnded(WPARAM wParam, LPARAM lParam)
{	
// 	int nModuleID = HIWORD(wParam);
// 	int nGroupIndex = LOWORD(wParam);
// 	ASSERT(lParam);
// 	EP_CHECK_RESULT ckResult;
// 	memcpy(&ckResult, (LPVOID)lParam, sizeof(EP_CHECK_RESULT));
// //	TRACE("Check Result 2 : %d, %d\n", ckResult.wErrorChNo, ckResult.wNormalChNo);
// 	CString strTemp;
// 	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
// 	ASSERT(pDoc);
// 	
// //	TRACE("Group State is %d\n",EPGetGroupState(nModuleID, nGroupIndex));
// 	CFormModule *pModule;
// 	pModule = pDoc->GetModuleInfo(nModuleID);
// 	CTestCondition *pTestCon = pModule->GetCondition();
// 	if(pTestCon->GetCheckParam()->compFlag == FALSE)		//Cell Check를 하지 않았으면 이부분이 실행 될수 없다.
// 	{
// 		strTemp.Format("%s is not setting the cell check but receive result", ::GetModuleName(nModuleID));
// 		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
// 		AfxMessageBox(strTemp);
// 		return 0;
// 	}
// 	
// 	int nRtn;
// 	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "AutoCellCheck", FALSE) == TRUE)
// 	{
// 		//시간 지연을 위해 Timer 처리 한다.
// 		m_nTimerArguModuleID = nModuleID;
// 		SetTimer(TIMER_CHECK_RESULT, 3000, NULL);
// /*		if(pDoc->SendContinueCommand(nModuleID, nGroupIndex) == FALSE)
// 		{
// 			strTemp.Format("%s [작업 계속] %s",  ::GetModuleName(nModuleID), ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL));
// 			AfxMessageBox(strTemp);
// 		}
// */		return 1;
// 	}
// 	
// 	//초기 진입 Tray의 입력 전지 수와 Cell Check 결과의 전지 수가 맞지 않으면 경고 표시  
// 	//Tray별 수량 분리
// /*	CString strFailTray;
// 	CString strNormalTray;
// 	CTray *pTray;
// 	for(int t=0; t<pModule->GetTotalJig(); t++)
// 	{
// 		pTray = pModule->GetTrayInfo(t);
// 		if(pTray->IsWorkingTray())
// 		{
// 			int nChCount = pModule->GetChInJig(t);
// 			if(nChCount == pModule->GetCellCountInTray())
// 			{
// 				//정상 수량 계산 
// 				int nNormalCnt = 0;
// 				int nStartIndex = pModule->GetStartChIndex(t);
// 				for(int c = nStartIndex; c<EP_MAX_CH_PER_MD && c<nStartIndex+nChCount; c++)
// 				{
// 					if(ckResult.byNocellCode[c] == EP_CODE_NORMAL)
// 					{
// 						nNormalCnt++;
// 					}
// 				}
// 				if(nNormalCnt != pTray->lInputCellCount)
// 				{
// 					strTemp.Format("%s[%s: %dCells/ %s: %dCells]\n", pTray->GetTrayNo(), ::GetStringTable(IDS_TEXT_CHECK), nNormalCnt, ::GetStringTable(IDS_TEXT_INPUT_CELL), pTray->lInputCellCount);
// 					strFailTray += strTemp;
// 				}	
// 				else
// 				{
// 					strTemp.Format("%s[%d Cells]\n", pTray->GetTrayNo(), nNormalCnt);
// 					strNormalTray += strTemp;
// 				}
// 			}
// 			else
// 			{
// 				//모두 정상 처리함
// 			}
// 		}
// 	}
// 
// //	if( pTray->IsNewProcedureTray()  && pTray->lInputCellCount != ckResult.wNormalChNo)
// 	CString msg;
// 	CString msg2;
// 	msg2.Format(::GetStringTable(IDS_MSG_CONTINUE_CONFIRM), ::GetModuleName(nModuleID, nGroupIndex));
// 	if(strFailTray.IsEmpty() == FALSE)
// 	{
// 		msg.Format(::GetStringTable(IDS_MSG_CELL_CHECK_RESULT_CONFIRM), ::GetModuleName(nModuleID, nGroupIndex));
// 		strTemp.Format("%s\n\n%s\n\n%s", msg, strFailTray, msg2	);
// 		nRtn = MessageBox(strTemp, "Cell check result", MB_ICONSTOP|MB_YESNOCANCEL);
// 	}
// 	else
// 	{
// 		msg.Format(::GetStringTable(IDS_MSG_CELL_CHECK_RESULT_OK), ::GetModuleName(nModuleID, nGroupIndex));
// 		strTemp.Format("%s\n\n%s\n\n%s", msg, strNormalTray, msg2);
// 		nRtn = MessageBox(strTemp, "Cell check result", MB_ICONQUESTION|MB_YESNOCANCEL);
// 	}
// 
// 	
// 	CString strFailTray;
// 	CString strNormalTray;
// 	CTray *pTray;
// 	for(int t=0; t<pModule->GetTotalJig(); t++)
// 	{
// 		pTray = pModule->GetTrayInfo(t);
// 		if(pTray->IsWorkingTray())
// 		{
// 			int nChCount = pModule->GetChInJig(t);
// 			if(nChCount == pModule->GetCellCountInTray())
// 			{
// 				//정상 수량 계산 
// 				int nNormalCnt = 0;
// 				int nStartIndex = pModule->GetStartChIndex(t);
// 				for(int c = nStartIndex; c<EP_MAX_CH_PER_MD && c<nStartIndex+nChCount; c++)
// 				{
// 					if(ckResult.byNocellCode[c] == EP_CODE_NORMAL)
// 					{
// 						nNormalCnt++;
// 					}
// 				}
// 
// 				if(nNormalCnt != pTray->lInputCellCount)
// 				{
// 					strFailTray = "이상";
// 				}	
// 				else
// 				{
// 					strFailTray = "정상";
// 				}
// 				
// 				strTemp.Format("%s %s [%s: %dCells/ %s: %dCells]\n", pTray->GetTrayNo(), strFailTray, ::GetStringTable(IDS_TEXT_CHECK), nNormalCnt, ::GetStringTable(IDS_TEXT_INPUT_CELL), pTray->lInputCellCount);
// 				strNormalTray += strTemp;
// 			}
// 			else
// 			{
// 				//모두 정상 처리함
// 			}
// 		}
// 	}
// 
// 	CString msg;
// 	CString msg2;
// 	msg2.Format(::GetStringTable(IDS_MSG_CONTINUE_CONFIRM), ::GetModuleName(nModuleID, nGroupIndex));
// 	msg.Format(::GetStringTable(IDS_MSG_CELL_CHECK_RESULT_OK), ::GetModuleName(nModuleID, nGroupIndex));
// 	strTemp.Format("%s\n\n%s\n\n%s", msg, strNormalTray, msg2);
// 
// 	ContactCheckResultDlg dlg( (CCTSMonDoc *)GetActiveDocument() );
// 	dlg.m_nModuleID = nModuleID;
// 	dlg.m_nGroupIndex = nGroupIndex;
// 	dlg.m_strResultMsg = strTemp;
// 	dlg.DoModal();
	return 1;
}

//모듈로 부터 Barcode가 Scan되어 전송도어 옴  
LRESULT CMainFrame::OnBarCodeReceive(WPARAM wParam, LPARAM lParam)
{
//	int nModuleID = HIWORD(wParam);
	CCTSMonDoc  *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);
	
	int nModuleID = HIWORD(wParam);
	int nJigNo = LOWORD(wParam);

	CString strTemp;
	strTemp.Format("%s :: OnBarCodeReceive cmd recieved", ::GetModuleName(nModuleID) );
	pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);	

	ASSERT(lParam);
	EP_TAG_INFOMATION	tagData;
	memcpy(&tagData, (LPVOID)lParam, sizeof(EP_TAG_INFOMATION));	
		
	if(pDoc->GetLineMode(nModuleID) == EP_OFFLINE_MODE)
	{
		CString strBCR(tagData.szTagID);
		BCRScaned(strBCR);
	}
	else
	{
		SendMessage(WM_BCR_READED, 
				MAKELONG(0, nModuleID), (LPARAM)&tagData);			//Send to Parent Wnd to Module State change
	}

	return 1;
}

//작업할 Tray번호와 위치가 결정되어 옴 
LRESULT CMainFrame::OnTraySerialReceive(WPARAM wParam, LPARAM lParam)
{
	ASSERT(lParam);

//	if(m_bWorking)		return;	//중복 실행 방지
////////////////////////////////////////////
	int nModuleID = HIWORD(wParam);
//	int nGroupIndex = LOWORD(wParam);
	int nJigNo = LOWORD(wParam);
	if(nJigNo < 1)	nJigNo = 1;

	CString strTemp;
	CTray	*pTrayData;
	
	CCTSMonDoc  *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == NULL)	 return FALSE;

	EP_TAG_INFOMATION	tagData;
	memcpy(&tagData, (LPVOID)lParam, sizeof(EP_TAG_INFOMATION));

	tagData.szTagID[EP_TRAY_NAME_LENGTH] = '\0';
	strTemp.Format("%s :: Tray ID [%s] Loaded", ::GetModuleName(nModuleID), tagData.szTagID);
	pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);

	pTrayData = pModule->GetTrayInfo(nJigNo-1);
	if(pTrayData == NULL)	return FALSE;

	//On Line Mode 일때 Tray ID가 전송 되어 온다. 
	//타사 Host에의해 Control 되고 있을 경우는 Tray 정보를 Display한다.
	if( pModule->GetLineMode() == EP_ONLINE_MODE )
	{
		pTrayData->SetTrayNo(tagData.szTagID);
		return 1;
	}
		
	WORD state = pModule->GetState();
	if(state != EP_STATE_IDLE && state != EP_STATE_READY && state != EP_STATE_STANDBY && state != EP_STATE_END)
	{
		return 1;
	}

	//실패시
	/////////////////////////////////////////////////////
	if(strcmp(tagData.szTagID, TRAY_RECT_FAIL_CODE) == 0)
	{
		UserInputTrayNo(nModuleID);		
		return 1;
	}
	
	//////////////////////////////////////////	
	//자동 공정 여부 
	BOOL bAuto = EPGetAutoProcess(nModuleID);
	if(bAuto == FALSE)
	{
		pDoc->SendTrayReady(nModuleID, nJigNo, TRUE);
		pTrayData->SetBcrRead();
		pTrayData->SetTrayNo(tagData.szTagID);
	}
	else
	{
		//자동 공정일 경우 
		//Tray 등록 여부 검사
		CTray scandedTray;
		if(pDoc->TrayRegCheck(nModuleID, scandedTray, tagData.szTagID) == FALSE)
		{
			return 0;				//Tray 등록 취소 
		}
		
		//////////////////////////////////////////////////////////////////////////
		int nRtn = pDoc->FindJigNoTrayNo(nModuleID, tagData.szTagID);
		if(nRtn  > 0 && nJigNo != nRtn)
		{
			strTemp.Format(TEXT_LANG[8],//"[%s] Tray는 %s의 Jig %d에 이미 투입되었습니다."
							tagData.szTagID, GetModuleName(nModuleID), nRtn);
			AfxMessageBox(strTemp);
			return 0;
		}

		//Tray를 받아 들일수 있는지 확인(최종 검사는 Run 명령 전송시 다시한다.)
		if(pModule->GetTestReserved())
		{
			//예약된 공정이 있으면 진입 Tray가 시행할 수 있는지 확인 
			if(pDoc->IsExeReservedTestTray(nModuleID) == FALSE)
			{
				strTemp.Format(TEXT_LANG[9],//"[%s]는 예약된 공정[%s]를 시행할수 없는 Tray입니다.(Tray 공정 정보를 확인 하십시요.)"
								tagData.szTagID, pModule->m_ReservedtestInfo.szName);
				AfxMessageBox(strTemp);
				return 0;
			}
		}
		else
		{	
			//같은 공정을 진행할 Tray들인지 검사 
			long bProcID = pDoc->CheckSameProcAndFindNextProc(nModuleID, nJigNo, tagData.szTagID);			
			if(bProcID < 0)
			{
				AfxMessageBox(pDoc->GetLastErrorString());
				return 0;
			}
			else
			{
				STR_CONDITION_HEADER testHeader = pDoc->GetTestData(bProcID);
				if(testHeader.lID > 0)		
				{
					if(pDoc->CheckProcAndSystemType(testHeader.lType) == FALSE)
					{
						strTemp.Format(TEXT_LANG[10], tagData.szTagID, testHeader.szName, pDoc->GetLastErrorString()); //"[%s]는 진행가능한 Tray가 아닙니다. (진행 예정 공정 : %s)\n%s"
						AfxMessageBox(strTemp);
						return 0;	
					}

					//장비 종류만 확인 (ex.Formation인데 Aging으로 진입)
					//TestType Tabel에서 검색
/*					CString str;
					str.Format("SELECT TestTypeName, ExeStep FROM TestType WHERE TestType = %d", testHeader.lType);
					
					CDaoDatabase  db;
					try
					{
						db.Open(::GetDataBaseName());
						CDaoRecordset rs(&db);
						rs.Open(dbOpenSnapshot, str, dbReadOnly);	
					
						str.Empty();
						if(!rs.IsBOF() && !rs.IsEOF())
						{
							COleVariant data;
							data = rs.GetFieldValue(0);
							data = rs.GetFieldValue(1);
							str = data.pcVal;
						}
						rs.Close();
						db.Close();
					}
					catch (CDaoException* e)
					{
						e->Delete();
					}	
					if(str.GetLength() > 2)
					{
						int nSysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();
						if(str.Left(2) == EP_PGS_AGING_CODE)	//Aging 장비 
						{
							if(nSysType != EP_ID_AGIGN_SYSTEM)
							{
								strTemp.Format("[%s]는 Aging System에서 진행할 Tray가 아닙니다. 진행 예정 공정 : %s", tagData.szTagID, testHeader.szName);
								AfxMessageBox(strTemp);
								return FALSE;
							}
						}
						else if(str.Left(2) == EP_PGS_PREIMPEDANCE_CODE || str.Left(2) == EP_PGS_IROCV_CODE || str.Left(2) == EP_PGS_OCV_CODE )			//IROCV 측정기 
						{
							if(nSysType != EP_ID_IROCV_SYSTEM)
							{
								strTemp.Format("[%s]는 IR/OCV System에서 진행할 Tray가 아닙니다. 진행 예정 공정 : %s", tagData.szTagID, testHeader.szName);
								AfxMessageBox(strTemp);
								return FALSE;
							}
						}
						else if(str.Left(2) == EP_PGS_GRADING_CODE || str.Left(2) == EP_PGS_SELECTOR_CODE)								//Selecting 설비 
						{
							if(nSysType != EP_ID_GRADING_SYSTEM)
							{
								strTemp.Format("[%s]는 Grading/Selecting System에서 진행할 Tray가 아닙니다. 진행 예정 공정 : %s", tagData.szTagID, testHeader.szName);
								AfxMessageBox(strTemp);
								return FALSE;
							}
						}
						else //if(strProcess == "PC" || strProcess == "FO" || strProcess == "FC" )	//충방전기 설비 
						{
							if(nSysType != EP_ID_FORM_OP_SYSTEM)
							{
								strTemp.Format("[%s]는 Formation System에서 진행할 Tray가 아닙니다. 진행 예정 공정 : %s", tagData.szTagID, testHeader.szName);
								AfxMessageBox(strTemp);
								return FALSE;
							}
						}
					}
*/
				}	
				//else {}	예약 공정이 없는 Tray이므로 진입 가능 
			}
		}

		//새로운 Tray가 읽힘 
		//이전 Lot Data??
		if(pTrayData->LoadTrayData(tagData.szTagID, FALSE))		//tray의 등록정보만 Loading 한다.	//pTrayData->strTrayNo = tempData.szTrayNo;
		{
			pTrayData->SetBcrRead();
			pDoc->SendTrayReady(nModuleID, nJigNo, TRUE);
		}
		else
		{
			strTemp.Format(TEXT_LANG[11], tagData.szTagID); //"[%s] 등록 정보를 찾을 수 없습니다."
			AfxMessageBox(strTemp);
			return 0;
		}
	}

	//Set Display module to current module
	if(m_pTopView)		m_pTopView->SetCurrentModule(nModuleID);
	// if(m_pAgingView)	m_pAgingView->SetCurrentModule(nModuleID);

	//1개씩 전송이면 작업을 시작 한다.
/*	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "MultiRun", 0) == FALSE)
	{
		pDoc->S endRunCommand(nModuleID, 0, FALSE);	
	}
*/
	return 1;
}

void CMainFrame::OnTrayReg() 
{
	// TODO: Add your command handler code here
	CRegTrayDlg	*pDlg;
	pDlg = new CRegTrayDlg(this);
	ASSERT(pDlg);
	if(pDlg->DoModal() == IDOK)
	{
	}
	delete pDlg;
	pDlg = NULL;
}

void CMainFrame::OnUpdateTrayReg(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//NVRAM 사용시는 Tray가 장착되어야 등록 가능함
#if TRAY_DATA_NVRAM
	pCmdUI->Enable(FALSE);
#endif
}

void CMainFrame::OnTraySerialInit() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	CTrayInputDlg dlg(pDoc,this);
	dlg.SetTitle(TEXT_LANG[12]); //"삭제할 Tray 번호를 입력하십시요."
	if(dlg.DoModal() != IDOK)	return;

	CString str;
	CTray tray;
	if(tray.LoadTrayData(dlg.m_strTrayNo))
	{
		//다음 예약 공정이 있는지 확인 한다.
		STR_CONDITION_HEADER testHeader = ((CCTSMonDoc *)GetActiveDocument())->GetNextTest(tray.GetProcessSeqNo());
		TRACE("Find Next Process ID	: %d\n", testHeader.lID);
		TRACE("Find Next Test Name	: %s\n", testHeader.szName);
		TRACE("Find Next Test		: %s\n", testHeader.szDescription);

		if(testHeader.lID == 0)
		{
			str.Format(TEXT_LANG[13], dlg.m_strTrayNo); //"Tray [%s]을 삭제 하시겠습니까?"
		}
		else
		{
			str.Format(TEXT_LANG[14], //"Tray [%s]를 삭제 하시겠습니까?\n[%s]는 [%s] 공정이 예약되어 있습니다. 삭제하면 이후 공정 예약이 취소 됩니다." 
				dlg.m_strTrayNo, dlg.m_strTrayNo, testHeader.szName);
		}
		if(MessageBox(str, TEXT_LANG[15], MB_ICONQUESTION|MB_YESNO) != IDYES) //"삭제"
			return;
	}
	else
	{
		str.Format(TEXT_LANG[16], dlg.m_strTrayNo); //"Tray [%s] 등록 정보를 찾을 수 없습니다.!!!"
		MessageBox(str, TEXT_LANG[17], MB_ICONSTOP|MB_OK); //"실패"
		return;
	}

	if(tray.DeleteTray(dlg.m_strTrayNo) == FALSE)
	{
		str.Format(TEXT_LANG[18], dlg.m_strTrayNo); //"Tray [%s] 등록 삭제 실패!!!"
		MessageBox(str, TEXT_LANG[17], MB_ICONSTOP|MB_OK);
	}

	str.Format(TEXT_LANG[19], dlg.m_strTrayNo);//"Tray [%s] 등록 삭제됨"
	((CCTSMonDoc *)GetActiveDocument())->WriteLog(str);
}

void CMainFrame::OnUpdateTraySerialInit(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//NVRAM 사용시는 Tray가 장착되어야 등록및 삭제가 가능함
#if TRAY_DATA_NVRAM
	pCmdUI->Enable(FALSE);
#endif

}


void CMainFrame::OnSensorDataView() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	if(m_pSensorDlg != NULL)
	{		
		delete m_pSensorDlg;
		m_pSensorDlg = NULL;
	}

	m_pSensorDlg = new CSensorDataDlg(pDoc, this);
	ASSERT(m_pSensorDlg);		

	m_pSensorDlg->m_nInstallModuleNum = pDoc->GetInstalledModuleNum();
	m_pSensorDlg->Create(IDD_SENSOR_DATA_DLG, GetDesktopWindow());
	m_pSensorDlg->ShowWindow(SW_SHOW);
}

void CMainFrame::OnFanModeSetting() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	if(m_pFanModeSettingDlg != NULL)
	{		
		delete m_pFanModeSettingDlg;
		m_pSensorDlg = NULL;
	}

	m_pFanModeSettingDlg = new FanModeSettingDlg(pDoc, this, m_pAllSystemView->GetCurModuleID());
	//ASSERT(m_pSensorDlg);		

	m_pFanModeSettingDlg->Create(IDD_FAN_MODE_SETTING, GetDesktopWindow());
	m_pFanModeSettingDlg->ShowWindow(SW_SHOW);
}

void CMainFrame::OnUpdateSensorDataView(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	DWORD sysType = ((CCTSMonApp *)AfxGetApp())->GetSystemType();
	if(sysType == EP_ID_FORM_OP_SYSTEM || sysType == EP_ID_AGIGN_SYSTEM || sysType == EP_ID_ALL_SYSTEM)
	{
		if( m_pSensorDlg != NULL )
		{
			if( m_pSensorDlg->IsWindowVisible() )
			{
				
			}
			else
			{
				pCmdUI->Enable(TRUE);
				return;
			}
		}
		else
		{
			pCmdUI->Enable(TRUE);
			return;
		}
	}
	
	pCmdUI->Enable(FALSE);
}

LRESULT CMainFrame::OnFmsErrorInfo(WPARAM wParam, LPARAM lParam)
{
	int nModuleID = wParam;
	int nErrorInfo = lParam;
	
	CString strErrMsg, strLog, strModuleName;
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);	

	CString strStageName = GetModuleName(nModuleID);
	strModuleName.Format("Stage : %s", strStageName);

	switch( nErrorInfo )
	{
	case 0:
		{
			strErrMsg.Format(TEXT_LANG[20]); //"충방전기 결과데이터 MES 전송 에러 발생"
		}
		break;

	default:
		{
			strErrMsg.Format(TEXT_LANG[21], nErrorInfo); //"MES Alaram Report [%d]"
		}
	}
	
	
	strLog.Format("%s,%s", strModuleName, strErrMsg);	

	pDoc->WriteEMGLog(strLog);


	//20200430 ksj
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);

// 20200713 KSCHOI S5F101 Bug Fixed StageID to StageIdx START
	if(pModule == NULL)
	{
		return 1;
	}
// 20200713 KSCHOI S5F101 Bug Fixed StageID to StageIdx END

	int nOperationMode = pModule->GetOperationMode();

	pDoc->SaveEmgToProductsMDB(nModuleID, nOperationMode, "", strErrMsg, pModule->GetTrayNo(), 0 );


// 	// 1. 통합모니터링에 EMG 신호를 보내는 부분
// 	if( pDoc->m_bUsePneMonitoringSystem == true )
// 	{
// 		CString strQuery = _T("");
// 		COleDateTime oleCurDate(COleDateTime::GetCurrentTime());
// 
// 		strQuery.Format("INSERT INTO tbl_EMG (Device, Op_Mode, unitnum, detail, updatetime) VALUES (%d,%d,%d,'%s','%s')", 
// 			pDoc->m_nDeviceID, nOperationMode, nModuleID, strDescription, oleCurDate.Format("%y/%m/%d %H:%M:%S") );
// 
// 		mainPneApp.mesData.AddTail(strQuery);
// 	}

	ShowEmgDlg *pEmgDlg;
	bool	bFindDlg = false;
	if (m_apHSMSEmgDlg.GetSize() > 0)
	{
		int i = 0;
		for ( i=0 ; i< m_apHSMSEmgDlg.GetSize() ; i++ )
		{
			pEmgDlg = (ShowEmgDlg *)m_apHSMSEmgDlg[i];

			if (pEmgDlg->GetModuleID() == nModuleID)
			{
				if (pEmgDlg->IsWindowVisible())
				{
					bFindDlg = true;
					pEmgDlg->SetModuleID(nModuleID);
					pEmgDlg->SetEmgData(strModuleName, strErrMsg, 0);					
				}
				else
				{
					bFindDlg = true;
					pEmgDlg->SetModuleID(nModuleID);
					pEmgDlg->SetEmgData(strModuleName, strErrMsg, 0);
					pEmgDlg->ShowWindow(SW_SHOW);				
				}			 
			}
		}
	}

	if( bFindDlg == false )
	{
		pEmgDlg = new ShowEmgDlg(this);
		ASSERT(pEmgDlg);
		pEmgDlg->Create(IDD_SHOW_EMG_DLG, this);
		pEmgDlg->SetModuleID(nModuleID);
		pEmgDlg->SetEmgData(strModuleName, strErrMsg, 0);
		pEmgDlg->ShowWindow(SW_SHOW);
		m_apHSMSEmgDlg.Add(pEmgDlg);	
	}

	if( pDoc->m_bUseFaultAlarm )
	{
		if( !m_bAlarmPlaying )
		{
			m_bAlarmPlaying = TRUE;
			PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL , SND_ASYNC|SND_LOOP|SND_RESOURCE);		
		}
	}

	return 1;
}

LRESULT CMainFrame::OnModuleEmg(WPARAM wParam, LPARAM lParam)
{
	int nModuleID = HIWORD(wParam);
	//	int nModuleIndex = EPGetModuleIndex(nModuleID);
	int nGroupIndex = LOWORD(wParam);
	ASSERT(lParam);
	EP_CODE codeData;
	memcpy(&codeData, (LPVOID)lParam, sizeof(EP_CODE));
	
	CString strTemp, strMsg, strLog;
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);	
	
	if(m_pAllSystemView)	m_pAllSystemView->UpdateGroupState(nModuleID, nGroupIndex);
	if(m_pTopView)			m_pTopView->UpdateGroupState(nModuleID, nGroupIndex);
	//	if(m_pConditionView)	m_pConditionView->UpdateTreeState(nModuleID, nGroupIndex);
	
	CString strCode, strDescription;	

	// 일반적임 EMG 신호
	if( pDoc->GetModuleTroubeMsg(codeData.nCode, strCode, strDescription) == true )
	{
		strMsg.Format("%s, %s", strCode, strDescription);
		strTemp.Format("[%s] %s\n%s", ::GetModuleName(nModuleID), TEXT_LANG[46], strMsg);		
		strLog.Format("%s,%s", ::GetModuleName(nModuleID), strMsg);
	}
	else
	{
		strMsg.Format("%d, NoDefine", codeData.nData);
		strTemp.Format("[%s] %s\n%s", ::GetModuleName(nModuleID), TEXT_LANG[46], strMsg);				
		strLog.Format("%s,%s", ::GetModuleName(nModuleID), strMsg);
	}

// 20201105 KSCHOI CU08~CU10 EMG Code Display By KSJ START
// 	if( pDoc->GetModuleTroubeMsg(codeData.nCode, strCode, strDescription) == true )
// 	{
// 		if(codeData.nCode == 81 || codeData.nCode == 151 || codeData.nCode == 173 || codeData.nCode == 174 || codeData.nCode == 175 || codeData.nCode == 176 || codeData.nCode == 177 ||codeData.nCode == 178 ||codeData.nCode == 179 || codeData.nCode == 180)
// 		{
// 			if( codeData.nData == 0)
// 			{
// 				strMsg.Format("%s(Num : 1), %s", strCode, strDescription);
// 			}
// 			if( codeData.nData == 1)
// 			{
// 				strMsg.Format("%s(Num : 2), %s", strCode, strDescription);
// 			}
// 			if( codeData.nData == 2)
// 			{
// 				strMsg.Format("%s(Num : 3), %s", strCode, strDescription);
// 			}			
// 		}
// 		strTemp.Format("[%s] %s\n%s", ::GetModuleName(nModuleID), ::GetStringTable(IDS_TEXT_EMG_DETECT), strMsg);		
// 		strLog.Format("%s,%s", ::GetModuleName(nModuleID), strMsg);
// 	}
	if( pDoc->GetModuleTroubeMsg(codeData.nCode, strCode, strDescription) == true )
	{
		if(codeData.nCode == 81 || codeData.nCode == 151 || codeData.nCode == 173 || codeData.nCode == 174 || 
			codeData.nCode == 175 || codeData.nCode == 176 || codeData.nCode == 177 ||codeData.nCode == 178 ||
			codeData.nCode == 179 || codeData.nCode == 180 || codeData.nCode == 21 || codeData.nCode == 22 || codeData.nCode == 23)
		{
			strMsg.Format("%s(Num : %d), %s", strCode, codeData.nData, strDescription);			// 2021-03-05 BJY SBC요청으로 기존 codeData.nData+1에서 +1 삭제
			strDescription.Format("%s (Num : %d)", strDescription, codeData.nData);				// 2021-03-05 BJY SBC요청으로 기존 codeData.nData+1에서 +1 삭제
		}
		strTemp.Format("[%s] %s\n%s", ::GetModuleName(nModuleID), ::GetStringTable(IDS_TEXT_EMG_DETECT), strMsg);      
		strLog.Format("%s,%s", ::GetModuleName(nModuleID), strMsg);
	}
// 20201105 KSCHOI CU08~CU10 EMG Code Display By KSJ END

	pDoc->WriteEMGLog(strLog);	
	
	// 1. Module 에서 EMG 신호를 백업 받기위해 설정	
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);

	//if(codeData.nCode == 142) //20200411 엄륭 화재감지 관련
	//{
	//	m_TempErrorOn = TRUE;
	//}
	//if(codeData.nCode == 120 || codeData.nCode == 132 ) //20200411 엄륭 화재감지 관련
	//{
	//	m_SmokeErrorOn = TRUE;
	//}

	//if(m_TempErrorOn == TRUE && m_SmokeErrorOn == TRUE) //20200411 엄륭 화재감지 관련
	//{
	//	m_TempErrorOn = FALSE;
	//	m_SmokeErrorOn = FALSE;
	//	m_ErrDlgVislble = TRUE;
	//}
	// 1. 온도관련 에러일 경우 센서값을 로그 파일에 남긴다.
	if( codeData.nCode == 142 || codeData.nCode == 145 )
	{
		_MAPPING_DATA	*pMapData;
		EP_GP_DATA gpData = EPGetGroupData(nModuleID, 0);

		strLog.Format("[%s] ", ::GetModuleName(nModuleID));
		
		for( int i=0; i<MAX_USE_TEMP; i++ )				// 총 Sensor값은 16개
		{
			pMapData = pModule->GetSensorMap(0, i);	
			if( pMapData->nChannelNo == 1 || pMapData->nChannelNo == 0 )
			{
				strTemp.Format("CH:%d(%.2f) ", i+1, (float)gpData.sensorData.sensorData1[i].lData/100.0f);

				strLog += strTemp;
			}
		}

		pDoc->WriteLog(strLog);
	}
	
	// Emg 신호 추가 관련 1. TrayNo, 2. Type 정보 추가
	CTestCondition *pTestConditon = pModule->GetCondition();
	int nType = 0;
	if( pTestConditon != NULL )
	{
		nType = pTestConditon->GetModelInfo()->lID;	
	}

	int nOperationMode = pModule->GetOperationMode();
	if( EPGetAutoProcess(nModuleID) == TRUE )
	{
		nOperationMode = EP_OPERATION_SAFETYLOCAL;
	}
	
	pDoc->SaveEmgToProductsMDB(nModuleID, nOperationMode, strCode, strDescription, pModule->GetTrayNo(), nType );
	
	if( EPGetAutoProcess(nModuleID) == TRUE
		|| pModule->GetOperationMode() == EP_OPEARTION_MAINTENANCE )
	{
		// FMS로 보고 안함.
	}
	else
	{
		pDoc->m_fmst.fnSetEMG(nModuleID, strCode, strDescription);
	}	
		
	// 1. 통합모니터링에 EMG 신호를 보내는 부분

	if( pDoc->m_bUsePneMonitoringSystem == true )
	{
		CString strQuery = _T("");
		COleDateTime oleCurDate(COleDateTime::GetCurrentTime());

		strQuery.Format("INSERT INTO tbl_EMG (Device, Op_Mode, unitnum, detail, updatetime) VALUES (%d,%d,%d,'%s','%s')", 
			pDoc->m_nDeviceID, nOperationMode, nModuleID + pDoc->m_nUnitIdx, strDescription, oleCurDate.Format("%y/%m/%d %H:%M:%S") );

		mainPneApp.mesData.AddTail(strQuery);
	}
	
	// 1. ContactCheck EMG 의 경우 EMG 파일을 만들지 않는다.
	if( strCode == "53" )
	{	
		// 1. Contact Error 발생
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_CONTACT;
		strLog.Format(TEXT_LANG[22], ::GetModuleName(nModuleID)); //"[%s] Contact Error 감지 후 FileType 변경( CONTACT )"
		pDoc->WriteLog(strLog);
	}
	else if( strCode == "01" || strCode == "02" )
	{	
		// 1. 정전 발생시
		strLog.Format(TEXT_LANG[23], ::GetModuleName(nModuleID)); //"[%s] 정전 감지 후 FileType 변경( RES )"
		pDoc->WriteLog(strLog);
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_RES;
	}
	else
	{
		// 1. 일반적인 EMG 발생시
		strLog.Format(TEXT_LANG[24], ::GetModuleName(nModuleID)); //"[%s] EMG 신호 감지 후 FileType 변경( EMG )"
		pDoc->WriteLog(strLog);
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_EMG;
	}
	
	pModule->UpdateTestLogHeaderTempFile();
	

	ShowEmgDlg *pEmgDlg;
	bool	bFindDlg = false;
	if (m_apEmgDlg.GetSize() > 0)
	{
		int i = 0;
		for ( i=0 ; i< m_apEmgDlg.GetSize() ; i++ )
		{
			pEmgDlg = (ShowEmgDlg *)m_apEmgDlg[i];
			
			if (pEmgDlg->GetModuleID() == nModuleID)
			{
				if (pEmgDlg->IsWindowVisible())
				{
					
				}
				else
				{
					delete pEmgDlg;
					pEmgDlg = NULL;			
					m_apEmgDlg.RemoveAt(i);			
				}			 
			}
		}
	}
	

	//2.새로운 Emg창 정리
	pEmgDlg =  new ShowEmgDlg(this);
	ASSERT(pEmgDlg);
	pEmgDlg->Create(IDD_SHOW_EMG_DLG, this);
	pEmgDlg->SetModuleID(nModuleID);
	pEmgDlg->SetEmgData(::GetModuleName(nModuleID), strMsg, codeData.nData);
	pEmgDlg->ShowWindow(SW_SHOW);
	m_apEmgDlg.Add(pEmgDlg);	




	// 1. Log 상태 갱신
	// 2. PlayAlarm 사운드
	if(m_pAllSystemView)	
	{
		m_pAllSystemView->UpdateEmgLog();
	}
	
	if( pDoc->m_bUseFaultAlarm )
	{
		if( !m_bAlarmPlaying )
		{
			m_bAlarmPlaying = TRUE;
			PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL , SND_ASYNC|SND_LOOP|SND_RESOURCE);		
		}
	}
	
	return 1;
}

VOID CMainFrame::SetAlarmOff()
{
	if( m_bAlarmPlaying == TRUE )
	{
		m_bAlarmPlaying = FALSE;
		PlaySound(NULL, 0, 0);
	}
}

// 1. 알람 테스트용
VOID CMainFrame::SetAlarmOn()
{
	if( !m_bAlarmPlaying )
	{
		m_bAlarmPlaying = TRUE;
		PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL , SND_ASYNC|SND_LOOP|SND_RESOURCE);		
	}	
	else
	{
		m_bAlarmPlaying = FALSE;
		PlaySound(NULL, 0, 0);	
	}
}

BOOL CMainFrame::InitSerialPort()
{
	CString strTemp;
	if(m_bSerialConnected)
	{
		strTemp.Format(TEXT_LANG[25], m_SerialConfig.nPortNum);//COM %d 포트는 이미 연결 되어 있습니다.
		AfxMessageBox(strTemp);
		return FALSE;
	}

	BOOL flag = m_SerialPort.InitPort(this,
				m_SerialConfig.nPortNum, 
				m_SerialConfig.nPortBaudRate,
				m_SerialConfig.portParity,
				m_SerialConfig.nPortDataBits,
				m_SerialConfig.nPortStopBits,
				m_SerialConfig.nPortEvents,
				m_SerialConfig.nPortBuffer
			);

	if( flag == FALSE )
	{
		strTemp.Format(TEXT_LANG[26], m_SerialConfig.nPortNum); //"COM %d 포트 초기화를 실패하였습니다!"
		AfxMessageBox(strTemp);
		return FALSE;
	}

	m_SerialPort.StartMonitoring();
	m_bSerialConnected = TRUE;
	
#ifdef USE_STATUS_BAR
	int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE);
	if(index >=0 )
	{
		m_wndStatusBar.SetPaneText(index, "UPS ON");
	}
#endif

	return TRUE;
}

void CMainFrame::OnSerialConfig() 
{
	// TODO: Add your command handler code here
	CSerialConfigDlg dlg;

	dlg.m_SerialConfig[0] = m_SerialConfig;
	if(dlg.DoModal() == IDOK)
	{
		m_SerialConfig = dlg.m_SerialConfig[0];
		WriteSerialSetting();
		
		BOOL bUsePort1 = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Bar Code Reader", FALSE);

		if(m_bSerialConnected)
		{
			CloseSerialPort();
		}

		if(bUsePort1)
		{
			InitSerialPort();
		}
	}
}

BOOL CMainFrame::CloseSerialPort()
{
	if(m_bSerialConnected == FALSE)		return TRUE;
	m_SerialPort.DisConnect();
	
	m_bSerialConnected = FALSE;

#ifdef USE_STATUS_BAR
	int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE);
	if(index >=0 )
	{
		m_wndStatusBar.SetPaneText(index, "BCR OFF");
	}
#endif

	return TRUE;
}

LRESULT CMainFrame::OnOnlineStepDataReceive(WPARAM wParam, LPARAM lParam)
{
 	int nModuleID = HIWORD(wParam);
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();

	int nPrevStepNo = 0;
	int nTotalStepNo = 0;

	STR_COMMON_STEP *pStepData;
	pStepData = (STR_COMMON_STEP*)lParam;
	
	STR_COMMON_STEP stepData;
	ZeroMemory( &stepData, sizeof(STR_COMMON_STEP));
	memcpy( &stepData, pStepData, sizeof( STR_COMMON_STEP ));
	delete pStepData;

	CStep *pStep;
	
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule)
	{
		CTestCondition *condition = pModule->GetCondition();

		nTotalStepNo = condition->GetTotalStepNo();
		if(nTotalStepNo > 0)
		{
			nPrevStepNo = nTotalStepNo - 1;
			pStep = condition->GetStep(nPrevStepNo);
			if(pStep)
			{
				if( pStep->m_type == EP_TYPE_END || nPrevStepNo == MAX_ONLINE_STEPDATASIZE )
				{
					condition->RemoveStep();
				}
			}
		}

		pStep = new CStep;
		pStep->SetStepData( &stepData );
		condition->AddStep(pStep);

		if(pStep->m_type == EP_TYPE_END)
		{
			if(pModule->WriteTestLogTempFile(condition) == FALSE)	//전송 공정 조건 Log 기록 파일 
			{
			}
		}
	}

	return 1;
}

// [11/24/2009 kky]
// ODBC에 PinError정보를 저장
/*
BOOL CMainFrame::SendDataToODBCForPinLog( int nModuleID, int nChIndex, BYTE channelCode )
{
	CString strDbName;
	CString strFieldCode;
	CString strFieldChIndex;
	CString strFieldCurField;
	int		nFieldCurField = 0;
	CString strModuleName;
	CString strDSN = __T("");
	CString strSQL = __T("");
	CString strTemp = __T("");
	BOOL	bInsertFlagModuleName = FALSE;
	BOOL	bInsertFlagChIndex = FALSE;

	strDbName = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "DatabaseDSN", "CTSProduct");		// DataBase Name
	strDSN.Format( "DSN=%s", strDbName );
	strModuleName = GetModuleName(nModuleID);

	CDatabase db;
	try
	{
		db.OpenEx( DSN_DATABASE_NAME, 0 );
		{
			CRecordset rs(&db);
			strSQL.Format("SELECT CurField FROM PinLog WHERE ModuleName = '%s' AND ChIndex = %d", strModuleName, nChIndex );
			try
			{
				rs.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL, CRecordset::none );
			}
			catch (CFileException* e)
			{
				AfxMessageBox("UpdateCellData MemoryException");				
			}
			catch (CException* e)
			{
				return FALSE;
			}
			
			if( !rs.IsBOF() && !rs.IsEOF() )
			{
				rs.GetFieldValue( (short)0, strFieldCurField ); 
				nFieldCurField = atoi( strFieldCurField );
				bInsertFlagModuleName = FALSE;		// Update
			}
			else
			{
				bInsertFlagModuleName = TRUE;		// Insert
			}

			if( bInsertFlagModuleName )
			{
				// Insert 구문
				strSQL.Format("INSERT INTO PinLog( ModuleName, ChIndex, CurField, Code1 )");
				strTemp.Format("VALUES('%s', %d, %d, %d)", strModuleName, nChIndex, 1, channelCode );
				strSQL = strSQL + strTemp;					
			}
			else
			{
				// Update 구문
				nFieldCurField = nFieldCurField+1;

				if( nFieldCurField > 31 )
				{
					nFieldCurField = 1;
				}				
				strSQL.Format("UPDATE PinLog SET CurField = %d, Code%d = %d ", nFieldCurField, nFieldCurField, channelCode );
				strTemp.Format("WHERE ModuleName = '%s' AND ChIndex = %d ", strModuleName, nChIndex );
				strSQL = strSQL + strTemp;
			}

			try
			{
				db.ExecuteSQL(strSQL);
				return TRUE;
			}
			catch (CMemoryException* e)
			{
				return FALSE;				
			}
			catch (CException* e)
			{
				return FALSE;
			}
		}
		db.Close();
	}
	catch (CFileException* e)
	{
		char szError[255];
		e->GetErrorMessage( szError, 255 );
		e->Delete();
		return FALSE;
	}
	return TRUE;
}
*/

VOID CMainFrame::SendDataToODBCForPinLog( int nModuleID, int nChIndex, BYTE channelCode )
{
	CString strModuleName;
	strModuleName = GetModuleName(nModuleID);

	CDatabase db;
	CDBVariant va;
	
	try
	{
		db.Open(DATABASE_NAME);
		db.BeginTrans();

		CString strSQL;
		long lCnt = 0;
		BOOL bFind = FALSE;
		
		strSQL.Format("SELECT CurField FROM PinLog WHERE ModuleName = '%s' AND ChIndex = %d", strModuleName, nChIndex );
		
		CRecordset rs(&db);
		rs.Open(CRecordset::forwardOnly, strSQL);
		if( !rs.IsEOF() && !rs.IsBOF() )
		{
			rs.GetFieldValue( (short)0, va, SQL_C_SLONG );
			lCnt = va.m_lVal;
			bFind = TRUE;
		}

		if( lCnt >= EP_MAX_PINLOG )
		{
			lCnt = 1;
		}
		else
		{
			lCnt++;
		}
		if(::IsNonCell(channelCode) == FALSE)
		{
			if(bFind)
			{
				strSQL.Format("UPDATE PinLog SET CurField = %d, Code%d = %d WHERE ModuleName = '%s' AND ChIndex = %d", 
					lCnt, lCnt, channelCode, strModuleName, nChIndex);
			}
			else
			{
				strSQL.Format("INSERT INTO PinLog(ModuleName, ChIndex, CurField, Code%d) VALUES ('%s', %d, %d, %d)", 
					lCnt, strModuleName, nChIndex, lCnt, channelCode);
			}
			db.ExecuteSQL(strSQL);
		}		
		db.CommitTrans();
		db.Close();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError);
		e->Delete();     //DataBase Open Fail
	}
}

VOID CMainFrame::SetUPSAlaram()
{
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	int nDeviceId = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "DeviceID", 2);
	int nModuleID = 0;
	
	CString strMsg, strCode;
	strMsg.Format(TEXT_LANG[27], nDeviceId); //"Com PC[%d] UPS Error 발생"

	int nUpsErrorCode = 80;
	// nUpsErrorCode += nDeviceId;

	strCode.Format("%d", nUpsErrorCode);

	pDoc->SaveEmgToProductsMDB(nModuleID, 0, strCode, strMsg, "", 0 );

	pDoc->m_fmst.fnSetLocalEMG(nDeviceId, strCode);

	ShowEmgDlg *pEmgDlg;
	bool	bFindDlg = false;

	CString strLane;
	strLane.Format("LANE %d", nDeviceId );

	if (m_apEmgDlg.GetSize() > 0)
	{
		int i = 0;
		for ( i=0 ; i< m_apEmgDlg.GetSize() ; i++ )
		{
			pEmgDlg = (ShowEmgDlg *)m_apEmgDlg[i];

			if (pEmgDlg->GetModuleID() == nModuleID)
			{
				if (pEmgDlg->IsWindowVisible())
				{
					bFindDlg = true;
					pEmgDlg->SetModuleID(nModuleID);
					pEmgDlg->SetEmgData(strLane, strMsg, 0);					
				}
				else
				{
					bFindDlg = true;
					pEmgDlg->SetModuleID(nModuleID);
					pEmgDlg->SetEmgData(strLane, strMsg, 0);
					pEmgDlg->ShowWindow(SW_SHOW);				
				}			 
			}
		}
	}

	if( bFindDlg == false )
	{
		pEmgDlg =  new ShowEmgDlg(this);
		ASSERT(pEmgDlg);
		pEmgDlg->Create(IDD_SHOW_EMG_DLG, this);
		pEmgDlg->SetModuleID(nModuleID);
		pEmgDlg->SetEmgData(strLane, strMsg, 0);
		pEmgDlg->ShowWindow(SW_SHOW);
		m_apEmgDlg.Add(pEmgDlg);	
	}

	if(m_pAllSystemView)	
	{
		m_pAllSystemView->UpdateEmgLog();
	}

	if( pDoc->m_bUseFaultAlarm )
	{
		if( !m_bAlarmPlaying )
		{
			m_bAlarmPlaying = TRUE;
			PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL , SND_ASYNC|SND_LOOP|SND_RESOURCE);		
		}
	}
}

//Local에서 Barcode가 Scan되어 옴 
LONG CMainFrame::OnCommunication(WPARAM wParam, LPARAM lParam)
{
	if(m_SerialConfig.nPortNum != UINT(lParam))	return 0;

	char data = (char)wParam;

//	TRACE("%c", (char)data);
	
//	if(data == COM_STX)
//	{
//		m_strReceiveBuff.Empty();
//		return 0;
//	}

	if(data != COM_ETX)
	{
		if(data >= 0x20)		m_strReceiveBuff += data;
	}
	else
	{
		CString strData;

		int nSize = m_strReceiveBuff.Find(' ');
		if( nSize > 0 )
		{
			AfxExtractSubString(strData, m_strReceiveBuff, 0,' ');
			
			float fInputVoltage = atof(strData.Right(5));
			if( fInputVoltage == 0 )
			{
				m_nUpsDetectCnt++;

				// 1. 30s 이상인 경우
				if( m_nUpsDetectCnt > 3 )
				{
					if( m_bUpsErrorChk == FALSE )
					{
						m_bUpsErrorChk = TRUE;
						TRACE(TEXT_LANG[28]);//"정전신호 감지\n"
						SetUPSAlaram();
					}

					m_nUpsDetectCnt = 4;
				}
				else
				{
					TRACE(TEXT_LANG[29]);//"순간정전신호 감지\n"
				}				
			}
			else
			{
				if( m_bUpsErrorChk == TRUE )
				{
					CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
					pDoc->m_fmst.fnSetLocalEMG_Reset();					
				}

				m_bUpsErrorChk = FALSE;
				m_nUpsDetectCnt = 0;
			}
		}
		
		m_strReceiveBuff += "\n";
		TRACE(m_strReceiveBuff);
		// BCRScaned(m_strReceiveBuff);
		m_strReceiveBuff.Empty();
	}
	return 0;
}

void CMainFrame::OnUpdateSerialConfig(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnIoTest() 
{
	// TODO: Add your command handler code here

	CSelUnitDlg *pDlg1;
	pDlg1 = new CSelUnitDlg();
	pDlg1->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pDlg1->SetTitle(TEXT_LANG[30]); //"TEST할 Unit을 선택하십시요"
	if(pDlg1->DoModal() != IDOK)	
	{
		delete pDlg1;
		pDlg1 = NULL;
		return;
	}
	int nModuleID = pDlg1->GetModuleID();
	delete pDlg1;
	pDlg1 =  NULL;
	
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	if(nModuleID > 0)
	{
		pDoc->ExecuteIOTest(nModuleID, FALSE);
	}

/*	CString strIP;
	CString strTemp;
	int nRtn;
	BOOL	bUseOutPut = TRUE;
	WORD state = EPGetGroupState(nModuleID, 0);
	
	if(state != EP_STATE_MAINTENANCE)
	{
		strIP.Format("IO Test는 작업 모드가 Maintenance 모드일때 가능 합니다. %s를 Maintenance 모드로 전환하지 않으면 OutPut은 사용할 수 없습니다. %s를 지금 Maintenance 모드로 전환 하시겠습니까?", ::GetModuleName(nModuleID), ::GetModuleName(nModuleID));
		nRtn = MessageBox(strIP, "모드 전환", MB_YESNOCANCEL|MB_ICONQUESTION);
		if(nRtn == IDCANCEL)	return;		//취소 
		else if(nRtn == IDYES)				//전환 
		{
			if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)		//권한 검사 
			{
				AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR));
				return ;
			}

			if((state == EP_STATE_IDLE || state == EP_STATE_READY) 
				&& pDoc->GetLineMode(nModuleID, 0) == EP_OFFLINE_MODE)	//변경 가능 상태 
			{
				pDoc->SetOperationMode(nModuleID, 0, EP_MAINTENANCE_MODE);		
			}
			else		//변경 가능 상태가 아닐 경우 
			{
				strIP.Format("%s는 현재 작업 모드를 변경할 수 있는 상태가 아닙니다.", ::GetModuleName(nModuleID));
				AfxMessageBox(strIP);
				return;
			}
		}
		else		//OutPut을 사용 안함 
		{
			bUseOutPut = FALSE;
		}
	}

	strIP = pDoc->GetModuleIPAddress(nModuleID);
	
	if(strIP.IsEmpty())
	{
		strIP.Format("%s 의 IP Address를 찾을 수 없습니다.", ::GetModuleName(nModuleID));
		AfxMessageBox(strIP);
		return;
	}

	HWND FirsthWnd;
	FirsthWnd = ::FindWindow(NULL, "IOTest");
	if (FirsthWnd)
	{
		::SetActiveWindow(FirsthWnd);	
		::SetForegroundWindow(FirsthWnd);
		return;
	}

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strDirTemp;
	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	}
	strTemp.Format("%s\\IOTest.exe %s %d", strDirTemp, strIP, bUseOutPut);	//IP를 인자로 넘김 
	//strTemp.Format("%s\\IOTest.exe %s %d", pDoc->m_strCurFolder, strIP, bUseOutPut);	//IP를 인자로 넘김 

	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		MessageBox("IO Test Program is not Found", "Execute Error", MB_OK|MB_ICONSTOP);
	}	
*/
}

/*
void CMainFrame::OnTabSelected(WPARAM wParam, LPARAM lParam)
{

	//현재 선택된 Tab만 Monitoring Timer 사용
	//현재 선택된 모듈만 Auto Report Data 수신 
	m_nCurTabIndex = wParam;

	SECTABINFO *lpTabInfo = (SECTABINFO *)lParam; 
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);
	
	if(m_pTopView == (CTopView *)(lpTabInfo->pActiveClient) )
	{
	m_pTopView->StartMonitoring();
	m_pDetailView->StopMonitoring();
	pDoc->SetAutoReport(m_pTopView->GetCurModuleID());
	
	}
	else if(m_pDetailView == (CDetailChannelView *)(lpTabInfo->pActiveClient))
	{
		m_pTopView->StopMonitoring();
		pDoc->SetAutoReport(m_pDetailView->GetCurModuleID());
		m_pDetailView->StartMonitoring();
	}
	return;
}
*/
void CMainFrame::OnAccuracyTest() 
{
	// TODO: Add your command handler code here	

	if(m_pAccurayDlg != NULL)
	{
		delete m_pAccurayDlg;
		m_pAccurayDlg = NULL;
	}


	m_pAccurayDlg =  new CMeasureDlg;
	ASSERT(m_pAccurayDlg);
	// m_pAccurayDlg->Create(IDD_MEASURE_DLG, GetDesktopWindow());
	m_pAccurayDlg->m_pDoc = (CCTSMonDoc *)GetActiveDocument();
	m_pAccurayDlg->Create(IDD_MEASURE_DLG, this);
	m_pAccurayDlg->ShowWindow(SW_SHOW);
	m_pAccurayDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
}

void CMainFrame::OnCalibrationSet() 
{
	// TODO: Add your command handler code here
	if(!PermissionCheck(PMS_MODULE_CAL_UPDATE))
	{
		AfxMessageBox(TEXT_LANG[40]+" [Calibration File Update]");
		return;
	}

/*	if(m_pCalDlg != NULL)
	{
		delete m_pCalDlg;
		m_pCalDlg = NULL;
	}
*/
/*	CCalFileDlg *pDlg = new CCalFileDlg(this);

	pDlg->SetModuleNum(EPGetInstalledModuleNum());
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
*/

/*	CModuleAddDlg *pDlg1;
	pDlg1 = new CModuleAddDlg();
	pDlg1->m_bAddDlg = FALSE;
//	pDlg1->m_nModuleID = m_n
	if(pDlg1->DoModal() != IDOK)	
	{
		delete pDlg1;
		pDlg1 = NULL;
		return;
	}
	int nModuleID = pDlg1->GetModuleID();
	delete pDlg1;
	pDlg1 =  NULL;
		
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	CString strIP;
	CString strTemp;

//	strTemp.Format("MDId %d Line state: %d", nModuleID, EPGetGroupState(nModuleID, 0));
//	pDoc->WriteLog(strTemp);

	if(EPGetGroupState(nModuleID, 0) != EP_STATE_LINE_OFF)
	{
		strIP = EPGetModuleIP(nModuleID);
//		pDoc->WriteLog("Line On state");
	}
	else
	{
		CCalFileDlg *pDlg;
		pDlg = new CCalFileDlg();
		strIP = pDlg->GetIPAddress(nModuleID);
		delete pDlg;
	}

	
	if(strIP.IsEmpty())
	{
		strIP.Format("%s [%s]", GetStringTable(IDS_LANG_MSG_ERROR_FOUND_IP), ::GetModuleName(nModuleID));
		AfxMessageBox(strIP);
		return;
	}
*/
	CString strTemp;
	
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strDirTemp;
/*	{
		TCHAR szCurDir[MAX_PATH];
		::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
		strDirTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));
	}
*/
	strDirTemp = ((CCTSMonDoc *)GetActiveDocument())->m_strCurFolder;
	strTemp.Format("%s\\CalFile.exe", strDirTemp);	//IP를 인자로 넘김 

	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
/*		LPVOID lpMsgBuf;
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			0, // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		);
		// Process any inserts in lpMsgBuf.
		// ...
		// Display the string.
		MessageBox( (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
		// Free the buffer.
		LocalFree( lpMsgBuf );
*/
		strTemp.Format("%s\\CalFile.exe File not Found.", strDirTemp);	//IP를 인자로 넘김 
		MessageBox(strTemp, "Execute Error", MB_OK|MB_ICONSTOP); 
	}			
}

void CMainFrame::OnRefAdData() 
{
	// TODO: Add your command handler code here
	CString strFileName;
	CRegulatorDlg *pDlg;
	pDlg =  new CRegulatorDlg(this);
	if(pDlg->DoModal() == IDOK)
	{
		strFileName = pDlg->m_strFileName;
		if(!strFileName.IsEmpty())
		{
			CString strMsg;
			strMsg.Format(TEXT_LANG[47], strFileName);
			if(MessageBox(strMsg, "File Open", MB_ICONQUESTION|MB_YESNO) != IDYES)
				return;

			//Excel로 실행
	/*		HWND FirsthWnd;
			FirsthWnd = ::FindWindow(NULL, "Excel");
			if (FirsthWnd)
			{
				::SetActiveWindow(FirsthWnd);	
				::SetForegroundWindow(FirsthWnd);
				return;
			}
	*/
			CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
			strMsg = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Excel Path", "");
			pDoc->ExecuteProgram(strMsg, strFileName, "", "", TRUE);

	/*		STARTUPINFO	stStartUpInfo;
			PROCESS_INFORMATION	ProcessInfo;
			ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
			ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
			
			stStartUpInfo.cb = sizeof(STARTUPINFO);
			stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
			stStartUpInfo.wShowWindow = SW_NORMAL;

			CString strTemp("C:\\Program Files\\Microsoft Office\\Office\\excel.exe");
			char buff1[512];
			char buff2[512];

			GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, 511);
			GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, 511);

			strTemp.Format("%s %s", buff2, buff1);
			BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
			if(bFlag == FALSE)
			{
				MessageBox("MS Office Excel is not Found.", "Execute Error", MB_OK|MB_ICONSTOP);
			}
	*/
		}	
	}
	delete pDlg;
	pDlg = NULL;
}

void CMainFrame::OnUpdateRefAdData(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
}

BOOL CMainFrame::CompareCurrentSelectTab(int nTabID)
{
	if(m_pTab == NULL)	
	{
		return FALSE;
	}	

	if( m_pTab->m_nCurTabIndex == nTabID )
	{
		return true;
	}

	return FALSE;
}

void CMainFrame::OnTimerFuncTryToConnectToDBServer()
{
	if(m_bDBSvrConnect)	
	{
		KillTimer(TIMER_TRY_TO_CONNECT_TO_DBSERVER);
		return;
	}

	CString strTemp;
	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION ,"Use Data Server", FALSE))
	{
		strTemp = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION ,"Data Server IP", "");
		UINT lSystemID = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "System ID", 1);
		UINT lSystemType = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "System Type", 1);
		int nRtn = ::dcConnectToServer((LPSTR)(LPCTSTR)strTemp, m_hWnd, MAKELONG(lSystemID, lSystemType));

		TRACE("**************Try to Connect to DBServer (Ret val:%d)\n", nRtn);
	}
}

LRESULT CMainFrame::OnUserCmdReceive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);

	EP_USER_CMD_INFO* pUserCmd = (EP_USER_CMD_INFO*)lParam;
	int nRtn;
	CCTSMonDoc *pDoc = ((CCTSMonDoc*)GetActiveDocument());

	switch(pUserCmd->nCmd)
	{
	case EP_CMD_SHUTDOWN:
		pDoc->OnReceiveModuleShutdownCmd(lModuleID, pUserCmd->nCmd);
		break;
	case EP_CMD_RUN:
		pDoc->SendRunCommand(lModuleID);
		break;
	case EP_CMD_PAUSE:
		if(( nRtn = EPSendCommand(lModuleID, 0, 0, EP_CMD_PAUSE)) != EP_ACK)
		{
			CString strTemp;
			strTemp.Format("%s (%s-%s)", TEXT_LANG[45],  ::GetModuleName(lModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
		// EPSendCommand(lModuleID);
		break;
	case EP_CMD_STOP:
		pDoc->SendStopCmd(lModuleID);
		break;
	case EP_CMD_CLEAR:
		pDoc->SendInitCommand(lModuleID);
		break;
	}
	delete pUserCmd;

	return 1;
}


//Step start real time data receive
LRESULT CMainFrame::OnRealTimeDataReceive(WPARAM wParam, LPARAM lParam)
{
	int nModuleID = HIWORD(wParam);

	// 20090702 kky for 온라인모드일시 로그 파일 상태
	CCTSMonDoc *pDoc = ((CCTSMonDoc*)GetActiveDocument());

	((CCTSMonDoc *)GetActiveDocument())->SaveRealTimeData(nModuleID);

	return 1;
}

void CMainFrame::OnUpdateCalibrationSet(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(((CCTSMonApp *)AfxGetApp())->GetSystemType() == EP_ID_FORM_OP_SYSTEM || ((CCTSMonApp *)AfxGetApp())->GetSystemType() == EP_ID_ALL_SYSTEM)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CMainFrame::OnCalTypeSelect() 
{
	// TODO: Add your command handler code here
	if(::LoginPremissionCheck(PMS_MODULE_CAL_UPDATE) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[40] +" [Parameter Setting]");
		return ;
	}
	
	CParamSetDlg *pDlg;
	pDlg = new CParamSetDlg(this);

	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}

void CMainFrame::OnDestroy() 
{
	CFrameWnd::OnDestroy();
	
	// TODO: Add your message handler code here
	if(m_bDBSvrConnect)		
		::dcDisConnect();

	CloseSerialPort();
		
	CRect rect;
	m_pAllSystemView->GetClientRect(rect);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "SpliteSize", rect.Height());
	// CView's clean up automatically. CWnd's aren't so lucky.
	m_pTab->DestroyWindow();	
	delete m_pTab;			
	
	if(m_pSensorDlg != NULL)
	{
		delete m_pSensorDlg;
		m_pSensorDlg = NULL;
	}
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	/*
	CRect rect;
	GetWindowRect( &rect );
	if(m_bSplitterCreated)  // m_bSplitterCreated set in OnCreateClient
	{	
		m_wndSplitter.SetRowInfo(0, rect.Height()*0.2, 10);
		m_wndSplitter.SetRowInfo(1, rect.Height()*0.8, 10);
		m_wndSplitter.RecalcLayout();
	}
	*/
}

void CMainFrame::OnUpdateIoTest(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnJigAdd() 
{
	// TODO: Add your command handler code here
	CJigIDRegDlg dlg(this);
	dlg.DoModal();
}

void CMainFrame::OnJigDelete() 
{
	// TODO: Add your command handler code here

	//delete Jig Location Infomation
	CString str, strTemp;

	//Jig ID나 모듈/지그 번호를 입력 받는다.
	//////////////////////////////////////////////////////////////////////////
	CJigIDInputDlg dlg;	
	if(dlg.DoModal() != IDOK)
	{
		return;
	}
	str = dlg.GetJigID();

	CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
	int nModuleID, nJig;
	if(pDlg->SearchLocation(str, nModuleID, nJig))
	{
		strTemp.Format(TEXT_LANG[31], GetModuleName(nModuleID), nJig, str); //"%s에 위치한 지그 %d의 위치 정보 [%s]를 삭제 하시겠습니까?"
		if(AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION) != IDYES)
		{
			delete pDlg;
			return;
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[32]); //"Jig 위치 정보를 찾을 수 없습니다."
		AfxMessageBox(strTemp);
		delete pDlg;
		return;
	}

	if(pDlg->DeleteJigIDInfo(str))
	{
		TRACE("Jig %s deleted\n", str);
	}
	delete pDlg;
}

//작업자가 수동으로 Tray 번호 입력할 경우 
BOOL CMainFrame::UserInputTrayNo(int nModuleID, int nJigNo, CString strTrayNo)
{
	//연속적으로 호출시 오류가 생김 
	static EP_TAG_INFOMATION tagData;
	memset(&tagData, 0, sizeof(tagData));
//	sprintf(nvData.szTraySerialNo, "%s", TRAY_RECT_FAIL_CODE);

	//사용자 입력을 받아 들임
	int nSelJigNo, nSelModuleID;

	if(strTrayNo.IsEmpty() || nJigNo < 1)
	{
		CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
		CTrayInputDlg dlg(pDoc,this);
		dlg.SetUnitInput(TRUE);
		dlg.m_nSelModuleID = nModuleID;

		//작업 가능 지그 검색 
		CFormModule *pModule = ((CCTSMonDoc *)GetActiveDocument())->GetModuleInfo(nModuleID);
		for(int t =0; t<pModule->GetTotalJig(); t++)
		{
			CTray *pTray = pModule->GetTrayInfo(t);
			if(pTray->GetBcrRead() == FALSE)
			{
				dlg.m_nSelJigNo = t+1;
				break;
			}
		}

		if(dlg.DoModal() == IDOK)
		{
			sprintf(tagData.szTagID, dlg.m_strTrayNo);
			nSelModuleID = dlg.m_nSelModuleID;
			nSelJigNo = dlg.m_nSelJigNo;

			//선택 장비 작업가능 여부 판단
			//

			return ::SendMessage(m_hWnd, WM_BCR_READED, 
				MAKELONG(nSelJigNo, nSelModuleID), (LPARAM)&tagData);			//Send to Parent Wnd to Module State change
		}
	}
	else
	//지정해서 올 경우 
	{
		sprintf(tagData.szTagID, strTrayNo);
		nSelJigNo = nJigNo;
		nSelModuleID = nModuleID;
		
		return ::SendMessage(m_hWnd, WM_BCR_READED, 
				MAKELONG(nSelJigNo, nSelModuleID), (LPARAM)&tagData);			//Send to Parent Wnd to Module State change
	}

	return FALSE;
}

//EP_STEP_SUMMERY를 생성하고 저장용 Channel 구조를 생성
//Channel 구조체 생성시 Precision 데이터를 생성한다.
BOOL CMainFrame::MakeFileStructure(int nModuleID, int nTrayIndex, int nStepIndex, LPVOID lpData, LPSTR_SAVE_CH_DATA lpSaveChData)
{
	ASSERT(lpSaveChData);
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == FALSE)	return FALSE;

	CTray *pTrayInfo = pModule->GetTrayInfo(nTrayIndex);
	if(pTrayInfo == NULL)	return FALSE;

//	int nChCount = pModule->GetCellCountInTray();
	int nChCount = pModule->GetChInJig(nTrayIndex);
	int nNonCellCount = 0;

	//Make Channel data structrue
//	EP_STEP_SUMMERY stepSummery;
//	ZeroMemory(&stepSummery, sizeof(EP_STEP_SUMMERY));

	CCalAvg avg[EP_RESULT_ITEM_NO];
	STR_SAVE_CH_DATA	dataBuff;
	LPSTR_SAVE_CH_DATA  pChSaveBuff= (LPSTR_SAVE_CH_DATA)lpSaveChData;
	LPEP_CH_DATA lpChData = (LPEP_CH_DATA)lpData;
	
	int nStartChIndex = pModule->GetStartChIndex(nTrayIndex);
	for(int i =0 ; i<nChCount; i++)
	{
		//동기화 필요한 Data를 포함시켜 저장 data 구조를 만든다. Add sensor data(Temp, Gas, Pressure, etc)
		if(i < pModule->GetChInJig(nTrayIndex))
		{
			//Tray에 실제 존재하는 Channel은 Data를 Update
			pModule->ConvertChSaveData(nTrayIndex, i, &lpChData[nStartChIndex+i], &dataBuff);
		}
		else
		{	//Tray에 실제 존재하지 않은 채널 (이전 Data와 같게 저장)
			//memset(pChSaveBuff, 0, sizeof(STR_SAVE_CH_DATA));
		}
		memcpy(&pChSaveBuff[i], &dataBuff, sizeof(STR_SAVE_CH_DATA));
	
		if(::IsNormalCell(pChSaveBuff[i].channelCode))
		{
			avg[EP_TIME_BIT_POS].AddData(i, pChSaveBuff[i].fStepTime);
			avg[EP_VOLTAGE_BIT_POS].AddData(i, pChSaveBuff[i].fVoltage);
			avg[EP_CURRENT_BIT_POS].AddData(i, pChSaveBuff[i].fCurrent);
			avg[EP_CAPACITY_BIT_POS].AddData(i, pChSaveBuff[i].fCapacity);
			avg[EP_WATT_BIT_POS].AddData(i, pChSaveBuff[i].fWatt);
			avg[EP_WATT_HOUR_BIT_POS].AddData(i, pChSaveBuff[i].fWattHour);
			avg[EP_IMPEDANCE_BIT_POS].AddData(i, pChSaveBuff[i].fImpedance);
		}
		else if(::IsNonCell(pChSaveBuff[i].channelCode))
		{
			nNonCellCount++;
		}
		pTrayInfo->cellCode[i] = pChSaveBuff[i].channelCode;
		pTrayInfo->cellGradeCode[i] = pChSaveBuff[i].grade;
	}

	pTrayInfo->nNormalCount = avg[EP_TIME_BIT_POS].GetDataCount();
	pTrayInfo->nFailCount = nChCount - (pTrayInfo->nNormalCount+nNonCellCount);		

	//////////////////////////////////////////////////////////////////////////
	//Make summery data structure
	int nSIndex = nStepIndex;							
	int nStepType = EP_TYPE_NONE;
	int nStepProcType = EP_PROC_TYPE_NONE;	
	CTestCondition *pCondition = pModule->GetCondition();
	CStep *pStep = pCondition->GetStep(nSIndex);
	if(pStep)
	{
		nStepType = pStep->m_type;
		nStepProcType = pStep->m_lProcType;
	}
	return TRUE;
}

BOOL CMainFrame::MakeCellCheckFileStructure(int nModuleID, int nTrayIndex, LPVOID lpData, LPSTR_SAVE_CH_DATA lpSaveChData)
{
	ASSERT(lpSaveChData);
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule == FALSE)	return FALSE;

	CTray *pTrayInfo = pModule->GetTrayInfo(nTrayIndex);
	if(pTrayInfo == NULL)	return FALSE;

	//	int nChCount = pModule->GetCellCountInTray();
	int nChCount = pModule->GetChInJig(nTrayIndex);
	int nNonCellCount = 0;

	//Make Channel data structrue
	//	EP_STEP_SUMMERY stepSummery;
	//	ZeroMemory(&stepSummery, sizeof(EP_STEP_SUMMERY));

	CCalAvg avg[EP_RESULT_ITEM_NO];
	STR_SAVE_CH_DATA	dataBuff;
	LPSTR_SAVE_CH_DATA  pChSaveBuff = (LPSTR_SAVE_CH_DATA)lpSaveChData;
	LPEP_CH_DATA lpChData = (LPEP_CH_DATA)lpData;

	int nStartChIndex = pModule->GetStartChIndex(nTrayIndex);
	for(int i =0 ; i<nChCount; i++)
	{
		//동기화 필요한 Data를 포함시켜 저장 data 구조를 만든다. Add sensor data(Temp, Gas, Pressure, etc)
		if(i < pModule->GetChInJig(nTrayIndex))
		{
			//Tray에 실제 존재하는 Channel은 Data를 Update
			pModule->ConvertChSaveData(nTrayIndex, i, &lpChData[nStartChIndex+i], &dataBuff);
		}
		else
		{	//Tray에 실제 존재하지 않은 채널 (이전 Data와 같게 저장)
			//memset(pChSaveBuff, 0, sizeof(STR_SAVE_CH_DATA));
		}
		memcpy(&pChSaveBuff[i], &dataBuff, sizeof(STR_SAVE_CH_DATA));

		if(::IsNormalCell(pChSaveBuff[i].channelCode))
		{
			avg[EP_TIME_BIT_POS].AddData(i, pChSaveBuff[i].fStepTime);
			avg[EP_VOLTAGE_BIT_POS].AddData(i, pChSaveBuff[i].fVoltage);
			avg[EP_CURRENT_BIT_POS].AddData(i, pChSaveBuff[i].fCurrent);
			avg[EP_CAPACITY_BIT_POS].AddData(i, pChSaveBuff[i].fCapacity);
			avg[EP_WATT_BIT_POS].AddData(i, pChSaveBuff[i].fWatt);
			avg[EP_WATT_HOUR_BIT_POS].AddData(i, pChSaveBuff[i].fWattHour);
			avg[EP_IMPEDANCE_BIT_POS].AddData(i, pChSaveBuff[i].fImpedance);
		}
		else if(::IsNonCell(pChSaveBuff[i].channelCode))
		{
			nNonCellCount++;
		}
		pTrayInfo->cellCode[i] = pChSaveBuff[i].channelCode;
		pTrayInfo->cellGradeCode[i] = pChSaveBuff[i].grade;
	}
	
	pTrayInfo->nNormalCount = avg[EP_TIME_BIT_POS].GetDataCount();
	
	pTrayInfo->nFailCount = nChCount - (pTrayInfo->nNormalCount+nNonCellCount);
	
	if( pTrayInfo->nFailCount > 0 )
	{
	
	}
	
	if( pTrayInfo->nFailCount < 0 || (pTrayInfo->nNormalCount+pTrayInfo->nFailCount) != pTrayInfo->GetInputCellCnt() )	
	{
		CString strTemp;
		strTemp.Format("Tray [%s] Cell Count Check %d/%d/%d", pTrayInfo->GetTrayNo(), pTrayInfo->nNormalCount, pTrayInfo->nFailCount, nNonCellCount);
		pDoc->WriteLog(strTemp);
	}
	//////////////////////////////////////////////////////////////////////////
	//Make summery data structure
	/*
	int nSIndex = 0;							
	int nStepType = EP_TYPE_NONE;
	int nStepProcType = EP_PROC_TYPE_NONE;	
	CTestCondition *pCondition = pModule->GetCondition();
	CStep *pStep = pCondition->GetStep(nSIndex);
	if(pStep)
	{
		nStepType = pStep->m_type;
		nStepProcType = pStep->m_lProcType;
	}
	*/
	return TRUE;
}



void CMainFrame::BCRScaned(CString strCode)
{
	//Read한 Barcode가 Tray 번호인지 Jig 번호인지 구별한다.
	char szBuff[64];
	sprintf(szBuff, "%s", strCode);

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	//CWnd *pWnd = GetLastActivePopup();

	int nModule, nJig;
	HWND hTargetWnd = ::GetActiveWindow();
	UINT nTagType = TAG_TYPE_NONE;
	if (Fun_FindJigID(strCode, nModule, nJig) == TRUE)
	{
		m_pTrayInputDlg = new CTrayInputDlg(pDoc,this);
		m_pTrayInputDlg->SetUnitInput(TRUE);
		m_pTrayInputDlg->Create(IDD_TRAYNO_DLG, this);
		hTargetWnd = m_pTrayInputDlg->m_hWnd;
		nTagType = TAG_TYPE_JIG;
	}

// 	if(hTargetWnd == NULL || hTargetWnd == m_hWnd)
// 	{

//		if(m_pTrayInputDlg == NULL)
//		{
// 			m_pTrayInputDlg = new CTrayInputDlg(pDoc,this);
// 			m_pTrayInputDlg->SetUnitInput(TRUE);
// 			m_pTrayInputDlg->Create(IDD_TRAYNO_DLG, this);
//		}
//		hTargetWnd = m_pTrayInputDlg->m_hWnd;
//	}

	//1. Tray ID로 사용중인지 검사 
	//Tray 위치 정보를 검색 
//	UINT nTagType = TAG_TYPE_NONE;
// 	CTray *pTray = new CTray();
// 	if(pTray->LoadTrayData(strCode))
// 	{
// 		nTagType = TAG_TYPE_TRAY;
// 	}
// 	delete pTray;

	//2. Location ID로 사용중인지 검사 
// 	CJigIDRegDlg *pDlg = new CJigIDRegDlg;
// 	if(pDlg->SearchLocation(strCode, nModule, nJig))
// 	{
// 		nTagType = TAG_TYPE_JIG;
// 	}
// 	delete pDlg;

	//3. Lot로 사용중인지 검사 
	{

	}

/*	if(nTagType == TAG_TYPE_NONE)
	{
		//여러개 발생시 Down됨 
		AfxMessageBox("알수 없는 코드입니다.(먼저 등록 후 사용하십시요)", MB_OK|MB_ICONSTOP);
		return;
	}
*/
	::SendMessage(hTargetWnd, EPWM_BCR_SCANED, (WPARAM)nTagType, (LPARAM)szBuff);
}


/**
@author  
@brief Tools -> 비상정지(EMG.) Log 보기(E)
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  각 Unit의 비상정지된 상태를 기록합니다
@return  
@see  
@todo    
*/
void CMainFrame::OnViewEmgLog()
{
	CSelEmgLogDlg *pDlg = new CSelEmgLogDlg;
	if(pDlg->DoModal() == IDOK)
	{
		CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
		ASSERT(pDoc);

		CString strTemp, strFileName;
		strFileName.Format("%s\\Log\\%s", pDoc->m_strCurFolder, pDlg->GetSelFileName());
		strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Excel Path", "");
		
		if(strTemp.IsEmpty())
		{	//Execute note pad
			char szWinDir[MAX_PATH];
			::GetWindowsDirectory(szWinDir, MAX_PATH-1);
			strTemp.Format("%s\\Notepad.exe", szWinDir);
			pDoc->ExecuteProgram(strTemp, strFileName, "", "", TRUE);
		}
		else
		{
			//Execute Excel
			pDoc->ExecuteProgram(strTemp, strFileName, "", "", TRUE);
		}
		
/*		char szWinDir[MAX_PATH];
		if(GetWindowsDirectory(szWinDir, MAX_PATH-1) <=0 )		return;
		strTemp.Format("%s\\Notepad.exe %s", szWinDir, strFileName);
		STARTUPINFO	stStartUpInfo;
		PROCESS_INFORMATION	ProcessInfo;
		ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
		ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
		stStartUpInfo.cb = sizeof(STARTUPINFO);
		stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
		stStartUpInfo.wShowWindow = SW_SHOWNORMAL;
		BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		if(bFlag == FALSE)
		{
			MessageBox(GetStringTable(IDS_LANG_MSG_ALERT_LOG_FILE_NOTE_FOUND), "Excute Error", MB_OK|MB_ICONSTOP);
		}
*/
	}
	delete pDlg;
}

/**
@author  
@brief Tools -> Unit(설비) 로그 보기(U)
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  CTSMon SBC 로그를 불러온다.
@return 
@see  
@todo    
*/
void CMainFrame::OnUnitLog() 
{
	// TODO: Add your command handler code here
	CSelUnitDlg *pDlg1;
	pDlg1 = new CSelUnitDlg();
	int nModuleID = EPGetModuleID(0);
	if(m_pTab->m_pTopView)
	{
		nModuleID = m_pTab->m_pTopView->GetCurModuleID();
	}
	pDlg1->SetModuleID(nModuleID);
	if(pDlg1->DoModal() != IDOK)	
	{
		delete pDlg1;
		pDlg1 = NULL;
		return;
	}
	nModuleID = pDlg1->GetModuleID();
	delete pDlg1;
	pDlg1 =  NULL;
	
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);	

	CString strIP = pDoc->GetModuleIPAddress(nModuleID);

	CUnitComLogDlg dlg(strIP, this);

	dlg.DoModal();
}

/**
@author  
@brief Tools -> 원격접속(Telnet)
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  SBC와 Telnet 연동한다.
@return 
@see  
@todo    
*/
void CMainFrame::OnTenletCom() 
{
	// TODO: Add your command handler code here
	CSelUnitDlg *pDlg1;
	pDlg1 = new CSelUnitDlg();
	// pDlg1->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pDlg1->SetModuleID(m_pAllSystemView->GetCurModuleID());
	if(pDlg1->DoModal() != IDOK)	
	{
		delete pDlg1;
		pDlg1 = NULL;
		return;
	}
	int nModuleID = pDlg1->GetModuleID();
	delete pDlg1;
	pDlg1 =  NULL;

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	if(nModuleID > 0)
	{
		CFormModule *pModuleInfo = pDoc->GetModuleInfo(nModuleID);
		CString strIP(pModuleInfo->GetIPAddress());

		if(strIP.IsEmpty() == FALSE)
		{
			char cmdline[128];
			CString strUser(AfxGetApp()->GetProfileString("Ftp Set", "User ID", "sbc"));			
			sprintf(cmdline, "C:\\Windows\\SysNative\\telnet %s -l %s", strIP, strUser);
			pDoc->ExecuteProgram(cmdline);
			/*
			char cmdline[128];
			CString strUser(AfxGetApp()->GetProfileString("Ftp Set", "User ID", "sbc"));			
			// sprintf(cmdline, "C:\\Windows\\SysNative\\telnet %s -l %s", strIP, strUser);
			sprintf(cmdline, "C:\\Windows\\system32\\telnet %s -l %s", strIP, strUser);
			pDoc->ExecuteProgram(cmdline);
			*/
		}
	}
}

/**
@author  
@brief  Network 연결 검사(Ping)
@bug    
@code   
@date   2013 - 06 -13
@endcode 
@param   
@remark  (SBC -> PC) 컴파일 환경에서 ping 테스트를 실행한다.
@return  
@see  
@todo    
*/
void CMainFrame::OnNetCheck() 
{
	// TODO: Add your command handler code here
	CSelUnitDlg *pDlg1;
	pDlg1 = new CSelUnitDlg();
	pDlg1->SetModuleID(m_pAllSystemView->GetCurModuleID());
	pDlg1->SetTitle(_T(TEXT_LANG[33]));//"통신을 확인할 Unit을 선택하십시요."
	if(pDlg1->DoModal() != IDOK)	
	{
		delete pDlg1;
		pDlg1 = NULL;
		return;
	}
	int nModuleID = pDlg1->GetModuleID();
	delete pDlg1;
	pDlg1 =  NULL;

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	if(nModuleID > 0)
	{
		CFormModule *pModuleInfo = pDoc->GetModuleInfo(nModuleID);
		CString strIP(pModuleInfo->GetIPAddress());		
		if(strIP.IsEmpty() == FALSE)
		{
			char cmdline[128];
			sprintf(cmdline, "C:\\Windows\\SysNative\\ping %s -t", strIP);
			pDoc->ExecuteProgram(cmdline);
		}
	}
}

/**
@author  
@brief  
@bug    
@code   
@date   
@endcode 
@param   
@remark  
@return  
@see  
@todo    
*/
void CMainFrame::OnModuleBackup() 
{
	// TODO: Add your command handler code here
	
}

//[cali Result Data Receive]============================================================
LRESULT CMainFrame::OnCalResultReceive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
	LPVOID lpData = (LPVOID)lParam;
// 	EP_CALIHCK_RESULT_DATA *codeData = new EP_CALIHCK_RESULT_DATA;
// 	memset(codeData, 0, sizeof(EP_CALIHCK_RESULT_DATA));
	EP_CALIHCK_RESULT_DATA pCodeData;
	memcpy( &pCodeData, (char*)lpData, sizeof( EP_CALIHCK_RESULT_DATA ));
	
	if(lParam != NULL)
	{
//		memcpy(codeData, (EP_CALIHCK_RESULT_DATA*)lParam, sizeof(EP_CALIHCK_RESULT_DATA));

// 		sprintf(msg, "Module %d Group %d, Calibration data received. (EMG Code %d / %d)", 
// 				m_stModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum);
// 		WriteLog(msg);
		CCalibratorDlg *pCaliDlg;
		if (m_apCaliDlg.GetSize() > 0)
		{
			for (int i=0 ; i< m_apCaliDlg.GetSize() ; i++)
			{
				pCaliDlg = (CCalibratorDlg *)m_apCaliDlg[i];
				
				// if (pCaliDlg->IsWindowVisible())
				// {
					if (pCaliDlg->m_nUnitNo == lModuleID)
					{ 
						pCaliDlg->UpdateResultData(lModuleID, nMode, pCodeData);
						return 0;
					}
				// }
			}
		}
// 		if(m_pCalibDlg)
// 		{
// 			m_pCalibDlg->UpdateResultData(lModuleID, nMode, codeData);
// 		}
	}
	return 1;
}

//[cali End Receive]============================================================
LRESULT CMainFrame::OnCalEndReceive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);	
// 	codeData = new EP_CAL_SELECT_INFO;
// 	ZeroMemory( codeData, sizeof( EP_CAL_SELECT_INFO));
	LPVOID lpData = (LPVOID)lParam;
	// 	EP_CALIHCK_RESULT_DATA *codeData = new EP_CALIHCK_RESULT_DATA;
	// 	memset(codeData, 0, sizeof(EP_CALIHCK_RESULT_DATA));
	EP_CAL_SELECT_INFO pCodeData;
	memcpy( &pCodeData, (char*)lpData, sizeof( EP_CAL_SELECT_INFO ));
	

	// EP_CAL_SELECT_INFO codeData;
	if(lParam != NULL)
	{
//		memcpy(codeData, (EP_CAL_SELECT_INFO*)lParam, sizeof(EP_CAL_SELECT_INFO));
		CCalibratorDlg *pCaliDlg;
		if (m_apCaliDlg.GetSize() > 0)
		{
			for (int i=0 ; i< m_apCaliDlg.GetSize() ; i++)
			{
				pCaliDlg = (CCalibratorDlg *)m_apCaliDlg[i];
				
				// if (pCaliDlg->IsWindowVisible())
				// {
					if (pCaliDlg->m_nUnitNo == lModuleID)
					{
						pCaliDlg->EndResultData(lModuleID, nMode, pCodeData);
						return 0;
					}
				// }
			}
		}
// 		if(m_pCalibDlg)
// 		{			
// 			m_pCalibDlg->EndResultData(lModuleID, nMode, codeData);
// 		}
	}

	return 1;
}

LRESULT CMainFrame::OnRealMeasEndReceive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
	EP_REAL_MEASURE_RESULT_DATA *RealMeasureResultData = (EP_REAL_MEASURE_RESULT_DATA*)lParam;
//	EP_REAL_MEASURE_RESULT_DATA RealMeasureResultData;
	
	if(lParam != NULL)
	{	
		//memcpy(&RealMeasureResultData, (EP_REAL_MEASURE_RESULT_DATA *)lParam, sizeof(EP_REAL_MEASURE_RESULT_DATA));
		//memcpy(RealMeasureResultData, (EP_REAL_MEASURE_RESULT_DATA *)lParam, sizeof(EP_REAL_MEASURE_RESULT_DATA));

		if( m_pAccurayDlg)
		{
			m_pAccurayDlg->UpdateResultDataOnList( RealMeasureResultData );
		}
	}

	return 1;
}

LRESULT CMainFrame::OnRealMeasWorkEndReceive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
		
	if( m_pAccurayDlg )
	{
		m_pAccurayDlg->FunWorkEndReveive();	//ljb 2011519 
	}

	return 1;
}

//////////////////////////////////////////////////////////
// 20200130 KSJ // MISWIRING

LRESULT CMainFrame::OnMiswiringDataReceive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
	LPVOID lpData = (LPVOID)lParam;

	EP_MISWIRING_CHECK_RESULT_DATA pCodeData;
	memcpy( &pCodeData, (char*)lpData, sizeof( EP_MISWIRING_CHECK_RESULT_DATA ));

	if(lParam != NULL)
	{

		CMisWiringDlg *pMisDlg;
		if (m_apMisWiringDlg.GetSize() > 0)
		{
			for (int i=0 ; i< m_apMisWiringDlg.GetSize() ; i++)
			{
				pMisDlg = (CMisWiringDlg *)m_apMisWiringDlg[i];

				if (pMisDlg->m_nModuleID == lModuleID)
				{ 
					pMisDlg->UpdateResultData(pCodeData);
					return 0;
				}
			}
		}
	}
	return 1;
}
// 20200130 KSJ // MISWIRING End Receive
LRESULT CMainFrame::OnMiswiringEndReceive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);	

	if(lParam != NULL)
	{
		CMisWiringDlg *pMisDlg;
		if (m_apMisWiringDlg.GetSize() > 0)
		{
			for (int i=0 ; i< m_apMisWiringDlg.GetSize() ; i++)
			{
				pMisDlg = (CMisWiringDlg *)m_apMisWiringDlg[i];

				if (pMisDlg->m_nModuleID == lModuleID)
				{ 
					pMisDlg->EndResultData(lModuleID, nMode);
					return 0;
				}
			}
		}
	}
	return 1;
}


///////////////////////////////////////////////////////////////////////////




LRESULT CMainFrame::OnGainReadReceive(WPARAM wParam, LPARAM lParam)
{
// 191223 엄륭 GAIN 추가 관련

	CCTSMonDoc  *pDoc = (CCTSMonDoc *)GetActiveDocument();
	ASSERT(pDoc);

	int nModuleID = HIWORD(wParam);	
	int nModuleIndex = EPGetModuleIndex(nModuleID);

	CString strTemp, strTemp2, strTok1;

	EP_CMD_GAIN_READ_RES ReadtargetData;
	memcpy(&ReadtargetData, (LPVOID)lParam, sizeof(EP_CMD_GAIN_READ_RES));	

	strTemp.Format("<<[RECV] [Stage %s] [OnGainReadReceive cmd recieved]", ::GetModuleName(nModuleID)); 
	pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);

	for(int i=0; i<4; i++)
	{
		pDoc->I_Gain1_PC[i] = ReadtargetData.gainData.I_Gain1[i];
		pDoc->I_Gain2_PC[i] = ReadtargetData.gainData.I_Gain2[i];
		pDoc->I_Gain3_PC[i] = ReadtargetData.gainData.I_Gain3[i];
		pDoc->I_Gain4_PC[i] = ReadtargetData.gainData.I_Gain4[i];
		pDoc->I_Gain5_PC[i] = ReadtargetData.gainData.I_Gain5[i];
	}



	for(int a=0; a<2; a++)
	{
		pDoc->V_Gain1_PC[a] = ReadtargetData.gainData.V_Gain1[a];
		pDoc->V_Gain2_PC[a] = ReadtargetData.gainData.V_Gain2[a];
	}

	pDoc->m_bGainReadOn = TRUE;
	

	return 1;
}
LRESULT CMainFrame::OnReciveSbcParam(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
	if(m_pSettingCheckDlg !=NULL)
	{
		m_pSettingCheckDlg->RecvData(lModuleID);
	}
	///////////////////////////////////////2021 04 06 강신중 D ERCD 코드 복사 START
	if( m_pErcdWriteDlg != NULL)
	{
		if(m_pErcdWriteDlg->IsWindowVisible())
		{
			m_pErcdWriteDlg->RecvParameter(lModuleID);
		}
	}
	///////////////////////////////////////2021 04 06 강신중 D ERCD 코드 복사 END
	return TRUE;
}
LRESULT CMainFrame::OnReciveSbcCaliData(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
	if(m_pSbcCaliDataDlg !=NULL)
	{
		m_pSbcCaliDataDlg->RecvData(lModuleID);
	}
	return TRUE;
}
LRESULT CMainFrame::OnReciveSbcVersionAllData(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
	if(m_pStageVersion !=NULL)
	{
		m_pStageVersion->RecvData(lModuleID);
	}
	return TRUE;
}
//20200831 ksj
LRESULT CMainFrame::OnSensorSettingDataRecive(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);

	EP_SENSOR_SET_DATA* recvSensorSetData = (EP_SENSOR_SET_DATA*)lParam;
	if(m_pSensorDlg !=NULL)
	{

		if(m_pSensorDlg->m_pSensorMap != NULL)
		{
			CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
			ASSERT(pDoc);	
			CString strTemp;
			bool bCheck = false;
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.bUseError != recvSensorSetData->bUseError)										{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.sErrorCumulativeBuffer != recvSensorSetData->sErrorCumulativeBuffer)				{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.wWarningNo1 != recvSensorSetData->wWarningNo1)									{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.wWarningNo2 != recvSensorSetData->wWarningNo2)									{bCheck = true;}

			for(int i=0; i<EP_MAX_SENSOR_CH; i++)
			{
				if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.tempSensorParam.bUseSensorFlag[i] != recvSensorSetData->tempSensorParam.bUseSensorFlag[i])		{bCheck = true;}
			}

			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.tempSensorParam.bUseLimit != recvSensorSetData->tempSensorParam.bUseLimit)					{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.tempSensorParam.lCutLimit != recvSensorSetData->tempSensorParam.lCutLimit)					{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.tempSensorParam.wJigTempErrorRefValue != recvSensorSetData->tempSensorParam.wJigTempErrorRefValue)			{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.tempSensorParam.wPowerTempErrorRefValue != recvSensorSetData->tempSensorParam.wPowerTempErrorRefValue)	{bCheck = true;}

			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.wJigTempWarningRefValue != recvSensorSetData->wJigTempWarningRefValue)			{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.wPowerTempWarningRefValue != recvSensorSetData->wPowerTempWarningRefValue)		{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.cJigFanFaultErrorCount != recvSensorSetData->cJigFanFaultErrorCount)				{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.cDCFanFaultErrorCount != recvSensorSetData->cDCFanFaultErrorCount)				{bCheck = true;}

			for(int i=0; i<EP_MAX_SENSOR_CH; i++)
			{
				if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.gasSensorParam.bUseSensorFlag[i] != recvSensorSetData->gasSensorParam.bUseSensorFlag[i])		{bCheck = true;}
			}

			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.gasSensorParam.bUseLimit != recvSensorSetData->gasSensorParam.bUseLimit)					{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.gasSensorParam.lCutLimit != recvSensorSetData->gasSensorParam.lCutLimit)					{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.gasSensorParam.wJigTempErrorRefValue != recvSensorSetData->gasSensorParam.wJigTempErrorRefValue)			{bCheck = true;}
			if(m_pSensorDlg->m_pSensorMap->m_sendSensorSetData.gasSensorParam.wPowerTempErrorRefValue != recvSensorSetData->gasSensorParam.wPowerTempErrorRefValue)	{bCheck = true;}

			if(bCheck == true)
			{
				strTemp.Format("%s :: Sensor Limit Data MissMatch!!", ::GetModuleName(lModuleID));
				pDoc->WriteLog( strTemp );
				m_pSensorDlg->m_pSensorMap->m_arraySendError.Add(GetModuleName(lModuleID));
			}


			{
				CDaoDatabase  db;
				CString strSQL = _T("");				

				int sysType = ((CCTSMonApp*)AfxGetApp())->GetSystemType();


				EP_SYSTEM_PARAM *lpSysParam = EPGetSysParam(lModuleID);
				try
				{
					//Save setting to database
					db.Open(GetDataBaseName());
					CString strSQL;

					strTemp.Format("%d %d %d", recvSensorSetData->wWarningNo1, recvSensorSetData->wWarningNo2, recvSensorSetData->bUseError);

					strSQL.Format("UPDATE SystemConfig SET UseTempLimitFlag = %d, JigTempWarningRefValue = %d, JigTempErrorRefValue = %d, PowerTempWarningRefValue = %d, PowerTempErrorRefValue = %d, DangerTemperature = %d, Data1 = '%s', JigTargetTemp = %d, JigFanFaultErrorCount = %d, DCFanFaultErrorCount = %d WHERE ModuleID = %d", 
						recvSensorSetData->tempSensorParam.bUseLimit,
						recvSensorSetData->wJigTempWarningRefValue /100,
						recvSensorSetData->tempSensorParam.wJigTempErrorRefValue /100,
						recvSensorSetData->wPowerTempWarningRefValue / 100,
						recvSensorSetData->tempSensorParam.wPowerTempErrorRefValue/100,
						recvSensorSetData->tempSensorParam.lCutLimit ,
						strTemp,	
						recvSensorSetData->sErrorCumulativeBuffer,
						recvSensorSetData->cJigFanFaultErrorCount,
						recvSensorSetData->cDCFanFaultErrorCount,
						lModuleID);

					if(sysType != 0)
					{
						strTemp.Format(" AND ModuleType = %d", sysType);
						strSQL += strTemp;
					}				
					db.Execute(strSQL);
					db.Close();

				}
				catch(CDaoException *e)
				{
					// Simply show an error message to the user.
					m_pSensorDlg->m_pSensorMap->MessageBox(e->m_pErrorInfo->m_strDescription);
					e->Delete();
					return FALSE;
				}
			}

			if(m_pSensorDlg->m_pSensorMap->m_lastApplyModuleID == lModuleID)
			{
				int nSize = m_pSensorDlg->m_pSensorMap->m_arraySendError.GetCount();
				if(nSize>0)
				{
					CString strMsg = "Failure to apply some modules : ";
					for(int i=0; i<nSize ;i++ )
					{
						strMsg += m_pSensorDlg->m_pSensorMap->m_arraySendError.GetAt(i) + ", ";
					}
					m_pSensorDlg->m_pSensorMap->MessageBox(strMsg);
				}
				pDoc->ReLoadSensorSetting();
				m_pSensorDlg->m_pSensorMap->DisplayMapping(lModuleID);

				if(nSize == 0)
				{
					m_pSensorDlg->m_pSensorMap->MessageBox("success all");
				}

			}
		}
	}


	return 1;
}



void CMainFrame::ShowInformation( int nUnitNum, CString strMsg, int nMsgType )
{
	bool bFindDlg = FALSE;
	
	CInformationDlg *pInformationDlg;
	int i = 0;
	
	if (m_apInformationDlg.GetSize() > 0)
	{
		for ( i=0 ; i< m_apInformationDlg.GetSize() ; i++ )
		{
			pInformationDlg = (CInformationDlg *)m_apInformationDlg[i];
			if( pInformationDlg->GetUnitNum() == nUnitNum )
			{
				bFindDlg = TRUE;
				pInformationDlg->SetResultMsg( strMsg, nMsgType );
				if( !pInformationDlg->IsWindowVisible() )
				{
					// pInformationDlg->ShowWindow(SW_HIDE);
					pInformationDlg->ShowWindow(SW_SHOW);				
				}				
			}
		}
	}
	
	if( bFindDlg == FALSE )
	{
		pInformationDlg = new CInformationDlg();
		pInformationDlg->Create(IDD_SHOW_WANNING_DLG,  AfxGetMainWnd());		
		pInformationDlg->SetUnitNum( nUnitNum );
		pInformationDlg->SetResultMsg( strMsg, nMsgType );
		pInformationDlg->ShowWindow(SW_SHOW);
		m_apInformationDlg.Add(pInformationDlg);
	}
}

void CMainFrame::OnBfcali()
{
	//Dialog Permission Check
	// TODO: Add your command handler code here
	m_pCaliDlg = NULL;
	CCalibratorDlg *pCaliDlg;
	if (m_apCaliDlg.GetSize() > 0)
	{
		for (int i=0 ; i< m_apCaliDlg.GetSize() ; i++)
		{
			pCaliDlg = (CCalibratorDlg *)m_apCaliDlg[i];
 
			if (pCaliDlg->IsWindowVisible())
				continue;
			else
			{
				delete pCaliDlg;
				pCaliDlg = NULL;			
				m_apCaliDlg.RemoveAt(i);
			}
		}
	}

	//ljb 2011520 
	CSelUnitDlg *pSelCalibDlg;
	pSelCalibDlg = new CSelUnitDlg();
	pSelCalibDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pSelCalibDlg->SetTitle(TEXT_LANG[34]);
	if(pSelCalibDlg->DoModal() != IDOK)	
	{
		delete pSelCalibDlg;
		pSelCalibDlg = NULL;
		return;
	}
	int nModuleID = pSelCalibDlg->GetModuleID();
	delete pSelCalibDlg;
	pSelCalibDlg = NULL;	

// 	CFormModule *pModule = ((CCTSMonDoc *)GetActiveDocument())->GetModuleInfo(nModuleID);
// 	if(pModule == NULL)	return;
// 	WORD state = pModule->GetState();
// 
	if( ::EPGetProtocolVer(nModuleID) < 0x1001)				//version check
	{
		CString strTemp;
		strTemp.Format(TEXT_LANG[39], ::GetModuleName(nModuleID),  ::EPGetProtocolVer(nModuleID)); //::GetStringTable(IDS_MSG_NOT_SUPPORT_VERSION) //
		AfxMessageBox(strTemp);
		return;
	}

// 	if(state != EP_STATE_IDLE && state != EP_STATE_STANDBY)
// 	{
// 		CString strTemp;
// 		strTemp.Format("%s는 교정 가능한 상태가 아닙니다.", GetModuleName(nModuleID));
// 		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
// 	}
// 	else
// 	{
		if(LoginPremissionCheck(PMS_MODULE_CAL_UPDATE) == FALSE)
		{
			AfxMessageBox(TEXT_LANG[40] +" [Calibration]");
			return ;
		}
//	}

	if (m_apCaliDlg.GetSize() > 0)
	{
		for (int i=0 ; i< m_apCaliDlg.GetSize() ; i++)
		{
			pCaliDlg = (CCalibratorDlg *)m_apCaliDlg[i];
			if (pCaliDlg->m_nUnitNo == nModuleID)
			{
				AfxMessageBox(TEXT_LANG[35]);
				return;
			}
		}
		pCaliDlg =  new CCalibratorDlg(nModuleID, this);
		ASSERT(pCaliDlg);
		
		pCaliDlg->m_pdoc = (CCTSMonDoc *)GetActiveDocument();
		pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
		pCaliDlg->ShowWindow(SW_SHOW);
		m_apCaliDlg.Add(pCaliDlg);
	}
	else
	{
		pCaliDlg =  new CCalibratorDlg(nModuleID, this);
		ASSERT(pCaliDlg);
		
		pCaliDlg->m_pdoc = (CCTSMonDoc *)GetActiveDocument();
		pCaliDlg->Create(IDD_CALIBRATION_DLG, this);
		pCaliDlg->ShowWindow(SW_SHOW);
		m_apCaliDlg.Add(pCaliDlg);
	}
	m_pCaliDlg = pCaliDlg;

}

void CMainFrame::OnUpdateBfcali(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CMainFrame::OnUpdateAccuracyTest(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

BOOL CMainFrame::Fun_FindJigID(CString strJig,int &nModuleID, int &nJigID)
{
	CDaoDatabase  db;
	nModuleID = 0;
	nJigID = 0;
	
	try
	{
		db.Open(::GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL, strTemp;
	strSQL.Format("SELECT ModuleID, JigNo FROM JigID WHERE ID = '%s'", strJig);
	//	strSQL.Format("SELECT ModuleID, No INTO FROM JigID WHERE ID = '%s'", strID);
	
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			nModuleID = data.lVal;
			data = rs.GetFieldValue(1);
			nJigID = data.lVal;
		}
		rs.Close();
		db.Close();
		
		if(nModuleID < 1 || nJigID < 1)
		{
			return FALSE;
			TRACE("Find error\n");
		}
	}
	catch (CDaoException* e)
	{
		//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	return TRUE;
}

void CMainFrame::OnUpsConfig() 
{
	// TODO: Add your command handler code here
	m_bChkUpsThread = false;

	Sleep(1000);

	UpsSettingDlg dlg;
	
	if(dlg.DoModal() == IDOK)
	{
		m_iUPSFactory = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UPS_FACTORY", 0);		
		m_bChkUpsThread = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UPS_RUN", 0);
		int index = m_wndStatusBar.CommandToIndex(ID_UPS_RUN_STATE);		
		if( m_bChkUpsThread )
		{
			CString str(TEXT_LANG[36]); //"UPS 감지중"
			if(index >=0 )
			{				
				m_wndStatusBar.SetPaneText(index, str);
			}

			// 1. UPS thread 시작
			m_pPCM_AliveProcessThread = AfxBeginThread(Fun_UPSChkThread, this);			
		} 
		else {
			CString str(TEXT_LANG[37]); //"UPS OFF"
			if(index >=0 )
			{
				m_wndStatusBar.SetPaneText(index, str);
			}
		}
	}
}

UINT CMainFrame::Fun_UPSChkThread(LPVOID lParam)
{
	CMainFrame *UpsChk;
	UpsChk = (CMainFrame *)lParam;
	
	HANDLE hSnapShot;
	int nCnt = 0;
	PROCESSENTRY32 pEntry;
	bool hRes = true;
	bool bUPSRun = false;
	bool bPreStatus = false;
	CString strTemp = _T("");
	char buf[256];

	int nRtn = -1;

	HWND pWnd;
	HWND pWnd1;

	CString strFileName = _T("");
	CStdioFile sourceFile;			
	CFileException ex;
	
	CString strReadData;
	CString strInputVoltage;
	CString strData;
	CString strMsg;
	
	while( true )
	{
		::Sleep(2000);

		if( UpsChk->m_bChkUpsThread == false )
		{
			return 0;
		}

		// 1. 0:APC, 1:PCM		
		if( UpsChk->m_iUPSFactory == 0 )
		{
			nCnt = 0;
			bUPSRun = false;
			hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL,NULL);
			pEntry.dwSize =sizeof(pEntry);		
			
			Process32First (hSnapShot, &pEntry);
			hRes = true;
			
			hRes = Process32Next(hSnapShot, &pEntry);
			
			while( hRes )
			{		
				if((strcmp(pEntry.szExeFile, "apctray.exe") == 0 ) && pEntry.cntThreads >= 1 ) 
				{
					bUPSRun = true;	
					break;				
				}
				else
				{
					hRes = Process32Next (hSnapShot, &pEntry);		
				}			
			}
			
			CloseHandle (hSnapShot);
			
			if( bUPSRun == true )
			{
				strFileName.Format("C:\\apcupsd\\etc\\apcupsd\\apcupsd.events");				
				
				// 1. 이벤트 파일이 존재하는지 확인
				
				if( !sourceFile.Open( strFileName, CFile::shareDenyNone, &ex ))
				{			
					/*
					ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
					UpsAliveChk->WriteAPCLog(strMsg);
					*/
				}
				else
				{
					while(TRUE)	
					{			
						sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.	
						
						if( strReadData.IsEmpty() )
						{
							break;
						}
						else
						{
							strData = strReadData;
						}
					}					
				}
				sourceFile.Close();	

				if( !strData.IsEmpty() )
				{
					if( bPreStatus == false )
					{
						nRtn = strData.Find("Power failure");
						if( nRtn > 0 )
						{
							// 1. 정전신호 감지
							// 2. 로그 파일 삭제
							bPreStatus = true;
						}
					}
					else
					{
						nRtn = strData.Find("Power is back");
						if( nRtn > 0 )
						{
							// 1. 정전신호 감지
							// 2. 로그 파일 삭제
							bPreStatus = false;						
						}
					}
				}
			}
		}
		else if( UpsChk->m_iUPSFactory == 1 )
		{
			nCnt = 0;
			bUPSRun = false;
			hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL,NULL);
			pEntry.dwSize =sizeof(pEntry);		
			
			Process32First (hSnapShot, &pEntry);
			hRes = true;
			
			hRes = Process32Next (hSnapShot, &pEntry);
			
			while( hRes )
			{		
				if((strcmp(pEntry.szExeFile, "UPSMON.exe") == 0 ) && pEntry.cntThreads >= 1 ) 
				{
					bUPSRun = true;	
					break;				
				}
				else
				{
					hRes = Process32Next (hSnapShot, &pEntry);		
				}			
			}
			
			CloseHandle (hSnapShot);
			
			if( bUPSRun == true )
			{
				pWnd = ::FindWindow("TnUPSMONBack", NULL);
				
				ZeroMemory( buf, sizeof(buf));
				
				pWnd1 = ::FindWindowEx(pWnd, NULL, "TMemo", NULL);
				
				// int nSize = ::GetWindowTextLength(pWnd1);
				// ::GetWindowText(pWnd1, buf, 256);
				
				::SendMessage(pWnd1, WM_GETTEXT,256,(LPARAM)buf);
				
				strTemp.Format("%s", buf);
				nCnt = strTemp.Find("Data");
				
				if( nCnt == -1 )
				{
					if( bPreStatus != false )
					{
						bPreStatus = false;						
					}
				}
				else
				{
					AfxExtractSubString(strData, strTemp, 3, ':');
					strData.TrimLeft();
					AfxExtractSubString(strInputVoltage, strData, 2, ' ');
					
					if( atoi(strInputVoltage) == 0 )
					{
						//  정전
						if( bPreStatus != true )
						{
							bPreStatus = true;							
						}
					}
					else
					{
						// 정상
						if( bPreStatus != false )
						{
							bPreStatus = false;							
						}
					}
				}			
			}
		}
	}
}

void CMainFrame::OnSystemSetting()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	
	if( m_pSystemSettingdlg == NULL )
	{
		m_pSystemSettingdlg = new CSystemsettingDlg( (CCTSMonDoc *)GetActiveDocument() );
		
		if( m_pSystemSettingdlg->m_hWnd == NULL )
		{
			m_pSystemSettingdlg->Create(IDD_SYSTEM_SETTING_DLG, this);							
		}
	}

	m_pSystemSettingdlg->CenterWindow();

	m_pSystemSettingdlg->LoadSetting();

	m_pSystemSettingdlg->ShowWindow(SW_SHOW);
	
	/*
	m_pSystemSettingdlg = new CSystemsettingDlg( (CCTSMonDoc *)GetActiveDocument() );
	// m_pSystemSettingdlg->CenterWindow();
	// m_pSystemSettingdlg->LoadSetting();
	if( m_pSystemSettingdlg->DoModal() != IDOK )
	{

	}

	delete m_pSystemSettingdlg;
	m_pSystemSettingdlg = NULL;
	*/
}

void CMainFrame::OnPrecision()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	
	if( m_pPrecisionDlg == NULL )
	{
		m_pPrecisionDlg = new CPrecisionDlg((CCTSMonDoc *)GetActiveDocument());
		
		if( m_pPrecisionDlg->m_hWnd == NULL )
		{
			m_pPrecisionDlg->Create(IDD_PRECISION_DLG, this);	
		}
	}
	
	m_pPrecisionDlg->CenterWindow();
	
	m_pPrecisionDlg->ShowWindow(SW_SHOW);
}

LRESULT CMainFrame::OnModuleInfoChange(WPARAM wParam, LPARAM lParam)
{
	SBCQueue_DATA QData;

	while(EPQueueDataIsEmpty() == FALSE)
	{
		EPQueueDataPop(QData);

		switch(QData.command)
		{
		case EP_CMD_MODULE_DISCONNECT:
			{
				OnModuleDisConnected((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_MODULE_CONNECT:
			{
				OnModuleConnected((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_STEPDATA:
			{
				
			}
			break;
		case EP_CMD_LINE_MODE_DATA:
			{
				OnModuleStateChange((WPARAM)QData.id, (LPARAM)&QData.Data);			
			}
			break;
		case EP_CMD_AUTO_GP_STATE_DATA:
			{
				OnModuleStateChange((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_AUTO_GP_DATA:
			{
			}
			break;			
		case EP_CMD_AUTO_GP_STEP_END_DATA:
			{
				OnSaveDataReceive((WPARAM)QData.id, (LPARAM)&QData.Data);				
			}
			break;
		case EP_CMD_TAG_INFO:
			{
				OnBarCodeReceive((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_AUTO_CHECK_RESULT:
			{
				OnCheckEnded((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_AUTO_EMG_DATA:
			{
				OnModuleEmg((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_USER_CMD:
			{
				OnUserCmdReceive((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;		
		case EP_CMD_REAL_TIME_DATA:
			{
				OnRealTimeDataReceive((WPARAM)QData.id, (LPARAM)&QData.Data);				
			}
			break;
		case EP_CMD_CAL_RES:
		case EP_CMD_CHK_RES:
			{
				OnCalResultReceive((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_CAL_END:
		case EP_CMD_CHK_END:
			{
				OnCalEndReceive((WPARAM)QData.id, (LPARAM)&QData.Data);
				
			}break;
		case EP_CMD_RM_RES:
			{
				OnRealMeasEndReceive((WPARAM)QData.id, (LPARAM)&QData.Data);				
			}
			break;		
		case EP_CMD_RM_STOP_RESPONS:
			{
				OnRealMeasWorkEndReceive((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_TO_PC_GAIN_READ_RES: //20191220 엄륭 Gain 추가 관련
			{
				OnGainReadReceive((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
		case EP_CMD_MISWIRING_REC_DATA:
			{
				//OnCalEndReceive((WPARAM)QData.id, (LPARAM)&QData.Data);
				OnMiswiringDataReceive((WPARAM)QData.id, (LPARAM)&QData.Data);

			}
			break;
		case EP_CMD_SENSOR_LIMIT_RES:	//20200831 ksj
			{
				OnSensorSettingDataRecive((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_SBC_PARAM_RESPONS:
			{
				OnReciveSbcParam((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;

		case EP_CMD_SBC_CALI_DATA_RESPONS:
			{
				OnReciveSbcCaliData((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
		case EP_CMD_SBC_ALL_VERSION_RESPONS:
			{
				OnReciveSbcVersionAllData((WPARAM)QData.id, (LPARAM)&QData.Data);
			}
			break;
// 20210129 KSCHOI Add ERCD Write Value Function START
		case EP_CMD_WRITE_ERCD_VALUE_RESPONS:
			{
				if(this->m_pErcdWriteDlg != NULL)
				{
					EP_ERCD_WRITE_VALUE pERCDWriteValue;
					memcpy(&pERCDWriteValue, QData.Data, sizeof(EP_ERCD_WRITE_VALUE));

					m_pErcdWriteDlg->OnSBCResponse(HIWORD(QData.id), pERCDWriteValue);
				}
			}
			break;
// 20210129 KSCHOI Add ERCD Write Value Function END
		case EP_CMD_FAN_INFORMATION_RESPONSE:
			{
				if(this->m_pFanModeSettingDlg != NULL)
				{
					EP_FAN_INFORMATION_RESPONSE response;
					memcpy(&response, QData.Data, sizeof(EP_FAN_INFORMATION_RESPONSE));

					m_pFanModeSettingDlg->OnFanModeSettingResponse(HIWORD(QData.id), response);
				}
			}
			break;
		case EP_CMD_MISWIRING_UNWIRED_CHECK_RESPONSE:
			{
				if(this->m_pMiswringUnwiringDlg != NULL)
				{
					EP_CMD_CHANNEL_MISWIRING_UNWIRED_CHECK_RESPONSE response;
					memcpy(&response, QData.Data, sizeof(EP_CMD_CHANNEL_MISWIRING_UNWIRED_CHECK_RESPONSE));

					m_pMiswringUnwiringDlg->OnMiswiringUnwiredCheckResponse(HIWORD(QData.id), response);
				}
			}
			break;
		case EP_CMD_NOTIFY_ONE_SENSOR_TEMP_CALI_END	:
			{
				if(this->m_pSensorDlg != NULL)
				{
					EP_CMD_NOTIFY_ONE_SENSOR_TEMPERATURE_CALIBRATION_END pOneSensorTempCaliEndData;
					memcpy(&pOneSensorTempCaliEndData, QData.Data, sizeof(EP_CMD_NOTIFY_ONE_SENSOR_TEMPERATURE_CALIBRATION_END));

					m_pSensorDlg->OnOneSensorTempCaliEndNotify(HIWORD(QData.id), pOneSensorTempCaliEndData);
				}
			}
			break;
		case EP_CMD_NOTIFY_ALL_SENSOR_TEMP_CALI_END	:
			{
				if(this->m_pSensorDlg != NULL)
				{
					m_pSensorDlg->OnAllSensorTempCaliEndNotify(HIWORD(QData.id));
				}
			}
			break;
		//case EP_CMD_CAL_RESULT:
		//	{
		//		OnSetCalResultData(wParam, (LPARAM)&QData.Data);
		//	}
		//	break;
		//case EP_CMD_AUTO_GP_STEP_SEC_DATA:
		//	{
		//		OnSave05DataReceive(wParam, (LPARAM)&QData.Data);
		//	}
		//	break;
		//case EP_CMD_REAL_CHAMBER_DATA:
		//	{
		//		OnReceiveRealChamberData(wParam, (LPARAM)&QData.Data);
		//	}
		//	break;
		//case EP_CMD_SWITCH:
		//	{
		//		OnSwitchDetect(wParam, (LPARAM)&QData.Data);
		//	}
		//	break;
		}
	}
	return 0;
}

VOID CMainFrame::SBCPROCThreadCallback(VOID)
{
	//DWORD KeepAlive = 0xFFFF;
	//INT64 preByte = 0;
	while (TRUE)
	{
		DWORD Result = WaitForSingleObject(mSBCPROCThreadDestroyEvent, 100);

		if (Result == WAIT_OBJECT_0)
			return;
		SBCQueue_DATA QData;

		while(EPQueueDataIsEmpty() == FALSE)
		{
			EPQueueDataPop(QData);

			switch(QData.command)
			{
			case EP_CMD_STEPDATA:
				{
					SendMessage(EPWM_MODULE_ONLINE_STEPLIST, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_AUTO_GP_STATE_DATA:
				{
					SendMessage(EPWM_MODULE_STATE_CHANGE, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_AUTO_GP_STEP_END_DATA:
				{
					//OnSaveDataReceive(QData.id, (LPARAM)&QData.Data);
					SendMessage(EPWM_STEP_ENDDATA_RECEIVE, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_TAG_INFO:
				{
					//OnBarCodeReceive(QData.id, (LPARAM)&QData.Data);
					SendMessage(EPWM_NVRAM_HEADER, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_AUTO_CHECK_RESULT:
				{
					//OnCheckEnded(QData.id, (LPARAM)&QData.Data);
					SendMessage(EPWM_CHECK_RESULT, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_AUTO_EMG_DATA:
				{
					//OnModuleEmg(QData.id, (LPARAM)&QData.Data);
					SendMessage(EPWM_MODULE_EMG, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_USER_CMD:
				{
					//OnUserCmdReceive(QData.id, (LPARAM)&QData.Data);
					SendMessage(EPWM_USER_CMD, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_REAL_TIME_DATA:
				{
					SendMessage(EPWM_REALTIME_DATA_RECEIVED, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_CAL_RES:
			case EP_CMD_CHK_RES:
				{
					SendMessage(EPWM_CAL_DATA_RECEIVED, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_CAL_END:
			case EP_CMD_CHK_END:
				{
					SendMessage(EPWM_CAL_END_RECEIVED, QData.id, (LPARAM)&QData.Data);
				}break;
			case EP_CMD_RM_RES:
				{
					SendMessage(EPWM_RM_END_RECEIVED, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_RM_STOP_RESPONS:
				{
					SendMessage(EPWM_RM_WORK_END_RECEIVED, QData.id, (LPARAM)&QData.Data);
				}
				break;
			case EP_CMD_TO_PC_GAIN_READ_RES:
				{
					SendMessage(EPWM_GAIN_READ_CMD, QData.id, (LPARAM)&QData.Data);   //20191220 엄륭 Gain 관련 추가
				}
				break;
			case EP_CMD_MISWIRING_REC_DATA:
				{
					SendMessage(EPWM_MISWIRING_DATA_RECV, QData.id, (LPARAM)&QData.Data);   //20200130KSJ MISWIRING
				}
				break;
			case EP_CMD_SAFETY_VERSION_RESPONS:
				{
					OnReciveFwVersion((WPARAM)QData.id, (LPARAM)&QData.Data);
				}
				break;
			//case EP_CMD_CAL_RESULT:
			//	{
			//		//OnSetCalResultData(QData.id, (LPARAM)&QData.Data);
			//		SendMessage(EPWM_CALI_RESULT_DATA, QData.id, (LPARAM)&QData.Data);
			//	}
			//	break;
			//case EP_CMD_AUTO_GP_STEP_SEC_DATA:z
			//	{
			//		//OnSave05DataReceive(QData.id, (LPARAM)&QData.Data);
			//		SendMessage(EPWM_STEP_05DATA_RECEIVE, QData.id, (LPARAM)&QData.Data);
			//	}
			//	break;
			//case EP_CMD_REAL_CHAMBER_DATA:
			//	{
			//		//OnReceiveRealChamberData(QData.id, (LPARAM)&QData.Data);
			//		SendMessage(EPWM_CHAMBER_DATA, QData.id, (LPARAM)&QData.Data);
			//	}
			//	break;
			//case EP_CMD_SWITCH:
			//	{
			//		//OnSwitchDetect(QData.id, (LPARAM)&QData.Data);
			//		SendMessage(EPWM_SWITCH_DETECT, QData.id, (LPARAM)&QData.Data);
			//	}
			//	break;
			}
		}
	}
}

void CMainFrame::OnTestDlg()
{
	if(m_pTestDlg == NULL)
	{
		m_pTestDlg = new CTestDlg;
		if(m_pTestDlg->m_hWnd == NULL)
		{
			m_pTestDlg->Create(IDD_DLG_TEST, this);
		}
	}
	
	m_pTestDlg->CenterWindow();
	
	m_pTestDlg->ShowWindow(SW_SHOW);	
}

LRESULT CMainFrame::OnStateChange(WPARAM wParam, LPARAM lParam)
{
	EPSetGroupState(lParam, 1, wParam);

	return 0;
}

LRESULT CMainFrame::OnFmsConnected(WPARAM wParam, LPARAM lParam)
{
	if( m_pAllSystemView )
	{ 
		m_pAllSystemView->FnSetFmsCommState(TRUE);
	}
	return 0;
}

LRESULT CMainFrame::OnFmsClosed(WPARAM wParam, LPARAM lParam)
{
	if( m_pAllSystemView )
	{ 
		m_pAllSystemView->FnSetFmsCommState(FALSE);
	}	
	return 0;
}

void CMainFrame::OnSbcLogchk()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	if( m_pSbcLogChkDlg == NULL )
	{
		m_pSbcLogChkDlg = new CSbcLogChkDlg((CCTSMonDoc *)GetActiveDocument());

		if( m_pSbcLogChkDlg->m_hWnd == NULL )
		{
			m_pSbcLogChkDlg->Create(IDD_SBC_LOGCHK_DLG, this);	
		}
	}

	m_pSbcLogChkDlg->CenterWindow();

	m_pSbcLogChkDlg->ShowWindow(SW_SHOW);
}

void CMainFrame::OnClampCntchk()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	//Get ModuleID
	CSelUnitDlg *pSelCalibDlg;
	pSelCalibDlg = new CSelUnitDlg();
	pSelCalibDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pSelCalibDlg->SetTitle(TEXT_LANG[34]);
	if(pSelCalibDlg->DoModal() != IDOK)	
	{
		delete pSelCalibDlg;
		pSelCalibDlg = NULL;
		return;
	}
	int nModuleID = pSelCalibDlg->GetModuleID();
	delete pSelCalibDlg;
	pSelCalibDlg = NULL;	


	if( m_pClampCountChkDlg == NULL )
	{
		m_pClampCountChkDlg = new CClampCountChkDlg((CCTSMonDoc *)GetActiveDocument(),nModuleID);

		if( m_pClampCountChkDlg->m_hWnd == NULL )
		{
			m_pClampCountChkDlg->Create(IDD_CLAMP_CNTCHK_DLG, this);	
		}
	}
	else
	{
		m_pClampCountChkDlg->m_nModuleID = nModuleID;
		m_pClampCountChkDlg->UpdateView();
		m_pClampCountChkDlg->fnUpdateCnt();
	}

	m_pClampCountChkDlg->CenterWindow();

	m_pClampCountChkDlg->OnBnClickedSelectAllOffBtn();

	m_pClampCountChkDlg->ShowWindow(SW_SHOW);
}

void CMainFrame::OnViewSet()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CViewSet *pDlg;
	pDlg = new CViewSet;
	if(pDlg == NULL)	return;

	if(::PasswordCheck() == FALSE)	return;

	if (pDlg->DoModal() == IDOK)
	{
	}

	delete pDlg;
	pDlg = NULL;
}

void CMainFrame::OnCalibrationUpdateInfo()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	if( m_pCalibrationUpdateTimeDlg == NULL )
	{
		m_pCalibrationUpdateTimeDlg = new CCalibrationUpdateTimeDlg((CCTSMonDoc *)GetActiveDocument());

		if( m_pCalibrationUpdateTimeDlg->m_hWnd == NULL )
		{
			m_pCalibrationUpdateTimeDlg->Create(IDD_CALIBRATION_UPDATECHK_DLG, this);	
		}
	}

	m_pCalibrationUpdateTimeDlg->CenterWindow();

	m_pCalibrationUpdateTimeDlg->Update();

	m_pCalibrationUpdateTimeDlg->ShowWindow(SW_SHOW);
}


LRESULT CMainFrame::OnReciveFwVersion(WPARAM wParam, LPARAM lParam)
{
	long lModuleID = HIWORD(wParam);
	WORD nMode = LOWORD(wParam);
	if( m_pVersionCheckDlg != NULL)
	{
		m_pVersionCheckDlg->UpdateValue(lModuleID);
	}
	return TRUE;
}

void CMainFrame::OnMiswiringCheck()
{
	// TEST EMG START
	// 	WPARAM wParam;
	// 	wParam = 1 << 16;
	// 	wParam ^= 1;
	// 
	// 
	// 	LPARAM lParam;
	// 	lParam = 3 << 16;
	// 	lParam ^= 122;
	// 
	// 	this->OnModuleEmg(wParam, (LPARAM)&lParam);
	// TEST EMG END
	if(m_pMiswringUnwiringDlg == NULL)
	{
		m_pMiswringUnwiringDlg = new CMiswiringUnwiredDlg((CCTSMonDoc *)GetActiveDocument(), this, m_pAllSystemView->GetCurModuleID());

		m_pMiswringUnwiringDlg->Create(IDD_MISWIRING_UNWIRED_DLG, this);		
		m_pMiswringUnwiringDlg->ShowWindow(TRUE);
	}
	else
	{
		m_pMiswringUnwiringDlg->SetCurrentModuleNo(m_pAllSystemView->GetCurModuleID());
		m_pMiswringUnwiringDlg->ShowWindow(TRUE);
	}

	return;
	// 창 정리
	m_pMissDlg = NULL; //2020-02-20 엄륭 오배선 관련 

	CMisWiringDlg *pMisWiringDlg;
	if (m_apMisWiringDlg.GetSize() > 0)
	{
		for (int i=0 ; i< m_apMisWiringDlg.GetSize() ; i++)
		{
			pMisWiringDlg = (CMisWiringDlg *)m_apMisWiringDlg[i];

			if (pMisWiringDlg->IsWindowVisible())
				continue;
			else
			{
				delete pMisWiringDlg;
				pMisWiringDlg = NULL;			
				m_apMisWiringDlg.RemoveAt(i);
			}
		}
	}

	//Get ModuleID
	CSelUnitDlg *pMSelCalibDlg;
	pMSelCalibDlg = new CSelUnitDlg();
	pMSelCalibDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pMSelCalibDlg->SetTitle(TEXT_LANG[34]);
	if(pMSelCalibDlg->DoModal() != IDOK)	
	{
		delete pMSelCalibDlg;
		pMSelCalibDlg = NULL;
		return;
	}
	int nModuleID = pMSelCalibDlg->GetModuleID();
	delete pMSelCalibDlg;
	pMSelCalibDlg = NULL;	



	if (m_apMisWiringDlg.GetSize() > 0)
	{
		for (int i=0 ; i< m_apMisWiringDlg.GetSize() ; i++)
		{
			pMisWiringDlg = (CMisWiringDlg *)m_apMisWiringDlg[i];
			if (pMisWiringDlg->m_nModuleID == nModuleID)
			{
				AfxMessageBox(TEXT_LANG[35]);
				return;
			}
		}
		pMisWiringDlg =  new CMisWiringDlg(nModuleID, this);
		ASSERT(pMisWiringDlg);

		pMisWiringDlg->m_pdoc = (CCTSMonDoc *)GetActiveDocument();
		pMisWiringDlg->Create(IDD_MISWIRING_CHECK_DLG, this);
		pMisWiringDlg->ShowWindow(SW_SHOW);
		m_apMisWiringDlg.Add(pMisWiringDlg);
	}
	else
	{
		pMisWiringDlg =  new CMisWiringDlg(nModuleID, this);
		ASSERT(pMisWiringDlg);

		pMisWiringDlg->m_pdoc = (CCTSMonDoc *)GetActiveDocument();
		pMisWiringDlg->Create(IDD_MISWIRING_CHECK_DLG, this);
		pMisWiringDlg->ShowWindow(SW_SHOW);
		m_apMisWiringDlg.Add(pMisWiringDlg);
	}

	m_pMissDlg = pMisWiringDlg;   //2020-02-20 엄륭 오배선 관련 

}

// 20201103 KSCHOI Add Build Date Display START
CString CMainFrame::MacroDateToCString(const char *MacroDate)
{
	const int comfile_date_len = 12;

	// error check
	if (NULL == MacroDate || comfile_date_len - 1 != strlen(MacroDate))
		return "";

	const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";

	char s_month[5] = {0,};
	int iyear = 0, iday = 0;

	sscanf(MacroDate, "%s %d %d", s_month, &iday, &iyear);
	int imonth = (strstr(month_names, s_month) - month_names) / 3 + 1;

	CString strDate;
	strDate.Format("%04d-%02d-%02d",iyear,imonth,iday);
	return strDate;
}
// 20201103 KSCHOI Add Build Date Display END


void CMainFrame::OnSbcSettingCheck()
{
// 20201204 KSCHOI Add PasswordCheck At SBCCaliDataCheck, SBCSettingValueView Function START
	if(::PasswordCheck() == FALSE)	return;
// 20201204 KSCHOI Add PasswordCheck At SBCCaliDataCheck, SBCSettingValueView Function END

	CSelUnitDlg *pSelCalibDlg;
	pSelCalibDlg = new CSelUnitDlg();
	pSelCalibDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pSelCalibDlg->SetTitle(TEXT_LANG[34]);
	if(pSelCalibDlg->DoModal() != IDOK)	
	{
		delete pSelCalibDlg;
		pSelCalibDlg = NULL;
		return;
	}
	int nModuleID = pSelCalibDlg->GetModuleID();
	delete pSelCalibDlg;
	pSelCalibDlg = NULL;

	if( m_pSettingCheckDlg == NULL )
	{
		CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
		ASSERT(pDoc);	

		m_pSettingCheckDlg = new CSettingCheckDlg(this, pDoc);

		if( m_pSettingCheckDlg->m_hWnd == NULL )
		{
			m_pSettingCheckDlg->Create(IDD_SETTING_CHECK, this);	
		}

	}
	m_pSettingCheckDlg->CenterWindow();
	m_pSettingCheckDlg->ShowWindow(SW_SHOW);
	m_pSettingCheckDlg->SetCurrentModule(nModuleID);
}


void CMainFrame::OnSbcCaliDataCheck()
{
// 20201204 KSCHOI Add PasswordCheck At SBCCaliDataCheck, SBCSettingValueView Function START
	if(::PasswordCheck() == FALSE)	return;
// 20201204 KSCHOI Add PasswordCheck At SBCCaliDataCheck, SBCSettingValueView Function END

	CSelUnitDlg *pSelCalibDlg;
	pSelCalibDlg = new CSelUnitDlg();
	pSelCalibDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pSelCalibDlg->SetTitle(TEXT_LANG[34]);
	if(pSelCalibDlg->DoModal() != IDOK)	
	{
		delete pSelCalibDlg;
		pSelCalibDlg = NULL;
		return;
	}
	int nModuleID = pSelCalibDlg->GetModuleID();
	delete pSelCalibDlg;
	pSelCalibDlg = NULL;

	if( m_pSbcCaliDataDlg == NULL )
	{
		CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
		ASSERT(pDoc);	

		m_pSbcCaliDataDlg = new CSbcCaliDataDlg(this, pDoc);

		if( m_pSbcCaliDataDlg->m_hWnd == NULL )
		{
			m_pSbcCaliDataDlg->Create(IDD_SBC_CALI_DATA_DLG, this);	
		}

	}
	m_pSbcCaliDataDlg->CenterWindow();
	m_pSbcCaliDataDlg->ShowWindow(SW_SHOW);
	m_pSbcCaliDataDlg->SetCurrentModule(nModuleID);
}
/////////////////////////////////////////////20210406 KSJ D ERCD 코드 추가 START
void CMainFrame::OnWriteErcdCode()
{
	CPasswordChkDlg pDlg;
	pDlg.m_bDiffPassword = TRUE;
	pDlg.m_strTitle = "Super Administrator";
	pDlg.m_strDiffPassword = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION, "SuperPassword", "sk1234");
	if( pDlg.DoModal() != IDOK)
	{
		return ;
	}

	CSelUnitDlg *pSelCalibDlg;
	pSelCalibDlg = new CSelUnitDlg();
	pSelCalibDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pSelCalibDlg->SetTitle("SBC Setting Parameter");
	if(pSelCalibDlg->DoModal() != IDOK)	
	{
		delete pSelCalibDlg;
		pSelCalibDlg = NULL;
		return;
	}
	int nModuleID = pSelCalibDlg->GetModuleID();
	delete pSelCalibDlg;
	pSelCalibDlg = NULL;

	if( m_pErcdWriteDlg == NULL )
	{
		CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
		ASSERT(pDoc);	

		m_pErcdWriteDlg = new CErcdWriteDlg(this, pDoc);

		if( m_pErcdWriteDlg->m_hWnd == NULL )
		{
			m_pErcdWriteDlg->Create(IDD_WRITE_ERCD_CODE, this);	
		}

	}
	m_pErcdWriteDlg->CenterWindow();
	m_pErcdWriteDlg->ShowWindow(SW_SHOW);
	m_pErcdWriteDlg->SetCurrentModule(nModuleID);
}
/////////////////////////////////////////////20210406 KSJ D ERCD 코드 추가 END
void CMainFrame::OnVersionCheck()
{
	if(::PasswordCheck() == FALSE)	return;

	if( m_pVersionCheckDlg == NULL )
	{
		CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
		ASSERT(pDoc);	

		m_pVersionCheckDlg = new CVersionCheckDlg(this, pDoc);

		if( m_pVersionCheckDlg->m_hWnd == NULL )
		{
			m_pVersionCheckDlg->Create(IDD_VERSION_CHECK, this);	
		}

	}
	m_pVersionCheckDlg->CenterWindow();
	m_pVersionCheckDlg->ShowWindow(SW_SHOW);
	m_pVersionCheckDlg->UpdateValue();

}
void CMainFrame::OnStageVersion()
{
	// 20201204 KSCHOI Add PasswordCheck At SBCCaliDataCheck, SBCSettingValueView Function START
	if(::PasswordCheck() == FALSE)	return;
	// 20201204 KSCHOI Add PasswordCheck At SBCCaliDataCheck, SBCSettingValueView Function END

	CSelUnitDlg *pSelCalibDlg;
	pSelCalibDlg = new CSelUnitDlg();
	pSelCalibDlg->SetModuleID(m_pTab->m_pTopView->GetCurModuleID());
	pSelCalibDlg->SetTitle("Select the unit to Stage Version");
	if(pSelCalibDlg->DoModal() != IDOK)	
	{
		delete pSelCalibDlg;
		pSelCalibDlg = NULL;
		return;
	}

	int nModuleID = pSelCalibDlg->GetModuleID();
	delete pSelCalibDlg;
	pSelCalibDlg = NULL;

	if( m_pStageVersion == NULL )
	{
		CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
		ASSERT(pDoc);	

		m_pStageVersion = new CStageVersion(this, pDoc);

		if( m_pStageVersion->m_hWnd == NULL )
		{
			m_pStageVersion->Create(IDD_STAGE_VERSION, this);	
		}

	}
	m_pStageVersion->CenterWindow();
	m_pStageVersion->ShowWindow(SW_SHOW);
	m_pStageVersion->SetCurrentModule(nModuleID);
}
