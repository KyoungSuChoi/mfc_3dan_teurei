#pragma once


// CCalibrationUpdateTimeDlg 대화 상자입니다.
#include "MyGridWnd.h"
#include "CTSMonDoc.h"

#define GRID_DATA_FONT_SIZE	16
#define CALI_UPDATE_ALL		0
#define CALI_UPDATE_NEED	1

class CCalibrationUpdateTimeDlg : public CDialog
{
	DECLARE_DYNAMIC(CCalibrationUpdateTimeDlg)

public:
	CString *TEXT_LANG;
	bool LanguageinitMonConfig(CString strClassName);

	CCalibrationUpdateTimeDlg(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCalibrationUpdateTimeDlg();

	CMyGridWnd	m_wndLayoutGrid;
	CCTSMonDoc	*m_pDoc;
	CFont		m_Font;
	bool		m_bRackIndexUp;

	void InitGridWnd();	
	void InitLabel();
	void InitColorBtn();
	BOOL Update(int nType = CALI_UPDATE_ALL);		// 0:All, 1:교정기간초과

	BOOL GetModuleRowCol(int nModuleID, int &nRow, int &nCol);

	int	m_nLayOutRow;
	int m_nLayOutCol;
	int m_nMaxStageCnt;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CALIBRATION_UPDATECHK_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CLabel m_LabelName;
	afx_msg void OnBnClickedCsvOutput();
};
