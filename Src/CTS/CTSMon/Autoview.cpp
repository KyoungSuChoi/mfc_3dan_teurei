// OperationView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "Autoview.h"
#include "MainFrm.h"

#define EDIT_FONT_SIZE 28
#define BTN_FONT_SIZE 30

IMPLEMENT_DYNCREATE(CAutoview, CFormView)

CAutoview::CAutoview()
: CFormView(CAutoview::IDD)
{
	LanguageinitMonConfig(_T("CAutoview"));
	m_nCurModuleID = 1;
	m_nPrevStep = 0;
	m_pConfig = NULL;
	m_nDisplayTimer = 0;
	m_pChStsSmallImage = NULL;
	m_strPreTrayId = _T("");
	m_strPreUpTrayId = _T("");

}

CAutoview::~CAutoview()
{
	if(m_pChStsSmallImage)
	{
		delete m_pChStsSmallImage;
	}
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CAutoview::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
			else
			{
				TEXT_LANG[i].Replace("\\d","\d");
				TEXT_LANG[i].Replace("\\s","\s");
				TEXT_LANG[i].Replace("\\n","\n");
				TEXT_LANG[i].Replace("\\N","\N");
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CAutoview::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
	DDX_Control(pDX, IDC_STEP_LIST, m_ctrlStepList);
	DDX_Control(pDX, IDC_LABEL_LINEMODE, m_ctrlLineMode);		
	DDX_Control(pDX, IDC_FMS_RESET1_BTN_AUTO, m_Btn_FmsReset1);
	DDX_Control(pDX, IDC_FMS_RESET2_BTN_AUTO, m_Btn_FmsReset2);
	DDX_Control(pDX, IDC_FMS_END_CMD_SEND_BTN_AUTO, m_Btn_FmsEndCommandSend);
	DDX_Control(pDX, IDC_FMS_OUT_CMD_SEND_BTN_AUTO, m_Btn_FmsOutCommandSend);
}

BEGIN_MESSAGE_MAP(CAutoview, CFormView)
	ON_WM_SIZE()
	//	ON_WM_TIMER()
	ON_NOTIFY(NM_DBLCLK, IDC_STEP_LIST, &CAutoview::OnNMDblclkStepList)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_STEP_LIST, &CAutoview::OnGetdispinfoStepList)
	ON_BN_CLICKED(IDC_FMS_RESET1_BTN_AUTO, &CAutoview::OnBnClickedFmsReset1Btn)
	ON_BN_CLICKED(IDC_FMS_RESET2_BTN_AUTO, &CAutoview::OnBnClickedFmsReset2Btn)
	ON_BN_CLICKED(IDC_FMS_END_CMD_SEND_BTN_AUTO, &CAutoview::OnBnClickedFmsEndCmdSendBtn)
	ON_BN_CLICKED(IDC_FMS_OUT_CMD_SEND_BTN_AUTO, &CAutoview::OnBnClickedFmsOutCmdSendBtn)

END_MESSAGE_MAP()


// CAutoview 진단입니다.

#ifdef _DEBUG
void CAutoview::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CAutoview::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CCTSMonDoc* CAutoview::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif //_DEBUG


// CAutoview 메시지 처리기입니다.
void CAutoview::InitLabel()
{
	m_ctrlLineMode.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)		
		.SetFontBold(TRUE)		
		//		.SetFontName("HY헤드라인M")
		.SetText("-");

	m_CmdTarget.SetFontSize(20)
		.SetFontName("Arial")
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)			
		.SetFontBold(TRUE);	
	//		.SetFontName("HY헤드라인M");

	m_LabelViewName.SetFontSize(24)
		.SetFontName("Arial")
		.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[0]);//"Operation"	


}

void CAutoview::InitCombo()
{	



}
void CAutoview::InitFont()
{	
	LOGFONT LogFont;



	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	m_Font.CreateFontIndirect( &LogFont );




}

void CAutoview::InitColorBtn()
{
	//200911 AUTO 버튼추가 BJY
	m_Btn_FmsReset1.SetFontStyle(24,1);
	m_Btn_FmsReset1.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_FmsReset2.SetFontStyle(24,1);
	m_Btn_FmsReset2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_FmsEndCommandSend.SetFontStyle(24,1);
	m_Btn_FmsEndCommandSend.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_FmsOutCommandSend.SetFontStyle(24,1);
	m_Btn_FmsOutCommandSend.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}

void CAutoview::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();						//Top Config

	InitLabel();
	InitFont();	
	InitColorBtn();
	InitCombo();
	InitStepList();
}

void CAutoview::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}

		if(::IsWindow(GetDlgItem(IDC_STEP_LIST)->GetSafeHwnd()))
		{
			GetDlgItem(IDC_STEP_LIST)->GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			GetDlgItem(IDC_STEP_LIST)->MoveWindow(ctrlRect.left, ctrlRect.top, ctrlRect.Width(), rect.bottom - ctrlRect.top-5 , FALSE);
		}		
	}	
}

void CAutoview::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());	


	UpdateGroupState(nModuleID);

	UpdateData(false);
}

CString CAutoview::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}

void CAutoview::InitStepList()
{
	m_pChStsSmallImage = new CImageList;
	//상태별 표시할 이미지 로딩
	m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON,19,24,RGB(255,255,255));
	m_ctrlStepList.SetImageList(m_pChStsSmallImage, LVSIL_SMALL);

	//
	DWORD style = 	m_ctrlStepList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;
	m_ctrlStepList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

	// Column 삽입
	m_ctrlStepList.InsertColumn(0, TEXT_LANG[8], LVCFMT_CENTER, 0);	//center 정렬을 0 column을 사용하지 않음   //"Step"
	m_ctrlStepList.InsertColumn(1, TEXT_LANG[8], LVCFMT_CENTER, 40); //"Step"
	m_ctrlStepList.InsertColumn(2, TEXT_LANG[9], LVCFMT_LEFT, 100); //"Type"
	m_ctrlStepList.InsertColumn(3, TEXT_LANG[10], LVCFMT_LEFT, 80); //"Status"
	m_ctrlStepList.InsertColumn(4, TEXT_LANG[11], LVCFMT_LEFT, 100); //"Referance"
}

void CAutoview::DrawStepList()
{
	m_ctrlStepList.DeleteAllItems();

	CCTSMonDoc *pDoc=  GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);

	if(pModule == NULL)		return;

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;

	CTestCondition *pCon = pModule->GetCondition();
	CStep *pStep = NULL;
	char szName[32];
	int nI = 0;
	for(nI = 0; nI <pCon->GetTotalStepNo(); nI++ )
	{
		pStep = pCon->GetStep(nI);
		if(pStep == NULL)	break;

		sprintf(szName, "%d", pStep->m_StepIndex+1);
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		lvItem.iImage = I_IMAGECALLBACK;
		m_ctrlStepList.InsertItem(&lvItem);
		m_ctrlStepList.SetItemData(lvItem.iItem, pStep->m_type);		//==>LVN_ITEMCHANGED 를 발생 기킴 

		m_ctrlStepList.SetItemText(nI, 1, szName);

		sprintf(szName, "%s", ::StepTypeMsg(pStep->m_type));
		m_ctrlStepList.SetItemText(nI, 2, szName);
		if(pStep->m_type == EP_TYPE_CHARGE || pStep->m_type == EP_TYPE_DISCHARGE || pStep->m_type == EP_TYPE_IMPEDANCE)
		{
			sprintf(szName, "%s/%s", pDoc->ValueString(pStep->m_fVref, EP_VOLTAGE, TRUE), pDoc->ValueString(pStep->m_fIref, EP_CURRENT, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
		else if(pStep->m_type == EP_TYPE_REST)
		{
			sprintf(szName, "%s", pDoc->ValueString(pStep->m_fEndTime, EP_STEP_TIME, TRUE));
			m_ctrlStepList.SetItemText(nI, 4, szName);
		}
	}	

	//Idle Monitoring을 위한 의미 없는 Step Item 추가
	//2008/7/18 KBH
	int nCurStep = pModule->GetCurStepNo();
	//현재 진행 Step에 맞게 Display 한다.
	for(nI = 0; nI < m_ctrlStepList.GetItemCount(); nI++)
	{
		if(pModule->IsComplitedStep(nI))
		{
			//완료된 data는 파일에서 loading한다.
			m_ctrlStepList.SetItemText(nI, 3, TEXT_LANG[37]);
		}
		else
		{
			if(nCurStep == nI+1)
			{
				//진행중인 data는 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, TEXT_LANG[38]);

				break;
			}
			else
			{
				//아직 진행하지 않은 step은 현재값을 표시한다.
				m_ctrlStepList.SetItemText(nI, 3, TEXT_LANG[39]);
			}
		}
	}
	m_ctrlStepList.Invalidate();
}

void CAutoview::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);

		StartMonitoring();	
	}
}

void CAutoview::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							// 현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);

		StopMonitoring();
	}
}

void CAutoview::StopMonitoring()
{
	if(m_nDisplayTimer != 0)	
	{
		KillTimer(m_nDisplayTimer);
		m_nDisplayTimer = 0;
	}

}


void CAutoview::StartMonitoring()
{
	m_nPrevStep = 0;

	DrawStepList();

	DrawProcessInfo();
}

void CAutoview::DrawProcessInfo()
{
	CCTSMonDoc *pDoc = GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	CString strTemp = _T("");

	if(pModule != NULL )
	{
		WORD state;
		state = EPGetGroupState(m_nCurModuleID);


		UpdateData(FALSE);
	}
}



void CAutoview::DrawChannel()
{
	BYTE colorFlag = 0;
	CString  strMsg, strOld, strToolTip, strUnit;

	CCTSMonDoc *pDoc = GetDocument();
	float	fTemp = 0;

	CTestCondition *pProc;
	CStep *pStep = NULL;

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		return;

	int nCurStep = pModule->GetCurStepNo();
	pProc = pModule->GetCondition();
	if(pProc != NULL)
	{
		//		TRACE("STEP No : %d\r\n", nCurStep);
		pStep = pProc->GetStep(nCurStep-1);		//Step 번호가 1 Base
	}

	// step 이 변했으면 자동으로 현재 step을 이동한다.
	if(m_nPrevStep != nCurStep)
	{
		m_nPrevStep = nCurStep;

		//현재 진행 Step에 맞게 Display 한다.
		for(int item = 0; item < m_ctrlStepList.GetItemCount(); item++)
		{
			if(pModule->IsComplitedStep(item))
			{
				//완료된 data는 파일에서 loading한다.
				m_ctrlStepList.SetItemText(item, 3, TEXT_LANG[37]);
			}
			else
			{
				// 작업을 완료한 후 결과 파일이 삭제 되었거나 존재 하지 않을 경우
				// 진행중과 대기중으로 표기된다.
				if(nCurStep == item+1)
				{
					m_ctrlStepList.SetItemText(item, 3, TEXT_LANG[38]);
				}
				else
				{
					m_ctrlStepList.SetItemText(item, 3, TEXT_LANG[39]);
				}
			}
		}
		m_ctrlStepList.Invalidate();
	}
}

void CAutoview::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	if( nModuleID != m_nCurModuleID )
	{	
		// 1. 현재 실행중인 모듈이 아니기에 상태값을 변경하지 않는다.
		return;
	}


	if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
	{
		m_ctrlLineMode.SetText(TEXT_LANG[27]);//"Line Off"
	}
	else
	{
		CCTSMonDoc *pDoc =  GetDocument();
		CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
		if(pModule != NULL)
		{
			if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
			{
				m_ctrlLineMode.SetText(TEXT_LANG[27]);//"Line Off"			
			}
			else
			{
				DrawStepList();

				DrawChannel();

				int nLineMode = EP_OFFLINE_MODE;		
				nLineMode = pModule->GetOperationMode();

				switch( nLineMode )
				{
				case EP_OPERATION_LOCAL:
					{
						m_ctrlLineMode.SetText(TEXT_LANG[28]);//"Local Mode"	
					}
					break;
				case EP_OPEARTION_MAINTENANCE:
					{
						m_ctrlLineMode.SetText(TEXT_LANG[29]);//"Maintenance Mode"
					}
					break;
				case EP_OPERATION_AUTO:
					{
						m_ctrlLineMode.SetText(TEXT_LANG[30]); //"Automatic Mode"
						DrawProcessInfo();
					}
					break;
				}	
			}
		}
		else
		{
			m_ctrlLineMode.SetText(TEXT_LANG[27]);//"Line Off"
		}	
	}
}




void CAutoview::OnNMDblclkStepList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	pNMHDR = NULL;
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);

	*pResult = 0;
}

void CAutoview::OnGetdispinfoStepList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here

	UINT nType, nCurStep = 0;
	DWORD dwImgIndex = 0;
	//	EP_CH_DATA netChData;

	if(pDispInfo->item.mask & LVIF_IMAGE)
	{
		CFormModule *pMD = GetDocument()->GetModuleInfo(m_nCurModuleID);

		if(pDispInfo->item.iSubItem == 2)
		{
			nType = m_ctrlStepList.GetItemData(pDispInfo->item.iItem);	//step type

			switch(nType)
			{
			case EP_TYPE_CHARGE	:		dwImgIndex = 2;		break;
			case EP_TYPE_DISCHARGE:		dwImgIndex = 1;		break;
			case EP_TYPE_REST	:		dwImgIndex = 11;	break;
			case EP_TYPE_OCV	:		dwImgIndex = 7;		break;
			case EP_TYPE_IMPEDANCE:		dwImgIndex = 10;	break;
			case EP_TYPE_END	:		dwImgIndex = 4;		break;
			default				:		dwImgIndex = 0;		break;
			}

			pDispInfo->item.iImage = dwImgIndex + 12;

			if(pMD)		
			{
				if(pMD->IsComplitedStep(pDispInfo->item.iItem))
				{
					pDispInfo->item.iImage = dwImgIndex;
				}
			}
		}
		else if(pDispInfo->item.iSubItem == 1)
		{
			if(pMD)		
			{

				nCurStep = pMD->GetCurStepNo();
				if((UINT)pDispInfo->item.iItem + 1 == nCurStep)
				{
					pDispInfo->item.iImage = 8;				
				}
				else
				{
					pDispInfo->item.iImage = -1;
				}
			}
		}
	}
	*pResult = 0;
}


void CAutoview::OnBnClickedFmsReset1Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	
	CString strTemp = _T("");

	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);					


	DWORD nCount = 0;	//진입한 Tray 갯수 
	for(int j =0; j<pModule->GetTotalJig(); j++)
	{
		if(EPTrayState(m_nCurModuleID, j) == EP_TRAY_LOAD)
		{
			nCount++;
		}
	}
	if(nCount == 0)
	{
		strTemp.Format("No Tray");//명령 전송을 실패 하였습니다.\n[ No Tray ]
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;
	} 

	//20200914 ksj
	if(pModule->m_bProcessStart == true)
	{
		strTemp.Format(TEXT_LANG[51]); //"트레이 위치 변경이 가능한 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT); 
		return;
	}

	WORD state = EPGetGroupState(m_nCurModuleID);

	if( state != EP_STATE_RUN && pModule->GetTrayInfo()->GetTrayNo().GetLength() != 0 )
	{
		strTemp.Format(TEXT_LANG[48],  ::GetModuleName(m_nCurModuleID)); //"[Stage %s] 트레이 입고 위치 변경을 요청합니다."
		if( MessageBox(strTemp, TEXT_LANG[50], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return; //"트레이 입고 위치 변경"

		if( pDoc->m_fmst.fnChangeReserveProcPosition(m_nCurModuleID) == TRUE )
		{
			strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);

			pModule->m_bRackToRack = true; //20200914ksj
		}
		else
		{
			strTemp.Format(TEXT_LANG[51]); //"트레이 위치 변경이 가능한 상태가 아닙니다."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[51]); //"트레이 위치 변경이 가능한 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
	}

// 	if( state == EP_STATE_READY || state == EP_STATE_STANDBY || state == EP_STATE_PAUSE 
// 		|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_CONTACT)
// 	{
// 		strTemp.Format("[Stage %s] 트레이 입고 위치 변경을 요청합니다.",  ::GetModuleName(m_nCurModuleID));
// 		if( MessageBox(strTemp, "트레이 입고 위치 변경", MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;
// 		// pDoc->SendInitCommand(m_nCurModuleID);
// 		pDoc->m_fmst.fnChangeReserveProcPosition(m_nCurModuleID);
// 
// 		strTemp.Format("명령을 전송 하였습니다.\n잠시만 기다려주십시오.");
// 		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
// 	}
// 	else
// 	{	
// 		strTemp.Format("입고 예약을 받은 상태가 아닙니다.");
// 		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
// 		// strTemp.Format("[Stage %s] 는 예약 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
// 		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
// 	}
}

void CAutoview::OnBnClickedFmsReset2Btn()
{
	CString strTemp = _T("");
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

//20201206 BJY (기존) RUN COMMEND를 주고 공정진행 확인하던 시퀀스 -> RUN COMMEND를 주기 전으로 수정 START
	strTemp.Format(TEXT_LANG[53],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 입고 예약된 공정을 진행합니다."
	if( MessageBox(strTemp, TEXT_LANG[54], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;//"공정 진행 확인"
//20201206 BJY (기존) RUN COMMEND를 주고 공정진행 확인하던 시퀀스 -> RUN COMMEND를 주기 전으로 수정 END

	if(pDoc->m_fmst.fnTrayInStateReset(m_nCurModuleID) == FALSE )
	{
		strTemp.Format(TEXT_LANG[52]);//"입고 예약된 공정을 진행할 수 있는 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("[Stage %s] 는 예약된 공정을 진행할 수 있는 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
	else
	{
		strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);

		pDoc->m_fmst.fnRunCMD(m_nCurModuleID);
	}
	/*
	CString strTemp;
		
	strTemp.Format("[Stage %s] 오류 상태 색상을 초기화 합니다.",  ::GetModuleName(m_nCurModuleID));
	if( MessageBox(strTemp, "색상 초기화", MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;
	
	if(pDoc->m_fmst.fnClearColor(m_nCurModuleID))
	{

	}
	else
	{	
		strTemp.Format("[Stage %s] 는 시간 초과 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
		MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
	*/
}

void CAutoview::OnBnClickedFmsEndCmdSendBtn()
{
	//end 처음부터 시작
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);					

	CString strTemp = _T("");

	DWORD nCount = 0;	//진입한 Tray 갯수 
	for(int j =0; j<pModule->GetTotalJig(); j++)
	{
		if(EPTrayState(m_nCurModuleID, j) == EP_TRAY_LOAD)
		{
			nCount++;
		}
	}
	if(nCount == 0)
	{
		strTemp.Format("No Tray");//명령 전송을 실패 하였습니다.\n[ No Tray ]
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;
	} 



	strTemp.Format(TEXT_LANG[55],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 검사 종료 통지 Command를 재전송합니다."

	if( MessageBox(strTemp, TEXT_LANG[56], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;//"검사 종료 통지 Command 전송"
		
	if(pDoc->m_fmst.fnEndStateReset(m_nCurModuleID))
	{	
		strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
	else
	{	
		strTemp.Format(TEXT_LANG[57]);//"공정이 완료된 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("[Stage %s] 는 종료 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
}
void CAutoview::OnBnClickedFmsOutCmdSendBtn()
{
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T("");

	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);					

	WORD state = EPGetGroupState(m_nCurModuleID);

	if( state != EP_STATE_RUN && pModule->GetTrayInfo()->GetTrayNo().GetLength() != 0 )
	{
		strTemp.Format("[Stage %s] Tray Out CMD SEND.",  ::GetModuleName(m_nCurModuleID));
		if( MessageBox(strTemp, "Tray Out CMD", MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;

		if( pDoc->m_fmst.fnTrayOutCMD(m_nCurModuleID) == TRUE )
		{
			strTemp.Format("CMD sent... Wait a moment");//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
		}
		else
		{
			strTemp.Format("CMD send fail");
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		}
	}
	else
	{
		strTemp.Format("CMD send fail"); 
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
	}

	//pDoc->m_fmst.fnChangeReserveProcPosition(m_nCurModuleID);
}
