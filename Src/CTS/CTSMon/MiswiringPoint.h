#pragma once


#define MIS_MAX_VOLTAGE_CNT 1
#define MIS_MAX_CURRENT_CNT 3

#define REG_MIS_SECTION	"Miswiring"
class CMiswiringPoint
{
public:
	void SetVMeasureCnt(int nCnt = 1)	{	m_nVMeasureCnt = nCnt; }
	void SetIMeasureCnt(int nCnt = 1)	{	m_nIMeasureCnt = nCnt; }
	int	GetVMeasureCnt()	{	return  m_nVMeasureCnt;	}
	int	GetIMeasureCnt()	{	return	m_nIMeasureCnt;	}


	void SetVData( WORD wPoint, double dValue);	//Point: 0 - 面规傈
	void SetIData( WORD wPoint, double dValue); //0 - 面傈, 1 - 规傈1, 2 - 规傈2
	double GetVData( WORD wPoint )	{ if( wPoint < MIS_MAX_VOLTAGE_CNT ) return m_VMeasureData[wPoint]; }
	double GetIData( WORD wPoint )	{ if( wPoint < MIS_MAX_CURRENT_CNT ) return m_IMeasureData[wPoint]; }

	CMiswiringPoint(void);

	~CMiswiringPoint(void);

protected:
	double	m_VMeasureData[1];
	double	m_IMeasureData[3];

	int m_nVMeasureCnt;
	int	m_nIMeasureCnt;
	
};
