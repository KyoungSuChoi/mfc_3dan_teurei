#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_READY::CFM_ST_AT_READY(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
}


CFM_ST_AT_READY::~CFM_ST_AT_READY(void)
{
}

VOID CFM_ST_AT_READY::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_READY);

	CHANGE_FNID(FNID_PROC);

//	m_Unit->fnSetTrayInStateRecv(FALSE);

	TRACE("CFM_ST_AT_READY::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_READY::fnProc()
{
	if(1 == m_iWaitCount)
	{
		fnSetFMSStateCode(FMS_ST_RED_READY);
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_AT_READY::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_READY::fnExit()
{
	TRACE("CFM_ST_AT_READY::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_READY::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

// 	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
    if(m_Unit->fnGetFMS()->fnGetError())
	{
        CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			//CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(AUTO_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	}
//	TRACE("CFM_ST_AT_READY::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_READY::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 2 )
	{
		BOOL bRunInfoErrChk = FALSE;

// 		st_S2F412 data;
// 		ZeroMemory(&data, sizeof(st_S2F412));
// 		memcpy(&data, _recvData->data, sizeof(st_S2F412));
// 
// 		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
// 		{
// 			// bRunInfoErrChk = TRUE;
// 		}
// 
// 		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
// 		{
// 			// bRunInfoErrChk = TRUE;
// 		}

		//_error = FMS_ER_NONE;
		_error = ER_NO_Process;

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

// 		if( bRunInfoErrChk == FALSE )
// 		{
// 			_error = ER_Run_Fail;
// 
// 			if(m_Unit->fnGetFMS()->fnRun())
// 			{
// 				_error = FMS_ER_NONE;
// 
// 				m_Unit->fnGetFMS()->fnSendToHost_S6F11C1( 1, 0 );
// 			}
// 		}

		
	}
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 4 )
	{
		_error = ER_NO_Process;

		/*
		st_S2F414 data;
		ZeroMemory(&data, sizeof(st_S2F414));
		memcpy(&data, _recvData->data, sizeof(st_S2F414));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		_error = m_Unit->fnGetFMS()->SendSBCInitCommand();
		*/

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		// m_Unit->fnGetFMS()->fnSendToHost_S6F11C13( _recvData->head.nDevId, _recvData->head.lSysByte );		
	}
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 6 )
	{
		st_S2F416 data;
		ZeroMemory(&data, sizeof(st_S2F416));
		memcpy(&data, _recvData->data, sizeof(st_S2F416));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		_error = m_Unit->fnGetFMS()->SendSBCInitCommand();

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		m_Unit->fnGetFMS()->fnSendToHost_S6F11C14( _recvData->head.nDevId, _recvData->head.lSysByte );
	}
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 7 )
	{
		st_S2F417 data;
		ZeroMemory(&data, sizeof(st_S2F417));
		memcpy(&data, _recvData->data, sizeof(st_S2F417));

		_error = m_Unit->fnGetFMS()->fnReserveProc();		

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( _error == FMS_ER_NONE )
		{
			m_Unit->fnGetFMS()->fnSendToHost_S6F11C15( _recvData->head.nDevId, _recvData->head.lSysByte );
		}
	}

	TRACE("CFM_ST_AT_READY::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}