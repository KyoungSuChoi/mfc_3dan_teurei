#include "stdafx.h"
#include "ColorButton2.h"

#include <afxtempl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace clr
{
	const COLORREF CLR_BTN_WHITE  = RGB(255, 255, 255);
	const COLORREF CLR_BTN_BLACK  = RGB(0, 0, 0);
	const COLORREF CLR_BTN_DGREY  = RGB(128, 128, 128);
	const COLORREF CLR_BTN_GREY   = RGB(192, 192, 192);
	const COLORREF CLR_BTN_LLGREY = RGB(223, 223, 223);
}

/////////////////////////////////////////////////////////////////////////////
// CColorButton2
CColorButton2::CColorButton2()
{
	SetColorToWindowsDefault();

	m_bCenter = true;
	m_bLeft = false;
	m_bRight = false;

	// Set default 
	m_bBold = false;
	m_bItalic = false;
	m_bUnderlined = false;
	m_bStrikethrough = false;
	m_bAntialias = false;
	m_nSize = 14;
	m_szFont.Format("��������");
}

CColorButton2::CColorButton2(COLORREF text, COLORREF bkgnd)
{
	m_TextColor				= text;
	m_BkgndColor			= bkgnd; 
	m_DisabledBkgndColor	= GetSysColor(COLOR_BTNFACE);
	m_Light					= GetSysColor(COLOR_3DLIGHT);
	m_Highlight				= GetSysColor(COLOR_BTNHIGHLIGHT);
	m_Shadow				= GetSysColor(COLOR_BTNSHADOW);
	m_DarkShadow			= GetSysColor(COLOR_3DDKSHADOW);	
}

CColorButton2::CColorButton2(COLORREF text, COLORREF bkgnd, COLORREF disabled)
{
	m_TextColor				= text;
	m_BkgndColor			= bkgnd; 
	m_DisabledBkgndColor	= disabled;
	m_Light					= GetSysColor(COLOR_3DLIGHT);
	m_Highlight				= GetSysColor(COLOR_BTNHIGHLIGHT);
	m_Shadow				= GetSysColor(COLOR_BTNSHADOW);
	m_DarkShadow			= GetSysColor(COLOR_3DDKSHADOW);	
}

CColorButton2::CColorButton2(COLORREF text, COLORREF bkgnd, COLORREF disabled, COLORREF light, COLORREF highlight, COLORREF shadow, COLORREF darkShadow)
{
	m_TextColor				= text;
	m_BkgndColor			= bkgnd; 
	m_DisabledBkgndColor	= disabled;
	m_Light					= light;
	m_Highlight				= highlight;
	m_Shadow				= shadow;
	m_DarkShadow			= darkShadow;
}

CColorButton2::~CColorButton2()
{
}


BEGIN_MESSAGE_MAP(CColorButton2, CButton)
	//{{AFX_MSG_MAP(CColorButton2)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorButton2 message handlers
void CColorButton2::SetFontStyle( int nSize, DWORD dwStyle )
{
	m_nSize = nSize;
	
	// Set the style of the static
	if(dwStyle&FS_BOLD)
		m_bBold = true;
	if(dwStyle&FS_ITALIC)
		m_bItalic = true;
	if(dwStyle&FS_UNDERLINED)
		m_bUnderlined = true;
	if(dwStyle&FS_STRIKETHROUGH)
		m_bStrikethrough = true;
	if(dwStyle&FS_ANTIALIAS)
		m_bAntialias = true;
	if(dwStyle&FS_NORMAL)
	{
		// This will (even if it's combined with other defines) 
		// set all styles to false.		
		m_bBold = false;
		m_bItalic = false;
		m_bUnderlined = false;
		m_bStrikethrough = false;
		m_bAntialias = false;
		m_bCenter = false;
		m_bLeft = true;
		m_bRight = false;
	}
	
	// Set all alignments to false
	m_bCenter = false;
	m_bLeft = false;
	m_bRight = false;
	
	// Set the horizontal alignment
	if(dwStyle&FS_CENTER)
		m_bCenter = true;
	else if(dwStyle&FS_RIGHT)
		m_bRight = true;
	else if(dwStyle&FS_LEFT)
		m_bLeft = true;
	else
		m_bLeft = true;
		
	if(m_hWnd != NULL)
	{
		Invalidate();
	}
}

void CColorButton2::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	CDC		*pDC;
	CRect	rcFocus, rcButton, rcText, rcOffsetText;
	UINT	state;

	CFont fFont;	
	CFont *fOldFont; 
	int nWeight;
	DWORD dwQuality;
	UINT unFormat=0;

		
	pDC    = CDC::FromHandle(lpDrawItemStruct->hDC);
	state = lpDrawItemStruct->itemState;	
	
	rcFocus.CopyRect(&lpDrawItemStruct->rcItem); 
	rcButton.CopyRect(&lpDrawItemStruct->rcItem); 
	
	rcText = rcButton;
	rcText.OffsetRect(-1, -1);

	rcOffsetText = rcText;
	rcOffsetText.OffsetRect(1, 1);
	
	// Set the focus rectangle to just past the border decoration
	rcFocus.left   += 4;
    rcFocus.right  -= 4;
    rcFocus.top    += 4;
    rcFocus.bottom -= 4;
	
	// Retrieve the button's caption
	CString strCaption;
	GetWindowText(strCaption);

	// Font Setting
	if(m_bBold)
		nWeight = FW_BLACK;
	else
		nWeight = FW_NORMAL;
	
	// Set the quality of the font
	if(m_bAntialias)
		dwQuality = ANTIALIASED_QUALITY;
	else
		dwQuality = DEFAULT_QUALITY;
	
	// Set the horizontal alignment
	if(m_bCenter)
		unFormat|=DT_CENTER;
	else if(m_bRight)
		unFormat|=DT_RIGHT;
	else if(m_bLeft)
		unFormat|=DT_LEFT;
	
	// Set no prefix and wordwrapping
	unFormat|=DT_NOPREFIX;
	unFormat|=DT_WORDBREAK;
	
	// Create the font
	fFont.Detach();
	if(fFont.CreateFont(m_nSize,0,0,0, nWeight ,m_bItalic, m_bUnderlined, m_bStrikethrough, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_CHARACTER_PRECIS, dwQuality, DEFAULT_PITCH | FF_DONTCARE, m_szFont)!=NULL)
	{
		fOldFont = pDC->SelectObject(&fFont);		
	}
	
	if (state & ODS_DISABLED)
    {
		DrawFilledRect(pDC, rcButton, m_DisabledBkgndColor);
	}
	else
	{
		DrawFilledRect(pDC, rcButton, m_BkgndColor);
	}
	
	if (state & ODS_SELECTED)
	{ 
		DrawFrame(pDC, rcButton, BUTTON_IN);
	}
	else
	{
		if ((state & ODS_DEFAULT) || (state & ODS_FOCUS))
		{
			DrawFrame(pDC, rcButton, BUTTON_OUT | BUTTON_BLACK_BORDER);			
		}
		else
		{
			DrawFrame(pDC, rcButton, BUTTON_OUT);
		}
	}
	
	if (state & ODS_DISABLED)
	{
		DrawButtonText(pDC, rcOffsetText, strCaption, clr::CLR_BTN_WHITE);
		DrawButtonText(pDC, rcText,	strCaption, clr::CLR_BTN_DGREY);
    }
	else
	{
		if (state & ODS_SELECTED)
		{
			DrawButtonText(pDC, rcOffsetText, strCaption, m_TextColor);
		}
		else
		{
			DrawButtonText(pDC, rcText, strCaption, m_TextColor);
		}
	}
	
	if (state & ODS_FOCUS)
	{
		DrawFocusRect(lpDrawItemStruct->hDC, (LPRECT)&rcFocus);
	}	

	fFont.DeleteObject();
}

void CColorButton2::DrawFrame(CDC *pDC, CRect rc, int state)
{
	COLORREF color;
	
	if (state & BUTTON_BLACK_BORDER)
	{
		color = clr::CLR_BTN_BLACK;
		
		DrawLine(pDC, rc.left, rc.top, rc.right, rc.top,    color); // Across top
		DrawLine(pDC, rc.left, rc.top, rc.left,  rc.bottom, color); // Down left
		
		DrawLine(pDC, rc.left,      rc.bottom - 1, rc.right,     rc.bottom - 1, color); // Across bottom
		DrawLine(pDC, rc.right - 1, rc.top,        rc.right - 1, rc.bottom,     color); // Down right
		
		rc.InflateRect(-1, -1);
	}
	
	if (state & BUTTON_OUT)
	{
		color = m_Highlight;
		
		DrawLine(pDC, rc.left, rc.top, rc.right, rc.top,    color); // Across top
		DrawLine(pDC, rc.left, rc.top, rc.left,  rc.bottom, color); // Down left
		
		color = m_DarkShadow;
		
		DrawLine(pDC, rc.left,      rc.bottom - 1, rc.right,     rc.bottom - 1, color); // Across bottom
		DrawLine(pDC, rc.right - 1, rc.top,        rc.right - 1, rc.bottom,     color); // Down right
		
		rc.InflateRect(-1, -1);
		
		color = m_Light;
		
		DrawLine(pDC, rc.left, rc.top, rc.right, rc.top,    color); // Across top
		DrawLine(pDC, rc.left, rc.top, rc.left,  rc.bottom, color); // Down left
		
		color = m_Shadow;
		
		DrawLine(pDC, rc.left,      rc.bottom - 1, rc.right,     rc.bottom - 1, color); // Across bottom
		DrawLine(pDC, rc.right - 1, rc.top,        rc.right - 1, rc.bottom,     color); // Down right
	}
	
	if (state & BUTTON_IN)
	{
		color = m_DarkShadow;
		
		DrawLine(pDC, rc.left, rc.top, rc.right, rc.top,    color); // Across top
		DrawLine(pDC, rc.left, rc.top, rc.left,  rc.bottom, color); // Down left
		DrawLine(pDC, rc.left,      rc.bottom - 1, rc.right,     rc.bottom - 1, color); // Across bottom
		DrawLine(pDC, rc.right - 1, rc.top,        rc.right - 1, rc.bottom,     color); // Down right
		
		rc.InflateRect(-1, -1);
		
		color = m_Shadow;
		
		DrawLine(pDC, rc.left, rc.top, rc.right, rc.top,    color); // Across top
		DrawLine(pDC, rc.left, rc.top, rc.left,  rc.bottom, color); // Down left
		DrawLine(pDC, rc.left,      rc.bottom - 1, rc.right,     rc.bottom - 1, color); // Across bottom
		DrawLine(pDC, rc.right - 1, rc.top,        rc.right - 1, rc.bottom,     color); // Down right
	}
}

void CColorButton2::DrawFilledRect(CDC *pDC, CRect rc, COLORREF color)
{
	CBrush brSolid;
	
	brSolid.CreateSolidBrush(color);
	pDC->FillRect(rc, &brSolid);
}

void CColorButton2::DrawLine(CDC *pDC, long sx, long sy, long ex, long ey, COLORREF color)
{
	CPen newPen;
	CPen *oldPen;
	
	newPen.CreatePen(PS_SOLID, 1, color);
	oldPen = pDC->SelectObject(&newPen);

	pDC->MoveTo(sx, sy);
	pDC->LineTo(ex, ey);
	pDC->SelectObject(oldPen);

    newPen.DeleteObject();	
}

void CColorButton2::DrawButtonText(CDC *pDC, CRect rc, CString strCaption, COLORREF textcolor)
{
	DWORD uStyle = GetWindowLong(this->m_hWnd,GWL_STYLE);

	CArray<CString, CString> arLines;

	if((uStyle & BS_MULTILINE) == BS_MULTILINE)
	{
		int nIndex = 0;
		while(nIndex != -1)
		{
			nIndex = strCaption.Find('\n');
			if(nIndex>-1)
			{
				CString line = strCaption.Left(nIndex);
				arLines.Add(line);
				strCaption.Delete(0,nIndex+1);
			}
			else
				arLines.Add(strCaption);
		}
	}
	else
	{
		arLines.Add(strCaption);
	}

	CSize sizeText = pDC->GetOutputTextExtent( strCaption );
	
    COLORREF oldColour;
	
	oldColour = pDC->SetTextColor(textcolor);
	pDC->SetBkMode(TRANSPARENT);
	
	int nStartPos = (rc.Height() - arLines.GetSize()*sizeText.cy)/2-1;
	if((uStyle & BS_TOP) == BS_TOP)
		nStartPos = rc.top+2;
	if((uStyle & BS_BOTTOM) == BS_BOTTOM)
		nStartPos = rc.bottom- arLines.GetSize()*sizeText.cy-2;
	if((uStyle & BS_VCENTER) == BS_VCENTER)
		nStartPos = (rc.Height() - arLines.GetSize()*sizeText.cy)/2-1;
	
	UINT uDrawStyles = 0;
	if((uStyle & BS_CENTER) == BS_CENTER)
		uDrawStyles |= DT_CENTER;
	else
	{
		if((uStyle & BS_LEFT) == BS_LEFT)
			uDrawStyles |= DT_LEFT;
		else
		if((uStyle & BS_RIGHT) == BS_RIGHT)
			uDrawStyles |= DT_RIGHT;
		else
		if(uDrawStyles == 0)
			uDrawStyles = DT_CENTER|DT_VCENTER | DT_SINGLELINE;
	}
	
	for(int i=0; i<arLines.GetSize(); i++)
	{
		CRect textrc = rc;
		textrc.DeflateRect(3,0,3,0);
		textrc.top = nStartPos + sizeText.cy*i;
		textrc.bottom = nStartPos + sizeText.cy*(i+1);
		CString line = arLines.GetAt(i);
		pDC->DrawText(line, line.GetLength(), textrc, uDrawStyles);
	}

	pDC->SetTextColor(oldColour);
}

void CColorButton2::SetColor(COLORREF text, COLORREF bkgnd)
{
	m_TextColor				= text;
	m_BkgndColor			= bkgnd; 

	if(m_hWnd != NULL)
		Invalidate();
}

void CColorButton2::SetColor(COLORREF text, COLORREF bkgnd, COLORREF disabled)
{
	m_TextColor				= text;
	m_BkgndColor			= bkgnd; 
	m_DisabledBkgndColor	= disabled;

	if(m_hWnd != NULL)
		Invalidate();
}

void CColorButton2::SetColor(COLORREF text, COLORREF bkgnd, COLORREF disabled, COLORREF light, COLORREF highlight, COLORREF shadow, COLORREF darkShadow)
{
	m_TextColor				= text;
	m_BkgndColor			= bkgnd; 
	m_DisabledBkgndColor	= disabled;
	m_Light					= light;
	m_Highlight				= highlight;
	m_Shadow				= shadow;
	m_DarkShadow			= darkShadow;

	if(m_hWnd != NULL)
		Invalidate();
}

void CColorButton2::SetColorToWindowsDefault()
{
	m_TextColor				= GetSysColor(COLOR_BTNTEXT);
	m_BkgndColor			= GetSysColor(COLOR_BTNFACE); 
	m_DisabledBkgndColor	= GetSysColor(COLOR_BTNFACE);
	m_Light					= GetSysColor(COLOR_3DLIGHT);
	m_Highlight				= GetSysColor(COLOR_BTNHIGHLIGHT);
	m_Shadow				= GetSysColor(COLOR_BTNSHADOW);
	m_DarkShadow			= GetSysColor(COLOR_3DDKSHADOW);	
}

int CColorButton2::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	lpCreateStruct->dwExStyle |= BS_OWNERDRAW;

	if (CButton::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}
