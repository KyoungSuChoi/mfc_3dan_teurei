// DetailAutoTestConditionDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "DetailAutoTestConditionDlg.h"


// CDetailAutoTestConditionDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDetailAutoTestConditionDlg, CDialog)

CDetailAutoTestConditionDlg::CDetailAutoTestConditionDlg(CTestCondition *pTestCondition, CTray *pTray, CWnd* pParent /*=NULL*/)
: CDialog(CDetailAutoTestConditionDlg::IDD, pParent)
, m_fDCIR_RegTemp(0)
, m_fDCIR_ResistanceRate(0)
, m_nChargeLowVCheckTime(0)
, m_nChargeVoltageChkTime(0)
, m_strChargeFanOffChk(_T(""))
{
	LanguageinitMonConfig(_T("CDetailAutoTestConditionDlg"));
	m_pDoc = NULL;
	m_pTestCondition = pTestCondition;
	m_pTray = pTray;

	m_nFontSize = 12;
	if(g_nLanguage == LANGUAGE_ENG || g_nLanguage == LANGUAGE_HUN)
	{
		m_nFontSize = 10;
	}
}

CDetailAutoTestConditionDlg::~CDetailAutoTestConditionDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CDetailAutoTestConditionDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
			else
			{
				TEXT_LANG[i].Replace("\\d","\d");
				TEXT_LANG[i].Replace("\\s","\s");
				TEXT_LANG[i].Replace("\\n","\n");
				TEXT_LANG[i].Replace("\\N","\N");
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CDetailAutoTestConditionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL1, m_Label1);
	DDX_Control(pDX, IDC_LABEL2, m_Label2);
	DDX_Control(pDX, IDC_LABEL3, m_Label3);
	DDX_Control(pDX, IDC_LABEL_TYPE, m_LabelType);
	DDX_Control(pDX, IDC_LABEL_PROCESS, m_LabelProcess);
	DDX_Text(pDX, IDC_EDIT_REG_TEMP, m_fDCIR_RegTemp);
	DDX_Text(pDX, IDC_EDIT_RESISTANCE_RATE, m_fDCIR_ResistanceRate);
	DDX_Text(pDX, IDC_CHARGE_LOW_VOLTAGE_CHK_TIME, m_nChargeLowVCheckTime);
	DDX_Text(pDX, IDC_CHARGE_VOLTAGE_CHK_TIME, m_nChargeVoltageChkTime);
	DDX_Text(pDX, IDC_CHARGE_FANOFF_CHK, m_strChargeFanOffChk);
}

BEGIN_MESSAGE_MAP(CDetailAutoTestConditionDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDetailAutoTestConditionDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CDetailAutoTestConditionDlg 메시지 처리기입니다.

void CDetailAutoTestConditionDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

BOOL CDetailAutoTestConditionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitLabel();
	InitFont();

	InitProtectSettingGrid();
	InitStepGrid();
	InitContactSettingGrid();
	InitAutoSettingGrid();

	DisplayCondition();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

VOID CDetailAutoTestConditionDlg::InitLabel()
{
	m_Label1.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(24)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[0]);//"공정"

	m_Label2.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[1]);//"타입"	

	m_Label3.SetBkColor(RGB_MIDNIGHTBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[2]);//"번호"	

	m_LabelType.SetBkColor(RGB_BLACK)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText("-");

	m_LabelProcess.SetBkColor(RGB_BLACK)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(22)
		.SetFontBold(TRUE)
		.SetText("-");
}

VOID CDetailAutoTestConditionDlg::DisplayStepGrid()
{
	CString strData = _T("");
	CStep *pStepInfo;
	STR_COMMON_STEP *pStep, stepData;
	STR_TOP_CONFIG *pConfig = m_pDoc->GetTopConfig();
	BYTE color;

	BOOL bLock = m_StepGrid.LockUpdate();

	int nStepRow = m_pTestCondition->GetTotalStepNo();
	if(m_StepGrid.GetRowCount() > 0)
	{
		m_StepGrid.RemoveRows(1, m_StepGrid.GetRowCount());
	}

	m_StepGrid.SetRowCount(nStepRow);

	for(int nRow =0 ; nRow < nStepRow; nRow++)
	{
		pStepInfo = m_pTestCondition->GetStep(nRow);
		if(pStepInfo == NULL) break;	
		stepData = pStepInfo->GetStepData();
		pStep = &stepData;

		m_StepGrid.SetValueRange(CGXRange(nRow+1, 1), (LONG)(pStep->stepHeader.stepIndex+1));

		switch(pStep->stepHeader.type)
		{
		case EP_TYPE_CHARGE:
			m_pDoc->GetStateMsg(EP_STATE_CHARGE, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		// Action
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));					// 정전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));					// 정전압
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));				// 시간
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));					// 종지전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));					// 종지전압
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));					// 종지용량
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY));				// UCA 20200307 KSJ
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY));				// LCA 20200307 KSJ

			strData.Format("%d", (UINT)pStep->fRecVIGetTime/60);
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 12), strData);

			strData.Format("%0.1f", pStep->fSocRate);				
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 13), strData);													// SOC

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 14), m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE));					// 종지용량
			if( pStep->m_nFanOffFlag == 0 )
			{
				m_strChargeFanOffChk = _T("NO");
			}
			else
			{
				m_strChargeFanOffChk = _T("YES");
			}
// 20201026 KSCHOI Delta Capacity Display Bug Fixed. START
//			m_StepGrid.SetValueRange(CGXRange(nRow+1, 13), m_strChargeFanOffChk);
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 15), m_strChargeFanOffChk);
// 20201026 KSCHOI Delta Capacity Display Bug Fixed. END
			break;

		case EP_TYPE_DISCHARGE:
			m_pDoc->GetStateMsg(EP_STATE_DISCHARGE, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		// Action
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));			// 정전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));			// 정전압
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));			// 종지전류
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));			// 종지전압
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY));				// 종지용량
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY));				// 종지용량

			strData.Format("%d", (UINT)pStep->fRecVIGetTime/60);
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 12), strData);	

			strData.Format("%0.1f", pStep->fSocRate);				
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 13), strData);													// SOC
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 14), m_pDoc->ValueString(pStep->fLimitV, EP_VOLTAGE));					// 종지용량
			break;
			
		case EP_TYPE_IMPEDANCE:
			m_pDoc->GetStateMsg(EP_STATE_IMPEDANCE, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	

			m_fDCIR_RegTemp = (float)(pStep->lDCIR_RegTemp/10);
			m_fDCIR_RegTemp = (float)(pStep->lDCIR_ResistanceRate/100.0);
			break;
		case EP_TYPE_REST:
			m_pDoc->GetStateMsg(EP_STATE_REST, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));		// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));				// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);														// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
			strData.Format("%d", (UINT)pStep->fRecVIGetTime/60);
			m_StepGrid.SetValueRange(CGXRange(nRow+1, 12), strData);	
			break;
		case EP_TYPE_OCV:
			m_pDoc->GetStateMsg(EP_STATE_OCV, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
			break;
		case EP_TYPE_END:
			m_pDoc->GetStateMsg(EP_STATE_END, color);
			m_StepGrid.SetStyleRange(CGXRange().SetRows(nRow+1), CGXStyle().SetInterior(pConfig->m_BStateColor[color]).SetTextColor(pConfig->m_TStateColor[color]));
			m_StepGrid.SetStyleRange(CGXRange(nRow+1, 2), CGXStyle().SetValue(StepTypeMsg(pStep->stepHeader.type)));	// Action	
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 3), ModeTypeMsg(pStep->stepHeader.type, pStep->stepHeader.mode));		
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 4), m_pDoc->ValueString(pStep->fIref, EP_CURRENT));				// 정전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 5), m_pDoc->ValueString(pStep->fVref, EP_VOLTAGE));				// 정전압
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->StepEndString(pStep));

			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 6), m_pDoc->ValueString(pStep->fEndTime, EP_STEP_TIME));		// 시간
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 7), m_pDoc->ValueString(pStep->fEndI, EP_CURRENT));				// 종지전류
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 8), m_pDoc->ValueString(pStep->fEndV, EP_VOLTAGE));				// 종지전압

			// strData.Format("%0.1f", pStep->fSocRate);				
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 9), strData);													// SOC
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 10), m_pDoc->ValueString(pStep->fEndC, EP_CAPACITY));			// 종지용량
			// strData.Format("%d", (UINT)pStep->fRecVIGetTime/100);
			// m_StepGrid.SetValueRange(CGXRange(nRow+1, 11), strData);	
			break;
		}
	}

	m_StepGrid.LockUpdate(bLock);
	m_StepGrid.Redraw();

	UpdateData(false);
}

VOID CDetailAutoTestConditionDlg::DisplayProtectSettingGrid()
{
	CString strData = _T("");
	CStep *pStepInfo;
	STR_COMMON_STEP *pStep, stepData;

	int nStepRow = m_pTestCondition->GetTotalStepNo();
	int nRow = 0;
	bool bFirst = false;

	BOOL bLock = m_ProtectSettingGrid.LockUpdate();

	for(nRow =0 ; nRow < nStepRow; nRow++)
	{
		pStepInfo = m_pTestCondition->GetStep(nRow);
		if(pStepInfo == NULL) break;	
		stepData = pStepInfo->GetStepData();
		pStep = &stepData;

		switch(pStep->stepHeader.type)
		{
		case EP_TYPE_CHARGE:
			{	
				m_ProtectSettingGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE)); // 충전전압Limit
				m_ProtectSettingGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(pStep->fOverI, EP_CURRENT)); // 충전전류Limit
				m_ProtectSettingGrid.SetValueRange(CGXRange(3, 2), m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY)); // 충전상한용량
				m_ProtectSettingGrid.SetValueRange(CGXRange(4, 2), m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY)); // 충전하한용량

				if( bFirst == false )
				{
					bFirst = true;
					m_ProtectSettingGrid.SetValueRange(CGXRange(5, 2), m_pDoc->ValueString(pStep->fCompVHigh[1], EP_VOLTAGE)); // 충전상한취득전압
					m_ProtectSettingGrid.SetValueRange(CGXRange(6, 2), m_pDoc->ValueString(pStep->fCompVLow[1], EP_VOLTAGE)); // 충전하한취득전압
					strData.Format("%d", (UINT)pStep->fCompTimeV[1]/60);				
					m_ProtectSettingGrid.SetValueRange(CGXRange(7, 2), strData);											//	충전취득시간
				}
			}
			break;
		case EP_TYPE_DISCHARGE:
			{
				m_ProtectSettingGrid.SetValueRange(CGXRange(8, 2), m_pDoc->ValueString(pStep->fOverV, EP_VOLTAGE));	// 방전전압Limit
				m_ProtectSettingGrid.SetValueRange(CGXRange(9, 2), m_pDoc->ValueString(pStep->fOverI, EP_CURRENT)); // 방전전류Limit
				m_ProtectSettingGrid.SetValueRange(CGXRange(10, 2), m_pDoc->ValueString(pStep->fOverC, EP_CAPACITY)); // 방전상한용량
				m_ProtectSettingGrid.SetValueRange(CGXRange(11, 2), m_pDoc->ValueString(pStep->fLimitC, EP_CAPACITY)); // 방전하한용량
			}
			break;
		}
	}

	m_ProtectSettingGrid.LockUpdate(bLock);
	m_ProtectSettingGrid.Redraw();
}

VOID CDetailAutoTestConditionDlg::DisplayAutoSettingGrid()
{
	BOOL bLock = m_AutoSettingGrid.LockUpdate();

	m_AutoSettingGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(m_pTray->m_nAutoTabDeepth, EP_NOT_USE_DATA));
	m_AutoSettingGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(m_pTray->m_nAutoTrayHeight, EP_NOT_USE_DATA));
	m_AutoSettingGrid.SetValueRange(CGXRange(3, 2), m_pDoc->ValueString(m_pTray->m_nTrayType, EP_NOT_USE_DATA));
	m_AutoSettingGrid.SetValueRange(CGXRange(4, 2), m_pDoc->ValueString(m_pTray->m_nChannelInsertType, EP_NOT_USE_DATA));
	m_AutoSettingGrid.SetValueRange(CGXRange(5, 2), m_pDoc->ValueString(m_pTray->m_nContactErrlimit, EP_NOT_USE_DATA));
	m_AutoSettingGrid.SetValueRange(CGXRange(6, 2), m_pDoc->ValueString(m_pTray->m_nCapaErrlimit, EP_NOT_USE_DATA));

	m_AutoSettingGrid.LockUpdate(bLock);
	m_AutoSettingGrid.Redraw();
}

VOID CDetailAutoTestConditionDlg::DisplayContactSettingGrid()
{
	ASSERT(m_pTestCondition);

	STR_CHECK_PARAM *pCheckParam = m_pTestCondition->GetCheckParam();
	CString strData = _T("");

	BOOL bLock = m_ContactSettingGrid.LockUpdate();

	m_ContactSettingGrid.SetValueRange(CGXRange(1, 2), m_pDoc->ValueString(pCheckParam->fIRef, EP_CURRENT));		// 충전전류
	m_ContactSettingGrid.SetValueRange(CGXRange(2, 2), m_pDoc->ValueString(pCheckParam->fTime, EP_STEP_TIME));		// 충전시간
	m_ContactSettingGrid.SetValueRange(CGXRange(3, 2), m_pDoc->ValueString(pCheckParam->fVRef, EP_VOLTAGE));
	m_ContactSettingGrid.SetValueRange(CGXRange(4, 2), m_pDoc->ValueString(pCheckParam->fOCVUpperValue, EP_VOLTAGE));
	m_ContactSettingGrid.SetValueRange(CGXRange(5, 2), m_pDoc->ValueString(pCheckParam->fOCVLowerValue, EP_VOLTAGE));
	m_ContactSettingGrid.SetValueRange(CGXRange(6, 2), m_pDoc->ValueString(pCheckParam->fDeltaVoltage, EP_CURRENT));
	m_ContactSettingGrid.SetValueRange(CGXRange(7, 2), m_pDoc->ValueString(pCheckParam->fMaxFaultBattery, EP_CURRENT));
	m_ContactSettingGrid.SetValueRange(CGXRange(8, 2), m_pDoc->ValueString(pCheckParam->fDeltaVoltageLimit, EP_VOLTAGE));

	m_ContactSettingGrid.LockUpdate(bLock);
	m_ContactSettingGrid.Redraw();
}

VOID CDetailAutoTestConditionDlg::DisplayCondition()
{	
	CString strData = _T("");

	strData.Format("%s", m_pTestCondition->GetModelInfo()->szProcesstype);

	if( strData != _T("") )
	{		
		m_LabelType.SetText(strData);
	}
	else
	{
		strData.Format("%d", m_pTestCondition->GetModelInfo()->lID);
		m_LabelType.SetText(strData);
	}

	strData.Format("%d", m_pTestCondition->GetTestInfo()->lID);	
	m_LabelProcess.SetText(strData);

	DisplayStepGrid();

	if( m_StepGrid.GetRowCount() > 0 )
	{
		DisplayContactSettingGrid();
		DisplayProtectSettingGrid();
		DisplayAutoSettingGrid();
	}

	UpdateData(FALSE);
}

VOID CDetailAutoTestConditionDlg::InitFont()
{
	LOGFONT	LogFont;

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 15;

	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC3)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC4)->SetFont(&m_Font);
	GetDlgItem(IDC_STATIC5)->SetFont(&m_Font);

	GetDlgItem(IDC_REG_TEMP_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_REG_TEMP)->SetFont(&m_Font);
	GetDlgItem(IDC_RESISTANCE_RATE_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_RESISTANCE_RATE)->SetFont(&m_Font);
	GetDlgItem(IDC_CHARGE_LOW_VOLTAGE_CHK_TIME_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_CHARGE_LOW_VOLTAGE_CHK_TIME)->SetFont(&m_Font);
	GetDlgItem(IDC_CHARGE_VOLTAGE_CHK_TIME_STATIC)->SetFont(&m_Font);
	GetDlgItem(IDC_CHARGE_VOLTAGE_CHK_TIME)->SetFont(&m_Font);	
	GetDlgItem(IDC_FANOFF_STATIC)->SetFont(&m_Font);	
	GetDlgItem(IDC_CHARGE_FANOFF_CHK)->SetFont(&m_Font);	
}

VOID CDetailAutoTestConditionDlg::InitAutoSettingGrid()
{
	m_AutoSettingGrid.SubclassDlgItem(IDC_AUTO_SETTING_GRID, this);
	m_AutoSettingGrid.Initialize();

	CString strItem = _T("");

	BOOL bLock = m_AutoSettingGrid.LockUpdate();
	m_AutoSettingGrid.SetRowCount(6);
	m_AutoSettingGrid.SetColCount(2);
	m_AutoSettingGrid.m_bSameColSize = TRUE;
	m_AutoSettingGrid.m_bSameRowSize = TRUE;
	// m_ContactSettingGrid.SetDefaultRowHeight(20);
	m_AutoSettingGrid.SetRowHeight(0,1,0);

	m_AutoSettingGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetFont(CGXFont().SetSize(m_nFontSize).SetBold(TRUE)).SetTextColor(RGB(255,255,255)).SetInterior(RGB_LABEL_BACKGROUND));
	m_AutoSettingGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetFont(CGXFont().SetSize(m_nFontSize).SetBold(TRUE)));

	strItem.Format(TEXT_LANG[3]);//"TAB DEEPTH"
	m_AutoSettingGrid.SetValueRange(CGXRange(1, 1), strItem);

	strItem.Format(TEXT_LANG[4]);//"TRAY HEIGHT"
	m_AutoSettingGrid.SetValueRange(CGXRange(2, 1), strItem);

	strItem.Format(TEXT_LANG[5]);//"TRAY TYPE"
	m_AutoSettingGrid.SetValueRange(CGXRange(3, 1), strItem);

	strItem.Format(TEXT_LANG[6]);//"CHANNEL INSERT TYPE"
	m_AutoSettingGrid.SetValueRange(CGXRange(4, 1), strItem);

	strItem.Format(TEXT_LANG[7]);//"CONTACT ERROR LIMIT"
	m_AutoSettingGrid.SetValueRange(CGXRange(5, 1), strItem);

	strItem.Format(TEXT_LANG[8]);//"CAPA ERROR LIMIT"
	m_AutoSettingGrid.SetValueRange(CGXRange(6, 1), strItem);

	m_AutoSettingGrid.LockUpdate(bLock);
	m_AutoSettingGrid.Redraw();
	m_AutoSettingGrid.GetParam()->EnableSelection(FALSE);
}

VOID CDetailAutoTestConditionDlg::InitContactSettingGrid()
{
	m_ContactSettingGrid.SubclassDlgItem(IDC_DETAIL_CONTACT_GRID, this);
	m_ContactSettingGrid.Initialize();

	CString strItem = _T("");

	BOOL bLock = m_ContactSettingGrid.LockUpdate();
	m_ContactSettingGrid.SetRowCount(8);
	m_ContactSettingGrid.SetColCount(2);
	m_ContactSettingGrid.m_bSameColSize = TRUE;
	m_ContactSettingGrid.m_bSameRowSize = TRUE;
	// m_ContactSettingGrid.SetDefaultRowHeight(20);
	m_ContactSettingGrid.SetRowHeight(0,1,0);

	m_ContactSettingGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetFont(CGXFont().SetSize(m_nFontSize).SetBold(TRUE)).SetTextColor(RGB(255,255,255)).SetInterior(RGB_LABEL_BACKGROUND));
	m_ContactSettingGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetFont(CGXFont().SetSize(m_nFontSize).SetBold(TRUE)));

	strItem.Format(TEXT_LANG[9], m_pDoc->m_strIUnit );//"충전전류(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(1, 1), strItem);
	m_ContactSettingGrid.SetValueRange(CGXRange(2, 1), TEXT_LANG[10]);//"충전시간"

	strItem.Format(TEXT_LANG[11], m_pDoc->m_strVUnit );//"초기전압(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(3, 1), strItem);

	strItem.Format(TEXT_LANG[12], m_pDoc->m_strVUnit );//"전압상한(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(4, 1), strItem);

	strItem.Format(TEXT_LANG[13], m_pDoc->m_strVUnit );//"전압하한(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(5, 1), strItem);

	strItem.Format(TEXT_LANG[14], m_pDoc->m_strIUnit );//"전류상한(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(6, 1), strItem);

	strItem.Format(TEXT_LANG[15], m_pDoc->m_strIUnit );//"전류하한(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(7, 1), strItem);

	strItem.Format(TEXT_LANG[16], m_pDoc->m_strVUnit );//"Delta Voltage Limit(%s)"
	m_ContactSettingGrid.SetValueRange(CGXRange(8, 1), strItem);
	
	m_ContactSettingGrid.LockUpdate(bLock);
	m_ContactSettingGrid.Redraw();
	m_ContactSettingGrid.GetParam()->EnableSelection(FALSE);
}

VOID CDetailAutoTestConditionDlg::InitStepGrid()
{	
	m_StepGrid.SubclassDlgItem(IDC_DETAIL_STEP_GRID, this);
	m_StepGrid.Initialize();

	CString strItem = _T("");

	BOOL bLock = m_StepGrid.LockUpdate();
	m_StepGrid.SetDefaultRowHeight(30);	
	m_StepGrid.SetRowHeight(0,0,40);
	m_StepGrid.SetRowCount(0);
	m_StepGrid.SetColCount(15);	//13 -> 15 : UCA&LCA 20200307 KSJ	

	m_StepGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB_LABEL_BACKGROUND));
	m_StepGrid.SetStyleRange(CGXRange().SetRows(0,0), CGXStyle().SetFont(CGXFont().SetSize(m_nFontSize).SetBold(TRUE)).SetTextColor(RGB(255,255,255)).SetInterior(RGB_LABEL_BACKGROUND));

	m_StepGrid.SetValueRange(CGXRange(0, 1), TEXT_LANG[17]);//"Step"
	m_StepGrid.SetValueRange(CGXRange(0, 2), TEXT_LANG[18]);//"Action"
	m_StepGrid.SetValueRange(CGXRange(0, 3), TEXT_LANG[19]);//"Mode"

	strItem.Format(TEXT_LANG[20]+"\n(%s)", m_pDoc->m_strIUnit );//"정전류\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 4), strItem);
	strItem.Format(TEXT_LANG[21]+"\n(%s)", m_pDoc->m_strVUnit );//"정전압\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 5), strItem);
	m_StepGrid.SetValueRange(CGXRange(0, 6), TEXT_LANG[22]);//"시간"

	strItem.Format(TEXT_LANG[23]+"\n(%s)", m_pDoc->m_strIUnit );//"종지전류\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 7), strItem);
	strItem.Format(TEXT_LANG[24]+"\n(%s)", m_pDoc->m_strVUnit );//"종지전압\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 8), strItem);	
	strItem.Format(TEXT_LANG[25]+"\n(%s)", m_pDoc->m_strCUnit );//"종지용량\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 9), strItem);
	strItem.Format("Upper Capacity\n(%s)", m_pDoc->m_strCUnit );	//UCA 20200307 KSJ		
	m_StepGrid.SetValueRange(CGXRange(0, 10), strItem);
	strItem.Format("Lower Capacity\n(%s)", m_pDoc->m_strCUnit );	//LCA 20200307 KSJ	
	m_StepGrid.SetValueRange(CGXRange(0, 11), strItem);
	m_StepGrid.SetValueRange(CGXRange(0, 12), TEXT_LANG[26]+"\n(Min)");//"온도시간\n(Min)"	
	m_StepGrid.SetValueRange(CGXRange(0, 13), TEXT_LANG[27]+"\n"+TEXT_LANG[41]);//"Delta\nCapacity(%)"
	strItem.Format(TEXT_LANG[28]+"\n(%s)", m_pDoc->m_strVUnit );//"시작전압\n(%s)"
	m_StepGrid.SetValueRange(CGXRange(0, 14), strItem);	
	m_StepGrid.SetValueRange(CGXRange(0, 15), TEXT_LANG[29]);//"팬OFF공정"	

	m_StepGrid.m_nWidth[1] = 50;
	m_StepGrid.m_nWidth[2] = 100;
	m_StepGrid.m_nWidth[3] = 100;
	m_StepGrid.m_nWidth[4] = 100;
	m_StepGrid.m_nWidth[5] = 100;
	m_StepGrid.m_nWidth[6] = 100;
	m_StepGrid.m_nWidth[7] = 90;
	m_StepGrid.m_nWidth[8] = 90;
	m_StepGrid.m_nWidth[9] = 90;
	m_StepGrid.m_nWidth[10] = 90;	//UCA 20200307 KSJ
	m_StepGrid.m_nWidth[11] = 90;	//LCA 20200307 KSJ
	m_StepGrid.m_nWidth[12] = 90;
	m_StepGrid.m_nWidth[13] = 120;
	m_StepGrid.m_nWidth[14] = 90;
	m_StepGrid.m_nWidth[15] = 80;
	m_StepGrid.LockUpdate(bLock);
	m_StepGrid.Redraw();
	m_StepGrid.GetParam()->EnableSelection(FALSE);
}

VOID CDetailAutoTestConditionDlg::InitProtectSettingGrid()
{
	m_ProtectSettingGrid.SubclassDlgItem(IDC_DETAIL_PROTECT_GRID, this);
	m_ProtectSettingGrid.Initialize();
	BOOL bLock = m_ProtectSettingGrid.LockUpdate();

	m_ProtectSettingGrid.SetRowCount(11);
	m_ProtectSettingGrid.SetColCount(2);
	m_ProtectSettingGrid.m_bSameColSize = TRUE;
	m_ProtectSettingGrid.m_bSameRowSize = TRUE;
	// m_ProtectSettingGrid.SetDefaultRowHeight(25);
	m_ProtectSettingGrid.SetRowHeight(0,1,0);

	// m_ProtectSettingGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB_LABEL_BACKGROUND));
	m_ProtectSettingGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetFont(CGXFont().SetSize(m_nFontSize).SetBold(TRUE)).SetTextColor(RGB(255,255,255)).SetInterior(RGB_LABEL_BACKGROUND));
	m_ProtectSettingGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetFont(CGXFont().SetSize(m_nFontSize).SetBold(TRUE)));
	m_ProtectSettingGrid.SetValueRange(CGXRange(1, 1), TEXT_LANG[30]);//"충전전압Limit"
	m_ProtectSettingGrid.SetValueRange(CGXRange(2, 1), TEXT_LANG[31]);//"충전전류Limit"	
	m_ProtectSettingGrid.SetValueRange(CGXRange(3, 1), TEXT_LANG[32]);//"충전상한용량"
	m_ProtectSettingGrid.SetValueRange(CGXRange(4, 1), TEXT_LANG[33]);//"충전하한용량"
	m_ProtectSettingGrid.SetValueRange(CGXRange(5, 1), TEXT_LANG[34]);//"충전상한취득전압"
	m_ProtectSettingGrid.SetValueRange(CGXRange(6, 1), TEXT_LANG[35]);//"충전하한취득전압"
	m_ProtectSettingGrid.SetValueRange(CGXRange(7, 1), TEXT_LANG[36]);//"충전전압취득시간"
	m_ProtectSettingGrid.SetValueRange(CGXRange(8, 1), TEXT_LANG[37]);//"방전전압Limit"
	m_ProtectSettingGrid.SetValueRange(CGXRange(9, 1), TEXT_LANG[38]);//"방전전류Limit"	
	m_ProtectSettingGrid.SetValueRange(CGXRange(10, 1), TEXT_LANG[39]);//"방전상한용량"
	m_ProtectSettingGrid.SetValueRange(CGXRange(11, 1), TEXT_LANG[40]);//"방전하한용량"

	m_ProtectSettingGrid.LockUpdate(bLock);
	m_ProtectSettingGrid.Redraw();
	m_ProtectSettingGrid.GetParam()->EnableSelection(FALSE);
}