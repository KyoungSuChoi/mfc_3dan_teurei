// NormalSensorMapDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "NormalSensorMapDlg.h"


// CNormalSensorMapDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNormalSensorMapDlg, CDialog)

CNormalSensorMapDlg::CNormalSensorMapDlg(int nDeviceID, bool& bReadySetCmd, IO_Data& sendJigIOData, IO_Data& recvJigIOData, IO_Data& sendFanIOData, IO_Data& recvFanIOData, CWnd* pParent /*=NULL*/)
	: CDialog(CNormalSensorMapDlg::IDD, pParent), m_bReadySetCmd(bReadySetCmd), m_SendJigIOData(sendJigIOData), m_RecvJigIOData(recvJigIOData), m_SendFanIOData(sendFanIOData), m_RecvFanIOData(recvFanIOData)
{
	m_nDeviceID = nDeviceID;

}

CNormalSensorMapDlg::~CNormalSensorMapDlg()
{
}

void CNormalSensorMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LABEL_IO_1, m_IOLabelArray[0]);
	DDX_Control(pDX, IDC_LABEL_IO_2, m_IOLabelArray[1]);
	DDX_Control(pDX, IDC_LABEL_IO_3, m_IOLabelArray[2]);
	DDX_Control(pDX, IDC_LABEL_IO_4, m_IOLabelArray[3]);
	DDX_Control(pDX, IDC_LABEL_IO_5, m_IOLabelArray[4]);
	DDX_Control(pDX, IDC_LABEL_IO_6, m_IOLabelArray[5]);
	DDX_Control(pDX, IDC_LABEL_IO_7, m_IOLabelArray[6]);
	DDX_Control(pDX, IDC_LABEL_IO_8, m_IOLabelArray[7]);
	DDX_Control(pDX, IDC_LABEL_IO_9, m_IOLabelArray[8]);
	DDX_Control(pDX, IDC_LABEL_IO_10, m_IOLabelArray[9]);
	DDX_Control(pDX, IDC_LABEL_IO_11, m_IOLabelArray[10]);
	DDX_Control(pDX, IDC_LABEL_IO_12, m_IOLabelArray[11]);

	DDX_Control(pDX, IDB_IO_ON_1, m_OnBtnArray[0]);
	DDX_Control(pDX, IDB_IO_ON_2, m_OnBtnArray[1]);
	DDX_Control(pDX, IDB_IO_ON_3, m_OnBtnArray[2]);
	DDX_Control(pDX, IDB_IO_ON_4, m_OnBtnArray[3]);
	DDX_Control(pDX, IDB_IO_ON_5, m_OnBtnArray[4]);
	DDX_Control(pDX, IDB_IO_ON_6, m_OnBtnArray[5]);
	DDX_Control(pDX, IDB_IO_ON_7, m_OnBtnArray[6]);
	DDX_Control(pDX, IDB_IO_ON_8, m_OnBtnArray[7]);
	DDX_Control(pDX, IDB_IO_ON_9, m_OnBtnArray[8]);
	DDX_Control(pDX, IDB_IO_ON_10, m_OnBtnArray[9]);
	DDX_Control(pDX, IDB_IO_ON_11, m_OnBtnArray[10]);
	DDX_Control(pDX, IDB_IO_ON_12, m_OnBtnArray[11]);

	DDX_Control(pDX, IDB_IO_OFF_1, m_OffBtnArray[0]);
	DDX_Control(pDX, IDB_IO_OFF_2, m_OffBtnArray[1]);
	DDX_Control(pDX, IDB_IO_OFF_3, m_OffBtnArray[2]);
	DDX_Control(pDX, IDB_IO_OFF_4, m_OffBtnArray[3]);
	DDX_Control(pDX, IDB_IO_OFF_5, m_OffBtnArray[4]);
	DDX_Control(pDX, IDB_IO_OFF_6, m_OffBtnArray[5]);
	DDX_Control(pDX, IDB_IO_OFF_7, m_OffBtnArray[6]);
	DDX_Control(pDX, IDB_IO_OFF_8, m_OffBtnArray[7]);
	DDX_Control(pDX, IDB_IO_OFF_9, m_OffBtnArray[8]);
	DDX_Control(pDX, IDB_IO_OFF_10, m_OffBtnArray[9]);
	DDX_Control(pDX, IDB_IO_OFF_11, m_OffBtnArray[10]);
	DDX_Control(pDX, IDB_IO_OFF_12, m_OffBtnArray[11]);

}


BEGIN_MESSAGE_MAP(CNormalSensorMapDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CNormalSensorMapDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDB_IO_ON_1, &CNormalSensorMapDlg::OnBtnClickedOn001)
	ON_BN_CLICKED(IDB_IO_ON_2, &CNormalSensorMapDlg::OnBtnClickedOn002)
	ON_BN_CLICKED(IDB_IO_ON_3, &CNormalSensorMapDlg::OnBtnClickedOn003)
	ON_BN_CLICKED(IDB_IO_ON_4, &CNormalSensorMapDlg::OnBtnClickedOn004)
	ON_BN_CLICKED(IDB_IO_ON_5, &CNormalSensorMapDlg::OnBtnClickedOn005)
	ON_BN_CLICKED(IDB_IO_ON_6, &CNormalSensorMapDlg::OnBtnClickedOn006)
	ON_BN_CLICKED(IDB_IO_ON_7, &CNormalSensorMapDlg::OnBtnClickedOn007)
	ON_BN_CLICKED(IDB_IO_ON_8, &CNormalSensorMapDlg::OnBtnClickedOn008)
	ON_BN_CLICKED(IDB_IO_ON_9, &CNormalSensorMapDlg::OnBtnClickedOn009)
	ON_BN_CLICKED(IDB_IO_ON_10, &CNormalSensorMapDlg::OnBtnClickedOn010)
	ON_BN_CLICKED(IDB_IO_ON_11, &CNormalSensorMapDlg::OnBtnClickedOn011)
	ON_BN_CLICKED(IDB_IO_ON_12, &CNormalSensorMapDlg::OnBtnClickedOn012)
	ON_BN_CLICKED(IDB_IO_OFF_1, &CNormalSensorMapDlg::OnBtnClickedOff001)
	ON_BN_CLICKED(IDB_IO_OFF_2, &CNormalSensorMapDlg::OnBtnClickedOff002)
	ON_BN_CLICKED(IDB_IO_OFF_3, &CNormalSensorMapDlg::OnBtnClickedOff003)
	ON_BN_CLICKED(IDB_IO_OFF_4, &CNormalSensorMapDlg::OnBtnClickedOff004)
	ON_BN_CLICKED(IDB_IO_OFF_5, &CNormalSensorMapDlg::OnBtnClickedOff005)
	ON_BN_CLICKED(IDB_IO_OFF_6, &CNormalSensorMapDlg::OnBtnClickedOff006)
	ON_BN_CLICKED(IDB_IO_OFF_7, &CNormalSensorMapDlg::OnBtnClickedOff007)
	ON_BN_CLICKED(IDB_IO_OFF_8, &CNormalSensorMapDlg::OnBtnClickedOff008)
	ON_BN_CLICKED(IDB_IO_OFF_9, &CNormalSensorMapDlg::OnBtnClickedOff009)
	ON_BN_CLICKED(IDB_IO_OFF_10, &CNormalSensorMapDlg::OnBtnClickedOff010)
	ON_BN_CLICKED(IDB_IO_OFF_11, &CNormalSensorMapDlg::OnBtnClickedOff011)
	ON_BN_CLICKED(IDB_IO_OFF_12, &CNormalSensorMapDlg::OnBtnClickedOff012)
END_MESSAGE_MAP()


// CNormalSensorMapDlg 메시지 처리기입니다.

void CNormalSensorMapDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	this->ShowWindow(SW_HIDE);
	OnOK();
}
BOOL CNormalSensorMapDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	LoadIoConfig();

	for(int i = 0; i < 156; i++ )
	{
		CWnd* pWnd = GetDlgItem(IDC_READ_CHECK1+i);

		if(pWnd != NULL)
		{
			// Read Addr Set
			m_stateButton[i].SubclassWindow(pWnd->GetSafeHwnd());
			m_stateButton[i].SetImage( IDB_RED_BUTTON, 15);
			m_stateButton[i].Depress(FALSE);
			m_stateButton[i].SetWindowText(m_strInputName[i]);

			m_stateButton[i].ShowWindow(TRUE);
		}
		else
		{
			m_stateButton[i].ShowWindow(FALSE);
		}
		
	}

	// KSCHOI
	for(int i = 0; i < 12; i++)
	{
		m_IOLabelArray[i].SetFontSize(14)
			.SetTextColor(RGB_LABEL_FONT_STAGENAME)
			.SetBkColor(RGB_LABEL_BACKGROUND)
			.SetFontBold(TRUE);

		m_OnBtnArray[i].SetFontStyle(24, 1);
		m_OnBtnArray[i].SetColor(RGB_BLACK, RGB_GRAY);

		m_OffBtnArray[i].SetFontStyle(24, 1);
		m_OffBtnArray[i].SetColor(RGB_BLACK, RGB_GRAY);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
}

bool CNormalSensorMapDlg::LoadIoConfig()
{
	CStdioFile	file;

	CFileException* e;
	e = new CFileException;

	CString strFileName;
	if(g_nLanguage == LANGUAGE_CHI)
	{
		strFileName = "ioconfig_ch.cfg";
	}
	else
	{
		strFileName = "ioconfig.cfg";
	}


	if(!file.Open("C:\\Program Files (x86)\\PNE CTS\\" + strFileName, CFile::modeRead|CFile::typeBinary ))
	{
		return FALSE;
	}

	WCHAR *buffer;
	buffer = new WCHAR[file.GetLength()];

	WCHAR buf[1];

	int i = 0;
	int nRength = 0;

	CString strData;
	CString strTemp;

	file.Read(buf, sizeof(WCHAR));

	TRY
	{
		while(TRUE)
		{

			file.Read(buf, sizeof(WCHAR));

			if( buf[0] == 0x0d )
			{
				buffer[i] = 0x00;

				int nLen = WideCharToMultiByte(CP_ACP, 0, buffer, -1, NULL, 0, NULL, NULL);			

				char* pMultibyte  = new char[nLen];
				memset(pMultibyte, 0x00, (nLen)*sizeof(char));

				WideCharToMultiByte(CP_ACP, 0, buffer, -1, pMultibyte, nLen, NULL, NULL);

				ZeroMemory( buffer, sizeof(WCHAR) * file.GetLength());			

				strData = pMultibyte;

				delete pMultibyte;

				i = 0;

				if(!strData.IsEmpty())
				{
					ParsingConfig(strData);

				}
			}
			else
			{
				buffer[i] = buf[0];		

				i++;
				nRength++;
			}

			if( nRength > file.GetLength()-2 )
			{
				break;
			}
		}

	}
	CATCH (CFileException, e)
	{
		AfxMessageBox(e->m_cause);
		file.Close();
		return FALSE;
	}
	END_CATCH
		file.Close();	
	return TRUE;
}

void CNormalSensorMapDlg::ParsingConfig(CString strConfig)
{
	if(strConfig.IsEmpty())		return;

	int a, b;
	a = strConfig.Find('[');
	b = strConfig.ReverseFind(']');

	if(b > 0 && a >= b)	return;

	CString strTitle, strData;
	strTitle = strConfig.Mid(a+1, b-a-1);

	a = strConfig.ReverseFind('=');

	strData = strConfig.Mid( a+1 );

	strData.TrimLeft(" ");
	strData.TrimRight(" ");

	if(strData.IsEmpty())	return;

	CString csByteIndex;
	CString csBitIndex;

	strTitle.Delete(0, 5);
	csByteIndex = strTitle.Mid(0, 2);
	csBitIndex = strTitle.Mid(3, 1);

	int iByteIndex = atoi(csByteIndex);
	int iBitIndex = atoi(csBitIndex);

	if(iByteIndex == 0 || iBitIndex == 0)
		return;

	int iByteStartIndex = 10;

	int iIndex = ((iByteIndex - iByteStartIndex) * 8) + (iBitIndex - 1);

	m_strInputName[iIndex] = strData;
}

bool CNormalSensorMapDlg::UpdateAdditionalSensorState( unsigned char* pSensorState )
{
	int nIndex = 0;

	int startByte = INPUT_IO_START_ADDRESS / 8;		// 5
	int startBitIndex = INPUT_IO_START_ADDRESS % 8; // 4

	for(int i = startByte; i < INPUT_IO_END_ADDRESS/8; i++ )
	{
		if(i == startByte)
		{
			for(int j = startBitIndex; j <  8; j++)
			{
				nIndex = (j-startBitIndex) + (i-startByte) * 8;

				if(nIndex >= MAX_LED_BUTTON_COUNT || nIndex >= 156)
					continue;

				m_stateButton[nIndex].Depress(pSensorState[i] & (0x01<<j));
			}
		}
		else
		{
			for(int j = 0; j <  8; j++)
			{
				nIndex = j + (i-startByte) * 8 - startBitIndex;

				if(nIndex >= MAX_LED_BUTTON_COUNT || nIndex >= 156)
					continue;

				m_stateButton[nIndex].Depress(pSensorState[i] & (0x01<<j));
			}
		}
	}

	// OUTPUT BUTTON HIGHLIGHT START
	m_OnBtnArray[0].SetColor(RGB_BLACK, m_RecvJigIOData.ch_12?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[1].SetColor(RGB_BLACK, m_RecvJigIOData.ch_13?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[2].SetColor(RGB_BLACK, m_RecvJigIOData.ch_14?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[3].SetColor(RGB_BLACK, m_RecvJigIOData.ch_15?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[4].SetColor(RGB_BLACK, m_RecvFanIOData.ch_0?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[5].SetColor(RGB_BLACK, m_RecvFanIOData.ch_1?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[6].SetColor(RGB_BLACK, m_RecvFanIOData.ch_2?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[7].SetColor(RGB_BLACK, m_RecvFanIOData.ch_3?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[8].SetColor(RGB_BLACK, m_RecvFanIOData.ch_4?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[9].SetColor(RGB_BLACK, m_RecvFanIOData.ch_5?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[10].SetColor(RGB_BLACK, m_RecvFanIOData.ch_6?RGB_LIGHTGREEN:RGB_GRAY);
	m_OnBtnArray[11].SetColor(RGB_BLACK, m_RecvFanIOData.ch_7?RGB_LIGHTGREEN:RGB_GRAY);

	m_OffBtnArray[0].SetColor(RGB_BLACK, m_RecvJigIOData.ch_12?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[1].SetColor(RGB_BLACK, m_RecvJigIOData.ch_13?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[2].SetColor(RGB_BLACK, m_RecvJigIOData.ch_14?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[3].SetColor(RGB_BLACK, m_RecvJigIOData.ch_15?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[4].SetColor(RGB_BLACK, m_RecvFanIOData.ch_0?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[5].SetColor(RGB_BLACK, m_RecvFanIOData.ch_1?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[6].SetColor(RGB_BLACK, m_RecvFanIOData.ch_2?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[7].SetColor(RGB_BLACK, m_RecvFanIOData.ch_3?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[8].SetColor(RGB_BLACK, m_RecvFanIOData.ch_4?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[9].SetColor(RGB_BLACK, m_RecvFanIOData.ch_5?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[10].SetColor(RGB_BLACK, m_RecvFanIOData.ch_6?RGB_GRAY:RGB_LIGHTGREEN);
	m_OffBtnArray[11].SetColor(RGB_BLACK, m_RecvFanIOData.ch_7?RGB_GRAY:RGB_LIGHTGREEN);

	return true;
}

void CNormalSensorMapDlg::OnBtnClickedOn001()
{
	m_SendJigIOData.ch_12 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn002()
{
	m_SendJigIOData.ch_13 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn003()
{
	m_SendJigIOData.ch_14 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn004()
{
	m_SendJigIOData.ch_15 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn005()
{
	m_SendFanIOData.ch_0 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn006()
{
	m_SendFanIOData.ch_1 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn007()
{
	m_SendFanIOData.ch_2 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn008()
{
	m_SendFanIOData.ch_3 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn009()
{
	m_SendFanIOData.ch_4 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn010()
{
	m_SendFanIOData.ch_5 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn011()
{
	m_SendFanIOData.ch_6 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOn012()
{
	m_SendFanIOData.ch_7 = 1;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff001()
{
	m_SendJigIOData.ch_12 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff002()
{
	m_SendJigIOData.ch_13 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff003()
{
	m_SendJigIOData.ch_14 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff004()
{
	m_SendJigIOData.ch_15 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff005()
{
	m_SendFanIOData.ch_0 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff006()
{
	m_SendFanIOData.ch_1 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff007()
{
	m_SendFanIOData.ch_2 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff008()
{
	m_SendFanIOData.ch_3 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff009()
{
	m_SendFanIOData.ch_4 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff010()
{
	m_SendFanIOData.ch_5 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff011()
{
	m_SendFanIOData.ch_6 = 0;

	m_bReadySetCmd = true;
}

void CNormalSensorMapDlg::OnBtnClickedOff012()
{
	m_SendFanIOData.ch_7 = 0;

	m_bReadySetCmd = true;
}