#pragma once

#include "CalibrationData.h"
//#include "CalibratorDlg.h"

#define FMS_RESPONSE(FMS_OP_COMMAND)\
	m_vUnit[indexNum]->fnGetFMS()->fnSend_##FMS_OP_COMMAND(_rec);\

#define UM_AUTO_CALI_RUN WM_USER+2001

typedef struct st_FMS_ChannelCode
{
	INT nCode;
	CHAR szMessage[4+1];
}FMS_ChannelCode;

typedef struct st_FMS_TabDeepth
{
	INT nValue;
}FMS_TabDeepth;

typedef struct st_FMS_TrayHeight
{
	INT nValue;
}FMS_TrayHeight;

#define _MIN_CC_I	100.0f
#define _MAX_CC_I	50000.0f
#define _MIN_CV_V	2500.0f
#define _MAX_CV_V	4350.0f
#define _MAX_I		50000.0f
#define _MAX_V		5000.0f
#define _OVP_V		4400.0f

// 20201013 KSCHOI S2F41 RCMD1 TIMEOUTSEC SET 9 SECOND START
#define _FMS_MSG_TIMEOUT	9000
// 20201013 KSCHOI S2F41 RCMD1 TIMEOUTSEC SET 9 SECOND END

class CFMS
{
public:
	CFMS(void);
	virtual ~CFMS(void);
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

	VOID	fnInit(CFormModule* _Module);

	INT		strLinker(CHAR *msg, VOID * _dest, INT* mesmap);
	INT		strCutter(CHAR *msg, VOID * _dest, INT* mesmap);

public:
	// GUI -> IMS
	BOOL m_OverCCheck;  //20200411 엄륭 용량 상한 관련
	int m_nChgChkF; //20200807 엄륭
	int m_ProcessNo;
	CString fnGetTrayType();

	FMS_ERRORCODE fnSetReserve(st_CHARGER_INRESEVE inreserv);
	FMS_ERRORCODE fnReserveProc();	

	VOID				fnSetFMSStateCode(UINT, FMS_STATE_CODE);
	FMS_STATE_CODE		fnGetFMSStateCode();
	
	st_HSMS_PACKET* fnGetHsmsPack(){return &m_HsmsPacket;}	

	VOID fnGetFMSImpossbleCode(st_IMPOSSBLE_STATE& _sate);	

	VOID fnGetFMSNewImpossbleCode(st_NEW_IMPOSSBLE_STATE& _sate);	
	
	BOOL fnRun();
	BOOL fnCalRun();
// 20201013 KSCHOI Critical Memory Issue Bug Fixed. START
//	int GetHWChCnt(int nBoardNo);
// 20201013 KSCHOI Critical Memory Issue Bug Fixed. END

	bool fnSetTrayID(CString strTrayID);
	INT fnGetStageNo();
	CString fnGetTrayID_1();
	CString fnGetTrayID_2();

	BOOL fnSendToLocalAlarm();
	//////////////////////////////////////////////////////////////////////////
	CString Time_Conversion(CString str_Time);
	FMS_ERRORCODE fnMakeResult();
	FMS_ERRORCODE fnMakeContactResult();	
	FMS_ERRORCODE fnMisSendMakeResult_CHARGE(CString, st_CHARGER_INRESEVE&);		
	//////////////////////////////////////////////////////////////////////////

	FMS_ERRORCODE fnSetOperationMode(WORD _code);
	CString fnGetResultData(){return m_str_Result_Data;}
	
	st_Result_Step_Result* fnGetStepResult(){ return m_sStepResult; }
	st_RESULT_FILE fnGetResultFile() { return m_sResultFile; }

	//////////////////////////////////////////////////////////////////////////
	FMS_ERRORCODE SendSBCInitCommand();
	FMS_ERRORCODE SendGUIInitCommand();
	//////////////////////////////////////////////////////////////////////////
	VOID fnSendToHost_S1F18( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode );	
	VOID fnSendToHost_S2F42( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode );
	
	VOID fnSendToHost_S5F1( CString alarmCode, CString alarmText );
	VOID fnSendToHost_S5F1_Reset();
	VOID fnSendToHost_S5F102( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode );
// 20201118 KSCHOI Add Break Fire Report START
	VOID fnSendToHost_S5F3( BOOL bSet, short nDevId = 1, BOOL bTrayState = 0, long lSysByte = 0);
// 20201118 KSCHOI Add Break Fire Report END

	VOID fnSendToHost_S6F11C4( short nDevId, long lSysByte );
	VOID fnSendToHost_S6F11C6( short nDevId, long lSysByte );
	VOID fnSendToHost_S6F11C7( short nDevId, long lSysByte );
	VOID fnSendToHost_S6F11C1( short nDevId, long lSysByte );
	VOID fnSendToHost_S6F11C5( short nDevId, long lSysByte );	
	VOID fnSendToHost_S6F11C12( short nDevId, long lSysByte );
	VOID fnSendToHost_S6F11C13( short nDevId, long lSysByte );
	VOID fnSendToHost_S6F11C14( short nDevId, long lSysByte );
	VOID fnSendToHost_S6F11C15( short nDevId, long lSysByte );

	VOID fnSendToHost_S6F11C16( short nDevId, long lSysByte ); //20190903 KSJ
	VOID fnSendToHost_S6F11C17( short nDevId, long lSysByte ); //19-01-24 엄륭 MES 사양 변경 
	VOID fnSendToHost_S6F11C18( short nDevId, long lSysByte ); //19-01-24 엄륭 MES 사양 변경 
	VOID fnSendToHost_S6F11C19( short nDevId, long lSysByte ); //19-01-24 엄륭 MES 사양 변경 


	VOID fnSendToHost_S6F11C200( short nDevId, long lSysByte );
	

	// BOOL fnResultDataStateUpdate(CHAR*, RESULTDATA_STATE _rState);
private:
	BOOL fnRangeCheck( STEP_VALUE_RANGE _range, CHAR* _szValue, FLOAT& _fValue, FMS_Step_Type fmsStepType = FMS_SKIP );
	BOOL fnLoadWorkInfo(INT64);
	BOOL fnTransFMSWorkInfoToCTSWorkInfo();
	INT	fnSendConditionToModule();
	BOOL fnCheckProcAndSystemType(long lProcTypeID);
	INT fnStepValidityCheck();
	BOOL fnMakeTrayInfo(bool bNew = TRUE);

	//////////////////////////////////////////////////////////////////////////
	//Run
	BOOL FileNameCheck(char *szfileName);
	BOOL CheckFileNameValidate(CString strFileName, CString &strNewFileName, int nModuleID, int nGroupIndex = 0 );
	CString CreateResultFilePath(int nModuleID, CString strDataFolder, CString strLot, CString strTray);
	INT ShowRunInformation(int nModuleID, int nGroupIndex, BOOL bNewFileName, long lProcPK);
	BOOL SendLastTrayStateData(int nModuleID, int nGroupIndex);
	DWORD CheckEnableRun(int nModuleID);
	BOOL SendProcedureLogToDataBase(int nModuleID, int nTrayIndex);
	//////////////////////////////////////////////////////////////////////////

	CStep* fnGetStepList(){return m_pStep;}
	//////////////////////////////////////////////////////////////////////////
	BOOL fnLoadWorkInfo_CHARGER(INT64, st_CHARGER_INRESEVE&);
	//////////////////////////////////////////////////////////////////////////
	BOOL fnLoadChannelCode();
	BOOL fnLoadTabDeepth();
	BOOL fnLoadTrayHeight();
	
	// GUI -> SBC
	INT fnGetTabDeepth( WORD _code );
	INT fnGetTrayHeight( WORD _code );	
	CHAR* fnGetChannelCode(INT _code);	

	VOID fnResetChannelCode();		// tabdeepth, trayheight, channelcode
	//////////////////////////////////////////////////////////////////////////	
	
private:
	COleDateTime	m_OleRunStartTime;
	COleDateTime	m_OleRunEndTime;

private:
	INT m_nModuleID;
	CFormModule* m_pModule;	
	

private:	
	st_CHARGER_INRESEVE m_inreserv;
private:
	CTestCondition m_TestCondition;
// 20201013 KSCHOI Critical Memory Issue Bug Fixed. START
//	CCalibrationData	m_calibrtaionData;
// 20201013 KSCHOI Critical Memory Issue Bug Fixed. END

	UINT m_totStep;
	CStep *m_pStep;
private:
	EP_RUN_CMD_INFO	m_runInfo;
	EP_CAL_SELECT_INFO m_SelInfo;
private:
	UINT m_Mode;
	FMS_STATE_CODE m_FMSStateCode;

private:	
	st_HSMS_PACKET m_HsmsPacket;	

private:
	CString m_strDataFolder;
	int		m_nTimeFolderInterval;
	BOOL	m_bFolderTime;
	BOOL	m_bFolderModuleName;
	BOOL	m_bFolderLot;
	BOOL	m_bFolderTray;

private:
	CString m_resultfilePathNName;
	CString m_str_Result_Data;
	
	st_Result_Step_Result m_sStepResult[100];
	st_RESULT_FILE m_sResultFile;

// 	st_RESULT_FILE_RESPONSE_L m_Result_Data_L;
// 	st_Result_Step_Set m_total_Step[100];
// 	st_RESULT_FILE_RESPONSE_R m_Result_Data_R;

private:
	std::vector<st_FMS_ChannelCode *> m_vChannelCode;
	std::vector<st_FMS_TabDeepth *> m_vTabDeepth;
	std::vector<st_FMS_TrayHeight *> m_vTrayHeight;

	CString m_strTraytype_ims;

private:
	INT m_MisSendState;
	INT m_SendMisIdx;
	INT m_SendMisCnt;	

// 	st_RESULT_FILE_RESPONSE_L m_MisSend_Result_Data_L;
// 	st_Result_Step_Set m_MisSend_total_Step[100];
// 	st_RESULT_FILE_RESPONSE_R m_MisSend_Result_Data_R;

	CString m_str_MisSendResult_Data;
public:
	//결과 파일 상태가 END
	//상태 강제 전이.....
	INT m_PreState;
public:
	//DWORD fnGetLastState(){return m_LastState;}
	//VOID fnSetLastState(DWORD _state){m_LastState = _state;}
	VOID fnSetError(){m_bError = TRUE;}
	BOOL fnGetError()
	{
		return m_bError;
	}
	VOID fnResetError(){m_bError = FALSE;}
private:
	BOOL m_bError;
public:
	DWORD m_LastState;
	DWORD m_LastMode;	//20190929 KSJ
	CString m_LastTrayID[2];	//20190929 KSJ
	BOOL m_LastInit;	//20190929 KSJ

// 20201118 KSCHOI Add Break Fire Report START
	VOID	SetFireAlarmStatus(BOOL bStatus);
	BOOL	GetFireAlarmStatus();
// 20201118 KSCHOI Add Break Fire Report END

// 20210105 KSCHOI Add SBCNetworkCheck START
	BOOL	SendSBCNetworkCheck();
// 20210105 KSCHOI Add SBCNetworkCheck END
private:	
	CHAR m_Result_FIle_Type;
};
