#include "StdAfx.h"
#include "CMySql_DB.h"


CMySql_DB::CMySql_DB(void)
{
	m_connection = NULL;
	m_bConnectSql = false;
}

CMySql_DB::~CMySql_DB(void)
{	
	
}

bool CMySql_DB::IsMySqlLive()
{
	if( m_bConnectSql == false )
	{
		return false;
	}
	
	int nRtn = 0;
	nRtn = mysql_ping(m_connection);
	
	if( nRtn != 0 )
	{
		m_bConnectSql = false;
	}
	
	return m_bConnectSql;
}

void CMySql_DB::Fun_DisConnect()
{	
	if( fnGetConnState() == true )
	{
		mysql_close(m_connection);
		m_connection = NULL;

		fnSetConnState( false );
	}
}

bool CMySql_DB::Fun_Connect()
{
	m_bSendFlag = false;

	if( fnGetConnState() == true )
	{
		return true;
	}	

	CString strAddress=_T(""), strDB_Name=_T(""), strUserID=_T(""), strPassword=_T("");
	
	int nPortNum = 0;

	strAddress	= AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "MonIP", "localhost");	
	nPortNum	= AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "MonPortNum", 3306);	
	strDB_Name	= AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "DB_Name", "totalmonitor");
	strUserID	= AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "UserID", "mondev");
	strPassword = AfxGetApp()->GetProfileString(FROM_SYSTEM_SECTION, "Password", "xhdgkq~!");	

	mysql_init(&m_dberrconn);		

	try
	{
		// DB 연결		
		m_connection = mysql_real_connect(&m_dberrconn, strAddress, strUserID, strPassword, strDB_Name, nPortNum,(char *)NULL, 0);
	}	
	catch (CException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();
	}
	
	if( m_connection == NULL )
	{
		// AfxMessageBox("PNE 통합모니터링 시스템 연결에 실패했습니다!", MB_ICONINFORMATION);
		return false;
	}	

	// 한글사용을위해추가.
	mysql_query(m_connection,"set session character_set_connection=euckr;");
	mysql_query(m_connection,"set session character_set_results=euckr;");
	mysql_query(m_connection,"set session character_set_client=euckr;");

	fnSetConnState(true);

	return TRUE;
}

int	CMySql_DB::Fun_ExecuteQuery( CString strQuery )
{
	int nRtn = 0;
	try
	{	
		if( m_connection != NULL )
		{
			nRtn = mysql_query(m_connection, strQuery);		
		}
	}
	catch (CException* e)
	{
		m_bConnectSql = false;
		
		// TCHAR sz[255];
		// e->GetErrorMessage(sz,255);
		// AfxMessageBox(sz);
		// e->Delete();		
	}
	return nRtn;
}