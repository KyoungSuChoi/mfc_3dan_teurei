// MiswiringPointSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "MiswiringPointSetDlg.h"


// CMiswiringPointSetDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMiswiringPointSetDlg, CDialog)

CMiswiringPointSetDlg::CMiswiringPointSetDlg(CMiswiringPoint* pMissPoint, CWnd* pParent /*=NULL*/)
	: CDialog(CMiswiringPointSetDlg::IDD, pParent)
{
	m_pMissPoint = pMissPoint;
}

CMiswiringPointSetDlg::~CMiswiringPointSetDlg()
{
}

void CMiswiringPointSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_POINT_V, m_wndListPointV);
	DDX_Control(pDX, IDC_POINT_I, m_wndListPointI);
}


BEGIN_MESSAGE_MAP(CMiswiringPointSetDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CMiswiringPointSetDlg::OnBnClickedOk)
	ON_MESSAGE(WM_LIST_EDIT_CHANGE, OnChangeListEdit)
END_MESSAGE_MAP()


// CMiswiringPointSetDlg 메시지 처리기입니다.


void CMiswiringPointSetDlg::LoadMeasureData()
{
	int i=0;
	int nDataCnt = 0;
	CString strTemp;

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	nDataCnt = m_pMissPoint->GetVMeasureCnt();
	if(nDataCnt >20)
	{
		nDataCnt = 0;
	}
	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointV.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMissPoint->GetVData(i) );
		m_wndListPointV.SetItemText(i, 1, strTemp);
	}

	m_wndListPointV.UpdateText();


	nDataCnt = m_pMissPoint->GetIMeasureCnt();
	if(nDataCnt >20)
	{
		nDataCnt = 0;
	}
	for( i=0; i<nDataCnt; i++ )
	{
		lvItem.iItem = i;
		strTemp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		m_wndListPointI.InsertItem(&lvItem);
		strTemp.Format("%.3f", m_pMissPoint->GetIData(i) );
		m_wndListPointI.SetItemText(i, 1, strTemp);
	}

	m_wndListPointI.UpdateText();


}

void CMiswiringPointSetDlg::SaveMeasureData()
{
	int i = 0;
	CString strTemp;

	m_wndListPointV.UpdateText();
	m_wndListPointI.UpdateText();

	int nDataCnt = m_wndListPointV.GetItemCount();

	if( nDataCnt > MIS_MAX_VOLTAGE_CNT )
	{
		nDataCnt = MIS_MAX_VOLTAGE_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointV.GetItemText(i, 1);
		m_pMissPoint->SetVData(i,atof(strTemp));
	}

	m_pMissPoint->SetVMeasureCnt( nDataCnt );

	nDataCnt = m_wndListPointI.GetItemCount();

	if( nDataCnt > MIS_MAX_CURRENT_CNT )
	{
		nDataCnt = MIS_MAX_CURRENT_CNT;
	}

	for( i=0; i<nDataCnt; i++ )
	{
		strTemp = m_wndListPointI.GetItemText(i, 1);
		m_pMissPoint->SetIData(i,atof(strTemp));
	}
	m_pMissPoint->SetIMeasureCnt( nDataCnt );

	AfxGetApp()->WriteProfileBinary(REG_MIS_SECTION, "MiswiringPoint", (LPBYTE)m_pMissPoint, sizeof(CMiswiringPoint));
}

BOOL CMiswiringPointSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	float fMaxBoundaryData;
	fMaxBoundaryData = 10000000.0f;		//Max 10000A;

	CString strVItem, strIItem;
	strVItem = "Voltage(mV)";//
	strIItem = "Current(mA)";//
	int nWidth = 95;

	m_wndListPointI.SetBoundaryData(fMaxBoundaryData);
	m_wndListPointV.SetBoundaryData(fMaxBoundaryData);
	
	m_wndListPointI.SetDigitMode(TRUE);
	m_wndListPointV.SetDigitMode(TRUE);

	m_wndListPointI.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
	m_wndListPointI.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);
	m_wndListPointV.InsertColumn(0, "No", LVCFMT_RIGHT, 35);
	m_wndListPointV.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);

	DWORD dwExStyle = m_wndListPointI.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
	m_wndListPointI.SetExtendedStyle( dwExStyle );
	m_wndListPointV.SetExtendedStyle( dwExStyle );

	CString strMsg;
	int i = 0;
	for( i =0; i<MIS_MAX_VOLTAGE_CNT; i++ )
	{
		strMsg.Format("%d", i+1);		
		m_wndListPointV.SetItemText(i, 0, strMsg);
	}

	for( i =0; i<MIS_MAX_CURRENT_CNT; i++ )
	{
		strMsg.Format("%d", i+1);
		m_wndListPointI.SetItemText(i, 0, strMsg);
	}


	LoadMeasureData();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMiswiringPointSetDlg::OnBnClickedOk()
{
	SaveMeasureData();
	OnOK();
}



LRESULT CMiswiringPointSetDlg::OnChangeListEdit(WPARAM wParam, LPARAM lParam)
{
	if(lParam == IDC_POINT_I)
	{
		int nDataCnt = m_wndListPointI.GetItemCount();

		if( nDataCnt > MIS_MAX_CURRENT_CNT )
		{
			for(int i= MIS_MAX_CURRENT_CNT; i< nDataCnt; i++)
			{
				m_wndListPointI.DeleteItem(i);
			}
		}
	}

	if(lParam == IDC_POINT_V)
	{
		int nDataCnt = m_wndListPointV.GetItemCount();

		if( nDataCnt > MIS_MAX_VOLTAGE_CNT )
		{
			for(int i= MIS_MAX_VOLTAGE_CNT; i< nDataCnt; i++)
			{
				m_wndListPointV.DeleteItem(i);
			}
		}

	}
	return 1;
}