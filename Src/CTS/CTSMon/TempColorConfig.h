#if !defined(AFX_TEMPCOLORCONFIG_H__88733931_837E_45A9_9755_DC642102C39C__INCLUDED_)
#define AFX_TEMPCOLORCONFIG_H__88733931_837E_45A9_9755_DC642102C39C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TempColorConfig.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTempColorConfig dialog
#include "CTSMonDoc.h"
#include "resource.h"
#include "TempColorConfig.h"

class CTempColorConfig : public CDialog
{
// Construction
public:
	CTempColorConfig(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CTempColorConfig)
	enum { IDD = IDD_TEMPCOLOR_CONFIG_DLG };
	CXPButton	m_BtnOk;
	CXPButton	m_BtnCancel;
	CXPButton	m_BtnInitail;
	float	m_fJigMaxLevel1;
	float	m_fJigMaxLevel2;
	float	m_fJigMaxLevel3;
	float	m_fJigMaxLevel4;
	float	m_fJigMaxLevel5;
	float	m_fJigMaxLevel6;
	float	m_fJigMaxLevel7;
	float	m_fJigMaxLevel8;
	float	m_fJigMinLevel1;
	float	m_fJigMinLevel2;
	float	m_fJigMinLevel3;
	float	m_fJigMinLevel4;
	float	m_fJigMinLevel5;
	float	m_fJigMinLevel6;
	float	m_fJigMinLevel7;
	float	m_fJigMinLevel8;
	float	m_fUnitMaxLevel1;
	float	m_fUnitMaxLevel2;
	float	m_fUnitMaxLevel3;
	float	m_fUnitMaxLevel4;
	float	m_fUnitMaxLevel5;
	float	m_fUnitMaxLevel6;
	float	m_fUnitMaxLevel7;
	float	m_fUnitMaxLevel8;
	float	m_fUnitMinLevel1;
	float	m_fUnitMinLevel2;
	float	m_fUnitMinLevel3;
	float	m_fUnitMinLevel4;
	float	m_fUnitMinLevel5;
	float	m_fUnitMinLevel6;
	float	m_fUnitMinLevel7;
	float	m_fUnitMinLevel8;	
	//}}AFX_DATA

public:	
	STR_TEMP_CONFIG m_TempConfig;
	stingray::foundation::SECWellButton m_Unit_Color8;
	stingray::foundation::SECWellButton m_Unit_Color7;
	stingray::foundation::SECWellButton m_Unit_Color6;
	stingray::foundation::SECWellButton m_Unit_Color5;
	stingray::foundation::SECWellButton m_Unit_Color4;
	stingray::foundation::SECWellButton m_Unit_Color3;
	stingray::foundation::SECWellButton m_Unit_Color2;
	stingray::foundation::SECWellButton m_Unit_Color1;
	stingray::foundation::SECWellButton m_Jig_Color8;
	stingray::foundation::SECWellButton m_Jig_Color7;
	stingray::foundation::SECWellButton m_Jig_Color6;
	stingray::foundation::SECWellButton m_Jig_Color5;
	stingray::foundation::SECWellButton m_Jig_Color4;
	stingray::foundation::SECWellButton m_Jig_Color3;
	stingray::foundation::SECWellButton m_Jig_Color2;
	stingray::foundation::SECWellButton m_Jig_Color1;	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTempColorConfig)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCTSMonDoc *m_pDoc;
	// Generated message map functions
	//{{AFX_MSG(CTempColorConfig)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnBtnInitial();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPCOLORCONFIG_H__88733931_837E_45A9_9755_DC642102C39C__INCLUDED_)
