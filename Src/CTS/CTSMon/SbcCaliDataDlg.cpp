// SettingCheckView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "SbcCaliDataDlg.h"


// CSettingCheckView


CSbcCaliDataDlg::CSbcCaliDataDlg(CWnd* pParent /*=NULL*/,CCTSMonDoc* pDoc)
	: CDialog(CSbcCaliDataDlg::IDD, pParent)
{
	m_nCurModuleID = 0;
	m_pDoc = pDoc;
	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();

}

CSbcCaliDataDlg::~CSbcCaliDataDlg()
{
}

void CSbcCaliDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);

	

	DDX_Control(pDX, IDC_COMBO_STAGE_CHANGE, m_comboStageID);
}

BEGIN_MESSAGE_MAP(CSbcCaliDataDlg, CDialog)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_BN_CLICKED(IDC_BTN_REFRESH, &CSbcCaliDataDlg::OnBnClickedBtnRefresh)
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO_STAGE_CHANGE, &CSbcCaliDataDlg::OnCbnSelchangeComboStageChange)
	ON_BN_CLICKED(IDC_BTN_SAVE_EXCEL, &CSbcCaliDataDlg::OnBnClickedBtnSaveExcel)
END_MESSAGE_MAP()



// CSettingCheckView 메시지 처리기입니다.

BOOL CSbcCaliDataDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	initCtrl();
	initValue();

	return TRUE;
}

void CSbcCaliDataDlg::initCtrl()
{
	initGrid();
	initLabel();
	initFont();
	initCombo();
}

void CSbcCaliDataDlg::initCombo()
{
	int nModuleID = 1;	

	for(int i=0; i<m_nMaxStageCnt; i++ )
	{
		nModuleID = EPGetModuleID(i);
	
		m_comboStageID.AddString(::GetModuleName(nModuleID));
		m_comboStageID.SetItemData(i, nModuleID);
	}
}
void CSbcCaliDataDlg::initFont()
{
	LOGFONT LogFont;

	m_comboStageID.GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 35;

	m_Font.CreateFontIndirect( &LogFont );

	m_comboStageID.SetFont(&m_Font);
}


void CSbcCaliDataDlg::initGrid()
{
	m_grid.SubclassDlgItem(IDC_GRID, this);
	m_grid.m_bSameRowSize = FALSE;
	m_grid.m_bSameColSize = FALSE;
	//---------------------------------------------------------------------//
	m_grid.m_bCustomWidth = TRUE;
	m_grid.m_bCustomColor = FALSE;

	m_grid.Initialize();

	m_grid.SetColCount(7);     
	m_grid.SetRowCount(MAX_CH_COUNT+2);        
	m_grid.SetDefaultRowHeight(20);

	m_grid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_grid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_grid.EnableGridToolTips();
	m_grid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_grid.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_grid.m_nWidth[1] = 50;
	m_grid.m_nWidth[2] = 150;
	m_grid.m_nWidth[3] = 150;
	m_grid.m_nWidth[4] = 150;
	m_grid.m_nWidth[5] = 150;
	m_grid.m_nWidth[6] = 150;	
	m_grid.m_nWidth[7] = 150;		

	m_grid.SetRowHeight(0,0,0);		

	//Row Header Setting
	m_grid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")).SetWrapText(FALSE).SetAutoSize(TRUE));

	m_grid.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetInterior(RGB(240, 240, 240)));

	m_grid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_grid.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(RGB(200, 200, 200)));
	m_grid.SetStyleRange(CGXRange().SetCols(4), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_grid.SetStyleRange(CGXRange().SetCols(5), CGXStyle().SetInterior(RGB(200, 200, 200)));
	m_grid.SetStyleRange(CGXRange().SetCols(6), CGXStyle().SetInterior(RGB(220, 220, 220)));
	m_grid.SetStyleRange(CGXRange().SetCols(7), CGXStyle().SetInterior(RGB(200, 200, 200)));

	m_grid.SetStyleRange(CGXRange().SetRows(1),	CGXStyle().SetInterior(RGB(240, 240, 240)));

	m_grid.SetValueRange(CGXRange( 1, 1), "Ch");
	m_grid.SetValueRange(CGXRange( 1, 2), "R1");
	m_grid.SetValueRange(CGXRange( 1, 4), "R2");
	m_grid.SetValueRange(CGXRange( 1, 6), "R3");

	m_grid.SetValueRange(CGXRange( 2, 2), "PV_AD_A(1)");
	m_grid.SetValueRange(CGXRange( 2, 3), "PV_AD_A(2)");
	m_grid.SetValueRange(CGXRange( 2, 4), "PV_AD_A(1)");
	m_grid.SetValueRange(CGXRange( 2, 5), "PV_AD_A(2)");
	m_grid.SetValueRange(CGXRange( 2, 6), "PV_AD_A(1)");
	m_grid.SetValueRange(CGXRange( 2, 7), "PV_AD_A(2)");


	m_grid.SetCoveredCellsRowCol(1,1, 2,1);
	m_grid.SetCoveredCellsRowCol(1,2, 1,3);
	m_grid.SetCoveredCellsRowCol(1,4, 1,5);
	m_grid.SetCoveredCellsRowCol(1,6, 1,7);

	//	m_wndDataList.SetScrollBarMode(SB_BOTH, gxnDisabled,TRUE);

	for(LONG i =0; i<MAX_CH_COUNT; i++)
	{
		m_grid.SetValueRange(CGXRange(i+3,1), i+1);
	}

	m_grid.Redraw();



}


void CSbcCaliDataDlg::initLabel()
{
	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE);

	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("SBC Calibration Data Check"); //"Monitoring"
}


void CSbcCaliDataDlg::initValue()
{
	for(LONG i =0; i < MAX_CH_COUNT; i++)
	{

		m_grid.SetStyleRange(CGXRange(i+3,2), CGXStyle().SetInterior(RGB(220, 220, 220)));
		m_grid.SetStyleRange(CGXRange(i+3,3), CGXStyle().SetInterior(RGB(200, 200, 200)));
		m_grid.SetStyleRange(CGXRange(i+3,4), CGXStyle().SetInterior(RGB(220, 220, 220)));
		m_grid.SetStyleRange(CGXRange(i+3,5), CGXStyle().SetInterior(RGB(200, 200, 200)));
		m_grid.SetStyleRange(CGXRange(i+3,6), CGXStyle().SetInterior(RGB(220, 220, 220)));
		m_grid.SetStyleRange(CGXRange(i+3,7), CGXStyle().SetInterior(RGB(200, 200, 200)));

		m_grid.SetValueRange(CGXRange(i+3,2), "-");
		m_grid.SetValueRange(CGXRange(i+3,3), "-");
		m_grid.SetValueRange(CGXRange(i+3,4), "-");
		m_grid.SetValueRange(CGXRange(i+3,5), "-");
		m_grid.SetValueRange(CGXRange(i+3,6), "-");
		m_grid.SetValueRange(CGXRange(i+3,7), "-");
	}
	m_grid.RedrawWindow();
}


void CSbcCaliDataDlg::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateAllData();
	}	
}

void CSbcCaliDataDlg::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		initValue();
	}
}

void CSbcCaliDataDlg::SetCurrentModule(int nModuleID)
{
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());
	m_comboStageID.SetCurSel(nModuleID-1);

	EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_CALI_DATA_REQUEST);
}

CString CSbcCaliDataDlg::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}


void CSbcCaliDataDlg::RecvData(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateAllData();
	}	
}
void CSbcCaliDataDlg::UpdateAllData()
{
	WORD state = EPGetGroupState(m_nCurModuleID);

	EP_MD_SBC_CALI_DATA *lpSbcCaliData;
	lpSbcCaliData = EPGetModuleSbcCaliData(EPGetModuleIndex(m_nCurModuleID));

	if( state == EP_STATE_LINE_OFF )
	{
		initValue();
		return;
	}

	CString strTemp;
	for(int i=0; i<MAX_CH_COUNT; i++)
	{
		////	1
		if(lpSbcCaliData->fVar1[i] == 0.0f)
		{
			m_grid.SetStyleRange(CGXRange(i+3,2), CGXStyle().SetInterior(RGB(240, 30, 40)));
		}
		else
		{
			m_grid.SetStyleRange(CGXRange(i+3,2), CGXStyle().SetInterior(RGB(220, 220, 220)));
		}
		strTemp.Format("%0.6f", lpSbcCaliData->fVar1[i]);
		m_grid.SetValueRange(CGXRange(i+3,2), strTemp);

		////	2
		if(lpSbcCaliData->fVar2[i] == 0.0f)
		{
			m_grid.SetStyleRange(CGXRange(i+3,3), CGXStyle().SetInterior(RGB(217, 18, 26)));
		}
		else
		{
			m_grid.SetStyleRange(CGXRange(i+3,3), CGXStyle().SetInterior(RGB(200, 200, 200)));
		}
		strTemp.Format("%0.6f", lpSbcCaliData->fVar2[i]);
		m_grid.SetValueRange(CGXRange(i+3,3), strTemp);


		////	3
		if(lpSbcCaliData->fVar3[i] == 0.0f)
		{
			m_grid.SetStyleRange(CGXRange(i+3,4), CGXStyle().SetInterior(RGB(240, 30, 40)));
		}
		else
		{
			m_grid.SetStyleRange(CGXRange(i+3,4), CGXStyle().SetInterior(RGB(220, 220, 220)));
		}
		strTemp.Format("%0.6f", lpSbcCaliData->fVar3[i]);
		m_grid.SetValueRange(CGXRange(i+3,4), strTemp);

		////	4
		if(lpSbcCaliData->fVar4[i] == 0.0f)
		{
			m_grid.SetStyleRange(CGXRange(i+3,5), CGXStyle().SetInterior(RGB(217, 18, 26)));
		}
		else
		{
			m_grid.SetStyleRange(CGXRange(i+3,5), CGXStyle().SetInterior(RGB(200, 200, 200)));
		}
		strTemp.Format("%0.6f", lpSbcCaliData->fVar4[i]);
		m_grid.SetValueRange(CGXRange(i+3,5), strTemp);

		////	5
		if(lpSbcCaliData->fVar5[i] == 0.0f)
		{
			m_grid.SetStyleRange(CGXRange(i+3,6), CGXStyle().SetInterior(RGB(240, 30, 40)));
		}
		else
		{
			m_grid.SetStyleRange(CGXRange(i+3,6), CGXStyle().SetInterior(RGB(220, 220, 220)));
		}
		strTemp.Format("%0.6f", lpSbcCaliData->fVar5[i]);
		m_grid.SetValueRange(CGXRange(i+3,6), strTemp);

		////	6
		if(lpSbcCaliData->fVar6[i] == 0.0f)
		{
			m_grid.SetStyleRange(CGXRange(i+3,7), CGXStyle().SetInterior(RGB(217, 18, 26)));
		}
		else
		{
			m_grid.SetStyleRange(CGXRange(i+3,7), CGXStyle().SetInterior(RGB(200, 200, 200)));
		}
		strTemp.Format("%0.6f", lpSbcCaliData->fVar6[i]);
		m_grid.SetValueRange(CGXRange(i+3,7), strTemp);
	}
	m_grid.RedrawWindow();

}

void CSbcCaliDataDlg::OnBnClickedBtnRefresh()
{
	WORD state = EPGetGroupState(m_nCurModuleID);
	initValue();		

	if( state == EP_STATE_LINE_OFF )
	{
		return;
	}

	int nRtn = 0;

	if((nRtn = EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_CALI_DATA_REQUEST)) != EP_ACK)
	{
		AfxMessageBox("명령 전송 실패!");//
	}

}


void CSbcCaliDataDlg::ClipboardChangeJustText()
{

	TCHAR *pszBuffer =NULL;
	CString strData;

	if(::OpenClipboard(this->GetSafeHwnd()) == TRUE)
	{	
		HANDLE hd ;

//		if(::IsClipboardFormatAvailable(CF_TEXT))
		{
			// 클립보드의 내용을 가져온다
			hd = ::GetClipboardData(CF_TEXT);
			LPCTSTR data = (LPCTSTR)::GlobalLock(hd);
			if(hd != NULL)
			{			
				int nLen = strlen(data) +1;
				// 메모리 재할당
				HGLOBAL hNewData = GlobalAlloc(GMEM_MOVEABLE, nLen );
				LPTSTR lptstrNewData = (LPTSTR)GlobalLock(hNewData);
				CopyMemory(lptstrNewData, data, nLen);
				GlobalUnlock(hNewData);

				::EmptyClipboard();
				SetClipboardData(CF_TEXT, lptstrNewData);
			}
			GlobalUnlock(hd);
		}

	
		::CloseClipboard();		
	}
}

void CSbcCaliDataDlg::OnEditCopy() 
{
	ROWCOL		nRow, nCol;
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();

	if(&m_grid == (CMyGridWnd *)pWnd)
	{
		m_grid.Copy();
	}
}




void CSbcCaliDataDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(::IsWindow(this->GetSafeHwnd()))
	{

		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		int width = rect.right/100;

		if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
		{
			CRect rectGrid;
			CRect rectStageLabel;
			GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
			ScreenToClient(&rectGrid);
			GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
			ScreenToClient(&rectStageLabel);
			GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
		}
	}
}

void CSbcCaliDataDlg::OnCbnSelchangeComboStageChange()
{

	initValue();
	int nIndex = m_comboStageID.GetCurSel();
	if(nIndex >= 0)
	{
		m_nCurModuleID = nIndex+1;
		m_CmdTarget.SetText(GetTargetModuleName());
		OnBnClickedBtnRefresh();
	}
}

void CSbcCaliDataDlg::initGridNum()
{

	for(LONG i =0; i < MAX_CH_COUNT; i++)
	{
		m_grid.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetInterior(RGB(240, 240, 240)));
		m_grid.SetStyleRange(CGXRange(i+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T("")));
	}
}

void CSbcCaliDataDlg::OnBnClickedBtnSaveExcel()
{
	WORD state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		initValue();
		AfxMessageBox("Save fail !!");
		return;
	}

	EP_MD_SBC_CALI_DATA *lpSbcCaliData;
	lpSbcCaliData = EPGetModuleSbcCaliData(EPGetModuleIndex(m_nCurModuleID));

	CStdioFile csvFile;			
	CFileException ex;
	CString strTemp;
	CString strName;
	CString strKey;
	CString strDefault;

	CFileDialog fileDlg(FALSE,NULL,NULL, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"csv File(*.csv) | *.csv; *.csv");

	if(fileDlg.DoModal() == IDOK)
	{ 
		strTemp=fileDlg.GetPathName();  
		CString strExt=fileDlg.GetFileExt();
		
		if(strExt.IsEmpty())
		{
			strTemp += ".csv";
		}
	}
	else
	{
		return;
	}



	if(csvFile.Open( (LPSTR)(LPCTSTR)strTemp, CFile::modeWrite | CFile::modeNoTruncate | CFile::modeCreate, &ex ))
	{
		COleDateTime oleTime = COleDateTime::GetCurrentTime();
		csvFile.WriteString(oleTime.Format("%Y/%m/%d %H:%M:%S"));
		csvFile.WriteString("\n");
		csvFile.WriteString("\n");
		csvFile.WriteString("SBC Calibration Data Check\n");


		////	R1
		csvFile.WriteString("\n");
		csvFile.WriteString("\n");
		csvFile.WriteString(" ,※R1※, ,※R2※, ,※R3※\n");
		csvFile.WriteString(" ,PV_AD_A(1),PV_AD_A(2),PV_AD_A(1),PV_AD_A(2),PV_AD_A(1),PV_AD_A(2)\n");
		for(LONG i =1; i <= MAX_CH_COUNT; i++)
		{
			strName.Format("Ch %d", i);
			strTemp.Format("%s,%0.6f,%0.6f,%0.6f,%0.6f,%0.6f,%0.6f\n",strName,
				lpSbcCaliData->fVar1[i-1],lpSbcCaliData->fVar2[i-1],
				lpSbcCaliData->fVar3[i-1],lpSbcCaliData->fVar4[i-1],
				lpSbcCaliData->fVar5[i-1],lpSbcCaliData->fVar6[i-1]);
			csvFile.WriteString(strTemp);
		}


		csvFile.Close();
		AfxMessageBox("Save succeed");
	}
	else
	{
		AfxMessageBox("Save fail !!");
		return ;
	}
}
