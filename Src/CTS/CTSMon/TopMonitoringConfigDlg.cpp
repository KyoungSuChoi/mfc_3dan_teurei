//==============================================================================
//  프로그램명   : TopMonitoringConfigDlg.cpp                                            
//  프로그램개요 : 
//  함수          :                                                          
//=============================================================================

#include "stdafx.h"
#include "CTSMon.h"
#include "TopMonitoringConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTopMonitoringConfigDlg dialog


CTopMonitoringConfigDlg::CTopMonitoringConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTopMonitoringConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTopMonitoringConfigDlg)
	m_ModuleInterval 	= 2;
	m_ChannelInterval 	= 2;
	m_DisplayText 		= FALSE;
	m_DisplayColor 		= FALSE;
	m_bLevelColor = FALSE;
	m_bOverColor = FALSE;
	m_fVRange = 0.0f;
	m_fIRange = 0.0f;
	m_fMaxLevel1 = 0.0f;
	m_fMaxLevel2 = 0.0f;
	m_fMaxLevel3 = 0.0f;
	m_fMinLevel1 = 0.0f;
	m_fMinLevel2 = 0.0f;
	m_fMinLevel3 = 0.0f;
	
	//}}AFX_DATA_INIT
	int i = 0;
	for(i =0; i<STATE_COLOR; i++)
		m_Flash[i] = FALSE;
		
	for(i =0; i<STATE_AUTO_COLOR; i++)
		m_AutoMode_Flash[i] = FALSE;

}


void CTopMonitoringConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTopMonitoringConfigDlg)
	DDX_Control	(pDX, IDC_TOPCFG_CHANNEL_SPIN, 		m_ChannelSpin);
	DDX_Control	(pDX, IDC_TOPCFG_MODULE_SPIN, 		m_ModuleSpin);
	DDX_Text	(pDX, IDC_TOPCFG_MODULE_TIMER, 		m_ModuleInterval);
	DDV_MinMaxUInt(pDX, m_ModuleInterval, 1, 100);
	DDX_Text	(pDX, IDC_TOPCFG_CHANNEL_TIMER, 	m_ChannelInterval);
	DDV_MinMaxUInt(pDX, m_ChannelInterval, 1, 100);
	DDX_Check	(pDX, IDC_TOPCFG_DISPLAY_TEXT, 		m_DisplayText);
	DDX_Check	(pDX, IDC_TOPCFG_DISPLAY_COLOR, 	m_DisplayColor);
	DDX_Check(pDX, IDC_V_LevelColor_CHECK, m_bLevelColor);
	DDX_Check	(pDX, IDC_VI_RANGE_CHECK, 	m_bOverColor);
	DDX_Text(pDX, IDC_V_RANGE, m_fVRange);
	DDX_Text(pDX, IDC_I_RANGE, m_fIRange);
	DDX_Text(pDX, IDC_VMAX_LEVEL1, m_fMaxLevel1);
	DDX_Text(pDX, IDC_VMAX_LEVEL2, m_fMaxLevel2);
	DDX_Text(pDX, IDC_VMAX_LEVEL3, m_fMaxLevel3);
	DDX_Text(pDX, IDC_VMIN_LEVEL1, m_fMinLevel1);
	DDX_Text(pDX, IDC_VMIN_LEVEL2, m_fMinLevel2);
	DDX_Text(pDX, IDC_VMIN_LEVEL3, m_fMinLevel3);	
	//}}AFX_DATA_MAP
	int i=0;
	for(i =0; i<STATE_COLOR; i++)
	{
		DDX_Check	(pDX, IDC_TOPCFG_INVERSE1+i, 	m_Flash[i]);
		DDX_Control	(pDX, IDC_TOPCFGE_RESULT1+i, 	m_Result[i]);
	}
		
	for(i =0; i<STATE_AUTO_COLOR; i++)
	{
		DDX_Check	(pDX, IDC_TOPCFG_AUTO_INVERSE1+i, 	m_AutoMode_Flash[i]);
		DDX_Control	(pDX, IDC_TOPCFGE_AUTO_RESULT1+i, 	m_AutoMode_Result[i]);
	}
}


BEGIN_MESSAGE_MAP(CTopMonitoringConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CTopMonitoringConfigDlg)
	ON_BN_CLICKED(IDC_TOPCFG_SETDEFAULT, 				OnTopcfgSetdefault)
	ON_NOTIFY	 (UDN_DELTAPOS, IDC_TOPCFG_MODULE_SPIN, OnDeltaposTopcfgModuleSpin)
	ON_NOTIFY	 (UDN_DELTAPOS, IDC_TOPCFG_CHANNEL_SPIN,OnDeltaposTopcfgChannelSpin)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE (IDC_TOPCFG_MODULE_TIMER, 				OnChangeTopcfgModuleTimer)
	ON_EN_CHANGE (IDC_TOPCFG_CHANNEL_TIMER, 			OnChangeTopcfgChannelTimer)
	//}}AFX_MSG_MAP
	ON_MESSAGE	 (CWN_COLOR_CHANGE, OnColorChange)
	ON_BN_CLICKED(IDOK, &CTopMonitoringConfigDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTopMonitoringConfigDlg message handlers

BOOL CTopMonitoringConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_ModuleSpin.SetRange(1,100);
	m_ChannelSpin.SetRange(1,100);
	
	int i = 0;

	for (i = 0; i < STATE_COLOR; i++)
	{
		m_TWellColor[i].AttachButton(IDC_TOPCFG_TCOLOR1+i, this);	
		m_BWellColor[i].AttachButton(IDC_TOPCFG_BCOLOR1+i, this);	
	}

	for (i = 0; i < STATE_AUTO_COLOR; i++)
	{
		m_AutoMode_TWellColor[i].AttachButton(IDC_TOPCFG_AUTO_TCOLOR1+i, this);	
		m_AutoMode_BWellColor[i].AttachButton(IDC_TOPCFG_AUTO_BCOLOR1+i, this);	
	}
		
	m_VOverColor.AttachButton(IDC_V_OVER_COLOR, this);
	m_IOverColor.AttachButton(IDC_I_OVER_COLOR, this);

	m_DefaultColor.AttachButton(IDC_VOLTAGE_COLOR_DEFAULT, this);
	m_LevelColor1.AttachButton(IDC_VOLTAGE_COLOR_LEVEL1, this);
	m_LevelColor2.AttachButton(IDC_VOLTAGE_COLOR_LEVEL2, this);
	m_LevelColor3.AttachButton(IDC_VOLTAGE_COLOR_LEVEL3, this);

	m_ModuleInterval=10;
	m_ChannelInterval=5;
	DrawColor();
	UpdateData(FALSE);
	return FALSE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// 1. Func.  : 
// 2. Desc   : 
//=============================================================================
void CTopMonitoringConfigDlg::OnOK() 
{
	UpdateData(TRUE);

//	if(m_bSetDefault == FALSE)
//	{
		for (int i = 0; i < STATE_COLOR; i++)
		{
			m_TopCfg.m_TStateColor[i] = m_TWellColor[i].GetColor();
			m_TopCfg.m_BStateColor[i] = m_BWellColor[i].GetColor();
			m_TopCfg.m_bStateFlash[i] = m_Flash[i];
		}
		
		for (int i = 0; i < STATE_AUTO_COLOR; i++)
		{
			m_TopCfg.m_TAutoStateColor[i] = m_AutoMode_TWellColor[i].GetColor();
			m_TopCfg.m_BAutoStateColor[i] = m_AutoMode_BWellColor[i].GetColor();
			m_TopCfg.m_bAutoStateFlash[i] = m_AutoMode_Flash[i];
		}

		m_TopCfg.m_ModuleInterval 	= m_ModuleInterval;
		m_TopCfg.m_ChannelInterval	= m_ChannelInterval;
		m_TopCfg.m_bShowText 		= m_DisplayText;
		m_TopCfg.m_bShowColor 		= m_DisplayColor;
		
		m_TopCfg.m_bOverColor = m_bOverColor;

		m_TopCfg.m_TOverColor[0] = 0;
		m_TopCfg.m_TOverColor[1] = 0;
		m_TopCfg.m_BOverColor[0] = m_VOverColor.GetColor();
		m_TopCfg.m_BOverColor[1] = m_IOverColor.GetColor();

		m_TopCfg.m_fRange[0] = m_fVRange;
		m_TopCfg.m_fRange[1] = m_fIRange;

		m_TopCfg.m_fMinLevel1 = m_fMinLevel1;
		m_TopCfg.m_fMinLevel2 = m_fMinLevel2;
		m_TopCfg.m_fMinLevel3 = m_fMinLevel3;
		
		m_TopCfg.m_fMaxLevel1 = m_fMaxLevel1;
		m_TopCfg.m_fMaxLevel2 = m_fMaxLevel2;
		m_TopCfg.m_fMaxLevel3 = m_fMaxLevel3;
				
		m_TopCfg.m_bLevelColor = m_bLevelColor;

		m_TopCfg.m_VDefaultColor = m_DefaultColor.GetColor();
		m_TopCfg.m_VLevelColor[0] = m_LevelColor1.GetColor();
		m_TopCfg.m_VLevelColor[1] = m_LevelColor2.GetColor();
		m_TopCfg.m_VLevelColor[2] = m_LevelColor3.GetColor();
//	}
	CDialog::OnOK();
}

void CTopMonitoringConfigDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CTopMonitoringConfigDlg::OnTopcfgSetdefault() 
{
	m_TopCfg = ::GetDefaultTopConfigSet();
	DrawColor();
}

void CTopMonitoringConfigDlg::OnDeltaposTopcfgModuleSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	UpdateData(TRUE);

	m_ModuleInterval = pNMUpDown->iPos + pNMUpDown->iDelta;
	if(m_ModuleInterval<1)		m_ModuleInterval = 1;
	if(m_ModuleInterval>100)	m_ModuleInterval = 100;
	UpdateData(FALSE);
	*pResult = 0;
}

void CTopMonitoringConfigDlg::OnDeltaposTopcfgChannelSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	UpdateData(TRUE);
	m_ChannelInterval = pNMUpDown->iPos + pNMUpDown->iDelta;

	if(m_ChannelInterval<1)		m_ChannelInterval = 1;
	if(m_ChannelInterval>100)	m_ChannelInterval = 100;
	UpdateData(FALSE);
	
	*pResult = 0;
}

LONG CTopMonitoringConfigDlg::OnColorChange(WPARAM wParam, LPARAM /*lParam*/)
{
	int 		index;
	COLORREF 	Color;

	// 1. Local Setting
	if (wParam >= IDC_TOPCFG_TCOLOR1 && wParam <= IDC_TOPCFG_TCOLOR15)
	{
		index = wParam - IDC_TOPCFG_TCOLOR1;
		Color = m_TWellColor[index].GetColor();
		m_Result[index].SetTextColor(Color);
	}

	if (wParam >= IDC_TOPCFG_BCOLOR1 && wParam <= IDC_TOPCFG_BCOLOR15)
	{
		index = wParam - IDC_TOPCFG_BCOLOR1;
		Color = m_BWellColor[index].GetColor();
		m_Result[index].SetBkColor(Color);
		m_Result[index].Invalidate();
	}
	
	// 2. Auto Setting
	if (wParam >= IDC_TOPCFG_AUTO_TCOLOR1 && wParam <= IDC_TOPCFG_AUTO_TCOLOR10)
	{
		index = wParam - IDC_TOPCFG_AUTO_TCOLOR1;
		Color = m_AutoMode_TWellColor[index].GetColor();
		m_AutoMode_Result[index].SetTextColor(Color);
	}

	if (wParam >= IDC_TOPCFG_AUTO_BCOLOR1 && wParam <= IDC_TOPCFG_AUTO_BCOLOR10)
	{
		index = wParam - IDC_TOPCFG_AUTO_BCOLOR1;
		Color = m_AutoMode_BWellColor[index].GetColor();
		m_AutoMode_Result[index].SetBkColor(Color);
		m_AutoMode_Result[index].Invalidate();
	}
	return 0;
}

HBRUSH CTopMonitoringConfigDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;
}

void CTopMonitoringConfigDlg::OnChangeTopcfgModuleTimer() 
{
	CString value;

	GetDlgItem(IDC_TOPCFG_MODULE_TIMER)->GetWindowText(value);

	m_ModuleInterval = (UINT) atoi(value);

	if (m_ModuleInterval >=1 && m_ModuleInterval<=100)
		m_ModuleSpin.SetPos(m_ModuleInterval);
}

void CTopMonitoringConfigDlg::OnChangeTopcfgChannelTimer() 
{
	CString value;

	GetDlgItem(IDC_TOPCFG_CHANNEL_TIMER)->GetWindowText(value);

	m_ChannelInterval = (UINT) atoi(value);

	if (m_ChannelInterval >=1 && m_ChannelInterval<=100)
		m_ChannelSpin.SetPos(m_ChannelInterval);
}

void CTopMonitoringConfigDlg::DrawColor()
{
	int i = 0;
	for ( i = 0; i < STATE_COLOR; i++)
	{
		m_TWellColor[i].SetColor(m_TopCfg.m_TStateColor[i]);
		m_BWellColor[i].SetColor(m_TopCfg.m_BStateColor[i]);
		m_Result[i].SetBkColor(m_TopCfg.m_BStateColor[i]);
		m_Result[i].SetTextColor(m_TopCfg.m_TStateColor[i]);
		m_Flash[i] = m_TopCfg.m_bStateFlash[i];
	}	
	
	for ( i = 0; i < STATE_AUTO_COLOR; i++)
	{
		m_AutoMode_TWellColor[i].SetColor(m_TopCfg.m_TAutoStateColor[i]);
		m_AutoMode_BWellColor[i].SetColor(m_TopCfg.m_BAutoStateColor[i]);
		m_AutoMode_Result[i].SetBkColor(m_TopCfg.m_BAutoStateColor[i]);
		m_AutoMode_Result[i].SetTextColor(m_TopCfg.m_TAutoStateColor[i]);
		m_AutoMode_Flash[i] = m_TopCfg.m_bStateFlash[i];
	}

	m_ModuleInterval 	= m_TopCfg.m_ModuleInterval;
	m_ChannelInterval 	= m_TopCfg.m_ChannelInterval;
	m_DisplayText 		= m_TopCfg.m_bShowText;
	m_DisplayColor 		= m_TopCfg.m_bShowColor;

	m_ModuleSpin.SetPos(m_ModuleInterval);
	m_ChannelSpin.SetPos(m_ChannelInterval);

	m_bOverColor = m_TopCfg.m_bOverColor;
	m_VOverColor.SetColor(m_TopCfg.m_BOverColor[0]);
	m_IOverColor.SetColor(m_TopCfg.m_BOverColor[1]);

	m_fVRange = m_TopCfg.m_fRange[0];
	m_fIRange = m_TopCfg.m_fRange[1];

	m_fMinLevel1 = m_TopCfg.m_fMinLevel1;
	m_fMinLevel2 = m_TopCfg.m_fMinLevel2;
	m_fMinLevel3 = m_TopCfg.m_fMinLevel3;
		
	m_fMaxLevel1 = m_TopCfg.m_fMaxLevel1;
	m_fMaxLevel2 = m_TopCfg.m_fMaxLevel2;
	m_fMaxLevel3 = m_TopCfg.m_fMaxLevel3;
	
	m_bLevelColor = m_TopCfg.m_bLevelColor;

	m_DefaultColor.SetColor(m_TopCfg.m_VDefaultColor);
	m_LevelColor1.SetColor(m_TopCfg.m_VLevelColor[0]);
	m_LevelColor2.SetColor(m_TopCfg.m_VLevelColor[1]);
	m_LevelColor3.SetColor(m_TopCfg.m_VLevelColor[2]);	

	UpdateData(FALSE);
}

void CTopMonitoringConfigDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}
