// ManualControlView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ManualControlView.h"
#include "MainFrm.h"
#include "StopperSettingDlg.h"

// CManualControlView

#define EDIT_FONT_SIZE		22

IMPLEMENT_DYNCREATE(CManualControlView, CFormView)

CManualControlView::CManualControlView()
: CFormView(CManualControlView::IDD)
, m_nAutoIntervalTime(0)	
{
	LanguageinitMonConfig(_T("CManualControlView"));
	m_nCurModuleID = 0;
	m_bReadySetCmd = false;

	ZeroMemory( &m_SendJigIOData, sizeof(IO_Data));
	m_SendJigIOData.ch_4 = 1;	
	ZeroMemory( &m_RecvJigIOData, sizeof(IO_Data));
	ZeroMemory( &m_SendFanIOData, sizeof(IO_Data));
	ZeroMemory( &m_RecvFanIOData, sizeof(IO_Data));

	m_nDisplayTimer = 0;

	m_pNormalSensorMapDlg = NULL;	
}

CManualControlView::~CManualControlView()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CManualControlView::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

void CManualControlView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
	DDX_Control(pDX, IDC_LABEL_WORKMODE, m_ctrlWorkMode);

	DDX_Control(pDX, IDC_RESTART_BTN, m_Btn_ReStart);
	DDX_Control(pDX, IDC_RESET_BTN, m_Btn_Reset);

	DDX_Control(pDX, IDC_LABEL28, m_LabelLog);

	// Stopper Setting START
	DDX_Control(pDX, IDC_BTN_STOPPER1, m_BtnStopper1);
	DDX_Control(pDX, IDC_BTN_STOPPER2, m_BtnStopper2);
	DDX_Control(pDX, IDC_BTN_STOPPER3, m_BtnStopper3);
	DDX_Control(pDX, IDC_BTN_STOPPER4, m_BtnStopper4);
	DDX_Control(pDX, IDC_BTN_STOPPER5, m_BtnStopper5);
	DDX_Control(pDX, IDC_BTN_STOPPER6, m_BtnStopper6);
	// Stopper Setting END

	// Output Sensor State START
	DDX_Control(pDX, IDC_LABEL12, m_Label12);
	DDX_Control(pDX, IDC_LABEL13, m_Label13);
	DDX_Control(pDX, IDC_LABEL14, m_Label14);
	DDX_Control(pDX, IDC_LABEL16, m_Label16);
	DDX_Control(pDX, IDC_LABEL17, m_Label17);
	DDX_Control(pDX, IDC_LABEL23, m_Label23);
	DDX_Control(pDX, IDC_LABEL24, m_Label24);
	DDX_Control(pDX, IDC_LABEL25, m_Label25);
	DDX_Control(pDX, IDC_LABEL27, m_Label27);
	DDX_Control(pDX, IDC_LABEL29, m_Label29);
	DDX_Control(pDX, IDC_LABEL30, m_Label30);

	DDX_Control(pDX, IDC_LABEL31, m_Label31);
	DDX_Control(pDX, IDC_LABEL33, m_Label33);	
	// Output Sensor State END

	DDX_Control(pDX, IDC_BUZZER_OFF_BTN, m_Btn_BuzzerOff);
	DDX_Control(pDX, IDC_EMERGENCY_STOP_BTN, m_Btn_EmergencyStop);
	DDX_Control(pDX, IDC_FMS_RESET1_BTN, m_Btn_FmsReset1);
	DDX_Control(pDX, IDC_FMS_RESET2_BTN, m_Btn_FmsReset2);
	DDX_Control(pDX, IDC_FMS_END_CMD_SEND_BTN, m_Btn_FmsEndCommandSend);

	DDX_Control(pDX, IDC_CONTINUE_BTN, m_Btn_Continue);
	DDX_Control(pDX, IDC_SAFETYLOCAL_MODE_BTN, m_Btn_SafetyLocalMode);
	DDX_Control(pDX, IDC_MAINTENANCE_MODE_BTN, m_Btn_MaintenanceMode);
	DDX_Control(pDX, IDC_LOCAL_MODE_BTN, m_Btn_LocalMode);
	DDX_Control(pDX, IDC_FMS_OUT_CMD_SEND_BTN, m_Btn_FmsOutCommandSend);

	// Input Sensor State START
	DDX_Control(pDX, IDC_LABEL_IO_001, m_IOLabelArray[IO_JC_UP]);
	DDX_Control(pDX, IDC_LABEL_IO_002, m_IOLabelArray[IO_JC_FRONT]);
	DDX_Control(pDX, IDC_LABEL_IO_003, m_IOLabelArray[IO_MAIN_SOL]);
	DDX_Control(pDX, IDC_LABEL_IO_004, m_IOLabelArray[IO_CLIP_OPEN]);
	DDX_Control(pDX, IDC_LABEL_IO_005, m_IOLabelArray[IO_STP_1]);
	DDX_Control(pDX, IDC_LABEL_IO_006, m_IOLabelArray[IO_STP_2]);
	DDX_Control(pDX, IDC_LABEL_IO_007, m_IOLabelArray[IO_STP_3]);
	DDX_Control(pDX, IDC_LABEL_IO_008, m_IOLabelArray[IO_STP_4]);
	DDX_Control(pDX, IDC_LABEL_IO_009, m_IOLabelArray[IO_JIG_FAN_ON_1]);
	DDX_Control(pDX, IDC_LABEL_IO_010, m_IOLabelArray[IO_JIG_FAN_ON_2]);
	DDX_Control(pDX, IDC_LABEL_IO_011, m_IOLabelArray[IO_JIG_FAN_ON_3]);
	DDX_Control(pDX, IDC_LABEL_IO_012, m_IOLabelArray[IO_JIG_FAN_ON_4]);
	DDX_Control(pDX, IDC_LABEL_IO_013, m_IOLabelArray[IO_EMG_LAMP]);
	DDX_Control(pDX, IDC_LABEL_IO_014, m_IOLabelArray[IO_CALI_TRAY_POWER]);

	DDX_Control(pDX, IDB_IO_ON_001, m_OnBtnArray[IO_JC_UP]);
	DDX_Control(pDX, IDB_IO_ON_002, m_OnBtnArray[IO_JC_FRONT]);
	DDX_Control(pDX, IDB_IO_ON_003, m_OnBtnArray[IO_MAIN_SOL]);
	DDX_Control(pDX, IDB_IO_ON_004, m_OnBtnArray[IO_CLIP_OPEN]);
	DDX_Control(pDX, IDB_IO_ON_005, m_OnBtnArray[IO_STP_1]);
	DDX_Control(pDX, IDB_IO_ON_006, m_OnBtnArray[IO_STP_2]);
	DDX_Control(pDX, IDB_IO_ON_007, m_OnBtnArray[IO_STP_3]);
	DDX_Control(pDX, IDB_IO_ON_008, m_OnBtnArray[IO_STP_4]);
	DDX_Control(pDX, IDB_IO_ON_009, m_OnBtnArray[IO_JIG_FAN_ON_1]);
	DDX_Control(pDX, IDB_IO_ON_010, m_OnBtnArray[IO_JIG_FAN_ON_2]);
	DDX_Control(pDX, IDB_IO_ON_011, m_OnBtnArray[IO_JIG_FAN_ON_3]);
	DDX_Control(pDX, IDB_IO_ON_012, m_OnBtnArray[IO_JIG_FAN_ON_4]);
	DDX_Control(pDX, IDB_IO_ON_013, m_OnBtnArray[IO_EMG_LAMP]);
	DDX_Control(pDX, IDB_IO_ON_014, m_OnBtnArray[IO_CALI_TRAY_POWER]);

	DDX_Control(pDX, IDB_IO_OFF_001, m_OffBtnArray[IO_JC_UP]);
	DDX_Control(pDX, IDB_IO_OFF_002, m_OffBtnArray[IO_JC_FRONT]);
	DDX_Control(pDX, IDB_IO_OFF_003, m_OffBtnArray[IO_MAIN_SOL]);
	DDX_Control(pDX, IDB_IO_OFF_004, m_OffBtnArray[IO_CLIP_OPEN]);
	DDX_Control(pDX, IDB_IO_OFF_005, m_OffBtnArray[IO_STP_1]);
	DDX_Control(pDX, IDB_IO_OFF_006, m_OffBtnArray[IO_STP_2]);
	DDX_Control(pDX, IDB_IO_OFF_007, m_OffBtnArray[IO_STP_3]);
	DDX_Control(pDX, IDB_IO_OFF_008, m_OffBtnArray[IO_STP_4]);
	DDX_Control(pDX, IDB_IO_OFF_009, m_OffBtnArray[IO_JIG_FAN_ON_1]);
	DDX_Control(pDX, IDB_IO_OFF_010, m_OffBtnArray[IO_JIG_FAN_ON_2]);
	DDX_Control(pDX, IDB_IO_OFF_011, m_OffBtnArray[IO_JIG_FAN_ON_3]);
	DDX_Control(pDX, IDB_IO_OFF_012, m_OffBtnArray[IO_JIG_FAN_ON_4]);
	DDX_Control(pDX, IDB_IO_OFF_013, m_OffBtnArray[IO_EMG_LAMP]);
	DDX_Control(pDX, IDB_IO_OFF_014, m_OffBtnArray[IO_CALI_TRAY_POWER]);
	// Input Sensor State END
}

BEGIN_MESSAGE_MAP(CManualControlView, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_LOCAL_MODE_BTN, &CManualControlView::OnBnClickedLocalModeBtn)
	ON_BN_CLICKED(IDC_MAINTENANCE_MODE_BTN, &CManualControlView::OnBnClickedMaintenanceModeBtn)
	ON_BN_CLICKED(IDC_RESTART_BTN, &CManualControlView::OnBnClickedRestartBtn)
	ON_BN_CLICKED(IDC_RESET_BTN, &CManualControlView::OnBnClickedResetBtn)
	ON_BN_CLICKED(IDC_BTN_STOPPER1, &CManualControlView::OnBnClickedBtnStopper1)
	ON_BN_CLICKED(IDC_BTN_STOPPER2, &CManualControlView::OnBnClickedBtnStopper2)
	ON_BN_CLICKED(IDC_BTN_STOPPER3, &CManualControlView::OnBnClickedBtnStopper3)
	ON_BN_CLICKED(IDC_BTN_STOPPER4, &CManualControlView::OnBnClickedBtnStopper4)
	ON_BN_CLICKED(IDC_BTN_STOPPER_SETTING, &CManualControlView::OnBnClickedBtnStopperSetting)
	ON_BN_CLICKED(IDC_BTN_NORMAL_SENSOR, &CManualControlView::OnBnClickedBtnNormalSensor)
	ON_BN_CLICKED(IDC_BUZZER_OFF_BTN, &CManualControlView::OnBnClickedBuzzerOffBtn)
	ON_BN_CLICKED(IDC_EMERGENCY_STOP_BTN, &CManualControlView::OnBnClickedEmergencyStopBtn)
	ON_BN_CLICKED(IDC_FMS_END_CMD_SEND_BTN, &CManualControlView::OnBnClickedFmsEndCmdSendBtn)
	ON_BN_CLICKED(IDC_FMS_RESET1_BTN, &CManualControlView::OnBnClickedFmsReset1Btn)
	ON_BN_CLICKED(IDC_CONTINUE_BTN, &CManualControlView::OnBnClickedContinueBtn)
	ON_BN_CLICKED(IDC_FMS_RESET2_BTN, &CManualControlView::OnBnClickedFmsReset2Btn)
	ON_BN_CLICKED(IDC_SAFETYLOCAL_MODE_BTN, &CManualControlView::OnBnClickedSafetylocalModeBtn)		
	ON_BN_CLICKED(IDB_IO_ON_001, &CManualControlView::OnBtnClickedOn001)
	ON_BN_CLICKED(IDB_IO_ON_002, &CManualControlView::OnBtnClickedOn002)
	ON_BN_CLICKED(IDB_IO_ON_003, &CManualControlView::OnBtnClickedOn003)
	ON_BN_CLICKED(IDB_IO_ON_004, &CManualControlView::OnBtnClickedOn004)
	ON_BN_CLICKED(IDB_IO_ON_005, &CManualControlView::OnBtnClickedOn005)
	ON_BN_CLICKED(IDB_IO_ON_006, &CManualControlView::OnBtnClickedOn006)
	ON_BN_CLICKED(IDB_IO_ON_007, &CManualControlView::OnBtnClickedOn007)
	ON_BN_CLICKED(IDB_IO_ON_008, &CManualControlView::OnBtnClickedOn008)
	ON_BN_CLICKED(IDB_IO_ON_009, &CManualControlView::OnBtnClickedOn009)
	ON_BN_CLICKED(IDB_IO_ON_010, &CManualControlView::OnBtnClickedOn010)
	ON_BN_CLICKED(IDB_IO_ON_011, &CManualControlView::OnBtnClickedOn011)
	ON_BN_CLICKED(IDB_IO_ON_012, &CManualControlView::OnBtnClickedOn012)
	ON_BN_CLICKED(IDB_IO_ON_013, &CManualControlView::OnBtnClickedOn013)
	ON_BN_CLICKED(IDB_IO_ON_014, &CManualControlView::OnBtnClickedOn014)
	ON_BN_CLICKED(IDB_IO_OFF_001, &CManualControlView::OnBtnClickedOff001)
	ON_BN_CLICKED(IDB_IO_OFF_002, &CManualControlView::OnBtnClickedOff002)
	ON_BN_CLICKED(IDB_IO_OFF_003, &CManualControlView::OnBtnClickedOff003)
	ON_BN_CLICKED(IDB_IO_OFF_004, &CManualControlView::OnBtnClickedOff004)
	ON_BN_CLICKED(IDB_IO_OFF_005, &CManualControlView::OnBtnClickedOff005)
	ON_BN_CLICKED(IDB_IO_OFF_006, &CManualControlView::OnBtnClickedOff006)
	ON_BN_CLICKED(IDB_IO_OFF_007, &CManualControlView::OnBtnClickedOff007)
	ON_BN_CLICKED(IDB_IO_OFF_008, &CManualControlView::OnBtnClickedOff008)
	ON_BN_CLICKED(IDB_IO_OFF_009, &CManualControlView::OnBtnClickedOff009)
	ON_BN_CLICKED(IDB_IO_OFF_010, &CManualControlView::OnBtnClickedOff010)
	ON_BN_CLICKED(IDB_IO_OFF_011, &CManualControlView::OnBtnClickedOff011)
	ON_BN_CLICKED(IDB_IO_OFF_012, &CManualControlView::OnBtnClickedOff012)
	ON_BN_CLICKED(IDB_IO_OFF_013, &CManualControlView::OnBtnClickedOff013)
	ON_BN_CLICKED(IDB_IO_OFF_014, &CManualControlView::OnBtnClickedOff014)		
	ON_BN_CLICKED(IDC_BTN_STOPPER5, &CManualControlView::OnBnClickedBtnStopper5)
	ON_BN_CLICKED(IDC_BTN_STOPPER6, &CManualControlView::OnBnClickedBtnStopper6)
END_MESSAGE_MAP()


// CManualControlView 진단입니다.

#ifdef _DEBUG
void CManualControlView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CManualControlView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CCTSMonDoc* CManualControlView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}

#endif //_DEBUG


// CManualControlView 메시지 처리기입니다.
void CManualControlView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	m_nLineMode = EP_OPERATION_LOCAL;

	InitLabel();
	InitColorBtn();
	InitFont();

	if(LoadIoConfig() == FALSE)
	{
		AfxMessageBox("Can't find configuration File.(ioconfig.cfg)");
		return;
	}

	CString strTemp = _T("");
	CString strTmpStoper = _T("");

	strTmpStoper = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper1", "370");
	if( strTmpStoper != _T("") )
	{
		strTemp.Format("#1 (%s)", strTmpStoper );
	}
	else
	{
		strTemp.Format("-");
	}
	GetDlgItem(IDC_BTN_STOPPER1)->SetWindowText(strTemp);

	strTmpStoper = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper2", "250");
	if( strTmpStoper != _T("") )
	{
		strTemp.Format("#2 (%s)", strTmpStoper );
	}
	else
	{
		strTemp.Format("-");
	}
	GetDlgItem(IDC_BTN_STOPPER2)->SetWindowText(strTemp);

	strTmpStoper = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper3", "200");
	if( strTmpStoper != _T("") )
	{
		strTemp.Format("#3 (%s)", strTmpStoper );
	}
	else
	{
		strTemp.Format("-");
	}
	GetDlgItem(IDC_BTN_STOPPER3)->SetWindowText(strTemp);

	strTmpStoper = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Stopper4", "180");
	if( strTmpStoper != _T("") )
	{
		strTemp.Format("#4 (%s)", strTmpStoper );
	}
	else
	{
		strTemp.Format("-");
	}
	GetDlgItem(IDC_BTN_STOPPER4)->SetWindowText(strTemp);

	int i=0;

	for( i =0; i<JIG_IO_COUNT; i++ )
	{
		// Read Addr Set
		CWnd* pWnd = GetDlgItem(IDC_READ_CHECK1+i);

		if(pWnd == NULL)
		{
			continue;
		}
		else
		{
			m_stateButton[i].SubclassWindow(pWnd->GetSafeHwnd());
			m_stateButton[i].SetImage( IDB_RED_BUTTON, 15);
			m_stateButton[i].Depress(FALSE);

			if(m_strInputName[i].Find(_T("SPARE")) > 0)
			{
				m_stateButton[i].ShowWindow(FALSE);
			}
			else
			{
				m_stateButton[i].SetWindowText(m_strInputName[i]);
			}
		}		
	}

	CCTSMonDoc *pDoc =  GetDocument();
}

void CManualControlView::InitLabel()
{
	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText(TEXT_LANG[0]); //"Maintenance"

	m_ctrlWorkMode.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE);

	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE);

	m_LabelLog.SetFontSize(14)
		.SetTextColor(RGB_YELLOW)
		.SetBkColor(RGB_BLACK)
		.SetFontBold(TRUE)
		.SetText("-");

	// 센서 부분 START
	m_Label13.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[4]);//"센서상태 왼쪽"

	m_Label14.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[5]);//"센서상태 오른쪽"

	m_Label16.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[9]);//"메인 지그 동작 상태"

	m_Label12.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[7]);//"2단 클립 동작 상태"

	m_Label17.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[10]);//"1단 클립 동작 상태"

	m_Label23.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[16]);//"연기 감지 센서 상태"

	m_Label24.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[17]);//"2단 트레이 높이 감지 상태"

	m_Label25.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[18]);//"1단 트레이 높이 감지 상태"

	m_Label27.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[21]);//"Contact Deepth Stopper 상태"	

	m_Label29.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[69]);//"Contact Deepth Stopper 상태"	

	m_Label30.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE)
		.SetText(TEXT_LANG[70]);//"Contact Deepth Stopper 상태"	

	m_Label31.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE);

	m_Label33.SetFontSize(14)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND)
		.SetFontBold(TRUE);
	// 센서 부분 END

	// KSCHOI
	for(int i = 0; i < 14; i++)
	{
		m_IOLabelArray[i].SetFontSize(14)
			.SetTextColor(RGB_LABEL_FONT_STAGENAME)
			.SetBkColor(RGB_LABEL_BACKGROUND)
			.SetFontBold(TRUE);
	}
	// KSCHOI
}

void CManualControlView::InitColorBtn()
{	
	m_Btn_LocalMode.SetFontStyle(24,1);
	m_Btn_LocalMode.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_MaintenanceMode.SetFontStyle(24,1);
	m_Btn_MaintenanceMode.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_AutoUpdateStop.SetFontStyle(24,1);

	m_Btn_SafetyLocalMode.SetFontStyle(24,1);
	m_Btn_SafetyLocalMode.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_AutoUpdateStop.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_Btn_ReStart.SetFontStyle(24,1);
	m_Btn_ReStart.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_Btn_Reset.SetFontStyle(24,1);
	m_Btn_Reset.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_BuzzerOff.SetFontStyle(24,1);
	m_Btn_BuzzerOff.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_EmergencyStop.SetFontStyle(24,1);
	m_Btn_EmergencyStop.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_FmsReset1.SetFontStyle(24,1);
	m_Btn_FmsReset1.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_FmsReset2.SetFontStyle(24,1);
	m_Btn_FmsReset2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_FmsEndCommandSend.SetFontStyle(24,1);
	m_Btn_FmsEndCommandSend.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);

	m_Btn_FmsOutCommandSend.SetFontStyle(24,1);
	m_Btn_FmsOutCommandSend.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	m_Btn_Continue.SetFontStyle(24,1);
	m_Btn_Continue.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnStopper1.SetFontStyle(24, 1);
	m_BtnStopper1.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper2.SetFontStyle(24, 1);
	m_BtnStopper2.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnStopper3.SetFontStyle(24, 1);
	m_BtnStopper3.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper4.SetFontStyle(24, 1);
	m_BtnStopper4.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper5.SetFontStyle(24, 1);
	m_BtnStopper5.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper6.SetFontStyle(24, 1);
	m_BtnStopper6.SetColor(RGB_BLACK, RGB_GRAY);
	// KSCHOI
	for(int i = 0; i < 14; i++)
	{
		m_OnBtnArray[i].SetFontStyle(24, 1);
		m_OnBtnArray[i].SetColor(RGB_BLACK, RGB_GRAY);

		m_OffBtnArray[i].SetFontStyle(24, 1);
		m_OffBtnArray[i].SetColor(RGB_BLACK, RGB_GRAY);

	}
	// KSCHOI
}

void CManualControlView::InitFont()
{	
	LOGFONT			LogFont;

	GetDlgItem(IDC_STATIC_MANUAL_CONTROL)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	m_font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC_MANUAL_CONTROL)->SetFont(&m_font);
	GetDlgItem(IDC_STATIC_SENSOR_STATUS)->SetFont(&m_font);	
	GetDlgItem(IDC_STATIC1)->SetFont(&m_font);	
}

bool CManualControlView::LoadIoConfig()
{
	CStdioFile	file;

	CFileException* e;
	e = new CFileException;

	CString strFileName;
	if(g_nLanguage == LANGUAGE_CHI)
	{
		strFileName = "ioconfig_ch.cfg";
	}
	else
	{
		strFileName = "ioconfig.cfg";
	}


	if(!file.Open(strFileName, CFile::modeRead|CFile::typeBinary ))
	{
		return FALSE;
	}

	WCHAR *buffer;
	buffer = new WCHAR[file.GetLength()];

	WCHAR buf[1];

	int i = 0;
	int nRength = 0;

	CString strData;
	CString strTemp;

	file.Read(buf, sizeof(WCHAR));

	TRY
	{
		while(TRUE)
		{

			file.Read(buf, sizeof(WCHAR));

			if( buf[0] == 0x0d )
			{
				buffer[i] = 0x00;

				int nLen = WideCharToMultiByte(CP_ACP, 0, buffer, -1, NULL, 0, NULL, NULL);			

				char* pMultibyte  = new char[nLen];
				memset(pMultibyte, 0x00, (nLen)*sizeof(char));

				WideCharToMultiByte(CP_ACP, 0, buffer, -1, pMultibyte, nLen, NULL, NULL);

				ZeroMemory( buffer, sizeof(WCHAR) * file.GetLength());			

				strData = pMultibyte;

				delete pMultibyte;

				i = 0;

				if(!strData.IsEmpty())
				{
					ParsingConfig(strData);

				}
			}
			else
			{
				buffer[i] = buf[0];		

				i++;
				nRength++;
			}

			if( nRength > file.GetLength()-2 )
			{
				break;
			}
		}

	}
	CATCH (CFileException, e)
	{
		AfxMessageBox(e->m_cause);
		file.Close();
		return FALSE;
	}
	END_CATCH
		file.Close();	
	return TRUE;
}

void CManualControlView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);

		StartMonitoring();
	}
}

void CManualControlView::ModuleDisConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{	
		UpdateGroupState(nModuleID);

		StopMonitoring();
	}
}

CString CManualControlView::GetTargetModuleName()
{
	CString temp;
	CString strMDName = GetModuleName(m_nCurModuleID);	
	temp.Format("Stage No. %s", strMDName);	
	return temp;
}

void CManualControlView::InitValue()
{
	m_LabelLog.SetText("-");	

	ClearIoState();
}

void CManualControlView::SetCurrentModule(int nModuleID, int nGroupIndex)
{	
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());

	InitValue();

	UpdateGroupState(m_nCurModuleID);

	UpdateData(false);
}


void CManualControlView::StartMonitoring()
{	
	// TEST
//	m_nDisplayTimer = SetTimer(102, 2000, NULL);		
	// TEST
	if( EPGetGroupState(m_nCurModuleID) != EP_STATE_LINE_OFF )
	{
		if( m_nDisplayTimer == 0 )
		{
			// 1. default 2s
			// 2. SBC에서 받은 설정된 값으로 화면 표시 및 적용
			EP_GP_DATA gpData = EPGetGroupData(m_nCurModuleID, 0);

			memcpy( &m_SendJigIOData, &gpData.gpState.ManualFunc1,  sizeof(IO_Data));
			memcpy( &m_SendFanIOData, &gpData.gpState.ManualFunc2,  sizeof(IO_Data));

			UpdateIoState( true );		

			m_nDisplayTimer = SetTimer(102, 2000, NULL);		
		}
	}
}

void CManualControlView::StopMonitoring()
{
	if( m_nDisplayTimer  != 0)
	{
		KillTimer(102);
		m_nDisplayTimer = 0;		
	}

	if( m_pNormalSensorMapDlg != NULL )
	{
		m_pNormalSensorMapDlg->DestroyWindow();
		delete m_pNormalSensorMapDlg;
		m_pNormalSensorMapDlg = NULL;
	}
}

void CManualControlView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	//// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	RECT rect;
	::GetClientRect(m_hWnd, &rect);

	if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
	{
		CRect rectGrid;
		CRect rectStageLabel;
		GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(rectGrid);
		ScreenToClient(&rectGrid);
		GetDlgItem(IDC_STAGE_NAME)->GetWindowRect(rectStageLabel);
		ScreenToClient(&rectStageLabel);
		GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rectGrid.left, rectGrid.top, rect.right-rectStageLabel.Width(), rectGrid.Height(), FALSE);
	}
}

int CManualControlView::Hex2Dec(CString hexData)
{
	int data = 0;

	int val = 0, val1=0;
	int count = 0;
	hexData.MakeLower();

	//remove pre and post space
	int i = 0;
	int nSize = hexData.GetLength();
	for(i = 0; i < nSize; i++)		//이전 공백 삭제 
	{
		if(hexData[0] == ' ')
		{
			hexData.Delete(0);
		}
		else
		{
			break;
		}
	}

	nSize = hexData.GetLength();
	for(i = nSize-1; i >= 0 ; i--)		//이후  공백 삭제 
	{
		if(hexData[i] == ' ')
		{
			hexData.Delete(i);
		}
		else
		{
			break;
		}
	}

	nSize = hexData.GetLength();
	for(i=nSize-1; i>=0; i--)
	{
		val = 0;
		if( hexData[i] >= '0' && hexData[i] <= '9')	//'0'~'1'
		{
			val = (hexData[i] - '0') << (4*count++);
		}
		else
		{
			val1 = hexData[i] - 'a';
			if(val1 >= 0 && val1 <= 5)	//'a'~'f', 'A'~'F'
			{
				val +=((10 + val1)<<(4*count++));
			}
			else	//	
			{
				return 0;
			}
		}
		data += val; 
	}
	return data;	
}


void CManualControlView::ParsingConfig(CString strConfig)
{
	if(strConfig.IsEmpty())		return;

	int a, b;
	a = strConfig.Find('[');
	b = strConfig.ReverseFind(']');

	if(b > 0 && a >= b)	return;

	CString strTitle, strData;
	strTitle = strConfig.Mid(a+1, b-a-1);

	a = strConfig.ReverseFind('=');

	strData = strConfig.Mid( a+1 );

	strData.TrimLeft(" ");
	strData.TrimRight(" ");

	if(strData.IsEmpty())	return;

	if(strTitle == "Input port 1")		{	if(Hex2Dec(strData) > 0)	m_nInputAddress[0] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 2")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[1] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 3")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[2] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 4")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[3] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 5")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[4] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 6")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[5] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 7")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[6] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 8")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[7] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 9")	{	if(Hex2Dec(strData) > 0)	m_nInputAddress[8] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 10"){	if(Hex2Dec(strData) > 0)	m_nInputAddress[9] = Hex2Dec(strData);	}
	else if(strTitle == "Input port 11"){	if(Hex2Dec(strData) > 0)	m_nInputAddress[10] = Hex2Dec(strData);	}
	
	// ioconfig.cfg의 데이터를 받아 배열로 저장시킨다. 
	else if(strTitle == "Input1_1 Name"){	m_strInputName[0] =  strData;	}
	else if(strTitle == "Input1_2 Name"){	m_strInputName[1] =  strData;	}
	else if(strTitle == "Input1_3 Name"){	m_strInputName[2] =  strData;	}
	else if(strTitle == "Input1_4 Name"){	m_strInputName[3] =  strData;	}
	else if(strTitle == "Input1_5 Name"){	m_strInputName[4] =  strData;	}
	else if(strTitle == "Input1_6 Name"){	m_strInputName[5] =  strData;	}
	else if(strTitle == "Input1_7 Name"){	m_strInputName[6] =  strData;	}
	else if(strTitle == "Input1_8 Name"){	m_strInputName[7] =  strData;	}
	else if(strTitle == "Input2_1 Name"){	m_strInputName[8] =  strData;	}
	else if(strTitle == "Input2_2 Name"){	m_strInputName[9] =  strData;	}
	else if(strTitle == "Input2_3 Name"){	m_strInputName[10] =  strData;	}
	else if(strTitle == "Input2_4 Name"){	m_strInputName[11] =  strData;	}
	else if(strTitle == "Input2_5 Name"){	m_strInputName[12] =  strData;	}
	else if(strTitle == "Input2_6 Name"){	m_strInputName[13] =  strData;	}
	else if(strTitle == "Input2_7 Name"){	m_strInputName[14] =  strData;	}
	else if(strTitle == "Input2_8 Name"){	m_strInputName[15] =  strData;	}
	else if(strTitle == "Input3_1 Name"){	m_strInputName[16] =  strData;	}
	else if(strTitle == "Input3_2 Name"){	m_strInputName[17] =  strData;	}
	else if(strTitle == "Input3_3 Name"){	m_strInputName[18] =  strData;	}
	else if(strTitle == "Input3_4 Name"){	m_strInputName[19] =  strData;	}
	else if(strTitle == "Input3_5 Name"){	m_strInputName[20] =  strData;	}
	else if(strTitle == "Input3_6 Name"){	m_strInputName[21] =  strData;	}
	else if(strTitle == "Input3_7 Name"){	m_strInputName[22] =  strData;	}
	else if(strTitle == "Input3_8 Name"){	m_strInputName[23] =  strData;	}
	else if(strTitle == "Input4_1 Name"){	m_strInputName[24] =  strData;	}
	else if(strTitle == "Input4_2 Name"){	m_strInputName[25] =  strData;	}
	else if(strTitle == "Input4_3 Name"){	m_strInputName[26] =  strData;	}
	else if(strTitle == "Input4_4 Name"){	m_strInputName[27] =  strData;	}
	else if(strTitle == "Input4_5 Name"){	m_strInputName[28] =  strData;	}
	else if(strTitle == "Input4_6 Name"){	m_strInputName[29] =  strData;	}
	else if(strTitle == "Input4_7 Name"){	m_strInputName[30] =  strData;	}
	else if(strTitle == "Input4_8 Name"){	m_strInputName[31] =  strData;	}
	else if(strTitle == "Input5_1 Name"){	m_strInputName[32] =  strData;	}
	else if(strTitle == "Input5_2 Name"){	m_strInputName[33] =  strData;	}
	else if(strTitle == "Input5_3 Name"){	m_strInputName[34] =  strData;	}
	else if(strTitle == "Input5_4 Name"){	m_strInputName[35] =  strData;	}
	else if(strTitle == "Input5_5 Name"){	m_strInputName[36] =  strData;	}
	else if(strTitle == "Input5_6 Name"){	m_strInputName[37] =  strData;	}
	else if(strTitle == "Input5_7 Name"){	m_strInputName[38] =  strData;	}
	else if(strTitle == "Input5_8 Name"){	m_strInputName[39] =  strData;	}
	else if(strTitle == "Input6_1 Name"){	m_strInputName[40] =  strData;	}
	else if(strTitle == "Input6_2 Name"){	m_strInputName[41] =  strData;	}
	else if(strTitle == "Input6_3 Name"){	m_strInputName[42] =  strData;	}
	else if(strTitle == "Input6_4 Name"){	m_strInputName[43] =  strData;	}
	else if(strTitle == "Input6_5 Name"){	m_strInputName[44] =  strData;	}
	else if(strTitle == "Input6_6 Name"){	m_strInputName[45] =  strData;	}
	else if(strTitle == "Input6_7 Name"){	m_strInputName[46] =  strData;	}
	else if(strTitle == "Input6_8 Name"){	m_strInputName[47] =  strData;	}
	else if(strTitle == "Input7_1 Name"){	m_strInputName[48] =  strData;	}
	else if(strTitle == "Input7_2 Name"){	m_strInputName[49] =  strData;	}
	else if(strTitle == "Input7_3 Name"){	m_strInputName[50] =  strData;	}
	else if(strTitle == "Input7_4 Name"){	m_strInputName[51] =  strData;	}
	else if(strTitle == "Input7_5 Name"){	m_strInputName[52] =  strData;	}
	else if(strTitle == "Input7_6 Name"){	m_strInputName[53] =  strData;	}
	else if(strTitle == "Input7_7 Name"){	m_strInputName[54] =  strData;	}
	else if(strTitle == "Input7_8 Name"){	m_strInputName[55] =  strData;	}
	else if(strTitle == "Input8_1 Name"){	m_strInputName[56] =  strData;	}
	else if(strTitle == "Input8_2 Name"){	m_strInputName[57] =  strData;	}
	else if(strTitle == "Input8_3 Name"){	m_strInputName[58] =  strData;	}
	else if(strTitle == "Input8_4 Name"){	m_strInputName[59] =  strData;	}
	else if(strTitle == "Input8_5 Name"){	m_strInputName[60] =  strData;	}
	else if(strTitle == "Input8_6 Name"){	m_strInputName[61] =  strData;	}
	else if(strTitle == "Input8_7 Name"){	m_strInputName[62] =  strData;	}
	else if(strTitle == "Input8_8 Name"){	m_strInputName[63] =  strData;	}
	else if(strTitle == "Input9_1 Name"){	m_strInputName[64] =  strData;	}
	else if(strTitle == "Input9_2 Name"){	m_strInputName[65] =  strData;	}
	else if(strTitle == "Input9_3 Name"){	m_strInputName[66] =  strData;	}
	else if(strTitle == "Input9_4 Name"){	m_strInputName[67] =  strData;	}
	else if(strTitle == "Input9_5 Name"){	m_strInputName[68] =  strData;	}
	else if(strTitle == "Input9_6 Name"){	m_strInputName[69] =  strData;	}
	else if(strTitle == "Input9_7 Name"){	m_strInputName[70] =  strData;	}
	else if(strTitle == "Input9_8 Name"){	m_strInputName[71] =  strData;	}

}

void CManualControlView::ClearIoState()
{
	m_BtnStopper1.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper2.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnStopper3.SetColor(RGB_BLACK, RGB_GRAY);	
	m_BtnStopper4.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper5.SetColor(RGB_BLACK, RGB_GRAY);
	m_BtnStopper6.SetColor(RGB_BLACK, RGB_GRAY);

	for(int i =0; i<JIG_IO_COUNT; i++ )
	{
		// Read Addr Set
		m_stateButton[i].Depress(FALSE);
	}
}

void CManualControlView::UpdateIoState( bool bForceUpdate )
{	
	if( m_nCurModuleID == 0 )
	{
		return;
	}

	EP_GP_DATA gpData = EPGetGroupData(m_nCurModuleID, 0);

	if( bForceUpdate == false )
	{
		if( m_nRecvJigIOData != gpData.gpState.ManualFunc1 || m_nRecvFanIOData != gpData.gpState.ManualFunc2)
		{
			ClearIoState();
			m_nRecvJigIOData = gpData.gpState.ManualFunc1;
			m_nRecvFanIOData = gpData.gpState.ManualFunc2;
			memcpy( &m_RecvJigIOData, &gpData.gpState.ManualFunc1, sizeof(IO_Data) );	
			memcpy( &m_RecvFanIOData, &gpData.gpState.ManualFunc2, sizeof(IO_Data) );
		}
	}
	else
	{
		ClearIoState();
		m_nRecvJigIOData = gpData.gpState.ManualFunc1;
		m_nRecvFanIOData = gpData.gpState.ManualFunc2;
		memcpy( &m_RecvJigIOData, &gpData.gpState.ManualFunc1, sizeof(IO_Data) );
		memcpy( &m_RecvFanIOData, &gpData.gpState.ManualFunc2, sizeof(IO_Data) );
	}

	// OUTPUT BUTTON HIGHLIGHT START
	for(int i = 0; i < sizeof(m_RecvJigIOData)*8; i++)
	{
		if(i > IO_CALI_TRAY_POWER) continue;

		if((gpData.gpState.ManualFunc1 & (0x01<<i)) > 0)
		{
			m_OnBtnArray[i].SetColor(RGB_BLACK, RGB_LIGHTGREEN);	// ON
			m_OffBtnArray[i].SetColor(RGB_BLACK, RGB_GRAY);			// OFF
		}
		else
		{
			m_OnBtnArray[i].SetColor(RGB_BLACK, RGB_GRAY);			// OFF
			m_OffBtnArray[i].SetColor(RGB_BLACK, RGB_LIGHTGREEN);	// ON
		}
	}

	// INPUT BUTTON START
	int i=0, j=0;
	int nIndex = 0;

	CString strTemp;
	CString strTrace;

	for(i = JIG_IO_BYTE_START_INDEX; i <= JIG_IO_BYTE_END_INDEX; i++ ) // Byte
	{
		for(j =0; j <  8; j++) // Bit
		{
			nIndex = ((i - JIG_IO_BYTE_START_INDEX) * 8) + j;

			if(nIndex >= JIG_IO_COUNT)
				continue;

			m_stateButton[nIndex].Depress(gpData.gpState.IOSensorState[i] & (0x01<<j));
		}

		CString sss;
		sss.Format("%d ", gpData.gpState.IOSensorState[i]);

		strTrace += sss;
	}

	// TEST
// 	static int stIValue = 1;
// 	char pData[36] = {0,};
// 	for(int i = 0; i < 36; i++)
// 		pData[i] = stIValue; 
// 
// 	memcpy(gpData.gpState.IOSensorState, pData, sizeof(char)*36);
// 	stIValue *= 2;
// 
// 	if(stIValue >= 255)
// 		stIValue = 1;
// 
//	char pData[36] = {0};
// 	pData[24] = 255;
// 	pData[25] = 255;
// 	pData[26] = 255;
// 	pData[27] = 255;
// 	pData[28] = 255;
// 	pData[29] = 255;
// 	pData[30] = 255;
//	pData[31] = 1;

//	memcpy(gpData.gpState.IOSensorState, pData, sizeof(char)*36);
// 	if(m_RecvFanIOData.ch_0 == 1)
// 		m_RecvFanIOData.ch_0 = 0;
// 	else
// 		m_RecvFanIOData.ch_0 = 1;

	// TEST

	if(m_pNormalSensorMapDlg != NULL)
	{
		m_pNormalSensorMapDlg->UpdateAdditionalSensorState(gpData.gpState.IOSensorState);
	}

	// INPUT BUTTON END

	strTrace += "CManualControlView";
	TRACE(strTrace);

	CCTSMonDoc *pDoc = GetDocument();
	pDoc->WriteLog((LPSTR)(LPCTSTR)strTrace);

}

void CManualControlView::WriteData()
{
	UpdateData(TRUE);	

	int nRtn = 0;
	EP_CMD_IO_INFO IoInfo;
	ZeroMemory( &IoInfo, sizeof(EP_CMD_IO_INFO));

	memcpy( &IoInfo.manualFunc[0], &m_SendJigIOData, sizeof(IO_Data) );
	memcpy( &IoInfo.manualFunc[1], &m_SendFanIOData, sizeof(IO_Data) );


	if((nRtn = EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_IO_SET_FUNC, &IoInfo, sizeof(EP_CMD_IO_INFO))) != EP_ACK)
	{
		AfxMessageBox(TEXT_LANG[27]);//"[WriteData] 명령 전송 실패!"
	}
}

bool CManualControlView::UpdateGroupState(int nModuleID, int /*nGroupIndex*/)
{
	if( nModuleID != m_nCurModuleID )
	{	
		// 1. 현재 실행중인 모듈이 아니기에 상태값을 변경하지 않는다.
		return false;
	}

	if( EPGetGroupState(nModuleID) == EP_STATE_LINE_OFF )
	{
		m_ctrlWorkMode.SetText(TEXT_LANG[28]);//"Line Off"
	}
	else
	{
		CCTSMonDoc *pDoc =  GetDocument();
		CFormModule *pModule = pDoc->GetModuleInfo(nModuleID);
		if(pModule != NULL)
		{
			m_nLineMode = pModule->GetOperationMode();

			switch( m_nLineMode )
			{
			case EP_OPERATION_LOCAL:
				{				
					ZeroMemory( &m_SendJigIOData, sizeof(IO_Data));
					ZeroMemory( &m_SendFanIOData, sizeof(IO_Data));
					m_SendJigIOData.ch_4 = 1;
					m_ctrlWorkMode.SetText("Local Mode");   
				}
				break;
			case EP_OPEARTION_MAINTENANCE:
				{
					m_ctrlWorkMode.SetText(TEXT_LANG[29]);//"Maintenance Mode" 
				}
				break;
			case EP_OPERATION_AUTO:
				{
					m_ctrlWorkMode.SetText(TEXT_LANG[30]);//"Automatic Mode"
				}
				break;
			}
		}
		else
		{
			m_ctrlWorkMode.SetText(TEXT_LANG[28]); //"Line off"
			return false;
		}	
	}	

	return true;
}

void CManualControlView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UpdateIoState();

	if( m_bReadySetCmd )
	{
		m_bReadySetCmd = false;
		WriteData();
	}	

	CFormView::OnTimer(nIDEvent);
}

void CManualControlView::OnBnClickedLocalModeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	CString strLog = _T("");

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strLog.Format(TEXT_LANG[31]); //"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strLog, INFO_TYPE_FAULT);
		return;		
	}

	if( m_nLineMode == EP_OPERATION_AUTO )
	{
		strLog.Format(TEXT_LANG[32]);//"명령 전송을 실패 하였습니다.\n[ Automatic Mode 에서는 FMS에서 조작 가능합니다. ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strLog, INFO_TYPE_FAULT);
		// AfxMessageBox("[Automatic Mode] 에서는 실행 할 수 없습니다.");
		return;	
	}

	if( EPGetGroupState(m_nCurModuleID) == EP_STATE_LINE_OFF )
	{
		return;
	}	

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format((TEXT_LANG[61]), m_nCurModuleID );
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPERATION_LOCAL) == false )
	{
		strFailMD += strTemp;
		strTemp.Format(TEXT_LANG[33],  ::GetModuleName(m_nCurModuleID));//"%s는 local mode 변경에 실패 하였습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[34], strFailMD); //"%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		MessageBox(strTemp, TEXT_LANG[59], MB_OK|MB_ICONWARNING);		
	}

	::EPSetAutoProcess(m_nCurModuleID, FALSE);
	pDoc->SaveAutoProcSetToDB(m_nCurModuleID, FALSE);
	UpdateGroupState(m_nCurModuleID);	

	strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
	pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
}

void CManualControlView::OnBnClickedMaintenanceModeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[31]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]";
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}

	if( m_nLineMode == EP_OPERATION_AUTO )
	{
		strTemp.Format(TEXT_LANG[32]);//"명령 전송을 실패 하였습니다.\n[ Automatic Mode 에서는 FMS에서 조작 가능합니다. ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// AfxMessageBox("[Automatic Mode] 에서는 실행 할 수 없습니다.");
		return;	
	}

	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(TEXT_LANG[59] + " [Change to Maintenance]");
		return ;
	}

	strTemp.Format(TEXT_LANG[62], 1, GetModuleName(m_nCurModuleID));
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));

	if( pDoc->SetOperationMode(m_nCurModuleID, 0, EP_OPEARTION_MAINTENANCE) == false )
	{
		strFailMD += strTemp;
		strTemp.Format(TEXT_LANG[37], ::GetModuleName(m_nCurModuleID));//"%s는 Maintenance mode 변경에 실패 하였습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[38], strFailMD); //"%s 에 명령 전송을 실패 하였습니다.[ Stage 상태정보 불일치 ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("%s 에 명령 전송을 실패 하였습니다.[ Stage 상태정보 불일치 ]", strFailMD);
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);		
	}

	strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
	pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
}

void CManualControlView::OnBnClickedRestartBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[31]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}

	// 1. Contact Check 일 경우에만 공정을 진행한다.
	strTemp.Format("[Stage %s] ", ::GetModuleName(m_nCurModuleID));

	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);	

	strTemp.Format("RestartBtn pressed");
	pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);

	//20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
	if(pModule->m_bRackToRack == TRUE)
	{
		strTemp.Format("Command transmission is failed.\n[ Tray position Change ]");//
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;	
	}

	if(pModule)
	{
		if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_CONTACT )
		{
			strTemp.Format(TEXT_LANG[63], ::GetModuleName(m_nCurModuleID));
			if(MessageBox(strTemp, TEXT_LANG[41], MB_ICONQUESTION|MB_YESNO) != IDYES)	return;			//"ReStart"		
		}
		else
		{
			strFailMD +=  strTemp;
			strTemp.Format("EP_RESULTFILE_TYPE_CONTACT else");
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
	else
	{
		strFailMD +=  strTemp; 
		strTemp.Format("pModule else");
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[42]);//"Contact Check Error 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("%s 는 Contact Check Error 상태가 아닙니다.", strFailMD );
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		return;
	}	

	int nRtn = EP_NACK;

	state = EPGetGroupState(m_nCurModuleID);
	if(state == EP_STATE_PAUSE )	
	{		
		pModule->LoadResultData();

		((CMainFrame *)AfxGetMainWnd())->m_pTab->UpdateGroupState(m_nCurModuleID);

		if(( nRtn = EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_RESTART)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  TEXT_LANG[64],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
		else
		{
			pModule->m_bProcessStart = true;	//20200914 ksj 공정시작 플래그 : 공정 시작시 랙간이동불가
			strTemp.Format("EP_CMD_RESTART send Success!!");
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
	else if(state == EP_STATE_STANDBY || state == EP_STATE_READY)					
	{					
		EP_RUN_CMD_INFO	runInfo;					
		ZeroMemory(&runInfo, sizeof(EP_RUN_CMD_INFO));			

		runInfo.nTabDeepth = pModule->GetTrayInfo(0)->m_nTabDeepth;	
		runInfo.nTrayHeight = pModule->GetTrayInfo(0)->m_nTrayHeight;
		runInfo.nTrayType = pModule->GetTrayInfo(0)->m_nTrayType;	
		runInfo.nCurrentCellCnt = pModule->GetTrayInfo(0)->GetInputCellCnt();
		runInfo.nContactErrlimit = pModule->GetTrayInfo(0)->m_nContactErrlimit;
		runInfo.nCappErrlimit = pModule->GetTrayInfo(0)->m_nCapaErrlimit;
		runInfo.nChErrlimit = pModule->GetTrayInfo(0)->m_nChErrlimit;
		runInfo.nJigHeightMode = pModule->GetJigHeightMode();

		if(pModule->GetTrayInfo(0)->GetTrayNo().GetLength() == 18 )
		{
			sprintf(runInfo.szTrayID_1st, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Left(6) );
			sprintf(runInfo.szTrayID_2nd, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Mid(6, 6));
			sprintf(runInfo.szTrayID_3rd, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Right(6) );

			runInfo.nTrayKind = 2;  //KSH 20190812 TrayKind
		}
		else if( pModule->GetTrayInfo(0)->GetTrayNo().GetLength() == 12 )
		{
			sprintf(runInfo.szTrayID_1st, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Left(6) );
			sprintf(runInfo.szTrayID_2nd, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Right(6) );

			runInfo.nTrayKind = 1;  //KSH 20190812 TrayKind
		}
		else
		{
			sprintf(runInfo.szTrayID_1st, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Left(6) );

			runInfo.nTrayKind = 0;  //KSH 20190812 TrayKind
		}		

		//Module에 전송 정보 저장 (파일생성 이전에 )						
		pModule->UpdateStartTime();						
		pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.					

		//TestSerialNo 전송(실시간 저장 Data를 구별하는데 사용)
		//20061025 => 새로운 번호 생성(TestSerialNo는 Tray간의 고유번호로 사용되므로 다른 번호 생성)
		//sprintf(runInfo.szTestSerialNo, pModule->GetTestSerialNo());
		sprintf(runInfo.szTestSerialNo, pModule->GetSyncCode());			

		runInfo.dwUseFlag = 1;						

		CTray *pTray;						
		for(int nTrayIndex=0; nTrayIndex < INSTALL_TRAY_PER_MD; nTrayIndex++)						
		{						
			pTray = pModule->GetTrayInfo(nTrayIndex);						
			if(pTray)						
			{						
				runInfo.nInputCell[nTrayIndex] = pTray->GetInputCellCnt();						
			}						
		}

		if((nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_RUN, &runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)						
		{						
			strFailMD += strTemp;						
			strTemp.Format("%s (%s-%s)",  TEXT_LANG[64],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));						
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);						
		}
		else
		{
			strTemp.Format("EP_CMD_RUN send Success!!");
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}					
	else					
	{					
		strFailMD += strTemp;				
		strTemp.Format(TEXT_LANG[43],  ::GetModuleName(m_nCurModuleID)); //"%s 는 작업 재시작을 전송할 수 없는 상태이므로 전송하지 않았습니다."
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);				
	}

	if(!strFailMD.IsEmpty())
	{
		strTemp.Format(TEXT_LANG[44]); //"전송할 수 없는 상태이거나 전송을 실패 하였습니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("%s 는 전송할 수 없는 상태이거나 전송을 실패 하였습니다.", strFailMD);
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}
	else//FMS 초기화 [2013/9/10 cwm]
	{	
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;
		pModule->UpdateTestLogHeaderTempFile();

		pDoc->m_fmst.fnClearReset(m_nCurModuleID);

		strTemp.Format(TEXT_LANG[35]); //"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedResetBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T("");

	pMainFrm->nModuleReadyToStart[m_nCurModuleID] = 0;

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[31]); //"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;
	}

	strTemp.Format(TEXT_LANG[65], ::GetModuleName(m_nCurModuleID));
	if(MessageBox(strTemp, "Reset", MB_ICONQUESTION|MB_YESNO) != IDYES)	 return;

	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	int nOperationMode = pModule->GetOperationMode();	


	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[%s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);

	if(state != EP_STATE_RUN)	
	{
		if(pDoc->SendInitCommand(m_nCurModuleID, 0) == FALSE)
		{
			strFailMD += strTemp;			
		}
	}
	else
	{
		strFailMD += strTemp;		
	}	

	if(!strFailMD.IsEmpty())
	{	
		strTemp.Format(TEXT_LANG[38]);//"명령 전송을 실패 하였습니다.\n[ Stage 상태정보 불일치 ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
		return;
	}

	strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
	pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
}

void CManualControlView::OnBnClickedBtnStopper1()
{
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[49]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvJigIOData.ch_2 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	OnBtnClickedOn005();

	return;
}

void CManualControlView::OnBnClickedBtnStopper2()
{
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[49]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvJigIOData.ch_2 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	OnBtnClickedOn006();

	return;
}

void CManualControlView::OnBnClickedBtnStopper3()
{
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[49]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvJigIOData.ch_2 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	OnBtnClickedOn007();

	return;
}

void CManualControlView::OnBnClickedBtnStopper4()
{
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[49]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvJigIOData.ch_2 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	OnBtnClickedOn008();

	return;
}


void CManualControlView::OnBnClickedBtnStopper5()
{
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[49]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvJigIOData.ch_2 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	OnBtnClickedOn009();

	return;
}

void CManualControlView::OnBnClickedBtnStopper6()
{
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[49]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return;	
	}

	if( m_RecvJigIOData.ch_2 == 1 )
	{
		m_LabelLog.SetText(TEXT_LANG[36]);//"클립 플레이트 전진상태에서는 작동 시킬 수 없습니다."
		return;
	}

	OnBtnClickedOn010();

	return;
}

void CManualControlView::OnBnClickedBtnStopperSetting()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTemp = _T("");
	CStopperSettingDlg dlg;
	if( dlg.DoModal() == IDOK )
	{	
		if( dlg.m_Stopper1 != _T("") )
		{
			strTemp.Format("#1 (%s)", dlg.m_Stopper1);
		}
		else
		{
			strTemp.Format("-");
		}		
		GetDlgItem(IDC_BTN_STOPPER1)->SetWindowText(strTemp);

		if( dlg.m_Stopper2 != _T("") )
		{
			strTemp.Format("#2 (%s)", dlg.m_Stopper2);
		}
		else
		{
			strTemp.Format("-");
		}
		GetDlgItem(IDC_BTN_STOPPER2)->SetWindowText(strTemp);

		if( dlg.m_Stopper3 != _T("") )
		{
			strTemp.Format("#3 (%s)", dlg.m_Stopper3);
		}
		else
		{
			strTemp.Format("-");
		}		
		GetDlgItem(IDC_BTN_STOPPER3)->SetWindowText(strTemp);


		if( dlg.m_Stopper4 != _T("") )
		{
			strTemp.Format("#4 (%s)", dlg.m_Stopper4);
		}
		else
		{
			strTemp.Format("-");
		}
		GetDlgItem(IDC_BTN_STOPPER4)->SetWindowText(strTemp);

		if( dlg.m_Stopper5 != _T("") )
		{
			strTemp.Format("#5 (%s)", dlg.m_Stopper5);
		}
		else
		{
			strTemp.Format("-");
		}
		GetDlgItem(IDC_BTN_STOPPER5)->SetWindowText(strTemp);

		if( dlg.m_Stopper6 != _T("") )
		{
			strTemp.Format("#6 (%s)", dlg.m_Stopper6);
		}
		else
		{
			strTemp.Format("-");
		}
		GetDlgItem(IDC_BTN_STOPPER6)->SetWindowText(strTemp);
	}
}


void CManualControlView::OnBnClickedBtnNormalSensor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( m_pNormalSensorMapDlg == NULL )
	{	
		m_pNormalSensorMapDlg = new CNormalSensorMapDlg(GetDocument()->m_nDeviceID, m_bReadySetCmd, m_SendJigIOData, m_RecvJigIOData, m_SendFanIOData, m_RecvFanIOData);
		m_pNormalSensorMapDlg->Create(IDD_NORMAL_SENSOR_PIC_DLG, this);
		m_pNormalSensorMapDlg->CenterWindow();
		m_pNormalSensorMapDlg->ShowWindow(SW_SHOW);	
	}
	else
	{
		m_pNormalSensorMapDlg->ShowWindow(SW_SHOW);
	}
}

void CManualControlView::OnBnClickedBuzzerOffBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	int nRtn = EP_NACK;

	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_BUZZER_OFF)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  TEXT_LANG[64],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[31]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[40]);//"명령 전송을 실패 하였습니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp);
	}
	else
	{
		strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedEmergencyStopBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	int nRtn = EP_NACK;

	state = EPGetGroupState(m_nCurModuleID);
	if( state != EP_STATE_LINE_OFF )
	{	
		if(( nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_EMERGENCY_STOP)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)", TEXT_LANG[64],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
		// EPSendCommand(m_nCurModuleID);
	}
	else
	{
		strTemp.Format(TEXT_LANG[31]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
		return;
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[40]);//"명령 전송을 실패 하였습니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
	}
	else
	{
		strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedContinueBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T(""), strFailMD = _T("");

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strTemp.Format(TEXT_LANG[31]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);				
		return;		
	}

	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);		

	if( pModule->m_bRackToRack == TRUE )	//20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
	{
		strTemp.Format("Command transmission is failed.\n[ Tray position Change ]");//
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		return;		
	}

	// 1. 전송할 Unit이 1개일 경우
	strTemp.Format("[Stage %s] ", ::GetModuleName(m_nCurModuleID));
	state = EPGetGroupState(m_nCurModuleID);

	if(pModule)
	{
		// 1. EMG 이거나 정전발생시 재시작이 가능하도록 수정
		if( pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_EMG 
			|| pModule->GetTrayInfo(0)->m_nResultFileType == EP_RESULTFILE_TYPE_RES
			|| state == EP_STATE_PAUSE )
		{
			strTemp.Format(TEXT_LANG[66], ::GetModuleName(m_nCurModuleID));
			if(MessageBox(strTemp, "Continue", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;				
		}
		else { 
			strFailMD +=  strTemp;
		}
	}
	else { 
		strFailMD +=  strTemp;
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[45]);//"명령 전송을 실패 하였습니다.\n[ Stage 상태 불일치(Error) ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		//strTemp.Format("%s 는 Error 상태가 아닙니다.", strFailMD );
		//MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		return;
	}

	int nRtn = EP_NACK;
	strTemp.Format("[Stage %s] ", ::GetModuleName(m_nCurModuleID));

	if( state == EP_STATE_PAUSE )
	{	
		CString strResultData = _T("");
		CString strTempResultFileName = _T("");

		char	drive[_MAX_PATH]; 
		char	dir[_MAX_PATH]; 
		char	fname[_MAX_PATH]; 
		char	ext[_MAX_PATH]; 

		strResultData = pModule->GetResultFileName(0);

		_splitpath((LPSTR)(LPCTSTR)strResultData, drive, dir, fname, ext);	

		strTempResultFileName.Format("%s\\%s\\%s_TEMP.fmt", drive, dir, fname);

		if( _access(strTempResultFileName, 0) != -1 )
		{
			if( _unlink(strResultData) != -1 )
			{
				CopyFile( strTempResultFileName, strResultData, false );			
			}
		}

		// Module 클래스의 공정 정보를 초기화 후 다시 불러온다.
		pModule->LoadResultData();

		((CMainFrame *)AfxGetMainWnd())->m_pTab->UpdateGroupState(m_nCurModuleID);			

		if(( nRtn = EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_CONTINUE)) != EP_ACK)
		{
			strFailMD += strTemp;
			strTemp.Format("%s (%s-%s)",  TEXT_LANG[64],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));		
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		}
	}
	else if(state == EP_STATE_STANDBY || state == EP_STATE_READY )					
	{						
		EP_RUN_CMD_INFO	runInfo;					
		ZeroMemory(&runInfo, sizeof(EP_RUN_CMD_INFO));			

		runInfo.nTabDeepth = pModule->GetTrayInfo(0)->m_nTabDeepth;	
		runInfo.nTrayHeight = pModule->GetTrayInfo(0)->m_nTrayHeight;
		runInfo.nTrayType = pModule->GetTrayInfo(0)->m_nTrayType;	
		runInfo.nCurrentCellCnt = pModule->GetTrayInfo(0)->GetInputCellCnt();
		runInfo.nContactErrlimit = pModule->GetTrayInfo(0)->m_nContactErrlimit;
		runInfo.nCappErrlimit = pModule->GetTrayInfo(0)->m_nCapaErrlimit;
		runInfo.nChErrlimit = pModule->GetTrayInfo(0)->m_nChErrlimit;
		runInfo.nJigHeightMode = pModule->GetJigHeightMode();

		if( pModule->GetTrayInfo(0)->GetTrayNo().GetLength() == 18 )
		{
			sprintf(runInfo.szTrayID_1st, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Left(6) );
			sprintf(runInfo.szTrayID_2nd, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Mid(6, 6));
			sprintf(runInfo.szTrayID_3rd, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Right(6) );

			runInfo.nTrayKind = 1;  //KSH 20190812 TrayKind
		}
		else if( pModule->GetTrayInfo(0)->GetTrayNo().GetLength() == 12 )
		{
			sprintf(runInfo.szTrayID_1st, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Left(6) );
			sprintf(runInfo.szTrayID_2nd, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Right(6) );

			runInfo.nTrayKind = 1;  //KSH 20190812 TrayKind
		}
		else
		{
			sprintf(runInfo.szTrayID_1st, "%s", pModule->GetTrayInfo(0)->GetTrayNo().Left(6) );

			runInfo.nTrayKind = 0;  //KSH 20190812 TrayKind
		}

		//Module에 전송 정보 저장 (파일생성 이전에 )						
		pModule->UpdateStartTime();						
		pModule->UpdateTestLogTempFile();	//CheckEnableRun()에서 파일을 생성하므로 그 이전에 Update해야 한다.					

		runInfo.dwUseFlag = 1;						
		sprintf(runInfo.szTestSerialNo, pModule->GetSyncCode());

		CTray *pTray;						
		for(int nTrayIndex=0; nTrayIndex < INSTALL_TRAY_PER_MD; nTrayIndex++)						
		{						
			pTray = pModule->GetTrayInfo(nTrayIndex);						
			if(pTray)						
			{						
				runInfo.nInputCell[nTrayIndex] = pTray->GetInputCellCnt();						
			}						
		}

		if((nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_RUN, &runInfo, sizeof(EP_RUN_CMD_INFO))) != EP_ACK)						
		{						
			strFailMD += strTemp;						
			strTemp.Format("%s (%s-%s)", TEXT_LANG[64],  ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));						
			pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);						
		}		
	}					
	else					
	{					
		strFailMD += strTemp;				
		strTemp.Format(TEXT_LANG[46],  ::GetModuleName(m_nCurModuleID));//"%s 는 계속진행을 전송할 수 없는 상태이므로 전송하지 않았습니다."			
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);				
	}

	if( !strFailMD.IsEmpty() )
	{
		strTemp.Format(TEXT_LANG[47]);//"명령 전송을 실패 하였습니다.\n[ 전송 실패 ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
	}
	else
	{
		pModule->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
		pModule->UpdateTestLogHeaderTempFile();

		pDoc->m_fmst.fnClearReset(m_nCurModuleID);

		strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);

		pModule->m_bProcessStart = true;	//20200914 ksj 공정시작 플래그 : 공정 시작시 랙간이동불가
	}
}

void CManualControlView::OnBnClickedFmsReset1Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T("");

	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);					

	WORD state = EPGetGroupState(m_nCurModuleID);

	if( state != EP_STATE_RUN && pModule->GetTrayInfo()->GetTrayNo().GetLength() != 0 )
	{
		strTemp.Format(TEXT_LANG[48],  ::GetModuleName(m_nCurModuleID)); //"[Stage %s] 트레이 입고 위치 변경을 요청합니다."
		if( MessageBox(strTemp, TEXT_LANG[50], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return; //"트레이 입고 위치 변경"

		if( pDoc->m_fmst.fnChangeReserveProcPosition(m_nCurModuleID) == TRUE )
		{
			strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
		}
		else
		{
			strTemp.Format(TEXT_LANG[51]); //"트레이 위치 변경이 가능한 상태가 아닙니다."
			pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		}
	}
	else
	{
		strTemp.Format(TEXT_LANG[51]); //"트레이 위치 변경이 가능한 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
	}
}

void CManualControlView::OnBnClickedFmsReset2Btn()
{
	CString strTemp = _T("");
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	if(pDoc->m_fmst.fnTrayInStateReset(m_nCurModuleID) == FALSE )
	{
		strTemp.Format(TEXT_LANG[52]);//"입고 예약된 공정을 진행할 수 있는 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);
		// strTemp.Format("[Stage %s] 는 예약된 공정을 진행할 수 있는 상태가 아닙니다.",  ::GetModuleName(m_nCurModuleID));
		// MessageBox(strTemp, ::GetStringTable(IDS_TEXT_SEND_FAIL), MB_OK|MB_ICONWARNING);
	}
	else
	{
		strTemp.Format(TEXT_LANG[53],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 입고 예약된 공정을 진행합니다."
		if( MessageBox(strTemp, TEXT_LANG[54], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;//"공정 진행 확인"

		strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
}

void CManualControlView::OnBnClickedFmsEndCmdSendBtn()
{
	//end 처음부터 시작
	CCTSMonDoc *pDoc = GetDocument();
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();

	CString strTemp = _T("");

	strTemp.Format(TEXT_LANG[55],  ::GetModuleName(m_nCurModuleID));//"[Stage %s] 검사 종료 통지 Command를 재전송합니다."

	if( MessageBox(strTemp, TEXT_LANG[56], MB_OKCANCEL|MB_ICONWARNING) != IDOK ) return;//"검사 종료 통지 Command 전송"

	if(pDoc->m_fmst.fnEndStateReset(m_nCurModuleID))
	{	
		strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
	}
	else
	{	
		strTemp.Format(TEXT_LANG[57]);//"공정이 완료된 상태가 아닙니다."
		pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_FAULT);		
	}
}

void CManualControlView::OnBnClickedSafetylocalModeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame *pMainFrm = (CMainFrame*)AfxGetMainWnd();
	CString strLog = _T("");

	WORD state;
	state = EPGetGroupState(m_nCurModuleID);
	if( state == EP_STATE_LINE_OFF )
	{
		strLog.Format(TEXT_LANG[31]);//"명령 전송을 실패 하였습니다.\n[ Stage Line off ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strLog, INFO_TYPE_FAULT);
		return;		
	}

	if( m_nLineMode == EP_OPERATION_AUTO )
	{
		strLog.Format(TEXT_LANG[32]);//"명령 전송을 실패 하였습니다.\n[ Automatic Mode 에서는 FMS에서 조작 가능합니다. ]"
		pMainFrm->ShowInformation( m_nCurModuleID, strLog, INFO_TYPE_FAULT);		
		return;	
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp = _T(""), strFailMD = _T("");

	strTemp.Format(TEXT_LANG[58], m_nCurModuleID );//"선택 Stage [%d]를 Safety operation 상태로 전환 하시겠습니까?"
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	::EPSetAutoProcess(m_nCurModuleID, TRUE);
	pDoc->SaveAutoProcSetToDB(m_nCurModuleID, TRUE);
	UpdateGroupState(m_nCurModuleID);

	strTemp.Format(TEXT_LANG[35]);//"명령을 전송 하였습니다.\n잠시만 기다려주십시오."
	pMainFrm->ShowInformation( m_nCurModuleID, strTemp, INFO_TYPE_NORMAL);
}

BOOL CManualControlView::IsMainternanceMode()
{
	if( m_nLineMode != EP_OPEARTION_MAINTENANCE )
	{
		m_LabelLog.SetText(TEXT_LANG[49]);//"[수동 조작] 기능은 Maintenance 상태에서만 실행할 수 있습니다."
		return FALSE;	
	}

	return TRUE;
}

void CManualControlView::OnBtnClickedOn001()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_0 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn002()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_1 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn003()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_2 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn004()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_3 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn005()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_4 = 1;
	m_SendJigIOData.ch_5 = 0;
	m_SendJigIOData.ch_6 = 0;
	m_SendJigIOData.ch_7 = 0;
	m_SendJigIOData.ch_8 = 0;
	m_SendJigIOData.ch_9 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn006()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_4 = 0;
	m_SendJigIOData.ch_5 = 1;
	m_SendJigIOData.ch_6 = 0;
	m_SendJigIOData.ch_7 = 0;
	m_SendJigIOData.ch_8 = 0;
	m_SendJigIOData.ch_9 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn007()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_4 = 0;
	m_SendJigIOData.ch_5 = 0;
	m_SendJigIOData.ch_6 = 1;
	m_SendJigIOData.ch_7 = 0;
	m_SendJigIOData.ch_8 = 0;
	m_SendJigIOData.ch_9 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn008()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_4 = 0;
	m_SendJigIOData.ch_5 = 0;
	m_SendJigIOData.ch_6 = 0;
	m_SendJigIOData.ch_7 = 1;
	m_SendJigIOData.ch_8 = 0;
	m_SendJigIOData.ch_9 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn009()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_4 = 0;
	m_SendJigIOData.ch_5 = 0;
	m_SendJigIOData.ch_6 = 0;
	m_SendJigIOData.ch_7 = 0;
	m_SendJigIOData.ch_8 = 1;
	m_SendJigIOData.ch_9 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn010()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_4 = 0;
	m_SendJigIOData.ch_5 = 0;
	m_SendJigIOData.ch_6 = 0;
	m_SendJigIOData.ch_7 = 0;
	m_SendJigIOData.ch_8 = 0;
	m_SendJigIOData.ch_9 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn011()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_10 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn012()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_11 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn013()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_12 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn014()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_13 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn015()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_14 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOn016()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_15 = 1;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff001()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_0 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff002()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_1 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff003()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_2 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff004()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_3 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff005()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_4 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff006()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_5 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff007()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_6 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff008()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_7 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff009()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_8 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff010()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_9 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff011()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_10 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff012()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_11 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff013()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_12 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff014()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_13 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff015()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_14 = 0;
	m_bReadySetCmd = true;
}

void CManualControlView::OnBtnClickedOff016()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(IsMainternanceMode() == FALSE) return;

	m_LabelLog.SetText("-");

	m_SendJigIOData.ch_15 = 0;
	m_bReadySetCmd = true;
}
