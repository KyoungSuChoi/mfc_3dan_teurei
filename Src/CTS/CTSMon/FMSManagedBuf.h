#pragma once

class CFMSManagedBuf : public CFMSMemoryPool<CFMSManagedBuf>
{
public:
	CFMSManagedBuf(void);
	~CFMSManagedBuf(void);

	// BYTE m_aucBuf[FMS_MAX_BUF_LEN];
	BYTE m_aucBuf[MAX_BUFFER_LENGTH];
	BYTE m_aucResultBuf[MAX_RESULT_BUFFER_LENGTH];
};

class CFMSManagedBufSP
{
public:
	CFMSManagedBufSP(VOID){
		m_poManagedBuf = new CFMSManagedBuf();}
	~CFMSManagedBufSP(VOID){
		delete m_poManagedBuf;}

	CFMSManagedBuf* operator->(VOID){return m_poManagedBuf;}
	operator CFMSManagedBuf*(VOID){return m_poManagedBuf;}

private:
	CFMSManagedBuf	*m_poManagedBuf;
};