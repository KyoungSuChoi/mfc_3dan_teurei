// MiswiringUnwiringDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "MiswiringUnwiredDlg.h"


// CMiswiringUnwiringDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMiswiringUnwiredDlg, CDialog)

CMiswiringUnwiredDlg::CMiswiringUnwiredDlg( CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleNo )
	: CDialog(CMiswiringUnwiredDlg::IDD, pParent)
{
	m_pDoc		= pDoc;
	m_nCurModuleNo	= iTargetModuleNo;	
	m_pInformationDlg = NULL;
}

CMiswiringUnwiredDlg::~CMiswiringUnwiredDlg()
{
	this->DestroyWindow();

	if(m_pInformationDlg != NULL)
	{
		delete m_pInformationDlg;
		m_pInformationDlg = NULL;
	}
}

void CMiswiringUnwiredDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_STAGE_LIST, m_combo_Stage_List);
	DDX_Control(pDX, IDC_IMG_MISSWIRING_UNWIRED, m_ctrl_Img_MissUnwired);
}


BEGIN_MESSAGE_MAP(CMiswiringUnwiredDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_MISWIRING_UNWIRED_REFRESH, &CMiswiringUnwiredDlg::OnBtnClickedMiswiringUnwiredRefresh)
	ON_CBN_SELCHANGE(IDC_COMBO_STAGE_LIST, &CMiswiringUnwiredDlg::OnCbnSelchangeComboStageList)
	ON_MESSAGE(WM_GRID_CLICK, OnGridMouseLButtonDown)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL CMiswiringUnwiredDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitializeGrid();
	InitializeComboList();

	OnBtnClickedMiswiringUnwiredRefresh();

// TEST START
	EP_CMD_CHANNEL_MISWIRING_UNWIRED_CHECK_RESPONSE response;
	ZeroMemory(&response, sizeof(EP_CMD_CHANNEL_MISWIRING_UNWIRED_CHECK_RESPONSE));

	response.nMainBoardCount = 15;
	response.nChannelCountPerBoard = 8;
	response.nInverterCountPerBoard = 2;

	// + 레퍼런스 값
	for(int i = 0; i < response.nMainBoardCount; i++)
	{
		for(int j = 0; j < response.nChannelCountPerBoard; j++)
		{
			if(i == 0 || i == 3 || i == 6)
			{
				response.pPlusChannelMiswiringRefValue[i][j] = 660;
			}
			else if(i == 1 || i == 4 || i == 7)
			{
				response.pPlusChannelMiswiringRefValue[i][j] = 1340;
			}
			else if(i == 2 || i == 5 || i == 8)
			{
				response.pPlusChannelMiswiringRefValue[i][j] = 1980;
			}
		}
	}

	// + 센싱
	for(int i = 0; i < response.nMainBoardCount; i++)
	{
		for(int j = 0; j < response.nChannelCountPerBoard; j++)
		{
			if(i == 0 || i == 3 || i == 6)
			{
				response.pPlusChannelSensingValue[i][j] = 660;
			}
			else if(i == 1 || i == 4 || i == 7)
			{
				response.pPlusChannelSensingValue[i][j] = 1340;
			}
			else if(i == 2 || i == 5 || i == 8)
			{
				response.pPlusChannelSensingValue[i][j] = 1980;
			}

			if(i == 0 && j == 0)
				response.pPlusChannelSensingValue[i][j] = 200;
		}
	}

	// - 레퍼런스 값
	for(int i = 0; i < response.nMainBoardCount; i++)
	{
		for(int j = 0; j < response.nChannelCountPerBoard; j++)
		{
			if(i == 0 || i == 3 || i == 6)
			{
				response.pMinusChannelMiswiringRefValue[i][j] = 1980;
			}
			else if(i == 1 || i == 4 || i == 7)
			{
				response.pMinusChannelMiswiringRefValue[i][j] = 660;
			}
			else if(i == 2 || i == 5 || i == 8)
			{
				response.pMinusChannelMiswiringRefValue[i][j] = 1340;
			}
		}
	}

	// - 센싱
	for(int i = 0; i < response.nMainBoardCount; i++)
	{
		for(int j = 0; j < response.nChannelCountPerBoard; j++)
		{
			if(i == 0 || i == 3 || i == 6)
			{
				response.pMinusChannelSensingValue[i][j] = 1980;
			}
			else if(i == 1 || i == 4 || i == 7)
			{
				response.pMinusChannelSensingValue[i][j] = 660;
			}
			else if(i == 2 || i == 5 || i == 8)
			{
				response.pMinusChannelSensingValue[i][j] = 1340;
			}

			if(i == 0 && j == 1)
				response.pMinusChannelSensingValue[i][j] = 640;

			if(i == 0 && j == 2)
				response.pMinusChannelSensingValue[i][j] = 2600;
		}
	}

	// 오배선 레인지 기준값
	response.iMisswiringRangeValue = 300;

	// 미배선 기준값
	response.iUnwiredRefValue = 2500;

	for(int i = 0; i < response.nMainBoardCount; i++)
	{
		for(int j = 0; j < response.nInverterCountPerBoard; j++)
		{
			response.pInverterSensingValue[i][j] = TRUE;
		}
	}

//	OnMiswiringUnwiredCheckResponse(m_nCurModuleNo, response);

//	if(m_ctrl_Img_MissUnwired.Load("C:\\Program Files (x86)\\PNE CTS\\IMG\\Board1_Ch1.gif"))
//	{
//		m_ctrl_Img_MissUnwired.Draw();
//	}
// TEST END

	return TRUE;
}

void CMiswiringUnwiredDlg::InitializeGrid()
{
	m_wndMiswiringUnwiringGrid.SubclassDlgItem(IDC_MISWIRING_UNWIRED_GRID, this);
	m_wndMiswiringUnwiringGrid.m_bSameColSize  = FALSE;
	m_wndMiswiringUnwiringGrid.m_bSameRowSize  = FALSE;
	m_wndMiswiringUnwiringGrid.m_bCustomWidth  = TRUE;

	m_wndMiswiringUnwiringGrid.Initialize();

	this->InitFirstRow();
	this->InitDefaultRows();	
}

BOOL CMiswiringUnwiredDlg::ClearGrid()
{
	InitDefaultRows();

	return TRUE;
}

BOOL CMiswiringUnwiredDlg::InitFirstRow()
{
	try
	{
		BOOL bLock = m_wndMiswiringUnwiringGrid.LockUpdate();	
		m_wndMiswiringUnwiringGrid.SetColCount(MISWIRING_COLUMN_MAX_COUNT);
		m_wndMiswiringUnwiringGrid.m_BackColor	= RGB(255,255,255);

		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_BOARD_NO),						"BD");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_CHANNEL_NO),					"Ch(B)");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_TOTAL_CHANNEL_NO),				"Ch(T)");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_PLUS_MINUS_DIVISION),			"Division");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_MISWIRING_LOWER_RANGE),		"Miss Lower Range");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_SENSING_VALUE),				"<= Sensing Value >=");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_MISWIRING_UPPER_RANGE),		"Miss Upper Range");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_UNSWIRED_REF_VALUE),			"Unwired Ref Value");		
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1),				"INV1");
		m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(0,MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2),				"INV2");

		CRect rectGrid;
		float width;
		m_wndMiswiringUnwiringGrid.GetClientRect(rectGrid);
		width = (float)rectGrid.Width()/100.0f;

		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_BOARD_NO]				= int(width* 5.0f); // Board
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_CHANNEL_NO]			= int(width* 7.0f); // Board Channel
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_TOTAL_CHANNEL_NO]		= int(width* 7.0f); // Total Channel
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_PLUS_MINUS_DIVISION]	= int(width* 7.0f);
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_MISWIRING_LOWER_RANGE]	= int(width* 16.0f);
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_SENSING_VALUE]			= int(width* 16.0f);
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_MISWIRING_UPPER_RANGE]	= int(width* 15.0f);
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_UNSWIRED_REF_VALUE]	= int(width* 15.0f);
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1]		= int(width* 6.0f); // Inverter1
		m_wndMiswiringUnwiringGrid.m_nWidth[MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2]		= int(width* 6.0f); // Invetter2

		m_wndMiswiringUnwiringGrid.SetDefaultRowHeight(20);
		m_wndMiswiringUnwiringGrid.GetParam()->GetProperties()->SetDisplayHorzLines(TRUE);
		m_wndMiswiringUnwiringGrid.GetParam()->GetProperties()->SetDisplayVertLines(TRUE);	

		m_wndMiswiringUnwiringGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
		m_wndMiswiringUnwiringGrid.SetScrollBarMode(SB_HORZ, gxnDisabled);
		m_wndMiswiringUnwiringGrid.EnableCellTips();

		m_wndMiswiringUnwiringGrid.LockUpdate(bLock);
	}	
	catch (CException* e)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMiswiringUnwiredDlg::InitDefaultRows(int nBoardCount, int nChannelCountPerBoard)
{
	try
	{
		BOOL		bLock = m_wndMiswiringUnwiringGrid.LockUpdate();
		const LONG	nGridRowCount = m_wndMiswiringUnwiringGrid.GetRowCount();
		
		if(nGridRowCount > 0)
			m_wndMiswiringUnwiringGrid.RemoveRows(1, nGridRowCount);

		// Insert Row By Channel Count
		for(int i = 0; i < nBoardCount*nChannelCountPerBoard*2; i++)
		{
			m_wndMiswiringUnwiringGrid.InsertRows(i+1, 1);
		}

		// Board Name
		for(int i = 0; i < nBoardCount; i++)
		{
			CString csTemp;
			csTemp.Format("BD %d", i+1);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*nChannelCountPerBoard*2+1,MIS_UN_WIRING_COLUMN_BOARD_NO), csTemp);
			m_wndMiswiringUnwiringGrid.SetCoveredCellsRowCol(i*nChannelCountPerBoard*2+1, MIS_UN_WIRING_COLUMN_BOARD_NO, i*nChannelCountPerBoard*2+nChannelCountPerBoard*2, MIS_UN_WIRING_COLUMN_BOARD_NO);
		}

		// Board Channel Name
		for(int i = 0; i < nBoardCount; i++)
		{
			for(int j = 0; j < nChannelCountPerBoard; j++)
			{
				CString csTemp;

				csTemp.Format("Ch %d", j+1);
				m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*nChannelCountPerBoard*2+j*2+1,MIS_UN_WIRING_COLUMN_CHANNEL_NO), csTemp); // 1, 3, 5, 7, 9, 11, 13, 15 . . .
				m_wndMiswiringUnwiringGrid.SetCoveredCellsRowCol(i*nChannelCountPerBoard*2+j*2+1, MIS_UN_WIRING_COLUMN_CHANNEL_NO, i*nChannelCountPerBoard*2+j*2+1+1, MIS_UN_WIRING_COLUMN_CHANNEL_NO);
			}
		}

		// Total Channel Name
		for(int i = 0; i < nBoardCount; i++)
		{
			for(int j = 0; j < nChannelCountPerBoard; j++)
			{
				CString csTemp;

				csTemp.Format("Ch %d", i*nChannelCountPerBoard + j+1);
				m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*nChannelCountPerBoard*2+j*2+1,MIS_UN_WIRING_COLUMN_TOTAL_CHANNEL_NO), csTemp); // 1, 3, 5, 7, 9, 11, 13, 15 . . .
				m_wndMiswiringUnwiringGrid.SetCoveredCellsRowCol(i*nChannelCountPerBoard*2+j*2+1, MIS_UN_WIRING_COLUMN_TOTAL_CHANNEL_NO, i*nChannelCountPerBoard*2+j*2+1+1, MIS_UN_WIRING_COLUMN_TOTAL_CHANNEL_NO);
			}
		}

		// Division
		for(int i = 0; i < nBoardCount; i++)
		{
			for(int j = 0; j < nChannelCountPerBoard; j++)
			{
				m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*nChannelCountPerBoard*2+j*2+1,MIS_UN_WIRING_COLUMN_PLUS_MINUS_DIVISION), "+");
				m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*nChannelCountPerBoard*2+j*2+1+1,MIS_UN_WIRING_COLUMN_PLUS_MINUS_DIVISION), "-");
			}
		}

		// Inverter
		for(int i = 0; i < nBoardCount; i++)
		{
			m_wndMiswiringUnwiringGrid.SetCoveredCellsRowCol(i*nChannelCountPerBoard*2+1, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1, i*nChannelCountPerBoard*2+nChannelCountPerBoard*2, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1);
			m_wndMiswiringUnwiringGrid.SetCoveredCellsRowCol(i*nChannelCountPerBoard*2+1, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2, i*nChannelCountPerBoard*2+nChannelCountPerBoard*2, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2);
		}

		m_wndMiswiringUnwiringGrid.LockUpdate(bLock);
		m_wndMiswiringUnwiringGrid.Redraw();
	}	
	catch (CException* e)
	{
		return FALSE;
	}

	return TRUE;
}

void CMiswiringUnwiredDlg::InitializeComboList()
{
	int nModuleNo;

	for(int i=0; i<m_pDoc->GetInstalledModuleNum(); i++)
	{
		nModuleNo = EPGetModuleID(i);
		
		m_combo_Stage_List.AddString(::GetModuleName(nModuleNo));
		m_combo_Stage_List.SetItemData(i, nModuleNo);
	}

	m_combo_Stage_List.SetCurSel(m_nCurModuleNo - 1);

	OnBtnClickedMiswiringUnwiredRefresh();
}

void CMiswiringUnwiredDlg::OnBtnClickedMiswiringUnwiredRefresh()
{
	CString strTemp;
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(m_nCurModuleNo);
	if(pModuleInfo == FALSE)
	{
		ShowInformation(m_nCurModuleNo, "Cannot Find ModuleInfo", INFO_TYPE_FAULT);
		return;
	}

	if(pModuleInfo->GetState() == EP_STATE_LINE_OFF)
	{
		InitDefaultRows();
	}
	else
	{
		if(EPSendCommand(m_nCurModuleNo, 1, 0, EP_CMD_MISWIRING_UNWIRED_CHECK_REQUEST, NULL, 0)!= EP_ACK)
		{
			strTemp.Format("%s :: Miswiring Check Command Send Fail.", ::GetModuleName(m_nCurModuleNo));
			m_pDoc->WriteLog( strTemp );

			ShowInformation(m_nCurModuleNo, strTemp, INFO_TYPE_FAULT);
		}
		else
		{
			ShowInformation(m_nCurModuleNo, "Miswiring Check Command Send Success", INFO_TYPE_NORMAL);
		}
	}
}

void CMiswiringUnwiredDlg::OnCbnSelchangeComboStageList()
{
	int nIndex = m_combo_Stage_List.GetCurSel();
	
	if(nIndex >= 0)
	{
		if(nIndex + 1 != m_nCurModuleNo)
		{
			InitDefaultRows();
			m_nCurModuleNo = nIndex+1;

			OnBtnClickedMiswiringUnwiredRefresh();
		}
	}
}

void CMiswiringUnwiredDlg::ShowInformation( int nUnitNum, CString strMsg, int nMsgType )
{
	if(m_pInformationDlg == NULL)
	{
		m_pInformationDlg = new CInformationDlg();
		m_pInformationDlg->Create(IDD_SHOW_WANNING_DLG,  this);		
		m_pInformationDlg->SetUnitNum( nUnitNum );
		m_pInformationDlg->SetResultMsg( strMsg, nMsgType );
		m_pInformationDlg->ShowWindow(SW_SHOW);		
	}
	else
	{
		m_pInformationDlg->SetUnitNum( nUnitNum );
		m_pInformationDlg->SetResultMsg( strMsg, nMsgType );
		m_pInformationDlg->ShowWindow(SW_SHOW);
	}
}

void CMiswiringUnwiredDlg::OnMiswiringUnwiredCheckResponse(int nModuleNo, EP_CMD_CHANNEL_MISWIRING_UNWIRED_CHECK_RESPONSE response)
{
	if(nModuleNo <= 0 || nModuleNo > m_pDoc->GetInstalledModuleNum())
	{
		ShowInformation(nModuleNo, "Invalid MiswiringUnwiringCheckResponse", INFO_TYPE_FAULT);
		return;
	}

	InitDefaultRows(response.nMainBoardCount, response.nChannelCountPerBoard);

	BOOL		bLock = m_wndMiswiringUnwiringGrid.LockUpdate();
	
	// Plus Minus Sensing Value Display And Check
	for(int i = 0; i < response.nMainBoardCount; i++)
	{
		for(int j = 0; j < response.nChannelCountPerBoard; j++)
		{
			CString csTemp;

			// Plus Miswiring Lower Range Value
			csTemp.Format("%d", response.pPlusChannelMiswiringRefValue[i][j] - response.iMisswiringRangeValue);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1,MIS_UN_WIRING_COLUMN_MISWIRING_LOWER_RANGE), csTemp);
			// Plus Sensing Value
			csTemp.Format("%d", response.pPlusChannelSensingValue[i][j]);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1,MIS_UN_WIRING_COLUMN_SENSING_VALUE), csTemp);
			// Plus Miswiring Upper Range Value
			csTemp.Format("%d", response.pPlusChannelMiswiringRefValue[i][j] + response.iMisswiringRangeValue);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1,MIS_UN_WIRING_COLUMN_MISWIRING_UPPER_RANGE), csTemp);
			// Plus Unwiring Ref Value
			csTemp.Format("%d", response.iUnwiredRefValue);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1,MIS_UN_WIRING_COLUMN_UNSWIRED_REF_VALUE), csTemp);

			// Plus 미배선 체크			
			if(response.pPlusChannelSensingValue[i][j] > response.iUnwiredRefValue)
			{
				// 미배선 감지				
				m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1, MIS_UN_WIRING_COLUMN_SENSING_VALUE), CGXStyle().SetInterior(COLOR_RED));
				m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1, MIS_UN_WIRING_COLUMN_UNSWIRED_REF_VALUE), CGXStyle().SetInterior(COLOR_RED));
			}
			else
			{
				// Plus 오배선 체크
				if(!(response.pPlusChannelSensingValue[i][j] >= response.pPlusChannelMiswiringRefValue[i][j] - response.iMisswiringRangeValue &&
					response.pPlusChannelSensingValue[i][j] <= response.pPlusChannelMiswiringRefValue[i][j] + response.iMisswiringRangeValue))
				{
					// 오배선 감지
					m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1, MIS_UN_WIRING_COLUMN_MISWIRING_LOWER_RANGE), CGXStyle().SetInterior(COLOR_RED));
					m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1, MIS_UN_WIRING_COLUMN_SENSING_VALUE), CGXStyle().SetInterior(COLOR_RED));
					m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1, MIS_UN_WIRING_COLUMN_MISWIRING_UPPER_RANGE), CGXStyle().SetInterior(COLOR_RED));
				}
			}

			// Minus Miswiring Lower Range Value
			csTemp.Format("%d", response.pMinusChannelMiswiringRefValue[i][j] - response.iMisswiringRangeValue);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1,MIS_UN_WIRING_COLUMN_MISWIRING_LOWER_RANGE), csTemp);

			// Minus Sensing Value
			csTemp.Format("%d", response.pMinusChannelSensingValue[i][j]);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1,MIS_UN_WIRING_COLUMN_SENSING_VALUE), csTemp);

			// Minus Miswiring Upper Range Value
			csTemp.Format("%d", response.pMinusChannelMiswiringRefValue[i][j] + response.iMisswiringRangeValue);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1,MIS_UN_WIRING_COLUMN_MISWIRING_UPPER_RANGE), csTemp);

			// Minus Unwiring Ref Value
			csTemp.Format("%d", response.iUnwiredRefValue);
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1,MIS_UN_WIRING_COLUMN_UNSWIRED_REF_VALUE), csTemp);

			// Plus 미배선 체크
			if(response.pMinusChannelSensingValue[i][j] > response.iUnwiredRefValue)
			{
				// 미배선 감지
				m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1, MIS_UN_WIRING_COLUMN_SENSING_VALUE), CGXStyle().SetInterior(COLOR_RED));
				m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1, MIS_UN_WIRING_COLUMN_UNSWIRED_REF_VALUE), CGXStyle().SetInterior(COLOR_RED));
			}
			else
			{
				// Plus 오배선 체크
				if(!(response.pMinusChannelSensingValue[i][j] >= response.pMinusChannelMiswiringRefValue[i][j] - response.iMisswiringRangeValue &&
					response.pMinusChannelSensingValue[i][j] <= response.pMinusChannelMiswiringRefValue[i][j] + response.iMisswiringRangeValue))
				{
					// 오배선 감지
					m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1, MIS_UN_WIRING_COLUMN_MISWIRING_LOWER_RANGE), CGXStyle().SetInterior(COLOR_RED));
					m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1, MIS_UN_WIRING_COLUMN_SENSING_VALUE), CGXStyle().SetInterior(COLOR_RED));
					m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+j*2+1+1, MIS_UN_WIRING_COLUMN_MISWIRING_UPPER_RANGE), CGXStyle().SetInterior(COLOR_RED));
				}
			}
		}
	}

	// Inverter1,2
	for(int i = 0; i < response.nMainBoardCount; i++)
	{
		if(response.pInverterSensingValue[i][0] == 1)
		{
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+1,MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1), "ON");
			m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+1, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1), CGXStyle().SetInterior(COLOR_GREEN));
		}
		else
		{
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+1,MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1), "OFF");
			m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+1, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1), CGXStyle().SetInterior(COLOR_RED));
		}

		if(response.pInverterSensingValue[i][1] == 1)
		{
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+1,MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2), "ON");
			m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+1, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2), CGXStyle().SetInterior(COLOR_GREEN));
		}
		else
		{
			m_wndMiswiringUnwiringGrid.SetValueRange(CGXRange(i*response.nChannelCountPerBoard*2+1,MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2), "OFF");
			m_wndMiswiringUnwiringGrid.SetStyleRange(CGXRange(i*response.nChannelCountPerBoard*2+1, MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2), CGXStyle().SetInterior(COLOR_RED));
		}
	}

	m_wndMiswiringUnwiringGrid.LockUpdate(bLock);
	m_wndMiswiringUnwiringGrid.Redraw();
}

void CMiswiringUnwiredDlg::SetCurrentModuleNo( int nModuleNo )
{
	this->m_nCurModuleNo = nModuleNo;
	m_combo_Stage_List.SetCurSel(m_nCurModuleNo - 1);

	OnBtnClickedMiswiringUnwiredRefresh();
}

BOOL CMiswiringUnwiredDlg::OnEraseBkgnd( CDC* pDC )
{
	CRect rc;
	GetClientRect(&rc);

	CBrush newBrush(RGB_PURELIGHTBLUE);
	CBrush* oldBrush = pDC->SelectObject(&newBrush);

	pDC->PatBlt(rc.left, rc.top, rc.Width(), rc.Height(), PATCOPY);

	pDC->SelectObject(oldBrush);

	return TRUE;
}

LRESULT CMiswiringUnwiredDlg::OnGridMouseLButtonDown( WPARAM wParam, LPARAM lParam )
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(pGrid != (CMyGridWnd *)&m_wndMiswiringUnwiringGrid && pGrid != (CMyGridWnd *)&m_wndMiswiringUnwiringGrid)	return 0;

	if(nCol < 1 || nRow < 1)	return 0;

	int iBoardNo = 0;
	int iChannelNo = 0;

	iBoardNo = ((nRow - 1) / 16) + 1;
	iChannelNo = (((nRow - 1) % 16) / 2) + 1;
	ImageChange(iBoardNo, iChannelNo);
	
	return TRUE;
}

BOOL CMiswiringUnwiredDlg::ImageChange( int iBoardNo, int iChannelNo )
{
	if(m_ctrl_Img_MissUnwired.IsPlaying())
	{
		m_ctrl_Img_MissUnwired.UnLoad();
	}

	CString csFileName;
	csFileName.Format("C:\\Program Files (x86)\\PNE CTS\\IMG\\MisswiringImages\\Board%02d_Ch%02d.gif", iBoardNo, iChannelNo);

	if(m_ctrl_Img_MissUnwired.Load(csFileName))
	{
		m_ctrl_Img_MissUnwired.Draw();
	}
	else
	{
		AfxMessageBox(CString("Image Load Failed : ") + csFileName);
	}

	return TRUE;
}
