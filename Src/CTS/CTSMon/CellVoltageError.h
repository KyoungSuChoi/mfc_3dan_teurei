#if !defined(AFX_CELLVOLTAGEERROR_H__9F70ED9D_F546_46DB_A17F_3F08EAC40767__INCLUDED_)
#define AFX_CELLVOLTAGEERROR_H__9F70ED9D_F546_46DB_A17F_3F08EAC40767__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CellVoltageError.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CellVoltageError dialog

class CellVoltageError : public CDialog
{
// Construction
public:
	~CellVoltageError();
	CString *TEXT_LANG;
	bool LanguageinitMonConfig(CString strClassName);

	CellVoltageError(CWnd* pParent = NULL, CString strUpErr = _T(""), CString strDownErr = _T("") );   // standard constructor
	VOID InitLabel();
	VOID ParsingErrMessage();
	int GetFindCharCount(CString pString, char pChar);
	
// Dialog Data
	//{{AFX_DATA(CellVoltageError)
	enum { IDD = IDD_DLG_ERROR_CELLVOLTAGE };
	CLabel	m_Label_ErrorUnitID;
	CString	m_strDownErrorChNum;
	CString	m_strUpErrorChNum;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CellVoltageError)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strUpErr;
	CString m_strDownErr;

	// Generated message map functions
	//{{AFX_MSG(CellVoltageError)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELLVOLTAGEERROR_H__9F70ED9D_F546_46DB_A17F_3F08EAC40767__INCLUDED_)
