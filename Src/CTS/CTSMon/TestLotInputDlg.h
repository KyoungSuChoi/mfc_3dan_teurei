#if !defined(AFX_TESTLOTINPUTDLG_H__4DFC98C0_0D82_4988_B7C6_3D04FCD02B04__INCLUDED_)
#define AFX_TESTLOTINPUTDLG_H__4DFC98C0_0D82_4988_B7C6_3D04FCD02B04__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestLotInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestLotInputDlg dialog

class CTestLotInputDlg : public CDialog
{
// Construction
public:
	CString m_strTitleLabelString;
	CString m_strTestName;
	CTestLotInputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTestLotInputDlg)
	enum { IDD = IDD_TEST_LOT_DLG };
	CLabel	m_ctrlSelTestName;
	CString	m_strLot;
	CString	m_strTrayNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestLotInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTestLotInputDlg)
	afx_msg void OnOk();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTLOTINPUTDLG_H__4DFC98C0_0D82_4988_B7C6_3D04FCD02B04__INCLUDED_)
