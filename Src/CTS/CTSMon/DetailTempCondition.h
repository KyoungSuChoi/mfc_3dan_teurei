#if !defined(AFX_DETAILTEMPCONDITION_H__6C856EBC_A3E1_4992_B107_850201361C9D__INCLUDED_)
#define AFX_DETAILTEMPCONDITION_H__6C856EBC_A3E1_4992_B107_850201361C9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DetailTempCondition.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDetailTempCondition dialog
#include "CTSMonDoc.h"
#include "MyGridWnd.h"

// #define for GRIDWND
#define TEMP_CONDITION_GRID_ROW 4
#define TEMP_CONDITION_GRID_COL 4
#define _COL_JIGGRID_TEMP1	1
#define _COL_JIGGRID_TEMP2	2
#define _COL_JIGGRID_TEMP3	3
#define _COL_JIGGRID_TEMP4	4

#define _ROW_JIGGRID_NAME1	1
#define _ROW_JIGGRID_NAME2	3
#define _ROW_JIGGRID_VAR1	2
#define _ROW_JIGGRID_VAR2	4

#define _COL_UNITGRID_TEMP1	1
#define _COL_UNITGRID_TEMP2	2
#define _COL_UNITGRID_TEMP3 3 
#define _COL_UNITGRID_TEMP4	4

#define _ROW_UNITGRID_NAME1	1
#define _ROW_UNITGRID_NAME2	3
#define _ROW_UNITGRID_VAR1	2
#define _ROW_UNITGRID_VAR2	4

#define TIMER_TEMP_ID		1
#define TIMER_TEMP_TIME		2000

class CDetailTempCondition : public CDialog
{
// Construction
public:
	CDetailTempCondition(CCTSMonDoc* pDoc, UINT nModuleID, CWnd* pParent = NULL);   // standard constructor
	VOID InitTempGrid();
	VOID InitLabel();
	VOID DisplayTempData();

public:
	CMyGridWnd m_wndJigTempGrid;
	CMyGridWnd m_wndUnitTempGrid;	

// Dialog Data
	//{{AFX_DATA(CDetailTempCondition)
	enum { IDD = IDD_DETAIL_TEMPCONDITION };
	CLabel	m_Label_UnitID;
	CLabel	m_Label_UnitTemp;
	CLabel	m_Label_JigTemp;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDetailTempCondition)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCTSMonDoc* m_pDoc;
	UINT		m_ModuleID;

	// Generated message map functions
	//{{AFX_MSG(CDetailTempCondition)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DETAILTEMPCONDITION_H__6C856EBC_A3E1_4992_B107_850201361C9D__INCLUDED_)
