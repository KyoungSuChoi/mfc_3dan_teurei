// SelResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "SelResultDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelResultDlg dialog


CSelResultDlg::CSelResultDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelResultDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelResultDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	LanguageinitMonConfig(_T("CSelResultDlg"));
	m_nTrayIndex = 0;
	m_pModuleInfo = NULL;
}
CSelResultDlg::~CSelResultDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSelResultDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CSelResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelResultDlg)
	DDX_Control(pDX, IDC_SEL_LIST, m_wndSelList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelResultDlg, CDialog)
	//{{AFX_MSG_MAP(CSelResultDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_SEL_LIST, OnDblclkSelList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelResultDlg message handlers

BOOL CSelResultDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(m_pModuleInfo);
	
	// TODO: Add extra initialization here
	DWORD style = 	m_wndSelList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_wndSelList.SetExtendedStyle(style );

	m_wndSelList.InsertColumn(0, TEXT_LANG[0], LVCFMT_LEFT, 50);//"No"
	m_wndSelList.InsertColumn(1, TEXT_LANG[1], LVCFMT_LEFT, 100);//"Tray ID"
	m_wndSelList.InsertColumn(2, TEXT_LANG[2], LVCFMT_LEFT, 350);//"File name"

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	char szName[32];

	for(int nI = 0; nI < m_pModuleInfo->GetTotalJig(); nI++)
	{	
		if( m_pModuleInfo->GetTrayNo(nI) == "" )
		{
			continue;
		}

		sprintf(szName, "%d", nI+1);
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		m_wndSelList.InsertItem(&lvItem);
		m_wndSelList.SetItemData(lvItem.iItem, nI);		//==>LVN_ITEMCHANGED 를 발생 기킴 
		m_wndSelList.SetItemText(nI, 1, m_pModuleInfo->GetTrayNo(nI));
		m_wndSelList.SetItemText(nI, 2, m_pModuleInfo->GetResultFileName(nI));
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control	    
}

void CSelResultDlg::OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnOK() ;
	
	*pResult = 0;
}

void CSelResultDlg::OnOK() 
{
	// TODO: Add extra validation here

	POSITION pos = m_wndSelList.GetFirstSelectedItemPosition();
	if(pos == NULL)	return;
	m_nTrayIndex = m_wndSelList.GetNextSelectedItem(pos);
	
	CDialog::OnOK();
}
