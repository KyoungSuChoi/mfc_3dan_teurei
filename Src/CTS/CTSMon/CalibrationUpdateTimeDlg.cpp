// CalibrationUpdateTimeDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CalibrationUpdateTimeDlg.h"


// CCalibrationUpdateTimeDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCalibrationUpdateTimeDlg, CDialog)

CCalibrationUpdateTimeDlg::CCalibrationUpdateTimeDlg(CCTSMonDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CCalibrationUpdateTimeDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CCalibrationUpdateTimeDlg"));
	m_pDoc = pDoc;

	m_nLayOutCol = 0;
	m_nLayOutRow = 0;
	m_nMaxStageCnt = 0;

	m_bRackIndexUp = false;
}

CCalibrationUpdateTimeDlg::~CCalibrationUpdateTimeDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CCalibrationUpdateTimeDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}
void CCalibrationUpdateTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelName);
}


BEGIN_MESSAGE_MAP(CCalibrationUpdateTimeDlg, CDialog)
	ON_BN_CLICKED(IDC_CSV_OUTPUT, &CCalibrationUpdateTimeDlg::OnBnClickedCsvOutput)
END_MESSAGE_MAP()


// CCalibrationUpdateTimeDlg 메시지 처리기입니다.

BOOL CCalibrationUpdateTimeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bRackIndexUp = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "RackIndexUp", TRUE);

	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();

	InitLabel();
	InitGridWnd();
	// InitColorBtn();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCalibrationUpdateTimeDlg::InitLabel()
{
	m_LabelName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelName.SetTextColor(RGB_WHITE);
	m_LabelName.SetFontSize(24);
	m_LabelName.SetFontBold(TRUE);

	CString strTemp;
	strTemp.Format(TEXT_LANG[0], AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "CalibrationNotifyAlarmPeriod", 0) ); //"Calibration Update Time [주기:%d일]"
	m_LabelName.SetText(strTemp);
}

BOOL CCalibrationUpdateTimeDlg::Update( int nType )
{
	int i = 0;	
	int nMD = 0;

	int nRow, nCol;

	CString strSQL = _T("");
	CString strTemp = _T("");;
	int nMouleIdx = 0;

	CDaoDatabase  db;

	try
	{	
		db.Open(m_pDoc->m_strDataBaseName);		
	}
	catch (CDaoException* e)
	{
		e->Delete();
		return false;
	}

	try
	{		
		CDaoRecordset rs(&db);
	
		strSQL.Format("Select ModuleID, CalibrationUpdateTime From SystemConfig" );

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		while(!rs.IsEOF())
		{
			COleVariant va = rs.GetFieldValue(0);

			nMD = va.lVal;

			va = rs.GetFieldValue(1);

			GetModuleRowCol(nMD, nRow, nCol);

			if( va.vt != VT_NULL )
			{
				COleDateTime dt(va.date);			

				if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
				{	
					m_wndLayoutGrid.SetStyleRange( CGXRange(nRow, nCol), CGXStyle().SetValue(dt.Format()).SetInterior(RGB_WHITE));								
				}
			}
			else
			{
				if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
				{
					m_wndLayoutGrid.SetStyleRange( CGXRange(nRow, nCol), CGXStyle().SetValue("-").SetInterior(RGB_WHITE));
				}
			}		

			rs.MoveNext();
		}

		rs.Close();

		int nInterval = AfxGetApp()->GetProfileInt(FROM_SYSTEM_SECTION, "CalibrationNotifyAlarmPeriod", 0);

		CTime nowtime(CTime::GetCurrentTime());
		CTimeSpan oneday( nInterval,0,0,0 );
		CTime endtime = nowtime - oneday;

		strTemp.Format(" Where CalibrationUpdateTime < #%s# ", endtime.Format("%Y/%m/%d") );

		strSQL += strTemp;

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		while(!rs.IsEOF())
		{
			COleVariant va = rs.GetFieldValue(0);

			nMD = va.lVal;

			GetModuleRowCol(nMD, nRow, nCol);

			if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
			{
				m_wndLayoutGrid.SetStyleRange( CGXRange(nRow, nCol), CGXStyle().SetInterior(RGB_GREEN));
			}

			rs.MoveNext();
		}

		rs.Close();
	}
	catch (CDBException* e)
	{	
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin);		
		e->Delete();
	}

	db.Close();
	
	return TRUE;	
}

/*
void CCalibrationUpdateTimeDlg::InitColorBtn()
{	
	m_BtnCSVConvert.SetFontStyle(20, 1);
	m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

	m_BtnIDOK.SetFontStyle(20, 1);
	m_BtnIDOK.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}
*/

void CCalibrationUpdateTimeDlg::InitGridWnd()
{
	ASSERT(m_pDoc);

	m_wndLayoutGrid.SubclassDlgItem(IDC_LAYOUT_GRID, this);	
	m_wndLayoutGrid.m_bSameRowSize = FALSE;
	m_wndLayoutGrid.m_bSameColSize = FALSE;
	m_wndLayoutGrid.m_bCustomWidth = TRUE;

	m_wndLayoutGrid.Initialize();

	BOOL bLock = m_wndLayoutGrid.LockUpdate();
	m_nLayOutRow = m_pDoc->m_nModulePerRack;
	m_wndLayoutGrid.SetRowCount(m_nLayOutRow);

	m_nLayOutCol =  m_pDoc->GetInstalledModuleNum()/m_nLayOutRow;
	if( (m_nMaxStageCnt%m_nLayOutRow) > 0) 
	{
		m_nLayOutCol++;
	}

	m_wndLayoutGrid.SetDrawingTechnique(gxDrawUsingMemDC);
	m_wndLayoutGrid.SetColCount(m_nLayOutCol*2);

	m_wndLayoutGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	CString strTemp = _T("");
	int i = 0;
	int nIndex = 0;	

	for(i =0; i<m_nLayOutCol; i++)
	{		
		m_wndLayoutGrid.SetValueRange(CGXRange().SetCols((i+1)*2), TEXT_LANG[2]);//"[ Empty ]"
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols((i+1)*2), CGXStyle().SetEnabled(FALSE));

		m_wndLayoutGrid.SetValueRange(CGXRange().SetCols((i+1)*2-1), "---");
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols((i+1)*2-1), CGXStyle().SetEnabled(FALSE).SetDraw3dFrame(gxFrameRaised));				
	}	

	for( i=0; i<m_nLayOutRow; i++ )
	{
		if( m_bRackIndexUp == TRUE )
		{	// 밑에서 위로
			nIndex = m_nLayOutRow - i;
			strTemp.Format("%d", nIndex);
			m_wndLayoutGrid.SetStyleRange(CGXRange(i+1, 0), CGXStyle().SetValue(strTemp));			
		}
		else
		{	// 위에서 아래로
			nIndex = i+1;
			strTemp.Format("%d",nIndex);
			m_wndLayoutGrid.SetStyleRange(CGXRange(i+1, 0), CGXStyle().SetValue(strTemp));
		}
	}

	int nRow, nCol;
	for( i =0; i<m_nMaxStageCnt; i++)
	{
		int nMD = ::EPGetModuleID(i);
		GetModuleRowCol(nMD, nRow, nCol);
		if(nRow <= m_nLayOutRow && nCol <= m_nLayOutCol*2)
		{	
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), 
				CGXStyle().SetValue("-")				
				.SetEnabled(TRUE));
			strTemp = ::GetModuleName(nMD);	//Module Name			
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol-1), CGXStyle().SetEnabled(FALSE).SetDraw3dFrame(gxFrameRaised).SetValue(strTemp));
			m_wndLayoutGrid.SetStyleRange(CGXRange(nRow, nCol), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
		}
		//열단위 굵은 라인 표시 
		m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(nCol), 
			CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0)))
			);
		if( nCol == m_nLayOutCol*2 )
		{
			m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(nCol), 
				CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0)))
				);
		}		
	}

	//열단위 굵은 라인 표시 
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetCols(1), 
		CGXStyle()
		.SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetRows(1), 
		CGXStyle()
		.SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));
	m_wndLayoutGrid.SetStyleRange(CGXRange().SetRows(m_nLayOutRow), 
		CGXStyle()
		.SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(RGB(255, 128 ,0))));

	// 1열/2열 표시 (상단)  
	for( i = 0; i<m_nLayOutCol; i++)
	{
		m_wndLayoutGrid.SetCoveredCellsRowCol(0, i*2+1, 0, (i+1)*2);			//
		strTemp.Format("%d %s", i+1, TEXT_LANG[3]);	//1열 2열 표시 
		m_wndLayoutGrid.SetStyleRange(CGXRange(0, i*2+1), CGXStyle().SetValue(strTemp).SetTextColor(RGB_LIGHTBLUE));
	}

	m_wndLayoutGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(GRID_DATA_FONT_SIZE).SetBold(TRUE).SetFaceName(TEXT_LANG[4]))		
		);

	m_wndLayoutGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	// 1. 그리드의 크기 조절
	CRect ctrlRect;
	m_wndLayoutGrid.GetWindowRect(ctrlRect);	//Step Grid Window Position
	ScreenToClient(ctrlRect);

	m_wndLayoutGrid.GetClientRect(&ctrlRect);
	int nTotCol = m_wndLayoutGrid.GetColCount()/2;			
	float width = (float)(ctrlRect.Width())/nTotCol;				
	int height = (int)(ctrlRect.Height()-20)/(m_wndLayoutGrid.GetRowCount());

	if(  nTotCol <= 10 && nTotCol > 0 )
	{	
		for(int c = 0; c<nTotCol&&c<32; c++)
		{	
			m_wndLayoutGrid.m_nWidth[c*2+1]	= int(width* 0.3f);					
			m_wndLayoutGrid.m_nWidth[(c+1)*2] = int(width* 0.7f);
		}
		m_wndLayoutGrid.SetDefaultRowHeight( height );
		m_wndLayoutGrid.SetColWidth(0, 0, 0);
	}
	else if( nTotCol > 10 && nTotCol > 0 )
	{	
		for(int c = 0; c<nTotCol&&c<32; c++)
		{
			m_wndLayoutGrid.m_nWidth[c*2+1]	= 0;					
			m_wndLayoutGrid.m_nWidth[(c+1)*2] = int(width* 1.0f);									
		}
		m_wndLayoutGrid.SetRowHeight(0,0,50);
		m_wndLayoutGrid.SetDefaultRowHeight( height );
	}	

	m_wndLayoutGrid.LockUpdate(bLock);
	m_wndLayoutGrid.Redraw();
}

BOOL CCalibrationUpdateTimeDlg::GetModuleRowCol(int nModuleID, int &nRow, int &nCol)
{
	int nModuleIndex = EPGetModuleIndex(nModuleID);
	if( nModuleIndex < 0 )	return FALSE;

	if( m_bRackIndexUp )
	{
		nCol = (nModuleIndex / m_nLayOutRow+1)*2;						//0~11 => 2
		nRow = m_nLayOutRow - ((nModuleIndex) % m_nLayOutRow);			//12-0~11
	}
	else
	{
		nCol = (nModuleIndex / m_nLayOutRow+1)*2;						
		nRow = ((nModuleIndex) % m_nLayOutRow)+1;			
	}
	return TRUE;
}

void CCalibrationUpdateTimeDlg::OnBnClickedCsvOutput()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTime, strData;	

	COleDateTime OleSelectTime;
	OleSelectTime.GetCurrentTime();
	strTime.Format("CalibrationInfo_%d%02d%02d", OleSelectTime.GetYear(), OleSelectTime.GetMonth(), OleSelectTime.GetDay());	

	CStdioFile	file;
	CString		strFileName;		

	int i=0;
	int j=0;

	CFileDialog dlg(false, "", strTime, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[1]);//"csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|"

	if(dlg.DoModal() == IDOK)
	{		
		strFileName = dlg.GetPathName();		
		file.Open(strFileName, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate);

		int nRowCnt = m_wndLayoutGrid.GetRowCount();
		int nColCnt = m_wndLayoutGrid.GetColCount();

		for( i=1; i<=nRowCnt; i++ )
		{
			for( j=1; j<=nColCnt; j++ )
			{
				if( j==1 )
				{
					strData = m_wndLayoutGrid.GetValueRowCol(i, j);
				}
				else
				{
					strData = strData + "," + m_wndLayoutGrid.GetValueRowCol(i, j);
				}
			}
			file.WriteString(strData+"\n");
		}		
		file.Close();
	}
}
