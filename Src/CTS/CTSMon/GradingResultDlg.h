#if !defined(AFX_GRADINGRESULTDLG_H__144A559D_D9E9_4A5D_9F31_487705287A00__INCLUDED_)
#define AFX_GRADINGRESULTDLG_H__144A559D_D9E9_4A5D_9F31_487705287A00__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GradingResultDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGradingResultDlg dialog

#include "MyGridWnd.h"

class CFormResultFile;
class CCTSMonDoc;
class CGradingResultDlg : public CDialog
{
// Construction
public:
	void SetColorArray();
	void DrawColor();
	void InitColorConfig();
	BYTE GetColorIndex(BYTE byResult);
	void Display(int nStepNum, int nDisplayMode = 0);
	bool LoadResultData();

	void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);

	CGradingResultDlg(CCTSMonDoc *pDoc, CFormResultFile &rConditionMain, 
					  CWnd* pParent = NULL);   // standard constructor

	CMyGridWnd		m_wndModuleGrid;
	STR_CONDITION*	m_pConditionMain;
	CPtrArray*		m_aStepData;
	CString			m_strFileName;

	char*			m_pTopColorFlag;
	BYTE*			m_pStepType;
	BYTE**			m_ppResultData;
	int				m_nStepNum;
	int				m_nChNum;

	bool			m_bExpand;
	CRect			m_rectTotal;
	CRect			m_rectLine;
	COLORREF		m_colorFault;
	SECWellButton	m_BWellFaultColor;
	CString			m_strGradeCode[BACK_COLOR_NUM];
	COLORREF		m_BStateColor[BACK_COLOR_NUM];
	SECWellButton 	m_BWellColor[BACK_COLOR_NUM];
	CCTSMonDoc *m_pDoc;
// Dialog Data
	//{{AFX_DATA(CGradingResultDlg)
	enum { IDD = IDD_GRADING_RESULT_DLG };
	CComboBox	m_wndStepNo;
	CComboBox	m_wndGridSetting;
	int		m_nDisplayMode;
	int		m_nDirect;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradingResultDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGradingResultDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeStepNo();
	afx_msg void OnSelchangeGridSetting();
	afx_msg void OnModeAnd();
	afx_msg void OnModeOr();
	afx_msg void OnDirectL2R();
	afx_msg void OnDirectR2L();
	afx_msg void OnBtnExpand();
	afx_msg void OnBtnUpdate();
	afx_msg void OnPrintResult();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADINGRESULTDLG_H__144A559D_D9E9_4A5D_9F31_487705287A00__INCLUDED_)
