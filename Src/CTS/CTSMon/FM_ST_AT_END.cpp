#include "stdafx.h"
#include "CTSMon.h"
#include "FMS.h"
#include "FM_Unit.h"
#include "FM_STATE.h"
#include "MainFrm.h"

CFM_ST_AT_END::CFM_ST_AT_END(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
	m_preState = 0;
}


CFM_ST_AT_END::~CFM_ST_AT_END(void)
{
}

VOID CFM_ST_AT_END::fnEnter()
{	
	// m_Unit->fnGetFMS()->fnResultDataStateUpdate(NULL, RS_END);
	m_preState = 0;
	
	FMS_ERRORCODE _rec = FMS_ER_NONE;

// 	if(m_CalchkFlag == 1) //19-02-12 엄륭 MES 사양 추가 관련
// 	{
// 		fnSetFMSStateCode(FMS_ST_END);
// 		m_Unit->fnGetFMS()->fnSendToHost_S6F11C18( 1, 0 );
// 		m_CalchkFlag = 0;
// 	}
// 	else
	//{
	//	m_Unit->fnGetFMS()->fnSendToHost_S6F11C4( 1, 0 );
	//}
	
	// m_Unit->fnGetFMS()->fnSend_E_CHARGER_WORKEND(_rec);

	CHANGE_FNID(FNID_PROC);

	TRACE("CFM_ST_AT_END::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_END::fnProc()
{
	if(-1 == m_iWaitCount)
	{
		fnSetFMSStateCode(FMS_ST_END);
	}
	
	if(1 == m_iWaitCount)
	{
		if( m_preState == 0 )
		{
			fnSetFMSStateCode(FMS_ST_BLUE_END);
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_RED_END);
		}

// 		if( !m_Unit->fnGetModuleTrayState() )
// 		{
// 			fnSetFMSStateCode(FMS_ST_RED_END);
// 		}
// 		else
// 		{
// 			fnSetFMSStateCode(FMS_ST_BLUE_END);
// 		}

		// 1. 타이머를 다시 시작
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_AT_END::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_END::fnExit()
{	
	if(1 == m_iWaitCount)
	{
		if( !m_Unit->fnGetModuleTrayState() )
		{
			fnSetFMSStateCode(FMS_ST_RED_END);
			// CFMSLog::WriteLog("[Stage %d] 작업 공정이 완료된 트레이를 배출 후 출고 완료 신호를 받지 못한 Stage 가 발견되었습니다. 해당 Stage 를 초기화 후 FMS PC 에서 [작업조회] => [Rack 위치 제공 조회] 정보를 초기화 해주시기 바랍니다.",  m_Unit->fnGetModuleID() );
			CFMSLog::WriteLog("[Stage %s] 작업 공정이 완료된 트레이를 배출 후 출고 완료 신호를 받지 못한 Stage 가 발견되었습니다. 해당 Stage 를 초기화 후 FMS PC 에서 [작업조회] => [Rack 위치 제공 조회] 정보를 초기화 해주시기 바랍니다.",  m_Unit->fnGetModule()->GetModuleName() );			
			// CHANGE_STATE(AUTO_ST_VACANCY);
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_BLUE_END);
		}
		
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_AT_END::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_END::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

//	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
	if(m_Unit->fnGetModuleTrayState())
	{
		m_Unit->m_bPreTrayIn = true;
	}
	else
	{
		if(m_Unit->m_bPreTrayIn == true)
		{
			CFMSLog::WriteLog("[Stage %s] Tray Out",  m_Unit->fnGetModule()->GetModuleName() );			
			m_Unit->m_bPreTrayIn = false;
		}
	}

        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	}
//	TRACE("CFM_ST_AT_END::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_END::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 5 )
	{
		fnWaitInit();		

		st_S2F415 data;
		ZeroMemory(&data, sizeof(st_S2F415));
		memcpy(&data, _recvData->data, sizeof(st_S2F415));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		_error = FMS_ER_NONE;
		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( data.MRCD != 0 )
		{
			// 결과데이터 전송 실패 알람 팝업 창 표시
		}
		else
		{
			m_Unit->fnGetFMS()->fnSendToHost_S6F11C5( _recvData->head.nDevId, 0 );

			m_preState = 1;
		}		
	}
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 3 )
	{
		fnWaitInit();

		st_S2F413 data;
		ZeroMemory(&data, sizeof(st_S2F413));
		memcpy(&data, _recvData->data, sizeof(st_S2F413));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if( m_Unit->fnGetModuleTrayState() )
		{
			_error = ER_NO_Process;
		}
		else
		{
			_error = m_Unit->fnGetFMS()->SendSBCInitCommand();
		}

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
	}


	//결과 요청
	//결과 수신 완료
	//출고 완료
// 	switch(_msgId)
// 	{
// 
// 	case E_CHARGER_WORKEND_RESPONSE:
// 		{
// 			fnWaitInit();
// 			//black end
// 		}
// 		break;
// 	case E_CHARGER_RESULT://결과 요청 - 블루 앤드
// 		{
// 			fnWaitInit();
// 
// 			_error = ER_Data_Error;
// 
// 			if(sizeof(st_CHARGER_RESULT) == _recvData->dataLen)
// 			{
// 				st_CHARGER_RESULT data;
// 				memset(&data, 0x20, sizeof(st_CHARGER_RESULT));
// 				m_Unit->fnGetFMS()->strCutter(_recvData->data, &data
// 					, g_iCHARGER_RESULT_Map);
// 				_error = ER_TrayID;
// 				if(strcmp(m_Unit->fnGetFMS()->fnGetTrayID_1(), data.Tray_ID) == 0
// 					&& m_Unit->fnGetFMS()->fnGetLineNo() == atoi(data.Equipment_Num))
// 				{
// 					_error = FMS_ER_NONE;
// 				}
// 			}
// 		}
// 		break;
// 	case E_CHARGER_RESULT_RECV://결과 요청 확인 - 블루 앤드 초기화
// 		{
// 			fnWaitInit();
// 			fnSetFMSStateCode(FMS_ST_END);
// 			CHANGE_FNID(FNID_EXIT);
// 
// 			st_CHARGER_RESULT_RECV data;
// 			memset(&data, 0x20, sizeof(st_CHARGER_RESULT_RECV));
// 			m_Unit->fnGetFMS()->strCutter(_recvData->data, &data
// 				, g_iCHARGER_RESULT_RECV_Map);
// 
// 			data.Notify_Mode;
// 			data.Tray_ID;
// 
// 			_error = ER_TrayID;
// 			if(strcmp(m_Unit->fnGetFMS()->fnGetTrayID_1(), data.Tray_ID) == 0)
// 			{
// 				_error = FMS_ER_NONE;
// 
// 				if(data.Notify_Mode[0] == '0')
// 				{
// 					m_Unit->fnGetFMS()->fnResultDataStateUpdate(NULL, RS_REPORT);
// 				}
// 			}
// 		}
// 		break;
// 	case E_CHARGER_OUT:
// 		{	
// 			_error = FMS_ER_NONE;
// 			
// 			//if(m_Unit->fnGetModuleTrayState())
// 			//{
// 			//	_error = ER_Tray_In;
// 			//}
// 			
// 			//m_Unit->fnGetFMS()->SendInitCommand();			
// 		}
// 		break;
// 	default:
// 		{
// 		}
// 		break;
// 	}
	TRACE("CFM_ST_AT_END::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}