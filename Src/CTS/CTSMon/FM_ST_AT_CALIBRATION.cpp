#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"


CFM_ST_CA_CALI::CFM_ST_CA_CALI(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_CALI(_Eqstid, _stid, _unit)
{
}


CFM_ST_CA_CALI::~CFM_ST_CA_CALI(void)
{
}

VOID CFM_ST_CA_CALI::fnEnter()
{
//	fnSetFMSStateCode(FMS_ST_CALIBRATION);

//	m_Unit->fnGetState()->fnSetProcID(FNID_PROC);

	//m_Unit->fnGetFMS()->fnSendToHost_S6F11C17( 1, 0 );

	m_CalchkFlag = 1; //19-02-12 엄륭 MES 사양 추가 관련


	fnSetFMSStateCode(FMS_ST_CALIBRATION);

	m_Unit->fnGetState()->fnSetProcID(FNID_PROC);




	TRACE("CFM_ST_CA_CALIBRATION::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}


VOID CFM_ST_CA_CALI::fnProc()
{


	TRACE("CFM_ST_CA_CALIBRATION::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_CALI::fnExit()
{
	//공정 완료

	TRACE("CFM_ST_AT_CALIBRATION::fnExit %d \n", m_Unit->fnGetModuleIdx());
}


VOID CFM_ST_CA_CALI::fnSBCPorcess(WORD _state)
{
	CFM_ST_CALI::fnSBCPorcess(_state);


	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(CALI_ST_ERROR);
	}

	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(CALI_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(CALI_ST_TRAY_IN);
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(CALI_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(CALI_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(CALI_ST_ERROR);
		}
		break;
	}

	TRACE("CFM_ST_CA_CALI::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_CA_CALI::fnFMSPorcess(st_HSMS_PACKET* _recvData)  //KSH CALIBRATION 20190902 
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 8 )  //19-02-12 엄륭 MES 사양 추가
	{
		st_S2F418 data;
		ZeroMemory(&data, sizeof(st_S2F418));
		memcpy(&data, _recvData->data, sizeof(st_S2F418));

		//_error = m_Unit->fnGetFMS()->fnReserveProc();	KSH Calibration 20190830	
		_error = FMS_ER_NONE;  //KSH Calibration 20190830	


		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( _error == FMS_ER_NONE )
		{
			m_Unit->fnGetFMS()->fnSendToHost_S6F11C16(1, 0);
		}
	}

	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 9 )  //19-02-12 엄륭 MES 사양 추가
	{
		st_S2F419 data;
		ZeroMemory(&data, sizeof(st_S2F419));
		memcpy(&data, _recvData->data, sizeof(st_S2F419));
		BOOL bRunInfoErrChk = FALSE;

		_error = FMS_ER_NONE;  //KSH Calibration 20190830	

		//20190903 ksj
		CString trayID;
		data.TRAYID_1[6] = NULL;
		data.TRAYID_2[6] = NULL;
		trayID.Format("%s%s",data.TRAYID_1, data.TRAYID_2);
//		m_Unit->fnGetFMS()->fnSetTrayID(trayID);
	
		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( bRunInfoErrChk == FALSE )
		{
			_error = ER_Run_Fail;

			if(m_Unit->fnGetFMS()->fnCalRun())
			{
				_error = FMS_ER_NONE;

				//m_Unit->fnGetFMS()->fnSendToHost_S6F11C1( 1, 0 );
				m_Unit->fnGetFMS()->fnSendToHost_S6F11C17( 1, 0 );
			}
		}
	}

	TRACE("CFM_ST_AT_CALIBRATION::fnFMSPorcess %d ErrorCode %d \n", m_Unit->fnGetModuleIdx(), _error );

	return _error;


// 	FMS_ERRORCODE _error = ER_NO_Process;
// 
// 	if( _recvData->head.nStrm == 6 && _recvData->head.nFunc == 12 )
// 	{
// 		_error = FMS_ER_NONE;
// 	}
// 
// 	return _error;
}