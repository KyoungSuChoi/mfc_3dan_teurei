#pragma once

#include "afxwin.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"

#define MAX_CH_COUNT	72

// CSettingCheckView 폼 뷰입니다.

class CSbcCaliDataDlg : public CDialog
{
public:
	CSbcCaliDataDlg(CWnd* pParent , CCTSMonDoc* pDoc);           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CSbcCaliDataDlg();

public:
	enum { IDD = IDD_SBC_CALI_DATA_DLG };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:

	void initCtrl();
	void initGrid();
	void initLabel();
	void initFont();
	void initCombo();
	void initValue();
	void initGridNum();
	void ModuleConnected(int nModuleID);
	void ModuleDisConnected(int nModuleID);
	void SetCurrentModule(int nModuleID);
	void RecvData(int nModuleID);
	void UpdateAllData();
	void ClipboardChangeJustText();//클립보드내용을 문자만 남도록 변경
	CString GetTargetModuleName();
	

	CCTSMonDoc* m_pDoc;
	int m_nCurModuleID;
	int m_nMaxStageCnt;

	CMyGridWnd      m_grid;

	CLabel			m_LabelViewName;
	CLabel			m_CmdTarget;

	CFont			m_Font;

	bool			m_bSbcDataEmpty[MAX_CH_COUNT];

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnRefresh();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CComboBox m_comboStageID;
	afx_msg void OnCbnSelchangeComboStageChange();
	afx_msg void OnBnClickedBtnRefresh2();
	afx_msg void OnBnClickedBtnSaveExcel();
};

