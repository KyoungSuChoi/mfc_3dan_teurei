// WanningDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "WarnningDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWanningDlg dialog


CWanningDlg::CWanningDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWanningDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWanningDlg)
	m_strValue = _T("");
	//}}AFX_DATA_INIT	
}


void CWanningDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWanningDlg)
	DDX_Text(pDX, IDC_STATIC_WANNINGMESSAGE, m_strValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWanningDlg, CDialog)
	//{{AFX_MSG_MAP(CWanningDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWanningDlg message handlers

void CWanningDlg::OnOK() 
{
	// TODO: Add extra validation here		
	CDialog::OnOK();
}
