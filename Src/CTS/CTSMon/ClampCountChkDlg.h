#pragma once


// CClampCountChkDlg 대화 상자입니다.
#include "stdafx.h"
#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "afxcmn.h"
#include "afxwin.h"

#define GRID_DATA_FONT_SIZE	16
#define GRID_CH_ROW_CNT 2

class CClampCountChkDlg : public CDialog
{
	DECLARE_DYNAMIC(CClampCountChkDlg)

public:
	CClampCountChkDlg(CCTSMonDoc *pDoc,int nModuleID, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CClampCountChkDlg();
	CString *TEXT_LANG;
	bool LanguageinitMonConfig(CString strClassName);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CLAMP_CNTCHK_DLG };

	CMyGridWnd	m_wndChGrid;
	CMyGridWnd	m_wndUpsGrid;
	CCTSMonDoc	*m_pDoc;
	CFont		m_Font;
	

	

	CLabel	m_LabelPrecisionName;	
	CString m_strModuleName;
	int m_nMaxStageCnt;	
	int m_nModuleID;	
	
	void InitGridWnd();	
	void InitUPSGridWnd();	
	void ReDrawGrid();
	void InitFont();
	void InitLabel();
	void InitColorBtn();	
	void fnUpdateCnt();
	void fnUpdateUPSState();

	void fnUpdateGainRead();  //20191220엄륭 Gain 관련 추가
	void fninitGain();
	void fnProgressStep();

	BOOL InitEditCtrl();


	float round_(float fValue, int nDotCnt);	//반올림 20200131 ksj

	void UpdateView();
private:

	SECCurrencyEdit	m_UPKP1;
	SECCurrencyEdit	m_UPKP2;
	SECCurrencyEdit	m_UPKP3;
	SECCurrencyEdit	m_UPKP4;
	SECCurrencyEdit	m_UPKP5;

	SECCurrencyEdit	m_UPKI1;
	SECCurrencyEdit	m_UPKI2;
	SECCurrencyEdit	m_UPKI3;
	SECCurrencyEdit	m_UPKI4;
	SECCurrencyEdit	m_UPKI5;

	SECCurrencyEdit	m_DOWNKP1;
	SECCurrencyEdit	m_DOWNKP2;
	SECCurrencyEdit	m_DOWNKP3;
	SECCurrencyEdit	m_DOWNKP4;
	SECCurrencyEdit	m_DOWNKP5;

	SECCurrencyEdit	m_DOWNKI1;
	SECCurrencyEdit	m_DOWNKI2;
	SECCurrencyEdit	m_DOWNKI3;
	SECCurrencyEdit	m_DOWNKI4;
	SECCurrencyEdit	m_DOWNKI5;


	SECCurrencyEdit	m_VGAINKP1;
	SECCurrencyEdit	m_VGAINKP2;
	SECCurrencyEdit	m_VGAINKI1;
	SECCurrencyEdit	m_VGAINKI2;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnUpdate();
	afx_msg void OnBnClickedOk();	
	virtual BOOL OnInitDialog();
	CLabel m_LabelName;
	CLabel m_Ups_LabelName;

	CLabel m_Ups_Label1;
	CLabel m_Ups_Label2;
	CLabel m_Ups_Label3;
	CLabel m_Ups_Label4;
	CLabel m_Ups_Label5;
	CLabel m_Ups_Label6;
	CLabel m_Ups_Label7;
	CLabel m_Ups_Label8;

	//float	m_IGain_UpKP[5];
	float	m_IGain1[4];
	//float	m_IGain_UpKP2;
	//float	m_IGain_UpKP3;
	//float	m_IGain_UpKP4;
	//float	m_IGain_UpKP5;

	float	m_IGain2[4];
	//float	m_IGain_UpKI[5];
	//float	m_IGain_UpKI2;
	//float	m_IGain_UpKI3;
	//float	m_IGain_UpKI4;
	//float	m_IGain_UpKI5;

	float	m_IGain3[4];
	//float	m_IGain_DownKP[5];
	//float	m_IGain_DownKP2;
	//float	m_IGain_DownKP3;
	//float	m_IGain_DownKP4;
	//float	m_IGain_DownKP5;

	float	m_IGain4[4];
	//float	m_IGain_DownKI[5];
	//float	m_IGain_DownKI2;
	//float	m_IGain_DownKI3;
	//float	m_IGain_DownKI4;
	//float	m_IGain_DownKI5;

	float	m_IGain5[4];

	//float	m_VGain_KP1;
	//float	m_VGain_KP2;
	//float	m_VGain_KI1;
	//float	m_VGain_KI2;
	float	m_VGain1[2];
	float	m_VGain2[2];





	CLabel m_Gain_Label1;
	CLabel m_Gain_Label2;
	CLabel m_Gain_Label3;
	CLabel m_Gain_Label4;
	CLabel m_Gain_Label5;
	CLabel m_Gain_Label6;
	CLabel m_Gain_Label7;
	CLabel m_Gain_Label8;
	CLabel m_Gain_Label9;
	CLabel m_Gain_Label10;

	CLabel m_Gain_Label11;
	CLabel m_Gain_Label12;


	CColorButton2 m_BtnUpdate;
	CColorButton2 m_BtnUpdate2;
	CColorButton2 m_BtnClose;
	CColorButton2 m_BtnCSVConvert;

	CColorButton2 m_BtnGainRead;
	CColorButton2 m_BtnGainWrite;
	CColorButton2 m_BtnGainInit;
	CColorButton2 m_BtnGainUpdate;
	CComboBox m_ctrlBDSelCombo;
	CLabel m_Gain_LabelName;

	int m_nBdNum;

	float EditOnlyFloat(int nID, int nIntegerMax, int nDot)	;


	afx_msg void OnBnClickedCsvOutput();
	afx_msg void OnBnClickedSelectChannelInitBtn();
	CComboBox m_ctrlMDSelCombo;
	afx_msg void OnCbnSelchangeUnitSelCombo();
	CColorButton2 m_BtnSelectAllOk;
	CColorButton2 m_BtnSelectAllOff;
	CColorButton2 m_BtnSelectChannelInit;
	afx_msg void OnBnClickedSelectAllOkBtn();
	afx_msg void OnBnClickedSelectAllOffBtn();
	afx_msg void OnCbnSelchangeItemSelCombo();
	CComboBox m_ctrlItemSelCombo;
// 20210225 KSCHOI ClampCountViewer Grid Click Bug Fixed. START
//	afx_msg LONG OnGridClick(WPARAM wParam, LPARAM lParam);
// 20210225 KSCHOI ClampCountViewer Grid Click Bug Fixed. END
	afx_msg void OnBnClickedBtnUpdate2();
	afx_msg void OnBnClickedBtnGainRead();
	afx_msg void OnBnClickedBtnGainWrite();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtnGainInit();
	afx_msg void OnBnClickedBtnGainUpdate();
	afx_msg void OnCbnSelchangeBoardSelCombo();
	
	

	afx_msg void OnEnChangeEditUpKp1();
	afx_msg void OnEnChangeEditUpKi1();
	afx_msg void OnEnChangeEditUpKp2();
	afx_msg void OnEnChangeEditUpKi2();
	afx_msg void OnEnChangeEditUpKp3();
	afx_msg void OnEnChangeEditUpKi3();
	afx_msg void OnEnChangeEditUpKp4();
	afx_msg void OnEnChangeEditUpKi4();
	afx_msg void OnEnChangeEditUpKp5();
	afx_msg void OnEnChangeEditUpKi5();
	afx_msg void OnEnChangeEditDownKp1();
	afx_msg void OnEnChangeEditDownKi1();
	afx_msg void OnEnChangeEditDownKi2();
	afx_msg void OnEnChangeEditDownKp2();
	afx_msg void OnEnChangeEditDownKp3();
	afx_msg void OnEnChangeEditDownKi3();
	afx_msg void OnEnChangeEditDownKi4();
	afx_msg void OnEnChangeEditDownKp4();
	afx_msg void OnEnChangeEditDownKp5();
	afx_msg void OnEnChangeEditDownKi5();
};
