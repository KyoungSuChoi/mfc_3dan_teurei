#pragma once

#include "FMSCriticalSection.h"
#include "FMSSyncParent.h"
#include "FMSCircularQueue.h"

#define MAX_ARRAY	7

class CFMS_SM : public CFMSSyncParent<CFMS_SM>
{
public:
	CFMS_SM(void);
	virtual ~CFMS_SM(void);

	

	enum COMM_SEQ{ SEQ_FMS_NONE=0, SEQ_FMS_POWER_ON, SEQ_FMS_RUN,  SEQ_FMS_WAIT};

	HWND		m_NotifyHwnd;
	HANDLE		mKeepThreadHandle;
	HANDLE		mKeepThreadDestroyEvent;

	HANDLE		mObjDelete;
	DWORD		m_HeartBeatCheck;

	VOID		FMS_SENDThreadCallback(VOID);
	VOID		SetHeartbeatClear();
	BOOL		ChkHeartbeat();
	BOOL		fnSendHsmeCmd( st_HSMS_PACKET pack );
	
	BOOL		m_bRunChk;
	INT			m_nDeviceID;
	INT			fnGetDeviceID() { return m_nDeviceID; }
	INT			fnGetStageIdx( int nMachineID );
	INT			fnGetModuleIdx( int nModuleID );

	VOID		FMS_MAINProcessCallback(VOID);
	VOID		FMS_SMProcessCallback(VOID);

	BOOL		VALUE_CONVERTOR( CString &strToData, int nTarget );

	BOOL		fnBegin(HWND, CPtrArray&, int nDeviceID);
	BOOL		fnEnd();

	BOOL		fnSendContactResultData( INT moduleID );
	BOOL		fnChangeReserveProcPosition( INT moduleID );
	BOOL		fnTrayOutCMD( INT moduleID );
	BOOL		fnRunCMD( INT moduleID );  //2020 12 06 BJY 기존 선언만 되어있던 함수 구현
	BOOL		fnSetLocalEMG(INT nLaneNum, CString emgCode);
	VOID		fnSetLocalEMG_Reset();

	BOOL		fnCalbrationComplete( INT moduleID ); //20190927 KSJ

	BOOL		fnSetEMG(INT moduleID, CString emgCode, CString emgDescription);

	FMS_STATE_CODE fnGetFMSStateForAllSystemView(INT moduleID);//20190925 KSJ

	BOOL		fnClearReset(INT moduleID);
	BOOL		fnClearColor(INT moduleID);
	BOOL		fnTrayInStateReset(INT moduleID);
	BOOL		fnEndStateReset(INT moduleID);
	CFormModule* fnGetModuleInfo(CString moduleName);	//20190927 KSJ

	BOOL		fnInitStageState(INT moduleID);
	
	std::vector<CFM_Unit *> m_vUnit;
	BOOL		m_bMisSendWorking;
	INT			m_nMisWorkingUnitNum;

	COMM_SEQ	m_FmsSeq;	
	BOOL 		m_bSECSCommunicationOK;

private:
	CFMS		m_Not_Module_Fms;
private:
	HWND		m_Papa;
	CString		m_strMDRN;
	CString		m_strSOFTREV;
	BOOL		m_bWork;
private:

	BOOL		m_bTreadStop;
	INT			mTcpTheadIndex;
	std::vector<HANDLE> m_vUnitThread;
	std::vector<HANDLE> m_vUnitProcEvent;

	HANDLE		mFMS_StartProcessHandle;

	HANDLE		mFMSMainThreadHandle;
	HANDLE		mFMSMainThreadDestroyEvent;

	VOID		OnSecsEvent(short nEventId, long lParam);
	VOID		OnSecsMsg();	

	VOID		fnSendToHost_S1F2( short nDevId, long lSysByte );
	VOID		fnSendToHost_S1F6( short nDevId, long lSysByte );
	VOID		fnSendToHost_S1F13( short nDevId, long lSysByte );
	VOID		fnSendToHost_S1F14( short nDevId, long lSysByte );
	VOID		fnSendToHost_S1F102( short nDevId, long lSysByte, st_S1F101 stData );
	VOID		fnSendToHost_S2F17( short nDevId, long lSysByte );
	VOID		fnSendToHost_S2F32( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode );
	VOID		fnSendToHost_S5F102( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode );
	
private:
	CFMSCircularQueue<st_HSMS_PACKET> m_FMS_Q[MAX_SBC_COUNT];

private:

};
