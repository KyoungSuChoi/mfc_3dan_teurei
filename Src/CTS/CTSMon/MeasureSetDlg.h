#if !defined(AFX_MEASURESETDLG_H__C3572735_A7E1_4960_B965_A268208C96FD__INCLUDED_)
#define AFX_MEASURESETDLG_H__C3572735_A7E1_4960_B965_A268208C96FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MeasureSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMeasureSetDlg dialog

class CMeasureSetDlg : public CDialog
{
// Construction
public:
	void SetOption(BOOL bManual, UINT nInterval, UINT nCount, float fShunt);
	void SetPort(int nVPort, int nIPort);
	BOOL m_bAuto;
	CMeasureSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMeasureSetDlg)
	enum { IDD = IDD_MEASURE_SET_DLG };
	CString	m_strMeter;
	CString	m_strVPort;
	CString	m_strIPort;
	UINT	m_nInterval;
	UINT	m_nCount;
	float	m_fShunt;
	//}}AFX_DATA

	int m_nVPort;
	int m_nIPort;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMeasureSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMeasureSetDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEASURESETDLG_H__C3572735_A7E1_4960_B965_A268208C96FD__INCLUDED_)
