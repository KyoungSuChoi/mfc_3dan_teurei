// ClampCountChkDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ClampCountChkDlg.h"
#include "MainFrm.h"

// CClampCountChkDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CClampCountChkDlg, CDialog)

#define TIMER_GAIN_READ		100
#define TIMER_GAIN_WRITE	200


CClampCountChkDlg::CClampCountChkDlg(CCTSMonDoc *pDoc, int nModuleID, CWnd* pParent /*=NULL*/)
	: CDialog(CClampCountChkDlg::IDD, pParent)
{
	m_pDoc = pDoc;

	m_nModuleID = nModuleID;
	m_nMaxStageCnt = 0;

	//for(int i=0; i<4; i++)
	//{
	//	m_IGain1[i] = 0.0000;
	//	m_IGain2[i] = 0.0000;
	//	m_IGain3[i] = 0.0000;
	//	m_IGain4[i] = 0.0000;
	//	m_IGain5[i] = 0.0000;
	//}
	//m_VGain1[0] = 0.0000;
	//m_VGain1[1] = 0.0000;
	//m_VGain2[0] = 0.0000;
	//m_VGain2[1] = 0.0000;

	m_nBdNum = 0;

	LanguageinitMonConfig(_T("CClampCountChkDlg"));
}

CClampCountChkDlg::~CClampCountChkDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CClampCountChkDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}

	return true;
}

void CClampCountChkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);	
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelName);
	DDX_Control(pDX, ID_DLG_UPS_LABEL_NAME, m_Ups_LabelName);
	DDX_Control(pDX, IDC_BTN_UPDATE, m_BtnUpdate);
	DDX_Control(pDX, IDC_BTN_UPDATE2, m_BtnUpdate2);
	DDX_Control(pDX, IDOK, m_BtnClose);
	DDX_Control(pDX, IDC_CSV_OUTPUT, m_BtnCSVConvert);
	DDX_Control(pDX, IDC_UNIT_SEL_COMBO, m_ctrlMDSelCombo);
	DDX_Control(pDX, IDC_SELECT_ALL_OK_BTN, m_BtnSelectAllOk);
	DDX_Control(pDX, IDC_SELECT_ALL_OFF_BTN, m_BtnSelectAllOff);
	DDX_Control(pDX, IDC_SELECT_CHANNEL_INIT_BTN, m_BtnSelectChannelInit);
	DDX_Control(pDX, IDC_ITEM_SEL_COMBO, m_ctrlItemSelCombo);

	DDX_Control(pDX, ID_DLG_UPS_LABEL1, m_Ups_Label1);
	DDX_Control(pDX, ID_DLG_UPS_LABEL2, m_Ups_Label2);
	DDX_Control(pDX, ID_DLG_UPS_LABEL3, m_Ups_Label3);
	DDX_Control(pDX, ID_DLG_UPS_LABEL4, m_Ups_Label4);
	DDX_Control(pDX, ID_DLG_UPS_LABEL5, m_Ups_Label5);
	DDX_Control(pDX, ID_DLG_UPS_LABEL6, m_Ups_Label6);
	DDX_Control(pDX, ID_DLG_UPS_LABEL7, m_Ups_Label7);
	DDX_Control(pDX, ID_DLG_UPS_LABEL8, m_Ups_Label8);


	DDX_Control(pDX, IDC_BOARD_SEL_COMBO, m_ctrlBDSelCombo);
	DDX_Control(pDX, IDC_BTN_GAIN_READ, m_BtnGainRead);
	DDX_Control(pDX, IDC_BTN_GAIN_UPDATE, m_BtnGainUpdate);
	DDX_Control(pDX, IDC_BTN_GAIN_WRITE, m_BtnGainWrite);
	DDX_Control(pDX, IDC_BTN_GAIN_INIT, m_BtnGainInit);
	DDX_Control(pDX, ID_DLG_GAIN_LABEL_NAME, m_Gain_LabelName);

	DDX_Control(pDX, ID_DLG_GAIN_RANGE1, m_Gain_Label1);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE2, m_Gain_Label2);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE3, m_Gain_Label3);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE4, m_Gain_Label4);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE5, m_Gain_Label5);

	DDX_Control(pDX, ID_DLG_GAIN_RANGE6, m_Gain_Label6);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE7, m_Gain_Label7);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE8, m_Gain_Label8);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE9, m_Gain_Label9);
	DDX_Control(pDX, ID_DLG_GAIN_RANGE10, m_Gain_Label10);
	DDX_Control(pDX, ID_DLG_V_RESISTOR, m_Gain_Label11);
	DDX_Control(pDX, ID_DLG_V_BATTERY, m_Gain_Label12);

	DDX_Text(pDX, IDC_EDIT_UP_KP1, m_IGain1[0]);
	DDX_Text(pDX, IDC_EDIT_UP_KP2, m_IGain2[0]);
	DDX_Text(pDX, IDC_EDIT_UP_KP3, m_IGain3[0]);
	DDX_Text(pDX, IDC_EDIT_UP_KP4, m_IGain4[0]);
	DDX_Text(pDX, IDC_EDIT_UP_KP5, m_IGain5[0]);

	DDX_Text(pDX, IDC_EDIT_UP_KI1, m_IGain1[1]);
	DDX_Text(pDX, IDC_EDIT_UP_KI2, m_IGain2[1]);
	DDX_Text(pDX, IDC_EDIT_UP_KI3, m_IGain3[1]);
	DDX_Text(pDX, IDC_EDIT_UP_KI4, m_IGain4[1]);
	DDX_Text(pDX, IDC_EDIT_UP_KI5, m_IGain5[1]);


	DDX_Text(pDX, IDC_EDIT_DOWN_KP1, m_IGain1[2]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KP2, m_IGain2[2]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KP3, m_IGain3[2]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KP4, m_IGain4[2]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KP5, m_IGain5[2]);




	DDX_Text(pDX, IDC_EDIT_DOWN_KI1, m_IGain1[3]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KI2, m_IGain2[3]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KI3, m_IGain3[3]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KI4, m_IGain4[3]);
	DDX_Text(pDX, IDC_EDIT_DOWN_KI5, m_IGain5[3]);

	DDX_Text(pDX, IDC_EDIT_V_KP1, m_VGain1[0]);
	DDX_Text(pDX, IDC_EDIT_V_KP2, m_VGain2[0]);

	DDX_Text(pDX, IDC_EDIT_V_KI1, m_VGain1[1]);
	DDX_Text(pDX, IDC_EDIT_V_KI2, m_VGain2[1]);



}


BEGIN_MESSAGE_MAP(CClampCountChkDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_UPDATE, &CClampCountChkDlg::OnBnClickedBtnUpdate)
	ON_BN_CLICKED(IDOK, &CClampCountChkDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CSV_OUTPUT, &CClampCountChkDlg::OnBnClickedCsvOutput)
	ON_BN_CLICKED(IDC_SELECT_CHANNEL_INIT_BTN, &CClampCountChkDlg::OnBnClickedSelectChannelInitBtn)
	ON_CBN_SELCHANGE(IDC_UNIT_SEL_COMBO, &CClampCountChkDlg::OnCbnSelchangeUnitSelCombo)
	ON_BN_CLICKED(IDC_SELECT_ALL_OK_BTN, &CClampCountChkDlg::OnBnClickedSelectAllOkBtn)
	ON_BN_CLICKED(IDC_SELECT_ALL_OFF_BTN, &CClampCountChkDlg::OnBnClickedSelectAllOffBtn)
	ON_CBN_SELCHANGE(IDC_ITEM_SEL_COMBO, &CClampCountChkDlg::OnCbnSelchangeItemSelCombo)
// 20210225 KSCHOI ClampCountViewer Grid Click Bug Fixed. START
//	ON_MESSAGE(WM_GRID_CLICK, OnGridClick)
// 20210225 KSCHOI ClampCountViewer Grid Click Bug Fixed. END
	ON_BN_CLICKED(IDC_BTN_UPDATE2, &CClampCountChkDlg::OnBnClickedBtnUpdate2)
	ON_BN_CLICKED(IDC_BTN_GAIN_READ, &CClampCountChkDlg::OnBnClickedBtnGainRead)
	ON_BN_CLICKED(IDC_BTN_GAIN_WRITE, &CClampCountChkDlg::OnBnClickedBtnGainWrite)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_GAIN_INIT, &CClampCountChkDlg::OnBnClickedBtnGainInit)
	ON_BN_CLICKED(IDC_BTN_GAIN_UPDATE, &CClampCountChkDlg::OnBnClickedBtnGainUpdate)
	ON_CBN_SELCHANGE(IDC_BOARD_SEL_COMBO, &CClampCountChkDlg::OnCbnSelchangeBoardSelCombo)
	ON_EN_CHANGE(IDC_EDIT_UP_KP1, &CClampCountChkDlg::OnEnChangeEditUpKp1)
	ON_EN_CHANGE(IDC_EDIT_UP_KI1, &CClampCountChkDlg::OnEnChangeEditUpKi1)
	ON_EN_CHANGE(IDC_EDIT_UP_KP2, &CClampCountChkDlg::OnEnChangeEditUpKp2)
	ON_EN_CHANGE(IDC_EDIT_UP_KI2, &CClampCountChkDlg::OnEnChangeEditUpKi2)
	ON_EN_CHANGE(IDC_EDIT_UP_KP3, &CClampCountChkDlg::OnEnChangeEditUpKp3)
	ON_EN_CHANGE(IDC_EDIT_UP_KI3, &CClampCountChkDlg::OnEnChangeEditUpKi3)
	ON_EN_CHANGE(IDC_EDIT_UP_KP4, &CClampCountChkDlg::OnEnChangeEditUpKp4)
	ON_EN_CHANGE(IDC_EDIT_UP_KI4, &CClampCountChkDlg::OnEnChangeEditUpKi4)
	ON_EN_CHANGE(IDC_EDIT_UP_KP5, &CClampCountChkDlg::OnEnChangeEditUpKp5)
	ON_EN_CHANGE(IDC_EDIT_UP_KI5, &CClampCountChkDlg::OnEnChangeEditUpKi5)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KP1, &CClampCountChkDlg::OnEnChangeEditDownKp1)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KI1, &CClampCountChkDlg::OnEnChangeEditDownKi1)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KI2, &CClampCountChkDlg::OnEnChangeEditDownKi2)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KP2, &CClampCountChkDlg::OnEnChangeEditDownKp2)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KP3, &CClampCountChkDlg::OnEnChangeEditDownKp3)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KI3, &CClampCountChkDlg::OnEnChangeEditDownKi3)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KI4, &CClampCountChkDlg::OnEnChangeEditDownKi4)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KP4, &CClampCountChkDlg::OnEnChangeEditDownKp4)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KP5, &CClampCountChkDlg::OnEnChangeEditDownKp5)
	ON_EN_CHANGE(IDC_EDIT_DOWN_KI5, &CClampCountChkDlg::OnEnChangeEditDownKi5)
END_MESSAGE_MAP()

// 20210225 KSCHOI ClampCountViewer Grid Click Bug Fixed. START
// LONG CClampCountChkDlg::OnGridClick(WPARAM wParam, LPARAM lParam)
// {
// 	ROWCOL nRow, nCol;
// 	CString      str;
// 	
// 	nCol = LOWORD(wParam);
// 	nRow = HIWORD(wParam);
// 
// 	if( nCol == 5 && nRow > 0 )
// 	{
// 		if( m_wndChGrid.GetValueRowCol(nRow,nCol) == "1" )
// 		{
// 			m_wndChGrid.SetValueRange(CGXRange(nRow, nCol), "0" );
// 		}
// 		else
// 		{
// 			m_wndChGrid.SetValueRange(CGXRange(nRow, nCol), "1" );
// 		}
// 	}
// 
// 	return 0;
// }
// 20210225 KSCHOI ClampCountViewer Grid Click Bug Fixed. END

// CClampCountChkDlg 메시지 처리기입니다.
void CClampCountChkDlg::fnUpdateCnt()
{
	BeginWaitCursor();

	LPVOID	pBuffer = NULL;

	CString strData = _T("");

	int i=0;
	int nSize = 0;

	nSize = sizeof( EP_GRIPPER_CLAMPING_RES );

	pBuffer = EPSendDataCmd( m_nModuleID, 0, 0, EP_CMD_GRIPPER_CLAMPING_REQUEST, nSize );

	if( pBuffer != NULL )
	{
		EP_GRIPPER_CLAMPING_RES recvData;
		ZeroMemory( &recvData, sizeof(EP_GRIPPER_CLAMPING_RES));
		memcpy(&recvData, pBuffer, sizeof(EP_GRIPPER_CLAMPING_RES));


		int rowCnt = m_wndChGrid.GetRowCount();
		for(i=0; i<m_wndChGrid.GetRowCount()*GRID_CH_ROW_CNT; i++ )
		{	
			strData.Format("%d", recvData.clampingCnt[i]);
			m_wndChGrid.SetValueRange(CGXRange((i%rowCnt) +1,GRID_CH_ROW_CNT+1+(i/rowCnt)), strData);
			strData.Format("%d", recvData.channelErrCnt_Cumulative[i]);
			m_wndChGrid.SetValueRange(CGXRange((i%rowCnt) +1,GRID_CH_ROW_CNT*2+1+(i/rowCnt)), strData);
			strData.Format("%d", recvData.channelErrCnt_Continue[i]);
			m_wndChGrid.SetValueRange(CGXRange((i%rowCnt) +1,GRID_CH_ROW_CNT*3+1+(i/rowCnt)), strData);
		}		
	}

	EndWaitCursor();
}


void CClampCountChkDlg::fnUpdateUPSState()
{
	BeginWaitCursor();

	LPVOID	pBuffer = NULL;

	CString strData = _T("");
	CString strVol, strFVol, strOpVol, strOpCur, strIpfrequnce,strBVol, strTemp, strBit;
	CString strUf_Bit,strBl_Bit,strBy_Bit,strUpsF_Bit,strUpsT_Bit,strTP_Bit,strShA_Bit,strBeO_Bit;
	CString strLog;

	int i=0;
	int nSize = 0;

	nSize = sizeof( EP_MD_UPS_STATE );

	pBuffer = EPSendDataCmd( m_nModuleID, 0, 0, EP_CMD_UPS_STATE, nSize );


	

	if( pBuffer != NULL )
	{
		strLog.Format("Module ID %d - DataRecv OK",m_nModuleID);//TEXT - %s 는 초기화 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.	
		m_pDoc->WriteLog((LPSTR)(LPCTSTR)strLog);

		EP_MD_UPS_STATE recvData;
		ZeroMemory( &recvData, sizeof(EP_MD_UPS_STATE));


		memcpy(&recvData, pBuffer, sizeof(EP_MD_UPS_STATE));

		strData.Format("%s", &recvData.state);

		// 208.4 140.0 208.4 034 59.9 2.05 35.0 00110000
		//00100011

		AfxExtractSubString(strVol,strData,0,' ');
		AfxExtractSubString(strFVol,strData,1,' ');
		AfxExtractSubString(strOpVol,strData,2,' ');
		AfxExtractSubString(strOpCur,strData,3,' ');
		AfxExtractSubString(strIpfrequnce,strData,4,' ');
		AfxExtractSubString(strBVol,strData,5,' ');
		AfxExtractSubString(strTemp,strData,6,' ');
		AfxExtractSubString(strBit,strData,7,' ');




		strUf_Bit = strBit.Mid(0,1);
		strBl_Bit = strBit.Mid(1,1);
		strBy_Bit = strBit.Mid(2,1);
		strUpsF_Bit = strBit.Mid(3,1);
		strUpsT_Bit = strBit.Mid(4,1);
		strTP_Bit = strBit.Mid(7,1);
		strShA_Bit = strBit.Mid(6,1);
		strBeO_Bit = strBit.Mid(7,1);


		
		
		m_wndUpsGrid.SetValueRange(CGXRange(1,2), strVol);
		m_wndUpsGrid.SetValueRange(CGXRange(2,2), strFVol);
		m_wndUpsGrid.SetValueRange(CGXRange(3,2), strOpVol);
		m_wndUpsGrid.SetValueRange(CGXRange(4,2), strOpCur);
		m_wndUpsGrid.SetValueRange(CGXRange(5,2), strIpfrequnce);
		m_wndUpsGrid.SetValueRange(CGXRange(6,2), strBVol);
		m_wndUpsGrid.SetValueRange(CGXRange(7,2), strTemp);	

		if(strBeO_Bit == "1")
		{
			m_Ups_Label1.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Label1.SetBkColor(RGB_WHITE);
		}

		if(strShA_Bit == "1")
		{
			m_Ups_Label2.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Label2.SetBkColor(RGB_WHITE);
		}

		if(strUpsT_Bit == "2"|| strUpsT_Bit =="3")
		{
			m_Ups_Label4.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Label4.SetBkColor(RGB_WHITE);
		}



		/*if(strBeO_Bit == "1")
		{
			m_Ups_Bit1.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit1.SetBkColor(RGB_WHITE);
		}

		if(strShA_Bit == "1")
		{
			m_Ups_Bit2.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit2.SetBkColor(RGB_WHITE);
		}

		if(strTP_Bit == "1")
		{
			m_Ups_Bit3.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit3.SetBkColor(RGB_WHITE);
		}

		if(strUpsT_Bit == "2"|| strUpsT_Bit =="3")
		{
			m_Ups_Bit4.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit4.SetBkColor(RGB_WHITE);
		}

		if(strUpsF_Bit == "1")
		{
			m_Ups_Bit5.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit5.SetBkColor(RGB_WHITE);
		}

		if(strBy_Bit == "1")
		{
			m_Ups_Bit6.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit6.SetBkColor(RGB_WHITE);
		}

		if(strBl_Bit == "1")
		{
			m_Ups_Bit7.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit7.SetBkColor(RGB_WHITE);
		}

		if(strUf_Bit == "1")
		{
			m_Ups_Bit8.SetBkColor(RGB_RED);
		}
		else
		{
			m_Ups_Bit8.SetBkColor(RGB_WHITE);
		}*/


	}

	strLog.Format("Module ID %d - DataRecv Fail",m_nModuleID);//TEXT - %s 는 초기화 명령을 전송할 수 없는 상태이므로 전송하지 않았습니다.	
	m_pDoc->WriteLog((LPSTR)(LPCTSTR)strLog);

	EndWaitCursor();
}

void CClampCountChkDlg::OnBnClickedBtnUpdate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	fnUpdateCnt();
}

void CClampCountChkDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
	OnOK();
}

BOOL CClampCountChkDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	CenterWindow();

	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();

	InitFont();
	InitLabel();
	InitGridWnd();
	InitUPSGridWnd();
	InitColorBtn();
	fninitGain();
	InitEditCtrl();

	int i=0;
	int nModuleID = 1;	
	int nDefaultIndex = 0;

	for( i=0; i<m_nMaxStageCnt; i++ )
	{
		nModuleID = EPGetModuleID(i);
		if( nModuleID == m_nModuleID )
		{
			nDefaultIndex = i;
		}
		m_ctrlMDSelCombo.AddString(::GetModuleName(nModuleID));
		m_ctrlMDSelCombo.SetItemData(i, nModuleID);
	}

	m_ctrlItemSelCombo.SetCurSel(0);
	m_ctrlMDSelCombo.SetCurSel(nDefaultIndex);	
	
	CString strTemp;

	for( int a=0; a<9; a++ ) //20191230 엄륭 Gain 관련 보드 선택 콤보 추가
	{
		strTemp.Format("Board %d",a+1);
		m_ctrlBDSelCombo.AddString(strTemp);
		m_ctrlBDSelCombo.SetItemData(a, nModuleID);
	}

	m_ctrlBDSelCombo.SetCurSel(0);	


	ReDrawGrid();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CClampCountChkDlg::InitFont()
{	
	LOGFONT LogFont;
	

	GetDlgItem(IDC_STATIC1)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	GetDlgItem(IDC_STATIC2)->SetFont(&m_Font);	
	LogFont.lfHeight = 18;

	
	m_Font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC1)->SetFont(&m_Font);
	GetDlgItem(IDC_UNIT_SEL_COMBO)->SetFont(&m_Font);
	GetDlgItem(IDC_ITEM_SEL_COMBO)->SetFont(&m_Font);

	GetDlgItem(IDC_BOARD_SEL_COMBO)->SetFont(&m_Font);

	GetDlgItem(IDC_EDIT_UP_KP1)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KP2)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KP3)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KP4)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KP5)->SetFont(&m_Font);

	GetDlgItem(IDC_EDIT_DOWN_KP1)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KP2)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KP3)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KP4)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KP5)->SetFont(&m_Font);

	GetDlgItem(IDC_EDIT_UP_KI1)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KI2)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KI3)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KI4)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_UP_KI5)->SetFont(&m_Font);

	GetDlgItem(IDC_EDIT_DOWN_KI1)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KI2)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KI3)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KI4)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_DOWN_KI5)->SetFont(&m_Font);
	
	GetDlgItem(IDC_EDIT_V_KP1)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_V_KP2)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_V_KI1)->SetFont(&m_Font);
	GetDlgItem(IDC_EDIT_V_KI2)->SetFont(&m_Font);

}

void CClampCountChkDlg::InitLabel()
{
	m_LabelName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelName.SetTextColor(RGB_WHITE);
	m_LabelName.SetFontSize(24);
	m_LabelName.SetFontBold(TRUE);
	m_LabelName.SetText(TEXT_LANG[12]);//"CLAMP Count Viewer"

	m_Ups_LabelName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Ups_LabelName.SetTextColor(RGB_WHITE);
	m_Ups_LabelName.SetFontSize(24);
	m_Ups_LabelName.SetFontBold(TRUE);

	m_Gain_LabelName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_LabelName.SetTextColor(RGB_WHITE);
	m_Gain_LabelName.SetFontSize(24);
	m_Gain_LabelName.SetFontBold(TRUE);
	//m_UpsLabelName.SetText(TEXT_LANG[12]);//"CLAMP Count Viewer"

	m_Ups_Label1.SetBkColor(RGB_WHITE);
	m_Ups_Label1.SetTextColor(RGB_BLACK);
	m_Ups_Label1.SetFontSize(12);
	m_Ups_Label1.SetFontBold(TRUE);
	m_Ups_Label1.SetFontAlign(0);

	m_Ups_Label2.SetBkColor(RGB_WHITE);
	m_Ups_Label2.SetTextColor(RGB_BLACK);
	m_Ups_Label2.SetFontSize(12);
	m_Ups_Label2.SetFontBold(TRUE);
	m_Ups_Label2.SetFontAlign(0);

	m_Ups_Label3.SetBkColor(RGB_WHITE);
	m_Ups_Label3.SetTextColor(RGB_BLACK);
	m_Ups_Label3.SetFontSize(12);
	m_Ups_Label3.SetFontBold(TRUE);
	m_Ups_Label3.SetFontAlign(0);

	m_Ups_Label4.SetBkColor(RGB_WHITE);
	m_Ups_Label4.SetTextColor(RGB_BLACK);
	m_Ups_Label4.SetFontSize(12);
	m_Ups_Label4.SetFontBold(TRUE);
	m_Ups_Label4.SetFontAlign(0);

	m_Ups_Label5.SetBkColor(RGB_WHITE);
	m_Ups_Label5.SetTextColor(RGB_BLACK);
	m_Ups_Label5.SetFontSize(12);
	m_Ups_Label5.SetFontBold(TRUE);
	m_Ups_Label5.SetFontAlign(0);

	m_Ups_Label6.SetBkColor(RGB_WHITE);
	m_Ups_Label6.SetTextColor(RGB_BLACK);
	m_Ups_Label6.SetFontSize(12);
	m_Ups_Label6.SetFontBold(TRUE);
	m_Ups_Label6.SetFontAlign(0);

	m_Ups_Label7.SetBkColor(RGB_WHITE);
	m_Ups_Label7.SetTextColor(RGB_BLACK);
	m_Ups_Label7.SetFontSize(12);
	m_Ups_Label7.SetFontBold(TRUE);
	m_Ups_Label7.SetFontAlign(0);

	m_Ups_Label8.SetBkColor(RGB_WHITE);
	m_Ups_Label8.SetTextColor(RGB_BLACK);
	m_Ups_Label8.SetFontSize(12);
	m_Ups_Label8.SetFontBold(TRUE);
	m_Ups_Label8.SetFontAlign(0);


	m_Gain_Label1.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label1.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label1.SetFontSize(12);
	m_Gain_Label1.SetFontBold(TRUE);
	m_Gain_Label1.SetFontAlign(0);


	m_Gain_Label2.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label2.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label2.SetFontSize(12);
	m_Gain_Label2.SetFontBold(TRUE);
	m_Gain_Label2.SetFontAlign(0);



	m_Gain_Label3.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label3.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label3.SetFontSize(12);
	m_Gain_Label3.SetFontBold(TRUE);
	m_Gain_Label3.SetFontAlign(0);



	m_Gain_Label4.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label4.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label4.SetFontSize(12);
	m_Gain_Label4.SetFontBold(TRUE);
	m_Gain_Label4.SetFontAlign(0);


	m_Gain_Label5.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label5.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label5.SetFontSize(12);
	m_Gain_Label5.SetFontBold(TRUE);
	m_Gain_Label5.SetFontAlign(0);


	m_Gain_Label6.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label6.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label6.SetFontSize(12);
	m_Gain_Label6.SetFontBold(TRUE);
	m_Gain_Label6.SetFontAlign(0);

	m_Gain_Label7.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label7.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label7.SetFontSize(12);
	m_Gain_Label7.SetFontBold(TRUE);
	m_Gain_Label7.SetFontAlign(0);


	m_Gain_Label8.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label8.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label8.SetFontSize(12);
	m_Gain_Label8.SetFontBold(TRUE);
	m_Gain_Label8.SetFontAlign(0);


	m_Gain_Label9.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label9.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label9.SetFontSize(12);
	m_Gain_Label9.SetFontBold(TRUE);
	m_Gain_Label9.SetFontAlign(0);


	m_Gain_Label10.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label10.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label10.SetFontSize(12);
	m_Gain_Label10.SetFontBold(TRUE);
	m_Gain_Label10.SetFontAlign(0);

	m_Gain_Label11.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label11.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label11.SetFontSize(12);
	m_Gain_Label11.SetFontBold(TRUE);
	m_Gain_Label11.SetFontAlign(0);

	m_Gain_Label12.SetBkColor(RGB_MIDNIGHTBLUE);
	m_Gain_Label12.SetTextColor(RGB_LABEL_FONT_STAGENAME);
	m_Gain_Label12.SetFontSize(12);
	m_Gain_Label12.SetFontBold(TRUE);
	m_Gain_Label12.SetFontAlign(0);

	

}

void CClampCountChkDlg::InitColorBtn()
{	
	if(g_nLanguage == 0)//korean
	{
		m_BtnUpdate.SetFontStyle(20, 1);
		m_BtnUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnUpdate2.SetFontStyle(20, 1);
		m_BtnUpdate2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnSelectChannelInit.SetFontStyle(20, 1);
		m_BtnSelectChannelInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
		m_BtnCSVConvert.SetFontStyle(20, 1);
		m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	

		m_BtnGainRead.SetFontStyle(20, 1);
		m_BtnGainRead.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainWrite.SetFontStyle(20, 1);
		m_BtnGainWrite.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainInit.SetFontStyle(20, 1);
		m_BtnGainInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainUpdate.SetFontStyle(20, 1);
		m_BtnGainUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	}
	else if(g_nLanguage == 1)//english
	{
		m_BtnUpdate.SetFontStyle(17, 1);
		m_BtnUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnUpdate2.SetFontStyle(17, 1);
		m_BtnUpdate2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnSelectChannelInit.SetFontStyle(16, 1);
		m_BtnSelectChannelInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
		m_BtnCSVConvert.SetFontStyle(16, 1);
		m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
		m_BtnGainRead.SetFontStyle(17, 1);
		m_BtnGainRead.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainWrite.SetFontStyle(17, 1);
		m_BtnGainWrite.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainInit.SetFontStyle(17, 1);
		m_BtnGainInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainUpdate.SetFontStyle(17, 1);
		m_BtnGainUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	}
	else if(g_nLanguage == 2)//chinese
	{
		m_BtnUpdate.SetFontStyle(20, 1);
		m_BtnUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnUpdate2.SetFontStyle(20, 1);
		m_BtnUpdate2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnSelectChannelInit.SetFontStyle(20, 1);
		m_BtnSelectChannelInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
		m_BtnCSVConvert.SetFontStyle(20, 1);
		m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
		m_BtnGainRead.SetFontStyle(20, 1);
		m_BtnGainRead.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainWrite.SetFontStyle(20, 1);
		m_BtnGainWrite.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainInit.SetFontStyle(20, 1);
		m_BtnGainInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainUpdate.SetFontStyle(20, 1);
		m_BtnGainUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	}
	else if(g_nLanguage == 3)//hungarian
	{
		m_BtnUpdate.SetFontStyle(17, 1);
		m_BtnUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnUpdate2.SetFontStyle(17, 1);
		m_BtnUpdate2.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnSelectChannelInit.SetFontStyle(16, 1);
		m_BtnSelectChannelInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
		m_BtnCSVConvert.SetFontStyle(16, 1);
		m_BtnCSVConvert.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
		m_BtnGainRead.SetFontStyle(16, 1);
		m_BtnGainRead.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainWrite.SetFontStyle(16, 1);
		m_BtnGainWrite.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainInit.SetFontStyle(16, 1);
		m_BtnGainInit.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
		m_BtnGainUpdate.SetFontStyle(16, 1);
		m_BtnGainUpdate.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	}
	m_BtnSelectAllOk.SetFontStyle(20, 1);
	m_BtnSelectAllOk.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_BtnSelectAllOff.SetFontStyle(20, 1);
	m_BtnSelectAllOff.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	m_BtnClose.SetFontStyle(20, 1);
	m_BtnClose.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
}


void CClampCountChkDlg::InitGridWnd()
{
	ASSERT(m_pDoc);

	CRect rectGrid;
	float width;

	m_wndChGrid.SubclassDlgItem(IDC_CHNNEL_GRID, this);
	m_wndChGrid.m_bSameColSize  = FALSE;		
	m_wndChGrid.m_bSameRowSize  = TRUE;
	m_wndChGrid.m_bCustomWidth  = TRUE;

	m_wndChGrid.Initialize();
	
	BOOL bLock = m_wndChGrid.LockUpdate();

	m_wndChGrid.SetRowCount(0);
	m_wndChGrid.SetColCount(5 * GRID_CH_ROW_CNT);
	m_wndChGrid.SetDefaultRowHeight(24);

	m_wndChGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	//20190927KSJ
	for(int i=0; i<5; i++)
	{
		m_wndChGrid.SetCoveredCellsRowCol(0, (GRID_CH_ROW_CNT*i)+1, 0, GRID_CH_ROW_CNT*(i+1));
	}

	m_wndChGrid.SetValueRange(CGXRange(0,(GRID_CH_ROW_CNT*0)+1	), TEXT_LANG[0]);//"Channel"
	m_wndChGrid.SetValueRange(CGXRange(0,(GRID_CH_ROW_CNT*1)+1	), TEXT_LANG[1]);//"Clamp 횟수"	
	m_wndChGrid.SetValueRange(CGXRange(0,(GRID_CH_ROW_CNT*2)+1	), TEXT_LANG[2]);//"누적채널에러"
	m_wndChGrid.SetValueRange(CGXRange(0,(GRID_CH_ROW_CNT*3)+1	), TEXT_LANG[3]);//"연속채널에러"
	m_wndChGrid.SetValueRange(CGXRange(0,(GRID_CH_ROW_CNT*4)+1	), TEXT_LANG[4]);//"Select"	

	m_wndChGrid.GetClientRect(rectGrid);
	width = (float)rectGrid.Width()/100.0f;


	for(int i=0; i<GRID_CH_ROW_CNT; i++)
	{
		m_wndChGrid.m_nWidth[(GRID_CH_ROW_CNT*0)+1+i]	= int(width* 20.0f / GRID_CH_ROW_CNT);
		m_wndChGrid.m_nWidth[(GRID_CH_ROW_CNT*1)+1+i]	= int(width* 20.0f / GRID_CH_ROW_CNT);
		m_wndChGrid.m_nWidth[(GRID_CH_ROW_CNT*2)+1+i]	= int(width* 20.0f / GRID_CH_ROW_CNT);
		m_wndChGrid.m_nWidth[(GRID_CH_ROW_CNT*3)+1+i]	= int(width* 20.0f / GRID_CH_ROW_CNT);
		m_wndChGrid.m_nWidth[(GRID_CH_ROW_CNT*4)+1+i]	= int(width* 20.0f / GRID_CH_ROW_CNT);
	}

	m_wndChGrid.SetStyleRange(CGXRange().SetCols((GRID_CH_ROW_CNT*4)+1, GRID_CH_ROW_CNT*5), 
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
		);

	m_wndChGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_wndChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndChGrid.SetStyleRange(CGXRange().SetCols(1,GRID_CH_ROW_CNT),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
	//Table setting
	m_wndChGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(14).SetBold(TRUE).SetFaceName(TEXT_LANG[13])));

	m_wndChGrid.LockUpdate(bLock);
	m_wndChGrid.Redraw();
}



void CClampCountChkDlg::InitUPSGridWnd()
{
	ASSERT(m_pDoc);

	CRect rectGrid;
	float width;

	m_wndUpsGrid.SubclassDlgItem(IDC_UPS_GRID, this);
	m_wndUpsGrid.m_bSameColSize  = FALSE;		
	m_wndUpsGrid.m_bSameRowSize  = TRUE;
	m_wndUpsGrid.m_bCustomWidth  = TRUE;

	m_wndUpsGrid.Initialize();

	BOOL bLock = m_wndUpsGrid.LockUpdate();

	m_wndUpsGrid.SetRowCount(7);
	m_wndUpsGrid.SetColCount(2);
	m_wndUpsGrid.SetDefaultRowHeight(24);

	m_wndUpsGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	m_wndUpsGrid.SetValueRange(CGXRange(0,1), "STATUS");
	m_wndUpsGrid.SetValueRange(CGXRange(0,2), "DATA");

	m_wndUpsGrid.SetValueRange(CGXRange(1,1), "Voltage in 208.4V");
	m_wndUpsGrid.SetValueRange(CGXRange(2,1), "I/P fault voltage is 140.0V");
	m_wndUpsGrid.SetValueRange(CGXRange(3,1), "O/P voltage is 208.4V");
	m_wndUpsGrid.SetValueRange(CGXRange(4,1), "O/P current is 34%");
	m_wndUpsGrid.SetValueRange(CGXRange(5,1), "I/P frequence is 59.9HZ");
	m_wndUpsGrid.SetValueRange(CGXRange(6,1), "Battery voltage is 2.05V");
	m_wndUpsGrid.SetValueRange(CGXRange(7,1), "Temperature is 35.0");


	



	m_wndUpsGrid.GetClientRect(rectGrid);
	width = (float)rectGrid.Width()/100.0f;

	m_wndUpsGrid.m_nWidth[1]	= int(width* 55.0f);
	m_wndUpsGrid.m_nWidth[2]	= int(width* 20.0f);


	/*m_wndUpsGrid.SetStyleRange(CGXRange().SetCols(2), 
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
		);*/

	m_wndUpsGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	m_wndUpsGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndUpsGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
	//Table setting
	m_wndUpsGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(12).SetBold(TRUE).SetFaceName(TEXT_LANG[13])));

	m_wndUpsGrid.LockUpdate(bLock);
	m_wndUpsGrid.Redraw();
}

void CClampCountChkDlg::ReDrawGrid()
{
	CFormModule *pModule = m_pDoc->GetModuleInfo(m_nModuleID);	
	if(pModule == NULL)
	{
		return;
	}

	int nTrayChSum = 0;
	int t=0;
	for(t=0; t<pModule->GetTotalJig(); t++)
	{
		nTrayChSum += pModule->GetChInJig(t);
	}

	if(nTrayChSum <=0)	
	{
		return;
	}

	int i = 0;

	BOOL bLock = m_wndChGrid.LockUpdate();

	for( i=1; i<=m_wndChGrid.GetRowCount(); i++ )
	{
		m_wndChGrid.SetValueRange(CGXRange(i, 2), "");
		m_wndChGrid.SetValueRange(CGXRange(i, 3), "");
		m_wndChGrid.SetValueRange(CGXRange(i, 4), "");
		m_wndChGrid.SetValueRange(CGXRange(i, 5), "");
	}

	if(m_wndChGrid.GetRowCount() != nTrayChSum)
	{
		m_wndChGrid.SetRowCount(nTrayChSum/GRID_CH_ROW_CNT);
		for( i=0; i<m_wndChGrid.GetRowCount()*GRID_CH_ROW_CNT; i++ )
		{
			m_wndChGrid.SetValueRange( CGXRange((i%m_wndChGrid.GetRowCount()) +1, 1 + i/m_wndChGrid.GetRowCount()),   (long)i+1);
		}
	}

	m_wndChGrid.LockUpdate(bLock);
	m_wndChGrid.Redraw();
}

void CClampCountChkDlg::OnBnClickedCsvOutput()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTime, strData;	

	COleDateTime nowtime(COleDateTime::GetCurrentTime());
	
	strTime.Format("CLAMPINFO_STAGE%d_%d%02d%02d", m_nModuleID, nowtime.GetYear(), nowtime.GetMonth(), nowtime.GetDay());	

	CStdioFile	file;
	CString		strFileName;		

	int i=0;
	int j=0;

	CFileDialog dlg(false, "", strTime, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[5]);//"csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|"

	if(dlg.DoModal() == IDOK)
	{		
		strFileName = dlg.GetPathName();		
		file.Open(strFileName, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate);

		int nRowCnt = m_wndChGrid.GetRowCount();
		int nColCnt = m_wndChGrid.GetColCount();

		for( i=0; i<=nRowCnt; i++ )
		{
			for( j=1; j<nColCnt; j++ )
			{
				if( j==1 )
				{
					strData = m_wndChGrid.GetValueRowCol(i, j);
				}
				else
				{
					strData = strData + "," + m_wndChGrid.GetValueRowCol(i, j);
				}
			}
			file.WriteString(strData+"\n");
		}		
		file.Close();
	}
}

void CClampCountChkDlg::OnBnClickedSelectChannelInitBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTemp=_T("");
	int i=0;
	int nRtn=0;
	int nCnt=0;

	EP_GRIPPER_CLAMPING_INIT cmd;
	ZeroMemory(&cmd, sizeof(EP_GRIPPER_CLAMPING_INIT));

	int nCurSel = m_ctrlItemSelCombo.GetCurSel();
	if( nCurSel == LB_ERR)
	{
		return;
	}

	cmd.lCmdType = nCurSel+1;


	int rowCnt =  m_wndChGrid.GetRowCount();
	for( i=0; i<rowCnt*GRID_CH_ROW_CNT; i++ )
	{
		if( m_wndChGrid.GetValueRowCol((i%rowCnt) +1,(GRID_CH_ROW_CNT*4)+1+(i/rowCnt)) == "1" )
		{
			nCnt++;
			cmd.set_ch[i] = 1;
		}
	}

	switch( cmd.lCmdType )
	{
	case 1:
		strTemp.Format(TEXT_LANG[6], nCnt); //"선택된 채널의 Gripper Clamping 누적 횟수를 초기화 하시겠습니까?\n [총: %d]"
		break;
	case 2:
		strTemp.Format(TEXT_LANG[7], nCnt); //"선택된 채널의 에러 횟수를 초기화 하시겠습니까?\n [총: %d]"
		break;
	case 3:
		strTemp.Format(TEXT_LANG[8], nCnt); //"선택된 채널의 전체정보를 초기화 하시겠습니까?\n [총: %d]"
		break;
	}	

	if( MessageBox(strTemp,_T(TEXT_LANG[9]),MB_ICONQUESTION|MB_YESNO) == IDYES ) //"초기화 확인"
	{	
		nRtn = EPSendCommand( m_nModuleID, 0, 0, EP_CMD_SET_GRIPPER_CLAMPING_INIT, &cmd, sizeof(EP_GRIPPER_CLAMPING_INIT) );
		if( nRtn !=  EP_ACK)
		{	
			strTemp.Format(_T(TEXT_LANG[10]));//"명령 전송을 실패 하였습니다."
			((CMainFrame *)AfxGetMainWnd())->ShowInformation( m_nModuleID, strTemp, INFO_TYPE_FAULT);
		}
		else
		{
			strTemp.Format(_T(TEXT_LANG[11]));//"명령을 전송 하였습니다."
			((CMainFrame *)AfxGetMainWnd())->ShowInformation( m_nModuleID, strTemp, INFO_TYPE_NORMAL);

			fnUpdateCnt();
		}
	}
}

void CClampCountChkDlg::OnCbnSelchangeUnitSelCombo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCurSel = m_ctrlMDSelCombo.GetCurSel();
	if( nCurSel == LB_ERR )
	{
		return;
	}

	if( m_nModuleID != m_ctrlMDSelCombo.GetItemData(nCurSel) )
	{
		m_nModuleID = m_ctrlMDSelCombo.GetItemData(nCurSel);
		ReDrawGrid();
	}
}

void CClampCountChkDlg::OnBnClickedSelectAllOkBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int i=0;
	for( i=0; i<m_wndChGrid.GetRowCount() * GRID_CH_ROW_CNT; i++ )
	{
		m_wndChGrid.SetValueRange( CGXRange((i%m_wndChGrid.GetRowCount()) +1, (GRID_CH_ROW_CNT*4)+1 + i/m_wndChGrid.GetRowCount()), "1");
	}
}

void CClampCountChkDlg::OnBnClickedSelectAllOffBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int i=0;
	for( i=0; i<m_wndChGrid.GetRowCount() * GRID_CH_ROW_CNT; i++ )
	{
		m_wndChGrid.SetValueRange( CGXRange((i%m_wndChGrid.GetRowCount()) +1, (GRID_CH_ROW_CNT*4)+1 + i/m_wndChGrid.GetRowCount()), "0");
	}
}

void CClampCountChkDlg::OnCbnSelchangeItemSelCombo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
}

void CClampCountChkDlg::OnBnClickedBtnUpdate2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	fnUpdateUPSState();
}

void CClampCountChkDlg::OnBnClickedBtnGainRead()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nRtn= TRUE;
	CString strTemp;

	

	EP_CMD_GAIN_READ_REQ Readtarget;
	ZeroMemory(&Readtarget, sizeof(EP_CMD_GAIN_READ_REQ));

	Readtarget.targetBd = m_nBdNum;
	
	
	if((nRtn = EPSendCommand(m_nModuleID, 0, 0, EP_CMD_TO_SBC_GAIN_READ_REQ, &Readtarget, sizeof(EP_CMD_GAIN_READ_REQ)))	!= EP_ACK)	
	{
		strTemp.Format("Module %d :: Gain Data Read Fail. (%s)\n", m_nModuleID, m_pDoc->CmdFailMsg(nRtn));
		m_pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);	
	}

	SetTimer(TIMER_GAIN_READ, 2000, NULL);
	
}


void CClampCountChkDlg::OnBnClickedBtnGainWrite()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int nRtn= TRUE;
	CString strTemp,strTemp1[4];
	//CString str_I_UpKP[5];
	//CString str_I_DownKP[5];
	//CString str_I_UpKI[5];
	//CString str_I_DownKI[5];

	CString str_I_GAIN1[4];
	CString str_I_GAIN2[4];
	CString str_I_GAIN3[4];
	CString str_I_GAIN4[4];
	CString str_I_GAIN5[4];


	CString str_V_GAIN1[2];
	CString str_V_GAIN2[2];

	EP_CMD_GAIN_WRITE_REQ Writetarget;
	ZeroMemory(&Writetarget, sizeof(EP_CMD_GAIN_READ_REQ));
	
	GetDlgItem(IDC_EDIT_UP_KP1)->GetWindowText(str_I_GAIN1[0]);
	GetDlgItem(IDC_EDIT_UP_KP2)->GetWindowText(str_I_GAIN2[0]);
	GetDlgItem(IDC_EDIT_UP_KP3)->GetWindowText(str_I_GAIN3[0]);
	GetDlgItem(IDC_EDIT_UP_KP4)->GetWindowText(str_I_GAIN4[0]);
	GetDlgItem(IDC_EDIT_UP_KP5)->GetWindowText(str_I_GAIN5[0]);

	

	GetDlgItem(IDC_EDIT_UP_KI1)->GetWindowText(str_I_GAIN1[1]);
	GetDlgItem(IDC_EDIT_UP_KI2)->GetWindowText(str_I_GAIN2[1]);
	GetDlgItem(IDC_EDIT_UP_KI3)->GetWindowText(str_I_GAIN3[1]);
	GetDlgItem(IDC_EDIT_UP_KI4)->GetWindowText(str_I_GAIN4[1]);
	GetDlgItem(IDC_EDIT_UP_KI5)->GetWindowText(str_I_GAIN5[1]);

	GetDlgItem(IDC_EDIT_DOWN_KP1)->GetWindowText(str_I_GAIN1[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP2)->GetWindowText(str_I_GAIN2[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP3)->GetWindowText(str_I_GAIN3[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP4)->GetWindowText(str_I_GAIN4[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP5)->GetWindowText(str_I_GAIN5[2]);


	GetDlgItem(IDC_EDIT_DOWN_KI1)->GetWindowText(str_I_GAIN1[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI2)->GetWindowText(str_I_GAIN2[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI3)->GetWindowText(str_I_GAIN3[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI4)->GetWindowText(str_I_GAIN4[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI5)->GetWindowText(str_I_GAIN5[3]);



	GetDlgItem(IDC_EDIT_V_KP1)->GetWindowText(str_V_GAIN1[0]);
	GetDlgItem(IDC_EDIT_V_KP2)->GetWindowText(str_V_GAIN2[0]);
	GetDlgItem(IDC_EDIT_V_KI1)->GetWindowText(str_V_GAIN1[1]);
	GetDlgItem(IDC_EDIT_V_KI2)->GetWindowText(str_V_GAIN2[1]);

	for(int i=0; i< 4; i++)
	{
		Writetarget.gainData.I_Gain1[i] = atof(str_I_GAIN1[i]);
		Writetarget.gainData.I_Gain2[i] = atof(str_I_GAIN2[i]);
		Writetarget.gainData.I_Gain3[i] = atof(str_I_GAIN3[i]);
		Writetarget.gainData.I_Gain4[i] = atof(str_I_GAIN4[i]);
		Writetarget.gainData.I_Gain5[i] = atof(str_I_GAIN5[i]);
	}

	Writetarget.gainData.V_Gain1[0] = atof(str_V_GAIN1[0]);
	Writetarget.gainData.V_Gain2[0] = atof(str_V_GAIN2[0]);

	Writetarget.gainData.V_Gain1[1] = atof(str_V_GAIN1[1]);
	Writetarget.gainData.V_Gain2[1] = atof(str_V_GAIN2[1]);

	Writetarget.targetBd = m_nBdNum;
	




	if((nRtn = EPSendCommand(m_nModuleID, 0, 0, EP_CMD_TO_SBC_GAIN_WRITE_REQ, &Writetarget, sizeof(EP_CMD_GAIN_WRITE_REQ)))	!= EP_ACK)	
	{
		strTemp.Format("Module %d :: Gain Data Write Fail. (%s)\n", m_nModuleID, m_pDoc->CmdFailMsg(nRtn));
		m_pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);	
	}
	else
	{
		int nProgressStep = 50000;
		m_pDoc->SetProgressWnd(0, nProgressStep, "Sending condition to module...");
		
		nProgressStep = 0;
		for(int i=0; i<50000; i++)
		{
			nProgressStep++;
			m_pDoc->SetProgressPos(nProgressStep);
		}

		m_pDoc->HideProgressWnd();
	}


}

float CClampCountChkDlg::round_(float fValue, int nDotCnt)
{
	float fDotE = 1.0f;
	for(int i=0; i<nDotCnt; i++)
	{
		fDotE *= 10;
	}
	float fDoMinus = 0.1f;
	for(int i=0; i<nDotCnt; i++)
	{
		fDoMinus /= 10; 
	}
	//버림

	//반올림
	float fValueRound = floor(fValue*fDotE);

	if((fValue*fDotE) - fValueRound >= 0.5)
	{
		fValue += fDoMinus;
	}
	return fValue;
}

void CClampCountChkDlg::fnUpdateGainRead()
{
	CString str_I_GAIN1[4];
	CString str_I_GAIN2[4];
	CString str_I_GAIN3[4];
	CString str_I_GAIN4[4];
	CString str_I_GAIN5[4];
	CString str_V_GAIN1[2];
	CString str_V_GAIN2[2];

	for(int i=0; i<4; i++)
	{
		m_IGain1[i] = m_pDoc->I_Gain1_PC[i];
		m_IGain2[i] = m_pDoc->I_Gain2_PC[i];
		m_IGain3[i] = m_pDoc->I_Gain3_PC[i];
		m_IGain4[i] = m_pDoc->I_Gain4_PC[i];
		m_IGain5[i] = m_pDoc->I_Gain5_PC[i];


// 		m_IGain1[i] = floor(10000.*m_IGain1[i])/10000;
// 		m_IGain2[i] = floor(10000.*m_IGain2[i])/10000;
// 		m_IGain3[i] = floor(10000.*m_IGain3[i])/10000;
// 		m_IGain4[i] = floor(10000.*m_IGain4[i])/10000;
// 		m_IGain5[i] = floor(10000.*m_IGain5[i])/10000; //2020-01-28 엄륭 소숫점 자리 4자리 이하 버림


		str_I_GAIN1[i].Format("%.4f",m_IGain1[i]);
		str_I_GAIN2[i].Format("%.4f",m_IGain2[i]);
		str_I_GAIN3[i].Format("%.4f",m_IGain3[i]);
		str_I_GAIN4[i].Format("%.4f",m_IGain4[i]);
		str_I_GAIN5[i].Format("%.4f",m_IGain5[i]);
	}

	


	m_VGain1[0] = m_pDoc->V_Gain1_PC[0];
	m_VGain1[1] = m_pDoc->V_Gain1_PC[1];
	m_VGain2[0] = m_pDoc->V_Gain2_PC[0];
	m_VGain2[1] = m_pDoc->V_Gain2_PC[1];


	m_VGain1[0] = floor(10000.*m_VGain1[0])/10000;
	m_VGain1[1] = floor(10000.*m_VGain1[1])/10000;
	m_VGain2[0] = floor(10000.*m_VGain2[0])/10000;
	m_VGain2[1] = floor(10000.*m_VGain2[1])/10000;  //2020-01-28 엄륭 소숫점 자리 4자리 이하 버림



	str_V_GAIN1[0].Format("%.4f",m_VGain1[0]);
	str_V_GAIN2[0].Format("%.4f",m_VGain2[0]);
	str_V_GAIN1[1].Format("%.4f",m_VGain1[1]);
	str_V_GAIN2[1].Format("%.4f",m_VGain2[1]);





	

	GetDlgItem(IDC_EDIT_UP_KP1)->SetWindowText(str_I_GAIN1[0]);
	GetDlgItem(IDC_EDIT_UP_KP2)->SetWindowText(str_I_GAIN2[0]);
	GetDlgItem(IDC_EDIT_UP_KP3)->SetWindowText(str_I_GAIN3[0]);
	GetDlgItem(IDC_EDIT_UP_KP4)->SetWindowText(str_I_GAIN4[0]);
	GetDlgItem(IDC_EDIT_UP_KP5)->SetWindowText(str_I_GAIN5[0]);

	GetDlgItem(IDC_EDIT_UP_KI1)->SetWindowText(str_I_GAIN1[1]);
	GetDlgItem(IDC_EDIT_UP_KI2)->SetWindowText(str_I_GAIN2[1]);
	GetDlgItem(IDC_EDIT_UP_KI3)->SetWindowText(str_I_GAIN3[1]);
	GetDlgItem(IDC_EDIT_UP_KI4)->SetWindowText(str_I_GAIN4[1]);
	GetDlgItem(IDC_EDIT_UP_KI5)->SetWindowText(str_I_GAIN5[1]);

	GetDlgItem(IDC_EDIT_DOWN_KP1)->SetWindowText(str_I_GAIN1[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP2)->SetWindowText(str_I_GAIN2[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP3)->SetWindowText(str_I_GAIN3[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP4)->SetWindowText(str_I_GAIN4[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP5)->SetWindowText(str_I_GAIN5[2]);

	GetDlgItem(IDC_EDIT_DOWN_KI1)->SetWindowText(str_I_GAIN1[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI2)->SetWindowText(str_I_GAIN2[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI3)->SetWindowText(str_I_GAIN3[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI4)->SetWindowText(str_I_GAIN4[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI5)->SetWindowText(str_I_GAIN5[3]);

	GetDlgItem(IDC_EDIT_V_KP1)->SetWindowText(str_V_GAIN1[0]);
	GetDlgItem(IDC_EDIT_V_KP2)->SetWindowText(str_V_GAIN2[0]);
	GetDlgItem(IDC_EDIT_V_KI1)->SetWindowText(str_V_GAIN1[1]);
	GetDlgItem(IDC_EDIT_V_KI2)->SetWindowText(str_V_GAIN2[1]);


	m_pDoc->m_bGainReadOn = FALSE;

	KillTimer(TIMER_GAIN_READ);
	
}

void CClampCountChkDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch( nIDEvent )
	{
	case TIMER_GAIN_READ:
		{
			
			if(m_pDoc->m_bGainReadOn == TRUE)
			{
				fnUpdateGainRead();
			}
		}

	/*case TIMER_GAIN_WRITE:
		{
			fnProgressStep();
		}*/
	}


	CDialog::OnTimer(nIDEvent);
}

void CClampCountChkDlg::OnBnClickedBtnGainInit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int nRtn= TRUE;
	CString strTemp,strTemp1[4];

	EP_CMD_GAIN_INIT_REQ  Inittarget;
	ZeroMemory(&Inittarget, sizeof(EP_CMD_GAIN_INIT_REQ));

	Inittarget.initBd = m_nBdNum;

	if((nRtn = EPSendCommand(m_nModuleID, 0, 0, EP_CMD_TO_SBC_GAIN_INIT_REQ, &Inittarget, sizeof(EP_CMD_GAIN_INIT_REQ)))	!= EP_ACK)	
	{
		strTemp.Format("Module %d :: Gain Data Init Fail. (%s)\n", m_nModuleID, m_pDoc->CmdFailMsg(nRtn));
		m_pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);	
	}
}

void CClampCountChkDlg::OnBnClickedBtnGainUpdate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nRtn= TRUE;
	CString strTemp;



	EP_CMD_GAIN_UPDATE_REQ Updatetarget;
	ZeroMemory(&Updatetarget, sizeof(EP_CMD_GAIN_UPDATE_REQ));

	Updatetarget.updateBd = m_nBdNum;


	if((nRtn = EPSendCommand(m_nModuleID, 0, 0, EP_CMD_TO_SBC_GAIN_UPDATE_REQ, &Updatetarget, sizeof(EP_CMD_GAIN_UPDATE_REQ)))	!= EP_ACK)	
	{
		strTemp.Format("Module %d :: UpDate Data Fail. (%s)\n", m_nModuleID, m_pDoc->CmdFailMsg(nRtn));
		m_pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);	
	}	
}

void CClampCountChkDlg::OnCbnSelchangeBoardSelCombo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int nCurSel = m_ctrlBDSelCombo.GetCurSel();
	if( nCurSel == LB_ERR )
	{
		return;
	}

	if( m_nBdNum != nCurSel )
	{
		m_nBdNum = m_ctrlBDSelCombo.GetCurSel();
	}
}


void CClampCountChkDlg::fninitGain()
{
	CString str_I_GAIN1[4];
	CString str_I_GAIN2[4];
	CString str_I_GAIN3[4];
	CString str_I_GAIN4[4];
	CString str_I_GAIN5[4];
	CString str_V_GAIN1[2];
	CString str_V_GAIN2[2];

	for(int i=0; i<4; i++)
	{
		m_IGain1[i] = 0.0000;
		m_IGain2[i] = 0.0000;
		m_IGain3[i] = 0.0000;
		m_IGain4[i] = 0.0000;
		m_IGain5[i] = 0.0000;


		str_I_GAIN1[i].Format("%.4f",m_IGain1[i]);
		str_I_GAIN2[i].Format("%.4f",m_IGain2[i]);
		str_I_GAIN3[i].Format("%.4f",m_IGain3[i]);
		str_I_GAIN4[i].Format("%.4f",m_IGain4[i]);
		str_I_GAIN5[i].Format("%.4f",m_IGain5[i]);
	}




	m_VGain1[0] = 0.0000;
	m_VGain1[1] = 0.0000;
	m_VGain2[0] = 0.0000;
	m_VGain2[1] = 0.0000;

	str_V_GAIN1[0].Format("%.4f",m_VGain1[0]);
	str_V_GAIN2[0].Format("%.4f",m_VGain2[0]);
	str_V_GAIN1[1].Format("%.4f",m_VGain1[1]);
	str_V_GAIN2[1].Format("%.4f",m_VGain2[1]);







	GetDlgItem(IDC_EDIT_UP_KP1)->SetWindowText(str_I_GAIN1[0]);
	GetDlgItem(IDC_EDIT_UP_KP2)->SetWindowText(str_I_GAIN2[0]);
	GetDlgItem(IDC_EDIT_UP_KP3)->SetWindowText(str_I_GAIN3[0]);
	GetDlgItem(IDC_EDIT_UP_KP4)->SetWindowText(str_I_GAIN4[0]);
	GetDlgItem(IDC_EDIT_UP_KP5)->SetWindowText(str_I_GAIN5[0]);

	GetDlgItem(IDC_EDIT_UP_KI1)->SetWindowText(str_I_GAIN1[1]);
	GetDlgItem(IDC_EDIT_UP_KI2)->SetWindowText(str_I_GAIN2[1]);
	GetDlgItem(IDC_EDIT_UP_KI3)->SetWindowText(str_I_GAIN3[1]);
	GetDlgItem(IDC_EDIT_UP_KI4)->SetWindowText(str_I_GAIN4[1]);
	GetDlgItem(IDC_EDIT_UP_KI5)->SetWindowText(str_I_GAIN5[1]);

	GetDlgItem(IDC_EDIT_DOWN_KP1)->SetWindowText(str_I_GAIN1[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP2)->SetWindowText(str_I_GAIN2[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP3)->SetWindowText(str_I_GAIN3[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP4)->SetWindowText(str_I_GAIN4[2]);
	GetDlgItem(IDC_EDIT_DOWN_KP5)->SetWindowText(str_I_GAIN5[2]);

	GetDlgItem(IDC_EDIT_DOWN_KI1)->SetWindowText(str_I_GAIN1[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI2)->SetWindowText(str_I_GAIN2[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI3)->SetWindowText(str_I_GAIN3[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI4)->SetWindowText(str_I_GAIN4[3]);
	GetDlgItem(IDC_EDIT_DOWN_KI5)->SetWindowText(str_I_GAIN5[3]);

	GetDlgItem(IDC_EDIT_V_KP1)->SetWindowText(str_V_GAIN1[0]);
	GetDlgItem(IDC_EDIT_V_KP2)->SetWindowText(str_V_GAIN2[0]);
	GetDlgItem(IDC_EDIT_V_KI1)->SetWindowText(str_V_GAIN1[1]);
	GetDlgItem(IDC_EDIT_V_KI2)->SetWindowText(str_V_GAIN2[1]);
}



BOOL CClampCountChkDlg::InitEditCtrl()
{
	COleDateTime ole;
	SECCurrencyEdit::Format fmt(FALSE);
	fmt.SetMonetarySymbol(0);
	fmt.SetPositiveFormat(2);
	fmt.SetNegativeFormat(2);
	fmt.EnableLeadingZero(FALSE);
	fmt.SetFractionalDigits(1);
	fmt.SetThousandSeparator(0);
	fmt.SetDecimalDigits(0);
	

	int a = 4, b =2;
	

	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
	

	//fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
	//안전조건 전압 상한값 
		//EDLC Capa 측정
// 
// 	VERIFY(m_UPKP1.Initialize(this, IDC_EDIT_UP_KP1));				m_UPKP1.SetFormat(fmt);	m_UPKP1.SetValue(0);
// 	VERIFY(m_UPKP2.Initialize(this, IDC_EDIT_UP_KP2));				m_UPKP2.SetFormat(fmt);	m_UPKP2.SetValue(0);
// 	VERIFY(m_UPKP3.Initialize(this, IDC_EDIT_UP_KP3));				m_UPKP3.SetFormat(fmt);	m_UPKP3.SetValue(0);
// 	VERIFY(m_UPKP4.Initialize(this, IDC_EDIT_UP_KP4));				m_UPKP4.SetFormat(fmt);	m_UPKP4.SetValue(0);
// 	VERIFY(m_UPKP5.Initialize(this, IDC_EDIT_UP_KP5));				m_UPKP5.SetFormat(fmt);	m_UPKP5.SetValue(0);
// 
// 	VERIFY(m_UPKI1.Initialize(this, IDC_EDIT_UP_KI1));				m_UPKI1.SetFormat(fmt);	m_UPKI1.SetValue(0);
// 	VERIFY(m_UPKI2.Initialize(this, IDC_EDIT_UP_KI2));				m_UPKI2.SetFormat(fmt);	m_UPKI2.SetValue(0);
// 	VERIFY(m_UPKI3.Initialize(this, IDC_EDIT_UP_KI3));				m_UPKI3.SetFormat(fmt);	m_UPKI3.SetValue(0);
// 	VERIFY(m_UPKI4.Initialize(this, IDC_EDIT_UP_KI4));				m_UPKI4.SetFormat(fmt);	m_UPKI4.SetValue(0);
// 	VERIFY(m_UPKI5.Initialize(this, IDC_EDIT_UP_KI5));				m_UPKI5.SetFormat(fmt);	m_UPKI5.SetValue(0);
// 
// 	VERIFY(m_DOWNKP1.Initialize(this, IDC_EDIT_DOWN_KP1));				m_DOWNKP1.SetFormat(fmt);	m_DOWNKP1.SetValue(0);
// 	VERIFY(m_DOWNKP2.Initialize(this, IDC_EDIT_DOWN_KP2));				m_DOWNKP2.SetFormat(fmt);	m_DOWNKP2.SetValue(0);
// 	VERIFY(m_DOWNKP3.Initialize(this, IDC_EDIT_DOWN_KP3));				m_DOWNKP3.SetFormat(fmt);	m_DOWNKP3.SetValue(0);
// 	VERIFY(m_DOWNKP4.Initialize(this, IDC_EDIT_DOWN_KP4));				m_DOWNKP4.SetFormat(fmt);	m_DOWNKP4.SetValue(0);
// 	VERIFY(m_DOWNKP5.Initialize(this, IDC_EDIT_DOWN_KP5));				m_DOWNKP5.SetFormat(fmt);	m_DOWNKP5.SetValue(0);
// 
// 	VERIFY(m_DOWNKI1.Initialize(this, IDC_EDIT_DOWN_KI1));				m_DOWNKI1.SetFormat(fmt);	m_DOWNKI1.SetValue(0);
// 	VERIFY(m_DOWNKI2.Initialize(this, IDC_EDIT_DOWN_KI2));				m_DOWNKI2.SetFormat(fmt);	m_DOWNKI2.SetValue(0);
// 	VERIFY(m_DOWNKI3.Initialize(this, IDC_EDIT_DOWN_KI3));				m_DOWNKI3.SetFormat(fmt);	m_DOWNKI3.SetValue(0);
// 	VERIFY(m_DOWNKI4.Initialize(this, IDC_EDIT_DOWN_KI4));				m_DOWNKI4.SetFormat(fmt);	m_DOWNKI4.SetValue(0);
// 	VERIFY(m_DOWNKI5.Initialize(this, IDC_EDIT_DOWN_KI5));				m_DOWNKI5.SetFormat(fmt);	m_DOWNKI5.SetValue(0);
// 
// 	VERIFY(m_VGAINKP1.Initialize(this, IDC_EDIT_V_KP1));				m_VGAINKP1.SetFormat(fmt);	m_VGAINKP1.SetValue(0);
// 	VERIFY(m_VGAINKP2.Initialize(this, IDC_EDIT_V_KP2));				m_VGAINKP2.SetFormat(fmt);	m_VGAINKP2.SetValue(0);
// 	VERIFY(m_VGAINKI1.Initialize(this, IDC_EDIT_V_KI1));				m_VGAINKI1.SetFormat(fmt);	m_VGAINKI1.SetValue(0);
// 	VERIFY(m_VGAINKI2.Initialize(this, IDC_EDIT_V_KI2));				m_VGAINKI2.SetFormat(fmt);	m_VGAINKI2.SetValue(0);
// 	
// 	
	return TRUE;
}


float CClampCountChkDlg::EditOnlyFloat(int nID, int nIntegerMax, int nDot)	
{
	//해당 edit을 float형으로 초기화시킬것
	//KSJ 소수 EDIT
	CString strDot;
	bool	bDel = false;	//삭제여부
	int		nCurMove = 1;		//커서변경
	CString strTemp;
	CString strInput;
	GetDlgItemText(nID,strTemp);

	int nCursel = (WORD)((CEdit*)GetDlgItem(nID))->GetSel() -1;
	strInput = strTemp.Mid(nCursel,1);

	strDot.Format("%%.%df",nDot);


	//입력이 숫자이외의것이면 삭제
	if(strInput.GetAt(0) < 48 || strInput.GetAt(0) > 57)
	{
		if(strInput != "." && strInput != "-")
		{
			strTemp.Delete(nCursel);
			bDel = TRUE;
			nCurMove = 0;
		}
	}
	else//정수부가 설정치보다 크면 /10
	{
		int nIntegerNum = int(atof(strTemp));//정수부
		bool bMinus = FALSE;;
		if(nIntegerNum<0)
		{
			nIntegerNum *= -1;	//절대값
			bMinus = TRUE;
		}
		if(nIntegerNum >= nIntegerMax)
		{
			CString strIntegerNum;
			strIntegerNum.Format("%d",nIntegerNum/10);
			if(bMinus == TRUE)
				strIntegerNum = "-"+strIntegerNum;

			int nIdx = strTemp.Find(".",0);

			strTemp = strIntegerNum+strTemp.Mid(nIdx,strTemp.GetLength()-nIdx);
			bDel = TRUE;
		}
	}



	//앞이 아닌위치에 있는 - 삭제
	if(bDel == false)
	{
		if(nCursel >0 && strInput == _T("-"))
		{
			strTemp.Delete(nCursel);
			bDel = TRUE;
			nCurMove = 0;
		}
	}


	//입력이 점인데 그점이 가장 처음나오는 점이면 뒤에 싹밀기
	if(bDel == false)
	{
		if(strInput == _T(".") )
		{
			int nIdx = strTemp.Find(_T("."),0);
			if(nCursel==nIdx)	
			{
				strTemp = strTemp.Left(nCursel+1)+_T("0");
				bDel = TRUE;
				nCurMove = 1;
			}
			else //이미 앞에 점이 있을때 . 삭제
			{
				strTemp.Delete(nCursel);
				bDel = TRUE;
				nCurMove = 0;
			}
		}
	}


	////////////////////////////////////////////////////////////////////
	//

	//소수점설정
	float fTemp;
	if(strTemp != "")
	{
		fTemp = atof(strTemp);
	}
	else
	{
		fTemp = 0;
	}
	strTemp.Format(strDot,fTemp);

	//내림
	int nDotE =1;
	for(int i=0; i<nDot; i++)
	{
		nDotE*=10;
	}
	fTemp = floor(fTemp *nDotE)/nDotE;

	//적용
	SetDlgItemText(nID,strTemp);
	((CEdit*)GetDlgItem(nID))->SetSel(nCursel+nCurMove,nCursel+nCurMove);
	return atof(strTemp);
	return 0.0f;
}
// 
void CClampCountChkDlg::OnEnChangeEditUpKp1(){		EditOnlyFloat(IDC_EDIT_UP_KP1,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKi1(){		EditOnlyFloat(IDC_EDIT_UP_KI1,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKp2(){		EditOnlyFloat(IDC_EDIT_UP_KP2,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKi2(){		EditOnlyFloat(IDC_EDIT_UP_KI2,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKp3(){		EditOnlyFloat(IDC_EDIT_UP_KP3,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKi3(){		EditOnlyFloat(IDC_EDIT_UP_KI3,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKp4(){		EditOnlyFloat(IDC_EDIT_UP_KP4,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKi4(){		EditOnlyFloat(IDC_EDIT_UP_KI4,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKp5(){		EditOnlyFloat(IDC_EDIT_UP_KP5,100,4);		}
void CClampCountChkDlg::OnEnChangeEditUpKi5(){		EditOnlyFloat(IDC_EDIT_UP_KI5,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKp1(){	EditOnlyFloat(IDC_EDIT_DOWN_KP1,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKi1(){	EditOnlyFloat(IDC_EDIT_DOWN_KI1,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKi2(){	EditOnlyFloat(IDC_EDIT_DOWN_KI2,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKp2(){	EditOnlyFloat(IDC_EDIT_DOWN_KP2,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKp3(){	EditOnlyFloat(IDC_EDIT_DOWN_KP3,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKi3(){	EditOnlyFloat(IDC_EDIT_DOWN_KI3,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKi4(){	EditOnlyFloat(IDC_EDIT_DOWN_KI4,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKp4(){	EditOnlyFloat(IDC_EDIT_DOWN_KP4,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKp5(){	EditOnlyFloat(IDC_EDIT_DOWN_KP5,100,4);		}
void CClampCountChkDlg::OnEnChangeEditDownKi5(){	EditOnlyFloat(IDC_EDIT_DOWN_KI5,100,4);		}


void CClampCountChkDlg::UpdateView()
{
	m_ctrlItemSelCombo.SetCurSel(0);
	m_ctrlMDSelCombo.SetCurSel(m_nModuleID -1);	
}