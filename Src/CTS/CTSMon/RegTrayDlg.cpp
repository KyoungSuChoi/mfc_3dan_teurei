// RegTrayDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "RegTrayDlg.h"

#include "UserAdminDlg.h"
#include "LBEditorWnd.h"
#include "JigIDRegDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegTrayDlg dialog

extern STR_LOGIN	g_LoginData;


CRegTrayDlg::CRegTrayDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRegTrayDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CRegTrayDlg"));
	//{{AFX_DATA_INIT(CRegTrayDlg)
	m_strTrayNo = _T("");
	m_strTryName = _T("");
	m_strRegUserName = _T("");
	m_strDescription = _T("");
	//}}AFX_DATA_INIT
//	m_nNextTraySerial= 0;

	m_cursel = -1;
	m_bModeMultiInput = TRUE;
}
CRegTrayDlg::~CRegTrayDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CRegTrayDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

void CRegTrayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegTrayDlg)
	DDX_Control(pDX, IDC_LIST1, m_ctrlTrayListBox);
	DDX_Text(pDX, IDC_TRAY_USER_NO, m_strTrayNo);
	DDV_MaxChars(pDX, m_strTrayNo, EP_TRAY_NAME_LENGTH);
	DDX_Text(pDX, IDC_TRAY_NAME, m_strTryName);
	DDX_Text(pDX, IDC_TRAY_USER, m_strRegUserName);
	DDV_MaxChars(pDX, m_strRegUserName, EP_MAX_LONINID_LENGTH);
	DDX_Text(pDX, IDC_TRAY_DESCRIP, m_strDescription);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRegTrayDlg, CDialog)
	//{{AFX_MSG_MAP(CRegTrayDlg)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	//}}AFX_MSG_MAP
	ON_MESSAGE(EPWM_BCR_SCANED, OnBcrscaned)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegTrayDlg message handlers

BOOL CRegTrayDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
/*	char szBuff[16];
	sprintf(szBuff, "%%0%dd", EP_TRAY_SERIAL_LENGTH);
	m_strTraySerial.Format(szBuff, m_nNextTraySerial);
*/	
//	m_TrayData.InitData();
	

	for(int i = 0; i<100; i++)
	{
		m_ctrlTrayListBox.AddString("");
	}

	if(m_bModeMultiInput)
	{
		GetDlgItem(IDC_TRAY_USER_NO)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_LIST1)->ShowWindow(SW_HIDE);
	}

	m_strRegUserName = g_LoginData.szLoginID;
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRegTrayDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	CString strTemp;

	if(m_strTrayNo.IsEmpty() && m_bModeMultiInput == FALSE)
	{
		AfxMessageBox(TEXT_LANG[4]);
		GetDlgItem(IDC_TRAY_USER_NO)->SetFocus();
		return;
	}

	if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UseLogin", 1))
	{
		CUserAdminDlg	userSearch;
		STR_LOGIN	userData;
		if(userSearch.SearchUser(m_strRegUserName, FALSE, "", &userData) == FALSE)	
		{
			AfxMessageBox(TEXT_LANG[5]);
			GetDlgItem(IDC_TRAY_USER)->SetFocus();
			return;
		}
			
		if(!(userData.nPermission & PMS_TRAY_REG))
		{
			GetDlgItem(IDC_TRAY_USER)->SetFocus();
			AfxMessageBox(TEXT_LANG[21] + TEXT_LANG[0]);//" [Tray registration]"
			return;		
		}
	}
	if(m_strRegUserName.IsEmpty())
	{
		AfxMessageBox(TEXT_LANG[6]); 
		GetDlgItem(IDC_TRAY_USER)->SetFocus();
		return;
	}


	int nTot = 1;
	if(m_bModeMultiInput)
	{
		nTot = m_ctrlTrayListBox.GetCount();
	}
	
	CWaitCursor aWait;

	int nCnt = 0;

	//등록 시험
/*	for(int index = 0; index < 50000; index++)
	{
		m_strTrayNo.Format("%c%04d", 'A'+index/10000, index%10000);

		//종복 Tray일 경우 삭제 
		if(m_TrayData.DeleteTray(m_strTrayNo))
		{
			strTemp.Format("Tray %d Delete Fail\n", m_TrayData.lTraySerial);
			TRACE(strTemp);
		}

		if(m_TrayData.AddNewTray(m_strTrayNo, m_strTryName, m_strRegUserName, m_strDescription))
		{
			nCnt++;
		}
	}
*/
	//Tray 1개당 DB가 6.144Kbyte 용량이 늘어남
	for(int index = 0; index < nTot; index++)
	{
		if(m_bModeMultiInput)
		{
			m_ctrlTrayListBox.GetText(index, m_strTrayNo);
			if(m_strTrayNo.IsEmpty())	continue;
		}

		//2004/5/8
		//Tray명이 리눅스에서 폴더명으로 사용되고 window에서 파일명에 포함되므로 특수 문자 제한
		//리눅스에서 폴더명에 공백이 허용 안됨
		// \\/?[]|=%.:\"*<> 와 Space
		
		if(m_strTrayNo.FindOneOf("\\/?[]|=%.:\"*<> ") >= 0)
		{
			AfxMessageBox(TEXT_LANG[7]);
			GetDlgItem(IDC_TRAY_USER_NO)->SetFocus();
			continue;
		}

		if(m_strTrayNo.GetLength() > EP_TRAY_NAME_LENGTH)
		{
			AfxMessageBox(TEXT_LANG[1]);//"초대 입력가능 길이를 초과 하였습니다."
			GetDlgItem(IDC_TRAY_USER_NO)->SetFocus();
			continue;
		}

		//종복 Tray일 경우 삭제 
//		if(m_TrayData.DeleteTray(m_strTrayNo))
//		{
//			strTemp.Format("Tray %s Delete Fail\n", m_strTrayNo);
//			TRACE(strTemp);
//		}

		if(m_TrayData.AddNewTray(m_strTrayNo, m_strTryName, m_strRegUserName, m_strDescription))
		{
			char szBuff[16];
			sprintf(szBuff, "%%0%dd", EP_TRAY_SERIAL_LENGTH);
			m_strTraySerial.Format(szBuff, m_TrayData.GetTraySerialNo());
			
			m_ctrlTrayListBox.DeleteString(index);
			m_ctrlTrayListBox.InsertString(index, "");

			nCnt++;
		}
	}


	if(nCnt <= 0)
	{
		return;
	}
	else
	{
		if(m_bModeMultiInput)
		{
			strTemp.Format(TEXT_LANG[2], nCnt); //"총 %d개의 Tray를 등록하였습니다.\n계속 등록 하시겠습니까?"
			if(AfxMessageBox(strTemp, MB_YESNO) ==IDYES)
			{
				return;
			}
		}
	}

	CDialog::OnOK();
}

void CRegTrayDlg::OnSelchangeList1() 
{
	// TODO: Add your control notification handler code here
	int cursel = m_ctrlTrayListBox.GetCurSel()  ;
	
//	if ( m_cursel == cursel )
//	{
		CLBEditorWnd* pEditor = new CLBEditorWnd( &m_ctrlTrayListBox ) ;
		pEditor->Edit( cursel ) ;
//	}
//	else
//		m_cursel = cursel ;	
}

void CRegTrayDlg::SetMultiInputMode(BOOL bMultiMode)
{
	m_bModeMultiInput = bMultiMode;
}

LRESULT CRegTrayDlg::OnBcrscaned(UINT wParam, LONG lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	if(str.IsEmpty())	
		return 0;

	if(wParam != TAG_TYPE_NONE && wParam != TAG_TYPE_TRAY)
	{
		AfxMessageBox(str + TEXT_LANG[3], MB_ICONSTOP|MB_OK); //"는 다른곳에서 사용중이므로 Tray 정보 ID로 사용할 수 없습니다."
		return 0;
	}
	
	if(m_bModeMultiInput)
	{
		int nTot = m_ctrlTrayListBox.GetCount();
		CString strTmp;
		for(int index = 0; index < nTot; index++)
		{
			m_ctrlTrayListBox.GetText(index, strTmp);
			if(strTmp.IsEmpty())
			{
				m_ctrlTrayListBox.DeleteString(index);
				m_ctrlTrayListBox.InsertString(index, str);
				break;
			}
		}
	}
	else
	{
		m_strTrayNo = str;
	}
	UpdateData(FALSE);
	return 1;
}
