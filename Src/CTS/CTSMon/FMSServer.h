#pragma once

// class CFMSServer : public CFMSRawServer<CFMSObj>
class CFMSServer
{
public:
	CFMSServer();
	virtual ~CFMSServer();

private:
	INT						mConnectCnt;	
	BOOL					m_bRunChk;
	//std::list<CFMSObj*>	m_lstFMSObj;

	HWND					m_NotifyHwnd;
	HANDLE					mKeepThreadHandle;
	HANDLE					mKeepThreadDestroyEvent;
	
	HANDLE					mObjDelete;
	
	st_FMS_PACKET			m_Pack;	

	DWORD					m_HeartBeatCheck;
public:
	BOOL	NetBegin(HWND);
	BOOL	NetEnd(VOID);

	VOID	KeepThreadCallback(VOID);	

	VOID	fnMakeHead(st_FMSMSGHEAD& _head, CHAR* _cmd, UINT _len, CHAR _result);
	VOID	fnSendResult(const CHAR* _Longdata);
	VOID	fnKillNetwork();

	VOID	SetHeartbeatClear();
	BOOL	ChkHeartbeat();
	BOOL	fnSendHsmeCmd( st_HSMS_PACKET pack );

protected:

	// VOID OnConnected(CFMSObj *poNetObj);
	// VOID OnDisconnected(CFMSObj *poNetObj);
	// VOID OnRead(CFMSObj *poNetObj, st_FMSMSGHEAD&_hdr,  BYTE *pReadBuf, DWORD dwLen);
	// VOID OnWrite(CFMSObj *poNetObj, DWORD dwLen);
};