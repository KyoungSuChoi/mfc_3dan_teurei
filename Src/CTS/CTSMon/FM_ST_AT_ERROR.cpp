#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_ERROR::CFM_ST_AT_ERROR(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
}


CFM_ST_AT_ERROR::~CFM_ST_AT_ERROR(void)
{
}

VOID CFM_ST_AT_ERROR::fnEnter()
{
	// m_Unit->fnGetFMS()->fnSend_E_EQ_ERROR(FMS_ER_NONE);
	CHANGE_FNID(FNID_PROC);
	//트레이 검사

	EP_GP_DATA gpData = EPGetGroupData(m_Unit->fnGetModuleID(), 0);

	IO_Data RecvIOData;
	memcpy(&RecvIOData, &gpData.gpState.ManualFunc1, sizeof(IO_Data));

	if( RecvIOData.ch_2 == TRUE || RecvIOData.ch_11 == TRUE)
	{
		m_Unit->fnGetModule()->m_bProcessStart = TRUE;
	}

	//공정 진행

	TRACE("CFM_ST_AT_ERROR::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_ERROR::fnProc()
{
	fnSetFMSStateCode(FMS_ST_ERROR);
	TRACE("CFM_ST_AT_ERROR::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_ERROR::fnExit()
{	
	// m_Unit->fnGetFMS()->fnSendToHost_S5F1_Reset();	

	TRACE("CFM_ST_AT_ERROR::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_ERROR::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

	EP_GP_DATA gpData = EPGetGroupData(m_Unit->fnGetModuleID(), 0);

	IO_Data RecvIOData;
	memcpy(&RecvIOData, &gpData.gpState.ManualFunc1, sizeof(IO_Data));

	if( RecvIOData.ch_2 == TRUE || RecvIOData.ch_11 == TRUE)
	{
		m_Unit->fnGetModule()->m_bProcessStart = TRUE;
	}
	else
	{
		m_Unit->fnGetModule()->m_bProcessStart = FALSE;
	}
	
	if(m_Unit->fnGetFMS()->fnGetError())
	    return;
	
    switch(m_Unit->fnGetModule()->GetState())
    {
    case EP_STATE_IDLE:
        if(m_Unit->fnGetModuleTrayState())
        {
            CHANGE_STATE(AUTO_ST_TRAY_IN);
        }
        else
        {
            CHANGE_STATE(AUTO_ST_VACANCY);
        }
        break;
    case EP_STATE_STANDBY:
        {
            CHANGE_STATE(AUTO_ST_TRAY_IN);
        }
        break;
    case EP_STATE_PAUSE:
        {
  //          CHANGE_STATE(AUTO_ST_ERROR);
        }
        break;
    case EP_STATE_RUN:
        {
            if(m_Unit->fnGetModuleCheckState())
            {
                CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
            }
            else
            {
                CHANGE_STATE(AUTO_ST_RUN);
            }
        }
        break;
    case EP_STATE_READY:
        {
            CHANGE_STATE(AUTO_ST_READY);
        }
        break;
    case EP_STATE_END:
        {
            CHANGE_STATE(AUTO_ST_END);
        }
        break;
    case EP_STATE_ERROR:
        {
            //CHANGE_STATE(AUTO_ST_ERROR);
        }
        break;
    }
//	TRACE("CFM_ST_AT_ERROR::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_ERROR::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	// 1. 입고 후 예약정보 취소
	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 4 )
	{
		if( m_Unit->fnGetModuleTrayState() == FALSE )
		{
			st_S2F414 data;
			ZeroMemory(&data, sizeof(st_S2F414));
			memcpy(&data, _recvData->data, sizeof(st_S2F414));

			if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
			{
				// bRunInfoErrChk = TRUE;
			}

			if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
			{
				// bRunInfoErrChk = TRUE;
			}

			_error = m_Unit->fnGetFMS()->SendSBCInitCommand();

			m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

			m_Unit->fnGetFMS()->fnSendToHost_S6F11C13( _recvData->head.nDevId, _recvData->head.lSysByte );
		}
		else
		{
			_error = ER_NO_Process;

			m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
		}
		
	}
	// 2. 입고 전 예약정보 취소
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 6 )
	{
		if( m_Unit->fnGetModuleTrayState() )
		{
			_error = ER_NO_Process;

			m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
		}
		else
		{
			st_S2F416 data;
			ZeroMemory(&data, sizeof(st_S2F416));
			memcpy(&data, _recvData->data, sizeof(st_S2F416));

			if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
			{
				// bRunInfoErrChk = TRUE;
			}

			if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
			{
				// bRunInfoErrChk = TRUE;
			}

			_error = m_Unit->fnGetFMS()->SendSBCInitCommand();

			m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

			m_Unit->fnGetFMS()->fnSendToHost_S6F11C14( _recvData->head.nDevId, _recvData->head.lSysByte );
		}
	}

	TRACE("CFM_ST_AT_ERROR::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}