// MisWiringDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "MisWiringDlg.h"
#include "MiswiringPointSetDlg.h"
#include "MainFrm.h"




// CMisWiringDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMisWiringDlg, CDialog)

CMisWiringDlg::CMisWiringDlg(int nModuleID,CWnd* pParent /*=NULL*/)
	: CDialog(CMisWiringDlg::IDD, pParent)
{
	m_nModuleID = nModuleID;
	m_bRun = false;

	m_nOptSelectChannel = -1; //20200224 엄륭  채널 선택 관련 기능 변경함

	EP_MD_SYSTEM_DATA *pSysData = ::EPGetModuleSysData(EPGetModuleIndex(m_nModuleID));

	m_nTotCh = pSysData->nTotalChNo;
	m_bCheckChannel = new bool(m_bCheckChannel);

	TEXT_CASE = NULL;
}

CMisWiringDlg::~CMisWiringDlg()
{
	if(TEXT_CASE != NULL){
		delete[] TEXT_CASE;
		TEXT_CASE = NULL;
	}
}

void CMisWiringDlg::DoDataExchange(CDataExchange* pDX)
{

	DDX_Control(pDX, IDC_TYPE_SEL_COMBO, m_comboType);
	DDX_Text(pDX, IDC_CH_SELECT_EDIT, m_strChSelect);
	DDX_Radio(pDX, IDC_OPT_ALL_CHANNEL, m_nOptSelectChannel);

	CDialog::DoDataExchange(pDX);
}





BEGIN_MESSAGE_MAP(CMisWiringDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_ALL_SELECT, &CMisWiringDlg::OnBnClickedBtnAllSelect)
	ON_BN_CLICKED(IDC_BTN_ALL_RELEASE, &CMisWiringDlg::OnBnClickedBtnAllRelease)
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnClickedGrid)
	ON_MESSAGE(WM_CHECKBOX_CLICKED, OnClickedGrid)
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(IDC_BTN_RUN, &CMisWiringDlg::OnBnClickedBtnRun)
	ON_BN_CLICKED(IDC_BTN_STOP, &CMisWiringDlg::OnBnClickedBtnStop)
	ON_BN_CLICKED(IDC_BUTTON_POINT_SET, &CMisWiringDlg::OnBnClickedButtonPointSet)
	ON_BN_CLICKED(IDCANCEL, &CMisWiringDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_OPT_ALL_CHANNEL, &CMisWiringDlg::OnBnClickedOptAllChannel)
	ON_BN_CLICKED(IDC_OPT_PART_CHANNEL, &CMisWiringDlg::OnBnClickedOptPartChannel)
END_MESSAGE_MAP()


// CMisWiringDlg 메시지 처리기입니다.

BOOL CMisWiringDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CString strTemp;

	nCh = 0;

	m_nOptSelectChannel = 0;	//20200224 엄륭  채널 선택 관련 기능 변경함
	GetDlgItem(IDC_CH_SELECT_EDIT)->EnableWindow(FALSE);	//20200224 엄륭  채널 선택 관련 기능 변경함

	LanguageinitCase("Misscase");

	InitGrid();
	m_comboType.SetCurSel(1);

	GetDlgItem(IDC_STATIC_UNIT_NAME)->SetWindowText(GetModuleName(m_nModuleID));

	strTemp.Format("%d",m_nTotCh);
	GetDlgItem(IDC_STATIC_CHANNEL_CNT)->SetWindowText(strTemp);
	


	UINT nSize;
	LPVOID* pData;

	AfxGetApp()->GetProfileBinary(REG_MIS_SECTION, "MiswiringPoint", (LPBYTE *)&pData, &nSize);
	if( nSize > 0 )
	{
		memcpy(&m_pMisPoint, pData, nSize);
	}
	else
	{
		memset(&m_pMisPoint, 0, sizeof(CMiswiringPoint));
	}
	delete [] pData;


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CMisWiringDlg::InitGrid()
{
	CString strTemp;
	m_wndGrid.SubclassDlgItem(IDC_MISWIRING_DATA, this);
	m_wndGrid.m_bSameRowSize = FALSE;
	m_wndGrid.m_bSameColSize = FALSE;
	m_wndGrid.m_bCustomWidth = FALSE;

	m_wndGrid.Initialize();
	m_wndGrid.EnableCellTips();

	BOOL bLock = m_wndGrid.LockUpdate();

	m_wndGrid.SetDefaultRowHeight(18);
	m_wndGrid.SetDefaultColWidth(120);
	m_wndGrid.SetRowCount(m_nTotCh); //
	m_wndGrid.SetColCount(GRID_COL);
	m_wndGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertical Scroll Bar

	m_wndGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Use MemDc
	m_wndGrid.SetDrawingTechnique(gxDrawUsingMemDC);
	m_wndGrid.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_NO), "No");

	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_USE), "CH");

	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_PV), "PV(mV)");
	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_VOL), "Voltage(mV)");
	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_CUR), "Current(mA)");
	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_DIV), "OK/NG");
	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_CASE), "Case");
	m_wndGrid.SetValueRange(CGXRange(0, MIS_COL_DTAIL), "Detail");
	
	//m_wndGrid.SetColWidth(MIS_COL_NO,MIS_COL_NO,50);
	m_wndGrid.SetColWidth(MIS_COL_NO,MIS_COL_USE,50);
	m_wndGrid.SetColWidth(MIS_COL_CASE,MIS_COL_CASE,150);
	m_wndGrid.SetColWidth(MIS_COL_DTAIL,MIS_COL_DTAIL,540);


 	m_wndGrid.SetCoveredCellsRowCol(0, 1, 0, 1);	//NO
// 	m_wndGrid.SetCoveredCellsRowCol(0, 3, 0, 5);	//CHARGE
// 	m_wndGrid.SetCoveredCellsRowCol(0, 6, 0, 8);	//DISCHARGE
// 	m_wndGrid.SetCoveredCellsRowCol(0, 9, 1, 9);	//양불
// 	m_wndGrid.SetCoveredCellsRowCol(0, 10, 1, 10);	//CASE
// 	m_wndGrid.SetCoveredCellsRowCol(0, 11, 1, 11);	//DETAIL

	m_wndGrid.SetStyleRange(CGXRange().SetRows(0),CGXStyle().SetFont(CGXFont().SetBold(TRUE)));
	m_wndGrid.SetStyleRange(CGXRange().SetCols(0),CGXStyle().SetFont(CGXFont().SetBold(TRUE)));

	/*m_wndGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetControl(GX_IDS_CTRL_CHECKBOX3D));
	m_wndGrid.SetValueRange( CGXRange().SetCols(2), "0");*/



	for(int i=1; i<=m_nTotCh; i++)
	{
		strTemp.Format("%d",i-1);
		m_wndGrid.SetValueRange(CGXRange(i, 1), strTemp);
		m_wndGrid.SetCoveredCellsRowCol(i, 1, i, 1);	//NO

		/*for(int j=2; j<=GRID_COL; j++)
		{
			m_wndGrid.SetStyleRange(CGXRange(i,j), CGXStyle().SetInterior(RGB(255,255,255)));
		}*/
	}

	m_wndGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(RGB(160,160,160)));
//	m_wndGrid.SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetInterior(RGB(220,220,220)));

	

	m_wndGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);


	m_wndGrid.EnableGridToolTips();
	m_wndGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_wndGrid.LockUpdate(bLock);
	m_wndGrid.Redraw();
}

void CMisWiringDlg::UpdateResultData(EP_MISWIRING_CHECK_RESULT_DATA pRcalData)
{
	CString strValue;
	//int nCh = pRcalData.nMonCh;
	nCh = nCh +1;
	if(pRcalData.nType == 0)	// 전압
	{
		strValue.Format("%d",pRcalData.nMonCh);
		m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_USE), strValue);

		strValue.Format("%.4f",float(pRcalData.lAd)/10000.0f);
		m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_VOL), strValue);

		strValue.Format("%4.0f",float(pRcalData.lAdi)/1000.0f);//20200214 엄륭 AD Value 전압 전류로 세분화함
		m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_CUR), strValue);//20200214 엄륭 AD Value 전압 전류로 세분화함

		
	}
	else
	{
		//strValue.Format("%.4f",float(pRcalData.lSetVal)/10000.0f);

		strValue.Format("%d",pRcalData.nMonCh);
		m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_USE), strValue);

		strValue.Format("%.4f",float(pRcalData.lPv)/100000000.0f);
		m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_PV), strValue);

		strValue.Format("%.4f",float(pRcalData.lAd)/1000000.0f);		
		m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_VOL), strValue);

		strValue.Format("%4.0f",float(pRcalData.lAdi)/1000.0f); ////20200214 엄륭 AD Value 전압 전류로 세분화함
		m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_CUR), strValue);//20200214 엄륭 AD Value 전압 전류로 세분화함

	}
	
	switch(pRcalData.nCheckDiv)
	{
	case 0:
		{
			m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_DIV), "OK");
			m_wndGrid.SetStyleRange(CGXRange().SetRows(nCh),CGXStyle().SetInterior(RGB_YELLOW));
		}break;
	case 1:
		{
			m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_DIV), "OPEN");
			m_wndGrid.SetStyleRange(CGXRange().SetRows(nCh),CGXStyle().SetInterior(RGB_RED_L));
		}break;
	case 2:
		{
			m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_DIV), "MISS");
			m_wndGrid.SetStyleRange(CGXRange().SetRows(nCh),CGXStyle().SetInterior(RGB_PUPPLE_L));
		}break;
	}

	switch(pRcalData.nCheckDiv)
	{
	case 1:
	case 2:
		{
			strValue.Format("Case%d", pRcalData.nCheckCase);
			m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_CASE),strValue);

			m_wndGrid.SetValueRange(CGXRange(nCh,MIS_COL_DTAIL),strValue/*TEXT_CASE[pRcalData.nCheckCase]*/);
		}break;
	}
}
void CMisWiringDlg::EndResultData(int nModuleID, int nMode)
{

}


void CMisWiringDlg::UpdateGrid()
{
 	BOOL bLock = m_wndGrid.LockUpdate();
 	//for(int i=2;i<=m_nTotCh+1; i++)
	for(int i=1;i<=m_wndGrid.GetRowCount(); i++)
 	{
 		if( m_wndGrid.GetValueRowCol(i,2) == "1" )
 		{
 			for(int j=2; j<=GRID_COL; j++)
 			{
 				m_wndGrid.SetStyleRange(CGXRange(i,j), CGXStyle().SetInterior(RGB(255,206,183)));
 			}
 		}
 		else
 		{
 			for(int j=2; j<=GRID_COL; j++)
 			{
 				m_wndGrid.SetStyleRange(CGXRange(i,j), CGXStyle().SetInterior(RGB(255,255,255)));
 			}
 		}
 	}
 	m_wndGrid.LockUpdate(bLock);
 	m_wndGrid.Redraw();

}

void CMisWiringDlg::OnBnClickedBtnAllSelect()
{
	BOOL bLock = m_wndGrid.LockUpdate();
	//for(int i=3;i<=m_nTotCh+1; i++)
	for(int i=1;i<=m_wndGrid.GetRowCount(); i++)
		
	{
		m_wndGrid.SetValueRange( CGXRange(i,2), "1");
		for(int j=2; j<=GRID_COL; j++)
		{
			m_wndGrid.SetStyleRange(CGXRange(i,j), CGXStyle().SetInterior(RGB(255,206,183)));
		}
	}
	m_wndGrid.LockUpdate(bLock);
	m_wndGrid.Redraw();
}

void CMisWiringDlg::OnBnClickedBtnAllRelease()
{
	BOOL bLock = m_wndGrid.LockUpdate();
	//for(int i=2;i<=m_nTotCh+1; i++)
	for(int i=1;i<=m_wndGrid.GetRowCount(); i++)
	{
		m_wndGrid.SetValueRange( CGXRange(i,2), "0");
		for(int j=2; j<=GRID_COL; j++)
		{
			m_wndGrid.SetStyleRange(CGXRange(i,j), CGXStyle().SetInterior(RGB(255,255,255)));
		}
	}
	m_wndGrid.LockUpdate(bLock);
	m_wndGrid.Redraw();
}


LRESULT CMisWiringDlg::OnClickedGrid(WPARAM wParam, LPARAM lParam)
{
 	UpdateGrid();
	return TRUE;
}

LRESULT CMisWiringDlg::OnModifyGrid(WPARAM wParam, LPARAM lParam)
{
	UpdateGrid();
	return TRUE;
}


void CMisWiringDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar == VK_ESCAPE)
	{
		UpdateGrid();
	}

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CMisWiringDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if(GetFocus() == &m_wndGrid)
		{
			switch( pMsg->wParam )
			{
			case VK_SPACE:
				{	
					if(m_bRun == false)	//오배선 검사중이 아니면
					{
						CWordArray awSelModule;
						GetSelectedModuleList(awSelModule);
						int nSelNo;									//선택 갯수
						nSelNo = awSelModule.GetSize();
						CString strTemp;
						if(nSelNo >0)
						{
							for (int i = 0; i<nSelNo; i++)			
							{
								int nRow = awSelModule.GetAt(i);	//선택 Row Get
								if(nRow>1)
								{
									//토글
									if( m_wndGrid.GetValueRowCol(nRow,2) == "1" )
									{
										m_wndGrid.SetValueRange(CGXRange(nRow,2), "0");
									}
									else
									{
										m_wndGrid.SetValueRange(CGXRange(nRow,2), "1");
									}
								}
							}
						}
						UpdateGrid();
						return TRUE;
					}
				}
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}



BOOL CMisWiringDlg::GetSelectedModuleList(CWordArray &awSelModule)
{	
	CGXRangeList *rangeList = m_wndGrid.GetParam()->GetRangeList();
	int nSelCellCount = rangeList->GetCount();
	POSITION	pos;
	ROWCOL		nRow, nCol, preRow;
	BOOL		nRtn;
	CGXRange	*SelCell;

	preRow = 0;
	if(nSelCellCount == 0) //Only One Cell is Selected
	{
		if(m_wndGrid.GetCurrentCell(nRow, nCol))
		{
			awSelModule.Add(nRow);
		}
	}

	for(pos = rangeList->GetHeadPosition(); pos != NULL; )
	{
		SelCell = (CGXRange *)rangeList->GetAt(pos);
		ASSERT(SelCell);
 		nRtn = SelCell->GetFirstCell(nRow, nCol);
		while(nRtn)
		{
			if(nRow != preRow)
			{
				awSelModule.Add(nRow);
			}
			preRow = nRow;
			nRtn = SelCell->GetNextCell(nRow, nCol);
		}
		rangeList->GetNext(pos); 
	} 

	return TRUE;
}
void CMisWiringDlg::OnBnClickedBtnRun()
{
 	int nCheckCnt = 0;
	CString strTemp;
	nCh = 0;  //20200224 엄륭  채널 선택 관련 기능 변경함


	for(int i=1; i<=m_nTotCh; i++) //20200224 엄륭  채널 선택 관련 기능 변경함
	{
		strTemp.Format("");
		m_wndGrid.SetValueRange(CGXRange(i, 2), strTemp);
		m_wndGrid.SetValueRange(CGXRange(i, 3), strTemp);
		m_wndGrid.SetValueRange(CGXRange(i, 4), strTemp);
		m_wndGrid.SetValueRange(CGXRange(i, 5), strTemp);
		m_wndGrid.SetValueRange(CGXRange(i, 6), strTemp);
		m_wndGrid.SetValueRange(CGXRange(i, 7), strTemp);
		m_wndGrid.SetValueRange(CGXRange(i, 8), strTemp);
	}
	
 	//for(int i=2; i<m_nTotCh+2; i++)
	/*for(int i=1;i<=m_wndGrid.GetRowCount(); i++)
 	{
		if(m_wndGrid.GetValueRowCol(i,2) == "1")
		{
			m_bCheckChannel[i] = TRUE;
		}
		else
		{
			m_bCheckChannel[i] = FALSE;
		}
 		if(m_bCheckChannel[i] == TRUE)
 		{
 			nCheckCnt++;
 		}
 	}*/

	if( m_nOptSelectChannel == 1 && GetSelectdChNum() == FALSE )
	{
		AfxMessageBox("No Selected Channel");//"No Selected Channel"
		return;
	}
 
 	/*if(nCheckCnt <1)
 	{
 		AfxMessageBox("No Selected Channel");
 	}
	else
	{*/
		if (SendChDataToSbc())	
		{
			m_bRun = true;
			m_wndGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetControl(GX_IDS_CTRL_STATIC));
		}
	/*}*/

	

}



BOOL CMisWiringDlg::SendChDataToSbc()
{
	CString strLog;
	CString strTemp;
	int nRtn = 0;
	int nCh,i;
	POSITION pos;
	CString strCh;

	int nJigCondition = EPJigState(m_nModuleID);

	//EP_JIG_DOWN

	EP_MISWIRING_CHECK_START_INFO selInfo;
	ZeroMemory(&selInfo, sizeof(EP_MISWIRING_CHECK_START_INFO));




//////////////////////////////////////////////////////////////
	if (m_nOptSelectChannel == 0)//20200224 엄륭  채널 선택 관련 기능 변경함
	{
		//전채널 선택
		for( i=0; i< EP_MAX_CH_PER_MD; i++ )	selInfo.bMonChNum[i] = 1;
	}
	else
	{
		//부분 채널 선택
		pos = m_ChList.GetHeadPosition();
		for( i=0; i<m_ChList.GetCount(); i++ )
		{
			nCh = m_ChList.GetNext(pos);
			selInfo.bMonChNum[nCh-1] = 1;
		}
		//m_pos = m_ChList.GetHeadPosition();	// 저장된 채널정보의 처음 위치
	}
///////////////////////////////////////////////////////////////////	



	//for( i=1; i<=m_wndGrid.GetRowCount(); i++ )	//전채널 선택
	//{
	//	//selInfo.bMonChNum[i] = 1;	

	//	if(m_wndGrid.GetValueRowCol(i,2) == "1")
	//	{
	//		selInfo.bMonChNum[i-1] = TRUE;
	//	}
	//	else
	//	{
	//		selInfo.bMonChNum[i-1] = FALSE;
	//	}		
	//}

	//if(m_comboType.GetCurSel() == 0)
	//{
	//	selInfo.nType = 0;	// VOLTAGE
	//}
	//else
	//{
		selInfo.nType = 1;	// CURRENT  2020-02-18 엄륭 강제로 전류만 진행
	//}

	for( i=0; i<m_pMisPoint.GetVMeasureCnt(); i++ )
	{
		selInfo.lVoltageSetVal[i] = (long)(m_pMisPoint.GetVData(i)*1000.0f);  //20200506  엄륭 PV값에 자릿수 변경 1000.0f -> 100.f
	}

	for( i=0; i<m_pMisPoint.GetIMeasureCnt(); i++ )
	{
		selInfo.lCurrentSetVal[i] = (long)(m_pMisPoint.GetIData(i)*1000.0f);
	}


	if((nRtn = EPSendCommand(m_nModuleID, 0, 0, EP_CMD_MISWIRING_CHECK_START, &selInfo, sizeof(EP_MISWIRING_CHECK_START_INFO)))!= EP_ACK)
	{
		AfxMessageBox("[[측정 시작] 명령 전송을 실패하였습니다.]");//""
		return FALSE;
	}
// 
// 	// EPSendCommand(m_nModuleID);
// 	m_bReceiveDataFlag = FALSE;
// 	m_HeartBeat = 0;
// 
// 	UpdateData(FALSE);

	return TRUE;

}

void CMisWiringDlg::OnBnClickedBtnStop()
{
	m_bRun = false;
//	m_wndGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetControl(GX_IDS_CTRL_CHECKBOX3D));
	if(EPSendCommand( m_nModuleID, 0, 0, EP_CMD_MISWIRING_CHECK_STOP) != EP_ACK)
	{
		AfxMessageBox("[정지] 명령 전송을 실패하였습니다.");//
		return;
	}

}


void CMisWiringDlg::OnBnClickedButtonPointSet()
{
	CMiswiringPointSetDlg dlg(&m_pMisPoint);
	if( dlg.DoModal() == IDOK )
	{

	}
}



bool CMisWiringDlg::LanguageinitCase(CString strKey)
{
	int i=0;
	int nTextCnt = 0;
	TEXT_CASE = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strKey, _T("TEXT_")+strKey+_T("_CNT"),  _T("TEXT_")+strKey+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_CASE = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strKey, i);
			TEXT_CASE[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strKey, strTemp, strTemp, g_nLanguage);

			if( TEXT_CASE[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

void CMisWiringDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
	pFrame->m_pMissDlg = NULL;   //2020-02-20 엄륭 오배선 관련 

	OnCancel();
}




void CMisWiringDlg::OnBnClickedOptAllChannel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nOptSelectChannel = 0;
	GetDlgItem(IDC_CH_SELECT_EDIT)->EnableWindow(FALSE);
}

void CMisWiringDlg::OnBnClickedOptPartChannel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	m_nOptSelectChannel = 1;
	GetDlgItem(IDC_CH_SELECT_EDIT)->EnableWindow(TRUE);
}


BOOL CMisWiringDlg::GetSelectdChNum()
{
	UpdateData(TRUE);

	if( m_strChSelect.IsEmpty() )
	{	return FALSE;	}

	int p1=0, p2=0, p3=0;

	m_ChList.RemoveAll();

	while(TRUE)
	{
		p2=m_strChSelect.Find(',', p1);
		p3=m_strChSelect.Find('-', p1);

		if(p2!=-1 && p3==-1)
		{
			long cyc = atoi(m_strChSelect.Mid(p1,p2-p1));
			if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);			
		}
		else if(p2!=-1 && p3!=-1)
		{
			if( p2 < p3 )
			{
				long cyc = atoi(m_strChSelect.Mid(p1,p2-p1));
				if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
			}
			else if(p3<p2)
			{
				long from = atoi(m_strChSelect.Mid(p1, p3-p1));
				long to   = atoi(m_strChSelect.Mid(p3+1, p2-p3-1));
				for(long cyc=from; cyc<=to; cyc++)
				{
					if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
				}
			}
		}
		else if(p2==-1 && p3==-1)
		{
			CString str = m_strChSelect.Mid(p1);
			if(!str.IsEmpty())
			{
				long cyc = atoi(str);
				if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
			}
			break;
		}
		else if(p2==-1 && p3!=-1)
		{
			long from = atoi(m_strChSelect.Mid(p1, p3-p1));
			long to   = atoi(m_strChSelect.Mid(p3+1));
			for(long cyc=from; cyc<=to; cyc++)
			{
				if(m_ChList.Find(cyc)==NULL) m_ChList.AddTail(cyc);
			}
			break;
		}
		p1=p2+1;
	}
	return TRUE;
}
