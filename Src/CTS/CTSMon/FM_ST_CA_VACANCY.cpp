#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_CA_VACANCY::CFM_ST_CA_VACANCY(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_CALI(_Eqstid, _stid, _unit)
{
	
}


CFM_ST_CA_VACANCY::~CFM_ST_CA_VACANCY(void)
{
}

VOID CFM_ST_CA_VACANCY::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_VACANCY);
	
	// 1. Automatic 모드에서 10분동안 Vacancy 상태가 지속될 경우 미전송 파일을 확인한다.
	// CHANGE_FNID(FNID_PROC);
	
	//if(m_Unit->fnGetFMS()->fnGetSendMisState()) 
	//{
	//	//load
	//	if(m_Unit->fnGetFMS()->fnSendMisProc())
	//		CHANGE_STATE(AUTO_ST_END);
	//}
	//else
	//{
	//	
	//}
	TRACE("CFM_ST_CA_VACANCY::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_VACANCY::fnProc()
{
	/*
	if( 1 == m_iWaitCount )
	{
		AfxMessageBox("ProcTest");	
		CHANGE_FNID(FNID_EXIT);	
	}
	
	
	m_RetryMap |= RETRY_NOMAL_ON;
	*/

	TRACE("CFM_ST_CA_VACANCY::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_VACANCY::fnExit()
{
	/*
	if( 1 == m_iWaitCount )
	{
		AfxMessageBox("EndTest");	
		// CHANGE_FNID(FNID_EXIT);	
	}
	*/
	
	TRACE("CFM_ST_CA_VACANCY::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_VACANCY::fnSBCPorcess(WORD _state)
{
	CFM_ST_CALI::fnSBCPorcess(_state);

	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(CALI_ST_ERROR);
	}
        
	//TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(CALI_ST_TRAY_IN);
		}
		else
		{
			//CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(CALI_ST_READY);
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(CALI_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(CALI_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(CALI_ST_ERROR);
		}
		break;
	case EP_STATE_CALIBRATION:
		{
			CHANGE_STATE(CALI_ST_CALI);
		}
		break;
		
	}
	TRACE("CFM_ST_CA_VACANCY::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_CA_VACANCY::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;
// 
// 	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 1 )
// 	{
// 		st_S2F411 data;
// 		ZeroMemory(&data, sizeof(st_S2F411));
// 		memcpy(&data, _recvData->data, sizeof(st_S2F411));
// 
// 		_error = m_Unit->fnGetFMS()->fnReserveProc();		
// 
// 		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
// 
// 		if( _error == FMS_ER_NONE )
// 		{
// 			//m_Unit->fnGetFMS()->fnSendToHost_S6F11C12( 1, 0 );
// 		}
// 	}

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 8 )  //19-02-12 엄륭 MES 사양 추가
	{
		st_S2F418 data;
		ZeroMemory(&data, sizeof(st_S2F418));
		memcpy(&data, _recvData->data, sizeof(st_S2F418));

		//_error = m_Unit->fnGetFMS()->fnReserveProc();	KSH Calibration 20190830	
		_error = FMS_ER_NONE;  //KSH Calibration 20190830	

		//20190903 ksj
		CString trayID;
		CString strTemp;
		CString TrayBottom, TrayTop;
		TrayBottom.Format("%s",data.TRAYID_1);
		TrayTop.Format("%s",data.TRAYID_2);

		trayID.Format("%s%s",TrayBottom.TrimRight(" "), TrayTop.TrimRight(" "));
		m_Unit->fnGetFMS()->fnSetTrayID(trayID);
		int nModuleID = m_Unit->fnGetModuleID();

		strTemp.Format("StageLastTray1_%02d", nModuleID);
		AfxGetApp()->WriteProfileString(FMS_REG, strTemp, TrayBottom.TrimRight(" "));
		strTemp.Format("StageLastTray2_%02d",nModuleID);
		AfxGetApp()->WriteProfileString(FMS_REG, strTemp, TrayTop.TrimRight(" "));

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( _error == FMS_ER_NONE )
		{
			m_Unit->fnGetFMS()->fnSendToHost_S6F11C16(1, 0);
			int nModuleID = m_Unit->fnGetModuleID();
			EP_GP_STATE_DATA prevState;
			memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
			prevState.state = EP_STATE_READY;
			CFormModule *pModule = NULL;
			pModule = m_Unit->fnGetModule();
			pModule->SetState(prevState);	
		}
	}

	TRACE("CFM_ST_CA_VACANCY::fnFMSPorcess %d ErrorCode %d \n", m_Unit->fnGetModuleIdx(), _error );

	return _error;
}