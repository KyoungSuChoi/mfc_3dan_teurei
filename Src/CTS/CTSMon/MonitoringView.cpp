// MonitoringView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "MonitoringView.h"
#include "MainFrm.h"


// CMonitoringView

IMPLEMENT_DYNCREATE(CMonitoringView, CFormView)

CMonitoringView::CMonitoringView()
	: CFormView(CMonitoringView::IDD)
{
	m_nCurModuleID = 1;
	m_nCurTrayIndex = 0;
	m_nTopGridColCount = 4;
	m_nDisplayTimer = 0;

	m_pRowCol2GpCh = NULL;

	m_pConfig = NULL;
}

CMonitoringView::~CMonitoringView()
{
	if(m_pRowCol2GpCh)
	{
		delete[] m_pRowCol2GpCh;
		m_pRowCol2GpCh = NULL;
	}
}

void CMonitoringView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
}

BEGIN_MESSAGE_MAP(CMonitoringView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CMonitoringView 진단입니다.

#ifdef _DEBUG
void CMonitoringView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMonitoringView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSMonDoc* CMonitoringView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}
#endif
#endif //_DEBUG


// CMonitoringView 메시지 처리기입니다.

void CMonitoringView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	RECT rect;
	CRect ctrlRect;
	::GetClientRect(m_hWnd, &rect);

	if( GetDlgItem(ID_DLG_LABEL_NAME)->GetSafeHwnd() )
	{		
		GetDlgItem(ID_DLG_LABEL_NAME)->GetWindowRect(ctrlRect);
		GetDlgItem(ID_DLG_LABEL_NAME)->MoveWindow(rect.left, rect.top, rect.right, ctrlRect.Height(), FALSE);
	}

	if(::IsWindow(this->GetSafeHwnd()))
	{	
		if(m_wndSmallChGrid.GetSafeHwnd())
		{
			m_wndSmallChGrid.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			m_wndSmallChGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-5, rect.bottom-ctrlRect.top-5, FALSE);
		}
	}
}

void CMonitoringView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_pConfig = pDoc->GetTopConfig();

	InitLabel();	
	InitSmallChGrid();	
	SetTopGridFont();	
	GroupSetChanged();		//default Group Grid Display
	DrawChannel();			//Update Channel State Grid				
}

void CMonitoringView::InitLabel()
{
	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetBorder(TRUE);
	m_LabelViewName.SetText("Monitoring");
}

void CMonitoringView::InitSmallChGrid()
{
	m_wndSmallChGrid.SubclassDlgItem(IDC_LARGE_GRID, this);
	m_wndSmallChGrid.m_bSameRowSize = TRUE;
	m_wndSmallChGrid.m_bSameColSize = TRUE;
	m_wndSmallChGrid.m_bCustomWidth = FALSE;
	m_wndSmallChGrid.m_bCustomColor = FALSE;

	m_wndSmallChGrid.Initialize();
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);
	m_wndSmallChGrid.SetDefaultRowHeight(26);
	m_wndSmallChGrid.SetFrozenRows(1);

	//Enable Multi Channel Select
	m_wndSmallChGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndSmallChGrid.EnableGridToolTips();
	m_wndSmallChGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
		CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	//Use MemDc
	m_wndSmallChGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	//Row Header Setting
	m_wndSmallChGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));

}

void CMonitoringView::SetTopGridFont()
{
	//Font를 Registry에서 Laod
	UINT nSize;
	LPVOID* pData;
	LOGFONT lfGridFont;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(FORMSET_REG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nRtn && nSize > 0)
	{
		memcpy(&lfGridFont, pData, nSize);
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&lfGridFont);
		AfxGetApp()->WriteProfileBinary(FORMSET_REG_SECTION, "GridFont",(LPBYTE)&lfGridFont, sizeof(LOGFONT));
	}
	delete [] pData;

	m_wndSmallChGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont(lfGridFont)));
}

void CMonitoringView::GroupSetChanged(int nColCount)
{
	//Calculate Grid Info
	if(nColCount >0 && nColCount<= 64)
	{
		m_nTopGridColCount = nColCount;						//Update Top Grid Column Count
	}

	WORD nCount = 0, wChinGroup;

	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)	return;

	//Tray 단위 보기 
	wChinGroup = pModule->GetChInJig(m_nCurTrayIndex);

	nCount = (wChinGroup/m_nTopGridColCount);
	if(wChinGroup%m_nTopGridColCount >0)		nCount++;
	m_nTopGridRowCount = (int)nCount;					//Update Top Gird Row Count

	//Make Group And Channel Index Table
	if(m_pRowCol2GpCh)
	{
		delete[] m_pRowCol2GpCh;
		m_pRowCol2GpCh = NULL;
	}
	nCount= m_nTopGridRowCount*m_nTopGridColCount;
	//	ASSERT(nCount);
	m_pRowCol2GpCh = new SHORT[nCount];
	memset(m_pRowCol2GpCh, -1, nCount);

	nCount = 0;
	WORD nRowIndex = 0, nCellCout = 0;

	//Grid의 특정 위치에 Group 번호와 Channel 번호를 쉽게 찾기 위해 Mapping Table을 생성한다. 

	WORD nTrayType = GetDocument()->m_nViewTrayType;
	WORD j;
	switch (nTrayType)
	{
	case TRAY_TYPE_CUSTOMER:
	case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
	case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nRowIndex++;
			
			if(nRowIndex >= m_nTopGridRowCount)
			{
				nCount++;
				nRowIndex = 0;
			}
		}
		break;
	case TRAY_TYPE_PB5_COL_ROW:	//가로 번호 체계(각형)
		for( j=0; j<wChinGroup; j++)
		{
			nCellCout = nRowIndex*m_nTopGridColCount+nCount;
			m_pRowCol2GpCh[nCellCout] = j;
			nCount++;
			
			if(nCount >= m_nTopGridColCount)
			{
				nCount=0;
				nRowIndex++;
			}
		}
		break;
	}	

	// 모둘에 할당된 지그(Tray) 숫자 가져오기
	int nCnt = pModule->GetTotalJig();
	
	//////////////////////////////////////////////////////////////////////////
	//Draw small channel display grid
	BOOL bLock = m_wndSmallChGrid.LockUpdate();
	m_wndSmallChGrid.SetRowCount(1);
	m_wndSmallChGrid.SetColCount(1);		//Delete Grid Attribute
	
	UINT nRow = 0, nCol = 0;
	int n = 0;
	for(n = 0; n<nCnt; n++)
	{
		if(nRow < pModule->GetChInJig(m_nCurTrayIndex))
		{
			nRow = pModule->GetChInJig(m_nCurTrayIndex);
		}
	}

	nCol = nCnt * 4;

	if(m_wndSmallChGrid.GetRowCount() != nRow+1 || m_wndSmallChGrid.GetColCount() != nCol+1)
	{
		m_wndSmallChGrid.SetRowCount(nRow+1);		//ReDraw 
		m_wndSmallChGrid.SetColCount(nCol+1);
		m_wndSmallChGrid.SetCoveredCellsRowCol(0, 1, 1, 1);

		CString strTemp;		
		for( n=0; n<pModule->GetTotalJig(); n++)
		{
			strTemp.Format("TRAY %d", n+1);
			m_wndSmallChGrid.SetCoveredCellsRowCol(0, (n+1)*2, 0, (n+1)*2+3);
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2), 
												CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE))
											);

			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2), 
												CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
														.SetHorizontalAlignment(DT_CENTER)
														.SetDraw3dFrame(gxFrameRaised)
														.SetInterior(GetSysColor(COLOR_3DFACE))
														.SetValue(1L)
											);

			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2+1), 
												CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
														.SetHorizontalAlignment(DT_CENTER)
														.SetDraw3dFrame(gxFrameRaised)
														.SetInterior(GetSysColor(COLOR_3DFACE))
														.SetValue(2L)
											);

			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2+2), 
												CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
														.SetHorizontalAlignment(DT_CENTER)
														.SetDraw3dFrame(gxFrameRaised)
														.SetInterior(GetSysColor(COLOR_3DFACE))
														.SetValue(3L)
											);

			m_wndSmallChGrid.SetStyleRange(CGXRange(1, (n+1)*2+3), 
												CGXStyle().SetControl(GX_IDS_CTRL_ZEROBASED_EX)
														.SetHorizontalAlignment(DT_CENTER)
														.SetDraw3dFrame(gxFrameRaised)
														.SetInterior(GetSysColor(COLOR_3DFACE))
														.SetValue(5L)
											);


			COLORREF boldColor = RGB(255, 192, 0);

			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2, 0, (n+1)*2+3), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2, nRow+1, (n+1)*2), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2, nRow+1, (n+1)*2), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2+1, nRow+1, (n+1)*2+1), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2+2, nRow+1, (n+1)*2+2), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(0, (n+1)*2+3, nRow+1, (n+1)*2+3), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(boldColor)));
			m_wndSmallChGrid.SetStyleRange(CGXRange(nRow+1, (n+1)*2, nRow+1, (n+1)*2+3), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(boldColor)));
		}

		m_wndSmallChGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CH(/)").SetFont(CGXFont().SetBold(TRUE)));

		for (int a = 1; a <=nRow; a++)
		{
			m_wndSmallChGrid.SetStyleRange(CGXRange(a+1, 1), CGXStyle().SetValue(ROWCOL(a)).SetDraw3dFrame(gxFrameRaised));
		}
	}
	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();
	//////////////////////////////////////////////////////////////////////////	
}

void CMonitoringView::DrawChannel()
{	
	BYTE colorFlag = 0;
	CString  strMsg, strCode, strToolTip, strUnit;

	CCTSMonDoc *pDoc = GetDocument();
	float	fTemp = 0;

	CFormModule *pModule;
	pModule =  pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		
	{
		return;
	}

	ROWCOL nRow = 0, nCol = 0;
	int nChannelNo = 0;
	int nChannelIndex;	
	STR_SAVE_CH_DATA chSaveData;	

	//Small channel grid display
	//////////////////////////////////////////////////////////////////////////
	//	CTray *pTray;	
	bool bLock = m_wndSmallChGrid.LockUpdate();
	for (INDEX nTrayIndex= 0; nTrayIndex < pModule->GetTotalJig(); nTrayIndex++)	//Group 수 만큼 
	{
		nChannelNo = pModule->GetChInJig(nTrayIndex);
		
		//Cell 순서 대로 표시 한다.
		for (INDEX nMonitorIndex = 0; nMonitorIndex < nChannelNo; nMonitorIndex++)				//Display Channel Ch Per Group
		{	
			nChannelIndex = nMonitorIndex;
			pModule->GetChannelStepData(chSaveData, nChannelIndex, nTrayIndex, pModule->GetCurStepNo());

			int nCurItem = EP_VOLTAGE;

			for(int item = 0; item < 4; item++)
			{
				strMsg = pDoc->GetStateMsg(chSaveData.state, colorFlag);										//Get State Msg
				strCode = pDoc->ChCodeMsg(chSaveData.channelCode);

				switch(item)
				{
				case EP_STATE:		
					break;
				case EP_VOLTAGE:		//voltage
					strMsg = pDoc->ValueString(chSaveData.fVoltage, EP_VOLTAGE);
					strUnit = pDoc->m_strVUnit;
					break;

				case EP_CURRENT:		//current
					strMsg = pDoc->ValueString(chSaveData.fCurrent, EP_CURRENT);
					strUnit = pDoc->m_strIUnit;
					break;

				case EP_CAPACITY:		//capacity
					strMsg = pDoc->ValueString(chSaveData.fCapacity, EP_CAPACITY);
					strUnit = pDoc->m_strCUnit;
					break;

				case EP_CH_CODE:	//failureCode
					strMsg = strCode;
					break;

				default:
					strMsg.Format("-");
					break;

				}
				
				if(pDoc->m_bHideNonCellData && (chSaveData.channelCode == EP_CODE_CELL_NONE || chSaveData.channelCode == EP_CODE_NONCELL))
				{
					strMsg.Empty();
				}
			}
		}
		
		//1열 배열 표기시 Column Heaer에 Tray 번호 표시 
		strMsg = pModule->GetTrayNo(nTrayIndex);
		if(strMsg.IsEmpty())
		{
			strMsg.Format("TRAY %d", nTrayIndex+1);
		}
		m_wndSmallChGrid.SetValueRange(CGXRange(0, (nTrayIndex+1)*2), strMsg);
	}
	m_wndSmallChGrid.LockUpdate(bLock);
	m_wndSmallChGrid.Redraw();
}

void CMonitoringView::StartMonitoring()
{		
	/*
	if(EPGetGroupState(m_nCurModuleID) == EP_STATE_LINE_OFF)	
	{
		return;
	}	
	
	if(((CMainFrame *)AfxGetMainWnd())->CompareCurrentSelectTab(CMainFrame::TAB_INDEX_TOP) == FALSE)
	{
		return;
	}

	if(m_nDisplayTimer)	
	{
		return;
	}
	
	int nTimer = 2000;
	if(m_pConfig)
	{
		nTimer = m_pConfig->m_ChannelInterval*1000;
	}	
	m_nDisplayTimer = SetTimer(102, nTimer, NULL);
	*/
}

void CMonitoringView::StopMonitoring()
{
	if(m_nDisplayTimer == 0)	return;
	KillTimer(m_nDisplayTimer);
	m_nDisplayTimer = 0;
}

void CMonitoringView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == 102 )
	{	
		DrawChannel();
	}

	CFormView::OnTimer(nIDEvent);
}
