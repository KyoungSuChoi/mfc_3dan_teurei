#if !defined(AFX_SENSORDATADLG_H__069BD939_3C03_412C_9854_2C115217F0EA__INCLUDED_)
#define AFX_SENSORDATADLG_H__069BD939_3C03_412C_9854_2C115217F0EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SensorDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg dialog
#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "SensorMapDlg.h"
#include "afxwin.h"
#include "InformationDlg.h"

#define SENSOR_COLUMN_MODULE_NO					0
#define SENSOR_COLUMN_UNIT_NAME					1
#define SENSOR_COLUMN_SENSOR_TYPE				2	
#define SENSOR_COLUMN_TEMP_CURRENT				3
#define SENSOR_COLUMN_TEMP_CURRENT_OFFSET		4
#define SENSOR_COLUMN_TEMP_CALI_JIG_TEMP		5
#define SENSOR_COLUMN_TEMP_CALI_TRAY_TEMP		6
#define SENSOR_COLUMN_TEMP_CALI_OFFSET			7
#define SENSOR_COLUMN_TEMP_CALI_OFFSET_ENABLE	8
#define SENSOR_COLUMN_TEMP_MAX					9
#define SENSOR_COLUMN_TEMP_MIN					10
#define SENSOR_COLUMN_TEMP_LIMIT_USE			11	
#define SENSOR_COLUMN_TEMP_LIMIT_VALUE			12
#define SENSOR_COLUMN_SENSOR_NO					13	// Initialize할 때 자체적으로 Count해서 Set해준다. 왜 쓰는지...?
#define SENSOR_COLUMN_SENSOR_ITEM				14	// Type과 Item은 뭐가 틀린지..?

class CSensorDataDlg : public CDialog
{
// 20210614 KSCHOI Add Temperature Calibration Function START
private :
	float	m_pJigTempData[MAX_SBC_COUNT][EP_MAX_SENSOR_CH];
	float	m_pTempCaliData[MAX_SBC_COUNT][EP_MAX_SENSOR_CH];
	float	m_pTempCaliOffsetData[MAX_SBC_COUNT][EP_MAX_SENSOR_CH];
// 20210614 KSCHOI Add Temperature Calibration Function START
public:
	virtual ~CSensorDataDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

// Construction
public:
	BOOL m_bLockUpdate;
	void ReDrawGrid();
	int m_nModuleID;
	CString m_strModuleName;
// KSCHOI 20201103 Display TemperatureCharacter by Locale START
	CString m_csTemperatureSpecialCharacter;
	CString CalcTemperatureChracterByLocale();
// KSCHOI 20201103 Display TemperatureCharacter by Locale END
	void	UpdateGridData();
	void	fnSelModuleNum( int nModuleID = 1 );
	int m_nInstallModuleNum;
	CMyGridWnd m_wndTempGrid;
	CSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent = NULL);   // standard constructor
	CSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleID); // 20200520 KSCHOI TEMPERATURE SENSOR BY MODULE

// Dialog Data
	//{{AFX_DATA(CSensorDataDlg)
	enum { IDD = IDD_SENSOR_DATA_DLG };
	CXPButton	m_btnOK;
	CXPButton	m_btnExcel;
	CXPButton	m_btnPrint;
	CXPButton	m_btnMap;
// 20210614 KSCHOI Add Temperature Calibration Function START
	CXPButton	m_btnStopTempCali;
	CXPButton	m_btnRunTempCali;
	CXPButton	m_btnApplyCaliOffset;
	CInformationDlg* m_pInformationDlg;
// 20210614 KSCHOI Add Temperature Calibration Function END

	CComboBox	m_ctrlMDSelCombo;
	CLabel	m_ctrlGasMin;
	CLabel	m_ctrlGasMax;
	CLabel	m_ctrlTempMin;
	CLabel	m_ctrlTempMax;

	CSensorMapDlg *m_pSensorMap;	//20200831 ksj
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSensorDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void InitTempGrid();
	CCTSMonDoc *m_pDoc;

	// Generated message map functions
	//{{AFX_MSG(CSensorDataDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnOK();
	afx_msg void OnMapButton();
	afx_msg void OnSelchangeModuleSelCombo();
	afx_msg void OnPrintButton();
	afx_msg void OnExcelSaveButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_cTemperaturePicture;
	afx_msg void OnBnClickedBtnTemperatureCalibrationRun();
	afx_msg void OnBnClickedBtnTemperatureCalibrationOffsetApply();
	afx_msg void OnBnClickedBtnTemperatureCalibrationStop();
// 20210614 KSCHOI Add Temperature Calibration Function START
	void	UpdateButtonControl();
	BOOL	OnOneSensorTempCaliEndNotify(int iModuleID, EP_CMD_NOTIFY_ONE_SENSOR_TEMPERATURE_CALIBRATION_END pOneSensorTempCaliEndData);
	BOOL	OnAllSensorTempCaliEndNotify(int iModuleID);
	void	ShowInformation( int nUnitNum, CString strMsg, int nMsgType );
// 20210614 KSCHOI Add Temperature Calibration Function END
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENSORDATADLG_H__069BD939_3C03_412C_9854_2C115217F0EA__INCLUDED_)
