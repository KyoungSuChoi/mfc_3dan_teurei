#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_RUN::CFM_ST_AT_RUN(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
}


CFM_ST_AT_RUN::~CFM_ST_AT_RUN(void)
{
}

VOID CFM_ST_AT_RUN::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_RUNNING);
	
	CHANGE_FNID(FNID_PROC);	

	TRACE("CFM_ST_AT_RUN::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_RUN::fnProc()
{
	//공정진행 중

	TRACE("CFM_ST_AT_RUN::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_RUN::fnExit()
{
	//공정 완료

	TRACE("CFM_ST_AT_RUN::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_RUN::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

//	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				//CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			// 20160819 kky
			// 1. 충방전기 지그 상태가 Open 된 경우 END 신호 전송
			EP_GP_DATA gpData = EPGetGroupData(m_Unit->fnGetModuleID(), 0);

			IO_Data RecvIOData;
			memcpy( &RecvIOData, &gpData.gpState.ManualFunc1, sizeof(IO_Data) );

			if( RecvIOData.ch_2 == 0 )
			{
				m_Unit->fnGetFMS()->fnSendToHost_S6F11C4( 1, 0 );	//RUN -> END 갈때만 결과데이터 보고하도록 변경 //20200513 KSJ 20200825 엄륭 수정
				CHANGE_STATE(AUTO_ST_END);
			}
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	}
//	TRACE("CFM_ST_AT_RUN::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_RUN::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 6 && _recvData->head.nFunc == 12 )
	{
		_error = FMS_ER_NONE;
	}

	//입고완료
// 	switch(_msgId)
// 	{
// 
// 	default:
// 		{
// 		}
// 		break;
// 	}
	TRACE("CFM_ST_AT_RUN::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}