// TempColorConfig.cpp : implementation file
//

#include "stdafx.h"
#include "TempColorConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTempColorConfig dialog
CTempColorConfig::CTempColorConfig(CCTSMonDoc *pDoc, CWnd* pParent)
	: CDialog(CTempColorConfig::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTempColorConfig)
	m_fJigMaxLevel1 = 0.0f;
	m_fJigMaxLevel2 = 0.0f;
	m_fJigMaxLevel3 = 0.0f;
	m_fJigMaxLevel4 = 0.0f;
	m_fJigMaxLevel5 = 0.0f;
	m_fJigMaxLevel6 = 0.0f;
	m_fJigMaxLevel7 = 0.0f;
	m_fJigMaxLevel8 = 0.0f;
	m_fJigMinLevel1 = 0.0f;
	m_fJigMinLevel2 = 0.0f;
	m_fJigMinLevel3 = 0.0f;
	m_fJigMinLevel4 = 0.0f;
	m_fJigMinLevel5 = 0.0f;
	m_fJigMinLevel6 = 0.0f;
	m_fJigMinLevel7 = 0.0f;
	m_fJigMinLevel8 = 0.0f;
	m_fUnitMaxLevel1 = 0.0f;
	m_fUnitMaxLevel2 = 0.0f;
	m_fUnitMaxLevel3 = 0.0f;
	m_fUnitMaxLevel4 = 0.0f;
	m_fUnitMaxLevel5 = 0.0f;
	m_fUnitMaxLevel6 = 0.0f;
	m_fUnitMaxLevel7 = 0.0f;
	m_fUnitMaxLevel8 = 0.0f;
	m_fUnitMinLevel1 = 0.0f;
	m_fUnitMinLevel2 = 0.0f;
	m_fUnitMinLevel3 = 0.0f;
	m_fUnitMinLevel4 = 0.0f;
	m_fUnitMinLevel5 = 0.0f;
	m_fUnitMinLevel6 = 0.0f;
	m_fUnitMinLevel7 = 0.0f;
	m_fUnitMinLevel8 = 0.0f;	
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
}


void CTempColorConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTempColorConfig)
	DDX_Control(pDX, IDOK, m_BtnOk);
	DDX_Control(pDX, IDCANCEL, m_BtnCancel);
	DDX_Control(pDX, IDC_BTN_INITIAL, m_BtnInitail);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL1, m_fJigMaxLevel1);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL2, m_fJigMaxLevel2);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL3, m_fJigMaxLevel3);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL4, m_fJigMaxLevel4);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL5, m_fJigMaxLevel5);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL6, m_fJigMaxLevel6);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL7, m_fJigMaxLevel7);
	DDX_Text(pDX, IDC_JIGMAX_LEVEL8, m_fJigMaxLevel8);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL1, m_fJigMinLevel1);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL2, m_fJigMinLevel2);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL3, m_fJigMinLevel3);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL4, m_fJigMinLevel4);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL5, m_fJigMinLevel5);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL6, m_fJigMinLevel6);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL7, m_fJigMinLevel7);
	DDX_Text(pDX, IDC_JIGMIN_LEVEL8, m_fJigMinLevel8);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL1, m_fUnitMaxLevel1);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL2, m_fUnitMaxLevel2);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL3, m_fUnitMaxLevel3);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL4, m_fUnitMaxLevel4);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL5, m_fUnitMaxLevel5);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL6, m_fUnitMaxLevel6);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL7, m_fUnitMaxLevel7);
	DDX_Text(pDX, IDC_UNITMAX_LEVEL8, m_fUnitMaxLevel8);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL1, m_fUnitMinLevel1);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL2, m_fUnitMinLevel2);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL3, m_fUnitMinLevel3);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL4, m_fUnitMinLevel4);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL5, m_fUnitMinLevel5);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL6, m_fUnitMinLevel6);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL7, m_fUnitMinLevel7);
	DDX_Text(pDX, IDC_UNITMIN_LEVEL8, m_fUnitMinLevel8);	
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTempColorConfig, CDialog)
	//{{AFX_MSG_MAP(CTempColorConfig)
	ON_BN_CLICKED(IDC_BTN_INITIAL, OnBtnInitial)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTempColorConfig message handlers

BOOL CTempColorConfig::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here

	m_Unit_Color8.AttachButton(IDC_UNITTEMP_COLOR_LEVEL8, this);
	m_Unit_Color7.AttachButton(IDC_UNITTEMP_COLOR_LEVEL7, this);
	m_Unit_Color6.AttachButton(IDC_UNITTEMP_COLOR_LEVEL6, this);
	m_Unit_Color5.AttachButton(IDC_UNITTEMP_COLOR_LEVEL5, this);
	m_Unit_Color4.AttachButton(IDC_UNITTEMP_COLOR_LEVEL4, this);
	m_Unit_Color3.AttachButton(IDC_UNITTEMP_COLOR_LEVEL3, this);
	m_Unit_Color2.AttachButton(IDC_UNITTEMP_COLOR_LEVEL2, this);
	m_Unit_Color1.AttachButton(IDC_UNITTEMP_COLOR_LEVEL1, this);
	m_Jig_Color8.AttachButton(IDC_JIGTEMP_COLOR_LEVEL8, this);
	m_Jig_Color7.AttachButton(IDC_JIGTEMP_COLOR_LEVEL7, this);
	m_Jig_Color6.AttachButton(IDC_JIGTEMP_COLOR_LEVEL6, this);
	m_Jig_Color5.AttachButton(IDC_JIGTEMP_COLOR_LEVEL5, this);
	m_Jig_Color4.AttachButton(IDC_JIGTEMP_COLOR_LEVEL4, this);
	m_Jig_Color3.AttachButton(IDC_JIGTEMP_COLOR_LEVEL3, this);
	m_Jig_Color2.AttachButton(IDC_JIGTEMP_COLOR_LEVEL2, this);
	m_Jig_Color1.AttachButton(IDC_JIGTEMP_COLOR_LEVEL1, this);		
	
	m_Unit_Color1.SetColor(m_TempConfig.m_UnitColorlevel[0]);
	m_Unit_Color2.SetColor(m_TempConfig.m_UnitColorlevel[1]);
	m_Unit_Color3.SetColor(m_TempConfig.m_UnitColorlevel[2]);
	m_Unit_Color4.SetColor(m_TempConfig.m_UnitColorlevel[3]);
	m_Unit_Color5.SetColor(m_TempConfig.m_UnitColorlevel[4]);
	m_Unit_Color6.SetColor(m_TempConfig.m_UnitColorlevel[5]);
	m_Unit_Color7.SetColor(m_TempConfig.m_UnitColorlevel[6]);
	m_Unit_Color8.SetColor(m_TempConfig.m_UnitColorlevel[7]);
	
	m_Jig_Color1.SetColor(m_TempConfig.m_JigColorlevel[0]);
	m_Jig_Color2.SetColor(m_TempConfig.m_JigColorlevel[1]);
	m_Jig_Color3.SetColor(m_TempConfig.m_JigColorlevel[2]);
	m_Jig_Color4.SetColor(m_TempConfig.m_JigColorlevel[3]);
	m_Jig_Color5.SetColor(m_TempConfig.m_JigColorlevel[4]);
	m_Jig_Color6.SetColor(m_TempConfig.m_JigColorlevel[5]);
	m_Jig_Color7.SetColor(m_TempConfig.m_JigColorlevel[6]);
	m_Jig_Color8.SetColor(m_TempConfig.m_JigColorlevel[7]);
	
	m_fUnitMinLevel1 = m_TempConfig.m_fUnitlevel1[0];
	m_fUnitMinLevel2 = m_TempConfig.m_fUnitlevel2[0];
	m_fUnitMinLevel3 = m_TempConfig.m_fUnitlevel3[0];
	m_fUnitMinLevel4 = m_TempConfig.m_fUnitlevel4[0];
	m_fUnitMinLevel5 = m_TempConfig.m_fUnitlevel5[0];
	m_fUnitMinLevel6 = m_TempConfig.m_fUnitlevel6[0];
	m_fUnitMinLevel7 = m_TempConfig.m_fUnitlevel7[0];
	m_fUnitMinLevel8 = m_TempConfig.m_fUnitlevel8[0];

	m_fUnitMaxLevel1 = m_TempConfig.m_fUnitlevel1[1];
	m_fUnitMaxLevel2 = m_TempConfig.m_fUnitlevel2[1];
	m_fUnitMaxLevel3 = m_TempConfig.m_fUnitlevel3[1];
	m_fUnitMaxLevel4 = m_TempConfig.m_fUnitlevel4[1];
	m_fUnitMaxLevel5 = m_TempConfig.m_fUnitlevel5[1];
	m_fUnitMaxLevel6 = m_TempConfig.m_fUnitlevel6[1];
	m_fUnitMaxLevel7 = m_TempConfig.m_fUnitlevel7[1];
	m_fUnitMaxLevel8 = m_TempConfig.m_fUnitlevel8[1];

	m_fJigMinLevel1 = m_TempConfig.m_fJiglevel1[0];
	m_fJigMinLevel2 = m_TempConfig.m_fJiglevel2[0];
	m_fJigMinLevel3 = m_TempConfig.m_fJiglevel3[0];
	m_fJigMinLevel4 = m_TempConfig.m_fJiglevel4[0];
	m_fJigMinLevel5 = m_TempConfig.m_fJiglevel5[0];
	m_fJigMinLevel6 = m_TempConfig.m_fJiglevel6[0];
	m_fJigMinLevel7 = m_TempConfig.m_fJiglevel7[0];
	m_fJigMinLevel8 = m_TempConfig.m_fJiglevel8[0];

	m_fJigMaxLevel1 = m_TempConfig.m_fJiglevel1[1];
	m_fJigMaxLevel2 = m_TempConfig.m_fJiglevel2[1];
	m_fJigMaxLevel3 = m_TempConfig.m_fJiglevel3[1];
	m_fJigMaxLevel4 = m_TempConfig.m_fJiglevel4[1];
	m_fJigMaxLevel5 = m_TempConfig.m_fJiglevel5[1];
	m_fJigMaxLevel6 = m_TempConfig.m_fJiglevel6[1];
	m_fJigMaxLevel7 = m_TempConfig.m_fJiglevel7[1];
	m_fJigMaxLevel8 = m_TempConfig.m_fJiglevel8[1];
	UpdateData(FALSE);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTempColorConfig::OnOK() 
{
	// TODO: Add extra validation here	
	UpdateData(TRUE);
	m_TempConfig.m_UnitColorlevel[0] = m_Unit_Color1.GetColor();
	m_TempConfig.m_UnitColorlevel[1] = m_Unit_Color2.GetColor();
	m_TempConfig.m_UnitColorlevel[2] = m_Unit_Color3.GetColor();
	m_TempConfig.m_UnitColorlevel[3] = m_Unit_Color4.GetColor();
	m_TempConfig.m_UnitColorlevel[4] = m_Unit_Color5.GetColor();
	m_TempConfig.m_UnitColorlevel[5] = m_Unit_Color6.GetColor();
	m_TempConfig.m_UnitColorlevel[6] = m_Unit_Color7.GetColor();
	m_TempConfig.m_UnitColorlevel[7] = m_Unit_Color8.GetColor();
	
	m_TempConfig.m_JigColorlevel[0] = m_Jig_Color1.GetColor();
	m_TempConfig.m_JigColorlevel[1] = m_Jig_Color2.GetColor();
	m_TempConfig.m_JigColorlevel[2] = m_Jig_Color3.GetColor();
	m_TempConfig.m_JigColorlevel[3] = m_Jig_Color4.GetColor();
	m_TempConfig.m_JigColorlevel[4] = m_Jig_Color5.GetColor();
	m_TempConfig.m_JigColorlevel[5] = m_Jig_Color6.GetColor();
	m_TempConfig.m_JigColorlevel[6] = m_Jig_Color7.GetColor();
	m_TempConfig.m_JigColorlevel[7] = m_Jig_Color8.GetColor();
	
	m_TempConfig.m_fUnitlevel1[0] = m_fUnitMinLevel1;
	m_TempConfig.m_fUnitlevel2[0] = m_fUnitMinLevel2;
	m_TempConfig.m_fUnitlevel3[0] = m_fUnitMinLevel3;
	m_TempConfig.m_fUnitlevel4[0] = m_fUnitMinLevel4;
	m_TempConfig.m_fUnitlevel5[0] = m_fUnitMinLevel5;
	m_TempConfig.m_fUnitlevel6[0] = m_fUnitMinLevel6;
	m_TempConfig.m_fUnitlevel7[0] = m_fUnitMinLevel7;
	m_TempConfig.m_fUnitlevel8[0] = m_fUnitMinLevel8;
	
	m_TempConfig.m_fUnitlevel1[1] = m_fUnitMaxLevel1;
	m_TempConfig.m_fUnitlevel2[1] = m_fUnitMaxLevel2;
	m_TempConfig.m_fUnitlevel3[1] = m_fUnitMaxLevel3;
	m_TempConfig.m_fUnitlevel4[1] = m_fUnitMaxLevel4;
	m_TempConfig.m_fUnitlevel5[1] = m_fUnitMaxLevel5;
	m_TempConfig.m_fUnitlevel6[1] = m_fUnitMaxLevel6;
	m_TempConfig.m_fUnitlevel7[1] = m_fUnitMaxLevel7;
	m_TempConfig.m_fUnitlevel8[1] = m_fUnitMaxLevel8;
	
	m_TempConfig.m_fJiglevel1[0] = m_fJigMinLevel1;
	m_TempConfig.m_fJiglevel2[0] = m_fJigMinLevel2;
	m_TempConfig.m_fJiglevel3[0] = m_fJigMinLevel3;
	m_TempConfig.m_fJiglevel4[0] = m_fJigMinLevel4;
	m_TempConfig.m_fJiglevel5[0] = m_fJigMinLevel5;
	m_TempConfig.m_fJiglevel6[0] = m_fJigMinLevel6;
	m_TempConfig.m_fJiglevel7[0] = m_fJigMinLevel7;
	m_TempConfig.m_fJiglevel8[0] = m_fJigMinLevel8;
	
	m_TempConfig.m_fJiglevel1[1] = m_fJigMaxLevel1;
	m_TempConfig.m_fJiglevel2[1] = m_fJigMaxLevel2;
	m_TempConfig.m_fJiglevel3[1] = m_fJigMaxLevel3;
	m_TempConfig.m_fJiglevel4[1] = m_fJigMaxLevel4;
	m_TempConfig.m_fJiglevel5[1] = m_fJigMaxLevel5;
	m_TempConfig.m_fJiglevel6[1] = m_fJigMaxLevel6;
	m_TempConfig.m_fJiglevel7[1] = m_fJigMaxLevel7;
	m_TempConfig.m_fJiglevel8[1] = m_fJigMaxLevel8;

	m_pDoc->m_TempConfig = m_TempConfig;
	::WriteTempConfig( m_TempConfig );		
	CDialog::OnOK();
}

void CTempColorConfig::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CTempColorConfig::OnBtnInitial() 
{
	// TODO: Add your control notification handler code here
	m_Unit_Color1.SetColor(0);
	m_Unit_Color2.SetColor(0);
	m_Unit_Color3.SetColor(0);
	m_Unit_Color4.SetColor(0);
	m_Unit_Color5.SetColor(0);
	m_Unit_Color6.SetColor(0);
	m_Unit_Color7.SetColor(0);
	m_Unit_Color8.SetColor(0);
	
	m_Jig_Color1.SetColor(0);
	m_Jig_Color2.SetColor(0);
	m_Jig_Color3.SetColor(0);
	m_Jig_Color4.SetColor(0);
	m_Jig_Color5.SetColor(0);
	m_Jig_Color6.SetColor(0);
	m_Jig_Color7.SetColor(0);
	m_Jig_Color8.SetColor(0);	
	
	m_fUnitMinLevel1 = 0;
	m_fUnitMinLevel2 = 0;
	m_fUnitMinLevel3 = 0;
	m_fUnitMinLevel4 = 0;
	m_fUnitMinLevel5 = 0;
	m_fUnitMinLevel6 = 0;
	m_fUnitMinLevel7 = 0;
	m_fUnitMinLevel8 = 0;
	
	m_fUnitMaxLevel1 = 0;
	m_fUnitMaxLevel2 = 0;
	m_fUnitMaxLevel3 = 0;
	m_fUnitMaxLevel4 = 0;
	m_fUnitMaxLevel5 = 0;
	m_fUnitMaxLevel6 = 0;
	m_fUnitMaxLevel7 = 0;
	m_fUnitMaxLevel8 = 0;
	
	m_fJigMinLevel1 = 0;
	m_fJigMinLevel2 = 0;
	m_fJigMinLevel3 = 0;
	m_fJigMinLevel4 = 0;
	m_fJigMinLevel5 = 0;
	m_fJigMinLevel6 = 0;
	m_fJigMinLevel7 = 0;
	m_fJigMinLevel8 = 0;
	
	m_fJigMaxLevel1 = 0;
	m_fJigMaxLevel2 = 0;
	m_fJigMaxLevel3 = 0;
	m_fJigMaxLevel4 = 0;
	m_fJigMaxLevel5 = 0;
	m_fJigMaxLevel6 = 0;
	m_fJigMaxLevel7 = 0;
	m_fJigMaxLevel8 = 0;
	UpdateData(false);	
}
