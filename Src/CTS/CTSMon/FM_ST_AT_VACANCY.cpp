#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_VACANCY::CFM_ST_AT_VACANCY(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit), m_ulNetworkCheckTimeInterval(600000), m_ulNetworkCheckTime(0)
{
	
}


CFM_ST_AT_VACANCY::~CFM_ST_AT_VACANCY(void)
{
}

VOID CFM_ST_AT_VACANCY::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_VACANCY);
	
	m_Unit->fnGetModule()->m_bRackToRack = false; //20200914 ksj 랙간이동 플래그 : 랙간이동시 공정 시작불가
	m_Unit->fnGetModule()->m_bProcessStart = false; //20200914 ksj  공정시작 플래그 : 공정 시작시 랙간이동불가 //공정이 시작했거나, 지그플래이트 및 지그가 Close 되있을때 On

	// 1. Automatic 모드에서 10분동안 Vacancy 상태가 지속될 경우 미전송 파일을 확인한다.
	// CHANGE_FNID(FNID_PROC);
	
	//if(m_Unit->fnGetFMS()->fnGetSendMisState()) 
	//{
	//	//load
	//	if(m_Unit->fnGetFMS()->fnSendMisProc())
	//		CHANGE_STATE(AUTO_ST_END);
	//}
	//else
	//{
	//	
	//}
	//TRACE("CFM_ST_AT_VACANCY::fnEnter %d \n", m_Unit->fnGetModuleIdx());

// 20210105 KSCHOI Add SBCNetworkCheck START
	this->fnSBCNetworkStatusCheck();
// 20210105 KSCHOI Add SBCNetworkCheck END
}

VOID CFM_ST_AT_VACANCY::fnProc()
{
	/*
	if( 1 == m_iWaitCount )
	{
		AfxMessageBox("ProcTest");	
		CHANGE_FNID(FNID_EXIT);	
	}
	
	
	m_RetryMap |= RETRY_NOMAL_ON;
	*/

	TRACE("CFM_ST_AT_VACANCY::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_VACANCY::fnExit()
{
	/*
	if( 1 == m_iWaitCount )
	{
		AfxMessageBox("EndTest");	
		// CHANGE_FNID(FNID_EXIT);	
	}
	*/
	
	TRACE("CFM_ST_AT_VACANCY::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_VACANCY::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	//TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			//CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(AUTO_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
		
	}
	//TRACE("CFM_ST_AT_VACANCY::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_VACANCY::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 1 )
	{
		st_S2F411 data;
		ZeroMemory(&data, sizeof(st_S2F411));
		memcpy(&data, _recvData->data, sizeof(st_S2F411));

// 20210115 KSCHOI Check fnReserveProc Elapsed Time START
// 		_error = m_Unit->fnGetFMS()->fnReserveProc();
// 
// 		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
		ULONGLONG ulStartTime =  GetTickCount64();
		_error = m_Unit->fnGetFMS()->fnReserveProc();
		int iElapsedTime = GetTickCount64() - ulStartTime;

		if(iElapsedTime > 5000)
		{ 
			CFMSLog::WriteErrorLog("[Stage %s] fnReserveProc Elapsed Time Is Over. Elapsed Time %d ms", m_Unit->fnGetModuleName(), iElapsedTime);
			_error = ER_SBC_NetworkError;

			EPSendCommand(m_Unit->fnGetModuleID(), 0, 0, EP_CMD_CLEAR);

			m_Unit->fnGetModule()->GetTrayInfo(0)->m_nResultFileType = EP_RESULTFILE_TYPE_NORMAL;	
			m_Unit->fnGetModule()->UpdateTestLogHeaderTempFile();			
			m_Unit->fnGetModule()->ResetGroupData();
			m_Unit->fnGetModule()->ClearTestLogTempFile();

			EPSendCommand(m_Unit->fnGetModuleID(), 0, 0, EP_CMD_SBC_SOCKET_RECONNECT_REQUEST );
		}
	 
		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
// 20210115 KSCHOI Check fnReserveProc Elapsed Time END

		if( _error == FMS_ER_NONE )
		{
			//m_Unit->fnGetFMS()->fnSendToHost_S6F11C12( 1, 0 );
		}
	}

// 	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 8 )  //19-02-12 엄륭 MES 사양 추가
// 	{
// 		st_S2F418 data;
// 		ZeroMemory(&data, sizeof(st_S2F418));
// 		memcpy(&data, _recvData->data, sizeof(st_S2F418));
// 
// 		//_error = m_Unit->fnGetFMS()->fnReserveProc();	KSH Calibration 20190830	
// 		_error = FMS_ER_NONE;  //KSH Calibration 20190830	
// 
// 		
// 		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
// 
// 		if( _error == FMS_ER_NONE )
// 		{
// 			m_Unit->fnGetFMS()->fnSendToHost_S6F11C16(1, 0);
// 		}
// 	}

	TRACE("CFM_ST_AT_VACANCY::fnFMSPorcess %d ErrorCode %d \n", m_Unit->fnGetModuleIdx(), _error );

	return _error;
}

// 20210105 KSCHOI Add SBCNetworkCheck START
BOOL CFM_ST_AT_VACANCY::fnSBCNetworkStatusCheck()
{
	TCHAR pErrorMsg[256];

	try
	{
		if(m_ulNetworkCheckTime < m_ulNetworkCheckTimeInterval)
		{
			m_ulNetworkCheckTime += FMSSM_MAIN_TIMER;

			return TRUE;
		}
		else
		{
			m_ulNetworkCheckTime = 0;

			return m_Unit->fnGetFMS()->SendSBCNetworkCheck();
		}		
	}	
	catch (CException* e)
	{
		e->GetErrorMessage(pErrorMsg, 256);		
	}

	return FALSE;
}
// 20210105 KSCHOI Add SBCNetworkCheck END