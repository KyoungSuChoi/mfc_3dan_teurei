// ShowDetailResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ShowDetailResultDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShowDetailResultDlg dialog


CShowDetailResultDlg::CShowDetailResultDlg(CFormResultFile * pFile, CWnd* pParent /*=NULL*/)
	: CDialog(CShowDetailResultDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CShowDetailResultDlg"));
	//{{AFX_DATA_INIT(CShowDetailResultDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pResultFile = pFile;
	ASSERT(m_pResultFile);
}

CShowDetailResultDlg::~CShowDetailResultDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CShowDetailResultDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}
void CShowDetailResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShowDetailResultDlg)
	DDX_Control(pDX, IDC_FILE_TREE, m_fileTree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CShowDetailResultDlg, CDialog)
	//{{AFX_MSG_MAP(CShowDetailResultDlg)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShowDetailResultDlg message handlers

BOOL CShowDetailResultDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString str, strTemp;
	int a = 0, b = 0, s = 0;

	GetDlgItem(IDC_EXT_DATA)->SetWindowText(m_pResultFile->GetFileName());
	HTREEITEM rghItem, subItem1, subItem2, subItem3, subItem4, subItem5, subItem6;

	//////////////////////////////////////////////////////////////////////////
	EP_FILE_HEADER *pHeader = m_pResultFile->GetFileHeader();
	rghItem = m_fileTree.InsertItem(TEXT_LANG[0]); //"File Info."
	str.Format(TEXT_LANG[1], pHeader->szFileID); //"File ID : %s"
	m_fileTree.InsertItem(str, rghItem);
	str.Format(TEXT_LANG[2], pHeader->szFileVersion); //"File Ver : %s"
	m_fileTree.InsertItem(str, rghItem);
	str.Format(TEXT_LANG[3], pHeader->szCreateDateTime); //"Date Time : %s"
	m_fileTree.InsertItem(str, rghItem);
	str.Format(TEXT_LANG[4], pHeader->szDescrition); //"Description : %s"
	m_fileTree.InsertItem(str, rghItem);
	str.Format(TEXT_LANG[5], pHeader->szReserved); //"Extra : %s"
	m_fileTree.InsertItem(str, rghItem);
	
	//////////////////////////////////////////////////////////////////////////
	rghItem = m_fileTree.InsertItem(TEXT_LANG[6]);//"Work Info."

	RESULT_FILE_HEADER *pRltHeader = m_pResultFile->GetResultHeader();
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[7], rghItem); //"Procedure"
	str.Format(TEXT_LANG[8], pRltHeader->nModuleID); //"ModuleID : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[9], pRltHeader->wGroupIndex+1); //"Group : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[10], pRltHeader->wJigIndex+1); //"Jig : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[11], pRltHeader->szDateTime); //"Time : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[12], pRltHeader->szModuleIP); //"IP : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[13], pRltHeader->szTrayNo); //"Tray ID : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[14], pRltHeader->szLotNo); //"Batch : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[15], pRltHeader->szOperatorID); //"Worker ID : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[16], pRltHeader->szTestSerialNo); //"TestSerial : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[16], pRltHeader->szTraySerialNo); //"TestSerial : %s"
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	EP_MD_SYSTEM_DATA *pSysData = m_pResultFile->GetMDSysData();
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[17], rghItem); //"Unit Info."
	str.Format(TEXT_LANG[8], pSysData->nModuleID);//"ModuleID : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[17], pSysData->nVersion); //"Protocol Ver : 0x%x"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[18], pSysData->nControllerType); //"Controller Type : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[19], pSysData->wInstalledBoard); //"Board Cnt. : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[20], pSysData->wChannelPerBoard); //"Ch per BD : %d",
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[21], pSysData->nModuleGroupNo); //"모듈 GroupNo : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[22], pSysData->nTotalChNo);//"Channel Cnt. : %d",
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[23], pSysData->wTotalTrayNo); //"Install Jig : %d"
	m_fileTree.InsertItem(str, subItem1);
	str = TEXT_LANG[24]; //"Ch per Jig : "
	for(a = 0; a<pSysData->wTotalTrayNo; a++)
	{
		strTemp.Format("%d/", pSysData->awChInTray[a]);
		str += strTemp;
	}
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[25], pSysData->wTrayType); //"Tray Type : %d"
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem("ETC Info.", rghItem);
	STR_FILE_EXT_SPACE *pExtSpace = m_pResultFile->GetExtraData();
	str.Format(TEXT_LANG[26], pExtSpace->szResultFileName); //"File Name : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[27], pExtSpace->nCellNo); //"Cell No : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[28], pExtSpace->nInput1stCellNo); //"1단 입력수량 : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[29], pExtSpace->nInput2ndCellNo); //"2단 입력수량 : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[29], pExtSpace->nInput3rdCellNo); //"3단 입력수량 : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[30], pExtSpace->nReserved); //"Tray Cell Cnt.: %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[31], pExtSpace->nSystemID); //"System ID : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[32], pExtSpace->nSystemType); //"System Type : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[33], pExtSpace->szModuleName); //"Module Name : %s"
  	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[34], pExtSpace->szReserved); //"Extra : %s"
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[35], rghItem); //"Cell ID"
	PNE_RESULT_CELL_SERIAL *pSerial = m_pResultFile->GetResultCellSerial();
	for(int i=0; i<EP_MAX_CH_PER_MD; i++)
	{
		str.Format("CH %d :: %s", i+1, pSerial->szCellNo[i]);
		m_fileTree.InsertItem(str, subItem1);
	}

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[36], rghItem); //"Sensor Mapping"
	subItem2 = m_fileTree.InsertItem(TEXT_LANG[37], subItem1); //"Sensor 1"
	_MAPPING_DATA *pSensorMap = m_pResultFile->GetSensorMap(CFormResultFile::sensorType1);
	
	for(s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		if(pSensorMap[s].nChannelNo < 0)
		{
			str.Format(TEXT_LANG[38], s+1); //"Ch %02d: Not Use"
		}
		else if(pSensorMap[s].nChannelNo == 0)
		{
			str.Format(TEXT_LANG[39], s+1); //"Ch %02d: Universal"
		}
		else
		{
			str.Format(TEXT_LANG[40], s+1, pSensorMap[s].szName, pSensorMap[s].nChannelNo); //"Ch %02d: %s => Unit CH %d"
		}
		m_fileTree.InsertItem(str, subItem2);

	}
	subItem2 = m_fileTree.InsertItem(TEXT_LANG[41], subItem1); //"Sensor 2"
	pSensorMap = m_pResultFile->GetSensorMap(CFormResultFile::sensorType2);
	for( s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		if(pSensorMap[s].nChannelNo < 0)
		{
			str.Format(TEXT_LANG[38], s+1); //"Ch %02d: Not Use"
		}
		else if(pSensorMap[s].nChannelNo == 0)
		{
			str.Format(TEXT_LANG[39], s+1); //"Ch %02d: Universal"
		}
		else
		{
			str.Format(TEXT_LANG[40], s+1, pSensorMap[s].szName, pSensorMap[s].nChannelNo); //"Ch %02d: %s => Unit CH %d"
		}
		m_fileTree.InsertItem(str, subItem2);
	}

	//////////////////////////////////////////////////////////////////////////
	rghItem = m_fileTree.InsertItem(TEXT_LANG[42]);//"Schedule Info."
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[43], rghItem);//"Schedule"
	//STR_CONDITION *pCon = m_pResultFile->GetConditionMain();
	CTestCondition *pCon = m_pResultFile->GetTestCondition();
	str.Format(TEXT_LANG[44], pCon->GetModelInfo()->lID); //"DB PK : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[45], pCon->GetModelInfo()->lNo); //"No : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[46], pCon->GetModelInfo()->lType); //"Type : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[47], pCon->GetModelInfo()->szName); //"Name : %s"
 	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[48], pCon->GetModelInfo()->szDescription); //"Descript. : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[49], pCon->GetModelInfo()->szCreator); //"Author : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[50], pCon->GetModelInfo()->szModifiedTime); //"Edit date : %s"
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[51], rghItem); //"Procedure"
	str.Format(TEXT_LANG[44], pCon->GetTestInfo()->lID);//"DB PK : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[45], pCon->GetTestInfo()->lNo); //"No : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[46], pCon->GetTestInfo()->lType);//"Type : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[47], pCon->GetTestInfo()->szName); //"Name : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[48], pCon->GetTestInfo()->szDescription);//"Descript. : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[49], pCon->GetTestInfo()->szCreator);//"Author : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[50], pCon->GetTestInfo()->szModifiedTime); //"Edit date : %s"
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[52], rghItem); //"Step Info."
	str.Format(TEXT_LANG[53], pCon->GetTotalStepNo()); //"Total Step : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[54], 0); //"Total Grading Step : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[55], "");//"Extra 1 : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[56], "");//"Extra 2 : %s"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[57], "");//"Extra 3 : %d"
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[58], rghItem); //"Cell Check"
	str.Format(TEXT_LANG[59], pCon->GetCheckParam()->fOCVUpperValue); //"V High : %f"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[60], pCon->GetCheckParam()->fOCVLowerValue); //"V Low : %f"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[61], pCon->GetCheckParam()->fVRef); //"V ref : %f"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[62], pCon->GetCheckParam()->fIRef); //"I ref : %f"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[63], pCon->GetCheckParam()->fTime); //"Time : %f"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[64], pCon->GetCheckParam()->fDeltaVoltage); //"Delta V : %f"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[65], pCon->GetCheckParam()->fMaxFaultBattery); //"Max fail cnt. : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[66], pCon->GetCheckParam()->compFlag); //"Use Flag : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[67], pCon->GetCheckParam()->autoProcessingYN); //"Auto Process : %d"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[68], pCon->GetCheckParam()->reserved);//"Extra : %d"
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[69], rghItem); //"Step Condition"
	STR_COMMON_STEP *pStep;
	for(a=0; a < pCon->GetTotalStepNo(); a++)
	{
		str.Format(TEXT_LANG[70], a+1); //"Step %d"
		subItem2 = m_fileTree.InsertItem(str, subItem1);
		pStep = (STR_COMMON_STEP *)pCon->GetStep(a);
		
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[71], subItem2);//"Step Header"
		str.Format(TEXT_LANG[72], pStep->stepHeader.stepIndex); //"Step No : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[46], pStep->stepHeader.type); //"Type : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[73], pStep->stepHeader.mode); //"Mode : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[74], pStep->stepHeader.gradeSize);//"Grade Step Cnt. : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[75], pStep->stepHeader.nProcType);//"Proc. Type : 0x%x"
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[76], pStep->fVref); //"V Ref. : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[77], pStep->fIref); //"I Ref. : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[78], pStep->fEndTime); //"End Time : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[79], pStep->fEndV); //"End Voltage : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[80], pStep->fEndI); //"End Current : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[81], pStep->fEndC); //"End Capacity : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[82], pStep->fEndDV); //"End Delta V : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[83], pStep->fEndDI); //"End Delta I : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[84], pStep->bUseActucalCap); //"Capa. Flag : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[85], pStep->bReserved); //"Extra1 : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[86], pStep->UseStepContinue); //"Step Continue : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[87], pStep->fSocRate); //"End SOC : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[88], pStep->fOverV); //"V High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[89], pStep->fLimitV); //"V Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[90], pStep->fOverI); //"I High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[91], pStep->fLimitI); //"I Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[92], pStep->fOverC); //"C High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[93], pStep->fLimitC); //"C Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[94], pStep->fOverImp); //"Z High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[95], pStep->fLimitImp); //"Z Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);

		
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[96], subItem2); //"Rising Check V"
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[97], b+1, pStep->fCompVLow[b],  pStep->fCompTimeV[b],  pStep->fCompVHigh[b]); //"Check V %d : %f< %f< %f"
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[98], subItem2);//"Rising Check I"
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[99], b+1, pStep->fCompILow[b],  pStep->fCompTimeI[b],  pStep->fCompIHigh[b]); //"Check I %d : %f< %f< %f"
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[100], subItem2); //"Delta Check"
		str.Format(TEXT_LANG[101], pStep->fDeltaV, pStep->fDeltaTimeV); //"Delta V Limit: %f/%f"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[102], pStep->fDeltaI, pStep->fDeltaTimeI); //"Delta I Limit: %f/%f"
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[103], pStep->nLoopInfoGoto); //"Move Step : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[104], pStep->nLoopInfoCycle); //"Loop Cnt. : %d"
		m_fileTree.InsertItem(str, subItem2);
	
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[105], subItem2); //"Grading Condition"
		for(b = 0; b<EP_MAX_GRADE_STEP; b++)
		{
			str.Format(TEXT_LANG[106], b+1, //"Grade Step %d : No %d, Item %d, %f <Value <%f =>%c"
						pStep->grade[b].index+1, pStep->grade[b].gradeItem, pStep->grade[b].fMin, pStep->grade[b].fMax, pStep->grade[b].gradeCode);
			m_fileTree.InsertItem(str, subItem3);
		}

		//float		fReserved[16];
	}

	rghItem = m_fileTree.InsertItem(TEXT_LANG[107]);//"Step Result"

	STR_STEP_RESULT *pResult;
	int nStepSize = m_pResultFile->GetStepSize();
	for( a = 0; a<nStepSize; a++)
	{
		pResult = m_pResultFile->GetStepData(a);
		if(pResult == NULL)	break;

		str.Format(TEXT_LANG[70], a+1); //"Step %d"
		subItem1 = m_fileTree.InsertItem(str, rghItem);

		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[108], subItem1);//"Status"
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[109], subItem2);//"Jig"
		str.Format(TEXT_LANG[110], pResult->gpStateSave.gpState.state);//"Box Status : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[111], pResult->gpStateSave.gpState.failCode);//"Fail Code : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[112], pResult->gpStateSave.gpState.sensorState1);//"Sensor1 : 0x%x"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[113], pResult->gpStateSave.gpState.sensorState2);//Sensor2 : 0x%x
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[114], pResult->gpStateSave.gpState.sensorState3);//Sensor3 : 0x%x
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[115], pResult->gpStateSave.gpState.sensorState4);//Sensor4 : 0x%x
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[116], pResult->gpStateSave.gpState.sensorState5);//Sensor5 : 0x%x
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[117], pResult->gpStateSave.gpState.sensorState6);//Sensor6 : 0x%x
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[118], pResult->gpStateSave.gpState.sensorState7);//Sensor7 : 0x%x
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[119], pResult->gpStateSave.gpState.sensorState8);//Sensor8 : 0x%x
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[120], pResult->gpStateSave.gpState.lReserved[0]); //"부가정보 : %d"
		m_fileTree.InsertItem(str, subItem3);

		subItem3 = m_fileTree.InsertItem(TEXT_LANG[121], subItem2);//"Sensor"
		subItem4 = m_fileTree.InsertItem(TEXT_LANG[37], subItem3);//"Sensor 1"
		for(int s=0; s<EP_MAX_SENSOR_CH; s++)
		{
			str.Format(TEXT_LANG[122], s+1);//"Ch %d"
			subItem5 = m_fileTree.InsertItem(str, subItem4);

			str.Format(TEXT_LANG[123], pResult->gpStateSave.sensorChData1[s].fData);//"Sensor Data : %f"
			m_fileTree.InsertItem(str, subItem5);
			str.Format(TEXT_LANG[124], pResult->gpStateSave.sensorChData1[s].lReserved); //"Reserved : %d"
			m_fileTree.InsertItem(str, subItem5);
			subItem6 = m_fileTree.InsertItem(TEXT_LANG[125], subItem5); //"MinMax"
			str.Format(TEXT_LANG[126], pResult->gpStateSave.sensorChData1[s].minmaxData.lMax); //"Max Val. : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[127], pResult->gpStateSave.sensorChData1[s].minmaxData.lMin); //"Min val. : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[128], pResult->gpStateSave.sensorChData1[s].minmaxData.lAvg); //"Average : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[129], pResult->gpStateSave.sensorChData1[s].minmaxData.wMaxIndex);//"Max Index : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[130], pResult->gpStateSave.sensorChData1[s].minmaxData.wMinIndex); //"Min Index : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[131], pResult->gpStateSave.sensorChData1[s].minmaxData.szMaxDateTime); //"Max Time : %s"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[132], pResult->gpStateSave.sensorChData1[s].minmaxData.szMinDateTime); //"Min Time: %s"
			m_fileTree.InsertItem(str, subItem6);
		}
	
		subItem4 = m_fileTree.InsertItem(TEXT_LANG[41], subItem3);//"Sensor 2"
		for( s=0; s<EP_MAX_SENSOR_CH; s++)
		{
			str.Format(TEXT_LANG[122], s+1);//"Ch %d"
			subItem5 = m_fileTree.InsertItem(str, subItem4);

			str.Format(TEXT_LANG[123], pResult->gpStateSave.sensorChData2[s].fData);;//"Sensor Data : %f"
			m_fileTree.InsertItem(str, subItem5);
			str.Format(TEXT_LANG[124], pResult->gpStateSave.sensorChData2[s].lReserved); //"Reserved : %d"
			m_fileTree.InsertItem(str, subItem5);
			subItem6 = m_fileTree.InsertItem(TEXT_LANG[125], subItem5); //"MinMax"
			str.Format(TEXT_LANG[126], pResult->gpStateSave.sensorChData1[s].minmaxData.lMax);//"Max Val. : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[127], pResult->gpStateSave.sensorChData1[s].minmaxData.lMin);//"Min val. : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[128], pResult->gpStateSave.sensorChData1[s].minmaxData.lAvg);//"Average : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[129], pResult->gpStateSave.sensorChData1[s].minmaxData.wMaxIndex);//"Max Index : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[130], pResult->gpStateSave.sensorChData1[s].minmaxData.wMinIndex);//"Min Index : %d"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[131], pResult->gpStateSave.sensorChData1[s].minmaxData.szMaxDateTime); //"Max Time : %s"
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[132], pResult->gpStateSave.sensorChData1[s].minmaxData.szMinDateTime);//"Min Time: %s"
			m_fileTree.InsertItem(str, subItem6);
		}

		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[133], subItem1); //"Step Header"
		str.Format(TEXT_LANG[134], pResult->stepHead.nModuleID); //"ModuleID : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[9], pResult->stepHead.wGroupIndex+1); //"Group : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[10], pResult->stepHead.wJigIndex+1); //"Jig : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[135], pResult->stepHead.szStartDateTime); //"Start Time : %s"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[136], pResult->stepHead.szEndDateTime); //"End Time : %s"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[12], pResult->stepHead.szModuleIP); //"IP : %s"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[137], pResult->stepHead.szTrayNo); //"Tray ID : %s"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[138], pResult->stepHead.szOperatorID); //"Worker ID : %s"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[16], pResult->stepHead.szTestSerialNo); //"TraySerial : %s"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[16], pResult->stepHead.szTraySerialNo); //"TraySerial : %s"
		m_fileTree.InsertItem(str, subItem2);

		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[69], subItem1);	//"Step Condition
		STR_COMMON_STEP *pStep = &pResult->stepCondition;
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[133], subItem2); //Step Header
		str.Format(TEXT_LANG[72], pStep->stepHeader.stepIndex);//"Step No : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[46], pStep->stepHeader.type); //"Type : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[73], pStep->stepHeader.mode); //"Mode : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[74], pStep->stepHeader.gradeSize); //"Grade Step Cnt. : %d"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[75], pStep->stepHeader.nProcType); //"Proc. Type : 0x%x"
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[76], pStep->fVref); //"V Ref. : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[77], pStep->fIref); //"I Ref. : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[78], pStep->fEndTime); //"End Time : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[79], pStep->fEndV); //"End Voltage : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[80], pStep->fEndI); //"End Current : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[81], pStep->fEndC); //"End Capacity : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[82], pStep->fEndDV); //"End Delta V : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[83], pStep->fEndDI); //"End Delta I : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[84], pStep->bUseActucalCap); //"Capa. Flag : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[85], pStep->bReserved);//"Extra1 : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[86], pStep->UseStepContinue); //"Step Continue : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[87], pStep->fSocRate); //"End SOC : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[88], pStep->fOverV); //"V High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[89], pStep->fLimitV); //"V Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[90], pStep->fOverI); //"I High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[91], pStep->fLimitI); //"I Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[92], pStep->fOverC); //"C High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[93], pStep->fLimitC); //"C Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[94], pStep->fOverImp); //"Z High Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[95], pStep->fLimitImp); //"Z Low Limit : %f"
		m_fileTree.InsertItem(str, subItem2);
		
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[96], subItem2); //"Rising Check V"
		for(int b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[97], b+1, pStep->fCompVLow[b],  pStep->fCompTimeV[b],  pStep->fCompVHigh[b]);//"Check V %d : %f< %f< %f"
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[98], subItem2);//"Rising Check I"
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[99], b+1, pStep->fCompILow[b],  pStep->fCompTimeI[b],  pStep->fCompIHigh[b]);//"Check I %d : %f< %f< %f"
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[100], subItem2);//"Delta Check"
		str.Format(TEXT_LANG[101], pStep->fDeltaV, pStep->fDeltaTimeV);//"Delta V Limit: %f/%f"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[102], pStep->fDeltaI, pStep->fDeltaTimeI); //"Delta I Limit: %f/%f"
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[103], pStep->nLoopInfoGoto); //"Move Step : %d"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[104], pStep->nLoopInfoCycle); //"Loop Cnt. : %d"
		m_fileTree.InsertItem(str, subItem2);
	
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[105], subItem2); //"Grading Condition"
		for(b = 0; b<EP_MAX_GRADE_STEP; b++)
		{
			str.Format(TEXT_LANG[106], b+1, //"Grade Step %d : No %d, Item %d, %f <Value <%f =>%c" 
						pStep->grade[b].index+1, pStep->grade[b].gradeItem, pStep->grade[b].fMin, pStep->grade[b].fMax, pStep->grade[b].gradeCode);
			m_fileTree.InsertItem(str, subItem3);
		}

		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[139], subItem1);//"Summery"
		for( b =0; b<EP_RESULT_ITEM_NO; b++)
		{
			str.Format(TEXT_LANG[140], b+1); //"Item %d"
			subItem3 = m_fileTree.InsertItem(str, subItem2);
			str.Format(TEXT_LANG[141],pResult->averageData[b].GetAvg()); //"Average : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[142],pResult->averageData[b].GetMinValue()); //"Min : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[143],pResult->averageData[b].GetMinIndex()+1); //"Min Ch : %d"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[144],pResult->averageData[b].GetMaxValue()); //"Max : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[145],pResult->averageData[b].GetMaxIndex()+1); //"Max Ch : %d"
			m_fileTree.InsertItem(str, subItem3);	
			str.Format(TEXT_LANG[146],pResult->averageData[b].GetSTDD()); //"STDEV : %f"
			m_fileTree.InsertItem(str, subItem3);	
			str.Format(TEXT_LANG[147],pResult->averageData[b].GetDataCount()); //"GOOD Cnt. : %d"
			m_fileTree.InsertItem(str, subItem3);	
		}
		
		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[148], subItem1);//"Channel"
		STR_SAVE_CH_DATA *pChData;
		for(b =0; b<pResult->aChData.GetSize(); b++)
		{
			str.Format(TEXT_LANG[122], b+1);// "Ch %d"
			subItem3 = m_fileTree.InsertItem(str, subItem2);

			pChData = (STR_SAVE_CH_DATA *)pResult->aChData[b]; 
			str.Format(TEXT_LANG[149], pChData->wChIndex+1); //"Tray CH Number : %d"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[150], pChData->state); //"Status : %d"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[151], pChData->grade); //"Grade code : %c"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[152], pChData->channelCode); //"CH Code : %d"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[139], pChData->nStepNo); //"Step No: %d"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[153], pChData->fStepTime); //"Step Time : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[154], pChData->fTotalTime); //"Total Run Time : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[155], pChData->fVoltage); //"Voltage : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[156], pChData->fCurrent); //"Current : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[157], pChData->fWatt); //"Watt : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[158], pChData->fWattHour); //"WattHour : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[159], pChData->fCapacity); //"Capacity : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[160], pChData->fImpedance); //"Impedeance : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[161], pChData->fAuxData[0]); //"Temperature : %f"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[162], pChData->wModuleChIndex+1); //"Unit CH Number : %d"
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[163], pChData->wReserved); //"Extra 1 : %d"
			m_fileTree.InsertItem(str, subItem3);
			
			//str.Format("Ch %d", pChData->fReserved[0]);
			//subItem4 = m_fileTree.InsertItem(str, subItem3);

		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CShowDetailResultDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		if(m_fileTree.GetSafeHwnd())
		{
			m_fileTree.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_fileTree.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			Invalidate();
		}
	}	
}
