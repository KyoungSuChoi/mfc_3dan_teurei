// AgingView.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "AgingView.h"
#include "CTSMonDoc.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAgingView

IMPLEMENT_DYNCREATE(CAgingView, CFormView)

CAgingView::CAgingView()
	: CFormView(CAgingView::IDD)
{
	//{{AFX_DATA_INIT(CAgingView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nCurModuleID = 0;
	m_nMonitoringTimer = 0;
}

CAgingView::~CAgingView()
{
}

void CAgingView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAgingView)
	DDX_Control(pDX, IDC_MODULE_STATIC, m_CmdTarget);
	DDX_Control(pDX, IDC_STATE_STATIC, m_StateStatic);
	DDX_Control(pDX, IDC_DISCHARGE_CHECK, m_DisChargeLamp);
	DDX_Control(pDX, IDC_CHARGE_CHECK, m_ChargeLamp);
//	DDX_Control(pDX, IDC_REMAIN_TIME_STATIC, m_RemainTimeStatic);
//	DDX_Control(pDX, IDC_RUN_TIME_STATIC, m_RunTimeStatic);
	DDX_Control(pDX, IDC_TEMP_STATIC, m_TempLed);
	DDX_Control(pDX, IDC_TEMP_STATIC1, m_TempLed1);
	DDX_Control(pDX, IDC_GREEN_CHECK, m_GreenLamp);
	DDX_Control(pDX, IDC_YELLOW_CHECK, m_YellowLamp);
	DDX_Control(pDX, IDC_RED_CHECK, m_RedLamp);
	DDX_Control(pDX, IDC_DISCHARGE_STATIC, m_DischargeLabel);
	DDX_Control(pDX, IDC_CHARGE_STATIC, m_ChargeLabel);
	DDX_Control(pDX, IDC_RACK_STATIC, m_RackBack);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAgingView, CFormView)
	//{{AFX_MSG_MAP(CAgingView)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAgingView diagnostics

#ifdef _DEBUG
void CAgingView::AssertValid() const
{
	CFormView::AssertValid();
}

void CAgingView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CAgingView message handlers

void CAgingView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class

	
	//Charge/Discharge sign lamp
	m_RackBack.SetBkColor(RGB(255,255,230));
	m_ChargeLabel.SetGradientColor(RGB(255, 255, 255)).SetGradient(TRUE).SetGradientType(CLabel::G_CENTER);
	m_DischargeLabel.SetGradientColor(RGB(255, 255, 255)).SetGradient(TRUE).SetGradientType(CLabel::G_CENTER);
	m_DischargeLabel.SetBorder(TRUE);
	m_ChargeLabel.SetBorder(TRUE);

	//Tower Lamp
	m_RedLamp.SetImage( IDB_RED_LAMP, 32 );
	m_YellowLamp.SetImage( IDB_YELLOW_LAMP, 32 );
	m_GreenLamp.SetImage( IDB_GREEN_LAMP, 32 );

	m_ChargeLamp.SetImage(IDB_CHARGE_BITMAP, 64);
	m_DisChargeLamp.SetImage(IDB_DISCHARGE_BITMAP, 64);

	//temperature display
	m_TempLed.SetNumberOfLines(1);
	m_TempLed.SetXCharsPerLine(6);
	m_TempLed.SetSize(CMatrixStatic::LARGE);
	m_TempLed.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(50, 10, 0));
	m_TempLed.AdjustClientXToSize(6);
	m_TempLed.AdjustClientYToSize(1);
	m_TempLed.SetText("      ");
	m_TempLed.SetAutoPadding(true, ' ');			//demonstrates auto padding with different character
//	m_TempLed.DoScroll(500, CMatrixStatic::LEFT);

	m_TempLed1.SetNumberOfLines(1);
	m_TempLed1.SetXCharsPerLine(6);
	m_TempLed1.SetSize(CMatrixStatic::LARGE);
	m_TempLed1.SetDisplayColors(RGB(0, 0, 0), RGB(60, 255, 0), RGB(10, 50, 0));
	m_TempLed1.AdjustClientXToSize(6);
	m_TempLed1.AdjustClientYToSize(1);
	m_TempLed1.SetText("      ");
	m_TempLed1.SetAutoPadding(true, ' ');			//demonstrates auto padding with different character

	//Set run time counter

// 	m_RunTimeStatic.SetNumberOfLines(1);
// 	m_RunTimeStatic.SetXCharsPerLine(8);
// 	m_RunTimeStatic.SetSize(CMatrixStatic::LARGE);
// //	m_RunTimeStatic.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(70, 10, 0));
// 	m_RunTimeStatic.SetDisplayColors(RGB(0, 0, 0), RGB(0, 255, 30), RGB(0, 60, 10));
// 	m_RunTimeStatic.AdjustClientXToSize(8);
// 	m_RunTimeStatic.AdjustClientYToSize(1);
// 	m_RunTimeStatic.SetText(_T("00:00:00"));
// 	m_RunTimeStatic.SetAutoPadding(true, ' ');			//demonstrates auto padding with different character
// //	m_RunTimeStatic.DoScroll(500, CMatrixStatic::LEFT);

// 	//Set remain time counter
// 	m_RemainTimeStatic.SetNumberOfLines(1);
// 	m_RemainTimeStatic.SetXCharsPerLine(8);
// 	m_RemainTimeStatic.SetSize(CMatrixStatic::TINY);
// //	m_RemainTimeStatic.SetDisplayColors(RGB(0, 0, 0), RGB(255, 60, 0), RGB(70, 10, 0));
// 	m_RemainTimeStatic.SetDisplayColors(RGB(0, 0, 0), RGB(255, 100, 100), RGB(70, 10, 0));
// 	m_RemainTimeStatic.AdjustClientXToSize(8);
// 	m_RemainTimeStatic.AdjustClientYToSize(1);
// 	m_RemainTimeStatic.SetText(_T("00:00:00"));
// 	m_RemainTimeStatic.SetAutoPadding(true, ' ');			//demonstrates auto padding with different character

/*	m_RunTimeStatic.SetDraw3DBar(FALSE);
	m_RunTimeStatic.SetBlankPadding(6);
	m_RunTimeStatic.SetColourFaded(RGB(80,0,0));	//Faded Led color
	m_RunTimeStatic.SetColours( RGB(240,0,0),	//Led Col
								0,				//Back color
								RGB(0,0,0) );	//Bar Color


	//Set remain time counter
	m_RemainTimeStatic.SetDraw3DBar(FALSE);
	m_RemainTimeStatic.SetBlankPadding(6);
	m_RemainTimeStatic.SetColourFaded(RGB(80,0,0));	//Faded Led color
	m_RemainTimeStatic.SetColours( RGB(240,0,0),	//Led Col
									0,				//Back color
									RGB(0,0,0) );	//Bar Color
*/

	m_StateStatic.SetFontSize(18);

	m_CmdTarget.SetFontSize(20)
				.SetTextColor(RGB(50, 100, 200))
				.SetBkColor(RGB(120,170,255))
				.SetGradientColor(RGB(255,255,255))
				.SetFontBold(TRUE)
				.SetGradient(TRUE)
				.SetFontName("HY헤드라인M");


	InitGrid();
	InitGrid2();

	SetCurrentModule(EPGetModuleID(0));

	//Flash lamp display timer
	SetTimer(602, 1000, NULL);

}

void CAgingView::InitGrid()
{
	m_wndGrid.SubclassDlgItem(IDC_AGING_GRID, this);
	m_wndGrid.m_bSameRowSize = TRUE;
	m_wndGrid.m_bSameColSize = TRUE;
//	m_wndGrid.m_nCol1 = 1;
//	m_wndGrid.m_nCol2 = 1;
//	m_wndGrid.m_ColHeaderFgColor[1] = RGB(192,0,255);
//	m_wndGrid.m_bCustomColor = TRUE;
	m_wndGrid.Initialize();
	m_wndGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndGrid.EnableGridToolTips();
    m_wndGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	//Use MemDc
	m_wndGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	//Row Header Setting
//	m_wndGrid.HideCols(0,0);
//	m_wndGrid.SetColWidth(0, 0, 50);
	m_wndGrid.SetDefaultRowHeight(26);

//	ReConfigGrid();
}

void CAgingView::InitGrid2()
{
	m_wndGrid2.SubclassDlgItem(IDC_AGING_GRID2, this);
	m_wndGrid2.m_bSameRowSize = FALSE;
	m_wndGrid2.m_bSameColSize = FALSE;
	m_wndGrid2.m_bCustomWidth = TRUE;
	m_wndGrid2.m_nWidth[1] = 100;
	m_wndGrid2.m_nWidth[2] = 100;
//	m_wndGrid2.m_nCol1 = 1;
//	m_wndGrid2.m_nCol2 = 1;
//	m_wndGrid2.m_ColHeaderFgColor[1] = RGB(192,0,255);
//	m_wndGrid2.m_bCustomColor = TRUE;
	m_wndGrid2.Initialize();
	m_wndGrid2.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Enable Tooltips
	m_wndGrid2.EnableGridToolTips();
    m_wndGrid2.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	//Use MemDc
	m_wndGrid2.SetDrawingTechnique(gxDrawUsingMemDC);

	//Row Header Setting
	m_wndGrid2.HideCols(0,0);
	m_wndGrid2.SetDefaultRowHeight(20);

	m_wndGrid2.SetRowCount(15);
	m_wndGrid2.SetColCount(2);
	m_wndGrid2.SetValueRange(CGXRange(0, 1), "항 목");
	m_wndGrid2.SetValueRange(CGXRange(0, 2), "값");

	int nRow = 1;
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1), "작 업  목 록");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1), "공   정   명");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1),  "Step No");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1),  "상        태");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1),  "설 정  온 도");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1),  "현 재  온 도");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1),  "Step진행시간");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1),  "Step 진행률");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1),  "전체진행시간");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1), "전체 진행률");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1), "투입 Tray");
	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1), ::GetStringTable(IDS_TEXT_INPUT_CELL));
//	m_wndGrid2.SetValueRange(CGXRange(nRow++, 1), "충전 총전류");
//	m_wndGrid2.SetValueRange(CGXRange(16, 1), "작   업   자");
//	m_wndGrid2.SetValueRange(CGXRange(5, 1),  "총   전   압");
//	m_wndGrid2.SetValueRange(CGXRange(6, 1),  "총   전   류");

	m_wndGrid2.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(GetSysColor(COLOR_3DFACE)));
//	m_wndGrid2.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(RGB(128,128,128)).SetTextColor(RGB(255,255,255)));

	m_wndGrid2.SetStyleRange(CGXRange(5, 2), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_PROGRESS)
								.SetValue(0L)
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%d ℃"))		// sprintf formatting for value
								.SetTextColor(RGB(255,128,64))									// blue progress bar
								.SetInterior(RGB(255, 255, 255))									// on white background
						  );
	m_wndGrid2.SetStyleRange(CGXRange(6, 2), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_PROGRESS)
								.SetValue(0L)
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%d ℃"))		// sprintf formatting for value
								.SetTextColor(RGB(255,128,64))									// blue progress bar
								.SetInterior(RGB(255, 255, 255))									// on white background
						  );
	m_wndGrid2.SetStyleRange(CGXRange(8, 2), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_PROGRESS)
								.SetValue(0L)
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%d %%"))		// sprintf formatting for value
								.SetTextColor(RGB(64,128,255))									// blue progress bar
								.SetInterior(RGB(255, 255, 255))									// on white background
						  );
	m_wndGrid2.SetStyleRange(CGXRange(10, 2), 
								CGXStyle()
								.SetControl(GX_IDS_CTRL_PROGRESS)
								.SetValue(0L)
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("100"))
								.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%d %%"))		// sprintf formatting for value
								.SetTextColor(RGB(64,128,255))									// blue progress bar
								.SetInterior(RGB(255, 255, 255))									// on white background
						  );

}


void CAgingView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	UpdateState();

	//1Sec timer
	//상태 표기 Timer가 가동중일 경우만 Update
	if(nIDEvent == 602)
	{
		if(m_nMonitoringTimer)
		{
			ShowState();
		}

	}

	CFormView::OnTimer(nIDEvent);
}

void CAgingView::UpdateState()
{
	//make lamp flash
	//////////////////////////////////////////////////////////////////////////
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		return;

	CTray *pTray;
	CString strTemp, strTray;
	ROWCOL nRow;

	BYTE color;
	GetDlgItem(IDC_STATE_STATIC)->SetWindowText(pDoc->GetStateMsg(pModule->GetState(), color));
	STR_TOP_CONFIG *pColorConfig = pDoc->GetTopConfig();
	m_StateStatic.SetTextColor(pColorConfig->m_TStateColor[color]).SetBkColor(pColorConfig->m_BStateColor[color]);
	
	int nCellCnt = 0;
	float fStepTime = pModule->GetStepTime();
	float fTotTime = pModule->GetTotRunTime();
	int nStepNo = pModule->GetCurStepNo();
	CStep *pStep = pModule->GetCondition()->GetStep(nStepNo-1);
	STR_SAVE_CH_DATA chSaveData;

	EP_GP_DATA gpData = EPGetGroupData(m_nCurModuleID, 0);
	WORD wTrayState;
	BYTE colorFlag;
	for(int tray = 0; tray<pModule->GetTotalJig(); tray++)
	{
		nRow = (tray/JIG_STACK_COL)*2+1;

		pModule->GetChannelStepData(chSaveData, pModule->GetStartChIndex(tray));
		
		//Show TrayNo
		pTray = pModule->GetTrayInfo(tray);
		if(pTray == NULL)	break;

		strTray = pTray->GetTrayNo();
		if(strTray.IsEmpty())	strTray = "____";
		wTrayState = EPTrayState(m_nCurModuleID, tray);
		if(wTrayState == EP_TRAY_LOAD)
		{
			strTemp = strTray;	
			nCellCnt += pTray->lInputCellCount;
		}
		else
		{
			strTemp.Format("(%s)", strTray);
		}
		m_wndGrid.SetValueRange(CGXRange(nRow, tray%2*COL_PER_TRAY+2), strTemp);
		
		if(wTrayState == EP_TRAY_LOAD)
		{
			//Voltage
			strTemp.Format("%s", pDoc->ValueString(chSaveData.fVoltage , EP_VOLTAGE, TRUE));
			m_wndGrid.SetValueRange(CGXRange(nRow, tray%2*COL_PER_TRAY+4), strTemp);
			//Current
			if(pStep && pStep->m_type == EP_TYPE_CHARGE)
			{
				strTemp.Format("%s", pDoc->ValueString(chSaveData.fCurrent , EP_CURRENT, TRUE));
			}
			else
			{
				strTemp = "---";
			}
			m_wndGrid.SetValueRange(CGXRange(nRow, tray%2*COL_PER_TRAY+5), strTemp);

			pDoc->GetStateMsg(chSaveData.state, colorFlag);
			m_wndGrid.SetStyleRange(CGXRange(nRow, tray%2*COL_PER_TRAY+4, nRow, tray%2*COL_PER_TRAY+5), 
												CGXStyle().SetInterior(pColorConfig->m_BStateColor[colorFlag])
														  .SetTextColor(pColorConfig->m_TStateColor[colorFlag]));

			strTemp = pDoc->ValueString(chSaveData.channelCode, EP_CH_CODE);
			m_wndGrid.SetStyleRange(CGXRange(nRow, tray%2*COL_PER_TRAY+2, nRow+1, tray%2*COL_PER_TRAY+5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
		}
		else
		{
			m_wndGrid.SetValueRange(CGXRange(nRow, tray%2*COL_PER_TRAY+4), "");
			m_wndGrid.SetValueRange(CGXRange(nRow, tray%2*COL_PER_TRAY+5), "");
		
			pDoc->GetStateMsg(EP_STATE_IDLE, colorFlag);
			m_wndGrid.SetStyleRange(CGXRange(nRow, tray%2*COL_PER_TRAY+4, nRow, tray%2*COL_PER_TRAY+5), 
												CGXStyle().SetInterior(pColorConfig->m_BStateColor[colorFlag])
														  .SetTextColor(pColorConfig->m_TStateColor[colorFlag]));
			m_wndGrid.SetStyleRange(CGXRange(nRow, tray%2*COL_PER_TRAY+2, nRow+1, tray%2*COL_PER_TRAY+5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T("")));
		}

		//Show 4 channels voltage sensing data
		int nSensorCh =0;
		for(int ch=0; ch<V_SENS_PER_TRAY; ch++)
		{
			nSensorCh = tray*V_SENS_PER_TRAY+ch;
			//방법 1: sensor data structure 이용
			strTemp = pDoc->ValueString(VTG_PRECISION(gpData.sensorData.sensorData2[nSensorCh].lData), EP_VOLTAGE);	 
			//방법 2: Channel data structure 이용
			/*if(pModule->GetChannelStepData(chSaveData, ch))
			{
				strTemp = pDoc->ValueString(chSaveData.fVoltage, EP_VOLTAGE)
			}
			*/
			if(wTrayState == EP_TRAY_LOAD)
			{
				m_wndGrid.SetValueRange(CGXRange(nRow+1, tray%2*COL_PER_TRAY+2+ch), strTemp);
			}
			else
			{
				m_wndGrid.SetValueRange(CGXRange(nRow+1, tray%2*COL_PER_TRAY+2+ch), "");
			}
		}
	}
			
	m_wndGrid2.SetValueRange(CGXRange(1, 2), pModule->GetCondition()->GetModelInfo()->szName);				//작 업  목 록");	
	m_wndGrid2.SetValueRange(CGXRange(2, 2), pModule->GetTestName());		//공   정   명");
	strTemp.Format("%d/%d", nStepNo, pModule->GetCondition()->GetTotalStepNo());
	m_wndGrid2.SetValueRange(CGXRange(3, 2), strTemp);				//"Step No

	WORD state = pModule->GetState(TRUE);
	STR_TOP_CONFIG *pConfig = pDoc->GetTopConfig();
	strTemp = pDoc->GetStateMsg(state, color);
//	if(state != EP_STATE_LINE_OFF)
//	{
		m_wndGrid2.SetStyleRange(CGXRange(4, 2), CGXStyle().SetValue(strTemp)
																			.SetTextColor(pConfig->m_TStateColor[color])
																			.SetInterior(pConfig->m_BStateColor[color]));
//	}
//	else
//	{
//		if(m_bFlash)
//		{
//			m_wndGrid2.SetStyleRange(CGXRange(4, 2),
//			  CGXStyle()
//					.SetTextColor(0)
//					.SetInterior(RGB(192,192,192))
//					.SetValue(::GetStringTable(IDS_TEXT_LINE_OFF))
//					);
//		}
/*		else
		{
			m_wndGrid2.SetStyleRange(CGXRange(4, 2),
				  CGXStyle()
 					.SetTextColor(0)
					.SetInterior(RGB(255,255,255))
					.SetValue(::GetStringTable(IDS_TEXT_LINE_OFF))
					);
		}
*/
//	}
//	strTemp = pDoc->ValueString(chSaveData.fVoltage, EP_VOLTAGE, TRUE);
//	m_wndGrid2.SetValueRange(CGXRange(5, 2), strTemp);				//총   전   압");
//	strTemp = pDoc->ValueString(chSaveData.fCurrent, EP_CURRENT, TRUE);
//	m_wndGrid2.SetValueRange(CGXRange(6, 2), strTemp);				//총   전   류");

	strTemp.Format("%d ℃", (int)ETC_PRECISION(gpData.sensorData.sensorData1[0].lData));
	m_wndGrid2.SetValueRange(CGXRange(5, 2), strTemp);				//설 정  온 도");
	strTemp.Format("%d ℃", (int)ETC_PRECISION(gpData.sensorData.sensorData1[1].lData));
	m_wndGrid2.SetValueRange(CGXRange(6, 2),  strTemp);				//현 재  온 도");

	strTemp.Format("%.1f'", ETC_PRECISION(gpData.sensorData.sensorData1[0].lData));
	m_TempLed1.SetText(strTemp);
	strTemp.Format("%.1f'", ETC_PRECISION(gpData.sensorData.sensorData1[1].lData));
	m_TempLed.SetText(strTemp);

	
	strTemp = pDoc->ValueString(fStepTime, EP_STEP_TIME);
	m_wndGrid2.SetValueRange(CGXRange(7, 2),  strTemp);				//Step진행시간");
	
	float fData;
	strTemp.Empty();
	if(pStep)
	{
		fData = pStep->m_fEndTime;
		if(fData != 0.0f)
		{
			strTemp.Format("%d", int(fStepTime/pStep->m_fEndTime*100.0f));
//			TRACE("Step %f/%f = %s\n", fStepTime, pStep->m_fEndTime, strTemp);
		}
	}
	m_wndGrid2.SetValueRange(CGXRange(8, 2),  strTemp);				//Step남은시간");

	strTemp = pDoc->ValueString(fTotTime, EP_STEP_TIME);
	m_wndGrid2.SetValueRange(CGXRange(9, 2),  strTemp);				//전체진행시간");

	fData = pModule->GetCondition()->GetStepTimeSum()+8.0f;			//8.0초는 Step 전이 시간 
	if(fData != 0.0f)
	{
		strTemp.Format("%d", int(fTotTime/fData*100.0f));
//		TRACE("Tot %f/%f = %s\n", fTotTime, fData, strTemp);
	}
	else
	{
		strTemp.Empty();
	}
	m_wndGrid2.SetValueRange(CGXRange(10, 2), strTemp);				//전체남은시간");
	m_wndGrid2.SetValueRange(CGXRange(11, 2), pDoc->GetTrayLoadState(m_nCurModuleID));			//투입 Tray 수");

	strTemp.Format("%d 개", nCellCnt);
	m_wndGrid2.SetValueRange(CGXRange(12, 2), strTemp);				//투입Cell수량");
	
//	strTemp.Format(")
//	m_wndGrid2.SetValueRange(CGXRange(13, 2), strTemp);				//총 전류");
//	m_wndGrid2.SetValueRange(CGXRange(16, 2), strTemp);				//작   업   자");
}

void CAgingView::SetCurrentModule(int nModuleID)
{
	if(m_nCurModuleID != nModuleID)						//다른 Module을 선택 하였을시
	{
		StopMonitoring();
		
		ReConfigGrid();
		UpdateState();			//Update Group State Grid

		StartMonitoring();

		m_CmdTarget.SetText(GetModuleName(nModuleID));
//		GetDlgItem(IDC_MODULE_STATIC)->SetWindowText(GetModuleName(nModuleID));
	}
}

void CAgingView::ReConfigGrid()
{
	CFormModule *pModule = ((CCTSMonDoc *)GetDocument())->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		return;

	COLORREF borderColor = RGB(255, 100, 0);
	
	//접
	int nTotRow = pModule->GetTotalJig()/JIG_STACK_COL;
	if(pModule->GetTotalJig()%JIG_STACK_COL)	nTotRow++;
	if(nTotRow < 1)		nTotRow++;
	nTotRow = (nTotRow)*2;		//Tray당 2열 필요 

	m_wndGrid.SetRowCount(nTotRow);
	m_wndGrid.SetColCount(COL_PER_TRAY*JIG_STACK_COL);

	CString strTemp;
	int nCnt = 0;
	int row = 0;
	int a = 0;
	for(row = 0; row<JIG_STACK_COL; row++)
	{
		for(a = 0; a<nTotRow/2; a++)
		{
			nCnt++;
			m_wndGrid.SetCoveredCellsRowCol((a+1)*2-1, COL_PER_TRAY*row+1, (a+1)*2, COL_PER_TRAY*row+1);
			m_wndGrid.SetStyleRange(CGXRange((a+1)*2-1, COL_PER_TRAY*row+1, (a+1)*2, COL_PER_TRAY*row+1), 
									CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(borderColor))
									);
			
			strTemp.Format("%s-%d", GetModuleName(m_nCurModuleID), long(a*2+1+row));
			m_wndGrid.SetStyleRange(CGXRange((a+1)*2-1, COL_PER_TRAY*row+1), CGXStyle().SetValue(strTemp).SetInterior(GetSysColor(COLOR_3DFACE)).SetDraw3dFrame(gxFrameRaised));
		}
		m_wndGrid.SetCoveredCellsRowCol(0, COL_PER_TRAY*row+2, 0, COL_PER_TRAY*row+3);
		m_wndGrid.SetValueRange(CGXRange(0, COL_PER_TRAY*row+1), "Jig");
		strTemp.Format("%d 열", row+1);
		m_wndGrid.SetValueRange(CGXRange(0, COL_PER_TRAY*row+2), strTemp);
		m_wndGrid.SetValueRange(CGXRange(0, COL_PER_TRAY*row+4), "충전기 전압");
		m_wndGrid.SetValueRange(CGXRange(0, COL_PER_TRAY*row+5), "충전기 전류");
	}

	for( row = 0; row < nTotRow; row++)
	{
		if(row%2 == 0)
		{
			for(a =0; a<JIG_STACK_COL; a++)
			{
				m_wndGrid.SetCoveredCellsRowCol(row+1, 2+(a*COL_PER_TRAY), row+1, 3+(a*COL_PER_TRAY));
				m_wndGrid.SetStyleRange(CGXRange().SetRows(row+1), CGXStyle()
																   .SetFont(CGXFont().SetSize(12).SetBold(TRUE))
																   .SetInterior(RGB(200, 230, 230)));
			}
			m_wndGrid.SetStyleRange(CGXRange(row+1, COL_PER_TRAY), 
										CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(borderColor)));
			m_wndGrid.SetStyleRange(CGXRange(0, COL_PER_TRAY), 
										CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(borderColor)));
			
		}
		else
		{
			m_wndGrid.SetStyleRange(CGXRange(row+1, COL_PER_TRAY), 
									CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(2).SetColor(borderColor)));
				
			m_wndGrid.SetStyleRange(CGXRange(row+1, 1, row+1, COL_PER_TRAY*2), 
									CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(2).SetColor(borderColor)));
		}
	}

	m_wndGrid.Redraw();
}

void CAgingView::ModuleConnected(int nModuleID)
{
	if(nModuleID == m_nCurModuleID)							//현재 선택된 Module이면 Grid를 Setting 시키고
	{														//Display Start
		ReConfigGrid();

		//현재 Window가 Active 되어 있지 않으면 사용 안함 

		if(((CMainFrame *)AfxGetMainWnd())->CompareCurrentSelectTab(CMainFrame::TAB_INDEX_AGING))	
		{
			((CCTSMonDoc *)GetDocument())->SetAutoReport(nModuleID);
			StartMonitoring();
		}
	}
}


LRESULT CAgingView::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	if(pGrid == (CMyGridWnd *)&m_wndGrid)
	{
		if(nRow < 1 || nCol <1)		//Target을 모듈로 바꾼다.
		{
		}
	}
	
	return 1;
}

void CAgingView::StartMonitoring()
{
	if(m_nMonitoringTimer)
	{
		KillTimer(601);
	}

	int nTimer = ((CCTSMonDoc *)GetDocument())->m_TopConfig.m_ChannelInterval*1000;

	m_nMonitoringTimer = SetTimer(601, nTimer, NULL);
}

void CAgingView::StopMonitoring()
{
	if(m_nMonitoringTimer)
	{
		KillTimer(601);
		m_nMonitoringTimer = 0;
	}
}

void CAgingView::ShowState()
{
	static BOOL bFlash = FALSE;
	bFlash = !bFlash;
	
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)		return;

	int nRow;
	CTray *pTray;
	WORD gpState = pModule->GetState();


	//Tray No가 있는데 Tray 감지가 안되거나
	//Tray No가 없는데 Tray가 감지된 경우 깜빡이게 한다.
	//////////////////////////////////////////////////////////////////////////

	COLORREF trayColor = RGB(255, 220, 220);		//Tray가 Loading되었을때 Color
	COLORREF normalColor = RGB(255, 255, 255);		//평상시 Color
	for(int tray = 0; tray<pModule->GetTotalJig(); tray++)
	{
		nRow = (tray/JIG_STACK_COL)*2+1;
			
		//Show TrayNo
		pTray = pModule->GetTrayInfo(tray);
		if(pTray == NULL)	break;

		if(EPTrayState(m_nCurModuleID, tray) == EP_TRAY_LOAD)
		{
			normalColor = trayColor;
		}
		else
		{
			normalColor = RGB(255, 255, 255);
		}
	
		//BCR을 읽어 놓은 상태이거나 
		//작업 예약을 해놓은 상태 이거나 
		if((pTray->GetBcrRead() || pTray->GetTestReserved()) && gpState != EP_STATE_RUN)
		{
			if(bFlash)
			{
				m_wndGrid.SetStyleRange(CGXRange(nRow, tray%2*COL_PER_TRAY+2), CGXStyle().SetInterior(RGB(192, 192 , 192)));
			}
			else
			{
				m_wndGrid.SetStyleRange(CGXRange(nRow, tray%2*COL_PER_TRAY+2), CGXStyle().SetInterior(normalColor));
			}
		}
		else
		{
			//원래색으로 
			m_wndGrid.SetStyleRange(CGXRange(nRow, tray%2*COL_PER_TRAY+2), CGXStyle().SetInterior(normalColor));
		}
	}
	//////////////////////////////////////////////////////////////////////////

//	m_ChargeLamp.Depress(bFlash);
//	m_DisChargeLamp.Depress(bFlash);
	//////////////////////////////////////////////////////////////////////////

	CString strTemp;

		//Tower lamp display
	switch(gpState)
	{
	case EP_STATE_RUN:
		m_RedLamp.Depress(FALSE);
		m_YellowLamp.Depress(FALSE);
		m_GreenLamp.Depress(TRUE);
		break;
	case EP_STATE_EMERGENCY:
		m_RedLamp.Depress(TRUE);
		m_YellowLamp.Depress(FALSE);
		m_GreenLamp.Depress(FALSE);
		break;
	case EP_STATE_IDLE:
	case EP_STATE_STANDBY:
	case EP_STATE_STOP:
	case EP_STATE_END:
		m_RedLamp.Depress(FALSE);
		m_YellowLamp.Depress(TRUE);
		m_GreenLamp.Depress(FALSE);
		break;
	default:
		m_RedLamp.Depress(FALSE);
		m_YellowLamp.Depress(FALSE);
		m_GreenLamp.Depress(FALSE);
		break;
	}

	//Display run & remain time
//	CTestCondition *pTestCon = pModule->GetCondition();
//	CStep *pStep = pTestCon->GetStep(pModule->GetCurStepNo()-1);
	gpState = pModule->GetState(TRUE);
			
	COLORREF OnColor = RGB(255, 0, 0);
	COLORREF OnTextColor = RGB(128,128,128);
	COLORREF OffColor = RGB(192,192,192);
	COLORREF OffTextColor = RGB(128,128,128);
	if(gpState == EP_STATE_CHARGE)
	{
		m_ChargeLabel.SetBkColor(OnColor);
		m_ChargeLabel.SetTextColor(OnTextColor);
	}
	else
	{
		m_ChargeLabel.SetBkColor(OffColor);
		m_ChargeLabel.SetTextColor(OffTextColor);
	}
		
	OnColor = RGB(0, 0, 255);
	if(gpState == EP_STATE_DISCHARGE)
	{
		m_DischargeLabel.SetBkColor(OnColor);
		m_DischargeLabel.SetTextColor(OnTextColor);
	}
	else
	{
		m_DischargeLabel.SetBkColor(OffColor);
		m_DischargeLabel.SetTextColor(OffTextColor);
	}	
}

void CAgingView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	KillTimer(602);
	StopMonitoring();
}


void CAgingView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndGrid == (CMyGridWnd *)pWnd)
	{
		m_wndGrid.Copy();
	}
	else if(&m_wndGrid2 == (CMyGridWnd *)pWnd)
	{
		m_wndGrid2.Copy();
	}	
}

void CAgingView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndGrid == (CMyGridWnd *)pWnd || &m_wndGrid2 == (CMyGridWnd *)pWnd )
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
	
}
