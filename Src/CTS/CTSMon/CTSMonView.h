// CTSMonView.h : interface of the CCTSMonView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSMonVIEW_H__D83CEA85_5071_4011_B48A_7BDC0CB9BCC9__INCLUDED_)
#define AFX_CTSMonVIEW_H__D83CEA85_5071_4011_B48A_7BDC0CB9BCC9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCTSMonView : public CView
{
protected: // create from serialization only
	CCTSMonView();
	DECLARE_DYNCREATE(CCTSMonView)

// Attributes
public:
	CCTSMonDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSMonView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCTSMonView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCTSMonView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CTSMonView.cpp
inline CCTSMonDoc* CCTSMonView::GetDocument()
   { return (CCTSMonDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSMonVIEW_H__D83CEA85_5071_4011_B48A_7BDC0CB9BCC9__INCLUDED_)
