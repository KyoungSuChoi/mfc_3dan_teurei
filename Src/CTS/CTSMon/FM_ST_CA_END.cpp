#include "stdafx.h"
#include "CTSMon.h"
#include "FMS.h"
#include "FM_Unit.h"
#include "FM_STATE.h"
#include "MainFrm.h"

CFM_ST_CA_END::CFM_ST_CA_END(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_CALI(_Eqstid, _stid, _unit)
{
	m_preState = 0;
}


CFM_ST_CA_END::~CFM_ST_CA_END(void)
{
}

VOID CFM_ST_CA_END::fnEnter()
{	
	// m_Unit->fnGetFMS()->fnResultDataStateUpdate(NULL, RS_END);
	m_preState = 0;
	
	FMS_ERRORCODE _rec = FMS_ER_NONE;

	fnSetFMSStateCode(FMS_ST_END);
	m_CalchkFlag = 0;

	// m_Unit->fnGetFMS()->fnSend_E_CHARGER_WORKEND(_rec);

	CHANGE_FNID(FNID_PROC);

	TRACE("CFM_ST_CA_END::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_END::fnProc()
{
	if(-1 == m_iWaitCount)
	{
		fnSetFMSStateCode(FMS_ST_END);
	}
	
	if(1 == m_iWaitCount)
	{
		if( m_preState == 0 )
		{
			fnSetFMSStateCode(FMS_ST_BLUE_END);
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_RED_END);
		}

// 		if( !m_Unit->fnGetModuleTrayState() )
// 		{
// 			fnSetFMSStateCode(FMS_ST_RED_END);
// 		}
// 		else
// 		{
// 			fnSetFMSStateCode(FMS_ST_BLUE_END);
// 		}

		// 1. 타이머를 다시 시작
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_CA_END::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_END::fnExit()
{	
	if(1 == m_iWaitCount)
	{
		if( !m_Unit->fnGetModuleTrayState() )
		{
			fnSetFMSStateCode(FMS_ST_RED_END);
			// CFMSLog::WriteLog("[Stage %d] 작업 공정이 완료된 트레이를 배출 후 출고 완료 신호를 받지 못한 Stage 가 발견되었습니다. 해당 Stage 를 초기화 후 FMS PC 에서 [작업조회] => [Rack 위치 제공 조회] 정보를 초기화 해주시기 바랍니다.",  m_Unit->fnGetModuleID() );
			CFMSLog::WriteLog("[Stage %s] 작업 공정이 완료된 트레이를 배출 후 출고 완료 신호를 받지 못한 Stage 가 발견되었습니다. 해당 Stage 를 초기화 후 FMS PC 에서 [작업조회] => [Rack 위치 제공 조회] 정보를 초기화 해주시기 바랍니다.",  m_Unit->fnGetModule()->GetModuleName() );			
			// CHANGE_STATE(AUTO_ST_VACANCY);
		}
		else
		{
			fnSetFMSStateCode(FMS_ST_BLUE_END);
		}
		
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_CA_END::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_END::fnSBCPorcess(WORD _state)
{
	CFM_ST_CALI::fnSBCPorcess(_state);

//	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(CALI_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(CALI_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(CALI_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			CHANGE_STATE(CALI_ST_READY);
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(CALI_ST_READY);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(CALI_ST_ERROR);
		}
		break;
	case EP_STATE_CALIBRATION:
		{
			CHANGE_STATE(CALI_ST_CALI);
		}
		break;
	}
	TRACE("CFM_ST_CA_END::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_CA_END::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 3 )
	{
		fnWaitInit();

		st_S2F413 data;
		ZeroMemory(&data, sizeof(st_S2F413));
		memcpy(&data, _recvData->data, sizeof(st_S2F413));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if( m_Unit->fnGetModuleTrayState() )
		{
			_error = ER_NO_Process;
		}
		else
		{
			_error = m_Unit->fnGetFMS()->SendSBCInitCommand();
		}

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		
		//END -> VACANCY
		int nModuleID = m_Unit->fnGetModuleID();
		EP_GP_STATE_DATA prevState;
		memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
		prevState.state = EP_STATE_IDLE;
		CFormModule *pModule = NULL;
		pModule = m_Unit->fnGetModule();
		pModule->SetState(prevState);	
	}

	TRACE("CFM_ST_CA_END::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}