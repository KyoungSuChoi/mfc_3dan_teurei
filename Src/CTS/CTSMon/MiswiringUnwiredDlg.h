#pragma once

#include "MyGridWnd.h"
#include "CTSMonDoc.h"
#include "afxwin.h"
#include "InformationDlg.h"
#include "../BFCtrl/PictureEx.h"

// CMiswiringUnwiredDlg 대화 상자입니다.

#define MIS_UN_WIRING_COLUMN_BOARD_NO				1
#define MIS_UN_WIRING_COLUMN_CHANNEL_NO				2
#define MIS_UN_WIRING_COLUMN_TOTAL_CHANNEL_NO		3
#define MIS_UN_WIRING_COLUMN_PLUS_MINUS_DIVISION	4
#define MIS_UN_WIRING_COLUMN_MISWIRING_LOWER_RANGE	5
#define MIS_UN_WIRING_COLUMN_SENSING_VALUE			6
#define MIS_UN_WIRING_COLUMN_MISWIRING_UPPER_RANGE	7
#define MIS_UN_WIRING_COLUMN_UNSWIRED_REF_VALUE		8
#define MIS_UN_WIRING_COLUMN_INVERTER_VALUE_1		9
#define MIS_UN_WIRING_COLUMN_INVERTER_VALUE_2		10

#define MISWIRING_COLUMN_MAX_COUNT					10

#define COLOR_RED			RGB(255, 0, 0)		// RED
#define COLOR_GREEN			RGB(0, 255, 0)		// GREEN
#define COLOR_YELLOW		RGB(255, 255, 0)	// YELLOW

class CMiswiringUnwiredDlg : public CDialog
{
	DECLARE_DYNAMIC(CMiswiringUnwiredDlg)
private :
	CCTSMonDoc* m_pDoc;
	int			m_nCurModuleNo;
	CInformationDlg* m_pInformationDlg;

	CMyGridWnd m_wndMiswiringUnwiringGrid;
	CColorButton2 m_cPictureBtn;
	CPictureEx m_ctrl_Img_MissUnwired;

	void	InitializeGrid();
	BOOL	InitFirstRow();
	BOOL	InitDefaultRows(int nBoardCount = 9, int nChannelCountPerBoard = 8);
	BOOL	ClearGrid();

	void	ShowInformation( int nUnitNum, CString strMsg, int nMsgType );

public :
	CMiswiringUnwiredDlg(CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleNo);

	void SetCurrentModuleNo(int nModuleNo);
	void OnMiswiringUnwiredCheckResponse(int nModuleNo, EP_CMD_CHANNEL_MISWIRING_UNWIRED_CHECK_RESPONSE response);

public:
	CMiswiringUnwiredDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMiswiringUnwiredDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MISWIRING_UNWIRED_DLG };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnEraseBkgnd(CDC* pDC);
	LRESULT OnGridMouseLButtonDown(WPARAM wParam, LPARAM lParam);
	BOOL	ImageChange(int iBoardNo, int iChannelNo);

	DECLARE_MESSAGE_MAP()
public:	
	afx_msg void OnBtnClickedMiswiringUnwiredRefresh();
	afx_msg void OnCbnSelchangeComboStageList();	
	void InitializeComboList();
	CComboBox m_combo_Stage_List;
};
