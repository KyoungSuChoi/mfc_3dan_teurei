#pragma once

//db insert add : //
//#include <mysql.h>
//#pragma comment(lib, "libmysql.lib")
//
//typedef struct
//{	
//	MYSQL_RES*  res;
//	MYSQL_ROW   row;
//	MYSQL_FIELD *fields;
//	int		    nfield;
//	int         nrecord;
//} MYSQLINFO;
//E

#ifndef AUTH_ADMIN
	#define AUTH_ADMIN		    1   //admin계정
#endif

#ifndef AUTH_CLIENT
	#define AUTH_CLIENT		    2   //client계정
#endif

//! 데이터베이스 쿼리 클래스
class MySql
{
public:

	CWnd          *m_pWnd;
	MYSQL         m_Mysql;	
	BOOL          m_bCon,m_bMbox,m_bConfg;	
	CString       m_sIp,m_sId,m_sPwd,m_sDb;
	int           m_iPort;
	CString       m_dxMesg;
	UINT          m_dwSqlfile;
	CString       m_strErr;
	int           m_iErr;

	void		  DBInit(CString sIp,CString sId,CString sPwd,CString sDb,int iPort=3306,BOOL bMbox=TRUE);
	BOOL          DBConn(CString sIp,CString sId,CString sPwd,CString sDb,int iPort=3306,BOOL bMbox=TRUE,BOOL bCreate=FALSE);	
    BOOL          ReConn();
	BOOL          DBPing();
	BOOL          DBClose();
	BOOL          DBStat();
	BOOL          DBConnCheck();
	BOOL          DBOption();	
	BOOL          DBRefresh();
	CString		  DBError(BOOL bflag);
	BOOL          DBUse(CString db);
	BOOL          DBUserInit();
	void		  log_save(CString sType,CString sLog);

	CStringArray* list_Query(CString sql);
	int			  list_RecordCnt(CStringArray* list);
	int		      list_FieldCnt(CStringArray* list);
    CString       list_FD(CStringArray *list,int record,CString name);	
	CString       list_FDA(CStringArray *list,int record,CString name);
	BOOL          list_ChkList(CStringArray *list);
	void          list_FreeList(CStringArray *list);
	int           list_FieldNum(CStringArray *list,CString name)  ;

	CString       test1();
	BOOL          obj_ChkMysqlInfo(MYSQLINFO* MysqlInfo);
	void          obj_MyDelete(MYSQLINFO* MysqlInfo);
	void		  obj_MyRefresh();
	void          obj_ResRefresh(MYSQL_RES*  res);
	void          obj_InfoRefresh(MYSQLINFO* MysqlInfo);
	void          obj_ResMyRefresh(MYSQL_RES*  res);
	MYSQLINFO*    obj_Result(CString sql);
	CString       obj_FD(MYSQLINFO *MysqlInfo,CString f);
	int           obj_FDInt(MYSQLINFO *MysqlInfo,CString f);
	CString       obj_FD(int fields,MYSQL_ROW row,MYSQL_FIELD *field,CString f);
	CString       obj_GetData(CString sql,CString f);
	
	BOOL	      SetQuery(CString arraySql);//input,update,delete
	CStringArray* ShowTable();
	BOOL          ChkTable(CString table);
	BOOL          ChkDatabase(CString db);
	CString       GetDBTime();
	CString       GetDBTimeFmt();


	LONGLONG      GetMax(CString table,CString f);	

	CString       GetData1(CString table,CString n1,CString v1,CString f);
	CString       GetData2(CString table,CString n1,CString v1,CString n2,CString v2,CString f);
	CString       GetData3(CString table,CString n1,CString v1,CString n2,CString v2,CString n3,CString v3,CString f);
	CString       GetData4(CString table,CString n1,CString v1,CString n2,CString v2,
										 CString n3,CString v3,CString n4,CString v4,
										 CString f);
	CString       GetData5(CString table,CString n1,CString v1,CString n2,CString v2,
										 CString n3,CString v3,CString n4,CString v4,
										 CString n5,CString v5,
										 CString f);
	CString       GetData6(CString table,CString n1,CString v1,CString n2,CString v2,
										 CString n3,CString v3,CString n4,CString v4,
										 CString n5,CString v5,CString n6,CString v6,
										 CString f);
	CString       GetData7(CString table,CString n1,CString v1,CString n2,CString v2,
										 CString n3,CString v3,CString n4,CString v4,
										 CString n5,CString v5,CString n6,CString v6,
										 CString n7,CString v7,
										 CString f);
	CString       GetData8(CString table,CString n1,CString v1,CString n2,CString v2,
										 CString n3,CString v3,CString n4,CString v4,
										 CString n5,CString v5,CString n6,CString v6,
										 CString n7,CString v7,CString n8,CString v8,
										 CString f);
//db insert
	BOOL		  GetTbName_L( CString &strTbName, CString &strIdx );
//E
	int           GetCount(CString table);
	int           GetCount1(CString table,CString n1,CString v1);
	/*int           GetCount2(CString table,CString n1,CString v1,CString n2,CString v2);
	int           GetCount3(CString table,CString n1,CString v1,
		                                  CString n2,CString v2,
										  CString n3,CString v3);
	int           GetCount4(CString table,CString n1,CString v1,
										  CString n2,CString v2,
									      CString n3,CString v3,
									      CString n4,CString v4);
*/
	BOOL          SetDelete1(CString table,CString n1,CString v1) ;
/*	BOOL          SetDelete2(CString table,CString n1,CString v1,CString n2,CString v2) ;

	BOOL          SetEditNow1(CString table,CString n1,CString n2,CString v2);
	BOOL          SetEditNow3(CString table, CString n1,CString v1,
										     CString n2,CString v2,
										     CString n3,CString v3,
										     CString n4,
										     CString n5,CString v5);*/

	BOOL          SetEditData1(CString table,CString n1,CString v1,CString n2,CString v2);	
	BOOL          SetEditData2(CString table,CString n1,CString v1,
						               CString n2,CString v2,
									   CString n3,CString v3);
	BOOL          SetEditData3(CString table,CString n1,CString v1,
						               CString n2,CString v2,
									   CString n3,CString v3,
									   CString n4,CString v4);
	
	BOOL		  SetEditData1Con6(CString table,CString n1,CString v1,
										   CString c1,CString d1,
										   CString c2,CString d2,
										   CString c3,CString d3,
										   CString c4,CString d4,
										   CString c5,CString d5,
										   CString c6,CString d6);

	/*BOOL		  SetEditData1Con2(CString table,CString n1,CString v1,
										   CString n2,CString v2,
										   CString n3,CString v3);
	BOOL		  SetEditPwd1Con2(CString table,CString n1,CString v1,
										   CString n2,CString v2,
										   CString n3,CString v3);
	CStringArray* db_Schedlist();
	CStringArray* db_Datalist(CString tfilename=_T(""));
	CString       db_GetPlayTime(CStringArray* list,CString tfilename);

	CStringArray* db_Grouplist();	
	CStringArray* db_Adminlist();
	CStringArray* db_Clientlist(CString nogroup=_T(""),int itype=0);
	CStringArray* db_Contentslist(CString nosched);
	CStringArray* db_TickerResvslist();
	void          db_GetFileInfo(CString tfilename,CString &sfilename,CString &splaytime,CString &sfilesize);

	CString       db_GetSchedXml(CString nosched);	
	CString       db_GetClientSched(CString nogroup,CString noclient,CString &xmlData);
	CStringArray* db_GetClientMesg(CString nogroup,CString noclient);
	CStringArray* db_GetClientCommd(CString nogroup,CString noclient);

	CStringArray* db_Loglist(int iauth,CString noclient,CString tplogs,CString stime,CString etime);
	CStringArray* db_SchedResvslist();
	BOOL          db_SetOption(CString variable,CString value,CString etc);
	CStringArray* db_GetOption();

	BOOL          db_LoginA(CString clientid,CString clientpwd,int iauth);
	BOOL          db_LoginB(CString clientid,CString clientpwd,int iauth,
		                    CString &nogroup,CString &noclient,CString &nmclient,CString &authkey);
	BOOL          db_SetClientInfo(CString noclient,CString nosched,
								   int hori,int vert,
								   CString ipaddr,CString macaddr,CString diskusage,CString version);

	int           db_deleteAdmin(CString noclient);
	int           db_deleteGroup(CString nogroup);
	int           db_deleteClient(CString noclient);
	int           db_deleteSchedResv(CString noresvs);
	int           db_deleteTickerResv(CString nomessg);
	BOOL          db_deleteSched(CString noclient,CString nosched);	
	
	BOOL          db_InsertGroup(CString noadmin,CString nmgroup);
	BOOL          db_InsertSched(CString noadmin,CString nmsched,CString hori,CString vert,CString xmldata,CString etc);
	BOOL          db_InsertDatas(CString noadmin,CString sfilename,CString tfilename,CString filesize,CString tpmedia,CString playtime);
	BOOL          db_InsertContents(CString noadmin,CString Nosched,CStringArray &FileList);	
	BOOL          db_InsertClient(CString noadmin,CString nogroup,CString nmclient,
							      CString clientid,CString clientpwd,CString authkey);
	BOOL          db_InsertAdmin(CString noadmin,CString nmclient,
							     CString clientid,CString clientpwd,CString authkey);
	BOOL          db_InsertLog(CString noclient,CString type,CString log,CString playtime);
	BOOL          db_InsertSchedResv(CString noadmin,CString nogroup,CString noclient,
							         CString tpresvs,CString nosched,CString dtresvs,
							         CString weeks);	
	BOOL          db_InsertTickerResv(CString noadmin,CString nogroup,CString noclient,
									   CString tpmessg,CString dtmessg,CString weeks,CString dttime,
									   CString sdata);

	CStringArray* db_CommResvslist();
	int           db_deleteCommdResv(CString nocommd);
	BOOL          db_InsertCommdResv(CString noadmin,CString nogroup,CString noclient,
							   CString tpcommd,CString dtcommd,CString weeks,CString dttime,
							   CString sdata);*/
		
public:
	MySql(void);
	virtual ~MySql(void);

};
