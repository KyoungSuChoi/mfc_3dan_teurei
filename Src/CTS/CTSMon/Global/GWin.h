/*#include <imm.h>
#include "Windows.h"
#include "Winsvc.h"
#include "time.h"
#include <WtsApi32.h>
#include <UserEnv.h>
#include <tlhelp32.h>
#include <mmsystem.h>
#include "Psapi.h"
    
#pragma comment(lib,"WtsApi32.lib")
#pragma comment(lib,"UserEnv.lib")
#pragma comment(lib,"imm32")
#pragma comment( lib, "Psapi.lib" )
#pragma comment(lib,"winmm.lib")

// Non defined virtual key codes.
#define VK_0				0x30
#define VK_1				0x31
#define VK_2				0x32
#define VK_3				0x33
#define VK_4				0x34
#define VK_5				0x35
#define VK_6				0x36
#define VK_7				0x37
#define VK_8				0x38
#define VK_9				0x39
#define VK_A				0x41
#define VK_B				0x42
#define VK_C				0x43
#define VK_D				0x44
#define VK_E				0x45
#define VK_F				0x46
#define VK_G				0x47
#define VK_H				0x48
#define VK_I				0x49
#define VK_J				0x4A
#define VK_K				0x4B
#define VK_L				0x4C
#define VK_M				0x4D
#define VK_N				0x4E
#define VK_O				0x4F
#define VK_P				0x50
#define VK_Q				0x51
#define VK_R				0x52
#define VK_S				0x53
#define VK_T				0x54
#define VK_U				0x55
#define VK_V				0x56
#define VK_W				0x57
#define VK_X				0x58
#define VK_Y				0x59
#define VK_Z				0x5A

#define RELEASE(p)			if(p) { delete p; p = NULL; }
#define HDCLOSE(p)			if(p) { CloseHandle(p); p = NULL; }
//WS_EX_TRANSPARENT

typedef BOOL	(WINAPI *PROCISHUNGAPPWINDOW)(HWND);
typedef BOOL	(WINAPI *PROCISHUNGTHREAD)(DWORD);

#define SCREENX            GetSystemMetrics(SM_CXSCREEN)
#define SCREENY            GetSystemMetrics(SM_CYSCREEN)

#define SCREENVX           GetSystemMetrics(SM_CXVIRTUALSCREEN)
#define SCREENVY           GetSystemMetrics(SM_CYVIRTUALSCREEN)

#define CAPTIONH           GetSystemMetrics(SM_CYCAPTION)
*/
class GWin
{
public:   
	GWin(); 
    ~GWin();
    		
	static CString win_GetAppPath()
	{//1 current directory
		CString strExeFileName;
	    HMODULE hModule=::GetModuleHandle(NULL); // handle of current module    
		VERIFY(::GetModuleFileName(hModule, strExeFileName.GetBuffer(_MAX_PATH),_MAX_PATH));
		strExeFileName.ReleaseBuffer();

		#ifdef _UNICODE	
			wchar_t Drive[_MAX_DRIVE];
			wchar_t Path[_MAX_PATH];
			//wchar_t Filename[_MAX_FNAME];
			//wchar_t Ext[_MAX_EXT];
			_wsplitpath(strExeFileName, Drive, Path, NULL, NULL); 
		#else
			char Drive[_MAX_DRIVE];
			char Path[_MAX_PATH];
			//char Filename[_MAX_FNAME];
			//char Ext[_MAX_EXT];
			_splitpath(strExeFileName, Drive, Path, NULL, NULL); 
		#endif
	
		CString appPath=CString(Drive)+CString(Path);
		appPath.TrimRight(_T("/"));
		appPath.TrimRight(_T("\\"));

		return appPath;// delete trailing backslash
	}

/*	static void win_BootDelFile(CString file)
	{//2 NT 부팅시 파일 삭제
		//HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Session Manager=PendingFileRenameOperations
   	    MoveFileEx(file, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	}

	static void win_BootMoveFile(CString sfile,CString tfile)
	{//3 NT 부팅시 파일 삭제
		//HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Session Manager=PendingFileRenameOperations
   	    MoveFileEx(sfile, tfile, MOVEFILE_DELAY_UNTIL_REBOOT);
	}

	static CString win_GetClsidPath(int clsid)
	{//4 시스템 폴더 http://msdn.microsoft.com/en-us/library/bb762494(VS.85).aspx
		//CSIDL_COMMON_DESKTOPDIRECTORY desktop
		//CSIDL_PROGRAM_FILES program files
		TCHAR szSpecialPath[MAX_PATH] = {0};
		SHGetSpecialFolderPath(NULL, szSpecialPath, clsid, FALSE);
		
		CString path=szSpecialPath;	

		path.TrimRight(_T("/"));
		path.TrimRight(_T("\\"));		
		return path;
	}

	static CString win_GetTime(CString sort)
	{//5 시간 구하기
		SYSTEMTIME now;
		CString str_time=_T("");
		GetLocalTime(&now);
		
		if(sort==_T("update"))
		{
		   str_time.Format(_T("%04d-%02d-%02d"),now.wYear,now.wMonth,now.wDay);
		} 
		else if(sort==_T("log"))
		{
		   str_time.Format(_T("[%04d-%02d-%02d %02d:%02d:%02d]"),
			               now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);
		}
		
		return str_time;
	}

	static void win_Doevents(HWND hWnd=NULL,DWORD nTimeOut=5000)
	{//6 윈도우 메세지 처리
		DWORD nStart=::GetTickCount();
		MSG Message= { 0 };		
		while (PeekMessage(&Message, hWnd, 0, 0, PM_NOREMOVE) == TRUE)
		{ 	   
			if((::GetTickCount()-nStart)>nTimeOut) return;

		   if(GetMessage(&Message, hWnd, 0, 0) ) 
		   {//큐가 비어 있을 경우 무한 대기를 하기 때문에 백그라운드 작업을 할수 없다.
			//메시지 큐에서 메시지를 읽되 무한 대기하며 메시지를 무조건 큐에서 제거한다.
			  TranslateMessage(&Message); 
			  DispatchMessage(&Message); 	    
		   } 
		   else
		   {
			   return;  	   
		   }
		}	 
	}
	
	static void win_KillBrowserCall()
	{//7 강제 종료
		EnumWindows((WNDENUMPROC)win_KillBrowserProc, (LPARAM)0);
	}

	static BOOL CALLBACK win_KillBrowserProc(HWND hwnd, LPARAM lParam)
	{//8 실행중인 IEFrame, CabinetWClass, ExploreWClass 종료
		CString IEFrameStr(_T("IEFrame"));    
		CString CabinetWClass(_T("CabinetWClass")); 
		CString ExploreWClass(_T("ExploreWClass")); 

		#ifdef _UNICODE	
			wchar_t classname[1024];    // 닫기를 위한 동작  
		#else
			char classname[1024];    // 닫기를 위한 동작  
		#endif
		
		if(GetClassName(hwnd, classname, 1024))
		{       
			CString Class(classname);       
			if(hwnd)
			{ 
				if(classname == IEFrameStr || 
				   classname == CabinetWClass ||
				   classname == ExploreWClass )
				{            
				   ::PostMessage(hwnd, WM_CLOSE,0,0);    
				}
			}
		}
		return TRUE;
	}
	
	static BOOL win_AfxExtractSubString(CString& rString, LPCTSTR lpszFullString,int iSubString, TCHAR chSep)
	{//9 문자열을 특정 문자를 기준으로 분리
		if (lpszFullString == NULL) return FALSE;

		while (iSubString--)
		{
			 lpszFullString = _tcschr(lpszFullString, chSep);
			 if (lpszFullString == NULL)
			 {
				  rString.Empty();        // return empty string as well
				  return FALSE;
			 }
			 lpszFullString++;       // point past the separator
		}
		LPCTSTR lpchEnd = _tcschr(lpszFullString, chSep);
		int nLen = (lpchEnd == NULL) ? lstrlen(lpszFullString) : (int)(lpchEnd - lpszFullString);
    
		if(nLen<1) return FALSE;

		memcpy(rString.GetBufferSetLength(nLen), lpszFullString, nLen*sizeof(TCHAR));
		return TRUE;
	}

	static BOOL win_ChkWinClass(HWND hWnd,CString value)
	{//10 클래스 이름 비교
		if(hWnd==NULL) return FALSE;
		if(value==_T(""))  return FALSE;
		if(::IsWindow(hWnd)==FALSE) return FALSE;

		CString className=win_GetClassName(hWnd);
		if(value==className) return TRUE;

		return FALSE;
	}
	
	static CString win_GetClassName(HWND hWnd)
	{//11 윈도우 클래스 이름
		if(hWnd==NULL) return _T("");

		TCHAR buf[MAX_PATH];
		::GetClassName(hWnd, (LPTSTR)&buf, MAX_PATH);
		
		return (CString)buf;
	}
	
	static void win_SetText(HWND hWnd,CString value)
	{//12 윈도우 타이틀 저장
		if(!hWnd) return;

		#ifdef _UNICODE	
			::SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)(LPCWSTR)value);  
		#else
			::SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)(LPCSTR)value);  
		#endif
	}

	static void win_SetText(CWnd *Wnd,CString value)
	{//108 윈도우 타이틀 저장
		if(Wnd && Wnd->m_hWnd)
		{
			#ifdef _UNICODE	
				::SendMessage(Wnd->m_hWnd, WM_SETTEXT, 0, (LPARAM)(LPCWSTR)value);  
			#else
				::SendMessage(Wnd->m_hWnd, WM_SETTEXT, 0, (LPARAM)(LPCSTR)value);  
			#endif
		}
	}
	
	static CString win_GetStr(HWND hWnd)
	{//13 GetWindowText
		if(hWnd==NULL) return _T("");

		char szBuff[MAX_PATH]={0};
		SendMessage(hWnd, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)szBuff);	
		return (CString)szBuff;
	}*/

	static CString win_GetText(HWND hWnd)
	{//14 윈도우 텍스트 구하기
		if(hWnd==NULL) return _T("");
		
		#ifdef _UNICODE			   
			static wchar_t buf[MAX_PATH];
			::GetWindowText(hWnd,(LPWSTR)&buf, MAX_PATH);
		#else
		    TCHAR buf[MAX_PATH];
			::GetWindowText(hWnd,(LPSTR)&buf, MAX_PATH);
		#endif	
						
		return (CString)buf;
	}

	static CString win_GetText(CWnd *wnd)
	{//15 윈도우 텍스트 구하기
		if(!wnd) return _T("");

		CString str=_T("");
		wnd->GetWindowText(str);
		
		return str;
	}

	/*
	static CRect win_GetAbsRect(CWnd *gwnd,CWnd *wnd)
	{//16 윈도우의 절대 위치
		if(gwnd==NULL || wnd==NULL) return CRect(0,0,0,0);
		CRect rect;
		wnd->GetClientRect(&rect);
		wnd->ClientToScreen(&rect);
		gwnd->ScreenToClient(rect);

		return rect;
	}

	static CRect win_GetAbsRect(HWND hWnd)
	{//17 윈도우 절대위치
		CRect rect;
		::GetWindowRect(hWnd,rect);
		return rect;
	}

	static CRect win_GetRect(HWND hWnd)
	{//18 윈도우의 상대 위치
		if(hWnd==NULL) return CRect(0,0,0,0);

		CRect rect;
		::GetClientRect(hWnd,&rect);

		return rect;
	}

	static CRect win_GetRect(CWnd *wnd)
	{//19 윈도우의 상대 위치
		if(wnd==NULL) return CRect(0,0,0,0);

		CRect rect;
		::GetClientRect(wnd->m_hWnd,&rect);

		return rect;
	}


	static void win_SysDelay(DWORD comTime,int iType=0)
	{//20 딜레이			
		if(iType==1)
		{
			Sleep(comTime);
			return;
		}

		DWORD dwStart = GetTickCount();
		MSG msg;
		while(GetTickCount()-dwStart<comTime){
		  if(PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE)){
			   TranslateMessage(&msg);
			   DispatchMessage(&msg);
			   Sleep(1);
		  }
		}
	}	

	static void win_SetMenuBmp(CMenu &menu,HINSTANCE gint,int pos,UINT BMP1,UINT BMP2) 
	{//21 메뉴에 아이콘 넣기 SetMenuBmp(m_Menu,AfxGetInstanceHandle(),0,IDB_BMP_MONITOR,IDB_BMP_MONITOR);
		HBITMAP hbmp1=NULL;
		HBITMAP hbmp2=NULL;

		if(BMP1>0) hbmp1=LoadBitmap(gint,MAKEINTRESOURCE(BMP1));  
		if(BMP2>0) hbmp2=LoadBitmap(gint,MAKEINTRESOURCE(BMP2));

		if(!hbmp1 || !hbmp2) return;

		int ret=::SetMenuItemBitmaps(menu,pos,MF_BYCOMMAND | MF_BYPOSITION, hbmp1, hbmp2);
	}

	static void win_SetMenuBmp(CMenu &menu,int pos,HBITMAP hbmp1,HBITMAP hbmp2) 
	{//22 메뉴에 bmp넣기
		if(!hbmp1 || !hbmp2) return;

		int ret=::SetMenuItemBitmaps(menu,pos,MF_BYCOMMAND | MF_BYPOSITION, hbmp1, hbmp2);
	}

	static void win_SetMenuState(CMenu *menu,int pos,BOOL bFlag) 
	{//23 메뉴 상태
		if(!menu) return;
		if(menu->GetMenuItemCount()<1) return;

		int nID=menu->GetMenuItemID(pos);

		int nState=MF_BYCOMMAND|MF_ENABLED;
		if(bFlag==FALSE)  nState=MF_BYCOMMAND|MF_GRAYED;
		
		menu->EnableMenuItem(nID,nState);
	}

	static CString win_GetMenuString(CMenu *menu,int nID) 
	{//24 메뉴 스트링 구하기
		if(!menu) return _T("");

		CString str=_T("");
	    menu->GetMenuString(nID,str,MF_BYCOMMAND);

		return str;
	}

	static CString win_GetErorMsg(DWORD error)
	{//25 에러 메세지	
		LPVOID lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
	 				  FORMAT_MESSAGE_FROM_SYSTEM |
					  FORMAT_MESSAGE_IGNORE_INSERTS,
					  NULL,
					  error, //GetLastError()
					  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					  (LPTSTR) &lpMsgBuf,
					  0,
					  NULL);
		CString str=(LPCTSTR)lpMsgBuf;
		LocalFree(lpMsgBuf);
		return str;
	}

	static void win_ImmClear(HWND hWnd)
	{//26 한글/영어 버퍼 클링어
		HWND subhWnd = ImmGetDefaultIMEWnd(hWnd);
		HIMC hiNow = ImmGetContext(subhWnd);
    
		if(hiNow)
		{			
			ImmSetCompositionString(hiNow, SCS_SETSTR, NULL, 0, NULL, 0);//imm clear        		
			ImmReleaseContext(hWnd,hiNow);
		}
	}

	static void win_ImmSetStr(HWND hWnd,CString str)
	{//27 
		HWND subhWnd = ImmGetDefaultIMEWnd(hWnd);
		HIMC hiNow = ImmGetContext(subhWnd);
    
		if(hiNow)
		{	
			char *buf=(LPSTR)(LPCTSTR)str;
			int len=strlen(buf);
			ImmSetCompositionString(hiNow, SCS_SETSTR, buf, len, buf, len);//imm clear        		
			ImmReleaseContext(hWnd,hiNow);
		}
	}

	static CString win_ImmGetStr(HWND hWnd)
	{//28 imm clear
		
		HWND subhWnd = ImmGetDefaultIMEWnd(hWnd);
		HIMC hiNow = ImmGetContext(subhWnd);
	    
		if(hiNow)
		{
			TCHAR szText[256];
			ZeroMemory(szText, sizeof(szText));
			ImmGetCompositionString(hiNow, GCS_RESULTSTR, szText, sizeof(szText));

			ImmReleaseContext(hWnd,hiNow);

			return (CString)szText;
		}
		return _T("");		
	}

	static void win_ImmCancel(HWND hWnd)
	{//29 imm clear		
		HWND subhWnd = ImmGetDefaultIMEWnd(hWnd);
		HIMC hiNow = ImmGetContext(hWnd);

		if(hiNow)
		{
			ImmNotifyIME(hiNow, NI_COMPOSITIONSTR, CPS_CANCEL, 0);
			ImmReleaseContext(hWnd, hiNow);
		}
	}

	static BOOL win_ImmChkHan(HWND hWnd)
	{//30 한글인지 영문인지를 알아본다 

		HWND subhWnd = ImmGetDefaultIMEWnd(hWnd);
		HIMC hiNow=ImmGetContext(subhWnd); 

		BOOL bflag=FALSE;
		if(hiNow)
		{//hanFlag가 1이면 한글 0이면 영문임 
			bflag=ImmGetOpenStatus(hiNow); 
			ImmReleaseContext(hWnd,hiNow); 
		}
		return bflag;
	}

	static void win_ImmSetType(HWND hWnd,int type)
	{//31 1,한글/0,영문전환

		HWND subhWnd = ImmGetDefaultIMEWnd(hWnd);
		HIMC hiNow=ImmGetContext(subhWnd); 
		
		if(hiNow)
		{
			ImmSetConversionStatus(hiNow,type,0);//1:한글,0:영문 
			ImmReleaseContext(hWnd,hiNow); 
		}		
	}
	
	static HWND win_GetParentWnd(HWND hWnd,CString className)
	{//32 부모윈도우에서 클래스명 구하기
		if(hWnd==NULL) return NULL;

		while(hWnd)
		{
		  CString sclassName=win_GetClassName(hWnd);
		  if(sclassName==className) break;
		  hWnd=::GetParent(hWnd);
		  if(hWnd==NULL) break;
		}
		return hWnd;
	}	

	static BOOL win_ServiceStatus(CString strServiceName)
	{//33 SERVICE_RUNNING SERVICE_PAUSED SERVICE_STOPPED
		// Open the SCM and the desired service
		SC_HANDLE hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if(hSCM)
		{		
			SC_HANDLE hService = OpenService(hSCM, strServiceName,
											SERVICE_ALL_ACCESS);
			if(hService)
			{				
				SERVICE_STATUS Status;
				QueryServiceStatus(hService, &Status); 
				return Status.dwCurrentState;
			}
			// Close the service and the SCM
			CloseServiceHandle(hService);
		}
		CloseServiceHandle(hSCM);
		return 0;
	}

	static BOOL win_ServiceStart(CString strServiceName)
	{//34		
		BOOL bRet = FALSE;		
		SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
		if(hScm) 
		{
			SC_HANDLE hService = OpenService(hScm, strServiceName, SERVICE_START | SERVICE_QUERY_STATUS);
			if ( hService ) 
			{
				SetCursor(LoadCursor(NULL, IDC_WAIT));

				if ( StartService(hService, 0, NULL) )  
				{
					bRet = TRUE;
					SERVICE_STATUS status;
					QueryServiceStatus(hService, &status);
					while (status.dwCurrentState != SERVICE_RUNNING)
					{
						Sleep(status.dwWaitHint);
						QueryServiceStatus(hService, &status);
					}
				}
				else
				{
					int nErrCode = GetLastError(); 
				}
				SetCursor(LoadCursor(NULL, IDC_ARROW));         
			}
			CloseServiceHandle(hService);
		}
		CloseServiceHandle(hScm);
		return bRet;
	}

	static void win_ServiceStop(CString strServiceName)
	{//35    
		// Open the SCM and the desired service
		SC_HANDLE hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
		if(hSCM)
		{
			SC_HANDLE hService = OpenService(hSCM, strServiceName,
				                             SERVICE_STOP | SERVICE_QUERY_STATUS);
			if(hService)
			{
				// Tell the service to stop
				SERVICE_STATUS ss;
				ControlService(hService, SERVICE_CONTROL_STOP, &ss);				  				
			}
			// Close the service and the SCM
			CloseServiceHandle(hService);
		}
		CloseServiceHandle(hSCM);
	}		

	static void win_ServiceDelete(CString strServiceName)
	{ //36  		
		SC_HANDLE ScManager=OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);
		if(ScManager)
		{						
			SC_HANDLE ScService=OpenService(ScManager,strServiceName,SERVICE_ALL_ACCESS);
			if(ScService)
			{
			   DeleteService(ScService);			   
			}
			CloseServiceHandle(ScService);						
		}
		CloseServiceHandle(ScManager);				
	}

	static BOOL win_ServiceChk(CString strServiceName)
	{//37 
		BOOL bRet=FALSE;
		SC_HANDLE hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
		if(hSCM)
		{
			SC_HANDLE hService = OpenService(hSCM, strServiceName,
				                             SERVICE_STOP | SERVICE_QUERY_STATUS);
			if(hService)
			{
				CloseServiceHandle(hService);
				bRet=TRUE;
			}
			CloseServiceHandle(hService);
		}
		CloseServiceHandle(hSCM);
		return bRet;
	}
	static void win_ServiceSetQuery(CString strServiceName,DWORD dwStartType)
	{//33  SERVICE_AUTO_START/SERVICE_DEMAND_START/SERVICE_DISABLED
		SC_HANDLE hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if(hSCM)
		{		
			SC_HANDLE hService = OpenService(hSCM, strServiceName,
											SERVICE_ALL_ACCESS);
			if(hService)
			{			
				::ChangeServiceConfig(hService,
					                  SERVICE_NO_CHANGE,
					                  dwStartType,
									  SERVICE_NO_CHANGE,
					                  NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			}
			// Close the service and the SCM
			CloseServiceHandle(hService);
		}
		CloseServiceHandle(hSCM);
	}
			

	static DWORD win_ServiceQuery(CString strServiceName)
	{//33  SERVICE_AUTO_START/SERVICE_DEMAND_START/SERVICE_DISABLED
		// Open the SCM and the desired service
		DWORD dwStartType;
		SC_HANDLE hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if(hSCM)
		{		
			SC_HANDLE hService = OpenService(hSCM, strServiceName,
											SERVICE_ALL_ACCESS);
			if(hService)
			{								
				QUERY_SERVICE_CONFIG ServiceConfig;
				DWORD CbBufSize, pcbBytesNeeded;								
				CbBufSize=sizeof(QUERY_SERVICE_CONFIG);
								
				::QueryServiceConfig(hService,&ServiceConfig,CbBufSize,&pcbBytesNeeded);

                dwStartType=ServiceConfig.dwStartType;				
			}
			// Close the service and the SCM
			CloseServiceHandle(hService);
		}
		CloseServiceHandle(hSCM);
		return dwStartType;
	}

	static void win_StartService(PCTSTR pszInternalName)
	{//38 서비스 시작
		SC_HANDLE ScManager,ScService;//Declaring the Some USeful Variable
								
		if((ScManager=::OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS))==NULL) return;
																
		if((ScService=::OpenService(ScManager,pszInternalName,SERVICE_ALL_ACCESS))==NULL) return;
								
		if(::StartService(ScService,0,NULL)==0) return;

		// Close the service and the SCM
		CloseServiceHandle(ScService);
		CloseServiceHandle(ScManager);
	}
	
	static void win_StopService(PCTSTR pszInternalName)
	{//39 서비스 중지
	   // Open the SCM and the desired service
	   SC_HANDLE hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
	   SC_HANDLE hService = OpenService(hSCM, pszInternalName,
										SERVICE_STOP | SERVICE_QUERY_STATUS);			
	   // Tell the service to stop
	   SERVICE_STATUS ss;   
	   ControlService(hService, SERVICE_CONTROL_STOP, &ss);
	   
	   // Close the service and the SCM
	   CloseServiceHandle(hService);
	   CloseServiceHandle(hSCM);
	}
    
	static HWND win_GetChildClassVisible(HWND phWnd,CString pclassName,int type=0)
	{//40 Get #32770
			
		for(phWnd=::GetWindow(phWnd,GW_CHILD);phWnd;phWnd=::GetNextWindow(phWnd,GW_HWNDNEXT))
		{
			CString className=win_GetClassName(phWnd);					
			if(className==pclassName && ::IsWindowVisible(phWnd)) return phWnd;
						
			if(type==1)
			{
				HWND shWnd=win_GetChildClassVisible(phWnd,pclassName,type);
				if(shWnd!=NULL) return shWnd;
			}
		}
		return NULL;
	}
	
	static HWND win_GetChildClass(HWND phWnd,CString pclassName,int type=0,CString text=_T(""))
	{//41 Get #32770
			
		for(phWnd=::GetWindow(phWnd,GW_CHILD);phWnd;phWnd=::GetNextWindow(phWnd,GW_HWNDNEXT))
		{
			CString className=win_GetClassName(phWnd);
			CString title=win_GetText(phWnd);
			if(className==pclassName)
			{
				if(text.IsEmpty() || text==_T("")) return phWnd;
				if(title==text)                    return phWnd;				
			}
			
			if(type==1)
			{
				HWND shWnd=win_GetChildClass(phWnd,pclassName,type);
				if(shWnd!=NULL) return shWnd;
			}
		}

		return NULL;
	}

	static HWND win_GetChildClassTitleLike(HWND phWnd,CString pclassName,int type=0,CString text=_T(""))
	{//42 Get #32770
			
		for(phWnd=::GetWindow(phWnd,GW_CHILD);phWnd;phWnd=::GetNextWindow(phWnd,GW_HWNDNEXT))
		{
			CString className=win_GetClassName(phWnd);
			CString title=win_GetText(phWnd);			
			if(className==pclassName)
			{								
				if(text.IsEmpty() || text==_T("")) return phWnd;
				if(title.Find(text)>-1)        return phWnd;							
			}
			
			if(type==1){
				HWND shWnd=win_GetChildClass(phWnd,pclassName,type);
				if(shWnd!=NULL) return shWnd;
			}
		}

		return NULL;
	}

	static HWND win_GetChildClassLike(HWND phWnd,CString pclassName,int type=0,CString text=_T(""))
	{//43 Get #32770
			
		for(phWnd=::GetWindow(phWnd,GW_CHILD);phWnd;phWnd=::GetNextWindow(phWnd,GW_HWNDNEXT))
		{
			CString className=win_GetClassName(phWnd);
			CString title=win_GetText(phWnd);
			if(className.Find(pclassName)>-1)
			{
				if(text.IsEmpty() || text==_T("")) return phWnd;
				if(title.Find(text)>-1) return phWnd;								
			}
			
			if(type==1){
				HWND shWnd=win_GetChildClassLike(phWnd,pclassName,type);
				if(shWnd!=NULL) return shWnd;
			}
		}

		return NULL;
	}

	static int win_GetChildClassCount(HWND phWnd,CString pclassName,int type=0)
	{//44 Get #32770
				
		int count=0;
		for(phWnd=::GetWindow(phWnd,GW_CHILD);phWnd;phWnd=::GetNextWindow(phWnd,GW_HWNDNEXT))
		{
			CString className=win_GetClassName(phWnd);
			CString title=win_GetText(phWnd);			
			if(className==pclassName && ::IsWindowVisible(phWnd))
			{
				count=count+1;
			}
			
			if(type==1){
				count=count+win_GetChildClassCount(phWnd,pclassName,type);				
			}
		}

		return count;
	}

	static int win_GetChildClassHeight(HWND phWnd,CString pclassName,int type=0)
	{//45 Get #32770
		int height=0;
		for(phWnd=::GetWindow(phWnd,GW_CHILD);phWnd;phWnd=::GetNextWindow(phWnd,GW_HWNDNEXT))
		{
			CString className=win_GetClassName(phWnd);
			CString title=win_GetText(phWnd);
			if(className==pclassName && ::IsWindowVisible(phWnd)){
				height=height+GWin::win_GetRect(phWnd).Height();
			}
			
			if(type==1){
				HWND shWnd=win_GetChildClass(phWnd,pclassName,type);
				if(shWnd!=NULL)	height=height+GWin::win_GetRect(phWnd).Height();				
			}
		}
		return height;
	}

	static void win_SetEditBackSpace(CEdit *Edit)
	{//46 백스페이스 키 구현
		if(!Edit) return;

		int istart,iend;
		Edit->GetSel(istart,iend);
		
		if(istart==iend) Edit->SetSel(istart-1,iend);
		else             Edit->SetSel(istart,iend);
		
		Edit->ReplaceSel(_T(""));
	}

	static BOOL win_Finder(CString path)
	{//47 file find
	        if(path.IsEmpty() || path==_T("")) return FALSE;

		BOOL bWork;
		CFileFind finder;
		bWork=finder.FindFile(path);		
		finder.Close();		

		return bWork;

	}

	/*static BOOL win_RisterDllCmd(CString szDll, BOOL flag)
	{//48 regsvr32 파일 등록
		
		CString path=_T("");
		path.Format(_T("%s%s%s"),_T("\""),szDll,_T("\""));
		
		CString exePath=win_GetClsidPath(CSIDL_SYSTEM)+_T("\\regsvr32.exe");
		if(win_Finder(exePath)==FALSE) exePath="regsvr32.exe";
		
		BOOL bExec=FALSE;
		if(flag==TRUE)  bExec=win_Exec(exePath,SW_HIDE,_T("/s /c ")+path);
		else            bExec=win_Exec(exePath,SW_HIDE,_T("/s /u ")+path);
		
		if(GReg::reg_RisterDll(szDll,flag)==TRUE) return TRUE;
	   
		return bExec;
	}

	static BOOL win_Exec(CString strPath,WORD style=SW_NORMAL,CString parameter=_T(""))
	{//49 전체 프로그램 실행	
		DWORD result=0; 

		result=win_ShellExec(strPath,style,parameter);
		if(result!=0) return TRUE;
		
		result=win_ExeCreateProcess(strPath,style,parameter);	
		if(result==TRUE) return TRUE;
		
		#ifdef _UNICODE			   
			CStringA strA(strPath+_T(" ")+parameter);			
		    result=::WinExec(strA,style);
		    if(result>31) return TRUE;
		#else
		    result=::WinExec(strPath+_T(" ")+parameter,style);
		    if(result>31) return TRUE;		
		#endif	

		return FALSE;
	}*/

	static DWORD win_ShellExec(CString strPath,DWORD style=SW_NORMAL,CString parameter=_T(""),CString sverb=_T("open"))
	{//50 shell을 이용한 프로그램 실행
		SHELLEXECUTEINFO sei;
		::ZeroMemory( &sei, sizeof(SHELLEXECUTEINFO) );
		sei.cbSize = sizeof(SHELLEXECUTEINFO);
		sei.lpFile = strPath;
		sei.nShow = style;
		sei.fMask = SEE_MASK_NOCLOSEPROCESS;
		sei.lpVerb = sverb;
		sei.lpParameters=parameter;

		return ::ShellExecuteEx(&sei);
	}

	/*static BOOL win_ExeCreateProcess(CString strPath,WORD style=SW_NORMAL,CString parameter=_T(""))
	{//51 폴더를 정확히 지정해야됌
		STARTUPINFO sui;
		PROCESS_INFORMATION pi;
		
		memset(&pi, 0x00, sizeof(PROCESS_INFORMATION));
		memset(&sui, 0x00, sizeof(STARTUPINFO));
		sui.cb = sizeof(STARTUPINFO);
		sui.wShowWindow=style;
		
		if(parameter.GetLength()>0 && parameter[0]!=' ')//매개변수가있으면 공백체크
		{
			parameter=_T(" ")+parameter;
		}
		
		BOOL bflag=FALSE;

		#ifdef _UNICODE	
		    BSTR bstrPath=strPath.AllocSysString();
			BSTR bstrParam=parameter.AllocSysString();
		    bflag=CreateProcessW(bstrPath,bstrParam,
			                    NULL, NULL, TRUE, 0, NULL, NULL, &sui, &pi);
			SysFreeString(bstrPath);//메모리 해제.
			SysFreeString(bstrParam);//메모리 해제.
		#else
		    bflag=CreateProcess((LPSTR)(LPCTSTR)strPath,(LPSTR)(LPCTSTR)parameter,
			                    NULL,NULL,TRUE,0,NULL,NULL,&sui,&pi);	
		#endif	

		CloseHandle(pi.hThread);
		return bflag;
	}

	static BOOL win_ExecWait(CString strPath,WORD style=SW_NORMAL,CString parameter=_T(""))
	{//52 대기 프로그램 실행	
		DWORD result=0;

		result=win_ShellExecWait(strPath,style,parameter);
		if(result!=0) return TRUE;
		
		result=win_ExeWaitCreateProcess(strPath,style,parameter);	
		if(result==TRUE) return TRUE;
		
		CStringA strA(strPath+_T(" ")+parameter);	
		result=::WinExec(strA,style);
		if(result>31) return TRUE;

		return FALSE;
	}

	static DWORD win_ShellExecWait(CString strPath,WORD style=SW_NORMAL,CString parameter=_T(""),CString sverb=_T("open"))
	{//53 대기 프로그램 실행	
		SHELLEXECUTEINFO sei;
		::ZeroMemory( &sei, sizeof(SHELLEXECUTEINFO) );
		sei.cbSize = sizeof(SHELLEXECUTEINFO);
		sei.lpFile = strPath;
		sei.nShow = style;
		sei.fMask = SEE_MASK_FLAG_NO_UI | SEE_MASK_NOCLOSEPROCESS;//no message		
		sei.lpVerb = sverb;
		sei.lpParameters=parameter;

		DWORD result = ::ShellExecuteEx( &sei );
		if(sei.hProcess!= NULL) ::WaitForSingleObject( sei.hProcess, INFINITE );

		return result;
	}
	
	static BOOL win_ExeWaitCreateProcess(CString strPath,WORD style=SW_NORMAL,CString parameter=_T(""))
	{//54 폴더를 정확히 지정해야됌
		STARTUPINFO sui;
		PROCESS_INFORMATION pi;
		
		memset(&sui, 0x00, sizeof(STARTUPINFO));
		sui.cb = sizeof(STARTUPINFO);    
		sui.wShowWindow = style;
		
		if(parameter.GetLength()>0 && parameter[0]!=' ')//매개변수가있으면 공백체크
		{
			parameter=_T(" ")+parameter;
		}


		BOOL Ret=FALSE;
		#ifdef _UNICODE	
		    BSTR bstrPath=strPath.AllocSysString();
			BSTR bstrParam=parameter.AllocSysString();
		    Ret=CreateProcessW(bstrPath,bstrParam,
			                    NULL, NULL, TRUE, 0, NULL, NULL, &sui, &pi);
			SysFreeString(bstrPath);//메모리 해제.
			SysFreeString(bstrParam);//메모리 해제.
		#else
		    Ret = CreateProcess((LPSTR)(LPCTSTR)strPath,(LPSTR)(LPCTSTR)parameter, 
								  NULL,NULL,FALSE,0,NULL,NULL,&sui,&pi);
		#endif	

		if (Ret) 
		{
			HANDLE hProcess = pi.hProcess;
			WaitForSingleObject(hProcess, 0xffffffff);
			CloseHandle(hProcess);
		}
		CloseHandle(pi.hThread);
		return Ret;
	}

	static CRect win_DrawIco(HDC hDC,HICON hIcon,CRect rect) 
	{//55 아이콘 그리기
		if(!hDC || !hIcon) return rect;

		DrawIconEx(hDC, rect.left,rect.top, hIcon, 
			                  rect.Width(), rect.Height(), 
							  0, NULL, DI_NORMAL);//close btn
		return rect;
	}

	static CString win_RGBToStr(int color) 
	{//56		
		CString str=_T("");
		str.Format(_T("%2x%2x%2x"),GetRValue(color),GetGValue(color),GetBValue(color));
		
		return str;
	}
	
	/*static BOOL win_LaunchSessionAll(CString strPath)
	{//57
		if( strPath.IsEmpty() || strPath==_T("")) return FALSE;
		
		OSVERSIONINFO osi;
		ZeroMemory(&osi,sizeof(OSVERSIONINFO));
		osi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		GetVersionEx(&osi); 

		CString strSearch;
		BOOL bResult = FALSE;
		DWORD dwSessionId = 0; // 세션 ID
		DWORD dwWinlogonPID = 0; // Winlogon PID 
		typedef DWORD (*pfnWTSGetActiveConsoleSessionId)(void);
		HMODULE hModule = NULL;
		pfnWTSGetActiveConsoleSessionId fnWTSGetActiveConsoleSessionId = NULL; 
		
		LPVOID pEnv = NULL;
		DWORD dwCreationFlags = NULL;
		HANDLE hProcess = NULL;
		HANDLE hToken = NULL;
		HANDLE hTokenDup = NULL;

		if( osi.dwMajorVersion == 6 )
		{
			//"Vista, 라이브러리 로딩 시도";
			hModule = ::LoadLibrary(_T("Kernel32.dll"));
			if( hModule != NULL )
			{
				//"Vista, 함수 로딩 시도";
				fnWTSGetActiveConsoleSessionId = (pfnWTSGetActiveConsoleSessionId) GetProcAddress(hModule, "WTSGetActiveConsoleSessionId");
				if( fnWTSGetActiveConsoleSessionId == NULL ){
					//"WTSGetActiveConsoleSessionId 함수 로딩 실패"; 
				}
				dwSessionId = fnWTSGetActiveConsoleSessionId();
			}
		} 

		// 같은 세션으로 실행할 프로세스 찾기(Winlogon.exe)
		BOOL IsExistsWinlogon = FALSE;
		PROCESSENTRY32 pe32;
		pe32.dwSize = sizeof(PROCESSENTRY32);

		HANDLE hSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if( hSnap == INVALID_HANDLE_VALUE )
		{
		bResult = FALSE;
		goto CleanUP;
		} 
		if( !Process32First(hSnap, &pe32) )
		{
		bResult = FALSE;
		goto CleanUP;
		} 
		if( osi.dwMajorVersion == 6 )      strSearch = _T("winlogon.exe");
		else if( osi.dwMajorVersion == 5 ) strSearch = _T("explorer.exe"); 
		
		do
		{
		if( lstrcmpi(pe32.szExeFile, strSearch) == 0 )
		{
			//"WinLogon 프로세스 검색";
			DWORD dwWLSessionID = 0;
			BOOL bRet = ProcessIdToSessionId(pe32.th32ProcessID, &dwWLSessionID); 
			if(  bRet && (osi.dwMajorVersion == 6) && (dwSessionId == dwWLSessionID) )
			{// 비스타				
				//"Vista";
				dwWinlogonPID = pe32.th32ProcessID;
				IsExistsWinlogon = TRUE;
				break;
			} else if( bRet && (osi.dwMajorVersion == 5) )
			{// 2000 ~ XP
				//"2000/XP";
				dwSessionId = dwWLSessionID;
				dwWinlogonPID = pe32.th32ProcessID;
				IsExistsWinlogon = TRUE;
				break;
			}
		}
		} while ( Process32Next(hSnap, &pe32) ); 

		if( IsExistsWinlogon == FALSE )
		{
			//"Winlogon.exe 검색 실패";
			bResult = FALSE;
			//goto CleanUP;
		} 

		// WINLOGON 프로세스 열고 토큰 복사하여 특정 세션 지정
		hProcess = ::OpenProcess(MAXIMUM_ALLOWED, FALSE, dwWinlogonPID);
		hToken = NULL;
		hTokenDup = NULL;
		if( hProcess == NULL ) goto CleanUP;

		if( !::OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY|TOKEN_DUPLICATE|TOKEN_ASSIGN_PRIMARY|TOKEN_ADJUST_SESSIONID|TOKEN_READ|TOKEN_WRITE, &hToken))
		{
			//"OpenProcessToken Failed";
			bResult = FALSE;
			goto CleanUP;
		}

		LUID luid;
		if( !LookupPrivilegeValue(NULL,SE_DEBUG_NAME, &luid) )
		{
			//"LookupPrivilegeValue Failed";
			bResult = FALSE;
			goto CleanUP;
		}
		TOKEN_PRIVILEGES tp;
		tp.PrivilegeCount = 1;
		tp.Privileges[0].Luid = luid;
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		if( !DuplicateTokenEx(hToken, MAXIMUM_ALLOWED, NULL, SecurityIdentification, TokenPrimary, &hTokenDup) )
		{
			//"DuplicateTokenEx Failed";
			bResult = FALSE;
			goto CleanUP;
		}

		SetTokenInformation(hTokenDup, TokenSessionId, (LPVOID)&dwSessionId, sizeof(DWORD));
		if( !AdjustTokenPrivileges(hTokenDup, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), (PTOKEN_PRIVILEGES)NULL,NULL) )
		{
			//"AdjustTokenPrivileges Failed";
			bResult = FALSE;
			goto CleanUP;
		}

		if( GetLastError() == ERROR_NOT_ALL_ASSIGNED )
		{
			//"AdjustTokenPrivileges Assign Failed";
			bResult = FALSE;
			goto CleanUP;
		} 

		// CreateProcessAsUser 준비
		STARTUPINFO si;
		ZeroMemory(&si, sizeof(STARTUPINFO));
		si.cb = sizeof(STARTUPINFO);
		si.lpDesktop = _T("winsta0\\default");
		PROCESS_INFORMATION pi;
		ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

		dwCreationFlags = NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE;
		pEnv = NULL;
		if( CreateEnvironmentBlock(&pEnv, hTokenDup, TRUE) ) dwCreationFlags |= CREATE_UNICODE_ENVIRONMENT;
		else pEnv = NULL; 
		
		#ifdef _UNICODE	
			LPWSTR lpszPath = strPath.AllocSysString();
		#else
			LPTSTR lpszPath = (LPSTR)(LPCTSTR)strPath;
		#endif

		if( lpszPath == NULL )
		{
			//"Path String Assign Failed";
			bResult = FALSE;
			goto CleanUP;
		} 

		//lpszPath;
		bResult = ::CreateProcessAsUser(hTokenDup,
										NULL,
										lpszPath,	
										NULL,
										NULL,
										FALSE,
										dwCreationFlags,
										pEnv,
										NULL,
										&si,
										&pi); 
		free(lpszPath); 
		//if( !bResult ) "CreateProcessAsUser Failed";

		::WaitForSingleObject(pi.hProcess, INFINITE );//실행하는동안 대기

		if( (bResult) && (pi.hProcess != INVALID_HANDLE_VALUE) ) CloseHandle(pi.hProcess);
		if( (bResult) && (pi.hThread != INVALID_HANDLE_VALUE) )  CloseHandle(pi.hThread); 

	CleanUP:
		if( hModule )                       FreeLibrary(hModule);
		if( hSnap != INVALID_HANDLE_VALUE ) CloseHandle(hSnap);
		if( hProcess != NULL )              CloseHandle(hProcess);
		if( hToken )                        CloseHandle(hToken);
		if( hTokenDup )                     CloseHandle(hTokenDup); 

		return bResult;
	}
				 	
	static void win_CenterPos(int Sort,HWND hWnd,CRect rcArea)
	{//58
		int h=0;
		CRect rcCenter,rect;
		::GetClientRect(hWnd,rcCenter);
		int xLeft = rcArea.left + (rcArea.Width()-rcCenter.Width())/2;
		int xTop  = rcArea.top  + (rcArea.Height()-rcCenter.Height())/2;
		if(Sort==1) h = GetSystemMetrics(SM_CYMENU)+GetSystemMetrics(SM_CYCAPTION);

   		::SetWindowPos(hWnd,HWND_TOPMOST,xLeft,xTop,rcCenter.Width(),rcCenter.Height()+h,
					   SWP_SHOWWINDOW);
	}
	
	static int win_SpinGetMax(CSpinButtonCtrl *spin)
	{//59
		int imin,imax;	
		spin->GetRange(imin,imax);
   
		return imax;
	}

	static int win_SpinGetMin(CSpinButtonCtrl *spin)
	{//60
		int imin,imax;	
		spin->GetRange(imin,imax);
   
		return imin;
	}

	static CString win_IntToStr(int num,int pt=1)
	{//61 자릿수 
		CString str=_T("");

		if(pt<2)  str.Format(_T("%d"),num);
		if(pt==2) str.Format(_T("%02d"),num);
		if(pt==3) str.Format(_T("%03d"),num);
		
		return str;
	}

	static void win_SpinChkValue(CSpinButtonCtrl *spin,CWnd *Edit)
	{//62
		int imin,imax;	
		spin->GetRange(imin,imax);
   
		CString text=win_GetText(Edit->m_hWnd);
		if(_ttoi(text)>imax)
		{  
			win_SetText(Edit->m_hWnd,win_IntToStr(imax));
		} 
		else if(_ttoi(text)<imin)
		{  
			win_SetText(Edit->m_hWnd,win_IntToStr(imin));
		}
	}

	static CRect win_MonitorArea(int Sort,CPoint point,CStringArray &pData) 
	{//63
		CString data,left,top,right,bottom;
		CRect rect;
		if(Sort==0)	::GetCursorPos(&point);//마우스위치

		for(int i=0;i<pData.GetSize();i++){		  
		   data=pData.GetAt(i);
		   
		   CString name=GStr::str_GetSplit(data,0,',');
		   int left=_ttoi(GStr::str_GetSplit(data,1,','));
		   int top=_ttoi(GStr::str_GetSplit(data,2,','));
		   int right=_ttoi(GStr::str_GetSplit(data,3,','));
		   int bottom=_ttoi(GStr::str_GetSplit(data,4,','));

		   SetRect(rect,left,top,right,bottom);
		   if(::PtInRect(&rect,point)) break;
		   rect.SetRectEmpty();
		}	  
		if(rect.IsRectEmpty()) SetRect(rect,0,0,1024,768);
		return rect;
	}

	static SIZE win_FontSize(int Sort,HWND fhWnd,HDC hDC,CString text)
	{//64 font height
		SIZE size;
		if(Sort==0) hDC=::GetDC(fhWnd); 
		GetTextExtentPoint32(hDC,text, text.GetLength(), &size);
		::ReleaseDC(fhWnd,hDC);

		return size;
	}

	static void win_SetPosition(int Sort,HWND phWnd)
	{//65
		CRect rect;
		int theight=0,mheight=0,cheight=0;
		::GetClientRect(phWnd,&rect);
		
		mheight = GetSystemMetrics(SM_CYMENU);
		cheight = GetSystemMetrics(SM_CYCAPTION);

		if(Sort==0)      theight=rect.Height()+cheight;
		else if(Sort==1) theight=rect.Height()+cheight+mheight;
		else if(Sort==2) theight=rect.Height();
		
		::MoveWindow(phWnd,3,3,rect.Width()+3,theight+3,TRUE);
	}

	static void win_TaskbarStatus(BOOL flag)
	{//66 작업표시줄(윈도우하단툴바)
		HWND shWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);
		if(shWnd==NULL) return;
		
		if(flag==TRUE) ::ShowWindow(shWnd, SW_HIDE);		 		
		else           ::ShowWindow(shWnd, SW_SHOW);
	}

	
	static void win_PptKillProcess()
	{//67 PowerPoint process exit
		HWND hwnd=::FindWindow(_T("PP9FrameClass"),NULL);
		if(hwnd)
		{
			DWORD pid, tid;  //(pid: process id, tid: thread id)
			tid = ::GetWindowThreadProcessId(hwnd, &pid);
			HANDLE hnd = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
			::TerminateProcess(hnd, 0);
			::CloseHandle(hnd);
		}
	}*/

	static CString win_CboGetString(CComboBox *cbo,CString blank=_T(""))
	{//68
		CString value;
		cbo->GetLBText(cbo->GetCurSel(),value);
		
		if(blank!=_T("") && value==blank) value=_T("");
		
		return value;
	}

	/*static int win_CboGetFind(CComboBox *cbo,CString str)
	{//69
	   int focus=cbo->FindString(-1,str);
	   if(focus<0) focus=0;
	   cbo->SetCurSel(focus);
   
	   return focus;
	}

	static BOOL win_IsOsNT()   
	{//70 OS detection routine.
		 OSVERSIONINFO version;
		 // First we have to find out which OS we are running on 
		 version.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		 // If we get and error, we can"t continue.    
		 BOOL bNT = FALSE;    // CString strplatformid;
		 if(GetVersionEx(&version))
		 {
			DWORD dwplatformid = version.dwPlatformId;
			switch (version.dwPlatformId){
			   case VER_PLATFORM_WIN32s: //strplatformid.Format("Windows 3.1");
					bNT = FALSE;
					break;
			   case VER_PLATFORM_WIN32_WINDOWS:
					if(version.dwMinorVersion == 0){//strplatformid.Format("Windows 95");
					   bNT = FALSE;
					}else if(version.dwMinorVersion >0){//strplatformid.Format("Windows 98");
					   bNT = FALSE;
					}
					break;
			   case VER_PLATFORM_WIN32_NT:   // strplatformid.Format("Windows NT");
					bNT = TRUE;
					break;
			   default:
					bNT = FALSE;
					break;
			} 
		 }
		 return bNT;
	}

	static void win_Reboot()
	{//71					
		if(win_IsOsNT()==FALSE) win_Win98ReShut(1); 
		else
		{
			if(win_NTReShutB(1)==FALSE) win_NTReShutA(1);
		}
	}

	static void win_Shutdown()
	{//72
		if(win_IsOsNT()==FALSE) win_Win98ReShut(0); 
		else 
		{
			if(win_NTReShutB(0)==FALSE) win_NTReShutA(0);
		}
	}

	static BOOL win_Win98ReShut(int Sort)
	{//73
		if(Sort==0)      ::ExitWindowsEx(EWX_SHUTDOWN|EWX_FORCE,0);//shutdown
		else if(Sort==1) ::ExitWindowsEx(EWX_REBOOT|EWX_FORCE,0);  //reboot
		
		return TRUE;
	}

	static BOOL win_NTReShutA(int Sort)
	{//74
		HANDLE hToken; 
		LUID luid;
		TOKEN_PRIVILEGES tp; 
		TOKEN_PRIVILEGES tpPrevious;
		DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);

		if(!::OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY,
		   &hToken))  return FALSE;
    
		if(!::LookupPrivilegeValue(NULL,SE_SHUTDOWN_NAME,&luid)) return FALSE;

		tp.PrivilegeCount           = 1;
		tp.Privileges[0].Luid       = luid;
		tp.Privileges[0].Attributes = 0;

		if(!::AdjustTokenPrivileges(
							hToken,                   // handle to token
							FALSE,                    // disabling option
							&tp,                      // new privilege information
							sizeof(TOKEN_PRIVILEGES), // size of PreviousState					
							&tpPrevious,              // original state
							&cbPrevious)){            // required size of PreviousState           
		}

		if(::GetLastError() != ERROR_SUCCESS) return FALSE;

		tpPrevious.PrivilegeCount       = 1;
		tpPrevious.Privileges[0].Luid   = luid;
		tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
		if(!::AdjustTokenPrivileges(hToken, FALSE, &tpPrevious, 
									cbPrevious, NULL, NULL)){}

		if(::GetLastError() != ERROR_SUCCESS) return FALSE;
		::CloseHandle(hToken);
  
		UINT nFlags=0;
		if(Sort==0)      nFlags=EWX_POWEROFF|EWX_FORCE;     //shutdown
		else if(Sort==1) nFlags=EWX_REBOOT|EWX_FORCE;  //reboot

		if(::ExitWindowsEx(nFlags,0)) return FALSE;

		return TRUE;
	}

	static BOOL win_NTReShutB(BOOL bReboot)
	{//75 true(rebooting),false(shutdown)
		HANDLE hToken;              // handle to process token 
		TOKEN_PRIVILEGES tkp;       // pointer to token structure 

		TCHAR l_sErrorCode[256];

		if(!OpenProcessToken(	GetCurrentProcess(), 
					TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,
					&hToken)) return FALSE;
	 
		LookupPrivilegeValue(	NULL,
					SE_SHUTDOWN_NAME, 
					&tkp.Privileges[0].Luid); 
	 
		tkp.PrivilegeCount = 1; 
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
	 
		AdjustTokenPrivileges(	hToken,
						FALSE,
						&tkp,
						0, 
						(PTOKEN_PRIVILEGES) NULL,
						0);

		if (GetLastError() != ERROR_SUCCESS) return FALSE;

		if(!InitiateSystemShutdown(	NULL,
							NULL,	//"System Shutdown , so You must Save datas of Each Application ",
							0,
							TRUE,
							bReboot))//이 함수가 2번째 Argument에 NULL을 입력해도 NT에서만 메세지가 나타나는지 검사할것.
		{						     //또다른 함수로써 ExitWindowsEx가 있음 이 함수의 Flag로써는 종료시 EWX_SHUTDOWN을 이용하면 됨
			#ifdef _UNICODE	
				wsprintf(l_sErrorCode, _T("%d"),GetLastError());
				if(wcscmp(l_sErrorCode,_T("1115"))!=0) return FALSE;	
			#else								
				sprintf(l_sErrorCode, "%d",GetLastError());
				if(strcmp(l_sErrorCode,"1115")!=0) return FALSE;	
			#endif
			
			return TRUE;
		}				
		return FALSE;
	}*/


	static CString win_CurrentTime(CString sort)
	{//76
		SYSTEMTIME now;
		CString str_time=_T("");
		GetLocalTime(&now);

		if(sort==_T("ftp"))
		{
		   str_time.Format(_T("%2d_%2d_%2d_%2d_%2d"),
			                             now.wMonth,now.wDay,
										 now.wHour,now.wMinute,now.wSecond);
		   str_time.Replace(_T(" "),_T("0"));
		} 
		else if(sort==_T("scen"))
		{
		   str_time.Format(_T("%02d:%02d"),now.wHour,now.wMinute);
		} 
		else if(sort==_T("week"))
		{		   
		   CStringArray strDay;
		   strDay.Add(_T("SUN"));
		   strDay.Add(_T("MON"));
		   strDay.Add(_T("TUE"));
		   strDay.Add(_T("WED"));
		   strDay.Add(_T("THU"));
		   strDay.Add(_T("FRI"));
		   strDay.Add(_T("SAT"));
			
		   str_time=strDay.GetAt(now.wDayOfWeek);		   
		   str_time.MakeLower();
		}
		else if(sort==_T("iweek"))
		{
		   str_time.Format(_T("%d"),now.wDayOfWeek+1);//1(sunday)부터 시작
		}
		else if(sort==_T("short"))
		{
		   str_time.Format(_T("%d_%d_%d_%d"),now.wMonth,now.wDay,now.wHour,now.wMinute);
		}
		else if(sort==_T("full"))
		{
		   str_time.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);
		}
		else if(sort==_T("fulllog"))
		{
		   str_time.Format(_T("[%04d-%02d-%02d %02d:%02d:%02d]"),now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);
		}
		else if(sort==_T("filelog"))
		{
		   str_time.Format(_T("%04d-%02d-%02d"),now.wYear,now.wMonth,now.wDay);
		}
		else if(sort==_T("tblog"))
		{
		   str_time.Format(_T("%04d%02d"),now.wYear,now.wMonth);
		   str_time.Delete(0,2);
		}
		else if(sort==_T("iday"))
		{
		   str_time.Format(_T("%02d"),now.wDay);
		} 
		
		return str_time;
	}
/*
	static void win_SendStr(HWND hWnd,DWORD cmd,CString str,int wParam=0)
	{//77 (LPCTSTR)(char *)lparam
	    ::SendMessage(hWnd,cmd,wParam,(LPARAM)(LPCTSTR)str);	
	}
	
	static void win_SendCopyData(CString sClassName,DWORD cmd,CString data,int wParam=0)
	{//78
		HWND hWnd=::FindWindow(sClassName,NULL);
		if(hWnd==NULL) return;	

		char *szMessage=(LPSTR)(LPCTSTR)data;
  		COPYDATASTRUCT cds; 
		cds.dwData = cmd;
		cds.cbData = data.GetLength(); 
		cds.lpData = (LPVOID)szMessage; 	

		::SendMessage(hWnd,WM_COPYDATA,(WPARAM)wParam,(LPARAM)&cds);	
	}

	static void win_SetWinRect(HWND hWnd,CRect rect)
	{//79
		if(!hWnd) return;
		
		::SetWindowPos(hWnd,NULL,rect.left,rect.top,
		                         rect.Width(),rect.Height(),SWP_SHOWWINDOW);
	}


	static int win_GetIconIndex(LPCTSTR lpszPath, BOOL bIsDir, BOOL bSelected=FALSE)
	{//80 시스템 아이콘 인덱스
		static int _nDirIconIdx = -1;
		SHFILEINFO sfi;
		memset(&sfi, 0, sizeof(sfi));

		if (bIsDir)
		{
			SHGetFileInfo(lpszPath, 
				FILE_ATTRIBUTE_DIRECTORY, 
				&sfi, 
				sizeof(sfi), 
				SHGFI_SMALLICON | SHGFI_SYSICONINDEX | 
				SHGFI_USEFILEATTRIBUTES | (bSelected ? SHGFI_OPENICON : 0));
			
			_nDirIconIdx = sfi.iIcon;
			return _nDirIconIdx;
		}
		else
		{
			SHGetFileInfo (lpszPath, 
				FILE_ATTRIBUTE_NORMAL, 
				&sfi, 
				sizeof(sfi), 
				SHGFI_SMALLICON | SHGFI_SYSICONINDEX | 
				SHGFI_USEFILEATTRIBUTES | (bSelected ? SHGFI_OPENICON : 0));
			return sfi.iIcon;
		}
		return -1;
	}

	static HICON win_GetIcon(LPCTSTR lpszPath, BOOL bIsDir, BOOL bSelected=FALSE)
	{//81 시스템 아이콘
		static int _nDirIconIdx = -1;
		SHFILEINFO sfi;
		memset(&sfi, 0, sizeof(sfi));
        
		if (bIsDir)
		{
			SHGetFileInfo(lpszPath, 
				FILE_ATTRIBUTE_DIRECTORY, 
				&sfi, 
				sizeof(sfi), 
				SHGFI_ICON | SHGFI_TYPENAME | 
				SHGFI_DISPLAYNAME | SHGFI_USEFILEATTRIBUTES | 
				(bSelected ? SHGFI_OPENICON : 0));

			return sfi.hIcon;			
		}
		else
		{
			SHGetFileInfo (lpszPath, 
				FILE_ATTRIBUTE_NORMAL, 
				&sfi, 
				sizeof(sfi), 
				SHGFI_ICON | SHGFI_TYPENAME | 
				SHGFI_DISPLAYNAME | SHGFI_USEFILEATTRIBUTES | 
				(bSelected ? SHGFI_OPENICON : 0));
			return sfi.hIcon;
		}
		return NULL;
	}

	static BOOL win_WaitWithMessageLoop(HANDLE hEvent, int nTimeout)
	{//82   
		DWORD dwRet;

		DWORD dwMaxTick = GetTickCount() + nTimeout;

		while (1)
		{
			// wait for event or message, if it's a message, process it and return to waiting state
			dwRet = MsgWaitForMultipleObjects(1, &hEvent, FALSE, dwMaxTick - GetTickCount(), QS_ALLINPUT);
			if (dwRet == WAIT_OBJECT_0)
			{
				TRACE(_T("WaitWithMessageLoop() event triggered.\n"));
				return TRUE;      
			}   
			else
			if (dwRet == WAIT_OBJECT_0 + 1)
			{
				// process window messages
				win_Doevents();
			}  
			else
			{
				// timed out !
				return FALSE;
			}
		}
	}

	static CString win_GetTypeName(CString strPath)
	{//83 파일 형태
		SHFILEINFO sfi;
		memset(&sfi, 0, sizeof(sfi));

		#ifdef _UNICODE			   
			static wchar_t lpBuff[MAX_PATH];
		#else
		    static char lpBuff[MAX_PATH];		
		#endif	
		
		lpBuff[0] = char ('\0');

		SHGetFileInfo(strPath, 0, &sfi, sizeof(sfi), SHGFI_TYPENAME);

		lstrcpy(lpBuff, sfi.szTypeName);
		if (lpBuff[0] == char('\0'))
		{
			int nDotIdx = strPath.ReverseFind('.');
			int nBSIdx = strPath.ReverseFind('\\');
			if (nDotIdx > nBSIdx)
			{
				strPath = strPath.Mid(nDotIdx+1);
				strPath.MakeUpper();
				lstrcpy (lpBuff, strPath + ' ');
			}

			lstrcat (lpBuff, _T("File"));
		}		
		return lpBuff;
	}

	static CString win_GetIpAddr(CIPAddressCtrl *IpAddr)
	{//84
		BYTE ip[4] = {0};
		CString sIP;

		IpAddr->GetAddress( ip[0],ip[1],ip[2],ip[3] );
		sIP.Format(_T("%d.%d.%d.%d"), ip[0], ip[1], ip[2], ip[3] );
		
		return sIP;
	}

	static int win_IsHung(HWND hWnd)
	{//85		
		OSVERSIONINFO osver;
		osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		if(!GetVersionEx(&osver)) return 0;
		
		HMODULE hUser32 = GetModuleHandle(_T("user32"));
		if(!hUser32) return 0;
	
		BOOL response=FALSE;
		if(osver.dwPlatformId&VER_PLATFORM_WIN32_NT)
		{//winnt			
			PROCISHUNGAPPWINDOW IsHungAppWindow=(PROCISHUNGAPPWINDOW)GetProcAddress(hUser32,"IsHungAppWindow");		
		    if(!IsHungAppWindow)
			{
				if(hUser32) FreeLibrary(hUser32);
				return 0;
			}
			response=IsHungAppWindow(hWnd);
			if(IsHungAppWindow) CloseHandle(IsHungAppWindow);
		}
		else
		{//win98		
			PROCISHUNGTHREAD IsHungThread=(PROCISHUNGTHREAD)GetProcAddress(hUser32,"IsHungThread");		
			if(!IsHungThread)
			{
				if(hUser32) FreeLibrary(hUser32);
				return 0;
			}

			response=IsHungThread(GetWindowThreadProcessId(hWnd,NULL));
			if(IsHungThread) CloseHandle(IsHungThread);
		}
		if(hUser32) FreeLibrary(hUser32);	

		if(response) return 2;//true(not response)
		return 1;             //false(response)		
	}

    static CStringArray* win_GetVideo()
	{//86 모니터 정보 
		CStringArray *sData=new CStringArray[1];

		DEVMODE		   devMode;
		DISPLAY_DEVICE displayDevice;
		displayDevice.cb = sizeof(displayDevice);		
		devMode.dmSize   = sizeof(DEVMODE);

		CString strInfo=_T("");				
		for(int i=0;true;i++)
		{
			if(!EnumDisplayDevices(NULL, i, &displayDevice, 0)) break;			
						
			BOOL bflag=EnumDisplaySettings(displayDevice.DeviceName, ENUM_CURRENT_SETTINGS, &devMode );
			
			if(bflag==TRUE)
			{
				int iflag=0;
				DWORD dwFlag = displayDevice.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE;
				if ( dwFlag == DISPLAY_DEVICE_PRIMARY_DEVICE ) iflag=1;//primary
					
				strInfo.Format(_T("%d,%d,%s,%d,%d,%d,%d,%s"),i,//number
					                                     iflag,//primary,secondary
													     devMode.dmDeviceName, 
													     devMode.dmPelsWidth,//width
													     devMode.dmPelsHeight,//height
													     devMode.dmBitsPerPel,//color
													     devMode.dmDisplayFrequency,//frequency 
													     displayDevice.DeviceString);//graphic card name				
				sData->Add(strInfo);
			}
		}
		return sData;
	}

	static int win_GetVideoPrimary()
	{//87 주모니터
		DEVMODE		   devMode;
		DISPLAY_DEVICE displayDevice;
		displayDevice.cb = sizeof(displayDevice);		
		devMode.dmSize   = sizeof(DEVMODE);
		
		for(int i=0;true;i++)
		{
			if(!EnumDisplayDevices(NULL, i, &displayDevice, 0)) break;	

			BOOL bflag=EnumDisplaySettings(displayDevice.DeviceName, ENUM_CURRENT_SETTINGS, &devMode );
			
			if(bflag==TRUE)
			{				
				DWORD dwFlag = displayDevice.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE;
				if ( dwFlag == DISPLAY_DEVICE_PRIMARY_DEVICE) return i;								
			}
		}
		return -1;
	}

	static void win_SetResolution(int nItem,int width,int height)
	{//88 해상도 변경
		if(nItem<0) return;

		DEVMODE devMode;
		DISPLAY_DEVICE DisplayDevice ;
		DisplayDevice.cb = sizeof ( DisplayDevice ) ;
		
		if(!::EnumDisplayDevices ( NULL, nItem, &DisplayDevice, 0 )) return;
		if (!::EnumDisplaySettings ( DisplayDevice.DeviceName, ENUM_CURRENT_SETTINGS, &devMode )) return;
		
		devMode.dmPelsHeight = width;
		devMode.dmPelsWidth = height;

		ChangeDisplaySettings ( &devMode, CDS_SET_PRIMARY );
	}

	static void win_SetListBoxHori(CListBox *pListBox,CString str)
	{//89		
		CDC* pDC  = pListBox->GetDC();
		if(!pDC) return;

		CSize sz = pDC->GetTextExtent(str);
		pListBox->ReleaseDC(pDC);

		if (pListBox->GetHorizontalExtent() < sz.cx)
		{
			 pListBox->SetHorizontalExtent(sz.cx);
		}
	}*/

	static void win_DelList(CStringArray *list) 
	{//90		
		if(!list) return;

		list->RemoveAll();		
		//delete list;//사용하지않음(에러발생)
		list=NULL;
	}

	/*
	static void win_DelArray(CPtrArray *list) 
	{//91
		if(!list) return;

		list->RemoveAll();
		delete list;
		list=NULL;
	}

	static CString win_GetDiskSpace(CString diskName)
	{//92 getsysteminfo() 
		if(diskName.IsEmpty() || diskName==_T("")) return _T("");

		CString data;
		long nFree=0;
		long nTotal=0;
		DWORD dwSectPerClust=0;
		DWORD dwBytesPerSect=0;
		DWORD dwFreeClusters=0;
		DWORD dwTotalClusters=0;

		if((GetDriveType(diskName)) == DRIVE_CDROM ) return _T("");
		
		GetDiskFreeSpace(diskName, &dwSectPerClust, &dwBytesPerSect, &dwFreeClusters, &dwTotalClusters);
		nFree = (long)((double)dwSectPerClust * (double)dwBytesPerSect * (double)dwFreeClusters / (double)1024 / (double)1024);
		nTotal = (long)((double)dwSectPerClust * (double)dwBytesPerSect * (double)dwTotalClusters / (double)1024 / (double)1024);
		
		data.Format(_T("%s/%d/%d/"),diskName,nFree,nTotal);
		return data;
	}

	static CString win_GetHddid(CString diskName)
	{//93 hdd serial id() 
		if(diskName.IsEmpty() || diskName==_T("")) return _T("");

		DWORD volumeSerial=0;
		DWORD maxFileNameLength=0;

		#ifdef _UNICODE	
			wchar_t nameBuffer[101] = _T("");
			wchar_t fileSystem[101] = _T("");
		#else
			char nameBuffer[101] = _T("");
			char fileSystem[101] = _T("");
		#endif

		DWORD result = GetVolumeInformation(
						   diskName,           // 경로 "E:\\"
						   (LPTSTR)nameBuffer, // volume label 버퍼
						   100,                // volume label을 저장할 버퍼의 크기
						   &volumeSerial,      // volume의 일련번호
						   &maxFileNameLength, // OS에서 지원하는 파일 이름의 최대 크기
						   NULL,
						   fileSystem,         // 드라이브의 파일 시스템. NTFS, FAT32 등
						   100                 // 파일 시스템 이름을 저장할 버퍼의 크기
						   );
		if(result == 0) return _T("");				
		return GStr::str_IntToStr(volumeSerial);
	}


	static HRESULT win_CreateShortcut(LPCTSTR lpszPath, LPCTSTR lpszDesc, LPCTSTR lpszShortcutPath, LPCTSTR lpszIconPath)
	{//94 실행아이콘생성 win_CreateShortcut("c:\\WINDOWS\\notepad.exe", "notepad", desktopPath+"\\notepad.lnk", "");        
		WCHAR wszShortcutPath[MAX_PATH];    		
		CComPtr<IShellLink> ipShellLink;

		HRESULT hRet = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, 
			                            IID_IShellLink, (void**)&ipShellLink);        
		if(FAILED(hRet)) return hRet;
		CComQIPtr<IPersistFile> ipPersistFile(ipShellLink);
		if(FAILED((hRet=ipShellLink->SetPath(lpszPath)))) return hRet;    
		if(FAILED((hRet=ipShellLink->SetIconLocation(lpszIconPath, 0)))) return hRet;            
		if(FAILED((hRet=ipShellLink->SetDescription(lpszDesc)))) return hRet;    

		#ifdef _UNICODE	
			MultiByteToWideChar(CP_ACP, 0, (LPCSTR)lpszShortcutPath, -1, wszShortcutPath, MAX_PATH);    
		#else
			MultiByteToWideChar(CP_ACP, 0, lpszShortcutPath, -1, wszShortcutPath, MAX_PATH);    
		#endif

		return ipPersistFile->Save(wszShortcutPath, TRUE);
	}


	static HRESULT win_CreateUrlShortcut(LPCTSTR lpszUrl, LPCTSTR lpszShortcutPath, 
		                                 LPCTSTR lpszDesc, LPCTSTR lpszIconPath, int nIconIdx)
	{//95 url아이콘 생성 reg_CreateUrlShortcut("http://www.daum.net", desktopPath+"\\daum.url", "daum","", 0);    
		    
		LPWSTR wszShortcutPath = new WCHAR[MAX_PATH];

		HRESULT h = NULL;    
		IPersistFile *ppf = NULL;    
		IShellLink *psl = NULL;    
		IUniformResourceLocator *pInet = NULL;    
		h = CoCreateInstance(CLSID_InternetShortcut, NULL, CLSCTX_INPROC_SERVER, 
			                 IID_IUniformResourceLocator, (void **)&pInet);        
		if ( FAILED(h) ) return h;    
		
		if ( (h = pInet->QueryInterface(IID_IPersistFile, (void **)&ppf)) != S_OK )    
		{       
			pInet->Release();        
			return h;    
		}       
		
		if ( (h = pInet->QueryInterface(IID_IShellLink, (void **)&psl)) != S_OK )   
		{        
			pInet->Release();       
		  ppf->Release();       
		  return h;    
		}    
		  
		if ( (h = pInet->SetURL(lpszUrl, 0)) != S_OK )   
		{       
		  pInet->Release();     
		  ppf->Release();     
		  psl->Release();       
		  return h;  
		} 
		    
		if ( (h = psl->SetIconLocation(lpszIconPath, nIconIdx)) != S_OK )  
		{       
			pInet->Release();  
		  ppf->Release();   
		  psl->Release();  
		  return h;  
		 }   
		 
		if ( (h = psl->SetDescription(lpszDesc)) != S_OK ) 
		{     
			pInet->Release();     	
			ppf->Release();     
		  psl->Release();       
		  return h;    
		}  

		#ifdef _UNICODE	
			MultiByteToWideChar(CP_ACP, 0, (LPCSTR)lpszShortcutPath, -1, wszShortcutPath, MAX_PATH);   
		#else
			MultiByteToWideChar(CP_ACP, 0, lpszShortcutPath, -1, wszShortcutPath, MAX_PATH);   
		#endif      

		if ( (h = ppf->Save(wszShortcutPath, TRUE)) != S_OK )    
		{       
			pInet->Release();        
			ppf->Release();        
			psl->Release();        
			return h;        
		}    
		ppf->Release();    
		psl->Release();    
		pInet->Release();        
		return h;
	} 

	static void win_MonitorStatus(int iflag)
	{//96 모니터 슬립모드 1(standbye), 2(power off), -1(power on)
		::SendMessage(HWND_BROADCAST, WM_SYSCOMMAND, SC_MONITORPOWER, iflag);
		//SendMessage(HWND_BROADCAST, WM_SYSCOMMAND,  SC_SCREENSAVE, 1);   
	}

	static BOOL win_MboxClose(CString className,CString title)
	{//97
		HWND hWnd=::FindWindow(className,title);
		if(hWnd)
		{
			::PostMessage(hWnd,WM_CLOSE,0,0);				
			return TRUE;
		}
		return FALSE;
	}
	
	static CString win_GetComputerName()
	{//98
		DWORD nSize =  MAX_COMPUTERNAME_LENGTH + 1;

		#ifdef _UNICODE	
			wchar_t szBuffer[MAX_COMPUTERNAME_LENGTH + 1];
		#else
			char szBuffer[MAX_COMPUTERNAME_LENGTH + 1];
		#endif

		::GetComputerName(szBuffer,&nSize);

		return (CString)szBuffer;
	}

	static CString win_GetUserName()
	{//99
		DWORD nSize =  MAX_COMPUTERNAME_LENGTH + 1;
		
		#ifdef _UNICODE	
			wchar_t szBuffer[MAX_COMPUTERNAME_LENGTH + 1];
		#else
			char szBuffer[MAX_COMPUTERNAME_LENGTH + 1];
		#endif

		::GetUserName(szBuffer,&nSize);

		return (CString)szBuffer;
	}

	static void win_LButton(HWND hWnd,int x, int y)
	{//100		
		if(!hWnd) return;

		//x = x * 65535 / 1280;  
        //y =  y * 65535 / 1024; 

		mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN, x, y, 0, 0);  
		mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP, x, y, 0, 0);  
	}

	static void win_Setforeground(HWND hWnd)
	{//101	
		SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, (LPVOID)0, 
			                 SPIF_SENDWININICHANGE | SPIF_UPDATEINIFILE);

		::SetForegroundWindow(hWnd);
	}

	static void win_WavePlay(CString szSound,UINT fssound=SND_RESOURCE|SND_ASYNC)
	{//102 SND_RESOURCE(리소스),SND_ASYNC(비동기)
		sndPlaySound(szSound,fssound);
	}

	static void win_WaveStop()
	{//103
		sndPlaySound( NULL, SND_SYNC );  
	}

	static BOOL win_ServiceInteractive(CString strServiceName)
	{//104 서비스와 데스크탑 상호작용허용
		BOOL bRet = FALSE;		
		SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
		if(hScm) 
		{
			SC_HANDLE hService = OpenService(hScm, strServiceName, SERVICE_ALL_ACCESS);
			if ( hService ) 
			{				
				SetCursor(LoadCursor(NULL, IDC_WAIT));

				if(ChangeServiceConfig(hService, 
					                SERVICE_INTERACTIVE_PROCESS|SERVICE_WIN32_OWN_PROCESS, 
					                SERVICE_AUTO_START, 
									SERVICE_NO_CHANGE, NULL, NULL, NULL, NULL, NULL, NULL, NULL))
				{
					bRet = TRUE;					
				}
				else
				{
					int nErrCode = GetLastError(); 
				}
				SetCursor(LoadCursor(NULL, IDC_ARROW));         
			}
			CloseServiceHandle(hService);
		}
		CloseServiceHandle(hScm);
		return bRet;
	}

	static int win_GetProcessorCount()
	{//105
		DWORD dwSize=0;
		DWORD nProcessCount=0;
		DWORD dwProcessIDs[1024];		 
		if (EnumProcesses(dwProcessIDs, sizeof(dwProcessIDs), &dwSize) == TRUE)
		{        
			nProcessCount = dwSize / sizeof(DWORD);
		}
		return nProcessCount;
	}

	
	static CString win_GetModuleBaseName(HANDLE hProcess)
	{//106 process name
		if(!hProcess) return _T("");

		CString strName=_T("");		
		GetModuleBaseName(hProcess, 0, strName.GetBuffer(MAX_PATH), MAX_PATH);

		return strName;
	}

	static CString win_GetModuleFileName(HANDLE hProcess)
	{//107 process full name
		if(!hProcess) return _T("");

		CString strPath=_T("");
		HMODULE hMod=NULL;
		DWORD cbNeeded;

		if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
		{		
		   GetModuleFileNameEx(hProcess, hMod, strPath.GetBuffer(MAX_PATH), MAX_PATH);
		}		
		return strPath;
	}

	static int win_GetMemorySize(HANDLE hProcess,BOOL bflag=FALSE)
	{//108 get memory
	   if( hProcess == NULL ) return 0;
	 
	   int nRet=0;
	   PROCESS_MEMORY_COUNTERS pmc;
	   if( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
	   {		  
          nRet=pmc.WorkingSetSize/(1024);
	   }

	   if(bflag==TRUE) HDCLOSE(hProcess);
	   return nRet;
	}

	static int win_GetGdiObjectCount(HANDLE hProcess,BOOL bflag=FALSE)
	{//109 gdi object
	   if( hProcess == NULL ) return 0;
	 
	   DWORD nRet=::GetGuiResources(hProcess,GR_GDIOBJECTS);			
	   	   
	   if(bflag==TRUE) HDCLOSE(hProcess);
	   return nRet;
	}

	static int win_GetUserObjectCount(HANDLE hProcess,BOOL bflag=FALSE)
	{//110 user object
	   if( hProcess == NULL ) return 0;
	 
	   DWORD nRet=::GetGuiResources(hProcess,GR_USEROBJECTS);			
	   	   
	   if(bflag==TRUE) HDCLOSE(hProcess);
	   return nRet;
	}

	static int win_ChkPlayerMemoryLimit(HANDLE hProcess,BOOL bflag=FALSE)
	{//111 memory check	   
	   if( hProcess == NULL ) return 0;
	 
	   int nRet=0;
	   PROCESS_MEMORY_COUNTERS pmc;
	   if( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
	   {		  
          nRet=pmc.WorkingSetSize/(1024)+pmc.PagefileUsage/(1024);
		  if(nRet>=300000)//200000 195M
		  {// 메모리 과부하 종료조건
             nRet=2;
		  }		 	
		  else
		  {
		     nRet=1;
		  }
	   }
	   if(bflag==TRUE) HDCLOSE(hProcess);
	   return nRet;
	}
	
	static int win_ChkPlayerCpuTime()
	{//112 cpu time check
		DWORD processID=GetCurrentProcessId();

		HANDLE hProcess;
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processID ); 
		if (NULL == hProcess) return 0; 

		FILETIME ftCreation;
		FILETIME ftExit;
		FILETIME ftKernel;
		FILETIME ftUser;

		int nRet=0;
		if(GetProcessTimes( hProcess , &ftCreation, &ftExit,&ftKernel, &ftUser))
		{		
			COleDateTime timeNow = COleDateTime::GetCurrentTime();
			COleDateTime timeCreation = ftCreation;
			COleDateTimeSpan timeDiff = timeNow - timeCreation;
						
			SYSTEMTIME stKernel,stUser;
			FileTimeToSystemTime(&ftKernel, &stKernel);
			FileTimeToSystemTime(&ftUser, &stUser);
			
			DWORD nKernel=stKernel.wSecond+stKernel.wMinute*60+stKernel.wHour*60*60;
			DWORD nUser=stUser.wSecond+stUser.wMinute*60+stUser.wHour*60*60;
					
			if((nKernel+nUser)>24*60*60)            nRet=2;//CPU time 하루이상이면
			if(timeDiff.GetTotalSeconds()>24*60*60) nRet=2;//프로세스가 하루이상 실행상태이면

			nRet=1;
		}
		if(hProcess) CloseHandle(hProcess);
		return nRet;				
	}

	static BOOL win_isHungTimeout(HWND hWnd,int TimeOut=1000)
	{//113 window hung
		if(!hWnd) return 0;
		 
		DWORD dwResult =0;
		HRESULT hr = SendMessageTimeout(hWnd, WM_NULL, 0, 0, 
			                            SMTO_ABORTIFHUNG | SMTO_NORMAL,
										TimeOut, &dwResult );
		//SMTO_ABORTIFHUNG(메세지를 받는 쓰레드가 멈춰있는거 같으면 기다리지 않고 바로 리턴)
		//SMTO_BLOCK(다른 프로세스가 쓰지 않도록 블럭하며 기다림)
		//SMTO_NORMAL(기다리는동안 다른 프로세스의 요청으로 부터 막히지 않음)
		//SMTO_NOTIMEOUTIFNOTHUNG(받는 쓰레드가 살아있을경우 타임아웃시간 넘어서도 기다림)
		//SMTO_ERRORONEXIT
		if( !hr )
		{			
			return 2;//not response
		}

		//dwResult:message handler
		return 1; //response ok
	}
	

	static void win_PeekMsg(HWND hWnd=NULL,DWORD TimeOut=5000)
	{//114 윈도우 메세지 처리
		DWORD nStart=::GetTickCount();

		MSG Message= { 0 };		
		while (PeekMessage(&Message, hWnd, 0, 0, PM_REMOVE) == TRUE)
		{
			if((::GetTickCount()-nStart)>TimeOut) break;

			TranslateMessage(&Message);
			DispatchMessage(&Message);
		}	 
	}

	static void win_SetCheckMenu(CMenu *menu,int pos,BOOL bFlag)
	{//menu state
		if(!menu) return;
		if(menu->GetMenuItemCount()<1) return;

		int nID=menu->GetMenuItemID(pos);

		int nState=MF_BYCOMMAND|MF_UNCHECKED;
		if(bFlag==TRUE)  nState=MF_BYCOMMAND|MF_CHECKED;
		
		menu->CheckMenuItem(nID,nState);		
	}

	
	/*static BOOL win_SetQueueTimer(CWnd *pWnd,HANDLE &uhandle,int nFirst,int nTime,WAITORTIMERCALLBACK lpTimerProc)
	{
		 return CreateTimerQueueTimer(&uhandle,NULL,lpTimerProc,
									  pWnd,nFirst,nTime,
									  WT_EXECUTEINTIMERTHREAD);
	}*/

	/*static void win_KillQueueTimer(HANDLE &uhandle)
	{
		DeleteTimerQueueTimer(NULL, uhandle, NULL);  
		HDCLOSE(uhandle);
	}*/
	
};



 

