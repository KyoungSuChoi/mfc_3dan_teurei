
//#include <afxtempl.h>

class GList
{
public:   
	GList();
    ~GList();
    
	/*static void list_Font(CListCtrl *pList)     
	{//한글 깨질경우 폰트설정함.
		LOGFONT logfont=GGdc::gdc_LogFont(_T("14/0/0/0/400/0/0/0/1/0/0/0/2/Arial"));
		CFont *font=GGdc::gdc_CFont(logfont);
		pList->SetFont(font);
	}*/

    static void list_Init(int sort,CListCtrl *pList,int nItem,CString *list, int *width)     
	{
		if(!pList) return;
		if(!list)  return;
		if(!width) return;

		LV_COLUMN lvcolumn; // 머리말 정보 LVS_SHOWSELALWAYS
		if(sort==0)
		{
		  	pList->SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0,
						   LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|
						   LVS_EX_ONECLICKACTIVATE|LVS_EDITLABELS);		  
		} 
		else if(sort==1)
		{
			pList->SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0,
						   LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|
						   LVS_EX_ONECLICKACTIVATE|LVS_EX_CHECKBOXES);
		}
		
		for(int i = 0; i < nItem; i++)
		{
			lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;
			lvcolumn.fmt = LVCFMT_LEFT; // 정렬 정보
			
			// 제목 항
			#ifdef _UNICODE
				lvcolumn.pszText = list[i].GetBuffer();	
				list[i].ReleaseBuffer();
			#else
			   lvcolumn.pszText = (LPSTR)(LPCTSTR)list[i];
			#endif	

			lvcolumn.iSubItem = i;  // column 번호
			lvcolumn.cx = width[i]; // column의 폭
			pList->InsertColumn(i, &lvcolumn);
		}			
	}

	static int list_Compare(CListCtrl *pList,int subItem,CString data)
	{
		CString getdata;
		int nItem=-1;
		for(int i=0;i<pList->GetItemCount();i++)
		{
		  getdata=pList->GetItemText(i,subItem);
		  if(data==getdata){
			nItem=i;
			break;
		  }
		}
		return nItem;
	}

	static int list_GetMCount(CListCtrl *pList)
	{
		CArray<int,int> arr;
		list_GetMFocus(pList,arr);
		return arr.GetSize();
	}

	static void list_GetMFocus(CListCtrl *pList,CArray<int,int> &arr)
	{//select item
		arr.RemoveAll();
		POSITION pos = pList->GetFirstSelectedItemPosition();	

		while (pos)
		{
		   arr.Add(pList->GetNextSelectedItem(pos));	  
		}
	}

	static int list_GetFocus(CListCtrl *pList)
	{  
		int nItem=-1;
		POSITION pos = pList->GetFirstSelectedItemPosition();	

		if (pos != NULL){     
		   nItem = pList->GetNextSelectedItem(pos);	 
		} 

		return nItem;
	}

	static BOOL list_Renum(CListCtrl *pList,int nItem)
	{
		CString num;
		int tItem=pList->GetItemCount();
		if(tItem<1) return TRUE;

		for(int i=0;i<tItem;i++)
		{
		  num.Format(_T("%d"),i+1);
		  pList->SetItemText(i,nItem,num);
		}
		return TRUE;
	}

	static void list_SetColumn(CListCtrl *pList,int nCol,int nStyle)
	{//LVCFMT_RIGHT				
		LVCOLUMN lc;
		lc.mask = LVCF_FMT;
		lc.fmt = nStyle;
		pList->SetColumn(nCol, &lc);
	}

	static void list_SetFocus(CListCtrl *pList,int nitem,BOOL bfocus=TRUE)
	{	
		if(!pList) return;
		if(pList->GetItemCount()<1) return;

		pList->SetSelectionMark(nitem);
		pList->SetItemState(nitem, LVIS_SELECTED | LVIS_FOCUSED,
								   LVIS_SELECTED | LVIS_FOCUSED);	    
        
		if(bfocus==TRUE) pList->SetFocus();

		pList->EnsureVisible(nitem,TRUE); 
	}

	
	static void list_SetUnFocus(CListCtrl *pList,int nitem)
	{	
		if(!pList) return;

		pList->SetItemState(nitem, NULL,NULL);   
		
		pList->EnsureVisible(nitem,TRUE); 
	}

	static void list_SetAllUnFocus(CListCtrl *pList)
	{		
		if(!pList) return;

		POSITION pos = pList->GetFirstSelectedItemPosition();	

		while (pos)
		{
		   int nItem=pList->GetNextSelectedItem(pos);	
		   list_SetUnFocus(pList,nItem);
		}
	}

	static void list_SetAllUnSelect(CListCtrl *pList)
	{
		if(!pList) return;

		pList->SetItemState(-1, 0, LVIS_SELECTED); //deselect all rows
	}

	static BOOL list_MoveRow(CListCtrl *pList,int from, int to)
	{
		if(from == to || from < 0 || to < 0)  return FALSE;

		if(list_CopyRow(pList,from, to)){		
			if(from > to) pList->DeleteItem(from + 1);
			else  pList->DeleteItem(from);
			list_SetFocus(pList,to);
			return TRUE;
		} else	return FALSE;
	}

	static BOOL list_CopyRow(CListCtrl *pList,int from, int to)
	{		
		if(from == to || from < 0 || to < 0)  return FALSE;
		pList->InsertItem(to, pList->GetItemText(from, 0));
		if(from > to)  from++;
		for(int i = 1; i < 3; i++) 
		{
			pList->SetItemText(to, i, pList->GetItemText(from, i));
		}
		return TRUE;
	}

	static void list_Insert(int Sort,CListCtrl *pList,int nItem,CString *str_Data,BOOL bfocus=TRUE)	
	{		
		int mCount = pList->GetItemCount();
		if(Sort==3 || str_Data[0]==_T(""))
		{
			str_Data[0].Format(_T("%d"),mCount);//auto increase
		}

		LV_ITEM lvitem;
		lvitem.mask= LVCF_TEXT;
		lvitem.iItem=mCount;
		lvitem.iSubItem = 0;
		
		#ifdef _UNICODE			
		    BSTR bstrString = str_Data[0].AllocSysString(); 
		    lvitem.pszText = bstrString;
			SysFreeString(bstrString);
		#else				    
			lvitem.pszText = (LPSTR)(LPCSTR)str_Data[0];			
		#endif											
		
		//first column InsertItem
		//pList->InsertItem(&lvitem);
		pList->InsertItem(mCount,str_Data[0]);
		
		for(int i=1;i<nItem;i++) pList->SetItemText(mCount,i,str_Data[i]);
		
		if(Sort==1 && str_Data[nItem-1]==_T("1")) pList->SetCheck(mCount);	
		
		if(bfocus==TRUE) list_SetFocus(pList,mCount);	
		delete[] str_Data;
	}

	
	static void list_Delete(CListCtrl *pList,int nItem)	
	{
		CArray<int,int> array;
		list_GetMFocus(pList,array);
	
		int count = array.GetSize();
		while(count--)
		{
		  int n = array.GetAt(count); 
		  pList->DeleteItem(n);
		}
        if(array.GetSize()>0) list_Renum(pList,nItem);
	}

	static BOOL list_SameText(CListCtrl *pList,CString szText,int SubItem=0,int nItem=-1)	
	{
		int nCount = pList->GetItemCount();
		if(nCount<1) return FALSE;

		for(int i=0; i<nCount; i++)
		{
			CString str=pList->GetItemText(i, SubItem);
			
			if(nItem>-1 && nItem==i) continue;
			if (str==szText) return TRUE;			
		}
		return FALSE;
	}

	static int list_GetSameText(CListCtrl *pList,CString szText,int SubItem=0)	
	{
		int nCount = pList->GetItemCount();
		if(nCount<1) return -1;

		int i;
		for(i=0; i<nCount; i++)
		{
			CString str=pList->GetItemText(i, SubItem);

			if (str==szText) return i;
		}
		return -1;
	}

	static int list_GetSameText2(CListCtrl *pList,CString szText1,CString szText2,int SubItem1=0,int SubItem2=1)	
	{
		int nCount = pList->GetItemCount();
		if(nCount<1) return -1;

		int i;
		for(i=0; i<nCount; i++)
		{
			CString str1=pList->GetItemText(i, SubItem1);
			CString str2=pList->GetItemText(i, SubItem2);

			if (str1==szText1 && str2==szText2) return i;
		}
		return -1;
	}

	static BOOL list_CheckBox(CListCtrl *pList)
	{//checkbox가 true인것이 있는지
		if(pList->GetItemCount()<1) return FALSE;

		BOOL bFlag=FALSE;		
		for(int i=0;i<pList->GetItemCount();i++)
		{
			if(pList->GetCheck(i)==TRUE)
			{
				bFlag=TRUE;
				break;
			}
		}
		return bFlag;
	}	

	static int list_SelCheckBox(CListCtrl *pList)
	{//맨처음 checkbox true인거		
		if(pList->GetItemCount()<1) return -1;

		for(int i=0;i<pList->GetItemCount();i++)
		{
			if(pList->GetCheck(i)==TRUE) return i;
		}
		return -1;
	}

	static int list_SelCheckBoxCount(CListCtrl *pList)
	{//
		if(pList->GetItemCount()<1) return 0;

		int ncount=0;
		for(int i=0;i<pList->GetItemCount();i++)
		{
			if(pList->GetCheck(i)==TRUE) ncount=ncount+1;
		}
		return ncount;
	}

	static int list_GetItemImage(CListCtrl *pList,int nItem, int nSubItem)
	{
		LVITEM lvi;
		lvi.iItem = nItem;
		lvi.iSubItem = nSubItem;
		lvi.mask = LVIF_IMAGE;
		return pList->GetItem(&lvi) ? lvi.iImage : -1;
	}

	static void list_DelFileInfo(CListCtrl *pList)
	{
		int nCount=pList->GetItemCount();
		if(nCount<1) return;

		for(int i=0;i<nCount;i++)
		{
			FILEINFO *FileInfo=(FILEINFO*)pList->GetItemData(i);
			if(!FileInfo) continue;
						
			delete FileInfo;
			FileInfo=NULL;
		}				
	}

    static void list_DelImageList(CImageList *pImgList)
	{
		int nImageCount = pImgList->GetImageCount();
		if(nImageCount<1) return;

		for( int i = 0 ; i < nImageCount ; i++ )
		{
			pImgList->Remove( i );
		}	
	}

	static int list_GetColumnSame(CListCtrl *pList,CString strHeader)
	{
		if(strHeader.IsEmpty() || strHeader=="") return -1;
		int nHeadNum = pList->GetHeaderCtrl()->GetItemCount();
		if(nHeadNum<1) return -1;

	    HDITEM hdItem;
        TCHAR szBuffer[MAX_PATH];

		for(int i=0;i<nHeadNum;i++)
		{
			::ZeroMemory(&hdItem, sizeof(hdItem));            
			hdItem.mask = HDI_TEXT;
            hdItem.pszText = szBuffer;
			hdItem.cchTextMax = MAX_PATH;
            pList->GetHeaderCtrl()->GetItem(i, &hdItem);   

			CString strName=(CString)hdItem.pszText;			
			if(strName==strHeader) return i;
		}
		return -1;
	}

};


//pListBox->SetRedraw(FALSE);
//pListBox->SetRedraw(TRUE);
//pListBox->Invalidate();