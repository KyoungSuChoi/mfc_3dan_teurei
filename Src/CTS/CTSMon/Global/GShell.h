
//#include <psapi.h>
//#pragma comment (lib, "psapi.lib")  /*for GetModuleFileNameEx*/

//구조체
struct TRAYDATA
{
	HWND hwnd;
	UINT uID;
	UINT uCallbackMessage;
	DWORD Reserved[2];
	HICON hIcon;
};


class GShell
{
public:   
	GShell();
    ~GShell();
    		
	/*static void shell_TrayInit(NOTIFYICONDATA &Nid,HWND hWnd,UINT dwicon,UINT dwmsg,CString className)
	{	  
		Nid.cbSize = sizeof(Nid);
		Nid.hWnd =hWnd;     
		Nid.uID = dwicon;         //icon resource
		Nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
		Nid.uCallbackMessage = dwmsg; //handler
		Nid.hIcon = AfxGetApp()->LoadIcon(dwicon);
		lstrcpy(Nid.szTip,className); //tooltip
		Shell_NotifyIcon(NIM_ADD,&Nid); 
		::SendMessage(hWnd,WM_SETICON,(WPARAM)TRUE,(LPARAM)Nid.hIcon);  
	}

	static void shell_TrayClose(NOTIFYICONDATA &Nid)
	{
		if(Nid.hWnd) Shell_NotifyIcon(NIM_DELETE,&Nid);  
	}

	//TRAY ICON window handler
	static HWND shell_GetTrayHwnd()
	{//win 98, win2k, xp
		HWND hWnd_ToolbarWindow32 = NULL;
		HWND hWnd_ShellTrayWnd;

		hWnd_ShellTrayWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);
		if(hWnd_ShellTrayWnd)
		{
			HWND hWnd_TrayNotifyWnd = ::FindWindowEx(hWnd_ShellTrayWnd,NULL,_T("TrayNotifyWnd"), NULL);

			if(hWnd_TrayNotifyWnd)
			{
				HWND hWnd_SysPager = ::FindWindowEx(hWnd_TrayNotifyWnd,NULL,_T("SysPager"), NULL);	// WinXP
			        
				if(hWnd_SysPager)
				{	// WinXP=> SysPager search
					hWnd_ToolbarWindow32 = ::FindWindowEx(hWnd_SysPager, NULL,_T("ToolbarWindow32"), NULL);					
				} 
				else
				{ // Win2000=> SysPager none, TrayNotifyWnd -> ToolbarWindow32
            		hWnd_ToolbarWindow32 = ::FindWindowEx(hWnd_TrayNotifyWnd, NULL,_T("ToolbarWindow32"), NULL);
				}
			}
		}
	    
		return hWnd_ToolbarWindow32;
	}

	static HWND shell_GetTrayOverHwnd()
	{//vista,win7	
		HWND over_hWnd = ::FindWindow(_T("NotifyIconOverflowWindow"), NULL);
		if(!over_hWnd) return NULL;
				
		HWND tool_hWnd = ::FindWindowEx(over_hWnd,NULL,_T("ToolbarWindow32"), NULL);
		if(!tool_hWnd) return NULL;
		
		return tool_hWnd;		
	}

	static void shell_TrayClean()
	{
		shell_TrayMainRefresh();	
		shell_TrayOverRefresh();
	}	
		 
	static BOOL shell_TrayMainRefresh()
	{//xp		
		HWND hToolBarWindow=shell_GetTrayHwnd();
		if(hToolBarWindow)
		{
		   shell_TrayRefresh(hToolBarWindow);
		   shell_TrayMouseRefresh(hToolBarWindow);
		}
		return FALSE;
	}

	static BOOL shell_TrayOverRefresh()
	{//vista,win7	
		HWND hToolBarWindow=shell_GetTrayOverHwnd();
		if(hToolBarWindow)
		{								
			shell_TrayRefresh(hToolBarWindow);		
			shell_TrayMouseRefresh(hToolBarWindow);
			return TRUE;
		}
		return FALSE;
	}

	static BOOL shell_TrayMouseRefresh(HWND hToolbarWnd)
	{
		CRect rect;
		::GetWindowRect(hToolbarWnd,&rect);
		for(int x = 0; x < rect.right - rect.left; x+=8) 
		{
			for(int y = 0; y < rect.bottom - rect.top; y+=8)
			{
			 ::SendMessage(hToolbarWnd,WM_MOUSEMOVE,0,MAKELPARAM(x,y));
			}
	    }
        return TRUE;
	}
		
	static BOOL shell_TrayRefresh(HWND hToolbarWnd)
	{		
		if (hToolbarWnd == NULL)	return FALSE;
		
		try 
		{
			HANDLE 			hProcess;
			LPVOID 			lpData;
			TBBUTTON		tb;
			TRAYDATA		tray;
			DWORD 			dwTrayPid;
			int				TrayCount;
									
			// Tray get count
			TrayCount = (int)::SendMessage(hToolbarWnd, TB_BUTTONCOUNT, 0, 0);
							
			// Tray window handle get PID
			GetWindowThreadProcessId(hToolbarWnd, &dwTrayPid);
						
			// Tray Process open memory allocate
			hProcess = OpenProcess(PROCESS_VM_READ|PROCESS_VM_WRITE|PROCESS_VM_OPERATION, FALSE, dwTrayPid);			
			if (!hProcess) return FALSE;
									
			// current process memory allocate 
			lpData = VirtualAllocEx(hProcess, NULL, sizeof (TBBUTTON), MEM_COMMIT, PAGE_READWRITE);
			if (!lpData)	return false;
						
			// Tray count
			for(int i = 0; i < TrayCount; i++)
			{				
				::SendMessage(hToolbarWnd, TB_GETBUTTON, i, (LPARAM)lpData);	
				
				// TBBUTTON structure and TRAYDATA content get
				ReadProcessMemory(hProcess, lpData, (LPVOID)&tb, sizeof (TBBUTTON), NULL);
				ReadProcessMemory(hProcess, (LPCVOID)tb.dwData, (LPVOID)&tray, sizeof (tray), NULL);
	            
				// tray process number get
				DWORD dwProcessId = 0;
				GetWindowThreadProcessId(tray.hwnd, &dwProcessId);
											
				// Process none, TrayIcon delete
				if (dwProcessId == 0)
				{
					NOTIFYICONDATA	icon; 
					icon.cbSize	= sizeof(NOTIFYICONDATA);
					icon.hIcon	= tray.hIcon;
					icon.hWnd	= tray.hwnd;
					icon.uCallbackMessage = tray.uCallbackMessage;
					icon.uID	= tray.uID;
															
					Shell_NotifyIcon(NIM_DELETE, &icon);
				}
			}
			
			// virtual memory clear and process handle close
			VirtualFreeEx(hProcess, lpData, NULL, MEM_RELEASE);
			CloseHandle(hProcess);        
			
			return TRUE;
		}
		catch (...)
		{
			return FALSE;
		}
	}

	static BOOL shell_TrayUpdate()
	{
		HWND hWndTray = NULL;
		LPVOID   pData = NULL;
		int i = 0, nCount = 0;

		#ifdef _UNICODE	
		    wchar_t szBinPath[0xFF] = {0};
		#else
		    char szBinPath[0xFF] = {0};
		#endif

		TBBUTTON tb;
		TRAYDATA tray;
		DWORD dwTrayPid = 0, dwProcId = 0;
		HANDLE   hProcTray = NULL, hCurProc = NULL;
	    
		hWndTray = shell_GetTrayHwnd();                            //tray handle get
		nCount = (int)SendMessage(hWndTray, TB_BUTTONCOUNT, 0, 0); //tray button count get
		GetWindowThreadProcessId(hWndTray, &dwTrayPid);
		hProcTray = OpenProcess(PROCESS_VM_READ|PROCESS_VM_WRITE|PROCESS_VM_OPERATION, FALSE, dwTrayPid);
		pData = VirtualAllocEx(hProcTray, NULL, sizeof(TBBUTTON), MEM_COMMIT, PAGE_READWRITE);
	    
		//tray count info get
		for (i = 0; i < nCount; i++, dwProcId = 0)
		{
			SendMessage(hWndTray, TB_GETBUTTON, i, (LPARAM)pData);
			ReadProcessMemory(hProcTray, pData, (LPVOID)&tb, sizeof(TBBUTTON), NULL);
			ReadProcessMemory(hProcTray, (LPCVOID)tb.dwData, (LPVOID)&tray, sizeof(tray), NULL);
			GetWindowThreadProcessId(tray.hwnd, &dwProcId);

			if ( (hCurProc = OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ, FALSE, dwProcId)) != NULL )
			{
				int n = GetModuleFileNameEx(hCurProc, NULL, szBinPath, sizeof(szBinPath)-1);
				szBinPath[n] = 0;
				//printf("%5ld %s\n", dwProcId, szBinPath);
			}
			else
			{				
				//tray icon parent process none, tray delete
				NOTIFYICONDATA  icon;

				icon.cbSize = sizeof(NOTIFYICONDATA);
				icon.hIcon  = tray.hIcon;
				icon.hWnd   = tray.hwnd;
				icon.uCallbackMessage = tray.uCallbackMessage;
				icon.uID    = tray.uID;
	            
				Shell_NotifyIcon(NIM_DELETE, &icon);
			}
		}

		VirtualFreeEx(hProcTray, pData, 0, MEM_RELEASE);
		CloseHandle(hProcTray);

		return TRUE;
	}

	static void shell_Lock()
	{//toggle jobtray lock
	    HWND hShellWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);
		if(!hShellWnd) return;

		::PostMessage(hShellWnd, WM_COMMAND, MAKELONG(424, 0), NULL);
	}

	static void shell_TaskBarShow(int ishow)
	{
		HWND hShellWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);
		if(!hShellWnd) return;							

		HWND hButton=::FindWindowEx(hShellWnd, NULL,_T("Button"), _T("Start"));
		if(hButton==NULL)
		{
			hButton = ::FindWindow(_T("Button"),NULL);			
		}		
		
		BOOL bflag=::IsWindowVisible(hShellWnd);

		if(ishow==0 && bflag==TRUE)       
		{//hide
			if(hShellWnd) ::ShowWindow(hShellWnd, SW_HIDE);//0
			if(hButton)   ::ShowWindow(hButton,SW_HIDE);//0 vista start menu/button
		}
		else if(ishow==1 && bflag==FALSE)
		{//show
			if(hShellWnd) ::ShowWindow(hShellWnd, SW_SHOW);//1
			if(hButton)   ::ShowWindow(hButton,SW_SHOW);//1	vista start menu/button						
		}
		else if(ishow==2)
		{//반전
			if(bflag==TRUE)
			{
				if(hShellWnd) ::ShowWindow(hShellWnd, SW_HIDE);//0
				if(hButton)   ::ShowWindow(hButton,SW_HIDE);//0
			}
			else
			{
				if(hShellWnd) ::ShowWindow(hShellWnd, SW_SHOW);//1
				if(hButton)   ::ShowWindow(hButton,SW_SHOW);//1	vista start menu/button								
			}
		}
	}

	static void shell_TaskZorder(HWND zorder)
	{//jobtray zorder
	    HWND hShellWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);
		if(!hShellWnd) return;
		
		::SetWindowPos(hShellWnd,zorder,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	}

	static void shell_TaskAutoHide(int ntype)
	{//jobtray auto hide

		APPBARDATA TAppBarData; 
		memset(&TAppBarData,0, sizeof(TAppBarData)); 
		TAppBarData.cbSize = sizeof(TAppBarData); 
		TAppBarData.hWnd = (HWND)FindWindow(_T("Shell_TrayWnd"),NULL); 
		
		SHAppBarMessage(ABM_GETSTATE, &TAppBarData);
						
		if(ntype==1)      TAppBarData.lParam=ABS_AUTOHIDE | ABS_ALWAYSONTOP;
		else if(ntype==2) TAppBarData.lParam=ABS_AUTOHIDE;
		else if(ntype==3) TAppBarData.lParam=ABS_ALWAYSONTOP;
		else              TAppBarData.lParam=0;

		SHAppBarMessage(ABM_SETSTATE, &TAppBarData);
	}*/
};