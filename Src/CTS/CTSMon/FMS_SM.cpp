#include "stdafx.h"

#include "CTSMon.h"
#include "MainFrm.h"
#include "CTSMonDoc.h"

#include "FMS.h"
#include "FM_Unit.h"
#include "FM_STATE.h"

#include "FMS_SM.h"

DWORD WINAPI FMS_SENDThreadCallback(LPVOID parameter)
{
	CFMS_SM *Owner = (CFMS_SM*) parameter;
	Owner->FMS_SENDThreadCallback();

	return 0;
}

VOID CFMS_SM::SetHeartbeatClear()
{	
	m_HeartBeatCheck = 0;
}

BOOL CFMS_SM::ChkHeartbeat()
{
	/*
	if( !m_bActive )
	{
		return FALSE;
	}
	*/

	m_HeartBeatCheck++;

	if( m_HeartBeatCheck > 10 * FMS_HEARTBEATCHECK )		
	{
		m_HeartBeatCheck = 0;
		return TRUE;
	}

	return FALSE;
}

DWORD WINAPI FMS_MAINProcessCallback(LPVOID parameter)
{
	CFMS_SM *Owner = (CFMS_SM*) parameter;
	Owner->FMS_MAINProcessCallback();

	return 0;
}

// 1. FMS 데이터 파싱부분
VOID CFMS_SM::FMS_MAINProcessCallback(VOID)
{
	long nEventType = 0;
	long lParam = 0;
	short nEventId = 0;
	int nRet = 0;

	while (TRUE)
	{
		DWORD Result = WaitForSingleObject(mFMSMainThreadDestroyEvent, 100);

		if(Result == WAIT_OBJECT_0)
			return;

		if( m_bRunChk == FALSE )
		{
			m_bRunChk = TRUE;

			nRet = theApp.m_XComPro.PopEvent(&nEventType,&nEventId,&lParam);
			if( nRet >= 0) 
			{
				if(nEventType == SECS_EVENT_TYPE)
				{
					OnSecsEvent(nEventId,lParam);
				}
				else if(nEventType == SECS_MSG_TYPE)
				{					
					OnSecsMsg();
				}
			}

			m_bRunChk = FALSE;			
		}	
	}
}

void CFMS_SM::OnSecsEvent (short nEventId, long lParam)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	char    szMsg[256];

	if( nEventId == 203 ) { //  T3 timeout...
		CFMSLog::WriteLog( "[ALARM] T3 timeout alarm occurs" );
	}
	else if( nEventId == 102 ) {    //  NOT_SELECTED...
		m_bSECSCommunicationOK = FALSE;
		CFMSLog::WriteLog( "[EVENT] HSMS not selected event happens" );
	}
	else if( nEventId == 101 ) {    //  NOT_CONNECTED...
		m_bSECSCommunicationOK = FALSE;				
		CFMSLog::WriteLog( "[EVENT] HSMS not connected event happens" );
	}
	else if( nEventId == 103 ) {    //  SELECTED...
		m_bSECSCommunicationOK = TRUE;				
		CFMSLog::WriteLog( "[EVENT] HSMS selected event happens" );
	}
	else {
		sprintf( szMsg, "[EVENT] Event Code = %d, Parameter = %d", nEventId, lParam );
		CFMSLog::WriteLog( szMsg );
	}

	if( m_bSECSCommunicationOK == TRUE )
	{
		if( m_FmsSeq == SEQ_FMS_NONE )
		{
			m_FmsSeq = SEQ_FMS_POWER_ON;
		}
		else
		{
			m_FmsSeq = SEQ_FMS_RUN;
		}

		::PostMessage(m_Papa, EPWM_FMS_CONNECTED, 0, 0);
	}
	else
	{
		if( m_FmsSeq == SEQ_FMS_RUN )
		{
			m_FmsSeq = SEQ_FMS_WAIT;
		}
		else
		{
			m_FmsSeq = SEQ_FMS_NONE;
		}

		::PostMessage(m_Papa, EPWM_FMS_CLOSED, 0, 0);
	}
}


VOID CFMS_SM::fnSendToHost_S1F6( short nDevId, long lSysByte )
{
	USHORT nStageCnt = 0;
	int i=0;

	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));
	
	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 1;
	pPack->head.nFunc = 6;

	st_S1F6 data;
	ZeroMemory( &data, sizeof(st_S1F6) );

	data.SFCD = 1;	
	nStageCnt = m_vUnit.size();

	data.STAGETOTALCOUNT = nStageCnt;

	sprintf(data.TRAYTYPE, "%s", m_Not_Module_Fms.fnGetTrayType() );

	for( i=0; i<nStageCnt; i++ )
	{	
		sprintf(data.EQ_State[i].STAGENUMBER,"%s", m_vUnit[i]->fnGetModule()->GetModuleName() );
		m_vUnit[i]->fnGetFMS()->fnGetFMSImpossbleCode(data.EQ_State[i]);
	}

	memcpy(pPack->data, &data, sizeof(data));

	if( m_FmsSeq == SEQ_FMS_POWER_ON )
	{
		theApp.m_HSMS_P_SendQ.Push(*pPack);
	}
	else
	{
		theApp.m_HSMS_SendQ.Push(*pPack);
	}	
}

VOID CFMS_SM::fnSendToHost_S1F13( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 1;
	pPack->head.nFunc = 13;

	st_S1F13 data;
	ZeroMemory( &data, sizeof(st_S1F13) );

	sprintf(data.MDLN, "%s", m_strMDRN.Left(4) );
	sprintf(data.SOFTREV, "%s", m_strSOFTREV.Left(6) );

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);	
}

VOID CFMS_SM::fnSendToHost_S1F14( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 1;
	pPack->head.nFunc = 14;

	st_S1F14 data;
	ZeroMemory( &data, sizeof(st_S1F14) );

	data.COMMACK = 0;

	sprintf(data.MDLN, "%s", m_strMDRN.Left(4) );
	sprintf(data.SOFTREV, "%s", m_strSOFTREV.Left(6) );

	memcpy(pPack->data, &data, sizeof(data));

	if( m_FmsSeq == SEQ_FMS_POWER_ON )
	{
		theApp.m_HSMS_P_SendQ.Push(*pPack);
	}
	else
	{
		theApp.m_HSMS_SendQ.Push(*pPack);
	}	
}

VOID CFMS_SM::fnSendToHost_S1F2( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 1;
	pPack->head.nFunc = 2;

	st_S1F2 data;
	ZeroMemory(&data, sizeof(st_S1F2));

	sprintf(data.MDLN, "%s", m_strMDRN.Left(4) );
	sprintf(data.SOFTREV, "%s", m_strSOFTREV.Left(6) );

	memcpy(pPack->data, &data, sizeof(data));

	if( m_FmsSeq == SEQ_FMS_POWER_ON )
	{
		theApp.m_HSMS_P_SendQ.Push(*pPack);
	}
	else
	{
		theApp.m_HSMS_SendQ.Push(*pPack);
	}
}

VOID CFMS_SM::fnSendToHost_S1F102( short nDevId, long lSysByte, st_S1F101 stData)
{
	USHORT nStageCnt = 0;   //19-01-24 엄륭 헝가리 MES 사양 추가
	int i=0;

	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 1;
	pPack->head.nFunc = 102;


	CString strModuleName;
	st_S1F102 data;
	ZeroMemory( &data, sizeof(st_S1F102) );


	data.STAGETOTALCOUNT = stData.STAGETOTALCOUNT;


	if(data.STAGETOTALCOUNT >0)
	{
		for( i = 0; i < data.STAGETOTALCOUNT; i++ )
		{
			sprintf(data.EQ_State[i].STAGENUMBER,"%s", stData.EQ_State[i].STAGENUMBER );
			strModuleName.Format("%s",data.EQ_State[i].STAGENUMBER);
			CFormModule* pModule = fnGetModuleInfo(strModuleName);
			int nModuleIdx = fnGetModuleIdx(pModule->GetModuleID());

			m_vUnit[nModuleIdx]->fnGetFMS()->fnGetFMSNewImpossbleCode(data.EQ_State[i]);
			data.EQ_State[i].LINENO[0]= m_vUnit[nModuleIdx]->fnGetModuleLineNo();
		}
	}
	else
	{
		int nStageCnt = m_vUnit.size();
		for( i=0; i<nStageCnt; i++)
		{
			sprintf(data.EQ_State[i].STAGENUMBER, "%s", m_vUnit[i]->fnGetModuleName().Left(5));
			m_vUnit[i]->fnGetFMS()->fnGetFMSNewImpossbleCode(data.EQ_State[i]);
			data.EQ_State[i].LINENO[0]= m_vUnit[i]->fnGetModuleLineNo();
		}	
	}
	memcpy(pPack->data, &data, sizeof(data));

	theApp.m_HSMS_SendQ.Push(*pPack);

}

VOID CFMS_SM::fnSendToHost_S2F17( short nDevId, long lSysByte )
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 2;
	pPack->head.nFunc = 17;
	
	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS_SM::fnSendToHost_S2F32( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode )
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 2;
	pPack->head.nFunc = 32;

	st_S2F32 data;
	ZeroMemory(&data, sizeof(st_S2F32));

	if( fmsERCode == FMS_ER_NONE )
	{
		data.ACK = 0;
	}
	else
	{
		data.ACK = 1;
	}

	memcpy(pPack->data, &data, sizeof(data));

	theApp.m_HSMS_SendQ.Push(*pPack);
}

VOID CFMS_SM::fnSendToHost_S5F102( short nDevId, long lSysByte, FMS_ERRORCODE fmsERCode )
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = nDevId;
	pPack->head.lSysByte = lSysByte;
	pPack->head.nStrm = 5;
	pPack->head.nFunc = 102;

	st_S5F102 data;
	ZeroMemory(&data, sizeof(st_S5F102));

	if( fmsERCode == FMS_ER_NONE )
	{
		data.ACK = 0;
	}
	else
	{
		data.ACK = 1;
	}

	memcpy(pPack->data, &data, sizeof(data));

	theApp.m_HSMS_SendQ.Push(*pPack);
}

void CFMS_SM::OnSecsMsg()
{
	//kveby HSMS 정보 받는 부분
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	char    szMsg[255], szTemp[32];
	short   nStrm, nFunc, nWbit, nDevId;
	long    lMsgId, lSysByte, lSize;
 	int     nReturn, i, j;
 	long	nItems = 0;
 	BYTE	nBinary = 0;
	short   nI2 = 0;
	long    lCnt;
	char    szString[255], szJis8[255];
 	WORD    nU2 = 0;

	while( theApp.m_XComPro.LoadSecsMsg( &lMsgId, &nDevId, &nStrm, &nFunc, &lSysByte, &nWbit ) >= 0 ) {
		sprintf( szMsg, "Received S%dF%d", nStrm, nFunc );
		CFMSLog::WriteLog( szMsg );

		if( (nStrm == 1) && (nFunc == 1) ) {
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
			fnSendToHost_S1F2(nDevId, lSysByte);	
			
		}	
		else if( (nStrm == 1) && (nFunc == 5) ) {
			nReturn = theApp.m_XComPro.GetBinaryItem( lMsgId, &nBinary, &lCnt, 1 );
			theApp.m_XComPro.CloseSecsMsg( lMsgId );

			if( nBinary == 1 )
			{
				fnSendToHost_S1F6(nDevId, lSysByte);				
			}			
		}
		else if( (nStrm == 1) && (nFunc == 13) ) {
			// Check the type of S1F13 message
			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
// 			sprintf( szMsg, "LIST %d: next=%d", nItems, nReturn );
// 			CFMSLog::WriteLog( szMsg );

			if( nItems == 0 ) {
				theApp.m_XComPro.CloseSecsMsg( lMsgId );
				fnSendToHost_S1F14(nDevId, lSysByte);
			}
			else {
				// Read data
				nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 				sprintf( szMsg, "     ASCII %s: next=%d", szString, nReturn );
// 				CFMSLog::WriteLog( szMsg );

				nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 				sprintf( szMsg, "     ASCII %s: next=%d", szString, nReturn );
// 				CFMSLog::WriteLog( szMsg );
				theApp.m_XComPro.CloseSecsMsg( lMsgId );

				fnSendToHost_S1F14(nDevId, lSysByte);
			}
		}
		else if( (nStrm == 1) && (nFunc == 17) ) {  //20190806 엄륭 자동교정 모드 변경

			st_S1F17 data;
			ZeroMemory(&data, sizeof(st_S1F17));
			
			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
			nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.STAGENUMBER, &lSize, sizeof(data.STAGENUMBER));
			nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.CTRLMODE, &lCnt, 1 );			
			theApp.m_XComPro.CloseSecsMsg( lMsgId );

			int stageidx = fnGetStageIdx( atoi(data.STAGENUMBER) );
			int nSetOperationMode;

			if( data.CTRLMODE == 0 )
			{
				nSetOperationMode = EP_OPERATION_AUTO;
			}
			else if(data.CTRLMODE == 1)
			{
				nSetOperationMode = EP_OPERATION_LOCAL;
			}
			else if(data.CTRLMODE == 2)// 2019-02-19 엄륭 MES 사양 변경 관련
			{
				nSetOperationMode = EP_OPERATION_CALIBRATION;
			}

			FMS_ERRORCODE fmsERCode = FMS_ER_NONE;

			if(stageidx >= 0 && stageidx < m_vUnit.size())
			{
				FM_STATUS_ID ModeID = m_vUnit[stageidx]->fnGetState()->fnGetEQStateID();
				FM_STATUS_ID stateID = m_vUnit[stageidx]->fnGetState()->fnGetStateID();
				
				// 현재 모드
				switch(ModeID)
				{
				case EQUIP_ST_AUTO:
					{
						switch(data.CTRLMODE)
						{
						case 0:		// Auto
							fmsERCode = FMS_ER_NONE;
							break;
						case 1:		// Local
							{
								if(stateID != AUTO_ST_RUN && stateID != AUTO_ST_CONTACT_CHECK )
								{
									FMS_ERRORCODE ercode = m_vUnit[stageidx]->fnGetFMS()->fnSetOperationMode(nSetOperationMode);
									if(ercode == ER_SBC_ConditionError)
									{
										m_vUnit[stageidx]->fnSendEmg(m_Papa, 253);
										m_vUnit[stageidx]->fnGetFMS()->fnSetError();
									}
									fmsERCode = FMS_ER_NONE;
								}
							}
							break;

						case 2:		// Calibration // 2019-02-19 엄륭 MES 사양 변경 관련
							{
//								if(m_vUnit[stageidx]->fnGetModuleTrayState() == FALSE 
								if(	m_vUnit[stageidx]->fnGetModuleState() != AUTO_ST_RUN
									&& m_vUnit[stageidx]->fnGetModuleState() != AUTO_ST_CONTACT_CHECK)	// OFFLINE TEST 할거면 지울것
								{									
									FMS_ERRORCODE ercode = m_vUnit[stageidx]->fnGetFMS()->fnSetOperationMode(nSetOperationMode);
									if(ercode == ER_SBC_ConditionError)
									{
										m_vUnit[stageidx]->fnSendEmg(m_Papa, 253);
										m_vUnit[stageidx]->fnGetFMS()->fnSetError();
									}
									fmsERCode = FMS_ER_NONE;

									g_nCaliMode = 1;
								}
							}
							break;

						default:
							{
								fmsERCode = ER_Settiing_Data_error;								
							}							
						}
					}
					break;
				case EQUIP_ST_LOCAL:
					{
						switch(data.CTRLMODE)
						{
						case 0:		// Auto
							{
								if(m_vUnit[stageidx]->fnGetModuleTrayState() == FALSE 
									&& m_vUnit[stageidx]->fnGetModuleState() == EP_STATE_IDLE)	//OFFLINE TEST
								{									
									FMS_ERRORCODE ercode = m_vUnit[stageidx]->fnGetFMS()->fnSetOperationMode(nSetOperationMode);
									if(ercode == ER_SBC_ConditionError)
									{
										m_vUnit[stageidx]->fnSendEmg(m_Papa, 253);
										m_vUnit[stageidx]->fnGetFMS()->fnSetError();
									}

									fmsERCode = FMS_ER_NONE;
								}											
							}
							break;
						case 1:		// Local
							{									
								fmsERCode = FMS_ER_NONE;
							}
							break;

						case 2:		// Calibration// 2019-02-19 엄륭 MES 사양 변경 관련
							{									
								fmsERCode = FMS_ER_NONE;
							}
							break;
						default:
							fmsERCode = ER_Settiing_Data_error;
						}
					}
					break;			
				case EQUIP_ST_CALI:	//20190903 KSJ
					{
						switch(data.CTRLMODE)
						{
						case 0:		// Auto
							{
								 if(m_vUnit[stageidx]->fnGetModuleTrayState() == FALSE 
								 && m_vUnit[stageidx]->fnGetModuleState() == EP_STATE_IDLE)	//OFFLINE TEST
								{									
									FMS_ERRORCODE ercode = m_vUnit[stageidx]->fnGetFMS()->fnSetOperationMode(nSetOperationMode);
									if(ercode == ER_SBC_ConditionError)
									{
										m_vUnit[stageidx]->fnSendEmg(m_Papa, 253);
										m_vUnit[stageidx]->fnGetFMS()->fnSetError();
									}

									g_nCaliMode = 0;

									fmsERCode = FMS_ER_NONE;
								}											
							}
							break;
						case 1:		// Local
							{									
								FMS_ERRORCODE ercode = m_vUnit[stageidx]->fnGetFMS()->fnSetOperationMode(nSetOperationMode);
								if(ercode == ER_SBC_ConditionError)
								{
									m_vUnit[stageidx]->fnSendEmg(m_Papa, 253);
									m_vUnit[stageidx]->fnGetFMS()->fnSetError();
								}

								g_nCaliMode = 0;

								fmsERCode = FMS_ER_NONE;
							}
							break;

						case 2:	
							{									
								fmsERCode = FMS_ER_NONE;
							}
							break;
						default:
							fmsERCode = ER_Settiing_Data_error;
						}
					}
					break;		
				}

				m_vUnit[stageidx]->fnGetFMS()->fnSendToHost_S1F18(nDevId, lSysByte, fmsERCode );
			}			
		}		
		else if( (nStrm == 1) && (nFunc == 101) ) 
		{
			// //19-01-24 엄륭 헝가리 MES 사양 추가


			st_S1F101 data;
			WORD nStageNum = 0;
			ZeroMemory(&data, sizeof(st_S1F101));

			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

			data.STAGETOTALCOUNT = nItems;
			if(data.STAGETOTALCOUNT != 0)
			{
				for(i=0; i<data.STAGETOTALCOUNT; i++)
				{
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.EQ_State[i].STAGENUMBER, &lSize, sizeof(data.EQ_State));
				}
			}

			theApp.m_XComPro.CloseSecsMsg( lMsgId );

			fnSendToHost_S1F102(nDevId, lSysByte, data);
		}
		else if( (nStrm == 2) && (nFunc == 18) ) {
			nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 			sprintf( szMsg, "     ASCII %s: next=%d", szString, nReturn );
// 			CFMSLog::WriteLog( szMsg );			

			UINT v_year, v_month, v_day,v_hour,v_min,v_sec;
			sscanf(szString,"%04d%02d%02d%02d%02d%02d"
				, &v_year, &v_month, &v_day, &v_hour, &v_min, &v_sec);

			SYSTEMTIME	tnow;
			tnow.wYear = v_year;
			tnow.wMonth = v_month;
			tnow.wDay = v_day;
			tnow.wHour = v_hour;
			tnow.wMinute = v_min;
			tnow.wSecond = v_sec;
			tnow.wMilliseconds=0;

			SetLocalTime(&tnow);

			theApp.m_XComPro.CloseSecsMsg( lMsgId );			
		}
		else if( (nStrm == 2) && (nFunc == 31) ) {
			nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 			sprintf( szMsg, "     ASCII %s: next=%d", szString, nReturn );
// 			CFMSLog::WriteLog( szMsg );			

			UINT v_year, v_month, v_day,v_hour,v_min,v_sec;
			sscanf(szString,"%04d%02d%02d%02d%02d%02d"
				, &v_year, &v_month, &v_day, &v_hour, &v_min, &v_sec);
			
			FMS_ERRORCODE fmsERCode;

			SYSTEMTIME	tnow;
			tnow.wYear = v_year;
			tnow.wMonth = v_month;
			tnow.wDay = v_day;
			tnow.wHour = v_hour;
			tnow.wMinute = v_min;
			tnow.wSecond = v_sec;
			tnow.wMilliseconds=0;

			if( SetLocalTime(&tnow) )
			{
				fmsERCode = FMS_ER_NONE;
			}
			else
			{
				fmsERCode = ER_Time_Set_Error;
			}

			theApp.m_XComPro.CloseSecsMsg( lMsgId );

			fnSendToHost_S2F32(nDevId, lSysByte, fmsERCode );
		}
		else if( (nStrm == 2) && (nFunc == 41) ) {
			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
 			sprintf( szMsg, "LIST %d: next=%d", nItems, nReturn );
 			CFMSLog::WriteLog( szMsg );

			BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
			//CString strStageNum;
			nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
			/*nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

			strStageNum = szString;
			short stageidx = fnGetStageIdx(atoi(strStageNum));*/
			
			switch( nId )
			{
			case 1:	// 입고예약통지
				{	
					//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
					CString strStageNum;
					//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

					strStageNum = szString;
					short stageidx = fnGetStageIdx(atoi(strStageNum));

					st_CHARGER_INRESEVE stInreserv;
					ZeroMemory(&stInreserv, sizeof(st_CHARGER_INRESEVE));
					
					nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &stInreserv.nTraykind, &lCnt, 1 );
					
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, stInreserv.Process_Type, &lSize, sizeof(stInreserv.Process_Type));
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Process_No, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, stInreserv.Batch_No, &lSize, sizeof(stInreserv.Batch_No));					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Tap_Deepth, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Tray_Height, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &stInreserv.Tray_Type, &lCnt, 1 );					 
					nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &stInreserv.Channel_Insert_Type, &lCnt, 1 );					

					int nTrayCellCnt = stInreserv.Channel_Insert_Type;

					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Total_Step, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Contact_Error_Setting, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Capa_Error_Limit, &lCnt, 1 );

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

					if( nItems != stInreserv.nTraykind )
					{
						theApp.m_XComPro.CloseSecsMsg( lMsgId );

						FMS_ERRORCODE fmsERCode = ER_Data_Error;
						
						st_S2F411 data;
						ZeroMemory(&data, sizeof(st_S2F411));

						data.RCMD = nId;
						strcpy_s(data.STAGENUMBER, strStageNum);
						data.fmsERCode = fmsERCode;

						st_HSMS_PACKET pack;
						memset(&pack.head, 0, sizeof(pack.head));
						memset(pack.data, 0x20, sizeof(pack.data));

						pack.head.nDevId = nDevId;
						pack.head.lSysByte = lSysByte;
						pack.head.nStrm = nStrm;
						pack.head.nFunc = nFunc;
						pack.head.nId = nId;					

						memcpy(pack.data, &data, sizeof(data));

						m_FMS_Q[stageidx].Push(pack);
					}
					else
					{
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
						nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &nU2, &lCnt, 1 );	// Tray position 정보

						if( nU2 == 1 )
						{
							strcpy_s(stInreserv.Tray_ID_1, szString);

							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

							for( i=0; i<MAX_CELL_COUNT; i++ )
							{
								if( i < nTrayCellCnt )
								{
									nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									strcpy_s(stInreserv.arCell_ID[i].Cell_ID, szString);

									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									strcpy_s(stInreserv.arCellInfo[i].Cell_Info, szString);
								}
// 								else
// 								{
// 									nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
// 									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 								}							
							}
						}
						else
						{
							strcpy_s(stInreserv.Tray_ID_2, szString);

							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

							for( i=0; i<MAX_CELL_COUNT; i++ )
							{
								if( stInreserv.Tray_Type == 0 )		// 0:양방향, 1:단방향
								{
									if( i < MAX_TRAYCELL_COUNT )
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCell_ID[i+MAX_TRAYCELL_COUNT].Cell_ID, szString);

										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCellInfo[i+MAX_TRAYCELL_COUNT].Cell_Info, szString);
									}
// 									else
// 									{
// 										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
// 										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 									}
								}
								else
								{
									nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));								
								}
							}
						}

						if( stInreserv.nTraykind == 2 )
						{
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &nU2, &lCnt, 1 );		// Tray position 정보

							if( nU2 == 1 )
							{
								strcpy_s(stInreserv.Tray_ID_1, szString);

								nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

								for( i=0; i<MAX_CELL_COUNT; i++ )
								{
									if( i < nTrayCellCnt )
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCell_ID[i].Cell_ID, szString);

										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCellInfo[i].Cell_Info, szString);
									}
// 									else
// 									{
// 										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
// 										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 									}						
								}
							}
							else
							{
								strcpy_s(stInreserv.Tray_ID_2, szString);

								nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

								for( i=0; i<MAX_CELL_COUNT; i++ )
								{
									if( stInreserv.Tray_Type == 0 )		// 0:양방향, 1:단방향
									{
										if( i < MAX_TRAYCELL_COUNT )
										{
											nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
											strcpy_s(stInreserv.arCell_ID[i+MAX_TRAYCELL_COUNT].Cell_ID, szString);

											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
											strcpy_s(stInreserv.arCellInfo[i+MAX_TRAYCELL_COUNT].Cell_Info, szString);
										}
// 										else
// 										{
// 											nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
// 											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
// 										}
									}
									else
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									}
								}
							}
						}

						CString PVNAME = _T("");
						CString PVVALUE = _T("");
						WORD nPVItemCnt = 0;

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nPVItemCnt = nItems;

						for( i = 0; i < nPVItemCnt; i++ ) {
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVNAME = szString;
							PVNAME.Trim();
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVVALUE = szString;
							PVVALUE.Trim();

							// if( PVNAME == _T("Set Contact Current") )
							if( PVNAME == _T("SCC") )
							{
								strcpy_s(stInreserv.Contact.Charge_Cur, PVVALUE);
							}
							// else if( PVNAME == _T("Set Contact Time") )
							else if( PVNAME == _T("SCT") )
							{
								strcpy_s(stInreserv.Contact.Charge_Time, PVVALUE);								 
							}
							// else if( PVNAME == _T("Initial Voltage") )
							else if( PVNAME == _T("IV") )
							{
								strcpy_s(stInreserv.Contact.Inverse_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("V Max") )
							else if( PVNAME == _T("VMA") )
							{
								strcpy_s(stInreserv.Contact.Upper_Vol_Check, PVVALUE);
							}
							// else if( PVNAME == _T("V Min") )
							else if( PVNAME == _T("VMI") )
							{
								strcpy_s(stInreserv.Contact.Lower_Vol_Check, PVVALUE);
							}
							// else if( PVNAME == _T("I Max") )
							else if( PVNAME == _T("IMA") )
							{
								strcpy_s(stInreserv.Contact.Upper_Cur_Check, PVVALUE);
							}
							// else if( PVNAME == _T("I Min") )
							else if( PVNAME == _T("IMI") )
							{
								strcpy_s(stInreserv.Contact.Lower_Cur_Check, PVVALUE);
							}
							// else if( PVNAME == _T("Delta Voltage Limit") )
							else if( PVNAME == _T("DVL") )
							{
								strcpy_s(stInreserv.Contact.Delta_Vol_Limit, PVVALUE);
							}
						}

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nPVItemCnt = nItems;

						for( i = 0; i < nPVItemCnt; i++ ) 
						{
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVNAME = szString;
							PVNAME.Trim();
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVVALUE = szString;
							PVVALUE.Trim();

							// if( PVNAME == _T("CHG Voltage Limit") )
							if( PVNAME == _T("CVL") )
							{
								strcpy_s(stInreserv.Protect.Charge_LIMIT_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("CHG Current Limit") )
							else if( PVNAME == _T("CCL") )
							{
								strcpy_s(stInreserv.Protect.Charge_LIMIT_Cur, PVVALUE);
							}
							// else if( PVNAME == _T("CHG Upper Capacity") )
// 							else if( PVNAME == _T("CUC") )
// 							{
// 								strcpy_s(stInreserv.Protect.Charge_H_Cap, PVVALUE);
// 							}
// 							// else if( PVNAME == _T("CHG Lower Capacity") )
// 							else if( PVNAME == _T("CLC") )
// 							{
// 								strcpy_s(stInreserv.Protect.Charge_L_Cap, PVVALUE);
// 							}
							// else if( PVNAME == _T("DCHG Voltage Limit") )
							else if( PVNAME == _T("DVL") )
							{
								strcpy_s(stInreserv.Protect.DisCharge_LIMIT_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("DCHG Current Limit") )
							else if( PVNAME == _T("DCL") )
							{
								strcpy_s(stInreserv.Protect.DisCharge_LIMIT_Cur, PVVALUE);
							}
							// else if( PVNAME == _T("DCHG Upper Capacity") )
// 							else if( PVNAME == _T("DUC") )
// 							{
// 								strcpy_s(stInreserv.Protect.DisCharge_H_Cap, PVVALUE);
// 							}
// 							// else if( PVNAME == _T("DCHG Lower Capacity") )
// 							else if( PVNAME == _T("DLC") )
// 							{
// 								strcpy_s(stInreserv.Protect.DisCharge_L_Cap, PVVALUE);
// 							}
							// else if( PVNAME == _T("CHG Check Time") )
							else if( PVNAME == _T("CCT") )
							{
								strcpy_s(stInreserv.Protect.Charge_Vol_Get_Time, PVVALUE);
							}
							// else if( PVNAME == _T("CHG Check Upper Voltage") )
							else if( PVNAME == _T("CCUV") )
							{
								strcpy_s(stInreserv.Protect.Charge_H_Time_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("CHG Check Lower Voltage") )
							else if( PVNAME == _T("CCLV") )
							{
								strcpy_s(stInreserv.Protect.Charge_L_Time_Vol, PVVALUE);
							}

							//19-01-09 엄륭 DCIR 관련 MES 추가

							else if( PVNAME == _T("CACT") )
							{
								strcpy_s(stInreserv.Protect.Calc_Cap_Type, PVVALUE);								
							}
							else if( PVNAME == _T("COA") )
							{
								strcpy_s(stInreserv.Protect.Const_A, PVVALUE);								
							}
							else if( PVNAME == _T("COB") )
							{
								strcpy_s(stInreserv.Protect.Const_B, PVVALUE);								
							}
							else if( PVNAME == _T("COC") )
							{
								strcpy_s(stInreserv.Protect.Const_C, PVVALUE);								
							}
							else if( PVNAME == _T("COD") )
							{
								strcpy_s(stInreserv.Protect.Const_D, PVVALUE);								
							}
							else if( PVNAME == _T("COE") )
							{
								strcpy_s(stInreserv.Protect.Const_E, PVVALUE);								
							}

							else if( PVNAME == _T("DCIR") )
							{
								strcpy_s(stInreserv.Protect.DCIR, PVVALUE);								
							}
							else if( PVNAME == _T("DCIRA") )
							{
								strcpy_s(stInreserv.Protect.DCIR_A, PVVALUE);								
							}
							else if( PVNAME == _T("DCIRB") )
							{
								strcpy_s(stInreserv.Protect.DCIR_B, PVVALUE);								
							}
							else if( PVNAME == _T("DCIRC") )
							{
								strcpy_s(stInreserv.Protect.DCIR_C, PVVALUE);							
							}
							else if( PVNAME == _T("DCIR_MIN") )
							{
								strcpy_s(stInreserv.Protect.DCIR_MIN, PVVALUE);								
							}
							else if( PVNAME == _T("DCIR_MAX") )
							{
								strcpy_s(stInreserv.Protect.DCIR_MAX, PVVALUE);								
							}


						}

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

						for( i = 0; i < stInreserv.Total_Step; i++ ) {
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

							nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Step_Info[i].Step_ID, &lCnt, 1 );
							nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &stInreserv.Step_Info[i].Step_Type, &lCnt, 1 );

							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nPVItemCnt = nItems;

							for( j = 0; j < nPVItemCnt; j++ ) 
							{
								nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

								nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
								PVNAME = szString;
								PVNAME.Trim();
								nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
								PVVALUE = szString;
								PVVALUE.Trim();

								if( PVNAME == _T("SC") )
								{	
									strcpy_s(stInreserv.Step_Info[i].Current, PVVALUE);
								}
								else if( PVNAME == _T("SV") )
								{
									strcpy_s(stInreserv.Step_Info[i].Volt, PVVALUE);
								}
								else if( PVNAME == _T("ET") )
								{
									strcpy_s(stInreserv.Step_Info[i].Time, PVVALUE);
								}
								else if( PVNAME == _T("EC") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_Current, PVVALUE);
								}
								else if( PVNAME == _T("EV") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_Volt, PVVALUE);
								}
								else if( PVNAME == _T("ECA") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_A, PVVALUE);
								}
								else if( PVNAME == _T("GT") )
								{
									strcpy_s(stInreserv.Step_Info[i].GetTime_Setting, PVVALUE);
								}
								else if( PVNAME == _T("UCA") )
								{
									strcpy_s(stInreserv.Step_Info[i].Cap_H, PVVALUE);
								}
								else if( PVNAME == _T("LCA") )
								{
									strcpy_s(stInreserv.Step_Info[i].Cap_L, PVVALUE);
								}
								else if( PVNAME == _T("DC") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_SOC, PVVALUE);
								}
								else if( PVNAME == _T("STV") )
								{
									strcpy_s(stInreserv.Step_Info[i].Step_Start_Volt, PVVALUE);
								}
							}
						}

						theApp.m_XComPro.CloseSecsMsg( lMsgId );

						FMS_ERRORCODE fmsERCode = FMS_ER_NONE;
						fmsERCode = m_vUnit[stageidx]->fnGetFMS()->fnSetReserve(stInreserv);

						st_S2F411 data;
						ZeroMemory(&data, sizeof(st_S2F411));

						data.RCMD = nId;
						strcpy_s(data.STAGENUMBER, strStageNum);
						data.fmsERCode = fmsERCode;

						st_HSMS_PACKET pack;
						memset(&pack.head, 0, sizeof(pack.head));
						memset(pack.data, 0x20, sizeof(pack.data));

						pack.head.nDevId = nDevId;
						pack.head.lSysByte = lSysByte;
						pack.head.nStrm = nStrm;
						pack.head.nFunc = nFunc;
						pack.head.nId = nId;					

						memcpy(pack.data, &data, sizeof(data));

						m_FMS_Q[stageidx].Push(pack);
					}									
				}
				break;
			case 2:	// 입고완료통지
				{
					//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
					CString strStageNum;
					//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

					strStageNum = szString;
					short stageidx = fnGetStageIdx(atoi(strStageNum));

					st_S2F412 data;
					ZeroMemory(&data, sizeof(st_S2F412));

					strcpy_s(data.STAGENUMBER, strStageNum);

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_1, &lSize, sizeof(data.TRAYID_1));					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_1, &lCnt, 1 );					 
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_2, &lSize, sizeof(data.TRAYID_2));
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_2, &lCnt, 1 );
					theApp.m_XComPro.CloseSecsMsg( lMsgId );

					st_HSMS_PACKET pack;
					memset(&pack.head, 0, sizeof(pack.head));
					memset(pack.data, 0x20, sizeof(pack.data));
					
					pack.head.nDevId = nDevId;
					pack.head.lSysByte = lSysByte;
					pack.head.nStrm = nStrm;
					pack.head.nFunc = nFunc;
					pack.head.nId = nId;					

					memcpy(pack.data, &data, sizeof(data));

					m_FMS_Q[stageidx].Push(pack);	
				}
				break;
			case 3:	// 출고완료통지
				{
					//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
					CString strStageNum;
					//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

					strStageNum = szString;
					short stageidx = fnGetStageIdx(atoi(strStageNum));

					st_S2F413 data;
					ZeroMemory(&data, sizeof(st_S2F413));

					strcpy_s(data.STAGENUMBER, strStageNum);

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_1, &lSize, sizeof(data.TRAYID_1));					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_1, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_2, &lSize, sizeof(data.TRAYID_2));					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_2, &lCnt, 1 );					 
					theApp.m_XComPro.CloseSecsMsg( lMsgId );

					st_HSMS_PACKET pack;
					memset(&pack.head, 0, sizeof(pack.head));
					memset(pack.data, 0x20, sizeof(pack.data));

					pack.head.nDevId = nDevId;
					pack.head.lSysByte = lSysByte;
					pack.head.nStrm = nStrm;
					pack.head.nFunc = nFunc;
					pack.head.nId = nId;					

					memcpy(pack.data, &data, sizeof(data));

					m_FMS_Q[stageidx].Push(pack);	
				}
				break;
			case 4:	// 입고전 예약취소통지
				{
					//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
					CString strStageNum;
					//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

					strStageNum = szString;
					short stageidx = fnGetStageIdx(atoi(strStageNum));

					st_S2F414 data;
					ZeroMemory(&data, sizeof(st_S2F414));

					strcpy_s(data.STAGENUMBER, strStageNum);

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_1, &lSize, sizeof(data.TRAYID_1));
					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_1, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_2, &lSize, sizeof(data.TRAYID_2));
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_2, &lCnt, 1 );

					theApp.m_XComPro.CloseSecsMsg( lMsgId );

					st_HSMS_PACKET pack;
					memset(&pack.head, 0, sizeof(pack.head));
					memset(pack.data, 0x20, sizeof(pack.data));

					pack.head.nDevId = nDevId;
					pack.head.lSysByte = lSysByte;
					pack.head.nStrm = nStrm;
					pack.head.nFunc = nFunc;
					pack.head.nId = nId;					

					memcpy(pack.data, &data, sizeof(data));

					m_FMS_Q[stageidx].Push(pack);	
				}
				break;
			case 5:	// 결과파일수신확인통지
				{
					//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
					CString strStageNum;
					//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

					strStageNum = szString;
					short stageidx = fnGetStageIdx(atoi(strStageNum));

					st_S2F415 data;
					ZeroMemory(&data, sizeof(st_S2F415));

					strcpy_s(data.STAGENUMBER, strStageNum);

					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.MRCD, &lCnt, 1 );

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
					strcpy_s(data.TRAYID_1, szString);					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &nU2, &lCnt, 1 );
					data.TRAYPOSITION_1 = nU2;

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
					strcpy_s(data.TRAYID_2, szString);
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &nU2, &lCnt, 1 );
					data.TRAYPOSITION_2 = nU2;

					theApp.m_XComPro.CloseSecsMsg( lMsgId );

					st_HSMS_PACKET pack;
					memset(&pack.head, 0, sizeof(pack.head));
					memset(pack.data, 0x20, sizeof(pack.data));

					pack.head.nDevId = nDevId;
					pack.head.lSysByte = lSysByte;
					pack.head.nStrm = nStrm;
					pack.head.nFunc = nFunc;
					pack.head.nId = nId;					

					memcpy(pack.data, &data, sizeof(data));

					m_FMS_Q[stageidx].Push(pack);

					if( data.MRCD == 1 )
					{
						// MRCD 가 1인 경우 MES로 결과데이터 전송에 실패 알람 발생
// 20200713 KSCHOI S5F101 Bug Fixed StageID to StageIdx START
//						::PostMessage(m_Papa, EPWM_FMS_ERROR_INFO, atoi(strStageNum), 0);
						::PostMessage(m_Papa, EPWM_FMS_ERROR_INFO, stageidx+1, 0);
// 20200713 KSCHOI S5F101 Bug Fixed StageID to StageIdx END
					}
				}
				break;
			case 6:	// 입고 전 예약취소 통지
				{
					//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
					CString strStageNum;
					//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

					strStageNum = szString;
					short stageidx = fnGetStageIdx(atoi(strStageNum));

					st_S2F416 data;
					ZeroMemory(&data, sizeof(st_S2F416));

					strcpy_s(data.STAGENUMBER, strStageNum);

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_1, &lSize, sizeof(data.TRAYID_1));					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_1, &lCnt, 1 );					 
					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_2, &lSize, sizeof(data.TRAYID_2));
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_2, &lCnt, 1 );
					theApp.m_XComPro.CloseSecsMsg( lMsgId );

					st_HSMS_PACKET pack;
					memset(&pack.head, 0, sizeof(pack.head));
					memset(pack.data, 0x20, sizeof(pack.data));

					pack.head.nDevId = nDevId;
					pack.head.lSysByte = lSysByte;
					pack.head.nStrm = nStrm;
					pack.head.nFunc = nFunc;
					pack.head.nId = nId;					

					memcpy(pack.data, &data, sizeof(data));

					m_FMS_Q[stageidx].Push(pack);	
				}
				break;

			case 7:	// 입고예약 정보 재전송
				{	
					//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
					CString strStageNum;
					//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

					strStageNum = szString;
					short stageidx = fnGetStageIdx(atoi(strStageNum));

					st_CHARGER_INRESEVE stInreserv;
					ZeroMemory(&stInreserv, sizeof(st_CHARGER_INRESEVE));

					nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &stInreserv.nTraykind, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, stInreserv.Process_Type, &lSize, sizeof(stInreserv.Process_Type));	
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Process_No, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, stInreserv.Batch_No, &lSize, sizeof(stInreserv.Batch_No));					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Tap_Deepth, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Tray_Height, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &stInreserv.Tray_Type, &lCnt, 1 );					 
					nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &stInreserv.Channel_Insert_Type, &lCnt, 1 );					

					int nTrayCellCnt = stInreserv.Channel_Insert_Type;
					
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Total_Step, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Contact_Error_Setting, &lCnt, 1 );
					nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Capa_Error_Limit, &lCnt, 1 );

					nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

					if( nItems != stInreserv.nTraykind )
					{
						theApp.m_XComPro.CloseSecsMsg( lMsgId );

						FMS_ERRORCODE fmsERCode = FMS_ER_NONE;
						fmsERCode = m_vUnit[stageidx]->fnGetFMS()->fnSetReserve(stInreserv);

						st_S2F411 data;
						ZeroMemory(&data, sizeof(st_S2F411));

						data.RCMD = nId;
						strcpy_s(data.STAGENUMBER, strStageNum);
						data.fmsERCode = fmsERCode;

						st_HSMS_PACKET pack;
						memset(&pack.head, 0, sizeof(pack.head));
						memset(pack.data, 0x20, sizeof(pack.data));

						pack.head.nDevId = nDevId;
						pack.head.lSysByte = lSysByte;
						pack.head.nStrm = nStrm;
						pack.head.nFunc = nFunc;
						pack.head.nId = nId;					

						memcpy(pack.data, &data, sizeof(data));

						m_FMS_Q[stageidx].Push(pack);						
					}
					else
					{
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
						nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &nU2, &lCnt, 1 );	// Tray position 정보

						if( nU2 == 1 )
						{
							strcpy_s(stInreserv.Tray_ID_1, szString);

							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

							for( i=0; i<MAX_CELL_COUNT; i++ )
							{
								if( i < nTrayCellCnt )
								{
									nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									strcpy_s(stInreserv.arCell_ID[i].Cell_ID, szString);

									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									strcpy_s(stInreserv.arCellInfo[i].Cell_Info, szString);
								}
								else
								{
									nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
								}							
							}
						}
						else
						{
							strcpy_s(stInreserv.Tray_ID_2, szString);

							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

							for( i=0; i<MAX_CELL_COUNT; i++ )
							{
								if( stInreserv.Tray_Type == 0 )		// 0:양방향, 1:단방향
								{
									if( i < MAX_TRAYCELL_COUNT )
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCell_ID[i+MAX_TRAYCELL_COUNT].Cell_ID, szString);

										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCellInfo[i+MAX_TRAYCELL_COUNT].Cell_Info, szString);
									}
									else
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									}
								}
								else
								{
									nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
								}
							}
						}

						if( stInreserv.nTraykind == 2 )
						{
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &nU2, &lCnt, 1 );		// Tray position 정보

							if( nU2 == 1 )
							{
								strcpy_s(stInreserv.Tray_ID_1, szString);

								nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

								for( i=0; i<MAX_CELL_COUNT; i++ )
								{
									if( i < nTrayCellCnt )
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCell_ID[i].Cell_ID, szString);

										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										strcpy_s(stInreserv.arCellInfo[i].Cell_Info, szString);
									}
									else
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									}							
								}
							}
							else
							{
								strcpy_s(stInreserv.Tray_ID_2, szString);

								nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

								for( i=0; i<MAX_CELL_COUNT; i++ )
								{
									if( stInreserv.Tray_Type == 0 )		// 0:양방향, 1:단방향
									{
										if( i < MAX_TRAYCELL_COUNT )
										{
											nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
											strcpy_s(stInreserv.arCell_ID[i+MAX_TRAYCELL_COUNT].Cell_ID, szString);

											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
											strcpy_s(stInreserv.arCellInfo[i+MAX_TRAYCELL_COUNT].Cell_Info, szString);
										}
										else
										{
											nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
											nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										}
									}
									else
									{
										nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
										nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
									}
								}
							}
						}			

						CString PVNAME = _T("");
						CString PVVALUE = _T("");
						WORD nPVItemCnt = 0;

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nPVItemCnt = nItems;

						for( i = 0; i < nPVItemCnt; i++ ) {
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVNAME = szString;
							PVNAME.Trim();
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVVALUE = szString;
							PVVALUE.Trim();

							// if( PVNAME == _T("Set Contact Current") )
							if( PVNAME == _T("SCC") )
							{
								strcpy_s(stInreserv.Contact.Charge_Cur, PVVALUE);
							}
							// else if( PVNAME == _T("Set Contact Time") )
							else if( PVNAME == _T("SCT") )
							{
								strcpy_s(stInreserv.Contact.Charge_Time, PVVALUE);								 
							}
							// else if( PVNAME == _T("Initial Voltage") )
							else if( PVNAME == _T("IV") )
							{
								strcpy_s(stInreserv.Contact.Inverse_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("V Max") )
							else if( PVNAME == _T("VMA") )
							{
								strcpy_s(stInreserv.Contact.Upper_Vol_Check, PVVALUE);
							}
							// else if( PVNAME == _T("V Min") )
							else if( PVNAME == _T("VMI") )
							{
								strcpy_s(stInreserv.Contact.Lower_Vol_Check, PVVALUE);
							}
							// else if( PVNAME == _T("I Max") )
							else if( PVNAME == _T("IMA") )
							{
								strcpy_s(stInreserv.Contact.Upper_Cur_Check, PVVALUE);
							}
							// else if( PVNAME == _T("I Min") )
							else if( PVNAME == _T("IMI") )
							{
								strcpy_s(stInreserv.Contact.Lower_Cur_Check, PVVALUE);
							}
							// else if( PVNAME == _T("Delta Voltage Limit") )
							else if( PVNAME == _T("DVL") )
							{
								strcpy_s(stInreserv.Contact.Delta_Vol_Limit, PVVALUE);
							}
						}

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nPVItemCnt = nItems;

						for( i = 0; i < nPVItemCnt; i++ ) 
						{
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVNAME = szString;
							PVNAME.Trim();
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
							PVVALUE = szString;
							PVVALUE.Trim();

							// if( PVNAME == _T("CHG Voltage Limit") )
							if( PVNAME == _T("CVL") )
							{
								strcpy_s(stInreserv.Protect.Charge_LIMIT_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("CHG Current Limit") )
							else if( PVNAME == _T("CCL") )
							{
								strcpy_s(stInreserv.Protect.Charge_LIMIT_Cur, PVVALUE);
							}
//kveby 20190508
							// else if( PVNAME == _T("CHG Upper Capacity") )
// 							else if( PVNAME == _T("CUC") )
// 							{
// 								strcpy_s(stInreserv.Protect.Charge_H_Cap, PVVALUE);
// 							}
// 							// else if( PVNAME == _T("CHG Lower Capacity") )
// 							else if( PVNAME == _T("CLC") )
// 							{
// 								strcpy_s(stInreserv.Protect.Charge_L_Cap, PVVALUE);
// 							}
							// else if( PVNAME == _T("DCHG Voltage Limit") )
							else if( PVNAME == _T("DVL") )
							{
								strcpy_s(stInreserv.Protect.DisCharge_LIMIT_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("DCHG Current Limit") )
							else if( PVNAME == _T("DCL") )
							{
								strcpy_s(stInreserv.Protect.DisCharge_LIMIT_Cur, PVVALUE);
							}
//kveby 20190508
							// else if( PVNAME == _T("DCHG Upper Capacity") )
// 							else if( PVNAME == _T("DUC") )
// 							{
// 								strcpy_s(stInreserv.Protect.DisCharge_H_Cap, PVVALUE);
// 							}
// 							// else if( PVNAME == _T("DCHG Lower Capacity") )
// 							else if( PVNAME == _T("DLC") )
// 							{
// 								strcpy_s(stInreserv.Protect.DisCharge_L_Cap, PVVALUE);
// 							}
							// else if( PVNAME == _T("CHG Check Time") )
							else if( PVNAME == _T("CCT") )
							{
								strcpy_s(stInreserv.Protect.Charge_Vol_Get_Time, PVVALUE);
							}
							// else if( PVNAME == _T("CHG Check Upper Voltage") )
							else if( PVNAME == _T("CCUV") )
							{
								strcpy_s(stInreserv.Protect.Charge_H_Time_Vol, PVVALUE);
							}
							// else if( PVNAME == _T("CHG Check Lower Voltage") )
							else if( PVNAME == _T("CCLV") )
							{
								strcpy_s(stInreserv.Protect.Charge_L_Time_Vol, PVVALUE);
							}

							else if( PVNAME == _T("CACT") )
							{
								strcpy_s(stInreserv.Protect.Calc_Cap_Type, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %d\n",PVNAME,stInreserv.Protect.Calc_Cap_Type);
							}
							else if( PVNAME == _T("COA") )
							{
								strcpy_s(stInreserv.Protect.Const_A, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.Const_A);
							}
							else if( PVNAME == _T("COB") )
							{
								strcpy_s(stInreserv.Protect.Const_B, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %4.1f\n",PVNAME,stInreserv.Protect.Const_B);
							}
							else if( PVNAME == _T("COC") )
							{
								strcpy_s(stInreserv.Protect.Const_C, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.Const_C);
							}
							else if( PVNAME == _T("COD") )
							{
								strcpy_s(stInreserv.Protect.Const_D, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.Const_D);
							}
							else if( PVNAME == _T("COE") )
							{
								strcpy_s(stInreserv.Protect.Const_E, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.Const_E);
							}

							else if( PVNAME == _T("DCIR") )
							{
								strcpy_s(stInreserv.Protect.DCIR, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.DCIR);
							}
							else if( PVNAME == _T("DCIRA") )
							{
								strcpy_s(stInreserv.Protect.DCIR_A, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.DCIR_A);
							}
							else if( PVNAME == _T("DCIRB") )
							{
								strcpy_s(stInreserv.Protect.DCIR_B, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %4.1f\n",PVNAME,stInreserv.Protect.DCIR_B);
							}
							else if( PVNAME == _T("DCIRC") )
							{
								strcpy_s(stInreserv.Protect.DCIR_C, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.DCIR_C);
							}
							else if( PVNAME == _T("DCIR_MIN") )
							{
								strcpy_s(stInreserv.Protect.DCIR_MIN, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.DCIR_MIN);
							}
							else if( PVNAME == _T("DCIR_MAX") )
							{
								strcpy_s(stInreserv.Protect.DCIR_MAX, PVVALUE);
								//TRACE("=======PVNAME : %s =====PVVALUE : %5.3f\n",PVNAME,stInreserv.Protect.DCIR_MAX);
							}


						}

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

						for( i = 0; i < stInreserv.Total_Step; i++ ) {
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

							nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &stInreserv.Step_Info[i].Step_ID, &lCnt, 1 );
							nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &stInreserv.Step_Info[i].Step_Type, &lCnt, 1 );

							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nPVItemCnt = nItems;

							for( j = 0; j < nPVItemCnt; j++ ) 
							{
								nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

								nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
								PVNAME = szString;
								PVNAME.Trim();
								nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));
								PVVALUE = szString;	
								PVVALUE.Trim();

								if( PVNAME == _T("SC") )
								{	
									strcpy_s(stInreserv.Step_Info[i].Current, PVVALUE);
								}
								else if( PVNAME == _T("SV") )
								{
									strcpy_s(stInreserv.Step_Info[i].Volt, PVVALUE);
								}
								else if( PVNAME == _T("ET") )
								{
									strcpy_s(stInreserv.Step_Info[i].Time, PVVALUE);
								}
								else if( PVNAME == _T("EC") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_Current, PVVALUE);
								}
								else if( PVNAME == _T("EV") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_Volt, PVVALUE);
								}
								else if( PVNAME == _T("ECA") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_A, PVVALUE);
								}
								else if( PVNAME == _T("GT") )
								{
									strcpy_s(stInreserv.Step_Info[i].GetTime_Setting, PVVALUE);
								}
								else if( PVNAME == _T("UCA") )
								{
									strcpy_s(stInreserv.Step_Info[i].Cap_H, PVVALUE);
								}
								else if( PVNAME == _T("LCA") )
								{
									strcpy_s(stInreserv.Step_Info[i].Cap_L, PVVALUE);
								}
								else if( PVNAME == _T("DC") )
								{
									strcpy_s(stInreserv.Step_Info[i].CutOff_SOC, PVVALUE);
								}
								else if( PVNAME == _T("STV") )
								{
									strcpy_s(stInreserv.Step_Info[i].Step_Start_Volt, PVVALUE);
								}
							}
						}

						theApp.m_XComPro.CloseSecsMsg( lMsgId );

						FMS_ERRORCODE fmsERCode = FMS_ER_NONE;
						fmsERCode = m_vUnit[stageidx]->fnGetFMS()->fnSetReserve(stInreserv);

						st_S2F411 data;
						ZeroMemory(&data, sizeof(st_S2F411));

						data.RCMD = nId;
						strcpy_s(data.STAGENUMBER, strStageNum);
						data.fmsERCode = fmsERCode;

						st_HSMS_PACKET pack;
						memset(&pack.head, 0, sizeof(pack.head));
						memset(pack.data, 0x20, sizeof(pack.data));

						pack.head.nDevId = nDevId;
						pack.head.lSysByte = lSysByte;
						pack.head.nStrm = nStrm;
						pack.head.nFunc = nFunc;
						pack.head.nId = nId;					

						memcpy(pack.data, &data, sizeof(data));

						m_FMS_Q[stageidx].Push(pack);	
					}										
				}
				break;

				case 8:	// 교정입고예약통지  19-01-25 엄륭 MES 사양 변경
					{
						//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
						CString strStageNum;
						//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

						strStageNum = szString;
						short stageidx = fnGetStageIdx(atoi(strStageNum));

						st_S2F418 data;
						ZeroMemory(&data, sizeof(st_S2F418));

						strcpy_s(data.STAGENUMBER, strStageNum);

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_1, &lSize, sizeof(data.TRAYID_1));					
						nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_1, &lCnt, 1 );					 
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_2, &lSize, sizeof(data.TRAYID_2));
						nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_2, &lCnt, 1 );
						theApp.m_XComPro.CloseSecsMsg( lMsgId );

// 						CString strTemp;
// 						strTemp.Format("%s",data.TRAYID_1);
// 						sprintf(g_strCaliTRAYID, "%10s", );

						st_HSMS_PACKET pack;
						memset(&pack.head, 0, sizeof(pack.head));
						memset(pack.data, 0x20, sizeof(pack.data));

						pack.head.nDevId = nDevId;
						pack.head.lSysByte = lSysByte;
						pack.head.nStrm = nStrm;
						pack.head.nFunc = nFunc;
						pack.head.nId = nId;					

						memcpy(pack.data, &data, sizeof(data));

						m_FMS_Q[stageidx].Push(pack);	
					}
					break;

				case 9:	// 교정입고완료통지  19-01-25 엄륭 MES 사양 변경
					{

						//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
						CString strStageNum;
						//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );
						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

						strStageNum = szString;
						short stageidx = fnGetStageIdx(atoi(strStageNum));
						
						st_S2F419 data;
						ZeroMemory(&data, sizeof(st_S2F419));

						strcpy_s(data.STAGENUMBER, strStageNum);

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_1, &lSize, sizeof(data.TRAYID_1));					
						nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_1, &lCnt, 1 );					 
						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_2, &lSize, sizeof(data.TRAYID_2));
						nReturn = theApp.m_XComPro.GetU2Item( lMsgId, &data.TRAYPOSITION_2, &lCnt, 1 );
						theApp.m_XComPro.CloseSecsMsg( lMsgId );

						st_HSMS_PACKET pack;
						memset(&pack.head, 0, sizeof(pack.head));
						memset(pack.data, 0x20, sizeof(pack.data));

						pack.head.nDevId = nDevId;
						pack.head.lSysByte = lSysByte;
						pack.head.nStrm = nStrm;
						pack.head.nFunc = nFunc;
						pack.head.nId = nId;					

						memcpy(pack.data, &data, sizeof(data));

						m_FMS_Q[stageidx].Push(pack);	
					}
					break;

				case 10:	// Stage 라인 정보 설정  19-01-25 엄륭 MES 사양 변경 Line no / Lit n개 관련 수정 필요함
					{
						//BYTE nId;  //19-01-25 엄륭 MES 사양 변경 S2 F41  RCMD 10 때문에 변경함
						WORD nLItemCnt = 0;
						WORD nLItemCnt2 = 0;
						CString strStageNum;
						short stageidx;
						st_S2F4110 data;
						//nReturn = theApp.m_XComPro.GetU1Item(lMsgId, &nId, &lCnt, 1 );

						nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
						
						nLItemCnt = nItems;

						for(i=0; i<nLItemCnt; i++)
						{
							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, szString, &lSize, sizeof(szString));

							strStageNum = szString;
							stageidx = fnGetStageIdx(atoi(strStageNum));
							
							ZeroMemory(&data, sizeof(st_S2F4110));
							strcpy_s(data.STAGENUMBER, strStageNum);


							nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
							nLItemCnt2 = nItems;

							for(j=0; j<nLItemCnt2; j++)
							{
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.LINENO, 1);
							}							
						}



						theApp.m_XComPro.CloseSecsMsg( lMsgId );

						st_HSMS_PACKET pack;
						memset(&pack.head, 0, sizeof(pack.head));
						memset(pack.data, 0x20, sizeof(pack.data));

						pack.head.nDevId = nDevId;
						pack.head.lSysByte = lSysByte;
						pack.head.nStrm = nStrm;
						pack.head.nFunc = nFunc;
						pack.head.nId = nId;					

						memcpy(pack.data, &data, sizeof(data));

						m_FMS_Q[stageidx].Push(pack);	
					}
					break;
			}

			// theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
		else if( (nStrm == 5) && (nFunc == 2) ) {
			// Read U1 value
			nReturn = theApp.m_XComPro.GetBinaryItem( lMsgId, &nBinary, &lCnt, 1  );
			if( nBinary == 0 )
			{
				// 정상 처리
			}
			else
			{
				// 비정상 처리
			}
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
// 20201118 KSCHOI Add Break Fire Report START
		else if( (nStrm == 5) && (nFunc == 4) ) {
			// Read U1 value
			BYTE nAck = 0;
			nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &nAck, &lCnt, 1  );
			if( nAck == 0 )
			{
				// 정상 처리
			}
			else
			{
				// 비정상 처리
			}
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
// 20201118 KSCHOI Add Break Fire Report END
		else if( (nStrm == 5) && (nFunc == 101) ) {
			st_S5F101 data;
			ZeroMemory(&data, sizeof(st_S5F101));

			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
			nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.STAGENUMBER, &lSize, sizeof(data.STAGENUMBER) );
			nReturn = theApp.m_XComPro.GetU2Item(lMsgId, &data.MRCD, &lCnt, 1  );
			theApp.m_XComPro.CloseSecsMsg( lMsgId );

			INT stageidx = fnGetStageIdx(atoi(data.STAGENUMBER));

			FMS_ERRORCODE fmsERCode = ER_Status_Error;

			if( m_vUnit[stageidx]->fnGetFMSState() != FMS_ST_ERROR )
			{				
				fmsERCode = FMS_ER_NONE;
// 20200713 KSCHOI S5F101 Bug Fixed StageID to StageIdx START
//				::PostMessage(m_Papa, EPWM_FMS_ERROR_INFO, atoi(data.STAGENUMBER), data.MRCD);
				::PostMessage(m_Papa, EPWM_FMS_ERROR_INFO, stageidx+1, data.MRCD);
// 20200713 KSCHOI S5F101 Bug Fixed StageID to StageIdx END
			}
			//CFMS m_FMS;

			//m_FMS.fnGetFMSStateCode(FMS_ST_ERROR);			//19-0219 엄륭 MES 알람 발생시 상태 에러 상태로 S6f11 보고 
			fnSendToHost_S5F102(nDevId, lSysByte, fmsERCode );			
		}
		else if( (nStrm == 6) && (nFunc == 12) ) {
			st_S6F12 data;
			ZeroMemory(&data, sizeof(st_S6F12));

			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

			nReturn = theApp.m_XComPro.GetU1Item( lMsgId, &data.ACK, &lCnt, 1  );
			nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.STAGENUMBER, &lSize, sizeof(data.STAGENUMBER));
			
			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );

			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
			nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_1, &lSize, sizeof(data.TRAYID_1) );	// 하단 트레이 정보
			nReturn = theApp.m_XComPro.GetU2Item(lMsgId, &data.TRAYPOSITION_1, &lCnt, 1  );

			nReturn = theApp.m_XComPro.GetListItem( lMsgId, &nItems );
			nReturn = theApp.m_XComPro.GetAsciiItem( lMsgId, data.TRAYID_2, &lSize, sizeof(data.TRAYID_2) ); // 상단 트레이 정보
			nReturn = theApp.m_XComPro.GetU2Item(lMsgId, &data.TRAYPOSITION_2, &lCnt, 1  );
		
			theApp.m_XComPro.CloseSecsMsg( lMsgId );

			if(data.ACK != 0)
			{

			}
			/*
			st_HSMS_PACKET pack;
			memset(&pack.head, 0, sizeof(pack.head));
			memset(pack.data, 0x20, sizeof(pack.data));

			pack.head.nDevId = nDevId;
			pack.head.lSysByte = lSysByte;
			pack.head.nStrm = nStrm;
			pack.head.nFunc = nFunc;
			pack.head.nId = 0;			

			memcpy(pack.data, &data, sizeof(data));

			INT stageidx = fnGetStageIdx(atoi(data.STAGENUMBER));
			m_FMS_Q[stageidx].Push(pack);
			*/
		}
		else if( (nStrm == 1) && (nFunc == 2) ) {
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
		else if( (nStrm == 1) && (nFunc == 14) ) {
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
		else if( (nStrm == 2) && (nFunc == 14) ) {
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
		else if( (nStrm == 2) && (nFunc == 34) ) {
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
		else {
			sprintf( szMsg, "Undefined message received (S%dF%d)", nStrm, nFunc);
			CFMSLog::WriteLog( szMsg );
			theApp.m_XComPro.CloseSecsMsg( lMsgId );
		}
	}
}


INT CFMS_SM::fnGetStageIdx( int nMachineID )
{
	int nSize = m_vUnit.size();
	
	for(UINT i = 0; i < nSize;i++)
	{
		if( m_vUnit[i]->fnGetMachineId() == nMachineID )
		{
			return i;
		}
	}

	return 0;
};

INT CFMS_SM::fnGetModuleIdx( int nMachineID )
{
	int nSize = m_vUnit.size();

	for(UINT i = 0; i < nSize;i++)
	{
		if( m_vUnit[i]->fnGetModuleID() == nMachineID )
		{
			return i;
		}
	}

	return 0;
};


CFormModule* CFMS_SM::fnGetModuleInfo(CString moduleName)
{
	int nSize = m_vUnit.size();

	for(UINT i = 0; i < nSize;i++)
	{
		if( m_vUnit[i]->fnGetModuleName() == moduleName )
		{
			return m_vUnit[i]->fnGetModule();
		}
	}
}

DWORD WINAPI FMS_SMProcessCallback(LPVOID parameter)
{
	CFMS_SM *Owner = (CFMS_SM*) parameter;
	Owner->FMS_SMProcessCallback();

	return 0;
}

VOID CFMS_SM::FMS_SMProcessCallback(VOID)
{
	INT indexNum = mTcpTheadIndex++;

	SetEvent(mFMS_StartProcessHandle);

	while (TRUE)
	{
		DWORD Result = WaitForSingleObject(m_vUnitProcEvent[indexNum], FMSSM_MAIN_TIMER);

		if (m_bTreadStop)
			return;

		if(Result == WAIT_OBJECT_0)
		{
		}
		else
		{
			m_vUnit[indexNum]->fnGetState()->fnSBCPorcess(0);

			if(m_FMS_Q[indexNum].GetIsEmpty())
			{
				// Cmd 전송시 Blue, Red 상태 Time Delay 체크 부분
				m_vUnit[indexNum]->fnProcessing();
			}			

			while(m_FMS_Q[indexNum].GetIsEmpty() == FALSE)
			{
				st_HSMS_PACKET pack;				
				m_FMS_Q[indexNum].Pop(pack);

				FMS_ERRORCODE _rec = ER_END;

				_rec = m_vUnit[indexNum]->fnGetState()->fnFMSPorcess(&pack);

				switch( _rec )
				{				
				case ER_Run_Fail:
				case ER_SBC_ConditionError:
				case ER_fnMakeTrayInfo:
					{
						m_vUnit[indexNum]->fnSendEmg(m_Papa, 253);
						m_vUnit[indexNum]->fnGetFMS()->fnSetError();
					}
					break;

				case ER_fnTransFMSWorkInfoToCTSWorkInfo:
				case ER_fnSendConditionToModule:
					{
						m_vUnit[indexNum]->fnSendEmg(m_Papa, 254);
						m_vUnit[indexNum]->fnGetFMS()->fnSetError();
					}
					break;

				case ER_SBC_NetworkError:
					{
// 20210115 KSCHOI Disable SBC Network Error START
//						m_vUnit[indexNum]->fnSendEmg(m_Papa, 255);
//						m_vUnit[indexNum]->fnGetFMS()->fnSetError();
// 20210115 KSCHOI Disable SBC Network Error END
					}
					break;

				case ER_NO_Process:				
					{
						m_vUnit[indexNum]->fnGetFMS()->fnSendToHost_S2F42( pack.head.nDevId, pack.head.lSysByte, _rec );
						break;
					}
				case ER_fnRecipeOverC_Error:  //20200411 엄륭 용량 상한 관련
					{
						m_vUnit[indexNum]->fnSendEmg(m_Papa, 256);
						m_vUnit[indexNum]->fnGetFMS()->fnSetError();
					}
					break;
				}
			}
		}
	}
}

CFMS_SM::CFMS_SM(void)
: m_Papa(NULL)
, m_strMDRN("")
, m_strSOFTREV("") 
, m_bTreadStop(FALSE)
, mTcpTheadIndex(0)
, mFMS_StartProcessHandle(NULL)
, mFMSMainThreadHandle(NULL)
, mFMSMainThreadDestroyEvent(NULL)
{
	m_bWork = false;
	m_bMisSendWorking = false;
	m_nMisWorkingUnitNum = -1;
	m_nDeviceID = 0;

	m_bSECSCommunicationOK = FALSE;
	m_FmsSeq = SEQ_FMS_POWER_ON;

	m_HeartBeatCheck = 0;

	mKeepThreadHandle = NULL;
	mKeepThreadDestroyEvent = NULL;

	mObjDelete = NULL;
}

CFMS_SM::~CFMS_SM(void)
{

}

BOOL CFMS_SM::fnBegin(HWND _papa, CPtrArray& _ArrayModule, int nDeviceID )
{
	CFMSSyncObj FMSSync;
	
	char msg[256];
	
	m_Papa = _papa;

	m_nDeviceID = nDeviceID;

	m_Not_Module_Fms.fnInit(NULL);
	UINT i = 0;
	
	//프로세스 스레스 정상 시작 이벤트 생성
	mFMS_StartProcessHandle = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mFMS_StartProcessHandle)
	{
		return fnEnd();
	}
	
	for(i = 0; i < _ArrayModule.GetSize(); i++)
		{
		CFM_Unit* pTemp = new CFM_Unit();
		pTemp->fnInit(i, (CFormModule *)_ArrayModule.GetAt(i));
		//pTemp->fnSetPack(pack, linNo);
		//m_strFMID = pTemp->fnGetModule()->m_strModuleCode.Left(8);
		m_vUnit.push_back(pTemp);
	}
	
	//모듈만큼 프로세스 시작 이벤트 핸들 생성
	//i = 0;
	//for(i = 0; i < 5; i++)
	for(i = 0; i < _ArrayModule.GetSize(); i++)
	{
		HANDLE ProcEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		if(ProcEvent)
			m_vUnitProcEvent.push_back(ProcEvent);
		else
			return fnEnd();
	}
	
	for(i = 0; i < _ArrayModule.GetSize(); i++)
	{
		//프로세스 스레드 생성
		HANDLE WorkerThread = CreateThread(NULL, 0, ::FMS_SMProcessCallback, this, 0, NULL);
		if(WorkerThread)
		{
			m_vUnitThread.push_back(WorkerThread);
			WaitForSingleObject(mFMS_StartProcessHandle, INFINITE);
		}
		else
			return fnEnd();
	}

	CloseHandle(mFMS_StartProcessHandle);
	mFMS_StartProcessHandle = NULL;

	mFMSMainThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!mFMSMainThreadDestroyEvent)
	{
		return fnEnd();
	}

	mFMSMainThreadHandle = CreateThread(NULL, 0, ::FMS_MAINProcessCallback, this, 0, NULL);
	if(!mFMSMainThreadHandle)
	{
		return fnEnd();
	}

	m_bWork = true;			// FMS_SM 동작을 위한 준비 OK.
	m_bRunChk = FALSE;

	theApp.m_XComPro.SetEventHandlerMode(0);	

	m_strMDRN = AfxGetApp()->GetProfileString("FMS", "MDRN", "S4C1");
	m_strSOFTREV = AfxGetApp()->GetProfileString("FMS", "SOFTREV", "VER1.0");

	CString strCurFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"CTSMon");
	CString strFileName = _T("");
	strFileName.Format("%s\\Hsms\\CTSMon.cfg", strCurFolder );

	// CString strCurFolder = "EqSample.cfg";
	int nReturn = 0;

	if( (nReturn = theApp.m_XComPro.Initialize( strFileName )) == 0 ) {
		CFMSLog::WriteLog("theApp.m_XComPro initialized successfully");		

		mKeepThreadHandle = NULL;
		mKeepThreadDestroyEvent = NULL;

		mKeepThreadDestroyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		if (!mKeepThreadDestroyEvent)
		{
			return fnEnd();
		}

		mKeepThreadHandle = CreateThread(NULL, 0, ::FMS_SENDThreadCallback, this, 0, NULL);
		if (!mKeepThreadHandle)
		{
			return fnEnd();
		}

		mObjDelete = CreateEvent(NULL, FALSE, FALSE, NULL);
		if (!mObjDelete)
		{
			return fnEnd();
		}
	}
	else {
		sprintf(msg, "Fail to initialize theApp.m_XComPro :: %d", nReturn);
		CFMSLog::WriteLog(msg);
		return FALSE;
	}

	// Initialize Combobox
	CString m_cmbMsg;
	CString m_cmbDevice;
	m_cmbMsg = "S1F1";
	m_cmbDevice = "1";

	m_bSECSCommunicationOK = FALSE;

	if( (nReturn = theApp.m_XComPro.Start()) == 0 ) {
		
		sprintf(msg, "theApp.m_XComPro started successfully :: %d", nReturn);
		CFMSLog::WriteLog(msg);
	}
	else {
		sprintf(msg, "Fail to start theApp.m_XComPr :: %d", nReturn);
		CFMSLog::WriteLog(msg);
		return FALSE;
	}

	return TRUE;
}

BOOL CFMS_SM::fnEnd()
{
	if (mKeepThreadDestroyEvent && mKeepThreadHandle)
	{
		SetEvent(mKeepThreadDestroyEvent);

		WaitForSingleObject(mKeepThreadHandle, INFINITE);

		CloseHandle(mKeepThreadDestroyEvent);
		CloseHandle(mKeepThreadHandle);
	}

	CloseHandle(mObjDelete);

	if (mFMSMainThreadDestroyEvent && mFMSMainThreadHandle)
	{
		SetEvent(mFMSMainThreadDestroyEvent);
		WaitForSingleObject(mFMSMainThreadHandle, INFINITE);
		CloseHandle(mFMSMainThreadDestroyEvent);
		CloseHandle(mFMSMainThreadHandle);
	}

	m_bTreadStop = TRUE;

	UINT i = 0;
	for(i = 0; i < m_vUnitThread.size(); i++)
	{
		SetEvent(m_vUnitProcEvent[i]);
		WaitForSingleObject(m_vUnitThread[i], INFINITE);
		CloseHandle(m_vUnitProcEvent[i]);
		CloseHandle(m_vUnitThread[i]);	
	}

	m_vUnitProcEvent.clear();
	m_vUnitThread.clear();

	for(i = 0; i < m_vUnit.size(); i++)
	{
		CFM_Unit* ptemp = m_vUnit[i];

		ptemp->fnUnInit();

		delete ptemp;
	}
	
	m_vUnit.clear();
	
	m_bWork = false;

	theApp.m_XComPro.Stop();

	return TRUE;
}

BOOL CFMS_SM::fnSetLocalEMG( INT nLaneNum, CString emgCode )
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = 1;
	pPack->head.lSysByte = 0;
	pPack->head.nStrm = 5;
	pPack->head.nFunc = 1;
	pPack->head.nId = 0;	

	st_S5F1 data;
	ZeroMemory( &data, sizeof(st_S5F1));

	sprintf(data.STAGENUMBER, "00000" );

	data.ALST = 1;
	data.ALID = 81;

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);

	return TRUE;
}

VOID CFMS_SM::fnSetLocalEMG_Reset()
{
	st_HSMS_PACKET* pPack = m_Not_Module_Fms.fnGetHsmsPack();
	memset(&pPack->head, 0, sizeof(pPack->head));
	memset(pPack->data, 0x20, sizeof(pPack->data));

	pPack->head.nDevId = 1;
	pPack->head.lSysByte = 0;
	pPack->head.nStrm = 5;
	pPack->head.nFunc = 1;
	pPack->head.nId = 0;	

	st_S5F1 data;
	ZeroMemory( &data, sizeof(st_S5F1));

	sprintf(data.STAGENUMBER, "00000" );

	data.ALST = 0;
	data.ALID = 0;

	memcpy(pPack->data, &data, sizeof(data));	

	theApp.m_HSMS_SendQ.Push(*pPack);
}

BOOL CFMS_SM::fnSendContactResultData( INT moduleID )
{
	if( m_bWork == false )
	{
		return FALSE;	
	}

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		if( m_vUnit[moduleidx]->fnGetState()->fnGetEQStateID() == EQUIP_ST_AUTO )
		{
			m_vUnit[moduleidx]->fnGetFMS()->fnSendToHost_S6F11C7(1, 0);
		}		
	}
	else
	{
		CFMSLog::WriteErrorLog("fnSendContactResultData [%d]", moduleID);	
	}

	return TRUE;
}

BOOL CFMS_SM::fnChangeReserveProcPosition( INT moduleID )
{
	if( m_bWork == false )
	{
		return FALSE;	
	}

	BOOL _result = FALSE;

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;

		CFM_Unit* pUnit = m_vUnit[moduleidx];
		
		if(pUnit->fnGetState()->fnGetEQStateID() == EQUIP_ST_AUTO
			&& pUnit->fnGetModuleTrayState() == TRUE
			&& !(pUnit->fnGetState()->fnGetStateID() == AUTO_ST_RUN)
			&& !(pUnit->fnGetState()->fnGetStateID() == AUTO_ST_CONTACT_CHECK) )
		{
			m_vUnit[moduleidx]->fnGetFMS()->fnSendToHost_S6F11C6(1, 0);

			_result = TRUE;
		}
	}
	else
	{
		CFMSLog::WriteErrorLog("fnChangeReserveProcPosition [%d]", moduleID);	
	}

	return _result;
}


BOOL CFMS_SM::fnCalbrationComplete( INT moduleID )
{
	int nModuleIdx =  fnGetModuleIdx(moduleID);
	m_vUnit[nModuleIdx]->fnGetFMS()->fnSendToHost_S6F11C18( 1, 0 );

	return TRUE;
}


BOOL CFMS_SM::fnTrayOutCMD( INT moduleID )
{
	if( m_bWork == false )
	{
		return FALSE;	
	}

	BOOL _result = FALSE;

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;

		CFM_Unit* pUnit = m_vUnit[moduleidx];

// 		if(pUnit->fnGetState()->fnGetEQStateID() == EQUIP_ST_AUTO
// 			&& pUnit->fnGetModuleTrayState() == TRUE
// 			&& !(pUnit->fnGetState()->fnGetStateID() == AUTO_ST_RUN)
// 			&& !(pUnit->fnGetState()->fnGetStateID() == AUTO_ST_CONTACT_CHECK) )
// 		{
			m_vUnit[moduleidx]->fnGetFMS()->fnSendToHost_S6F11C19(1, 0);

			_result = TRUE;
//		}
	}
	else
	{
		CFMSLog::WriteErrorLog("fnTrayOutCMD [%d]", moduleID);	
	}

	return _result;
}

BOOL CFMS_SM::fnSetEMG(INT moduleID, CString emgCode, CString emgDescription)
{
	if( m_bWork == false )
	{
		return FALSE;	
	}
	
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		m_vUnit[moduleidx]->fnGetFMS()->fnSendToHost_S5F1(emgCode, emgDescription);

		emgCode.Trim();

// 20201125 KSCHOI Add BreakFireStatusReport By Bottom Smoke Sensor EMG START
//		if( emgCode == "68" || emgCode == "72" || emgCode == "71" )  //20200411 엄륭 화재관련 시퀀스
		if( emgCode == "68" || emgCode == "72" || emgCode == "71" || emgCode == "78" )  //20200411 엄륭 화재관련 시퀀스
// 20201125 KSCHOI Add BreakFireStatusReport By Bottom Smoke Sensor EMG END
		{
// 20201118 KSCHOI Add Break Fire Report START
//			// 1. Stage 에 트레이가 있을 경우에만 송신
//			if( m_vUnit[moduleidx]->fnGetModuleTrayState() == TRUE )
//			{
//				m_vUnit[moduleidx]->fnGetFMS()->fnSendToHost_S6F11C200(1, 0);
//			}
			m_vUnit[moduleidx]->fnGetFMS()->SetFireAlarmStatus(TRUE);
			m_vUnit[moduleidx]->fnGetFMS()->fnSendToHost_S5F3(TRUE, 1, m_vUnit[moduleidx]->fnGetModuleTrayState(), 0);
// 20201118 KSCHOI Add Break Fire Report END
		}

		CFMSLog::WriteErrorLog("Module:%d fnSetEMG [%s] [TRAY:%d]", moduleID, emgCode, m_vUnit[moduleidx]->fnGetModuleTrayState());
	}
	else
	{
		CFMSLog::WriteErrorLog("fnSetEMG [%d]", moduleID);
	}

	return TRUE;
}

FMS_STATE_CODE CFMS_SM::fnGetFMSStateForAllSystemView(INT moduleID)
{
	if( m_bWork == false )
	{
		return FMS_ST_OFF;	
	}
	
	if(moduleID >= 1 && moduleID < m_vUnit.size() + 1)
	{
		INT moduleIdx = moduleID - 1;
		return m_vUnit[moduleIdx]->fnGetFMSState();	
	}
	else
	{
		CFMSLog::WriteErrorLog("fnGetFMSStateForAllSystemView [Module ID : %d]", moduleID);	
	}

	return FMS_ST_OFF;
}

BOOL CFMS_SM::fnClearReset(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}
	
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		m_vUnit[moduleidx]->fnSetState(m_vUnit[moduleidx]->fnGet_EQUIP_ST_OFF());
		//m_vUnit[moduleidx]->fnGetFMS()->fnSetLastState(FMS_ST_OFF);
		m_vUnit[moduleidx]->fnGetFMS()->fnResetError();
	}
	else
	{
		CFMSLog::WriteErrorLog("fnRsponseSet [%d]", moduleID);
	}
	
	return TRUE;
}

BOOL CFMS_SM::fnClearColor(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}
	
	BOOL _result = FALSE;
	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		CFM_Unit* pUnit = m_vUnit[moduleidx];
		switch(pUnit->fnGetFMSState())
		{
		case FMS_ST_RED_READY:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
			break;
		case FMS_ST_TRAY_IN:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
		
			break;
		case FMS_ST_RED_END:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetFMSStateCode(FMS_ST_END);
			pUnit->fnGetState()->fnSetProcID(FNID_EXIT);
			_result = TRUE;
			break;
		case FMS_ST_BLUE_END:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_PROC);
			_result = TRUE;
			break;
		case FMS_ST_RED_TRAY_IN:
			pUnit->fnGetState()->fnWaitInit();
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
			break;
		default:
			_result = FALSE;
			break;
		}
	}

	return _result;
}

// 1. Tray 가 입고중이고 공정이 하달된 경우 Run 명령을 전송한다.
BOOL CFMS_SM::fnTrayInStateReset(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}

	BOOL _result = FALSE;

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		CFM_Unit* pUnit = m_vUnit[moduleidx];
		if(pUnit->fnGetState()->fnGetEQStateID() == EQUIP_ST_AUTO
			&& pUnit->fnGetState()->fnGetStateID() == AUTO_ST_TRAY_IN )
		{	
			if( pUnit->fnGetFMS()->fnRun() )
			{
				_result = TRUE;
			}
		}
	}

	return _result;
}

BOOL CFMS_SM::fnEndStateReset(INT moduleID)
{
	if( m_bWork == false )
	{
		return FALSE;
	}
	
	BOOL _result = FALSE;

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;
		CFM_Unit* pUnit = m_vUnit[moduleidx];
		if(pUnit->fnGetState()->fnGetEQStateID() == EQUIP_ST_AUTO
			 && pUnit->fnGetState()->fnGetStateID() == AUTO_ST_END)
		{
			pUnit->fnGetState()->fnWaitInit();
// 20200821 KSCHOI Result Data Report Location Change START
			pUnit->fnGetFMS()->fnSendToHost_S6F11C4(1, 0);
// 20200821 KSCHOI Result Data Report Location Change END
			pUnit->fnGetState()->fnSetProcID(FNID_ENTER);
			_result = TRUE;
		}
	}

	return _result;
}

BOOL CFMS_SM::VALUE_CONVERTOR( CString &strToData, int nTarget )
{
	int nSize = strToData.GetLength();

	int i=0;
	int nTo = nTarget - nSize;
	for(i=0; i<nTo; i++)
	{
		strToData += " ";
	}
	return TRUE;

}

BOOL CFMS_SM::fnSendHsmeCmd( st_HSMS_PACKET pack )
{
	char    szMsg[255];
	BYTE	nBinary = 0;
	long    lMsgId, lSize;
	int     nReturn, i, j, t, a;
	CString sAscii;
	WORD    nU2 = 0;
	CString DVNAME;
	CString DVVALUE;
	CString TOVALUE;
	CString strTempVal;
	float fcImpValue, fcATempValue;

	switch( pack.head.nStrm )
	{
	case 1:
		{
			switch( pack.head.nFunc )
			{
			case 2:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F2 data;
					ZeroMemory( &data, sizeof(st_S1F2));
					memcpy(&data, pack.data, sizeof(st_S1F2));

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.MDLN, 4 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.SOFTREV, 6 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
 					m_FmsSeq = SEQ_FMS_RUN;	//20191001 S1F101 메시지가 없어서 임시로 사용 삭제해야함.

				}
				break;

			case 6:
				{
					st_S1F6 data;
					ZeroMemory( &data, sizeof(st_S1F6));
					memcpy(&data, pack.data, sizeof(st_S1F6));

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );
					nReturn = theApp.m_XComPro.SetBinaryItem( lMsgId, &data.SFCD, 1 );

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.TRAYTYPE, 128);

					nReturn = theApp.m_XComPro.SetListItem( lMsgId, data.STAGETOTALCOUNT );

					for( i=0; i<data.STAGETOTALCOUNT; i++ )
					{
						nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );
						nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.EQ_State[i].STAGENUMBER, 5);
						nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.EQ_State[i].CTRLMODE, 1 );						
						nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.EQ_State[i].EQSTATUS, 1 );
					}

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}

					if( m_FmsSeq == SEQ_FMS_POWER_ON )
					{	
						m_FmsSeq = SEQ_FMS_RUN;
						fnSendToHost_S2F17(pack.head.nDevId, 0);
					}
				}
				break;

			case 13:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F13 data;
					ZeroMemory( &data, sizeof(st_S1F13));
					memcpy(&data, pack.data, sizeof(st_S1F13));

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.MDLN, 4 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.SOFTREV, 6 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 14:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F14 data;
					ZeroMemory( &data, sizeof(st_S1F14));
					memcpy(&data, pack.data, sizeof(st_S1F14));

					nReturn = theApp.m_XComPro.SetBinaryItem(lMsgId, &data.COMMACK, 1);
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.MDLN, 4 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.SOFTREV, 6 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;			

			case 18:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					st_S1F18 data;
					ZeroMemory( &data, sizeof(st_S1F18));
					memcpy(&data, pack.data, sizeof(st_S1F18));

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId,data.STAGENUMBER, 5);
					nReturn = theApp.m_XComPro.SetBinaryItem( lMsgId,&data.ACK, 1);

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 102:
				{
// 20200526 KSCHOI Change S1F102 Message For LineNo START
/*
					USHORT nLineCnt = 1;

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte);

					st_S1F102 data;
					ZeroMemory( &data, sizeof(st_S1F102));
					memcpy(&data, pack.data, sizeof(st_S1F102));
					
					int nStageCnt;
					if(data.STAGETOTALCOUNT>0)
					{
						nStageCnt = data.STAGETOTALCOUNT;
					}
					else
					{
						nStageCnt = m_vUnit.size();
					}
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, nStageCnt);
					for( i=0; i<nStageCnt; i++ )
					{
						nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4);
						nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.EQ_State[i].STAGENUMBER, 5);
						CString strModuleName;
						strModuleName.Format("%s",data.EQ_State[i].STAGENUMBER);


						nReturn = theApp.m_XComPro.SetU2Item(lMsgId, &data.EQ_State[i].CTRLMODE, 1);

						nReturn = theApp.m_XComPro.SetU2Item(lMsgId, &data.EQ_State[i].EQSTATUS, 1);
						nReturn = theApp.m_XComPro.SetListItem(lMsgId, nLineCnt);
						for(j=0; j<nLineCnt; j++)
						{
							nReturn = theApp.m_XComPro.SetAsciiItem(lMsgId, data.EQ_State[i].LINENO, 1);
						}						
					}
			
					
					m_FmsSeq = SEQ_FMS_RUN;

					if( (nReturn = theApp.m_XComPro.Send(lMsgId)) == 0)
					{
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else
					{
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				} 
*/
					int nLineNo = 0;
					int j;
					CString strStopper;

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte);
					//nReturn = theApp.m_XComPro.SetListItem( lMsgId, nStageCnt);

					st_S1F102 data;
					ZeroMemory( &data, sizeof(st_S1F102));
					memcpy(&data, pack.data, sizeof(st_S1F102));

					nReturn = theApp.m_XComPro.SetListItem( lMsgId, data.STAGETOTALCOUNT);

					for(i = 1; i < 5; i++)
					{
						strStopper.Format("Stopper%d", i);
						if( AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , strStopper, "370") == "370" )
						{
							nLineNo = nLineNo + 1;
						}
						else if(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , strStopper, "250") == "250")
						{
							nLineNo = nLineNo + 1;
						}
					}

					for( i = 0; i < data.STAGETOTALCOUNT; i++ )
					{
						nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4);
						nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.EQ_State[i].STAGENUMBER, 5);

						nReturn = theApp.m_XComPro.SetU2Item(lMsgId, &data.EQ_State[i].CTRLMODE, 1);

						nReturn = theApp.m_XComPro.SetU2Item(lMsgId, &data.EQ_State[i].EQSTATUS, 1);
						nReturn = theApp.m_XComPro.SetListItem(lMsgId, nLineNo);

						for(j = 1; j < 5; j++)
						{
							strStopper.Format("Stopper%d", j);
							if( AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , strStopper, "370") == "370" )
							{
								nReturn = theApp.m_XComPro.SetAsciiItem(lMsgId, "E600", 4);
							}
							else if(AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , strStopper, "250") == "250")
							{
								nReturn = theApp.m_XComPro.SetAsciiItem(lMsgId, "E623", 4);
							}
						}
					}

					if( (nReturn = theApp.m_XComPro.Send(lMsgId)) == 0)
					{
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else
					{
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );                  
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
// 20200526 KSCHOI Change S1F102 Message For LineNo END
				break;
			}
		}
	case 2:
		{
			switch( pack.head.nFunc )
			{
			case 17:
				{
					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 32:
				{
					st_S2F32 data;
					ZeroMemory( &data, sizeof(st_S2F32));
					memcpy(&data, pack.data, sizeof(st_S2F32));

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetBinaryItem( lMsgId, &data.ACK, 1 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 42:
				{
					st_S2F42 data;
					ZeroMemory( &data, sizeof(st_S2F42));
					memcpy(&data, pack.data, sizeof(st_S2F42));					

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
					nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &data.ACK, 1 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);


					//nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					//TOVALUE.Format("%s", data.TRAYID_1);
					//TOVALUE.Trim();
					//VALUE_CONVERTOR(TOVALUE, 10);
					//nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
					//nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

					//nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					//TOVALUE.Format("%s", data.TRAYID_2);
					//TOVALUE.Trim();
					//VALUE_CONVERTOR(TOVALUE, 10);
					//nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
					//nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;

			case 2:
				break;
			}

		}
		break;

	case 5:
		{
			switch( pack.head.nFunc )
			{
			case 1:
				{
					st_S5F1 data;
					ZeroMemory( &data, sizeof(st_S5F1));
					memcpy(&data, pack.data, sizeof(st_S5F1));

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);
					nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.ALST, 1 );
					nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.ALID, 1 );
				
// 					CString strData;
// 					strData.Format("%s", data.ALTX);
// 					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, "", 0);

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;
// 20201118 KSCHOI Add Break Fire Report START
			case 3:
				{
					st_S5F3 data;
					ZeroMemory( &data, sizeof(st_S5F3));
					memcpy(&data, pack.data, sizeof(st_S5F3));

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

					nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.STATUS, 1 );
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					TOVALUE.Format("%s", data.TRAYID_1);
					TOVALUE.Trim();
					VALUE_CONVERTOR(TOVALUE, 10);
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

					nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

					nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

					TOVALUE.Format("%s", data.TRAYID_2);
					TOVALUE.Trim();
					VALUE_CONVERTOR(TOVALUE, 10);
					nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

					nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;
// 20201118 KSCHOI Add Break Fire Report END
			case 102:
				{
					st_S5F102 data;
					ZeroMemory( &data, sizeof(st_S5F102));
					memcpy(&data, pack.data, sizeof(st_S5F102));

					nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );
					nReturn = theApp.m_XComPro.SetBinaryItem( lMsgId, &data.ACK, 1 );

					if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
						sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
						CFMSLog::WriteLog( szMsg );
					}
					else {
						sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
						CFMSLog::WriteLog( szMsg );
						return FALSE;
					}
				}
				break;
			default:
				{

				}
			}
		}
		break;
	
	case 6:
		{
			switch( pack.head.nFunc )
			{
			case 11:
				{
					switch( pack.head.nId )
					{
					case 1:					
						{
							st_S6F11C1 data;
							ZeroMemory( &data, sizeof(st_S6F11C1));
							memcpy(&data, pack.data, sizeof(st_S6F11C1));					

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}							
						}
						break;

					case 4:
						{

							st_S6F11C4 data;
							ZeroMemory( &data, sizeof(st_S6F11C4));
							memcpy(&data, pack.data, sizeof(st_S6F11C4));

							if( data.fmsERCode == FMS_ER_NONE )
							{
								st_Result_Step_Result *sStepResult;
								st_RESULT_FILE sResultFile;								

								INT stageidx = fnGetStageIdx(atoi(data.STAGENUMBER));

								sResultFile = m_vUnit[stageidx]->fnGetFMS()->fnGetResultFile();
								sStepResult = m_vUnit[stageidx]->fnGetFMS()->fnGetStepResult();

								BYTE nTrayCellCnt = 0;
								if( sResultFile.TRAYTYPE == 0 )		// 0:양방향, 1:단방향
								{
									nTrayCellCnt = 36;
								}
								else
								{
									nTrayCellCnt = 72;
								}

								nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

								nReturn = theApp.m_XComPro.SetListItem( lMsgId, 10 );

								WORD CEID;
								CEID = pack.head.nId;
								nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );								

								// nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &pack.head.nId, 1 );
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);
								nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &sResultFile.RESULTTYPE, 1 );

								TOVALUE.Format("%s", sResultFile.PROCESSTYPE);
								TOVALUE.Trim();
								VALUE_CONVERTOR(TOVALUE, 4);
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 4);

								// nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.PROCESSTYPE, 1 );
								
								nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.PROCESSNO, 1 );

								TOVALUE.Format("%s", sResultFile.BACHNO);
								TOVALUE.Trim();
								VALUE_CONVERTOR(TOVALUE, 20);
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 20 );

								// nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &sResultFile.CHANNELINSERTTYPE, 1 );
								nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &nTrayCellCnt, 1 );
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, sResultFile.STIME, 14 );
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, sResultFile.ETIME, 14 );

								if( sResultFile.nTraykind == 2 )
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
								}
								else
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 1 );
								}								
															
								nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4 );

								TOVALUE.Format("%s", sResultFile.TRAYID_1);
								VALUE_CONVERTOR(TOVALUE, 10);
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10 );
								
								nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.TRAYPOSITION_1, 1 );
								nReturn = theApp.m_XComPro.SetListItem( lMsgId, nTrayCellCnt );							

								for( i=0; i<nTrayCellCnt; i++ )
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

									TOVALUE.Format("%s", sResultFile.arCell_ID[i].Cell_ID);
									TOVALUE.Trim();
									VALUE_CONVERTOR(TOVALUE, 20);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 20 );

									TOVALUE.Format("%4s", sResultFile.arCellInfo[i].Cell_Info);
									TOVALUE.Trim();
									VALUE_CONVERTOR(TOVALUE, 4);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 4 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									DVNAME.Format("IV");
									// VALUE_CONVERTOR(DVNAME, 20);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
									DVVALUE.Format("%5.1f", sResultFile.sContactResult[i].Initial_Vol);
									DVVALUE.Trim();
									// VALUE_CONVERTOR(DVVALUE, 10);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									// DVNAME.Format("Contact Voltage");
									DVNAME.Format("CV");
									// VALUE_CONVERTOR(DVNAME, 20);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
									DVVALUE.Format("%5.1f", sResultFile.sContactResult[i].Contact_Vol);
									DVVALUE.Trim();
									// VALUE_CONVERTOR(DVVALUE, 10);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									// DVNAME.Format("Contact Current");
									DVNAME.Format("CC");
									// VALUE_CONVERTOR(DVNAME, 20);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
									DVVALUE.Format("%5.1f", sResultFile.sContactResult[i].Contact_Cur);
									DVVALUE.Trim();
									// VALUE_CONVERTOR(DVVALUE, 10);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									// DVNAME.Format("Delta OCV");
									DVNAME.Format("DV");
									// VALUE_CONVERTOR(DVNAME, 20);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
									DVVALUE.Format("%6.1f", sResultFile.sContactResult[i].Delta_OCV);
									DVVALUE.Trim();
									// VALUE_CONVERTOR(DVVALUE, 10);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );
								}							

								//sResultFile.TotalStep = 1;
								nReturn = theApp.m_XComPro.SetListItem( lMsgId, sResultFile.TotalStep );

								for( i=0; i<sResultFile.TotalStep; i++ )
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

									nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sStepResult[i].StepNo, 1 );
									nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &sStepResult[i].Action, 1 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, nTrayCellCnt );

									for( j=0; j<nTrayCellCnt; j++ )
									{										
										//nReturn = theApp.m_XComPro.SetListItem( lMsgId, 27 );
										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 44 ); //19-01-14 엄륭 Data 추가로 리스트 변경

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("OCV");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
										DVVALUE.Format("%5.1f", sStepResult[i].Voltage[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CA");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%6.0f", sStepResult[i].Capacity[j]);
										//DVVALUE.Format("%d", (int)sStepResult[i].Capacity[j]); //19-01-21 엄륭 자릿수 변경
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										for( t=0; t<16; t++ )
										{
											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											DVNAME.Format("CTT%d", t+1);
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
											DVVALUE.Format("%4.1f", sStepResult[i].CheckTimeTemp[j][t]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );
										}

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CC");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%6.0f", sStepResult[i].CutCurrent[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("SET");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%6.0f", sStepResult[i].StepEndTime[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CCT");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%6.0f", sStepResult[i].CC_Time[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CCC");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%6.0f", sStepResult[i].CC_Capacity[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CVT");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%6.0f", sStepResult[i].CV_Time[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CVC");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%6.0f", sStepResult[i].CV_Capacity[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("DC");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%3.2f", sStepResult[i].Delta_Capacity[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("STV");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%5.1f", sStepResult[i].StartVoltage[j]);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CCV");
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%5.1f", sStepResult[i].ChecktimeVoltage[j]);
										DVVALUE.Trim();
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );


										//19-01-09 엄륭 FMS 사양 추가 관련
										for( a=0; a<16; a++ )
										{
											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											DVNAME.Format("ATT%d", a+1);
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );	
											fcATempValue = floor(10.* sStepResult[i].AverageTimeTemp[j][a]) / 10;
											DVVALUE.Format("%4.1f", fcATempValue);
											//DVVALUE.Format("%4.1f", sStepResult[i].AverageTimeTemp[j][a]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );
										}

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("DCIR");
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );	
										fcImpValue = floor(1000.*sStepResult[i].DCIR_Measure[j]) / 1000;  //19-01-17 엄륭 소수점 3자리 버림
										DVVALUE.Format("%5.3f", fcImpValue);
										DVVALUE.Trim();
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );
										
									}
								}

								if( sResultFile.nTraykind == 2 )
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4 );

									TOVALUE.Format("%s", sResultFile.TRAYID_2);
									TOVALUE.Trim();
									VALUE_CONVERTOR(TOVALUE, 10);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10 );

									nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.TRAYPOSITION_2, 1 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, nTrayCellCnt );

									for( i=0; i<nTrayCellCnt; i++ )
									{
										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

										TOVALUE.Format("%s", sResultFile.arCell_ID[i+MAX_TRAYCELL_COUNT].Cell_ID);
										TOVALUE.Trim();
										VALUE_CONVERTOR(TOVALUE, 20);										
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 20 );

										TOVALUE.Format("%4s", sResultFile.arCellInfo[i+MAX_TRAYCELL_COUNT].Cell_Info);
										TOVALUE.Trim();
										VALUE_CONVERTOR(TOVALUE, 4);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 4 );									

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("IV");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%5.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Initial_Vol);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CV");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
										DVVALUE.Format("%5.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Contact_Vol);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CC");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
										DVVALUE.Format("%5.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Contact_Cur);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("DV");
										// VALUE_CONVERTOR(DVNAME, 20);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
										DVVALUE.Format("%6.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Delta_OCV);
										DVVALUE.Trim();
										// VALUE_CONVERTOR(DVVALUE, 10);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );									
									}								

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, sResultFile.TotalStep );

									for( i=0; i<sResultFile.TotalStep; i++ )
									{
										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

										nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sStepResult[i].StepNo, 1 );
										nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &sStepResult[i].Action, 1 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, nTrayCellCnt );

										for( j=0; j<nTrayCellCnt; j++ )
										{
											//nReturn = theApp.m_XComPro.SetListItem( lMsgId, 27 );
											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 44 ); //19-01-14 엄륭 Data 추가로 리스트 변경

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("Voltage");
											DVNAME.Format("OCV");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
											DVVALUE.Format("%5.1f", sStepResult[i].Voltage[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("Capacity");
											DVNAME.Format("CA");
											// VALUE_CONVERTOR(DVNAME, 20);
											
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
											//DVVALUE.Format("%6.0f", sStepResult[i].Capacity[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Format("%d", (int)sStepResult[i].Capacity[j+MAX_TRAYCELL_COUNT]); //19-01-21엄륭 자릿수 변경
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											for( t=0; t<16; t++ )
											{
												nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
												DVNAME.Format("CTT%d", t+1);
												// VALUE_CONVERTOR(DVNAME, 20);
												nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
												
												//DVVALUE.Format("%4.1f", sStepResult[i].CheckTimeTemp[j][t]);  //KSH 온도데이터
												DVVALUE.Format("%4.1f", sStepResult[i].CheckTimeTemp[j+MAX_TRAYCELL_COUNT][t]);
												DVVALUE.Trim();
												// VALUE_CONVERTOR(DVVALUE, 10);
												nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );
											}

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("Cut Current");
											DVNAME.Format("CC");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
											
											DVVALUE.Format("%6.0f", sStepResult[i].CutCurrent[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("Step End Time");
											DVNAME.Format("SET");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									

											DVVALUE.Format("%6.0f", sStepResult[i].StepEndTime[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("CC Time");
											DVNAME.Format("CCT");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									

											DVVALUE.Format("%6.0f", sStepResult[i].CC_Time[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											//DVNAME.Format("CC Capacity");
											DVNAME.Format("CCC");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
											
											DVVALUE.Format("%6.0f", sStepResult[i].CC_Capacity[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("CV Time");
											DVNAME.Format("CVT");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									

											DVVALUE.Format("%6.0f", sStepResult[i].CV_Time[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("CV Capacity");
											DVNAME.Format("CVC");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									

											DVVALUE.Format("%6.0f", sStepResult[i].CV_Capacity[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("Delta Capacity");
											DVNAME.Format("DC");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );

											DVVALUE.Format("%3.2f", sStepResult[i].Delta_Capacity[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											// DVNAME.Format("Start Voltage");
											DVNAME.Format("STV");
											// VALUE_CONVERTOR(DVNAME, 20);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
											
											DVVALUE.Format("%5.1f", sStepResult[i].StartVoltage[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											// VALUE_CONVERTOR(DVVALUE, 10);
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											DVNAME.Format("CCV");
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
											DVVALUE.Format("%5.1f", sStepResult[i].ChecktimeVoltage[j+MAX_TRAYCELL_COUNT]);
											DVVALUE.Trim();
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

											//19-01-09 엄륭 FMS 사양 추가 관련
											for( a=0; a<16; a++ )
											{
												nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
												DVNAME.Format("ATT%d", a+1);
												// VALUE_CONVERTOR(DVNAME, 20);
												nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
												//fcATempValue = floor(10.* sStepResult[i].AverageTimeTemp[j][a]) / 10;  //KSH 온도데이터
												fcATempValue = floor(10.* sStepResult[i].AverageTimeTemp[j+MAX_TRAYCELL_COUNT][a]) / 10;
												DVVALUE.Format("%4.1f", fcATempValue);
												//DVVALUE.Format("%4.1f", sStepResult[i].AverageTimeTemp[j][a]);
												DVVALUE.Trim();
												// VALUE_CONVERTOR(DVVALUE, 10);
												nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );
											}

											nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
											DVNAME.Format("DCIR");
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
											fcImpValue = floor(1000.*sStepResult[i].DCIR_Measure[j+MAX_TRAYCELL_COUNT]) / 1000;  //19-01-17 엄륭 소수점 3자리 버림
											DVVALUE.Format("%5.3f", fcImpValue);
											DVVALUE.Trim();
											nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );
											
											
										}
									}
								}

								if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
									sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
									CFMSLog::WriteLog( szMsg );
								}
								else {
									sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
									CFMSLog::WriteLog( szMsg );
									return FALSE;
								}
							}
						}
						break;

					case 5:									
						{
							st_S6F11C5 data;
							ZeroMemory( &data, sizeof(st_S6F11C5));
							memcpy(&data, pack.data, sizeof(st_S6F11C5));					

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 6:
						{
							st_S6F11C6 data;
							ZeroMemory( &data, sizeof(st_S6F11C6));
							memcpy(&data, pack.data, sizeof(st_S6F11C6));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 7:					
						{
							st_S6F11C7 data;
							ZeroMemory( &data, sizeof(st_S6F11C7));
							memcpy(&data, pack.data, sizeof(st_S6F11C7));

							if( data.fmsERCode == FMS_ER_NONE )
							{
								st_RESULT_FILE sResultFile;

								INT stageidx = fnGetStageIdx(atoi(data.STAGENUMBER));

								sResultFile = m_vUnit[stageidx]->fnGetFMS()->fnGetResultFile();

								BYTE nTrayCellCnt = 0;
								if( sResultFile.TRAYTYPE == 0 )		// 0:양방향, 1:단방향
								{
									nTrayCellCnt = 36;
								}
								else
								{
									nTrayCellCnt = 72;
								}
								
								nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

								nReturn = theApp.m_XComPro.SetListItem( lMsgId, 10 );

								WORD CEID;
								CEID = pack.head.nId;
								nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );

								// nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &pack.head.nId, 1 );
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);
								nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &sResultFile.RESULTTYPE, 1 );

								TOVALUE.Format("%s", sResultFile.PROCESSTYPE);
								TOVALUE.Trim();
								VALUE_CONVERTOR(TOVALUE, 4);
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 4);
								// nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.PROCESSTYPE, 1 );

								nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.PROCESSNO, 1 );

								TOVALUE.Format("%s", sResultFile.BACHNO);
								TOVALUE.Trim();
								VALUE_CONVERTOR(TOVALUE, 20);
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 20 );

								nReturn = theApp.m_XComPro.SetU1Item( lMsgId, &nTrayCellCnt, 1 );
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, sResultFile.STIME, 14 );
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, sResultFile.ETIME, 14 );

								if( sResultFile.nTraykind == 2 )
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
								}
								else
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 1 );
								}								

								nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

								TOVALUE.Format("%s", sResultFile.TRAYID_1);
								TOVALUE.Trim();
								VALUE_CONVERTOR(TOVALUE, 10);
								nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10 );

								nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.TRAYPOSITION_1, 1 );

								nReturn = theApp.m_XComPro.SetListItem( lMsgId, nTrayCellCnt );								

								for( i=0; i<nTrayCellCnt; i++ )
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );
									
									TOVALUE.Format("%s", sResultFile.arCell_ID[i].Cell_ID);
									TOVALUE.Trim();
									VALUE_CONVERTOR(TOVALUE, 20);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 20 );

									TOVALUE.Format("%4s", sResultFile.arCellInfo[i].Cell_Info);
									TOVALUE.Trim();
									VALUE_CONVERTOR(TOVALUE, 4);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 4 );									

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									DVNAME.Format("IV");
									VALUE_CONVERTOR( DVNAME, 20 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
									DVVALUE.Format("%5.1f", sResultFile.sContactResult[i].Initial_Vol);
									DVVALUE.Trim();
									VALUE_CONVERTOR( DVVALUE, 10 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									DVNAME.Format("CV");
									VALUE_CONVERTOR( DVNAME, 20 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
									DVVALUE.Format("%5.1f", sResultFile.sContactResult[i].Contact_Vol);
									DVVALUE.Trim();
									VALUE_CONVERTOR( DVVALUE, 10 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									DVNAME.Format("CC");
									VALUE_CONVERTOR( DVNAME, 20 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
									DVVALUE.Format("%5.1f", sResultFile.sContactResult[i].Contact_Cur);
									DVVALUE.Trim();
									VALUE_CONVERTOR( DVVALUE, 10 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
									DVNAME.Format("DV");
									VALUE_CONVERTOR( DVNAME, 20 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
									DVVALUE.Format("%6.1f", sResultFile.sContactResult[i].Delta_OCV);
									DVVALUE.Trim();
									VALUE_CONVERTOR( DVVALUE, 10 );
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );									
								}

								if( sResultFile.nTraykind == 2 )
								{
									nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

									TOVALUE.Format("%s", sResultFile.TRAYID_2);
									TOVALUE.Trim();
									VALUE_CONVERTOR(TOVALUE, 10);
									nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10 );
									
									nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &sResultFile.TRAYPOSITION_2, 1 );

									nReturn = theApp.m_XComPro.SetListItem( lMsgId, nTrayCellCnt );									

									for( i=0; i<nTrayCellCnt; i++ )
									{
										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

										TOVALUE.Format("%s", sResultFile.arCell_ID[i+MAX_TRAYCELL_COUNT].Cell_ID);
										TOVALUE.Trim();
										VALUE_CONVERTOR(TOVALUE, 20);
										
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 20 );

										TOVALUE.Format("%4s", sResultFile.arCellInfo[i+MAX_TRAYCELL_COUNT].Cell_Info);
										TOVALUE.Trim();																				
										VALUE_CONVERTOR(TOVALUE, 4);
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 4 );									

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 4 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("IV");
										DVNAME.Trim();
										VALUE_CONVERTOR( DVNAME, 20 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );									
										DVVALUE.Format("%5.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Initial_Vol);
										DVVALUE.Trim();
										VALUE_CONVERTOR( DVVALUE, 10 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CV");
										DVNAME.Trim();
										VALUE_CONVERTOR( DVNAME, 20 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
										DVVALUE.Format("%5.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Contact_Vol);
										DVVALUE.Trim();
										VALUE_CONVERTOR( DVVALUE, 10 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("CC");
										DVNAME.Trim();
										VALUE_CONVERTOR( DVNAME, 20 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
										DVVALUE.Format("%5.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Contact_Cur);
										DVVALUE.Trim();
										VALUE_CONVERTOR( DVVALUE, 10 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );

										nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
										DVNAME.Format("DV");
										DVNAME.Trim();
										VALUE_CONVERTOR( DVNAME, 20 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVNAME.GetBuffer(), 20 );
										DVVALUE.Format("%6.1f", sResultFile.sContactResult[i+MAX_TRAYCELL_COUNT].Delta_OCV);
										DVVALUE.Trim();
										VALUE_CONVERTOR( DVVALUE, 10 );
										nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, DVVALUE.GetBuffer(), 10 );										
									}
								}						

								if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
									sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
									CFMSLog::WriteLog( szMsg );
								}
								else {
									sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
									CFMSLog::WriteLog( szMsg );
									return FALSE;
								}
							}
						}
						break;				

					case 11:
						{
							st_S6F11C11 data;
							ZeroMemory( &data, sizeof(st_S6F11C11));
							memcpy(&data, pack.data, sizeof(st_S6F11C11));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							WORD CEID;
							CEID = pack.head.nId;

							nReturn = theApp.m_XComPro.SetListItem(lMsgId, 4);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.CTRLMODE, 1 );
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.EQSTATUS, 1 );			

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 12:					
						{
							st_S6F11C1 data;
							ZeroMemory( &data, sizeof(st_S6F11C1));
							memcpy(&data, pack.data, sizeof(st_S6F11C1));					

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);
							
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );
							
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							
							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 13:
						{
							st_S6F11C13 data;
							ZeroMemory( &data, sizeof(st_S6F11C13));
							memcpy(&data, pack.data, sizeof(st_S6F11C13));					

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 14:
						{
							st_S6F11C14 data;
							ZeroMemory( &data, sizeof(st_S6F11C14));
							memcpy(&data, pack.data, sizeof(st_S6F11C14));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 15:
						{
							st_S6F11C15 data;
							ZeroMemory( &data, sizeof(st_S6F11C15));
							memcpy(&data, pack.data, sizeof(st_S6F11C15));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 16: //19-01-24 엄륭 MES 사양 변경 -교정입고예약확인 보고
						{
							st_S6F11C16 data;
							ZeroMemory( &data, sizeof(st_S6F11C16));
							memcpy(&data, pack.data, sizeof(st_S6F11C16));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}

						}
						break;

					case 17://19-01-24 엄륭 헝가리 MES 사양 추가 -교정시작보고
						{
							st_S6F11C17 data;
							ZeroMemory( &data, sizeof(st_S6F11C17));
							memcpy(&data, pack.data, sizeof(st_S6F11C17));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}

						}
						break;

					case 18://19-01-24 엄륭 헝가리 MES 사양 추가 -교정완료보고
						{
							st_S6F11C18 data;
							ZeroMemory( &data, sizeof(st_S6F11C18));
							memcpy(&data, pack.data, sizeof(st_S6F11C18));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else 
							{
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}

						}
						break;

					case 19://19-01-24 엄륭 헝가리 MES 사양 추가 -Tray배출요청
						{
							st_S6F11C19 data;
							ZeroMemory( &data, sizeof(st_S6F11C19));
							memcpy(&data, pack.data, sizeof(st_S6F11C19));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else 
							{
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}

						}
						break;

					case 100:
						{
							st_S6F11C100 data;
							ZeroMemory( &data, sizeof(st_S6F11C100));
							memcpy(&data, pack.data, sizeof(st_S6F11C100));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							WORD CEID;
							CEID = pack.head.nId;

							nReturn = theApp.m_XComPro.SetListItem(lMsgId, 4);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.CTRLMODE, 1 );
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.EQSTATUS, 1 );			

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;

					case 200:
						{
							st_S6F11C200 data;
							ZeroMemory( &data, sizeof(st_S6F11C200));
							memcpy(&data, pack.data, sizeof(st_S6F11C200));

							nReturn = theApp.m_XComPro.MakeSecsMsg( &lMsgId, pack.head.nDevId, pack.head.nStrm, pack.head.nFunc, pack.head.lSysByte );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 3 );

							WORD CEID;
							CEID = pack.head.nId;
							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &CEID, 1 );
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, data.STAGENUMBER, 5);

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );
							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_1);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_1, 1 );

							nReturn = theApp.m_XComPro.SetListItem( lMsgId, 2 );

							TOVALUE.Format("%s", data.TRAYID_2);
							TOVALUE.Trim();
							VALUE_CONVERTOR(TOVALUE, 10);
							nReturn = theApp.m_XComPro.SetAsciiItem( lMsgId, TOVALUE, 10);

							nReturn = theApp.m_XComPro.SetU2Item( lMsgId, &data.TRAYPOSITION_2, 1 );

							if( (nReturn = theApp.m_XComPro.Send( lMsgId )) == 0 ) {
								sprintf( szMsg, "Reply S%dF%dC%d", pack.head.nStrm, pack.head.nFunc, pack.head.nId );
								CFMSLog::WriteLog( szMsg );
							}
							else {
								sprintf( szMsg, "Fail to reply S%dF%dC%d = (%d)", pack.head.nStrm, pack.head.nFunc, pack.head.nId, nReturn );						
								CFMSLog::WriteLog( szMsg );
								return FALSE;
							}
						}
						break;
					}
				}
				break;
			}
		}
		break;
	}

	return TRUE;
}




VOID CFMS_SM::FMS_SENDThreadCallback(VOID)
{
	BOOL bMsgSendChk = FALSE;
	
	while (TRUE)
	{
		SetEvent(mObjDelete);
		DWORD Result = WaitForSingleObject(mKeepThreadDestroyEvent, 100);

		if (Result == WAIT_OBJECT_0)
			return;

		// 1. 전송할 CMD 데이터가 없으면 heartbeat 체크 신호를 ON 한다.
		// 2. Heartbeat 신호를 전송한 후
		if( m_bRunChk == FALSE && m_bSECSCommunicationOK == TRUE )
		{
			m_bRunChk = TRUE;			

			if( m_FmsSeq == SEQ_FMS_NONE || m_FmsSeq == SEQ_FMS_WAIT )
			{

			}
			else
			{
				switch( m_FmsSeq )
				{
					case SEQ_FMS_POWER_ON:
						{
							while( !theApp.m_HSMS_P_SendQ.GetIsEmpty() )
							{
								st_HSMS_PACKET pack;
								theApp.m_HSMS_P_SendQ.Pop(pack);

								if( fnSendHsmeCmd(pack) == TRUE )
								{
									// 1. Log
								}
								else
								{
									// 
								}
							}
						}
						break;

					case SEQ_FMS_RUN:
						{
							if( !theApp.m_HSMS_SendQ.GetIsEmpty() )
							{
								SetHeartbeatClear();

								while( !theApp.m_HSMS_SendQ.GetIsEmpty() )
								{
									st_HSMS_PACKET pack;
									theApp.m_HSMS_SendQ.Pop(pack);

									if( fnSendHsmeCmd(pack) == TRUE )
									{
										// 1. Log
									}
									else
									{
										// 
									}
								}
							}
							else
							{
								if( ChkHeartbeat() == TRUE )
								{

								}								
							}							
						}
						break;

					default:
						{

						}
						break;
				}
			}				
		}

		m_bRunChk = FALSE;
	}
}
BOOL CFMS_SM::fnInitStageState(INT moduleID)
{
	int nModuleIdx =  fnGetModuleIdx(moduleID);
	CFMS* fms = m_vUnit[nModuleIdx]->fnGetFMS();
	fms->m_LastInit = FALSE;
	CString strTemp;
	strTemp.Format("StageLastState_%02d", moduleID);
	fms->m_LastState = AfxGetApp()->GetProfileInt(FMS_REG, strTemp, 1);
	if(fms->m_LastState == 0)
	{
		fms->m_LastState = 1;
	}
	strTemp.Format("StageLastMode_%02d", moduleID);
	fms->m_LastMode = AfxGetApp()->GetProfileInt(FMS_REG, strTemp, 0);
	return TRUE;
}
//2020 12 06 BJY 기존 선언만 되어있던 함수 구현
BOOL CFMS_SM::fnRunCMD( INT moduleID )
{
	if( m_bWork == false )
	{
		return FALSE;	
	}

	BOOL _result = FALSE;

	if(moduleID > 0 && moduleID <= m_vUnit.size())
	{
		INT moduleidx = moduleID - 1;

		if(m_vUnit[moduleidx] != NULL)
			m_vUnit[moduleidx]->fnGetFMS()->fnSendToHost_S6F11C1(1, 0);

		_result = TRUE;
	}
	else
	{
		CFMSLog::WriteErrorLog("fnRunCMD [%d]", moduleID);	
	}

	return _result;
}