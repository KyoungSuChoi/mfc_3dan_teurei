// TemperatureSensorDataDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "MainFrm.h"
#include "TemperatureSensorDataDlg.h"


// CTemperatureSensorDataDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTemperatureSensorDataDlg, CDialog)

CTemperatureSensorDataDlg::CTemperatureSensorDataDlg(CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleID)
	: CDialog(IDD, pParent)
{
	m_pDoc			= pDoc;
	m_nModuleID		= iTargetModuleID;	
	m_bWindowSize	= TRUE;

	m_csTemperatureStringFormat = this->MakeTemperatureStringFormat(this->CalcTemperatureChracterByLocale());
}

CTemperatureSensorDataDlg::~CTemperatureSensorDataDlg()
{
	this->DestroyWindow();
}

void CTemperatureSensorDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TEMPERATURE_GRID, m_wndTempGrid);
	DDX_Control(pDX, IDC_STATIC_TEMPERATURE_SENSOR_PICTURE, m_cTemperaturePicture);
	DDX_Control(pDX, IDC_BTN_PICTURE, m_cPictureBtn);
}


BEGIN_MESSAGE_MAP(CTemperatureSensorDataDlg, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_PICTURE, OnPictureButton)
END_MESSAGE_MAP()

void CTemperatureSensorDataDlg::OnPictureButton()
{

	HBITMAP hBmp =(HBITMAP)::LoadImage(AfxGetInstanceHandle(),"C:\\Program Files (x86)\\PNE CTS\\IMG\\temper_sensor.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	m_cTemperaturePicture.SetBitmap(hBmp);
	this->ResizeWindow(!m_bWindowSize);
}

void CTemperatureSensorDataDlg::ResizeWindow(BOOL bUp/*=TRUE*/)
{
	CRect rect;
	CRect pictureRect;	

	this->GetWindowRect(&rect);
	this->m_cTemperaturePicture.GetWindowRect(&pictureRect);

	if(bUp)
	{
		rect.BottomRight().x += (pictureRect.BottomRight().x - pictureRect.TopLeft().x);
		this->m_bWindowSize = TRUE;
	}
	else
	{
		rect.BottomRight().x -= (pictureRect.BottomRight().x - pictureRect.TopLeft().x);
		this->m_bWindowSize = FALSE;
	}

	this->MoveWindow(&rect);
}

BOOL CTemperatureSensorDataDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cPictureBtn.SetFontStyle(BTN_FONT_SIZE,1);
	m_cPictureBtn.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);
	
	this->InitializeGrid();
	this->ResizeWindow(FALSE);

	this->SetWindowPos(this->GetParent(), 1623, 278,0, 0, SWP_NOSIZE);

	this->SetTimer(TIMER_TEMPERATURE_REFRESH_ID, TIMER_TEMPERATURE_REFRESH_INTERVAL, NULL);

	return TRUE;
}

void CTemperatureSensorDataDlg::InitializeGrid()
{
	m_wndTempGrid.SubclassDlgItem(IDC_TEMP_GRID, this);
	m_wndTempGrid.m_bSameColSize  = FALSE;
	m_wndTempGrid.m_bSameRowSize  = FALSE;
	m_wndTempGrid.m_bCustomWidth  = TRUE;

	m_wndTempGrid.Initialize();
	
	this->InitFirstRow();
	this->InitDefaultRows();
	this->UpdateGridData();
}

BOOL CTemperatureSensorDataDlg::InitFirstRow()
{
	try
	{
		BOOL bLock = m_wndTempGrid.LockUpdate();	
		m_wndTempGrid.SetColCount(3);
		m_wndTempGrid.m_BackColor	= RGB(255,255,255);

		m_wndTempGrid.SetValueRange(CGXRange(0,1), "Unit");
		m_wndTempGrid.SetValueRange(CGXRange(0,2), "Sensor");
		m_wndTempGrid.SetValueRange(CGXRange(0,3), "Current");

		CRect rectGrid;
		float width;
		m_wndTempGrid.GetClientRect(rectGrid);
		width = (float)rectGrid.Width()/100.0f;

		m_wndTempGrid.m_nWidth[1]	= int(width* 20.0f); // ID
		m_wndTempGrid.m_nWidth[2]	= int(width* 20.0f); // Name
		m_wndTempGrid.m_nWidth[3]	= int(width* 40.0f); // Temperature

		m_wndTempGrid.SetDefaultRowHeight(20);
		m_wndTempGrid.GetParam()->GetProperties()->SetDisplayHorzLines(TRUE);
		m_wndTempGrid.GetParam()->GetProperties()->SetDisplayVertLines(TRUE);	

		m_wndTempGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
		m_wndTempGrid.SetScrollBarMode(SB_HORZ, gxnDisabled);
		m_wndTempGrid.EnableCellTips();

		m_wndTempGrid.LockUpdate(bLock);
	}	
	catch (CException* e)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CTemperatureSensorDataDlg::InitDefaultRows()
{
	try
	{
		BOOL		bLock = m_wndTempGrid.LockUpdate();
		LONG		nRowCount = 0;
		const LONG	nGridRowCount = m_wndTempGrid.GetRowCount();
		CFormModule *pModule	= NULL;	
		_MAPPING_DATA *pMapData = NULL;

		if(nGridRowCount > 0)
			m_wndTempGrid.RemoveRows(1, nGridRowCount);

		pModule = m_pDoc->GetModuleInfo(m_nModuleID);

		for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
		{
			pMapData = pModule->GetSensorMap(0, i);

			if(pMapData != NULL && pMapData->nChannelNo == 1) // ONLY TEMPERATURE
			{
				m_wndTempGrid.InsertRows(nRowCount+1, 1);

// 				m_wndTempGrid.SetStyleRange(CGXRange(nRowCount+1, 3), 
// 					CGXStyle()
// 					.SetControl(GX_IDS_CTRL_PROGRESS)
// 					.SetValue(0L)
// 					.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0.0"))
// 					.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("50.0"))
// 					.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, _T("%.1f"))		// sprintf formatting for value  // 20201026 엄륭 프로그래스바만 나오게 하고 다음 열에 온도 표시
// 					.SetTextColor(RGB(100, 100, 200))									// blue progress bar
// 					.SetInterior(RGB(255, 255, 255))									// on white background
// 					);

				nRowCount++;
			}
		}

		m_wndTempGrid.LockUpdate(bLock);
		m_wndTempGrid.Redraw();
	}	
	catch (CException* e)
	{
		return FALSE;
	}

	return TRUE;
}

void CTemperatureSensorDataDlg::UpdateGridData()
{
	CString				strTemp	= "";
	CFormModule*		pModule	= NULL;
	BOOL				bLock	= FALSE;
	_MAPPING_DATA*		pMapData;
	EP_GP_DATA			gpData;
	int					nSensorCount = 0;
	const LONG			nGridRowCount = m_wndTempGrid.GetRowCount();
	ULONG				nIndex = 1;
	
	bLock = m_wndTempGrid.LockUpdate();
	
	gpData = EPGetGroupData(this->GetModuleID(), 0);
	
	pModule = m_pDoc->GetModuleInfo(this->GetModuleID());

	// CHECK SENSOR COUNT MODIFIED START
	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		pMapData = pModule->GetSensorMap(0, i);

		if(pMapData->nChannelNo == 1) nSensorCount++;
	}

	if(nSensorCount != m_wndTempGrid.GetRowCount())
	{
		this->InitDefaultRows();
	}
	// CHECK SENSOR COUNT MODIFIED END
	
	for(int i = 0; i < EP_MAX_SENSOR_CH; i++)
	{
		pMapData = pModule->GetSensorMap(0, i);

		if(pMapData->nChannelNo == 1)
		{
			strTemp.Format("%s", ::GetModuleName(this->GetModuleID()));
			m_wndTempGrid.SetValueRange(CGXRange(nIndex,1), strTemp);				// Module ID

			m_wndTempGrid.SetValueRange(CGXRange(nIndex,2), pMapData->szName);		// Sensor Name

// 20201013 KSCHOI NEED TO DISPLAY TEMPERATURE BY FLOAT START
			strTemp.Format(m_csTemperatureStringFormat,float(ETC_PRECISION(gpData.sensorData.sensorData1[i].lData)));

			m_wndTempGrid.SetStyleRange(CGXRange(nIndex, 3), 
				CGXStyle()
				.SetControl(GX_IDS_CTRL_PROGRESS)
				.SetValue(0L)
				.SetUserAttribute(GX_IDS_UA_PROGRESS_MIN, _T("0.0"))
				.SetUserAttribute(GX_IDS_UA_PROGRESS_MAX, _T("50.0"))
				.SetUserAttribute(GX_IDS_UA_PROGRESS_CAPTIONMASK, strTemp)			// sprintf formatting for value
				.SetTextColor(RGB(100, 100, 200))									// blue progress bar
				.SetInterior(RGB(255, 255, 255))									// on white background
				);

			m_wndTempGrid.SetValueRange(CGXRange(nIndex,3), ETC_PRECISION(gpData.sensorData.sensorData1[i].lData));
// 20201013 KSCHOI NEED TO DISPLAY TEMPERATURE BY FLOAT END

			nIndex++;
		}
	}

	m_wndTempGrid.LockUpdate(bLock);
	m_wndTempGrid.Redraw();
}

void CTemperatureSensorDataDlg::OnTimer(UINT nIDEvent)
{
	if(nIDEvent == TIMER_TEMPERATURE_REFRESH_ID)
	{
		// Main/Sub Category Check In MainFrame
		if(((CMainFrame*)AfxGetMainWnd())->m_uSelect == CAllSystemView::SELECT_MAIN_ITEM)
		{
			if(this->IsWindowVisible() == TRUE)
			{
				this->ShowWindow(FALSE);			
			}
			return;
		}

		// Only Update Sensor Data On This Dialog SW_SHOW
		if(this->IsWindowVisible() == TRUE)
		{
			this->UpdateGridData();
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CTemperatureSensorDataDlg::SetModuleID(int nModuleID)
{
	this->m_nModuleID = nModuleID;
}

void CTemperatureSensorDataDlg::ChangeModuleID(int nModuleID)
{
	this->SetModuleID(nModuleID);
	this->UpdateGridData();
}

int CTemperatureSensorDataDlg::GetModuleID()
{
	return this->m_nModuleID;
}

BOOL CTemperatureSensorDataDlg::DestroyWindow()
{
	return CDialog::DestroyWindow();
}

CString CTemperatureSensorDataDlg::CalcTemperatureChracterByLocale()
{
	char pTemperatureSpecialCharacter[32] = {};
	LANGID langID = 0xFF&GetSystemDefaultLangID();

	if(langID == LANG_HUNGARIAN || langID == LANG_ENGLISH)
	{
		// NO EXISTS TEMPERATURE CHARACTER IN CODE PAGE 1252 (ENGLISH, HUNGARIAN)
		pTemperatureSpecialCharacter[0] = TEMPERATURE_HUNGARIAN_SPECIAL_CHARACTER;
		pTemperatureSpecialCharacter[1] = 'C';

		return pTemperatureSpecialCharacter;
	}
	else
	{
		// FOR LANG_KOREAN, LANG_CHINESE
		CStringW cswTemper = "";
		cswTemper.AppendChar(UNICODE_VALUE_TEMPERATURE);

		int len = WideCharToMultiByte( CP_ACP, 0, cswTemper, -1, NULL, 0, NULL, NULL );
		WideCharToMultiByte(CP_ACP, 0, cswTemper, -1, pTemperatureSpecialCharacter, len, NULL, NULL);

		return pTemperatureSpecialCharacter;
	}
}

CString CTemperatureSensorDataDlg::MakeTemperatureStringFormat(CString csTemperatureChar)
{
	m_csTemperatureStringFormat = "%.1f " + csTemperatureChar;

	return m_csTemperatureStringFormat;
}