// UpsSettingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "UpsSettingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UpsSettingDlg dialog


UpsSettingDlg::UpsSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(UpsSettingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(UpsSettingDlg)
	m_bUseUPS = FALSE;
	//}}AFX_DATA_INIT	
}


void UpsSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(UpsSettingDlg)
	DDX_Control(pDX, IDC_COMB_UPS_SELECT, m_CtrlUpsSelect);
	DDX_Check(pDX, IDC_CHK_UPS_USE, m_bUseUPS);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UpsSettingDlg, CDialog)
	//{{AFX_MSG_MAP(UpsSettingDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UpsSettingDlg message handlers

BOOL UpsSettingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	LoadSetting();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void UpsSettingDlg::LoadSetting()
{
	int nIndex = 0;
	nIndex = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UPS_FACTORY", 0);
	m_CtrlUpsSelect.SetCurSel(nIndex);
	m_bUseUPS = (bool)AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "UPS_FACTORY", 0);
	
	UpdateData(false);
}

void UpsSettingDlg::SaveSetting()
{
	UpdateData(true);

	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "UPS_RUN", m_bUseUPS);	
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "UPS_FACTORY", m_CtrlUpsSelect.GetCurSel());	
}

void UpsSettingDlg::OnOK() 
{
	// TODO: Add extra validation here
	SaveSetting();

	CDialog::OnOK();
}
