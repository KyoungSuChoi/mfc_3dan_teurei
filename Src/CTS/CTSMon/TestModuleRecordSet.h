#if !defined(AFX_TESTMODULERECORDSET_H__D9E15E10_175D_46CD_A619_292C9E11FD66__INCLUDED_)
#define AFX_TESTMODULERECORDSET_H__D9E15E10_175D_46CD_A619_292C9E11FD66__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestModuleRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestModuleRecordSet DAO recordset

class CTestModuleRecordSet : public CDaoRecordset
{
public:
	CTestModuleRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestModuleRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CTestModuleRecordSet, CDaoRecordset)
	long	m_ProcedureID;
	long	m_ModuleID;
	long	m_BoardIndex;
	CString	m_TrayNo;
	CString	m_UserID;
	long	m_GroupIndex;
	COleDateTime	m_DateTime;
	CString	m_TraySerial;
	CString	m_TestSerialNo;
	CString	m_State;
	CString	m_LotNo;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestModuleRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTMODULERECORDSET_H__D9E15E10_175D_46CD_A619_292C9E11FD66__INCLUDED_)
