// DetailChannelView.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CTSMonDoc.h"
#include "DetailChannelView.h"

#include "MainFrm.h"

#include "RegTrayDlg.h"
#include "RebootDlg.h"

#ifdef _DEBUG
#include "Mmsystem.h"


#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef _MYCOLOR
#define _MYCOLOR
#define TITLELABELBK	RGB(0,0,150)
#define TITLETEXT		RGB(255,255,0)
#define CONTENTTEXT		RGB(0,0,0)		
#define CONTENTBK		RGB(255,255,255)
#endif

static BOOL g_bChannelBlink = FALSE;

extern STR_LOGIN g_LoginData;
/////////////////////////////////////////////////////////////////////////////
// CDetailChannelView

IMPLEMENT_DYNCREATE(CDetailChannelView, CFormView)

CDetailChannelView::CDetailChannelView()
	: CFormView(CDetailChannelView::IDD)
{
	//{{AFX_DATA_INIT(CDetailChannelView)
	m_bAutoProcess = FALSE;
	//}}AFX_DATA_INIT
	m_pTreeCtrlX =  new SECTreeCtrl;
	m_nCurModuleID = 0;
	m_nCurTrayIndex = 0;
	m_nCurGroup = 0;
	m_pDetalChColorFlag = NULL;
	m_nDisplayTimer = 0;
	m_nCmdTarget = EP_GROUP_CMD;
	m_nTrayColCount = 16;
	m_pConfig = NULL;
//	m_nCurStep = 0;
}

CDetailChannelView::~CDetailChannelView()
{
	if(m_pTreeCtrlX)
	{
		delete m_pTreeCtrlX;
		m_pTreeCtrlX = NULL;
	}
	if(m_pDetalChColorFlag)
	{
		delete [] m_pDetalChColorFlag;
		m_pDetalChColorFlag = NULL;
	}
}

void CDetailChannelView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDetailChannelView)
	DDX_Control(pDX, IDC_LAB_USER_T, m_TestUserT);
	DDX_Control(pDX, IDC_LAB_EDIT_DAY_T, m_TestEditDayT);
	DDX_Control(pDX, IDC_LAB_DESCRIPTION_T, m_TestDescriptionT);
	DDX_Control(pDX, IDC_INIT_ATUO_PROCESS, m_btnResetAutoProcess);
	DDX_Control(pDX, IDC_REG_TRAY, m_btnRegTray);
	DDX_Control(pDX, IDC_DATA_VIEW_BUTTON, m_btnFileOptn);
	DDX_Control(pDX, IDC_CHANNEL_CONDITION_BUTTON, m_btnTestConView);
	DDX_Control(pDX, IDC_GAS_DATA, m_ctrlGas);
	DDX_Control(pDX, IDC_TEMP_DATA, m_ctrlTemp);
	DDX_Control(pDX, IDC_AUTO_PROCESS_D, m_AutoProcD);
	DDX_Control(pDX, IDC_AUTO_PROCESS_T, m_AutoProcT);
	DDX_Control(pDX, IDC_ERROR_RATE, m_ctrlErrorRate);
	DDX_Control(pDX, IDC_ERROR_COUNT, m_ctrlError);
	DDX_Control(pDX, IDC_NORMAL_COUNT, m_ctrlNormal);
	DDX_Control(pDX, IDC_CHANNEL_MODULENO_T, 		m_ModuleNoT);
	DDX_Control(pDX, IDC_CHANNEL_MODULENO_D, 		m_ModuleNoD);
	DDX_Control(pDX, IDC_CHANNEL_STATE_T, 			m_StateT);
	DDX_Control(pDX, IDC_CHANNEL_STATE_D, 			m_StateD);
	DDX_Control(pDX, IDC_CHANNEL_DOORSTATE_T,		m_DoorStateT);
	DDX_Control(pDX, IDC_CHANNEL_DOORSTATE_D, 		m_DoorStateD);
	DDX_Control(pDX, IDC_CHANNEL_TESTNAME_T, 		m_TestNameT);
	DDX_Control(pDX, IDC_CHANNEL_TESTNAME_D, 		m_TestNameD);
	DDX_Control(pDX, IDC_CHANNEL_TRAYNO_T, 			m_TrayNoT);
	DDX_Control(pDX, IDC_CHANNEL_TRAYNO_D, 			m_TrayNoD);
	DDX_Control(pDX, IDC_CHANNEL_BATTERYNO_T, 		m_TotalStepT);
	DDX_Control(pDX, IDC_CHANNEL_BATTERYNO_D, 		m_TotalStepD);
	DDX_Control(pDX, IDC_CHANNEL_LOTNO_T, 			m_LotNoT);
	DDX_Control(pDX, IDC_CHANNEL_LOTNO_D, 			m_LotNoD);
	DDX_Control(pDX, IDC_CHANNEL_CYCLENO_T, 		m_UserIDT);
	DDX_Control(pDX, IDC_CHANNEL_CYCLENO_D, 		m_UserIDD);
	DDX_Control(pDX, IDC_CHANNEL_CELLNUM_T, 		m_CellNumT);
	DDX_Control(pDX, IDC_CHANNEL_CELLNUM_D, 		m_CellNumD);
	DDX_Control(pDX, IDC_CHANNEL_TOTALTIME_T, 		m_TotalTimeT);
	DDX_Control(pDX, IDC_CHANNEL_TOTALTIME_D, 		m_TotalTimeD);
	DDX_Control(pDX, IDC_CHANNEL_FILENAME_T, 		m_FileNameT);
	DDX_Control(pDX, IDC_CHANNEL_FILENAME_D, 		m_FileNameD);
	DDX_Check(pDX, IDC_AUTO_PROCESS, m_bAutoProcess);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDetailChannelView, CFormView)
	//{{AFX_MSG_MAP(CDetailChannelView)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHANNEL_CONDITION_BUTTON, OnChannelConditionButton)
	ON_WM_DESTROY()
	ON_COMMAND(ID_RUN_MODULE, OnRunModule)
	ON_COMMAND(ID_PAUSE, OnPause)
	ON_COMMAND(ID_CONTINUE_MODULE, OnContinueModule)
	ON_COMMAND(ID_STOP, OnStop)
	ON_COMMAND(ID_CLEAR, OnClear)
	ON_COMMAND(ID_STEPOVER, OnStepover)
	ON_UPDATE_COMMAND_UI(ID_RUN_MODULE, OnUpdateRunModule)
	ON_UPDATE_COMMAND_UI(ID_PAUSE, OnUpdatePause)
	ON_UPDATE_COMMAND_UI(ID_CONTINUE_MODULE, OnUpdateContinueModule)
	ON_UPDATE_COMMAND_UI(ID_STOP, OnUpdateStop)
	ON_UPDATE_COMMAND_UI(ID_CLEAR, OnUpdateClear)
	ON_UPDATE_COMMAND_UI(ID_STEPOVER, OnUpdateStepover)
	ON_WM_SIZE()
	ON_NOTIFY(TVN_GETDISPINFO, IDC_CHANNEL_MODULE_TREE, OnGetdispinfoChannelModuleTree)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	ON_BN_CLICKED(IDC_AUTO_PROCESS, OnAutoProcess)
	ON_BN_CLICKED(IDC_INIT_ATUO_PROCESS, OnInitAtuoProcess)
	ON_BN_CLICKED(IDC_REG_TRAY, OnRegTray)
	ON_COMMAND(ID_RECONNECT, OnReconnect)
	ON_UPDATE_COMMAND_UI(ID_RECONNECT, OnUpdateReconnect)
	ON_COMMAND(ID_TRAYNO_USER_INPUT, OnTraynoUserInput)
	ON_UPDATE_COMMAND_UI(ID_TRAYNO_USER_INPUT, OnUpdateTraynoUserInput)
	ON_COMMAND(ID_MAINTENANCE_MODE, OnMaintenanceMode)
	ON_UPDATE_COMMAND_UI(ID_MAINTENANCE_MODE, OnUpdateMaintenanceMode)
	ON_COMMAND(ID_REBOOT, OnReboot)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_REF_AD_VALUE_UPDATE, OnRefAdValueUpdate)
	ON_UPDATE_COMMAND_UI(ID_REF_AD_VALUE_UPDATE, OnUpdateRefAdValueUpdate)
	ON_NOTIFY(NM_RCLICK, IDC_CHANNEL_MODULE_TREE, OnRclickChannelModuleTree)
	ON_BN_CLICKED(IDC_DATA_VIEW_BUTTON, OnDataViewButton)
	ON_COMMAND(ID_GET_PROFILE_DATA, OnGetProfileData)
	ON_UPDATE_COMMAND_UI(ID_GET_PROFILE_DATA, OnUpdateGetProfileData)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO6, OnRadio6)
	ON_BN_CLICKED(IDC_RADIO7, OnRadio7)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_VIEW_RESULT, OnViewResult)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RESULT, OnUpdateViewResult)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_PRINT, 			CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, 	CView::OnFilePrintPreview)

	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
//	ON_MESSAGE(WM_GRID_CLICK, OnGridClicked)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDetailChannelView diagnostics

#ifdef _DEBUG
void CDetailChannelView::AssertValid() const
{
	CFormView::AssertValid();
}

void CDetailChannelView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSMonDoc* CDetailChannelView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonDoc)));
	return (CCTSMonDoc*)m_pDocument;
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDetailChannelView message handlers

void CDetailChannelView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	m_nCurModuleID = EPGetModuleID(0);
	((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(1);

	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	m_nTrayColCount = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TopGridColCout", 16);
	
	m_ModuleNoT.SetText(pDoc->m_strModuleName);
	m_ModuleNoT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_ModuleNoD.SetGradientColor(CONTENTBK).SetGradient(TRUE);

	m_StateT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_StateD.SetBkColor(CONTENTBK);

	m_DoorStateT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_DoorStateD.SetGradientColor(CONTENTBK).SetGradient(TRUE);

//	m_TrayStateT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT);
//	m_TrayStateD.SetBkColor(CONTENTBK).SetGradient(TRUE);
	
	m_TestNameT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_TestNameD.SetGradientColor(CONTENTBK).SetGradient(TRUE);

	m_TrayNoT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_TrayNoD.SetGradientColor(CONTENTBK).SetGradient(TRUE);

	m_TotalStepT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_TotalStepD.SetGradientColor(CONTENTBK).SetGradient(TRUE);

	m_LotNoT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_LotNoD.SetGradientColor(CONTENTBK).SetGradient(TRUE);

	m_UserIDT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_UserIDD.SetGradientColor(CONTENTBK).SetGradient(TRUE);
	
	m_CellNumT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_CellNumD.SetGradientColor(CONTENTBK).SetGradient(TRUE);

	m_TotalTimeT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_TotalTimeD.SetGradientColor(CONTENTBK).SetGradient(TRUE);
	
	m_FileNameT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_FileNameD.SetGradientColor(CONTENTBK).SetGradient(TRUE);
	
	m_AutoProcT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_AutoProcD.SetGradientColor(CONTENTBK).SetGradient(TRUE);
	//★★ ljb 20091020 S	★★★★★★★★
	m_TestDescriptionT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_TestUserT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	m_TestEditDayT.SetBkColor(TITLELABELBK).SetTextColor(TITLETEXT).SetGradientColor(RGB(100,150,200)).SetGradientType(CLabel::G_LEFT).SetGradient();
	//★★ ljb 20091020 E ★★★★★★★★
	m_ctrlTemp.SetTextColor(RGB(0, 0, 255));
	m_ctrlGas.SetTextColor(RGB(255, 0, 0));

	m_ctrlErrorRate.SetTextColor(RGB(255, 0, 0)).SetBorder(TRUE).SetBorder(TRUE);
	m_ctrlError.SetTextColor(RGB(255, 0, 0)).SetBorder(TRUE).SetBorder(TRUE);
	m_ctrlNormal.SetTextColor(RGB(0, 0, 255)).SetBorder(TRUE).SetBorder(TRUE);

	pDoc->InitTree(m_pTreeCtrlX, IDC_CHANNEL_MODULE_TREE, this);	//Initilaize Tree Ctrl

	InitDetailChannelGrid();													//Init. Detail view Grid	
//	m_btnTestConView.LoadBitmaps(IDB_BTNBACK,5, 7, 7, 7, 6 );
	m_pConfig = pDoc->GetTopConfig();

	GetDocument()->DrawTree(m_pTreeCtrlX);			//Default Tree View
	GroupSetChanged(m_nTrayColCount);								//Default Grid View
	SetModuleData();
	UpdateData(FALSE);
}

void CDetailChannelView::InitDetailChannelGrid()
{
	m_wndDetailGrid.SubclassDlgItem(IDC_CHANNEL_DETAIL_GRID, this);
	m_wndDetailGrid.m_bRowSelection = TRUE;
	
	m_wndDetailGrid.m_bSameColSize  = FALSE;
	m_wndDetailGrid.m_bSameRowSize  = FALSE;
	m_wndDetailGrid.m_bCustomWidth  = TRUE;

/*	CRect rect;
	m_wndDetailGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);

	int width = rect.Width()/100;
	m_wndDetailGrid.m_nWidth[1]	= width* 10;
	m_wndDetailGrid.m_nWidth[2]	= width* 8;
	m_wndDetailGrid.m_nWidth[3]	= width* 10;
	m_wndDetailGrid.m_nWidth[4]	= width* 9;
	m_wndDetailGrid.m_nWidth[5]	= width* 9;
	m_wndDetailGrid.m_nWidth[6]	= width* 9;
	m_wndDetailGrid.m_nWidth[7]	= width* 9;
	m_wndDetailGrid.m_nWidth[8]	= width* 9;
	m_wndDetailGrid.m_nWidth[9]	= width* 18;
	m_wndDetailGrid.m_nWidth[10] = width* 5;
*/
	m_wndDetailGrid.Initialize();
	m_wndDetailGrid.SetRowCount(0);
	m_wndDetailGrid.SetColCount(14);
	m_wndDetailGrid.SetDefaultRowHeight(18);
//	m_wndDetailGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
//	m_wndDetailGrid.m_BackColor	= RGB(255,255,255);
//	m_wndDetailGrid.m_SelColor	= RGB(225,255,225);
//	m_wndDetailGrid.HideCols(5, 5);

	m_wndDetailGrid.m_bCustomColor 	= TRUE;
	m_wndDetailGrid.SetDrawingTechnique(gxDrawUsingMemDC);

	//Enable Tooltips
	m_wndDetailGrid.EnableGridToolTips();
    m_wndDetailGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


/*	m_wndDetailGrid.SetValueRange(CGXRange(0,1),  ::GetStringTable(IDS_LABEL_CHANNEL));
	m_wndDetailGrid.SetValueRange(CGXRange(0,2),  ::GetStringTable(IDS_LABEL_CELL_NO));
	m_wndDetailGrid.SetValueRange(CGXRange(0,3),  ::GetStringTable(IDS_LABEL_STEP));
	m_wndDetailGrid.SetValueRange(CGXRange(0,4),  ::GetStringTable(IDS_LABEL_STATUS));
	m_wndDetailGrid.SetValueRange(CGXRange(0,5),  ::GetStringTable(IDS_LABEL_BAD));
	m_wndDetailGrid.SetValueRange(CGXRange(0,6),  ::GetStringTable(IDS_LABEL_GRADE));
	m_wndDetailGrid.SetValueRange(CGXRange(0,7),  ::GetStringTable(IDS_LABEL_STEP_TIME));
	m_wndDetailGrid.SetValueRange(CGXRange(0,8),  ::GetStringTable(IDS_LABEL_VOLTAGE));
	m_wndDetailGrid.SetValueRange(CGXRange(0,9),  ::GetStringTable(IDS_LABEL_CURRENT));
	m_wndDetailGrid.SetValueRange(CGXRange(0,10), ::GetStringTable(IDS_LABEL_CAPACITY));
	m_wndDetailGrid.SetValueRange(CGXRange(0,11), ::GetStringTable(IDS_LABEL_POWER));
	m_wndDetailGrid.SetValueRange(CGXRange(0,12), ::GetStringTable(IDS_LABEL_ENERGY));
	m_wndDetailGrid.SetValueRange(CGXRange(0,13), ::GetStringTable(IDS_LABEL_IMPEDANCE));
*/
	DataUnitChanged();
	
	m_wndDetailGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndDetailGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192)));
	//Table setting
	m_wndDetailGrid.SetStyleRange(CGXRange().SetTable(),
			CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(GetStringTable(IDS_LANG_TEXT_FONT))));

//	SetGridColor();
}


void CDetailChannelView::DrawDetailGrid()
{
	CCTSMonDoc *pDoc=  GetDocument();
	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);

	if(pModule == NULL)		return;

	int nChannelCount = pModule->GetChInJig(m_nCurTrayIndex);
	if(nChannelCount <=0)	return;
	
	BOOL bLock = m_wndDetailGrid.LockUpdate();

	int nRowCount = nChannelCount;
	if(m_wndDetailGrid.GetRowCount() != nChannelCount)
	{
		m_wndDetailGrid.SetRowCount(nChannelCount);
		if(m_pDetalChColorFlag)
		{
			delete [] m_pDetalChColorFlag;
			m_pDetalChColorFlag = NULL;
		}
	
		nRowCount = nChannelCount/m_nTrayColCount;
		if(nChannelCount%m_nTrayColCount) nRowCount++;

		m_pDetalChColorFlag = new char[nChannelCount];

		m_wndDetailGrid.m_CustomColorRange	= CGXRange(1, 4, nChannelCount, 4);
		memset(m_pDetalChColorFlag, 15, nChannelCount);		//Default color Setting Array[15]
		m_wndDetailGrid.m_pCustomColorFlag = (char *)m_pDetalChColorFlag;
	}
	
	CString strTemp;
	WORD nTrayType = pDoc->m_nViewTrayType;

	for (int nI = 0; nI < nChannelCount; nI++)
	{
// 		if(bShowContinueChNo)
// 		{
//  			strTemp.Format("%d (CH%03d)", pDoc->GetModuleStartChNo(m_nCurModuleID)+nI, nI);
// 		}
// 		else
// 		{
			if(GetDocument()->m_bUseGroupSet)
			{
				strTemp.Format("%s%d-C%d", GetDocument()->m_strGroupName.Left(1), m_nCurGroup+1, nI+1);
			}
			else
			{
				//A~Z가 다쓰이면(26개이상) AA 부터 나감 
				switch (nTrayType)
				{
				case TRAY_TYPE_LG_ROW_COL:	//세로 번호 체계
					if(nI/nRowCount >= 26)
					{
						strTemp.Format("%3d (%c%c-%2d)", nI+1, 'A'+nI/(nRowCount*26)-1, 'A'+ (nI/nRowCount-26), nI%nRowCount+1);
					}
					else
					{
						strTemp.Format("%3d (%c-%2d)", nI+1, 'A'+nI/nRowCount, nI%nRowCount+1);
					}
					break;
				case TRAY_TYPE_PB5_ROW_COL:	//세로 번호 체계(각형)
					strTemp.Format("%3d", nI+1);
					break;
				default:
					if(nI/m_nTrayColCount >= 26)
					{
						strTemp.Format("%3d (%c%c-%2d)", nI+1, 'A'+nI/(m_nTrayColCount*26)-1, 'A'+ (nI/m_nTrayColCount-26), nI%m_nTrayColCount+1);
					}
					else
					{
						strTemp.Format("%3d (%c-%2d)", nI+1, 'A'+nI/m_nTrayColCount, nI%m_nTrayColCount+1);
					}
					break;
				}
			}
//		}

		m_wndDetailGrid.SetValueRange(CGXRange(nI+1, 1), strTemp);
		if(AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Continue ChNo", 0))
		{
			strTemp.Format("%d", pDoc->GetModuleStartChNo(m_nCurModuleID)+nI);
			m_wndDetailGrid.SetValueRange(CGXRange(nI+1, 2), strTemp);
		}
	}
	
	SetGridColor();
	m_wndDetailGrid.LockUpdate(bLock);
	m_wndDetailGrid.Redraw();

// #if CUSTOMER_TYPE == LG_CHEMICAL || CUSTOMER_TYPE == LGC_PB5
// #if CUSTOMER_TYPE == LGC_PB5
// 			strTemp.Format("%3d", nI+1);
// #else
// 			if(nI/nRowCount >= 26)
// 			{
// 				strTemp.Format("%3d (%c%c-%2d)", nI+1, 'A'+nI/(nRowCount*26)-1, 'A'+ (nI/nRowCount-26), nI%nRowCount+1);
// 			}
// 			else
// 			{
// 				strTemp.Format("%3d (%c-%2d)", nI+1, 'A'+nI/nRowCount, nI%nRowCount+1);
// 			}
// #endif
// #else
// 			if(nI/m_nTrayColCount >= 26)
// 			{
// 				strTemp.Format("%3d (%c%c-%2d)", nI+1, 'A'+nI/(m_nTrayColCount*26)-1, 'A'+ (nI/m_nTrayColCount-26), nI%m_nTrayColCount+1);
// 			}
// 			else
// 			{
// 				strTemp.Format("%3d (%c-%2d)", nI+1, 'A'+nI/m_nTrayColCount, nI%m_nTrayColCount+1);
// 			}
// #endif
// 		}


// 		m_wndDetailGrid.SetValueRange(CGXRange(nI+1, 1), strTemp);
// 	}
// 
// 	SetGridColor();
// 	m_wndDetailGrid.LockUpdate(bLock);
// 	m_wndDetailGrid.Redraw();
}

void CDetailChannelView::SetChannelData()
{
	BYTE colorFlag;
	BOOL bFlash ;
	char szTemp[128];
//	LONG lMaxTotalTime = 0;
	LONG lTemp;
	CString strTemp;

	CCTSMonDoc *pDoc = GetDocument();	
	int nNormalCout= 0;	

	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == FALSE)	return;

	int nCellNo = pModule->GetCellNo(m_nCurTrayIndex);
	CTray *pTray = pDoc->GetTrayData(m_nCurModuleID, m_nCurTrayIndex);
	int nChannelNo = pModule->GetChInJig(m_nCurTrayIndex);
	STR_SAVE_CH_DATA chSaveData;
	BOOL bContinueChNo = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "Continue ChNo", 0);


	BOOL bLock = m_wndDetailGrid.LockUpdate();
	for (int nCh = 0; nCh < nChannelNo; nCh++)
	{
		if(pModule->GetChannelStepData(chSaveData, nCh, m_nCurTrayIndex) == FALSE)	continue;
		
		if(pDoc->m_bHideNonCellData && (chSaveData.channelCode  == EP_CODE_NONCELL || chSaveData.channelCode == EP_CODE_CELL_NONE))
		{
			m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 2, nCh+1, m_wndDetailGrid.GetColCount()), "");	//Cell No~
			pDoc->GetStateMsg(EP_STATE_IDLE, colorFlag);													//Get State Msg
			m_pDetalChColorFlag[nCh] = colorFlag;
			m_wndDetailGrid.SetStyleRange(CGXRange(nCh+1, 5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(chSaveData.channelCode, TRUE))));
			continue;
		}

		//Cell No
		sprintf(szTemp, "");
		if(pTray)
		{
			strTemp = pTray->GetCellSerial(nCh);
			if(strTemp.IsEmpty())
			{
				if(bContinueChNo)	sprintf(szTemp, "%d", nCellNo+nCh);
			}
			else
			{	
				if(bContinueChNo)	sprintf(szTemp, "%d :: %s", nCellNo+nCh, strTemp);
				else				sprintf(szTemp, "%s", nCellNo+nCh, strTemp);
			}
		}
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 2),  szTemp);	//Cell No

		sprintf(szTemp, "%d", chSaveData.nStepNo);
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 3), szTemp);	//Step No
		
#ifdef _DEBUG
		sprintf(szTemp, "%s(%d)", pDoc->GetStateMsg(chSaveData.state, colorFlag), chSaveData.state);										//Get State Msg
#else
		sprintf(szTemp, "%s", pDoc->GetStateMsg(chSaveData.state, colorFlag));										//Get State Msg
#endif
		if (!m_pConfig->m_bShowText)		sprintf(szTemp, "");
		bFlash = m_pConfig->m_bStateFlash[colorFlag];
		colorFlag = ((m_pConfig->m_bShowColor && !bFlash) || (m_pConfig->m_bShowColor && bFlash && g_bChannelBlink)) ? colorFlag : 0x0f;	//15 is Channel Default color
		m_pDetalChColorFlag[nCh] = colorFlag;
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 4), szTemp);

		if(::IsNormalCell(chSaveData.channelCode))	nNormalCout++;
		
		//Failure
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 5), pDoc->ValueString(chSaveData.channelCode, EP_CH_CODE));
		m_wndDetailGrid.SetStyleRange(CGXRange(nCh+1, 5), 
						CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->ChCodeMsg(chSaveData.channelCode, TRUE))));
		//Grade
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 6), pDoc->ValueString(chSaveData.grade, EP_GRADE_CODE));
		//StepTime
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 7), pDoc->ValueString(chSaveData.fStepTime, EP_STEP_TIME));					
		//Voltage
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 8), pDoc->ValueString(chSaveData.fVoltage, EP_VOLTAGE));
		//Current
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 9), pDoc->ValueString(chSaveData.fCurrent, EP_CURRENT));
		//Capacity
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 10), pDoc->ValueString(chSaveData.fCapacity, EP_CAPACITY));
		//Watt
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 11), pDoc->ValueString(chSaveData.fWatt, EP_WATT));
		//Watt Hour
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 12), pDoc->ValueString(chSaveData.fWattHour, EP_WATT_HOUR));
		//Impedance
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 13), pDoc->ValueString(chSaveData.fImpedance, EP_IMPEDANCE));
		m_wndDetailGrid.SetValueRange(CGXRange(nCh+1, 14), pDoc->ValueString(chSaveData.fAuxData[0], EP_TEMPER));
	}
	m_wndDetailGrid.LockUpdate(bLock);
	m_wndDetailGrid.Redraw();
	
//	ConvertTime(szTemp, lMaxTotalTime);
	WORD state  = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(state == EP_STATE_RUN || state == EP_STATE_PAUSE)
	{
		sprintf(szTemp, "%d Step/ %d Step", pModule->GetCurStepNo() ,pDoc->GetStepCount(m_nCurModuleID, m_nCurGroup));
		m_TotalStepD.SetText(szTemp);
		
		COleDateTime totTime;
		totTime = COleDateTime::GetCurrentTime() - pModule->GetRunStartTime();

/*		GetRunStartTime().Format()
		time_t  finish, elapsed_time;
		time( &finish );
		elapsed_time = difftime(finish, pDoc->m_pModule[nModuleIndex].GetStartTime());
		localtime(&elapsed_time);
		CTime runTime(elapsed_time);
*/		m_TotalTimeD.SetText(totTime.Format("%H:%M:%S"));

/*		DWORD tmpTime;//= (timeGetTime()-pDoc->m_pModule[nModuleIndex].pGroup[m_nCurGroup].nRunTime)/1000;	//Second
		div_t result;
		result = div(elapsed_time, 3600);
		tmpTime = result.quot;
		result = div(result.rem, 60);
		sprintf(szTemp, "%d:%02d:%02d", tmpTime, result.quot, result.rem);
		m_TotalTimeD.SetText(szTemp);
*/	}

	if(pTray)
	{
		sprintf(szTemp, "%3d %s", nNormalCout, ::GetStringTable(IDS_TEXT_COUNT_UNIT));
		m_ctrlNormal.SetText(szTemp);

		lTemp = pTray->lInputCellCount;
		nCellNo = lTemp - nNormalCout;
		if(nCellNo < 0)		nCellNo = 0;
		sprintf(szTemp, "%3d %s", nCellNo, ::GetStringTable(IDS_TEXT_COUNT_UNIT));
		m_ctrlError.SetText(szTemp);
		
		float errorRate = 0.0f;
		if(lTemp > 0)
		{
			errorRate = (float)nCellNo/(float)lTemp*100.0f;
		}
		sprintf(szTemp, "%.1f%%", errorRate);
		m_ctrlErrorRate.SetText(szTemp);
	}
/*	EP_GP_DATA gpData = EPGetGroupData(m_nCurModuleID, m_nCurGroup);
	sprintf(szTemp, "%.1f℃", ETC_PRECISION(gpData.gpSensorData.nTempData));
	m_ctrlTemp.SetText(szTemp);
	sprintf(szTemp, "%.1f", ETC_PRECISION(gpData.gpSensorData.nGasData));
	m_ctrlGas.SetText(szTemp);
*/
}

void CDetailChannelView::SetModuleData()
{
	CString strTemp;
	BYTE	colorFlag;
	char szTemp[128];
	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetDocument();
	CFormModule *pModule = pDoc->GetModuleInfo(m_nCurModuleID);
	if(pModule == NULL)	return;

	sprintf(szTemp,"%s", ::GetModuleName(m_nCurModuleID, m_nCurGroup));
	m_ModuleNoD.SetText(szTemp);

	WORD state = pModule->GetState(TRUE);
#if TRAY_DATA_NVRAM		//NVRAM 사용하에 되면 Idle 상태에서만 Tray등록이 가능하다.
//	if((state == EP_STATE_IDLE || state == EP_STATE_STANDBY) && EPTrayState(m_nCurModuleID, m_nCurGroup) == EP_TRAY_LOAD)
	if(state == EP_STATE_READY || state == EP_STATE_STANDBY || state == EP_STATE_END)
	{
		GetDlgItem(IDC_INIT_ATUO_PROCESS)->EnableWindow(TRUE);
		GetDlgItem(IDC_REG_TRAY)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_INIT_ATUO_PROCESS)->EnableWindow(FALSE);
		GetDlgItem(IDC_REG_TRAY)->EnableWindow(FALSE);
	}
#endif
	strTemp = pDoc->GetStateMsg(state, colorFlag);
	if(pModule->GetLineMode() == EP_ONLINE_MODE)
	{
		strTemp = "[ON]"+strTemp;
	}
	m_StateD.SetText(strTemp);
	
	m_StateD.SetTextColor(m_pConfig->m_TStateColor[colorFlag]);
	m_StateD.SetBkColor(m_pConfig->m_BStateColor[colorFlag]);
	m_TestNameD.SetText (pModule->GetTestName());
//★★ ljb 20091020 S	★★★★★★★★
	CTestCondition *pTestConditon = pModule->GetCondition();
	STR_CONDITION_HEADER *pConditionHeader = pTestConditon->GetTestInfo();	
	//GetDlgItem(IDC_LAB_DESCRIPTION)->SetWindowText(pConditionHeader->szName);
	GetDlgItem(IDC_LAB_DESCRIPTION)->SetWindowText(pConditionHeader->szDescription);
	GetDlgItem(IDC_LAB_USER)->SetWindowText(pConditionHeader->szCreator);
	GetDlgItem(IDC_LAB_EDIT_DAY)->SetWindowText(pConditionHeader->szModifiedTime);

//★★ ljb 20091020 E ★★★★★★★★
	sprintf(szTemp, "%d", pModule->GetInputCellCount(m_nCurTrayIndex));
	m_CellNumD.SetText (szTemp);
	m_FileNameD.SetText(pModule->GetResultFileName(m_nCurTrayIndex));
	m_DoorStateD.SetText(pDoc->GetDoorState(m_nCurModuleID));
	m_LotNoD.SetText ( pModule->GetLotNo(m_nCurTrayIndex));
	m_TrayNoD.SetText(pModule->GetTrayNo(m_nCurTrayIndex));	

	m_UserIDD.SetText(pModule->GetOperatorID());
	sprintf(szTemp, "%d Step/ %d Step", pModule->GetCurStepNo(), pDoc->GetStepCount(m_nCurModuleID, m_nCurGroup));
	m_TotalStepD.SetText(szTemp);

	m_bAutoProcess = ::EPGetAutoProcess(m_nCurModuleID);
	if(m_bAutoProcess)
	{
		m_AutoProcD.SetText(::GetStringTable(IDS_TEXT_USE));
	}
	else
	{
		m_AutoProcD.SetText(::GetStringTable(IDS_TEXT_NOT_USE));
	}
	strTemp = pModule->GetTrayNo(0);
	if(strTemp.IsEmpty() == FALSE)
	{
		GetDlgItem(IDC_RADIO1)->SetWindowText(strTemp);
	}
	else
	{
		GetDlgItem(IDC_RADIO1)->SetWindowText("Tray 1");
	}
	strTemp = pModule->GetTrayNo(1);
	if(strTemp.IsEmpty() == FALSE)
	{
		GetDlgItem(IDC_RADIO2)->SetWindowText(strTemp);
	}
	else
	{
		GetDlgItem(IDC_RADIO2)->SetWindowText("Tray 2");
	}
	strTemp = pModule->GetTrayNo(2);
	if(strTemp.IsEmpty() == FALSE)
	{
		GetDlgItem(IDC_RADIO6)->SetWindowText(strTemp);
	}
	else
	{
		GetDlgItem(IDC_RADIO6)->SetWindowText("Tray 3");
	}
	strTemp = pModule->GetTrayNo(3);
	if(strTemp.IsEmpty() == FALSE)
	{
		GetDlgItem(IDC_RADIO7)->SetWindowText(strTemp);
	}
	else
	{
		GetDlgItem(IDC_RADIO7)->SetWindowText("Tray 4");
	}

	UpdateData(FALSE);

/*
	m_CommStateD.SetText(SystemData.GetCommStateMsg(module.commState));

	CTopView *pTopView = ((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_pTopView;


	strTmp.Format("%d", module.mcs.stepNo);
	m_StepNoD.SetText(strTmp);

	m_StepTypeD.SetText(SystemData.GetCodeName(CODE_STEPTYPE, (int)module.mcs.stepType));
	m_StepModeD.SetText(SystemData.GetCodeName(CODE_STEPMODE, (int)module.mcs.stepMode));
	m_StepTimeD.SetText(SystemData.ConvertTime(module.mcs.stepTime));
*/

}

void CDetailChannelView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
/*	if(nIDEvent == 103)
	{
		SetModuleData(FALSE);
	}
	else if(nIDEvent == 104)
	{
*/	
	if(nIDEvent == m_nDisplayTimer)
	{
		g_bChannelBlink = !g_bChannelBlink;
		SetChannelData();
	}
	else
	{
		ResetCmdFlag(nIDEvent);
	}

	CFormView::OnTimer(nIDEvent);
}

void CDetailChannelView::OnChannelConditionButton() 
{
	// TODO: Add your control notification handler code here
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	pDoc->ShowTestCondition(m_nCurModuleID);
}

void CDetailChannelView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndDetailGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont().SetSize(9)));
	m_wndDetailGrid.OnGridEndPrinting(pDC, pInfo);
	//CFormView::OnEndPrinting(pDC, pInfo);
}

BOOL CDetailChannelView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
	pInfo->SetMaxPage(0xffff);
	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	return DoPreparePrinting(pInfo);
	//return CFormView::OnPreparePrinting(pInfo);
}

void CDetailChannelView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	CString strLine;
	strLine = _T("   ");
	for(int i =0; i<90; i++)
		strLine += _T("━");
	strLine += _T("   ");

	CString strTmp, strModuleNo, strTestName, strCycle, strStep, strTotalTime, strStepTime;
	CGXData& Header	= m_wndDetailGrid.GetParam()->GetProperties()->GetDataHeader();
	CGXData& Footer	= m_wndDetailGrid.GetParam()->GetProperties()->GetDataFooter();

	Header.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(1, 2, 
			CGXStyle().SetValue("Detail Channel Data").SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxOverride);
	Header.StoreStyleRowCol(2, 1, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(2, 2, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(3, 1, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(3, 2, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(4, 1, CGXStyle().SetValue(""),gxOverride);
	Header.StoreStyleRowCol(4, 2, CGXStyle().SetValue(strLine), gxOverride);


	GetDlgItem(IDC_CHANNEL_MODULENO_D )->GetWindowText(strModuleNo);
	GetDlgItem(IDC_CHANNEL_TESTNAME_D )->GetWindowText(strTestName);
	GetDlgItem(IDC_CHANNEL_TOTALTIME_D)->GetWindowText(strTotalTime);
	strTmp.Format(" %s : %-3s       %s : %-15s     %s : %-15s", 
		::GetStringTable(IDS_TEXT_MODULE_NO), strModuleNo, ::GetStringTable(IDS_LABEL_PROC_NAME), strTestName, ::GetStringTable(IDS_TEXT_TOTAL_TIME), strTotalTime);
	Header.StoreStyleRowCol(5, 2, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);
	Header.StoreStyleRowCol(5, 1, CGXStyle().SetValue(""), gxOverride);

	Header.StoreStyleRowCol(6, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(6, 2, CGXStyle().SetValue(""), gxOverride);

//	GetDlgItem(IDC_CHANNEL_CYCLENO_D )->GetWindowText(strCycle);
//	GetDlgItem(IDC_CHANNEL_STEPNO_D  )->GetWindowText(strStep);
//	GetDlgItem(IDC_CHANNEL_STEPTIME_D)->GetWindowText(strStepTime);
	strTmp.Format(" Step No : %-15s    Step Time : %-15s", 	strStep, strStepTime);
	Header.StoreStyleRowCol(7, 2, CGXStyle().SetValue(strTmp).SetFont(CGXFont().SetSize(10).SetBold(TRUE)),gxOverride);
	Header.StoreStyleRowCol(7, 1, CGXStyle().SetValue(""), gxOverride);

	Header.StoreStyleRowCol(8, 1, CGXStyle().SetValue(""), gxOverride);
	Header.StoreStyleRowCol(8, 2, CGXStyle().SetValue(strLine), gxOverride);

	Footer.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxOverride);
	Footer.StoreStyleRowCol(1, 2, CGXStyle().SetValue("$P/$N"),gxOverride);
	Footer.StoreStyleRowCol(1, 3, CGXStyle().SetValue("$d{%Y/%m/%d %H:%M:%S}"), gxOverride);
	m_wndDetailGrid.OnGridPrint(pDC, pInfo);
	CFormView::OnPrint(pDC, pInfo);
}

void CDetailChannelView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndDetailGrid.OnGridPrepareDC(pDC, pInfo);
	//CFormView::OnPrepareDC(pDC, pInfo);
}

void CDetailChannelView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_wndDetailGrid.GetParam()->GetProperties()->SetMargins(150,10,50,10);
	m_wndDetailGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);
	m_wndDetailGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetFont(CGXFont().SetSize(8)));
	int nAWndGrid[] = {70, 50, 40, 70, 80, 40, 85, 55, 55, 55, 0, 55, 55};
	m_wndDetailGrid.SetColWidth(1, 12, 0, nAWndGrid, GX_UPDATENOW);
//	m_wndDetailGrid.UpdateChangedColWidths(1, 11, NULL, GX_UPDATENOW, FALSE);
	m_wndDetailGrid.OnGridBeginPrinting(pDC, pInfo);
	//CFormView::OnBeginPrinting(pDC, pInfo);
}

LONG CDetailChannelView::OnGridDoubleClick(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
/*	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	
	CRTGraphDlg dlg;
	if(pGrid == (CMyGridWnd *)&m_wndDetailGrid)
	{
		if(nRow < 1 || nCol <1 || pGrid == NULL)
			return 0;
		dlg.m_nErrorCode = atoi(m_wndDetailGrid.GetValueRowCol(nRow, 4));
		dlg.m_nCurModuleNo = m_nCurModuleIndex;
		dlg.m_nCurChannelNo = nRow;
		dlg.DoModal();
	
/*		dlg.m_nErrorCode = atoi(m_wndDetailGrid.GetValueRowCol(nRow, 4));
		dlg.m_nCurModuleNo = m_nCurModuleIndex;
		dlg.m_nCurChannelNo = nRow;
//		dlg.UpdateData(FALSE);
		dlg.Create(IDD_RT_GRAPH, this);
		dlg.ShowWindow(SW_SHOW);
*///	}
	return 0;
}

void CDetailChannelView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
}

void CDetailChannelView::OnRunModule() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +  " [Start]");
		return ;
	}

	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = GetDocument();
	int nModuleIndex = EPGetModuleIndex(m_nCurModuleID);
	ASSERT(nModuleIndex >= 0 && nModuleIndex < pDoc->GetInstalledModuleNum());
	int i, nGroupNo;//, nSelNo;
	CString strTemp;
	CFileFind 	finder;

	if(m_nCmdTarget == EP_GROUP_CMD)
	{
		SetCmdFlag(EP_CMD_RUN);
		pDoc->SendRunCommand(m_nCurModuleID, m_nCurGroup);
	}
	else if(m_nCmdTarget == EP_MODULE_CMD)
	{
		if(pDoc->m_bUseGroupSet)
		{
			strTemp.Format("%s 에 등록된 전체 %s 을 작업을 시작 하시겠습니까?", ::GetModuleName(m_nCurModuleID), pDoc->m_strGroupName);
		}
		else
		{
			strTemp.Format(GetStringTable(IDS_MSG_RUN_CONFIRM), ::GetModuleName(m_nCurModuleID));
		}
		
		if(MessageBox(strTemp, "Start", MB_ICONQUESTION|MB_YESNO) == IDYES)
		{
			nGroupNo = EPGetGroupCount(m_nCurModuleID);
			SetCmdFlag(EP_CMD_RUN);
			for(i = 0; i<nGroupNo; i++)
			{
				pDoc->SendRunCommand(m_nCurModuleID, m_nCurGroup);
			}
		}
	}
}

void CDetailChannelView::OnPause() 
{
	// TODO: Add your command handler code here
	SendCommand(EP_CMD_PAUSE);
}

void CDetailChannelView::OnContinueModule() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Continue]" );
		return ;
	}

	int i, nGroupNo;//, nSelNo;
	CString strTemp;

	nGroupNo = EPGetGroupCount(m_nCurModuleID);
	CCTSMonDoc *pDoc = GetDocument();

	if(m_nCmdTarget == EP_GROUP_CMD)
	{
		SetCmdFlag(EP_CMD_RESTART);
		pDoc->SendContinueCommand(m_nCurModuleID, m_nCurGroup);
	}
	else if(m_nCmdTarget == EP_MODULE_CMD)
	{
		if(pDoc->m_bUseGroupSet)
		{
			strTemp.Format("%s 에 등록된 전체 %s를 이어서 하시겠습니까?", ::GetModuleName(m_nCurModuleID), pDoc->m_strGroupName);
		}
		else
		{
			strTemp.Format(GetStringTable(IDS_MSG_CONTINUE_CONFIRM), ::GetModuleName(m_nCurModuleID));
		}

		if(MessageBox(strTemp, "Continue", MB_ICONQUESTION|MB_YESNO) == IDYES)
		{
			SetCmdFlag(EP_CMD_RESTART);
			for(i = 0; i<nGroupNo; i++)
			{
				pDoc->SendContinueCommand(m_nCurModuleID, m_nCurGroup);
			}
		}
	}
}

void CDetailChannelView::OnStop() 
{
	// TODO: Add your command handler code here
	if(GetDocument()->SendStopCmd(m_nCurModuleID) == FALSE)
	{
		CString strTemp;
		strTemp.Format("%s %s",  ::GetModuleName(m_nCurModuleID), ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL));		
		AfxMessageBox(strTemp);
	}
}

void CDetailChannelView::OnClear() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) + " [Initialize]");
		return ;
	}

	int i, nGroupNo;//, nSelNo;
	CString strTemp;

	nGroupNo = EPGetGroupCount(m_nCurModuleID);
	CCTSMonDoc *pDoc = GetDocument();

	if(m_nCmdTarget == EP_GROUP_CMD)
	{
		SetCmdFlag(EP_CMD_CLEAR);
		pDoc->SendInitCommand(m_nCurModuleID, m_nCurGroup);
	}
	else if(m_nCmdTarget == EP_MODULE_CMD)
	{
		if(pDoc->m_bUseGroupSet)
		{
			strTemp.Format("%s 에 등록된 전체 %s 을 초기화 하시겠습니까?", ::GetModuleName(m_nCurModuleID), pDoc->m_strGroupName);
		}
		else
		{
			strTemp.Format(GetStringTable(IDS_MSG_INIT_CONFIRM), ::GetModuleName(m_nCurModuleID), pDoc->m_strGroupName);
		}

		if(MessageBox(strTemp, "Reset", MB_ICONQUESTION|MB_YESNO) == IDYES)
		{
			SetCmdFlag(EP_CMD_CLEAR);
			for(i = 0; i<nGroupNo; i++)
			{
				pDoc->SendInitCommand(m_nCurModuleID, m_nCurGroup);
			}
		}
	}
}

void CDetailChannelView::OnStepover() 
{
	// TODO: Add your command handler code here
	SendCommand(EP_CMD_NEXTSTEP);
}

//Send Command to Group or Channel
BOOL CDetailChannelView::SendCommand(int nCmd, BOOL bChCmd)
{
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) );
		return FALSE;
	}

	int i, nGroupNo, nSelNo, nRtn;
	CString strTemp;

	CCTSMonDoc *pDoc = GetDocument();
	if(bChCmd)
	{
		CRowColArray	awRows;
		m_wndDetailGrid.GetSelectedRows(awRows);
		nSelNo = awRows.GetSize();
		for (i = 0; i< nSelNo; i++)
		{
			nRtn = EPSendCommand(m_nCurModuleID, m_nCurGroup+1, awRows[i], nCmd);
			if(nRtn	!= EP_ACK)
			{
				strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(m_nCurModuleID, m_nCurGroup), pDoc->CmdFailMsg(nRtn));
				AfxMessageBox(strTemp);
			}
		}
		/*
		nSelNo = GetSelChannelArray();
		for(i= 0; i<nSelNo; i++)
		{
			if(EPSendCommand(m_nCurModuleID, HIBYTE(m_awSelChArray[i])+1, LOBYTE(m_awSelChArray[i])+1, nCmd) != EP_CMD_ACK)
			{
				strTemp.Format("Module %d, Group %d, Channel %d Command Send Fail", m_nCurModuleID, HIBYTE(m_awSelChArray[i])+1, LOBYTE(m_awSelChArray[i])+1);
				AfxMessageBox(strTemp);
			}
		}
*/
		return TRUE;
	}

	if(m_nCmdTarget == EP_GROUP_CMD)
	{
		SetCmdFlag(nCmd);
		nRtn = EPSendCommand(m_nCurModuleID, m_nCurGroup+1, 0, nCmd);
		if(nRtn	!= EP_ACK)
		{
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(m_nCurModuleID, m_nCurGroup), pDoc->CmdFailMsg(nRtn));
			AfxMessageBox(strTemp);
			return FALSE;
		}
//		strTemp.Format("Command %d", nCmd);
//		pDoc->WriteActionLog(m_nCurModuleID, m_nCurGroup, strTemp);
	}
	else if(m_nCmdTarget == EP_MODULE_CMD)
	{
		if(pDoc->m_bUseGroupSet)
		{
			strTemp.Format("%s 에 등록된 전체 %s 전송 하시겠습니까?", ::GetModuleName(m_nCurModuleID), pDoc->m_strGroupName);
		}
		else
		{
			strTemp.Format(GetStringTable(IDS_MSG_SEND_COFIRM), ::GetModuleName(m_nCurModuleID));
		}
		
		if(MessageBox(strTemp, "Command Send", MB_ICONQUESTION|MB_YESNO) == IDYES)
		{
			nGroupNo = EPGetGroupCount(m_nCurModuleID);
			SetCmdFlag(nCmd);
			for(i = 0; i<nGroupNo; i++)
			{
				nRtn = EPSendCommand(m_nCurModuleID, i+1, 0, nCmd);
				if(nRtn != EP_ACK)
				{
					strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(m_nCurModuleID, nGroupNo),  pDoc->CmdFailMsg(nRtn));
					AfxMessageBox(strTemp);
				}
/*				else
				{
					strTemp.Format("Command %d", nCmd);
					pDoc->WriteActionLog(m_nCurModuleID, i, strTemp);
				}
*/			}
		}
	}
	return TRUE;
}

void CDetailChannelView::OnUpdateRunModule(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);

	//2005/12/30 모듈이 Standby 상태(즉 PC에선 Standby, Ready, End)에서 시험조건이 전송가능하도록 수정됨	
 	if((gpState == EP_STATE_STANDBY || gpState == EP_STATE_END || gpState == EP_STATE_READY|| gpState==EP_STATE_IDLE) 
 			&& !GetDocument()->m_bRunCmd
 			&& GetDocument()->GetLineMode( m_nCurModuleID ) == EP_OFFLINE_MODE)			
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);

}

void CDetailChannelView::OnUpdatePause(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	WORD mdState = EPGetModuleState(m_nCurModuleID);
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
//	if(mdState != EP_STATE_LINE_OFF && gpState != EP_STATE_IDLE && gpState != EP_STATE_STANDBY && gpState != EP_STATE_PAUSE)
	if( gpState == EP_STATE_RUN 
		&& !GetDocument()->m_bPauseCmd
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
}

void CDetailChannelView::OnUpdateContinueModule(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
// 	if( gpState == EP_STATE_PAUSE 
// 		&& !GetDocument()->m_bContinueCmd
// 		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)	
	// 2009/05/04 EP_ONLINE_MODE 에서도 사용가능하게 변경
	if( gpState == EP_STATE_PAUSE 
		&& !GetDocument()->m_bContinueCmd )	
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
}

void CDetailChannelView::OnUpdateStop(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here	
//	WORD mdState = EPGetModuleState(m_nCurModuleID);
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
//	if(mdState != EP_STATE_LINE_OFF && gpState != EP_STATE_IDLE && gpState != EP_STATE_STANDBY)
// 	if((gpState == EP_STATE_RUN || gpState == EP_STATE_PAUSE) 
// 		&& !GetDocument()->m_bStopCmd
// 		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
	// 2009/05/04 EP_ONLINE_MODE 에서도 사용가능하게 변경
	if((gpState == EP_STATE_RUN || gpState == EP_STATE_PAUSE) && !GetDocument()->m_bStopCmd )
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
}

void CDetailChannelView::OnUpdateClear(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	WORD mdState = EPGetModuleState(m_nCurModuleID);
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
//	if((gpState == EP_STATE_STANDBY || gpState == EP_STATE_END || gpState == EP_STATE_READY) 
	if(gpState != EP_STATE_RUN	 && gpState != EP_STATE_LINE_OFF && !GetDocument()->m_bInitCmd)
//		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
}

void CDetailChannelView::OnUpdateStepover(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	WORD mdState = EPGetModuleState(m_nCurModuleID);
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
//	if(mdState != EP_STATE_LINE_OFF && gpState != EP_STATE_IDLE && gpState != EP_STATE_STANDBY)
	if((gpState == EP_STATE_RUN || gpState == EP_STATE_PAUSE) 
		&& !GetDocument()->m_bNextStepCmd
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE
		&& ::EPGetAutoProcess(m_nCurModuleID) == FALSE)
	{
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
}

void CDetailChannelView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
//	CFormView::ShowScrollBar(SB_VERT,FALSE);
//	CFormView::ShowScrollBar(SB_HORZ,FALSE);
	if( this->GetSafeHwnd())
	{
		RECT rect;
		CRect ctrlRect;
		::GetClientRect(m_hWnd, &rect);

		if(m_wndDetailGrid.GetSafeHwnd())
		{
			m_wndDetailGrid.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			m_wndDetailGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-15, rect.bottom - ctrlRect.top-5, FALSE);

			m_wndDetailGrid.GetWindowRect(&ctrlRect);

			float width = ((float)ctrlRect.Width()/100.0f);
			m_wndDetailGrid.m_nWidth[1]	= int(width* 6.0f);		//Ch No
			m_wndDetailGrid.m_nWidth[2]	= int(width* 9.0f);		//Cell No
			m_wndDetailGrid.m_nWidth[3]	= 0;//int(width* 5.0f);		//Step No
			m_wndDetailGrid.m_nWidth[4]	= int(width* 10.0f);		//State
			m_wndDetailGrid.m_nWidth[5]	= int(width* 11.0f);	//Fail
			m_wndDetailGrid.m_nWidth[6]	= int(width* 5.0f);		//Grade
			m_wndDetailGrid.m_nWidth[7]	= int(width* 10.0f);	//Step t(s)
			//51

			m_wndDetailGrid.m_nWidth[8]	= int(width* 7.0f);		//Vtg(V)
			m_wndDetailGrid.m_nWidth[9]	= int(width* 7.0f);		//Crt(㎃)
			m_wndDetailGrid.m_nWidth[10]= int(width* 7.0f);		//Cap(㎃h)
			m_wndDetailGrid.m_nWidth[11] =int(width* 7.0f);		//Impedance Field
			m_wndDetailGrid.m_nWidth[12] =int(width* 7.0f);		//Watt(㎽)
			m_wndDetailGrid.m_nWidth[13] =int(width* 7.0f);		//WattH(mWh)
			m_wndDetailGrid.m_nWidth[14] =int(width* 7.0f);		//temp
			//49
		}
	}
}

BOOL CDetailChannelView::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	StopMonitoring();
	return CFormView::DestroyWindow();
}

void CDetailChannelView::OnGetdispinfoChannelModuleTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	*pResult = 0;
}

void CDetailChannelView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CDetailChannelView::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

//Group State changed
void CDetailChannelView::UpdateTreeState(int /*nModuleID*/, int /*nGroupIndex*/)
{
	m_pTreeCtrlX->Invalidate();
	SetModuleData();
}

//Contol Context Menu를 보여 준다.
LRESULT CDetailChannelView::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);
	
	ASSERT(pGrid);
	if(nCol<2 || nRow < 1)	return 0;
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		GetClientRect(rect);
		ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}

//	if(pGrid == (CMyGridWnd *)&m_wndDetailGrid)
//	{
		CMenu menu;
		VERIFY(menu.LoadMenu(IDR_CH_CONTROL_CONTEXT));

		CMenu* pPopup = menu.GetSubMenu(0);
		ASSERT(pPopup != NULL);
		CWnd* pWndPopupOwner = this;

		while (pWndPopupOwner->GetStyle() & WS_CHILD)
			pWndPopupOwner = pWndPopupOwner->GetParent();

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
//	}
	return TRUE;
}
/*
//Channel grid의 선택된 Channel Array 를 얻는다.
int CDetailChannelView::GetSelChannelArray()
{
	// TODO: Add your control notification handler code here
	int nTotRow = m_wndDetailGrid.GetRowCount();
	if(nTotRow <=0 )	return;

	CRowColArray	awRows;
	int nTotSelCell=0;

	m_wndDetailGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();
	
	for (int i = 0; i< nSelNo; i++)
	{
		if(awRows[i] >0 && awRows[i] <= nTotRow)
		{
			m_awSelChArray[nTotSelCell++] = m_nCurGroup<<8|(awRows[i]-1);		//Make selected channel Index Save
		}
	}
	return nTotSelCell;


/*	CGXRangeList *rangeList = m_wndDetailGrid.GetParam()->GetRangeList();
	int nSelCellCount = rangeList->GetCount();
	POSITION		pos;
	ROWCOL nRow, nCol;
	BOOL	nRtn;
	int nChannelIndex = 0, nTotSelCell=0;
	CGXRange *SelCell;
	
	if(nSelCellCount == 0)		//Only One Cell is Selected
	{
		if(m_wndDetailGrid.GetCurrentCell(nRow, nCol))
		{
			if(nRow > 0 || nCol >0)
			{
				nChannelIndex = nRow-1;
				ASSERT(nChannelIndex < MAX_CH_PER_MD && nChannelIndex >=0);
				m_awSelChArray[nTotSelCell++] = (WORD)((nChannelIndex/MAX_CH_PER_MD+1)<<8)|(WORD)((nChannelIndex%MAX_CH_PER_MD)+1);
			}
		}
	}
	memset(m_awSelChArray, 0, sizeof(WORD)*MAX_CH_PER_MD);
	
	for(pos = rangeList->GetHeadPosition(); pos != NULL; )
	{
		SelCell = (CGXRange *)rangeList->GetAt(pos);
		ASSERT(SelCell);
		nRtn = SelCell->GetFirstCell(nRow, nCol);
		while(nRtn)
		{
			if(nRow > 0 || nCol >1)
			{
				nChannelIndex = nRow-1;
				if(nChannelIndex < MAX_CH_PER_MD && nChannelIndex>=0)
				{
					m_awSelChArray[nChannelIndex]++;
//					m_awSelChArray[nTotSelCell++] = (WORD)((nChannelIndex/CH_PER_BD+1)<<8)|(WORD)((nChannelIndex%CH_PER_BD)+1);
				}
			}
			nRtn = SelCell->GetNextCell(nRow, nCol);
		}
	   rangeList->GetNext(pos); 
	}

	for(INDEX i = 0; i<MAX_CH_PER_MD; i++)
	{
		if(m_awSelChArray[i])
			m_awSelChArray[nTotSelCell++] = (WORD)((i/MAX_CH_PER_MD)<<8)|(WORD)(i%MAX_CH_PER_MD);
	}

#ifdef _DEBUG
	char szTemp[128];
	for(i = 0; i<nTotSelCell; i++)
	{
		sprintf(szTemp, "Sel Ch List :: B %d - C %d", HIBYTE(m_awSelChArray[i]), LOBYTE(m_awSelChArray[i]));
//		WriteLog(szTemp);
	}
#endif //_DEBUG

	return nTotSelCell;
}
*/

// -----------------------------------------------------------------------------
//  [5/7/2009 kky]
// DESC : 공정에서 진행중인 지그의 수만큼 선택가능한 버튼을 생성
// -----------------------------------------------------------------------------
void CDetailChannelView::GroupSetChanged(int nColCount)
{
	m_nTrayColCount = nColCount;
	DrawDetailGrid();

	CFormModule *pModule = GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		int nCnt = pModule->GetTotalJig();
		if(nCnt == 1)
		{
			GetDlgItem(IDC_RADIO1)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_RADIO6)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_RADIO7)->ShowWindow(SW_HIDE);

			int nPrevIndex = m_nCurTrayIndex;
			m_nCurTrayIndex = 0;

			((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE);
			((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_RADIO6))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_RADIO7))->SetCheck(FALSE);
			
			if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
			{
				DrawDetailGrid();
			}
		}
		else if(nCnt == 2)
		{
			GetDlgItem(IDC_RADIO1)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO2)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO6)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_RADIO7)->ShowWindow(SW_HIDE);
			if(m_nCurTrayIndex > 1)	
			{
				int nPrevIndex = m_nCurTrayIndex;
				m_nCurTrayIndex = 0;
				((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE);
				((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(FALSE);
				((CButton *)GetDlgItem(IDC_RADIO6))->SetCheck(FALSE);
				((CButton *)GetDlgItem(IDC_RADIO7))->SetCheck(FALSE);
			
				if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
				{
					DrawDetailGrid();
				}
			}
		}
		else if(nCnt == 3)
		{
			GetDlgItem(IDC_RADIO1)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO2)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO6)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO7)->ShowWindow(SW_HIDE);
			if(m_nCurTrayIndex > 2)	
			{
				int nPrevIndex = m_nCurTrayIndex;
				m_nCurTrayIndex = 0;
				((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE);
				((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(FALSE);
				((CButton *)GetDlgItem(IDC_RADIO6))->SetCheck(FALSE);
				((CButton *)GetDlgItem(IDC_RADIO7))->SetCheck(FALSE);

				if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
				{
					DrawDetailGrid();
				}
			}
		}
		else //if(nCnt == 4)
		{
			GetDlgItem(IDC_RADIO1)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO2)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO6)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RADIO7)->ShowWindow(SW_SHOW);
			if(m_nCurTrayIndex > 3)
			{
				int nPrevIndex = m_nCurTrayIndex;
				m_nCurTrayIndex = 0;
				((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE);
				((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(FALSE);
				((CButton *)GetDlgItem(IDC_RADIO6))->SetCheck(FALSE);
				((CButton *)GetDlgItem(IDC_RADIO7))->SetCheck(FALSE);
		
				if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
				{
					DrawDetailGrid();
				}
			}
		}
	}
}

void CDetailChannelView::StartMonitoring()
{
	//현재 모듈이 OffLine 상태이면 사용 안함
	if(EPGetGroupState(m_nCurModuleID, m_nCurGroup) == EP_STATE_LINE_OFF)	
		return;
	
	//현재 Window가 Active 되어 있지 않으면 사용 안함 
//	if(((CMainFrame *)AfxGetMainWnd())->m_nCurTabIndex != 2)
	if(((CMainFrame *)AfxGetMainWnd())->CompareCurrentSelectTab(CMainFrame::TAB_INDEX_DETAIL) == FALSE)
		return;

	if(m_nDisplayTimer)	return;

	if(m_pConfig)
		m_nDisplayTimer = SetTimer(104, m_pConfig->m_ChannelInterval*1000, NULL);
}

void CDetailChannelView::StopMonitoring()
{
	if(m_nDisplayTimer == 0)	return;
	KillTimer(m_nDisplayTimer);	
	m_nDisplayTimer = 0;
}

///
void CDetailChannelView::ModuleConnected(int nModuleID)
{
	GetDocument()->DrawTree(m_pTreeCtrlX, nModuleID);
	if(nModuleID == m_nCurModuleID)
	{
		// 최초에 먼저 세팅을 변경한다.
		GroupSetChanged(m_nTrayColCount);

		//현재 Window가 Active 되어 있지 않으면 사용 안함 
		//if(((CMainFrame *)AfxGetMainWnd())->m_nCurTabIndex != 2)
		if(((CMainFrame *)AfxGetMainWnd())->CompareCurrentSelectTab(CMainFrame::TAB_INDEX_DETAIL) == FALSE)	
			return;

		GetDocument()->SetAutoReport(nModuleID);
		m_nCurGroup = 0;
		//GroupSetChanged(m_nTopGridColCount);
		StartMonitoring();
	}
}

///
void CDetailChannelView::TopConfigChanged()
{
	SetGridColor();
	if(m_nDisplayTimer)
	{
		StopMonitoring();
		StartMonitoring();
	}
}

///
void CDetailChannelView::SetGridColor()
{
	ASSERT(m_pConfig);

	//State Color 
	for (int nI = 0; nI < STATE_COLOR; nI++)
	{
		m_wndDetailGrid.m_ColorArray[nI].BackColor = m_pConfig->m_BStateColor[nI];
		m_wndDetailGrid.m_ColorArray[nI].TextColor = m_pConfig->m_TStateColor[nI];
	}
	m_wndDetailGrid.Redraw();
}


BOOL CDetailChannelView::UpdateModuleSet()
{
	m_bAutoProcess = EPGetAutoProcess(m_nCurModuleID);
	UpdateData(FALSE);
	return TRUE;
}

void CDetailChannelView::OnAutoProcess() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CWordArray awRows;
	awRows.Add(m_nCurModuleID);

	CCTSMonDoc *pDoc = GetDocument();
	pDoc->SendAutoProcessOn(awRows, m_bAutoProcess);

/*	CString strTemp;
	CCTSMonDoc *pDoc = GetDocument();
	if(m_bAutoProcess)
	{
		strTemp.Format(GetStringTable(IDS_MSG_CHANGE_AUTO_PROCESS), ::GetModuleName(m_nCurModuleID));
	}
	else
	{
		strTemp.Format(GetStringTable(IDS_MSG_CHANGE_MANUAL_PROCESS), ::GetModuleName(m_nCurModuleID));
	}

	if(MessageBox(strTemp, "Auto Process", MB_ICONQUESTION|MB_YESNO) == IDYES)
	{	
/*		int nRtn;
		//Linux에서 Standby 상태에서도 변경 가능하게 바꿔야 한다.	==> 2004/3/26 동부 파인셀부터 변경됨
		if(EPGetGroupState(m_nCurModuleID, 0) != EP_STATE_IDLE)
		{
			nRtn = EPSendCommand(m_nCurModuleID, 1, 0, EP_CMD_CLEAR);//, &nvRamSetData, sizeof(EP_NVRAM_SET));
			if(nRtn != EP_ACK)
			{
				strTemp.Format("%s Auto Process Setting Fail... (%s)", ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));
	//			AfxMessageBox(strTemp);
			}
			Sleep(500);
		}
		
		WORD state = EPGetGroupState(m_nCurModuleID, 0);
		if(state != EP_STATE_IDLE && state != EP_STATE_READY && state != EP_STATE_STANDBY && state != EP_STATE_END)
		{
			strTemp.Format(GetStringTable(IDS_MSG_STATE_ERROR), ::GetModuleName(m_nCurModuleID));
			AfxMessageBox(strTemp);
			return;
		}

		EP_NVRAM_SET	nvRamSetData;
		nvRamSetData.nUseNVRam = m_bAutoProcess;
		nRtn = EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_NVRAM_SET, &nvRamSetData, sizeof(EP_NVRAM_SET));
		if(nRtn != EP_ACK)
		{
			strTemp.Format("%s Auto Process Setting Fail... (%s)", ::GetModuleName(m_nCurModuleID), pDoc->CmdFailMsg(nRtn));
			AfxMessageBox(strTemp);
		}
		else
		{
*/
//			EPSetAutoProcess(m_nCurModuleID, m_bAutoProcess);
//			pDoc->SaveAutoProcSetToDB(m_nCurModuleID, m_bAutoProcess);
//		}
//	}
	
	m_bAutoProcess = EPGetAutoProcess(m_nCurModuleID);
	if(m_bAutoProcess)
	{
		m_AutoProcD.SetText(GetStringTable(IDS_TEXT_AUTO_MODE));
	}
	else
	{
		m_AutoProcD.SetText(GetStringTable(IDS_TEXT_MANUAL_MODE));
	}
	UpdateData(FALSE);

}

void CDetailChannelView::OnInitAtuoProcess() 
{
	// TODO: Add your control notification handler code here
	((CCTSMonDoc *)GetDocument())->SendInitCommand(m_nCurModuleID);

}

void CDetailChannelView::OnRegTray() 
{
	// TODO: Add your control notification handler code here
	CCTSMonDoc	*pDoc = (CCTSMonDoc *)GetDocument();
	int nModuleIndex = EPGetModuleIndex(m_nCurModuleID);
	if(nModuleIndex <0 || nModuleIndex >= pDoc->GetInstalledModuleNum())	return;
	if(m_nCurGroup < 0 && m_nCurGroup >= EPGetGroupCount(m_nCurModuleID))	return;

	CString strTemp;
//	long lDefaultSerial = 0;

	//NVRAM 이용시는 Tray가 Laoding되어 있어야 등록이 가능하다.
#if TRAY_DATA_NVRAM
	if(EPGetModuleState(m_nCurModuleID) == EP_STATE_LINE_OFF)
	{
		strTemp.Format(GetStringTable(IDS_MSG_NOT_CONNECTED), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		return;
	}

	WORD state = EPTrayState(m_nCurModuleID, m_nCurGroup);
	if( state != EP_TRAY_LOAD)
	{
		strTemp.Format(GetStringTable(IDS_MSG_TRAY_NOT_INSERTED), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		return;
	}
	

	EP_TRAY_SERIAL	traySerial;
	LPVOID	pBuffer;		//Module에 Tray Serial 요청 
	int nSize = sizeof(EP_TRAY_SERIAL);
	if((pBuffer =  EPSendDataCmd(m_nCurModuleID, m_nCurGroup+1, 0, EP_CMD_TRAY_SERIAL_REQUEST, nSize)) != NULL)	
	{
		memcpy(&traySerial, pBuffer, sizeof(EP_TRAY_SERIAL));
		lDefaultSerial = atoi(traySerial.szTraySerialNo);
		strTemp.Format("%s", traySerial.szTrayNo);
	}
#endif
	
	CRegTrayDlg	*pDlg;
	pDlg = new CRegTrayDlg(this);
	ASSERT(pDlg);
//	pDlg->m_TrayData.lTraySerial = lDefaultSerial;
	pDlg->m_strTrayNo = strTemp;

	if(pDlg->DoModal() != IDOK)		//DataBase에 Tray 등록 
	{
		delete pDlg;	pDlg = NULL;
		return ;
	}
	//NVRam 이용시 사용자가 입력한 Tray 정보를 module에 전송하여 기록한다.
#if TRAY_DATA_NVRAM
	EP_TRAY_SERIAL	*lpData = new EP_TRAY_SERIAL;
	strcpy(lpData->szTraySerialNo, pDlg->m_strTraySerial);
	strcpy(lpData->szTrayNo, pDlg->m_strTrayNo);
	delete pDlg;
	pDlg = NULL;


	int nRtn;	//NVRAM에 Tray Serial No 기록 
	if((nRtn = EPSendCommand(m_nCurModuleID, m_nCurGroup+1, 0, EP_CMD_SET_TRAY_SERIAL, lpData, sizeof(EP_TRAY_SERIAL))) != EP_ACK)
	{
		strTemp.Format(GetStringTable(IDS_MSG_TRAY_REG_ERROR), ::GetModuleName(m_nCurGroup, m_nCurGroup), lpData->szTrayNo);
		strTemp = strTemp + " (" + pDoc->CmdFailMsg(nRtn) +")";
		pDoc->WriteLog((LPSTR)(LPCTSTR)strTemp);
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		delete lpData;
		lpData = NULL;
		return ;
	}
	delete lpData;
	lpData = NULL;
#endif

}

BOOL CDetailChannelView::SetCurrentModule(int nModuleID, int nGroupIndex)
{
	if(nModuleID != m_nCurModuleID)		// 선택된 모듈과 이전의 모듈값을 비교
	{
		int index = EPGetModuleIndex(nModuleID);
		HTREEITEM hItem = m_pTreeCtrlX->GetRootItem();
		for(int i =0; i< index; i++)
		{
			hItem =  m_pTreeCtrlX->GetNextSiblingItem(hItem);
			if(hItem == NULL)	break;
		}
		if(hItem != NULL)
		{
			m_pTreeCtrlX->SelectItem(hItem);
		}

		m_nCurModuleID = nModuleID;
		
		StopMonitoring();
		GroupSetChanged(m_nTrayColCount);
		StartMonitoring();
		SetModuleData();
		//SetChannelData();
	}
	return TRUE;
}

BOOL CDetailChannelView::SetCmdFlag(int cmd)
{
	CCTSMonDoc *pDoc = GetDocument();

	if(pDoc->m_nCmdInterval <= 0)	return TRUE;

	switch(cmd)		//Timer Event 가 발생 하지 않은 경우도 있는것 같아 다른 Command 상태를 다시 reset 시킨다.
	{
	case EP_CMD_RUN:
		if(pDoc->m_bRunCmd)
			return TRUE;
		
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = TRUE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_RESTART:
		if(pDoc->m_bContinueCmd == TRUE)	return TRUE;
		pDoc->m_bContinueCmd = TRUE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_STOP:
		if(pDoc->m_bStopCmd)	return TRUE;
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = TRUE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_CLEAR:	
		if(pDoc->m_bInitCmd)	return TRUE;
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = TRUE;
		pDoc->m_bRunCmd = FALSE;	
		pDoc->m_bNextStepCmd = FALSE;
		break;

	case EP_CMD_NEXTSTEP:
		if(pDoc->m_bNextStepCmd)	return TRUE;
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;
		pDoc->m_bNextStepCmd = TRUE;
		break;

	default:
		pDoc->m_bContinueCmd = FALSE;
		pDoc->m_bStopCmd = FALSE;
		pDoc->m_bInitCmd = FALSE;
		pDoc->m_bRunCmd = FALSE;	
		pDoc->m_bNextStepCmd = FALSE;
		return TRUE;
	}
		
	SetTimer(cmd, pDoc->m_nCmdInterval, NULL);
	return TRUE;
}

BOOL CDetailChannelView::ResetCmdFlag(int cmd)
{
	KillTimer(cmd);
	CCTSMonDoc *pDoc = GetDocument();

	switch(cmd)
	{
	case EP_CMD_RUN:		pDoc->m_bRunCmd = FALSE;		break;
	case EP_CMD_RESTART:	pDoc->m_bContinueCmd = FALSE;	break;
	case EP_CMD_STOP:		pDoc->m_bStopCmd	= FALSE;	break;
	case EP_CMD_CLEAR:		pDoc->m_bInitCmd = FALSE;		break;
	case EP_CMD_NEXTSTEP:	pDoc->m_bNextStepCmd = FALSE;	break;
	default:
		return TRUE;
	}
	return TRUE;
}

void CDetailChannelView::OnReconnect() 
{
	// TODO: Add your command handler code here
	CString strTemp;

	if(PermissionCheck(PMS_MODULE_SHUT_DOWN) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [System Initializ]");
		return;
	}
	
	strTemp.Format(GetStringTable(IDS_MSG_SYS_INI_CONFIRM), ::GetModuleName(m_nCurModuleID, 0));
	if(MessageBox(strTemp, "System Initialize", MB_ICONSTOP|MB_YESNO) == IDYES)
	{
		SendCommand(EP_CMD_SYSTEM_INIT);
	}			
}

void CDetailChannelView::OnUpdateReconnect(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CDetailChannelView::GroupCellSetting(int nFromCh, int nToCh, COLORREF color, int nWidth)
{
	if(nFromCh <1 || nToCh > MAX_CH_PER_MD || nToCh < nFromCh)	return;

	ROWCOL	nFromRow, nFromCol, nToRow, nToCol;

	nFromRow = nFromCh;
	nFromCol = 1;
	if(nToCh <= m_wndDetailGrid.GetRowCount())
		nToRow = nToCh;
	else
		nToRow = m_wndDetailGrid.GetRowCount();
	nToCol = m_wndDetailGrid.GetColCount();

	m_wndDetailGrid.SetStyleRange(CGXRange(nFromRow, nFromCol, nToRow, nFromCol), CGXStyle().SetBorders(gxBorderLeft, CGXPen().SetWidth(nWidth).SetColor(color)));
	m_wndDetailGrid.SetStyleRange(CGXRange(nFromRow, nFromCol, nFromRow, nToCol), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(nWidth).SetColor(color)));
	m_wndDetailGrid.SetStyleRange(CGXRange(nToRow, nFromCol, nToRow, nToCol), CGXStyle().SetBorders(gxBorderBottom, CGXPen().SetWidth(nWidth).SetColor(color)));
	m_wndDetailGrid.SetStyleRange(CGXRange(nFromRow, nToCol, nToRow, nToCol), CGXStyle().SetBorders(gxBorderRight, CGXPen().SetWidth(nWidth).SetColor(color)));
}

void CDetailChannelView::OnTraynoUserInput() 
{
	// TODO: Add your command handler code here
	((CMainFrame *)AfxGetMainWnd())->UserInputTrayNo(m_nCurModuleID);	
}

void CDetailChannelView::OnUpdateTraynoUserInput(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);

	if( (gpState == EP_STATE_STANDBY || gpState == EP_STATE_END || gpState == EP_STATE_READY || gpState == EP_STATE_IDLE) 
		&& EPGetAutoProcess(m_nCurModuleID)
		&& GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup) == EP_OFFLINE_MODE)			
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{	pCmdUI->Enable(FALSE);

	}		
}

void CDetailChannelView::OnMaintenanceMode() 
{
	// TODO: Add your command handler code here
	if(PermissionCheck(PMS_GROUP_CONTROL_CMD) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Change to Maintenance]");
		return ;
	}

	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;

	strTemp.Format(GetStringTable(IDS_MSG_CHANGE_MAINTEN_CONFIRM), ::GetModuleName(m_nCurModuleID, m_nCurGroup));
	if(MessageBox(strTemp, "Mode Change", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;

	pDoc->SetControlMode(m_nCurModuleID, m_nCurGroup, EP_MAINTENANCE_MODE);		
}

void CDetailChannelView::OnUpdateMaintenanceMode(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	//Maintenance/Control Mode는 충방전 Program이 아니라 다른 외부 프로그램을 이용하도록 변경
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	int nLineMode = GetDocument()->GetLineMode(m_nCurModuleID, m_nCurGroup);
	if(((gpState == EP_STATE_IDLE || gpState == EP_STATE_READY) && nLineMode == EP_OFFLINE_MODE) ||
		(gpState != EP_STATE_RUN && nLineMode == EP_ONLINE_MODE))		//Sensor State Check Mode
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

void CDetailChannelView::OnReboot() 
{
	// TODO: Add your command handler code here
	CRebootDlg	*pDlg;
	pDlg = new CRebootDlg(this);
	ASSERT(pDlg);

	if(EPGetGroupState(m_nCurModuleID, 0) == EP_STATE_RUN)
	{
		CString strTemp;
		strTemp.Format(GetStringTable(IDS_MSG_REBOOT_CONFIRM), 
			::GetModuleName(m_nCurModuleID));
		if(MessageBox(strTemp, "Running...", MB_ICONQUESTION|MB_YESNO) != IDYES)
			return;
	}

	pDlg->SetModuleName(::GetModuleName(m_nCurModuleID), GetDocument()->GetModuleIPAddress(m_nCurModuleID));
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;	
}

int CDetailChannelView::GetCurModuleID()
{
	return m_nCurModuleID;
}

void CDetailChannelView::OnFileSave() 
{
	// TODO: Add your command handler code here
	CString FileName;
	FileName = ::GetModuleName(m_nCurModuleID, m_nCurGroup);
	CFileDialog pDlg(FALSE, "", FileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return;
	}

	FILE *fp = fopen(FileName, "wt");
	if(fp == NULL)	return;

	CString strData;
	m_ModuleNoD.GetWindowText(strData);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_TEXT_MODULE_NO), strData);

	m_StateD.GetWindowText(strData);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_STATUS), strData);

	strData.Format("%s,%s", ::GetStringTable(IDS_LABEL_TRAY_NO), GetDocument()->GetTrayNo(m_nCurModuleID, m_nCurGroup));
	fprintf(fp, "%s\n", strData);

	strData.Format("%s,%s", ::GetStringTable(IDS_LABEL_LOT_NO), GetDocument()->GetLotNo(m_nCurModuleID, m_nCurGroup));
	fprintf(fp, "%s\n", strData);

	m_TestNameD.GetWindowText(strData);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_PROC_NAME), strData);

	m_AutoProcD.GetWindowText(strData);
	fprintf(fp, "Auto Processing,%s\n", strData);

	m_DoorStateD.GetWindowText(strData);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_DOOR_STATE), strData);

	m_TotalTimeD.GetWindowText(strData);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_TEXT_TOTAL_TIME), strData);

	m_UserIDD.GetWindowText(strData);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_OPERATOR), strData);

	GetDlgItem(IDC_CHANNEL_FILENAME_D)->GetWindowText(strData);
	fprintf(fp, "%s,%s\n", ::GetStringTable(IDS_LABEL_FILE_NAME), strData);

	m_ctrlNormal.GetWindowText(strData);
	fprintf(fp, "%s,%s,", ::GetStringTable(IDS_LABEL_NORMAL), strData);

	m_ctrlError.GetWindowText(strData);
	fprintf(fp, "%s,%s\n\n", ::GetStringTable(IDS_LABEL_BAD), strData);

	int nRow = m_wndDetailGrid.GetRowCount();
	int nCol =  m_wndDetailGrid.GetColCount();

	for(int row = 0; row < nRow; row++)
	{
		for(int col = 1; col <= nCol; col++)
		{
			strData = m_wndDetailGrid.GetValueRowCol(row, col);
			fprintf(fp, "%s,", strData);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);	
}

void CDetailChannelView::OnRefAdValueUpdate() 
{
	// TODO: Add your command handler code here
	CCTSMonDoc *pDoc = GetDocument();
	CString strTemp;

	strTemp.Format(GetStringTable(IDS_MSG_REF_UPDATE_CONFIRM), pDoc->m_strModuleName);
	if(MessageBox(strTemp, "Ref IC Update", MB_ICONQUESTION|MB_YESNO) != IDYES)	return;
	
	//Dialog Permission Check
	if(LoginPremissionCheck(PMS_MODULE_CAL_UPDATE) == FALSE)
	{
		AfxMessageBox(GetStringTable(IDS_LANG_MSG_PERMISSION_ERROR) +" [Reference IC Data Update]");
		return ;
	}


	WORD state;
//	LPVOID lpData = NULL;
	EP_MD_SYSTEM_DATA *lpSysData;

	lpSysData = EPGetModuleSysData(EPGetModuleIndex(m_nCurModuleID));
	if(lpSysData == NULL)	return;

	//Version 검사
/*	if(lpSysData->nVersion < 0x3003)
	{
		strTemp.Format(::GetStringTable(IDS_MSG_NOT_SUPPORT_VERSION), ::GetModuleName(m_nCurModuleID, m_nCurGroup), lpSysData->nVersion);
		AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
		return;
	}
*/			
	state = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	//상태 검사 
	if(state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_END)
	{
		int nRtn=EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_CAL_EXEC);
		if(nRtn != EP_ACK)
		{
			strTemp.Format("%s (%s-%s)",  ::GetStringTable(IDS_TEXT_CMD_SEND_FAIL), ::GetModuleName(m_nCurModuleID, 0), pDoc->CmdFailMsg(nRtn));
			AfxMessageBox(strTemp);
		}
/*		lpData = EPSendDataCmd(m_nCurModuleID, 0, 0, EP_CMD_CAL_EXEC, sizeof(EP_REF_IC_DATA));
		if(lpData != NULL)
		{
			//응답으로 현재 Update 시킨 값이 오는데 활용 안함 
			LPEP_REF_IC_DATA lpRefData = new EP_REF_IC_DATA;
			ASSERT(lpRefData);
			ZeroMemory(lpRefData, sizeof(EP_REF_IC_DATA));
			memcpy(lpRefData, lpData, sizeof(EP_REF_IC_DATA));
			
			  delete lpRefData;
			lpRefData = NULL;

		}
*/	}	
}

void CDetailChannelView::OnUpdateRefAdValueUpdate(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
/*	EP_MD_SYSTEM_DATA *lpSysData;
	lpSysData = EPGetModuleSysData(EPGetModuleIndex(m_nCurModuleID));
	if(lpSysData == NULL)	return;
*/
	WORD state = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	
//	if(PermissionCheck(PMS_MODULE_CAL_UPDATE) && lpSysData != NULL && lpSysData->nVersion >= 0x3003
//		&& (state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_END))
	if(PermissionCheck(PMS_MODULE_CAL_UPDATE) && (state == EP_STATE_STANDBY || state == EP_STATE_IDLE || state == EP_STATE_READY || state == EP_STATE_END))
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

void CDetailChannelView::OnRclickChannelModuleTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
    CPoint point, point2;
    ::GetCursorPos(&point);
	point2 = point;
    m_pTreeCtrlX->ScreenToClient(&point);
	UINT nFlags;
    HTREEITEM m_hTreeItem = m_pTreeCtrlX->HitTest(point, &nFlags);

	if(m_hTreeItem == NULL)	return;

	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		m_pTreeCtrlX->GetClientRect(rect);
		m_pTreeCtrlX->ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}

	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_CONTEXT1));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	CWnd* pWndPopupOwner = this;

	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	if(m_pTreeCtrlX->ItemHasChildren(m_pTreeCtrlX->GetSelectedItem()))
	{
		m_nCmdTarget = EP_MODULE_CMD;
	}
	else
	{
		m_nCmdTarget = EP_GROUP_CMD;
	}
	
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point2.x, point2.y, pWndPopupOwner);

	*pResult = 0;
}

void CDetailChannelView::ActiveAutoReport()
{
	GetDocument()->SetAutoReport(GetCurModuleID());
}

void CDetailChannelView::DataUnitChanged()
{
	CString str;

	m_wndDetailGrid.SetValueRange(CGXRange(0,1),  ::GetStringTable(IDS_LABEL_CHANNEL));
	m_wndDetailGrid.SetValueRange(CGXRange(0,2),  ::GetStringTable(IDS_LABEL_CELL_NO));
	m_wndDetailGrid.SetValueRange(CGXRange(0,3),  ::GetStringTable(IDS_LABEL_STEP));
	m_wndDetailGrid.SetValueRange(CGXRange(0,4),  ::GetStringTable(IDS_LABEL_STATUS));
	m_wndDetailGrid.SetValueRange(CGXRange(0,5),  "Code");
	m_wndDetailGrid.SetValueRange(CGXRange(0,6),  ::GetStringTable(IDS_LABEL_GRADE));
	if(GetDocument()->m_nTimeUnit == 1)	//sec
	{
		str.Format("%s(sec)", ::GetStringTable(IDS_LABEL_STEP_TIME));
	}
	else if(GetDocument()->m_nTimeUnit == 2)
	{
		str.Format("%s(min)", ::GetStringTable(IDS_LABEL_STEP_TIME));
	}
	else
	{
		str = ::GetStringTable(IDS_LABEL_STEP_TIME);
	}
	m_wndDetailGrid.SetValueRange(CGXRange(0,7),  str);
	str.Format("%s(%s)", ::GetStringTable(IDS_LABEL_VOLTAGE), GetDocument()->m_strVUnit);
	m_wndDetailGrid.SetValueRange(CGXRange(0,8),  str);
	str.Format("%s(%s)", ::GetStringTable(IDS_LABEL_CURRENT), GetDocument()->m_strIUnit);
	m_wndDetailGrid.SetValueRange(CGXRange(0,9),  str);
	str.Format("%s(%s)", ::GetStringTable(IDS_LABEL_CAPACITY), GetDocument()->m_strCUnit);
	m_wndDetailGrid.SetValueRange(CGXRange(0,10), str);

	str.Format("%s(%s)", ::GetStringTable(IDS_LABEL_POWER), GetDocument()->m_strWUnit);
	m_wndDetailGrid.SetValueRange(CGXRange(0,11), str);
	
	str.Format("%s(%s)", ::GetStringTable(IDS_LABEL_ENERGY), GetDocument()->m_strWhUnit);
	m_wndDetailGrid.SetValueRange(CGXRange(0,12), str);

	str.Format("%s(mOhm)", ::GetStringTable(IDS_LABEL_IMPEDANCE));
	m_wndDetailGrid.SetValueRange(CGXRange(0,13), str);
	
	str = "Temp.(D)";
	m_wndDetailGrid.SetValueRange(CGXRange(0,14), str);
}

void CDetailChannelView::OnDataViewButton() 
{
	// TODO: Add your control notification handler code here
	 GetDocument()->ViewResult(m_nCurModuleID);
}

void CDetailChannelView::OnGetProfileData() 
{
	// TODO: Add your command handler code here
	if(GetDocument()->DownLoadProfileData(m_nCurModuleID, GetDocument()->GetResultFileName(m_nCurModuleID, m_nCurTrayIndex)) == FALSE)
	{
		AfxMessageBox("결과 파일을 찾을 수 없습니다.");
	}	
}

void CDetailChannelView::OnUpdateGetProfileData(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(gpState != EP_STATE_LINE_OFF && ((CCTSMonApp *)AfxGetApp())->GetSystemType() != EP_ID_IROCV_SYSTEM)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

void CDetailChannelView::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	int nPrevIndex = m_nCurTrayIndex;
	m_nCurTrayIndex = 0;

	CFormModule *pModule =  GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
		{
			GroupSetChanged(m_nTrayColCount);
		}
	}

	SetModuleData();
	SetChannelData();
}

void CDetailChannelView::OnRadio2() 
{
	// TODO: Add your control notification handler code here
	int nPrevIndex = m_nCurTrayIndex;
	m_nCurTrayIndex = 1;
	CFormModule *pModule =  GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
		{
			GroupSetChanged(m_nTrayColCount);
		}
	}

	SetModuleData();
	SetChannelData();	
}

void CDetailChannelView::OnRadio6() 
{
	// TODO: Add your control notification handler code here
	int nPrevIndex = m_nCurTrayIndex;
	m_nCurTrayIndex = 2;
	CFormModule *pModule =  GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
		{
			GroupSetChanged(m_nTrayColCount);
		}
	}

	SetModuleData();
	SetChannelData();		
}

void CDetailChannelView::OnRadio7() 
{
	// TODO: Add your control notification handler code here
	int nPrevIndex = m_nCurTrayIndex;
	m_nCurTrayIndex = 3;

	CFormModule *pModule =  GetDocument()->GetModuleInfo(m_nCurModuleID);
	if(pModule)
	{
		if(pModule->GetChInJig(nPrevIndex) != pModule->GetChInJig(m_nCurTrayIndex))
		{
			GroupSetChanged(m_nTrayColCount);
		}
	}
	SetModuleData();
	SetChannelData();	
}

void CDetailChannelView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndDetailGrid == (CMyGridWnd *)pWnd)
	{
		m_wndDetailGrid.Copy();
	}	
}

void CDetailChannelView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndDetailGrid == (CMyGridWnd *)pWnd)
	{
		pCmdUI->Enable(TRUE);
	}	
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDetailChannelView::OnViewResult() 
{
	// TODO: Add your command handler code here
	GetDocument()->ViewResult(m_nCurModuleID);
}

void CDetailChannelView::OnUpdateViewResult(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	// TODO: Add your command update UI handler code here
	WORD gpState = EPGetGroupState(m_nCurModuleID, m_nCurGroup);
	if(gpState != EP_STATE_LINE_OFF && ((CCTSMonApp *)AfxGetApp())->GetSystemType() != EP_ID_IROCV_SYSTEM)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}		
}
