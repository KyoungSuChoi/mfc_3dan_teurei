// SelEmgLogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "SelEmgLogDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelEmgLogDlg dialog


CSelEmgLogDlg::CSelEmgLogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelEmgLogDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CSelEmgLogDlg"));
	//{{AFX_DATA_INIT(CSelEmgLogDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSelEmgLogDlg::~CSelEmgLogDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CSelEmgLogDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

void CSelEmgLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelEmgLogDlg)
	DDX_Control(pDX, IDC_SEL_LIST, m_wndSelList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelEmgLogDlg, CDialog)
	//{{AFX_MSG_MAP(CSelEmgLogDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_SEL_LIST, OnDblclkSelList)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SEL_LIST, OnItemchangedSelList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelEmgLogDlg message handlers

BOOL CSelEmgLogDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	DWORD style = 	m_wndSelList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_wndSelList.SetExtendedStyle(style );

	m_wndSelList.InsertColumn(0, TEXT_LANG[0], LVCFMT_LEFT, 50); //"No"
	m_wndSelList.InsertColumn(1, TEXT_LANG[1], LVCFMT_LEFT, 350);//"File name"

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	char szName[32];

	CString strTemp;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp.Format("%s\\Log\\*_EMG.csv", CString(szCurDir).Left(CString(szCurDir).ReverseFind('\\')));

	CFileFind finder;
	BOOL bFind = finder.FindFile(strTemp);

	int nI = 0;
	while(bFind)
	{
		bFind = finder.FindNextFile();
		sprintf(szName, "%d", nI+1);
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
		m_wndSelList.InsertItem(&lvItem);
		m_wndSelList.SetItemData(lvItem.iItem, nI);		//==>LVN_ITEMCHANGED 를 발생 기킴 

		m_wndSelList.SetItemText(nI, 1, finder.GetFileName());
		nI++;
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

CString CSelEmgLogDlg::GetSelFileName()
{
	return m_strSelFileName;
}

void CSelEmgLogDlg::OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnOK() ;

	*pResult = 0;
}

void CSelEmgLogDlg::OnOk() 
{
	// TODO: Add your control notification handler code here

}

void CSelEmgLogDlg::OnItemchangedSelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	POSITION pos = m_wndSelList.GetFirstSelectedItemPosition();
	if(pos == NULL)	return;
	int nItem = m_wndSelList.GetNextSelectedItem(pos);
	m_strSelFileName = m_wndSelList.GetItemText(nItem, 1);
	
	*pResult = 0;
}

BOOL CSelEmgLogDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class

	return CDialog::PreTranslateMessage(pMsg);
}
