// FanModeSettingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "FanModeSettingDlg.h"


// FanModeSettingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(FanModeSettingDlg, CDialog)


FanModeSettingDlg::FanModeSettingDlg( CCTSMonDoc *pDoc, CWnd* pParent, int iTargetModuleNo )
: CDialog(FanModeSettingDlg::IDD, pParent), m_pDoc(pDoc), m_nCurModuleNo(iTargetModuleNo)
{
	m_nCurrentJigFanCount = 0;
	m_nCurrentClipFanCount = 0;
}

FanModeSettingDlg::~FanModeSettingDlg()
{
}

void FanModeSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_COMBO_STAGE_LIST, m_combo_Stage_List);
}


BEGIN_MESSAGE_MAP(FanModeSettingDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_FAN_MODE_SETTING_APPLY, &FanModeSettingDlg::OnBnClickedBtnFanModeSettingApply)
	ON_BN_CLICKED(IDC_BTN_FAN_MODE_SETTING_REFRESH, &FanModeSettingDlg::OnBnClickedBtnFanModeSettingRefresh)
END_MESSAGE_MAP()

void FanModeSettingDlg::InitializeGrid()
{
	m_wndFanModeSettingGrid.SubclassDlgItem(IDC_FAN_MODE_SETTING_GRID, this);
	m_wndFanModeSettingGrid.m_bSameColSize  = FALSE;
	m_wndFanModeSettingGrid.m_bSameRowSize  = FALSE;
	m_wndFanModeSettingGrid.m_bCustomWidth  = TRUE;

	m_wndFanModeSettingGrid.Initialize();

	this->InitFirstRow();
	this->InitDefaultRows();	
}

BOOL FanModeSettingDlg::InitFirstRow()
{
	try
	{
		BOOL bLock = m_wndFanModeSettingGrid.LockUpdate();	
		m_wndFanModeSettingGrid.SetColCount(3);
		m_wndFanModeSettingGrid.m_BackColor	= RGB(255,255,255);

		m_wndFanModeSettingGrid.SetValueRange(CGXRange(0,FAN_MODE_SETTING_FAN_TYPE),		"Fan Type"); //
		m_wndFanModeSettingGrid.SetValueRange(CGXRange(0,FAN_MODE_SETTING_FAN_NAME),		"Fan Name");
		m_wndFanModeSettingGrid.SetValueRange(CGXRange(0,FAN_MODE_CURRENT_FAN_SETTING),		"Setting");

		m_wndFanModeSettingGrid.SetStyleRange(CGXRange().SetCols(FAN_MODE_CURRENT_FAN_SETTING), 
			CGXStyle()
			.SetControl(GX_IDS_CTRL_CHECKBOX3D)
			);

		CRect rectGrid;
		float width;
		m_wndFanModeSettingGrid.GetClientRect(rectGrid);
		width = (float)rectGrid.Width()/100.0f;

		m_wndFanModeSettingGrid.m_nWidth[FAN_MODE_SETTING_FAN_TYPE]			= int(width* 40.0f); // Board
		m_wndFanModeSettingGrid.m_nWidth[FAN_MODE_SETTING_FAN_NAME]			= int(width* 40.0f);
		m_wndFanModeSettingGrid.m_nWidth[FAN_MODE_CURRENT_FAN_SETTING]		= int(width* 20.0f);

		m_wndFanModeSettingGrid.SetDefaultRowHeight(20);
		m_wndFanModeSettingGrid.GetParam()->GetProperties()->SetDisplayHorzLines(TRUE);
		m_wndFanModeSettingGrid.GetParam()->GetProperties()->SetDisplayVertLines(TRUE);	

		m_wndFanModeSettingGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
		m_wndFanModeSettingGrid.SetScrollBarMode(SB_HORZ, gxnDisabled);
		m_wndFanModeSettingGrid.EnableCellTips();

		m_wndFanModeSettingGrid.LockUpdate(bLock);
	}	
	catch (CException* e)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL FanModeSettingDlg::ClearGrid()
{
	InitDefaultRows();

	return TRUE;
}

void FanModeSettingDlg::SetCurrentModuleNo( int nModuleNo )
{
	this->m_nCurModuleNo = nModuleNo;
	m_combo_Stage_List.SetCurSel(m_nCurModuleNo - 1);

	OnBnClickedBtnFanModeSettingRefresh();
}

void FanModeSettingDlg::OnCbnSelchangeComboStageList()
{
	int nIndex = m_combo_Stage_List.GetCurSel();

	if(nIndex >= 0)
	{
		if(nIndex + 1 != m_nCurModuleNo)
		{
			InitDefaultRows();
			m_nCurModuleNo = nIndex+1;

			OnBnClickedBtnFanModeSettingRefresh();
		}
	}
}

void FanModeSettingDlg::OnFanModeSettingResponse( int nModuleNo, EP_FAN_INFORMATION_RESPONSE response )
{
	if(nModuleNo <= 0 || nModuleNo > m_pDoc->GetInstalledModuleNum())
	{
		return;
	}

	InitDefaultRows(response.nJigFanCount, response.nClipFanCount);

	BOOL		bLock = m_wndFanModeSettingGrid.LockUpdate();

	int nTotalFanCount = response.nJigFanCount + response.nClipFanCount;
	// Plus Minus Sensing Value Display And Check
	for(int i = 0; i < nTotalFanCount; i++)
	{
		if(i < response.nJigFanCount)
		{
			CString csTemp;
			if(response.pJigFanEnableStatus[i] == TRUE)
			{
				csTemp = "1";
			}
			else
			{
				csTemp = "0";
			}

			m_wndFanModeSettingGrid.SetValueRange(CGXRange(i+1,FAN_MODE_CURRENT_FAN_SETTING), csTemp);
		}
		else if(i - response.nJigFanCount < response.nClipFanCount)
		{
			CString csTemp;
			if(response.pClipFanEnableStatus[i - response.nJigFanCount] == TRUE)
			{
				csTemp = "1";
			}
			else
			{
				csTemp = "0";
			}

			m_wndFanModeSettingGrid.SetValueRange(CGXRange(i+1,FAN_MODE_CURRENT_FAN_SETTING), csTemp);
		}
		else
		{

		}
	}

	m_wndFanModeSettingGrid.LockUpdate(bLock);
	m_wndFanModeSettingGrid.Redraw();
}

void FanModeSettingDlg::InitializeComboList()
{
	int nModuleNo;

	for(int i=0; i<m_pDoc->GetInstalledModuleNum(); i++)
	{
		nModuleNo = EPGetModuleID(i);

		m_combo_Stage_List.AddString(::GetModuleName(nModuleNo));
		m_combo_Stage_List.SetItemData(i, nModuleNo);
	}

	m_combo_Stage_List.SetCurSel(m_nCurModuleNo - 1);

	OnBnClickedBtnFanModeSettingRefresh();
}



BOOL FanModeSettingDlg::InitDefaultRows( int nJigFanCount /*= 6*/, int nClipFanCount /*= 6*/ )
{
	try
	{
		BOOL		bLock = m_wndFanModeSettingGrid.LockUpdate();
		const LONG	nGridRowCount = m_wndFanModeSettingGrid.GetRowCount();

		if(nGridRowCount > 0)
			m_wndFanModeSettingGrid.RemoveRows(1, nGridRowCount);

		// Insert Row By Channel Count
		for(int i = 0; i < nJigFanCount+nClipFanCount; i++)
		{
			m_wndFanModeSettingGrid.InsertRows(i+1, 1);
		}

		// Board Name
		for(int i = 0; i < nJigFanCount+nClipFanCount; i++)
		{
			CString csTemp;
			if(i < nJigFanCount)
			{
				csTemp = _T("JIG FAN");
			}
			else if(i - nJigFanCount < nClipFanCount)
			{
				csTemp = _T("CLIP FAN");
			}
			else
			{
				csTemp = _T("UNKNOWN");
			}

			m_wndFanModeSettingGrid.SetValueRange(CGXRange(i+1,FAN_MODE_SETTING_FAN_TYPE), csTemp);
		}

		// Board Channel Name
		for(int i = 0; i < nJigFanCount+nClipFanCount; i++)
		{
			CString csTemp;
			if(i == 0)
				csTemp = _T("1F JIG LEFT");
			else if(i == 1)
				csTemp = _T("1F JIG RIGHT");
			else if(i == 2)
				csTemp = _T("2F JIG LEFT");
			else if(i == 3)
				csTemp = _T("2F JIG RIGHT");
			else if(i == 4)
				csTemp = _T("3F JIG LEFT");
			else if(i == 5)
				csTemp = _T("3F JIG RIGHT");
			else if(i == 6)
				csTemp = _T("1F CLIP LEFT");
			else if(i == 7)
				csTemp = _T("1F CLIP RIGHT");
			else if(i == 8)
				csTemp = _T("2F CLIP LEFT");
			else if(i == 9)
				csTemp = _T("2F CLIP RIGHT");
			else if(i == 10)
				csTemp = _T("3F CLIP LEFT");
			else if(i == 11)
				csTemp = _T("3F CLIP RIGHT");
			else
				csTemp = _T("UNKNOWN");

			m_wndFanModeSettingGrid.SetValueRange(CGXRange(i+1,FAN_MODE_SETTING_FAN_NAME), csTemp);
		}

		m_wndFanModeSettingGrid.LockUpdate(bLock);
		m_wndFanModeSettingGrid.Redraw();
	}	
	catch (CException* e)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL FanModeSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitializeGrid();
	InitializeComboList();

	// TEST
	EP_FAN_INFORMATION_APPLY response;
	ZeroMemory(&response, sizeof(EP_FAN_INFORMATION_APPLY));

	response.nClipFanCount = 6;
	response.nJigFanCount = 6;

	response.pJigFanEnableStatus[0] = 1;

	response.pClipFanEnableStatus[1] = 1;

	//OnFanModeSettingResponse(1, response);
	// TEST
	return TRUE;
}

void FanModeSettingDlg::OnBnClickedBtnFanModeSettingRefresh()
{
	CString strTemp;
	CFormModule *pModuleInfo = m_pDoc->GetModuleInfo(m_nCurModuleNo);
	if(pModuleInfo == FALSE)
	{
		AfxMessageBox(_T("Cannot Find Module Info"));
		return;
	}

	if(pModuleInfo->GetState() == EP_STATE_LINE_OFF)
	{
		InitDefaultRows();
	}
	else
	{
		if(EPSendCommand(m_nCurModuleNo, 1, 0, EP_CMD_FAN_INFORMATION_REQUEST, NULL, 0)!= EP_ACK)
		{
			strTemp.Format("%s :: FAN_INFORMATION_REQUEST Command Send Fail.", ::GetModuleName(m_nCurModuleNo));
			m_pDoc->WriteLog( strTemp );

			AfxMessageBox(strTemp);
		}
		else
		{
			//AfxMessageBox("FAN_INFORMATION_REQUEST Command Send Success");
		}
	}
}

void FanModeSettingDlg::OnBnClickedBtnFanModeSettingApply()
{
	CString csResultLog;

	EP_FAN_INFORMATION_APPLY pSendValue;

	ZeroMemory(&pSendValue, sizeof(EP_FAN_INFORMATION_APPLY));

	if(m_nCurrentJigFanCount == 0 || m_nCurrentClipFanCount == 0)
	{
		pSendValue.nJigFanCount = 6;
		pSendValue.nClipFanCount = 6;
	}
	else
	{
		pSendValue.nJigFanCount = m_nCurrentJigFanCount;
		pSendValue.nClipFanCount = m_nCurrentClipFanCount;
	}

	for(int i = 0; i < pSendValue.nJigFanCount + pSendValue.nClipFanCount; i++)
	{
		CString csEnable = m_wndFanModeSettingGrid.GetValueRowCol(i+1, FAN_MODE_CURRENT_FAN_SETTING);

		if(i < pSendValue.nJigFanCount)
		{
			pSendValue.pJigFanEnableStatus[i] = atoi(csEnable);
		}
		else if(i - pSendValue.nJigFanCount < pSendValue.nClipFanCount)
		{
			pSendValue.pClipFanEnableStatus[i - pSendValue.nJigFanCount] = atoi(csEnable);
		}
	}

	if(EPSendCommand(m_nCurModuleNo, 0, 0, EP_CMD_FAN_INFORMATION_APPLY, &pSendValue, sizeof(EP_FAN_INFORMATION_APPLY)) == EP_ACK)
	{
		AfxMessageBox("Send Success");
	}
}