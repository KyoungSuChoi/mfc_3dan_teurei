#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_AT_TRAY_IN::CFM_ST_AT_TRAY_IN(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_AUTO(_Eqstid, _stid, _unit)
{
}


CFM_ST_AT_TRAY_IN::~CFM_ST_AT_TRAY_IN(void)
{
}

VOID CFM_ST_AT_TRAY_IN::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_TRAY_IN);
	
	m_Unit->fnGetState()->fnSetProcID(FNID_PROC);

	TRACE("CFM_ST_AT_TRAY_IN::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_TRAY_IN::fnProc()
{
	if(1 == m_iWaitCount)
	{
		fnSetFMSStateCode(FMS_ST_RED_TRAY_IN);
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_AT_TRAY_IN::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_TRAY_IN::fnExit()
{
	//공정 완료

	TRACE("CFM_ST_AT_TRAY_IN::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_AT_TRAY_IN::fnSBCPorcess(WORD _state)
{
	CFM_ST_AUTO::fnSBCPorcess(_state);

//	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(AUTO_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			//CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			//CHANGE_STATE(AUTO_ST_TRAY_IN);
		}
		break;
	case EP_STATE_PAUSE:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
		break;
	case EP_STATE_RUN:
		{
			if(m_Unit->fnGetModuleCheckState())
			{
				CHANGE_STATE(AUTO_ST_CONTACT_CHECK);
			}
			else
			{
				CHANGE_STATE(AUTO_ST_RUN);
			}
		}
		break;
	case EP_STATE_READY:
		{
			CHANGE_STATE(AUTO_ST_READY);
		}
		break;
	case EP_STATE_END:
		{
			CHANGE_STATE(AUTO_ST_END);
		}
		break;
	case EP_STATE_ERROR:
		{
			CHANGE_STATE(AUTO_ST_ERROR);
		}
// 	case EP_STATE_CALIBRATION:
// 		{
// 			CHANGE_STATE(CALI_ST_CALI);
// 		}
// 		break;
	}
	TRACE("CFM_ST_AT_TRAY_IN::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_AT_TRAY_IN::fnFMSPorcess(st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 1 )
	{
		st_S2F411 data;
		ZeroMemory(&data, sizeof(st_S2F411));
		memcpy(&data, _recvData->data, sizeof(st_S2F411));

		_error = m_Unit->fnGetFMS()->fnReserveProc();		

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( _error == FMS_ER_NONE )
		{
			//m_Unit->fnGetFMS()->fnSendToHost_S6F11C12( 1, 0 );
		}
	}
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 2 )
	{

		st_S2F412 data;
		ZeroMemory(&data, sizeof(st_S2F412));
		memcpy(&data, _recvData->data, sizeof(st_S2F412));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		//20200423 KSJ - TrayType다를때 NACK주기위해 변경
		_error = FMS_ER_NONE;

		if(m_Unit->fnGetFMS()->fnRun())
		{
			m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
			m_Unit->fnGetFMS()->fnSendToHost_S6F11C1( 1, 0 );
		}
		else
		{
			_error = ER_fnTransFMSWorkInfoToCTSWorkInfo;
			m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
		}
	}
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 6 )
	{
		_error = ER_NO_Process;

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );
	}
	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 4 )
	{
		st_S2F414 data;
		ZeroMemory(&data, sizeof(st_S2F414));
		memcpy(&data, _recvData->data, sizeof(st_S2F414));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		_error = m_Unit->fnGetFMS()->SendSBCInitCommand();

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		m_Unit->fnGetFMS()->fnSendToHost_S6F11C13( _recvData->head.nDevId, _recvData->head.lSysByte );
	}

	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 8 )
	{
		st_S2F418 data;
		ZeroMemory(&data, sizeof(st_S2F418));
		memcpy(&data, _recvData->data, sizeof(st_S2F418));

		//_error = m_Unit->fnGetFMS()->fnReserveProc();	//KSH Calibration 20190830
	
		_error = FMS_ER_NONE;

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( _error == FMS_ER_NONE )
		{
			//m_Unit->fnGetFMS()->fnSendToHost_S6F11C12( 1, 0 );
		}
	}

	else if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 9 )
	{
		BOOL bRunInfoErrChk = FALSE;

		st_S2F419 data;
		ZeroMemory(&data, sizeof(st_S2F419));
		memcpy(&data, _recvData->data, sizeof(st_S2F419));

		if(strncmp(data.TRAYID_1, m_Unit->fnGetFMS()->fnGetTrayID_1(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		if(strncmp(data.TRAYID_2, m_Unit->fnGetFMS()->fnGetTrayID_2(), 7) != 0)
		{
			// bRunInfoErrChk = TRUE;
		}

		_error = FMS_ER_NONE;

		//MessageBox( "AUTO CALIBRATION", MB_ICONINFORMATION );

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( bRunInfoErrChk == FALSE )
		{
			_error = ER_Run_Fail;

			if(m_Unit->fnGetFMS()->fnCalRun())
			{
				_error = FMS_ER_NONE;

				//m_Unit->fnGetFMS()->fnSendToHost_S6F11C1( 1, 0 );
				m_Unit->fnGetFMS()->fnSendToHost_S6F11C17( 1, 0 );
			}
		}
	}

	TRACE("CFM_ST_AT_TRAY_IN::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;
}