#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_CA_TRAY_IN::CFM_ST_CA_TRAY_IN(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_ST_CALI(_Eqstid, _stid, _unit)
{
}


CFM_ST_CA_TRAY_IN::~CFM_ST_CA_TRAY_IN(void)
{
}

VOID CFM_ST_CA_TRAY_IN::fnEnter()
{
	fnSetFMSStateCode(FMS_ST_TRAY_IN);
	
	m_Unit->fnGetState()->fnSetProcID(FNID_PROC);

	TRACE("CFM_ST_CA_TRAY_IN::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_TRAY_IN::fnProc()
{
	if(1 == m_iWaitCount)
	{
		fnSetFMSStateCode(FMS_ST_RED_TRAY_IN);
		m_iWaitCount = -1;
	}

	m_RetryMap |= RETRY_NOMAL_ON;

	TRACE("CFM_ST_CA_TRAY_IN::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_TRAY_IN::fnExit()
{
	//?? ??

	TRACE("CFM_ST_CA_TRAY_IN::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CA_TRAY_IN::fnSBCPorcess(WORD _state)
{
	CFM_ST_CALI::fnSBCPorcess(_state);

//	TRACE("%s \n", g_strSBCState[m_Unit->fnGetModule()->GetState()]);
	
	if(m_Unit->fnGetFMS()->fnGetError())
	{
		CHANGE_STATE(CALI_ST_ERROR);
	}
        
	switch(m_Unit->fnGetModule()->GetState())
	{
	case EP_STATE_IDLE:
		if(m_Unit->fnGetModuleTrayState())
		{
			//CHANGE_STATE(CALI_ST_TRAY_IN);
		}
		else
		{
			CHANGE_STATE(AUTO_ST_VACANCY);
		}
		break;
	case EP_STATE_STANDBY:
		{
			if(m_Unit->fnGetModuleTrayState())
			{
				CHANGE_STATE(CALI_ST_TRAY_IN);
			}
		}
		break;
	case EP_STATE_READY:
		if(m_Unit->fnGetModuleTrayState())
		{
			CHANGE_STATE(CALI_ST_TRAY_IN);
		}
		break;
	case EP_STATE_CALIBRATION:
		{
			CHANGE_STATE(CALI_ST_CALI);
		}
		break;
	}
	TRACE("CFM_ST_CA_TRAY_IN::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_CA_TRAY_IN::fnFMSPorcess(st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 8 )  //19-02-12 ?? MES ?? ??
	{
		st_S2F418 data;
		ZeroMemory(&data, sizeof(st_S2F418));
		memcpy(&data, _recvData->data, sizeof(st_S2F418));

		//_error = m_Unit->fnGetFMS()->fnReserveProc();	KSH Calibration 20190830	
		_error = FMS_ER_NONE;  //KSH Calibration 20190830	

		//20190903 ksj
		CString trayID;
		CString strTemp;
		data.TRAYID_1[6] = NULL;
		data.TRAYID_2[6] = NULL;
		trayID.Format("%s%s",data.TRAYID_1, data.TRAYID_2);
		m_Unit->fnGetFMS()->fnSetTrayID(trayID);
		int nModuleID = m_Unit->fnGetModuleID();

		strTemp.Format("StageLastTray1_%02d", nModuleID);
		AfxGetApp()->WriteProfileString(FMS_REG, strTemp, data.TRAYID_1);
		strTemp.Format("StageLastTray2_%02d",nModuleID);
		AfxGetApp()->WriteProfileString(FMS_REG, strTemp, data.TRAYID_2);

		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( _error == FMS_ER_NONE )
		{
			m_Unit->fnGetFMS()->fnSendToHost_S6F11C16(1, 0);
			int nModuleID = m_Unit->fnGetModuleID();
			EP_GP_STATE_DATA prevState;
			memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
			prevState.state = EP_STATE_READY;
			CFormModule *pModule = NULL;
			pModule = m_Unit->fnGetModule();
			pModule->SetState(prevState);	

		}
	}


	if( _recvData->head.nStrm == 2 && _recvData->head.nFunc == 41 && _recvData->head.nId == 9 )  //19-02-12 ?? MES ?? ??
	{
		st_S2F419 data;
		ZeroMemory(&data, sizeof(st_S2F419));
		memcpy(&data, _recvData->data, sizeof(st_S2F419));
		BOOL bRunInfoErrChk = FALSE;
		CString strTemp;

		_error = FMS_ER_NONE;  //KSH Calibration 20190830	

		//20190903 ksj
		CString trayID;
		data.TRAYID_1[6] = NULL;
		data.TRAYID_2[6] = NULL;
		trayID.Format("%s%s",data.TRAYID_1, data.TRAYID_2);

		int nModuleID = m_Unit->fnGetModuleID();

		if( trayID.GetLength() != 12) //TRAYID? ??? NARK
		{
			_error = ER_Data_Error;	//NARK
		}
		m_Unit->fnGetFMS()->fnSendToHost_S2F42( _recvData->head.nDevId, _recvData->head.lSysByte, _error );

		if( bRunInfoErrChk == FALSE )
		{
			_error = ER_Run_Fail;

			if(m_Unit->fnGetFMS()->fnCalRun())
			{
				_error = FMS_ER_NONE;

				//m_Unit->fnGetFMS()->fnSendToHost_S6F11C1( 1, 0 );
				m_Unit->fnGetFMS()->fnSendToHost_S6F11C17( 1, 0 );
			}
		}
	}

	TRACE("CFM_ST_AT_TRAY_IN::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;
}