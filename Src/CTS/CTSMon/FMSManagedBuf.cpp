#include "stdafx.h"
#include "FMSCriticalSection.h"
#include "FMSStaticSyncParent.h"
#include "FMSSyncParent.h"
#include "FMSMemoryPool.h"
#include "FMSManagedBuf.h"

CFMSManagedBuf::CFMSManagedBuf(void)
{
	ZeroMemory(m_aucBuf, sizeof(m_aucBuf));
}

CFMSManagedBuf::~CFMSManagedBuf(void)
{
}
