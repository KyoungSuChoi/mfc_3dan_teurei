#if !defined(AFX_REGTRAYDLG_H__1F9B4758_4AB8_4628_B533_7BAB32A9BEA1__INCLUDED_)
#define AFX_REGTRAYDLG_H__1F9B4758_4AB8_4628_B533_7BAB32A9BEA1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegTrayDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRegTrayDlg dialog

class CRegTrayDlg : public CDialog
{
// Construction
public:
	virtual ~CRegTrayDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
	void SetMultiInputMode(BOOL bMultiMode = TRUE);
	BOOL m_bModeMultiInput;
	CTray m_TrayData;
	CString m_strTraySerial;
//	int m_nNextTraySerial;
	CRegTrayDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRegTrayDlg)
	enum { IDD = IDD_REG_TRAY_DLG };
	CListBox	m_ctrlTrayListBox;
	CString	m_strTrayNo;
	CString	m_strTryName;
	CString	m_strRegUserName;
	CString	m_strDescription;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegTrayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	int m_cursel ;

	// Generated message map functions
	//{{AFX_MSG(CRegTrayDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeList1();
	//}}AFX_MSG
	afx_msg LRESULT OnBcrscaned(UINT wParam,LONG lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGTRAYDLG_H__1F9B4758_4AB8_4628_B533_7BAB32A9BEA1__INCLUDED_)
