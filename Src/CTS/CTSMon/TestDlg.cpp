// TestDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "TestDlg.h"


// CTestDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTestDlg, CDialog)

CTestDlg::CTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestDlg::IDD, pParent)
{
	m_curId = 0;
}

CTestDlg::~CTestDlg()
{
}

void CTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CB_ID, m_ctlCB_ID);
}


BEGIN_MESSAGE_MAP(CTestDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_STATE_1, &CTestDlg::OnBnClickedBtnState1)
	ON_BN_CLICKED(IDC_BTN_STATE_2, &CTestDlg::OnBnClickedBtnState2)
	ON_BN_CLICKED(IDC_BTN_STATE_3, &CTestDlg::OnBnClickedBtnState3)
	ON_BN_CLICKED(IDC_BTN_STATE_4, &CTestDlg::OnBnClickedBtnState4)
	ON_BN_CLICKED(IDC_BTN_STATE_5, &CTestDlg::OnBnClickedBtnState5)
	ON_BN_CLICKED(IDC_BTN_STATE_6, &CTestDlg::OnBnClickedBtnState6)
	ON_BN_CLICKED(IDC_BTN_STATE_7, &CTestDlg::OnBnClickedBtnState7)
	ON_BN_CLICKED(IDC_BTN_STATE_8, &CTestDlg::OnBnClickedBtnState8)
	ON_BN_CLICKED(IDC_BTN_STATE_9, &CTestDlg::OnBnClickedBtnState9)
	ON_CBN_SELCHANGE(IDC_CB_ID, &CTestDlg::OnCbnSelchangeCbId)
	ON_BN_CLICKED(IDC_BTN_STATE_10, &CTestDlg::OnBnClickedBtnState10)
	ON_BN_CLICKED(IDC_BTN_STATE_11, &CTestDlg::OnBnClickedBtnState11)
	ON_BN_CLICKED(IDC_BTN_STATE_12, &CTestDlg::OnBnClickedBtnState12)
	ON_BN_CLICKED(IDC_BTN_STATE_13, &CTestDlg::OnBnClickedBtnState13)
	ON_BN_CLICKED(IDC_BTN_STATE_14, &CTestDlg::OnBnClickedBtnState14)
	ON_BN_CLICKED(IDC_BTN_STATE_15, &CTestDlg::OnBnClickedBtnState15)
END_MESSAGE_MAP()


// CTestDlg 메시지 처리기입니다.

void CTestDlg::OnBnClickedBtnState1()
{
	EPTestSetState(m_curId, EP_STATE_LINE_OFF);
	EPSetGroupState(m_curId, 1, EP_STATE_LINE_OFF);
}
void CTestDlg::OnBnClickedBtnState2()
{
	EPTestSetState(m_curId, EP_STATE_LINE_ON);
	EPSetGroupState(m_curId, 1, EP_STATE_LINE_ON);
}

void CTestDlg::OnBnClickedBtnState3()
{
	EPTestSetState(m_curId, EP_STATE_EMERGENCY);
	EPSetGroupState(m_curId, 1, EP_STATE_EMERGENCY);
}

void CTestDlg::OnBnClickedBtnState4()
{
	EPTestSetState(m_curId, EP_STATE_ERROR);
	EPSetGroupState(m_curId, 1, EP_STATE_ERROR);
}

void CTestDlg::OnBnClickedBtnState5()
{
	EPTestSetState(m_curId, EP_STATE_IDLE);
	EPSetGroupState(m_curId, 1, EP_STATE_IDLE);
}

void CTestDlg::OnBnClickedBtnState6()
{
	EPTestSetState(m_curId, EP_STATE_STANDBY);
	EPSetGroupState(m_curId, 1, EP_STATE_STANDBY);
}

void CTestDlg::OnBnClickedBtnState7()
{
	EPTestSetState(m_curId, EP_STATE_RUN);
	EPSetGroupState(m_curId, 1, EP_STATE_RUN);
}

void CTestDlg::OnBnClickedBtnState8()
{
	EPTestSetState(m_curId, EP_STATE_PAUSE);
	EPSetGroupState(m_curId, 1, EP_STATE_PAUSE);
}

void CTestDlg::OnBnClickedBtnState9()
{
	EPTestSetState(m_curId, EP_STATE_MAINTENANCE);
	EPSetGroupState(m_curId, 1, EP_STATE_MAINTENANCE);
}

void CTestDlg::OnBnClickedBtnState10()
{
	EPTestSetLineMode(m_curId, 1, EP_OPERATION_MODE, EP_OPERATION_AUTO);
}

void CTestDlg::OnBnClickedBtnState11()
{
	EPTestSetLineMode(m_curId, 1, EP_OPERATION_MODE, EP_OPERATION_LOCAL);
}

void CTestDlg::OnBnClickedBtnState12()
{
	EPTestSetLineMode(m_curId, 1, EP_OPERATION_MODE, EP_OPEARTION_MAINTENANCE);
}

BOOL CTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	for(INT i = 0; i < 100; i++)
	{
		CString id;
		id.Format("%d", i + 1);
		m_ctlCB_ID.AddString(id);
	}

	m_ctlCB_ID.SetCurSel(0);

	m_curId = 1;
	

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTestDlg::OnCbnSelchangeCbId()
{
	INT idx = m_ctlCB_ID.GetCurSel();
	m_curId = idx + 1;
}



void CTestDlg::OnBnClickedBtnState13()
{
	EPTestSetState(m_curId, EP_STATE_STANDBY);
	EPSetGroupState(m_curId, 1, EP_STATE_END);
}

void CTestDlg::OnBnClickedBtnState14()
{
	EPTestSetState(m_curId, EP_STATE_END);
	EPSetGroupState(m_curId, 1, EP_STATE_END);
}

void CTestDlg::OnBnClickedBtnState15()
{
	EPTestSetState(m_curId, EP_STATE_ERROR);
	EPSetGroupState(m_curId, 1, EP_STATE_ERROR);
}
