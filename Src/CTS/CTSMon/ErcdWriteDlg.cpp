// ErcdWriteDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ErcdWriteDlg.h"


// CErcdWriteDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CErcdWriteDlg, CDialog)

CErcdWriteDlg::CErcdWriteDlg(CWnd* pParent /*=NULL*/, CCTSMonDoc* pDoc)
	: CDialog(CErcdWriteDlg::IDD, pParent)
{

	m_nCurModuleID = 0;
	m_pDoc = pDoc;
	m_nMaxStageCnt = m_pDoc->GetInstalledModuleNum();

	ZeroMemory(&m_SendERCDWriteValue, sizeof(EP_ERCD_WRITE_VALUE));
}

CErcdWriteDlg::~CErcdWriteDlg()
{

}

void CErcdWriteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_STAGE_CHANGE, m_comboStageID);
	DDX_Control(pDX, IDC_STAGE_NAME, m_CmdTarget);
	DDX_Control(pDX, ID_DLG_LABEL_NAME, m_LabelViewName);
	DDX_Control(pDX, IDC_SBC_RESPONSE_LOG, m_cEditBoxSBCResponseLog);
}

BEGIN_MESSAGE_MAP(CErcdWriteDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_STAGE_CHANGE, &CErcdWriteDlg::OnCbnSelchangeComboStageChange)
	ON_BN_CLICKED(IDC_BTN_APPLY, &CErcdWriteDlg::OnBnClickedBtnApply)
	ON_BN_CLICKED(IDC_BTN_APPLY_ALL, &CErcdWriteDlg::OnBnClickedBtnApplyAll)
	ON_BN_CLICKED(IDC_BTN_APPLY_ALL2, &CErcdWriteDlg::OnBnClickedBtnApplyAll2)
END_MESSAGE_MAP()
// CErcdWriteDlg 메시지 처리기입니다.

BOOL CErcdWriteDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	initCtrl();

	return TRUE;
}

void CErcdWriteDlg::initCtrl()
{
	initGrid();
	initLabel();
	initFont();
	initCombo();
}

void CErcdWriteDlg::initCombo()
{
	int nModuleID = 1;	

	for(int i=0; i<m_nMaxStageCnt; i++ )
	{
		nModuleID = EPGetModuleID(i);

		m_comboStageID.AddString(::GetModuleName(nModuleID));
		m_comboStageID.SetItemData(i, nModuleID);
	}
}
void CErcdWriteDlg::initFont()
{
	LOGFONT LogFont;

	m_comboStageID.GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = 35;

	m_Font.CreateFontIndirect( &LogFont );

	m_comboStageID.SetFont(&m_Font);
}


void CErcdWriteDlg::initGrid()
{
	m_grid.SubclassDlgItem(IDC_GRID, this);
	m_grid.m_bSameRowSize = FALSE;
	m_grid.m_bSameColSize = FALSE;
	
	m_grid.m_bCustomWidth = TRUE;
	m_grid.m_bCustomColor = FALSE;

	m_grid.Initialize();

	m_grid.SetColCount(3);
	m_grid.SetRowCount(8);
	m_grid.SetDefaultRowHeight(46);

	m_grid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_grid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);	//Only Vertial Scroll Bar

	m_grid.EnableGridToolTips();
	m_grid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));


	m_grid.GetParam()->SetSortRowsOnDblClk(TRUE);

	m_grid.SetRowHeight(0,0,0);
	m_grid.m_nWidth[1] = 200;
	m_grid.m_nWidth[2] = 200;
	m_grid.m_nWidth[3] = 200;		

	// Enable Edit Cells START
	m_grid.SetStyleRange(CGXRange().SetCells(2,2,3,3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetMaxLength(20)
		);
	m_grid.SetStyleRange(CGXRange().SetCells(4,2,4,2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetMaxLength(20)
		);
	// Enable Edit Cells END

	//Row Header Setting START
	m_grid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(14).SetFaceName("굴림").SetBold(TRUE)).SetWrapText(FALSE).SetAutoSize(TRUE));

	m_grid.SetStyleRange(CGXRange().SetCols(1),	CGXStyle().SetInterior(RGB(255, 245, 245)).SetHorizontalAlignment(DT_LEFT));
	m_grid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetInterior(RGB(225, 235, 225)));
	m_grid.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetInterior(RGB(205, 205, 215)));

	m_grid.SetStyleRange(CGXRange(1,1),	CGXStyle().SetInterior(RGB(240, 230, 230)));
	m_grid.SetStyleRange(CGXRange(1,2), CGXStyle().SetInterior(RGB(210, 220, 210)));
	m_grid.SetStyleRange(CGXRange(1,3), CGXStyle().SetInterior(RGB(190, 190, 200)));
	//Row Header Setting END

	// Default Value Setting START
	m_grid.SetValueRange(CGXRange(1,1), "Code");
	m_grid.SetValueRange(CGXRange(1,2), "SettingValue");
	m_grid.SetValueRange(CGXRange(1,3), "Time");

	m_grid.SetValueRange(CGXRange(2,1), "CC0D (mV, Sec)");
	m_grid.SetValueRange(CGXRange(3,1), "CC0E (μV, Sec)");
	m_grid.SetValueRange(CGXRange(4,1), "CC0F (mA)");

	m_grid.SetValueRange(CGXRange(5,1), "Code");
	m_grid.SetValueRange(CGXRange(5,2), "DefaultValue");
	m_grid.SetValueRange(CGXRange(5,3), "DefaultTime");

	m_grid.SetValueRange(CGXRange(6,1), "CC0D (mV, Sec)");
	m_grid.SetValueRange(CGXRange(7,1), "CC0E (μV, Sec)");
	m_grid.SetValueRange(CGXRange(8,1), "CC0F (mA)");

	// Set Default Value
	m_grid.SetValueRange(CGXRange(6,2), "50");
	m_grid.SetValueRange(CGXRange(6,3), "60");

	m_grid.SetValueRange(CGXRange(7,2), "580");
	m_grid.SetValueRange(CGXRange(7,3), "600");

	m_grid.SetValueRange(CGXRange(8,2), "500");
	// Default Value Setting END

	m_grid.Redraw();
}

void CErcdWriteDlg::initLabel()
{
	m_CmdTarget.SetFontSize(20)
		.SetTextColor(RGB_LABEL_FONT_STAGENAME)
		.SetBkColor(RGB_LABEL_BACKGROUND_STAGENAME)
		.SetFontBold(TRUE);

	m_LabelViewName.SetBkColor(RGB_MIDNIGHTBLUE);
	m_LabelViewName.SetTextColor(RGB_WHITE);
	m_LabelViewName.SetFontSize(24);
	m_LabelViewName.SetFontBold(TRUE);
	m_LabelViewName.SetText("WRITE ERCD CODE VALUE"); //"Monitoring"
}

void CErcdWriteDlg::SetCurrentModule(int nModuleID)
{
	m_nCurModuleID = nModuleID;
	m_CmdTarget.SetText(GetTargetModuleName());
	m_comboStageID.SetCurSel(nModuleID-1);
	EPJustSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_PARAM_REQUEST);
	m_grid.SetValueRange(CGXRange(2,2,4,3),"");
}

CString CErcdWriteDlg::GetTargetModuleName()
{
	return "Stage No. " + GetModuleName(m_nCurModuleID);
}

void CErcdWriteDlg::OnCbnSelchangeComboStageChange()
{
	int nIndex = m_comboStageID.GetCurSel();
	if(nIndex >= 0)
	{
		m_nCurModuleID = nIndex+1;
		m_CmdTarget.SetText(GetTargetModuleName());
		EPJustSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_PARAM_REQUEST);
	}
	m_grid.SetValueRange(CGXRange(2,2,4,3),"");
}
void CErcdWriteDlg::OnBnClickedBtnApply()
{
	CString csResultLog;

	if(GetERCDValueFromUI(m_SendERCDWriteValue))
	{
		if(EPSendCommand(m_nCurModuleID, 0, 0, EP_CMD_WRITE_ERCD_VALUE_REQUEST, &m_SendERCDWriteValue, sizeof(EP_ERCD_WRITE_VALUE)) == EP_ACK)
		{
			csResultLog.Format("[Stage %s] Send Success.\r\n", ::GetModuleName(m_nCurModuleID));
		}
		else
		{
			csResultLog.Format("[Stage %s] Send Fail.\r\n", ::GetModuleName(m_nCurModuleID));
		}

		this->AddERCDWriteValueEditBox(csResultLog);
	}
	else
	{
		this->AddERCDWriteValueEditBox("Invalid Value Input\r\n");
	}
}

void CErcdWriteDlg::OnBnClickedBtnApplyAll()
{
	CString csResultLog;
	
	if(GetERCDValueFromUI(m_SendERCDWriteValue))
	{
		for(int i = 0; i < m_nMaxStageCnt; i++)
		{
			int iModuleID = EPGetModuleID(i);

			if(EPSendCommand(iModuleID, 0, 0, EP_CMD_WRITE_ERCD_VALUE_REQUEST, &m_SendERCDWriteValue, sizeof(EP_ERCD_WRITE_VALUE)) == EP_ACK)
			{
				csResultLog.Format("[Stage %s] Send Success.\r\n", ::GetModuleName(iModuleID));
			}
			else
			{
				csResultLog.Format("[Stage %s] Send Fail\r\n", ::GetModuleName(iModuleID));
			}

			this->AddERCDWriteValueEditBox(csResultLog);
		}
	}
	else
	{
		this->AddERCDWriteValueEditBox("Invalid Value Input\r\n");
	}
}

BOOL CErcdWriteDlg::GetERCDValueFromUI(EP_ERCD_WRITE_VALUE& sendERCDWriteValue)
{
	try
	{
		CString csCC0DSettingValue	= m_grid.GetValueRowCol(2, 2);	// CC0D Setting Value
		CString csCC0DFrequencyTime = m_grid.GetValueRowCol(2, 3);	// CC0D Frequency Time
		CString csCC0ESettingValue	= m_grid.GetValueRowCol(3, 2);	// CC0E Setting Value
		CString csCC0EFrequencyTime	= m_grid.GetValueRowCol(3, 3);	// CC0E Frequency Time
		CString csCC0FSettingValue	= m_grid.GetValueRowCol(4, 2 );	// CC0F Setting Value

		if(!IsInteger(csCC0DSettingValue)) csCC0DSettingValue		= "-1";
		if(!IsInteger(csCC0DFrequencyTime)) csCC0DFrequencyTime		= "-1";
		if(!IsInteger(csCC0ESettingValue))  csCC0ESettingValue		= "-1";
		if(!IsInteger(csCC0EFrequencyTime)) csCC0EFrequencyTime		= "-1";
		if(!IsInteger(csCC0FSettingValue)) csCC0FSettingValue		= "-1";

		sendERCDWriteValue.lCC0DSettingValue = atoi(csCC0DSettingValue);
		sendERCDWriteValue.lCC0DFrequencyTime = atoi(csCC0DFrequencyTime);
		sendERCDWriteValue.lCC0ESettingValue = atoi(csCC0ESettingValue);
		sendERCDWriteValue.lCC0EFrequencyTime = atoi(csCC0EFrequencyTime);
		sendERCDWriteValue.lCC0FSettingValue = atoi(csCC0FSettingValue);

		return TRUE;
	}	
	catch (CException* e)
	{
		char pErrorMessage[256];

		e->GetErrorMessage(pErrorMessage, 256);

		AfxMessageBox(pErrorMessage);
	}

	return FALSE;
}

void CErcdWriteDlg::OnSBCResponse(int iModuleID, EP_ERCD_WRITE_VALUE ercdValue)
{
	CString csResultLog;
	CString csTemp;

	csResultLog ="";
	if(m_SendERCDWriteValue.lCC0DSettingValue>0 )
	{
		if( m_SendERCDWriteValue.lCC0DSettingValue == ercdValue.lCC0DSettingValue)
		{
			csTemp.Format("[Stage %s] Success CC0D Setting Value : %d\r\n", ::GetModuleName(iModuleID), ercdValue.lCC0DSettingValue);
			csResultLog += csTemp;
		}
		else
		{
			csTemp.Format("[Stage %s] Value Mismatch. Need to Check\r\n CC0D Setting Value : %d, Response Value %d\r\n"
				,GetModuleName(iModuleID), m_SendERCDWriteValue.lCC0DSettingValue, ercdValue.lCC0DSettingValue);
			csResultLog += csTemp;
		}
		this->AddERCDWriteValueEditBox(csResultLog);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	csResultLog ="";
	if(m_SendERCDWriteValue.lCC0DFrequencyTime>0)
	{
		if(m_SendERCDWriteValue.lCC0DFrequencyTime == ercdValue.lCC0DFrequencyTime)
		{
			csTemp.Format("[Stage %s] Success  CC0D Frequency Time : %d\r\n", ::GetModuleName(iModuleID), ercdValue.lCC0DFrequencyTime);
			csResultLog += csTemp;
		}
		else
		{
			csTemp.Format("[Stage %s] Value Mismatch. Need to Check\r\n CC0D Frequency Time : %d, Response Value %d\r\n"
				,GetModuleName(iModuleID), m_SendERCDWriteValue.lCC0DFrequencyTime, ercdValue.lCC0DFrequencyTime);
			csResultLog += csTemp;
		}
		this->AddERCDWriteValueEditBox(csResultLog);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	csResultLog ="";
	if(m_SendERCDWriteValue.lCC0ESettingValue>0)
	{
		if(m_SendERCDWriteValue.lCC0ESettingValue == ercdValue.lCC0ESettingValue)
		{
			csTemp.Format("[Stage %s] Success CC0E Setting Valu : %d\r\n", ::GetModuleName(iModuleID), ercdValue.lCC0ESettingValue);
			csResultLog += csTemp;
		}
		else
		{
			csTemp.Format("[Stage %s] Value Mismatch. Need to Check\r\n CC0E Setting Value : %d, Response Value %d\r\n"
				,GetModuleName(iModuleID), m_SendERCDWriteValue.lCC0ESettingValue, ercdValue.lCC0ESettingValue);
			csResultLog += csTemp;
		}
		this->AddERCDWriteValueEditBox(csResultLog);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	csResultLog ="";
	if(m_SendERCDWriteValue.lCC0EFrequencyTime>0)
	{
		if(m_SendERCDWriteValue.lCC0EFrequencyTime == ercdValue.lCC0EFrequencyTime)
		{
			csTemp.Format("[Stage %s] Success CC0E Frequency Value : %d\r\n", ::GetModuleName(iModuleID), ercdValue.lCC0EFrequencyTime);
			csResultLog += csTemp;
		}
		else
		{
			csTemp.Format("[Stage %s] Value Mismatch. Need to Check\r\n CC0E Frequency Value : %d, Response Value %d\r\n"
				,GetModuleName(iModuleID), m_SendERCDWriteValue.lCC0EFrequencyTime, ercdValue.lCC0EFrequencyTime);
			csResultLog += csTemp;
		}
		this->AddERCDWriteValueEditBox(csResultLog);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	csResultLog ="";
	if(m_SendERCDWriteValue.lCC0FSettingValue>0 )
	{
		if(m_SendERCDWriteValue.lCC0FSettingValue == ercdValue.lCC0FSettingValue)
		{
			csTemp.Format("[Stage %s] Success CC0F Setting Value : %d\r\n", ::GetModuleName(iModuleID), ercdValue.lCC0FSettingValue);
			csResultLog += csTemp;
		}
		else
		{
			csTemp.Format("[Stage %s] Value Mismatch. Need to Check\r\n CC0F Setting Value : %d, Response Value %d\r\n"
				,GetModuleName(iModuleID), m_SendERCDWriteValue.lCC0FSettingValue, ercdValue.lCC0FSettingValue);
			csResultLog += csTemp;
		}
		this->AddERCDWriteValueEditBox(csResultLog);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}

BOOL CErcdWriteDlg::AddERCDWriteValueEditBox(CString csLog)
{
	try
	{
		CString csOriginText;
		
		m_cEditBoxSBCResponseLog.GetWindowText(csOriginText);
		m_cEditBoxSBCResponseLog.SetWindowText(csOriginText + csLog);

		int nLastLineFirstIndex = m_cEditBoxSBCResponseLog.LineIndex(m_cEditBoxSBCResponseLog.GetLineCount()-1);
		m_cEditBoxSBCResponseLog.SetSel(nLastLineFirstIndex, nLastLineFirstIndex);

		return TRUE;
	}	
	catch (CException* e)
	{
		char pErrorMessage[256];

		e->GetErrorMessage(pErrorMessage, 256);

		AfxMessageBox(pErrorMessage);
	}

	return FALSE;
}

BOOL CErcdWriteDlg::IsInteger(CString csValue)
{
	if(csValue.IsEmpty())
		return FALSE;

	for(int i = 0; i < csValue.GetLength(); i++)
	{
		if(!(csValue.GetAt(i) >= '0' && csValue.GetAt(i) <= '9'))
			return FALSE;
	}

	return TRUE;
}
void CErcdWriteDlg::OnBnClickedBtnApplyAll2()
{
	if((EPSendCommand( m_nCurModuleID, 0, 0, EP_CMD_SBC_PARAM_REQUEST)) != EP_ACK)
	{
		AfxMessageBox("명령 전송 실패!");//
	}
}


//2021-03-25 KSJ 현재 값 표시 추가
void CErcdWriteDlg::RecvParameter(int nModuleID)
{

	if(m_nCurModuleID == nModuleID)
	{
		WORD state = EPGetGroupState(m_nCurModuleID);

		EP_MD_SBC_PARAM *lpSbcParam;
		lpSbcParam = EPGetModuleSbcParam(EPGetModuleIndex(m_nCurModuleID));

		if(lpSbcParam == NULL)
		{
			return;
		}
		if( state == EP_STATE_LINE_OFF )
		{
			return;
		}

		CString strTemp;
		strTemp.Format("%d", lpSbcParam->lFaultSettingValue[19]/1000);
		m_grid.SetValueRange(CGXRange(2,2), strTemp);
		strTemp.Format("%d", lpSbcParam->lFaultSettingValue[20]/1000);
		m_grid.SetValueRange(CGXRange(2,3), strTemp);

		strTemp.Format("%d", lpSbcParam->lFaultSettingValue[22]);
		m_grid.SetValueRange(CGXRange(3,2), strTemp);
		strTemp.Format("%d", lpSbcParam->lFaultSettingValue[24]/1000);
		m_grid.SetValueRange(CGXRange(3,3), strTemp);

		strTemp.Format("%d", lpSbcParam->lFaultSettingValue[28]/1000);
		m_grid.SetValueRange(CGXRange(4,2), strTemp);
	}

}