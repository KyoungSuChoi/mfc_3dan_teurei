// DetailTempCondition.cpp : implementation file
//

#include "stdafx.h"
#include "ctsmon.h"
#include "DetailTempCondition.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDetailTempCondition dialog


CDetailTempCondition::CDetailTempCondition(CCTSMonDoc* pDoc, UINT nModuleID, CWnd* pParent /*=NULL*/)
	: CDialog(CDetailTempCondition::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDetailTempCondition)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	m_ModuleID = nModuleID;
}


void CDetailTempCondition::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDetailTempCondition)
	DDX_Control(pDX, IDC_LABEL_UNITID, m_Label_UnitID);
	DDX_Control(pDX, IDC_LABEL_UNITTEMP, m_Label_UnitTemp);
	DDX_Control(pDX, IDC_LABEL_JIGTEMP, m_Label_JigTemp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDetailTempCondition, CDialog)
	//{{AFX_MSG_MAP(CDetailTempCondition)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDetailTempCondition message handlers

BOOL CDetailTempCondition::OnInitDialog() 
{
	CDialog::OnInitDialog();	
	// TODO: Add extra initialization here
	CString strTemp;
	InitLabel();
	InitTempGrid();	
	
	strTemp.Format( "%s", GetModuleName(m_ModuleID) );
	m_Label_UnitID.SetText(strTemp);

	SetTimer(TIMER_TEMP_ID, TIMER_TEMP_TIME, NULL);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDetailTempCondition::InitLabel()
{	
	m_Label_JigTemp.SetBkColor(0);
	m_Label_JigTemp.SetTextColor(RGB(255, 255, 0));
	m_Label_JigTemp.SetFontSize(20);
	m_Label_JigTemp.SetFontBold(TRUE);
	m_Label_JigTemp.SetText("NO TEMPDATA");

	m_Label_UnitTemp.SetBkColor(0);
	m_Label_UnitTemp.SetTextColor(RGB(255, 255, 0));
	m_Label_UnitTemp.SetFontSize(20);
	m_Label_UnitTemp.SetFontBold(TRUE);
	m_Label_UnitTemp.SetText("NO TEMPDATA");
	
	m_Label_UnitID.SetBkColor(0);
	m_Label_UnitID.SetTextColor(RGB(255, 255, 0));
	m_Label_UnitID.SetFontSize(20);
	m_Label_UnitID.SetFontBold(TRUE);
	m_Label_UnitID.SetText("NO TEMPDATA");
}

void CDetailTempCondition::InitTempGrid()
{
	m_wndJigTempGrid.SubclassDlgItem(IDC_GRID_JIGTEMP, this);
	m_wndJigTempGrid.Initialize();
	
	m_wndUnitTempGrid.SubclassDlgItem(IDC_GRID_UNITTEMP, this);
	m_wndUnitTempGrid.Initialize();	

	int nChIndex = 0;
	int nJigIndex = 0;
	int nUnitIndex = 0;

	CFormModule *pModule = m_pDoc->GetModuleInfo(m_ModuleID);
	_MAPPING_DATA *pMapData;	

	m_wndJigTempGrid.m_bSameRowSize = TRUE;
	m_wndJigTempGrid.m_bSameColSize = TRUE;
	m_wndJigTempGrid.m_bCustomWidth = TRUE;

	m_wndJigTempGrid.SetRowCount(TEMP_CONDITION_GRID_ROW);
	m_wndJigTempGrid.SetColCount(TEMP_CONDITION_GRID_COL);
	m_wndUnitTempGrid.SetRowCount(TEMP_CONDITION_GRID_ROW);
	m_wndUnitTempGrid.SetColCount(TEMP_CONDITION_GRID_COL);

	m_wndJigTempGrid.SetStyleRange(CGXRange().SetRows(1),
			CGXStyle().SetControl(GX_IDS_CTRL_HEADER).SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192))
			  );
	m_wndJigTempGrid.SetStyleRange(CGXRange().SetRows(3),
		CGXStyle().SetControl(GX_IDS_CTRL_HEADER).SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192))
			  );
	m_wndUnitTempGrid.SetStyleRange(CGXRange().SetRows(1),
		CGXStyle().SetControl(GX_IDS_CTRL_HEADER).SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192))
		);
	m_wndUnitTempGrid.SetStyleRange(CGXRange().SetRows(3),
		CGXStyle().SetControl(GX_IDS_CTRL_HEADER).SetEnabled(FALSE).SetTextColor(RGB(255,255,255)).SetInterior(RGB(192,192,192))
			  );

	m_wndJigTempGrid.SetRowHeight(0,0,0);
	m_wndUnitTempGrid.SetRowHeight(0,0,0);

	for( nChIndex=0; nChIndex < 16; nChIndex++ )
	{
		pMapData = pModule->GetSensorMap(0, nChIndex);
		
		if(pMapData->nChannelNo == 1)
		{
			if( nJigIndex < 8 )
			{
				// 1. Jig부로 셋팅된 온도값
				if( nJigIndex > 3 )
				{
					pMapData = pModule->GetSensorMap(0, nChIndex);
					m_wndJigTempGrid.SetStyleRange(CGXRange(_ROW_JIGGRID_NAME2, nJigIndex-3), CGXStyle().SetValue(pMapData->szName).SetFont(CGXFont().SetBold(TRUE)));
				}
				else
				{
					pMapData = pModule->GetSensorMap(0, nChIndex);
					m_wndJigTempGrid.SetStyleRange(CGXRange(_ROW_JIGGRID_NAME1, nJigIndex+1), CGXStyle().SetValue(pMapData->szName).SetFont(CGXFont().SetBold(TRUE)));
				}
				nJigIndex++;
			}
			
		}
		else if( pMapData->nChannelNo == 0 )
		{				
			if( nUnitIndex < 8 )
			{
				if( nUnitIndex > 3 )
				{
					pMapData = pModule->GetSensorMap(0, nChIndex);
					m_wndUnitTempGrid.SetStyleRange(CGXRange(_ROW_UNITGRID_NAME2,nUnitIndex-3), CGXStyle().SetValue(pMapData->szName).SetFont(CGXFont().SetBold(TRUE)));
				}
				else
				{
					pMapData = pModule->GetSensorMap(0, nChIndex);
					m_wndUnitTempGrid.SetStyleRange(CGXRange(_ROW_UNITGRID_NAME1,nUnitIndex+1), CGXStyle().SetValue(pMapData->szName).SetFont(CGXFont().SetBold(TRUE)));
				}
				nUnitIndex++;
			}
		}
	}
}

void CDetailTempCondition::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if( nIDEvent == TIMER_TEMP_ID )
	{
		DisplayTempData();		
	}	
	CDialog::OnTimer(nIDEvent);
}

void CDetailTempCondition::DisplayTempData()
{
	CFormModule		*pModule;
	_MAPPING_DATA	*pMapData;
	
	pModule = m_pDoc->GetModuleInfo(m_ModuleID);
	EP_GP_DATA gpData = EPGetGroupData(m_ModuleID, 0);

	CString strTemp = _T("");
	CString strValue = _T("");

	long lTempUnitAvg = 0;
	long lTempJigAvg = 0;

	int nChIndex = 0;
	int nJigIndex = 0;
	int nUnitIndex = 0;

	// 1. Jig부와 Unit부의 온도를 표시한다.
	// 2. 각각 최대 8개의 값을 포시할 수 있다.
	if( gpData.gpState.state != EP_STATE_LINE_OFF )
	{
		for( nChIndex=0; nChIndex < 16; nChIndex++ )
		{
			pMapData = pModule->GetSensorMap(0, nChIndex);
			
			if(pMapData->nChannelNo == 1)
			{
				if( nJigIndex < 8 )
				{
					// 1. Jig부로 셋팅된 온도값
					if( nJigIndex > 3 )
					{
						lTempJigAvg += gpData.sensorData.sensorData1[nChIndex].lData;
						strTemp.Format("%.1f℃", (float)gpData.sensorData.sensorData1[nChIndex].lData/100.0f );
						m_wndJigTempGrid.SetStyleRange(CGXRange(_ROW_JIGGRID_VAR2,nJigIndex-3), CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE)));
					}
					else
					{
						lTempJigAvg += gpData.sensorData.sensorData1[nChIndex].lData;
						strTemp.Format("%.1f℃", (float)gpData.sensorData.sensorData1[nChIndex].lData/100.0f );
						m_wndJigTempGrid.SetStyleRange(CGXRange(_ROW_JIGGRID_VAR1,nJigIndex+1), CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE)));
					}
					nJigIndex++;
				}
				
			}
			else if( pMapData->nChannelNo == 0 )
			{				
				if( nUnitIndex < 8 )
				{
					if( nUnitIndex > 3 )
					{
						lTempUnitAvg += gpData.sensorData.sensorData1[nChIndex].lData;		
						strTemp.Format("%.1f℃", (float)gpData.sensorData.sensorData1[nChIndex].lData/100.0f );
						m_wndUnitTempGrid.SetStyleRange(CGXRange(_ROW_UNITGRID_VAR2,nUnitIndex-3), CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE)));
					}
					else
					{
						lTempUnitAvg += gpData.sensorData.sensorData1[nChIndex].lData;		
						strTemp.Format("%.1f℃", (float)gpData.sensorData.sensorData1[nChIndex].lData/100.0f );
						m_wndUnitTempGrid.SetStyleRange(CGXRange(_ROW_UNITGRID_VAR1,nUnitIndex+1), CGXStyle().SetValue(strTemp).SetFont(CGXFont().SetBold(TRUE)));
					}
					nUnitIndex++;
				}
			}
		}
		
		lTempJigAvg = lTempJigAvg/nJigIndex;
		strTemp.Format("%.1f℃", (float)lTempJigAvg/100.0f);
		m_Label_JigTemp.SetText(strTemp);
				
		lTempUnitAvg = lTempUnitAvg/nUnitIndex;
		strTemp.Format("%.1f℃", (float)lTempUnitAvg/100.0f);
		m_Label_UnitTemp.SetText(strTemp);
	}
}
