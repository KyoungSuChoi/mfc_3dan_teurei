// InfomationDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMon.h"
#include "InformationDlg.h"


// CInfomationDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInformationDlg, CDialog)

CInformationDlg::CInformationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInformationDlg::IDD, pParent)
	, m_strResultMsg(_T(""))
{
	m_nModuleID = 0;
}

CInformationDlg::~CInformationDlg()
{
}

void CInformationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_INFO, m_strResultMsg);
	DDX_Control(pDX, IDC_STATIC1, m_LabelStageName);
	DDX_Control(pDX, IDOK, m_BtnOK);
}


BEGIN_MESSAGE_MAP(CInformationDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CInformationDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CInfomationDlg 메시지 처리기입니다.

void CInformationDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

BOOL CInformationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitFont();
	
	m_BtnOK.SetFontStyle(30, 1);
	m_BtnOK.SetColor(RGB_BTN_FONT, RGB_BTN_BACKGROUND, RGB_BTN_BACKGROUND);	
	
	m_LabelStageName.SetBkColor(RGB_DARKSLATEBLUE)
		.SetTextColor(RGB_WHITE)
		.SetFontSize(28)
		.SetFontBold(TRUE)		
		.SetText("-");

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CInformationDlg::InitFont()
{
	LOGFONT	LogFont;

	GetDlgItem(IDC_STATIC_INFO)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = 1000;
	LogFont.lfHeight = EDIT_FONT_SIZE;

	font.CreateFontIndirect( &LogFont );

	GetDlgItem(IDC_STATIC_INFO)->SetFont(&font);
}

void CInformationDlg::SetResultMsg( CString strMsg, int nMsgType )
{
	CenterWindow();
	
	CString strTemp = _T("");
	if( m_nModuleID != 0 )
	{
		strTemp.Format(">> Stage No. %s <<", GetModuleName(m_nModuleID) );
		
		switch( nMsgType )
		{
			case INFO_TYPE_NORMAL:
			{
				m_LabelStageName.SetBkColor(RGB_DARKSLATEBLUE);	
				m_LabelStageName.FlashBackground(FALSE);		
			}
			break;
			case INFO_TYPE_WARNNING:
			{
				m_LabelStageName.SetBkColor(RGB_BLUE);
				m_LabelStageName.FlashBackground(FALSE);						
			}
			break;
			case INFO_TYPE_FAULT:			
			{
				m_LabelStageName.SetBkColor(RGB_RED);
				m_LabelStageName.FlashBackground(TRUE);		
			}
			break;
			
		}	
		
		m_LabelStageName.SetText(strTemp);
		
		m_strResultMsg.Format("\n%s", strMsg );	
	}
	else
	{
		m_strResultMsg.Format("\n%s", strMsg );
	}
		
	UpdateData(FALSE);
}

int CInformationDlg::GetUnitNum()
{
	return m_nModuleID;
}

VOID CInformationDlg::SetUnitNum( int nUnitNum )
{
	m_nModuleID = nUnitNum;
}