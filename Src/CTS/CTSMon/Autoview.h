#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "CTSMonDoc.h"
#include "ColorButton2.h"
// CAutoview 폼 뷰입니다.
//2020 12 06 BJY AUTO VIEW Tab 추가
class CAutoview : public CFormView
{
	DECLARE_DYNCREATE(CAutoview)

protected:
	CAutoview();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CAutoview();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
public:
	enum { IDD = IDD_AUTO_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CCTSMonDoc* GetDocument();
	void InitLabel();
	void InitFont();
	void InitCombo();
	void InitColorBtn();
	void ModuleConnected(int nModuleID);
	void ModuleDisConnected(int nModuleID);
	void StartMonitoring();
	void DrawProcessInfo();
	void StopMonitoring();
	void InitStepList();
	void DrawStepList();
	void SetCurrentModule(int nModuleID, int nGroupIndex=0);
	CString GetTargetModuleName();
	void DrawChannel();	
	void UpdateGroupState(int nModuleID, int nGroupIndex=0 );

public:
	CFont	m_Font;
	int		m_nPrevStep;
	int		m_nCurModuleID;
	STR_TOP_CONFIG		*m_pConfig;
	CImageList *m_pChStsSmallImage;
	UINT	m_nDisplayTimer;

	CString m_strPreTrayId;
	CString m_strPreUpTrayId;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CLabel m_CmdTarget;
	CLabel m_LabelViewName;
	CListCtrl m_ctrlStepList;


	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CLabel m_ctrlLineMode;
	afx_msg void OnNMDblclkStepList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnGetdispinfoStepList(NMHDR *pNMHDR, LRESULT *pResult);


	//200911 AUTO 버튼추가  BJY 
	CColorButton2 m_Btn_FmsReset1;
	CColorButton2 m_Btn_FmsReset2;
	CColorButton2 m_Btn_FmsEndCommandSend;
	CColorButton2 m_Btn_FmsOutCommandSend;
	afx_msg void OnBnClickedFmsEndCmdSendBtn();
	afx_msg void OnBnClickedFmsOutCmdSendBtn();
	afx_msg void OnBnClickedFmsReset2Btn();	
	afx_msg void OnBnClickedFmsReset1Btn();

};

#ifndef _DEBUG  // debug version in TopView.cpp
inline CCTSMonDoc* CAutoview::GetDocument()
{ return (CCTSMonDoc*)m_pDocument; }
#endif
