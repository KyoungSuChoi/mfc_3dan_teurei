// MeasureSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "MeasureSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMeasureSetDlg dialog


CMeasureSetDlg::CMeasureSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMeasureSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMeasureSetDlg)
	m_strMeter = _T("HP34401A");
	m_strVPort = _T("1");
	m_strIPort = _T("2");
	m_nInterval = 2000;
	m_nCount = 0;
	m_nVPort = 1;
	m_nIPort = 2;
	m_fShunt = 1.0f;
	//}}AFX_DATA_INIT
	m_bAuto = TRUE;
}


void CMeasureSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMeasureSetDlg)
	DDX_CBString(pDX, IDC_METET_SEL_COMBO, m_strMeter);
	DDX_CBString(pDX, IDC_V_PORT_COMBO, m_strVPort);
	DDX_CBString(pDX, IDC_I_PORT_COMBO, m_strIPort);
	DDX_Text(pDX, IDC_EDIT1, m_nInterval);
	DDX_Text(pDX, IDC_EDIT2, m_nCount);
	DDX_Text(pDX, IDC_EDIT3, m_fShunt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMeasureSetDlg, CDialog)
	//{{AFX_MSG_MAP(CMeasureSetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMeasureSetDlg message handlers

BOOL CMeasureSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	((CButton *)GetDlgItem(IDC_AUTO_RADIO))->SetCheck(!m_bAuto);
	((CButton *)GetDlgItem(IDC_AUTO_RADIO))->SetCheck(m_bAuto);

	m_strVPort.Format("%d", m_nVPort);
	m_strIPort.Format("%d", m_nIPort);


	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMeasureSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	m_bAuto = ((CButton *)GetDlgItem(IDC_AUTO_RADIO))->GetCheck();

	m_nVPort =	atol(m_strVPort);
	m_nIPort = atol(m_strIPort);

	
	CDialog::OnOK();
}

void CMeasureSetDlg::SetPort(int nVPort, int nIPort)
{
	m_nVPort = nVPort;
	m_nIPort = nIPort;
}

void CMeasureSetDlg::SetOption(BOOL bManual, UINT nInterval, UINT nCount, float fShunt)
{
	m_bAuto = bManual;
	m_nInterval = nInterval;
	m_nCount = nCount;
	m_fShunt = fShunt;
}
