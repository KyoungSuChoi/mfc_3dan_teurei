// TestInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "CTSMonDoc.h"
#include "TestInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestInfoDlg dialog


CTestInfoDlg::CTestInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestInfoDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CTestInfoDlg"));
	//{{AFX_DATA_INIT(CTestInfoDlg)
	m_bNotShowTestInfoDlg = FALSE;
	//}}AFX_DATA_INIT
	m_pDoc = NULL;
}

CTestInfoDlg::~CTestInfoDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CTestInfoDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}



void CTestInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestInfoDlg)
	DDX_Control(pDX, IDC_NEXT_TEST_DESCRIP, m_ctrlNextTestDescrip);
	DDX_Control(pDX, IDC_TITLE_LABEL, m_ctrlTitle);
	DDX_Control(pDX, IDC_NEXT_TEST, m_ctrlNextTestName);
	DDX_Control(pDX, IDC_TESTED_CELL_STATE, m_ctrlTestedCellState);
	DDX_Control(pDX, IDC_TESTED_DATETIME, m_ctrlTestedDate);
	DDX_Control(pDX, IDC_TESTED_NAME, m_ctrlTestedName);
	DDX_Control(pDX, IDC_START_CELL_NO, m_ctrlCellNo);
	DDX_Control(pDX, IDC_LOT_LABEL, m_ctrlLot);
	DDX_Check(pDX, IDC_CELL_DLG_CHECK, m_bNotShowTestInfoDlg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CTestInfoDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDABORT, OnAbort)
	ON_BN_CLICKED(IDC_PREV_PROC, OnPrevProc)
	ON_BN_CLICKED(IDC_NEXT_PROC, OnNextProc)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestInfoDlg message handlers

BOOL CTestInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strTemp;
	
	if(m_pDoc == NULL)	return TRUE;

	m_bNotShowTestInfoDlg = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TestInfoDlg", FALSE);

	strTemp.Format(TEXT_LANG[0], ::GetModuleName(nModuleID, nGroupIndex)); //"%s Tray Cell 정보"
	m_ctrlTitle.SetText(strTemp);
	m_ctrlTitle.SetBkColor(RGB(255,255,255));
	m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
	m_ctrlTitle.SetFontUnderline(TRUE);
	m_ctrlTitle.SetFontBold(TRUE);
	m_ctrlTitle.SetFontSize(15);

	//현재 공정 검색 
	m_pTestedTrayData = m_pDoc->GetTrayData(nModuleID, 0);
	m_curTestHeader = m_pDoc->GetTestData(m_pTestedTrayData->GetProcessSeqNo());
//	m_nextTestHeader = m_pDoc->GetNextTest(m_TestedTrayData.lTestKey);
	m_nextTestHeader = m_testHeader;

	if(m_curTestHeader.lID !=0)
	{
		m_ctrlTestedName.SetText(m_curTestHeader.szName);
		strTemp.Format("%s: %d/%d, %s: %d/%d", TEXT_LANG[4], m_pTestedTrayData->nNormalCount, m_pTestedTrayData->nNormalCount+m_pTestedTrayData->nFailCount,
			TEXT_LANG[5], m_pTestedTrayData->nFailCount, m_pTestedTrayData->nNormalCount+m_pTestedTrayData->nFailCount); 
		m_ctrlTestedCellState.SetText(strTemp);
		m_ctrlTestedDate.SetText(m_pTestedTrayData->testDateTime.Format());
	}
	else
	{
		m_ctrlTestedName.SetText(TEXT_LANG[1]);//"Test information not found."
		m_ctrlTestedCellState.SetText("");
		m_ctrlTestedDate.SetText("");
	}

	if(m_nextTestHeader.lID != 0)
	{
		m_ctrlNextTestName.SetText(m_nextTestHeader.szName);
		m_ctrlNextTestDescrip.SetText(m_nextTestHeader.szDescription);

	}
	else
	{
		m_ctrlNextTestName.SetText(TEXT_LANG[1]);//"Test information not found."
	}
	
	strTemp.Format("%d", m_pTestedTrayData->lCellNo);
	m_ctrlCellNo.SetText(strTemp);
	m_ctrlLot.SetText(m_pTestedTrayData->strLotNo);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTestInfoDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TestInfoDlg", m_bNotShowTestInfoDlg);
	m_nType = IDOK;
	CDialog::OnOK();
}

void CTestInfoDlg::OnAbort() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	AfxGetApp()->WriteProfileInt(FORMSET_REG_SECTION, "TestInfoDlg", m_bNotShowTestInfoDlg);
	m_nType = IDABORT;
	CDialog::OnOK();
}

void CTestInfoDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	m_nType = IDCANCEL;
	m_pDoc->SendInitCommand(nModuleID);
	CDialog::OnCancel();
}

void CTestInfoDlg::OnPrevProc() 
{
	// TODO: Add your control notification handler code here
	STR_CONDITION_HEADER	testHeader;		//이전 공정 저장 구조 

	//현재 공정의 이전 공정 검색 
	testHeader = m_pDoc->GetPrevTest(m_curTestHeader.lID);
	
	if(testHeader.lID == 0)
	{
		AfxMessageBox(TEXT_LANG[2]); //"Can't Find Previous Process Contition."
		return;
	}

	//다음 공정 Display
	m_nextTestHeader = m_curTestHeader;		//다음 공정이 현재 공정이 됨
	m_curTestHeader = testHeader;			//현재 공정을 이전 공정으로 Update
}

void CTestInfoDlg::OnNextProc() 
{
	// TODO: Add your control notification handler code here
	STR_CONDITION_HEADER	testHeader;
	
	//현재 공정의 이후 공정 검색 
	testHeader = m_pDoc->GetNextTest(m_curTestHeader.lID);

	if(testHeader.lID == 0)
	{
		AfxMessageBox(TEXT_LANG[3]); //"Can't Find Next Process Contition."
		return;
	}

	//다음 공정 Display
	m_curTestHeader = m_nextTestHeader;		//현재 공정을 다음 공정으로 Update
	m_nextTestHeader = testHeader;			//다음 공정을 다다음 공정으로 Update

}

void CTestInfoDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	m_nType = IDCANCEL;
	CDialog::OnCancel();	
}

void CTestInfoDlg::UpdateInformation()
{
	CString strTemp;
	if(m_curTestHeader.lID !=0)
	{
		m_ctrlTestedName.SetText(m_curTestHeader.szName);
//		strTemp.Format("정상: %d/%d, 불량: %d/%d", m_TestedTrayData.nNormalCount, m_TestedTrayData.nNormalCount+m_TestedTrayData.nFailCount,
//						m_TestedTrayData.nFailCount, m_TestedTrayData.nNormalCount+m_TestedTrayData.nFailCount); 
//		m_ctrlTestedCellState.SetText(strTemp);
//		m_ctrlTestedDate.SetText(m_TestedTrayData.testDateTime.Format());
	}
	else
	{
		m_ctrlTestedName.SetText(TEXT_LANG[6]);
		m_ctrlTestedCellState.SetText("");
		m_ctrlTestedDate.SetText("");
	}

	if(m_nextTestHeader.lID != 0)
	{
		m_ctrlNextTestName.SetText(m_nextTestHeader.szName);
		m_ctrlNextTestDescrip.SetText(m_nextTestHeader.szDescription);

	}
	else
	{
		m_ctrlNextTestName.SetText(TEXT_LANG[6]);
	}
	
	strTemp.Format("%d", m_pTestedTrayData->lCellNo);
	m_ctrlCellNo.SetText(strTemp);
	m_ctrlLot.SetText(m_pTestedTrayData->strLotNo);
}
