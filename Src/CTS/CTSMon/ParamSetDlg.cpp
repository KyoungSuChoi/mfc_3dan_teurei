// ParamSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSMon.h"
#include "ParamSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParamSetDlg dialog


CParamSetDlg::CParamSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CParamSetDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CParamSetDlg"));
	//{{AFX_DATA_INIT(CParamSetDlg)
	m_bVAutoCal = TRUE;
	m_bIAutoCal = FALSE;
	m_nHuttingCount = 3;
	m_nVhuttingLevel = 150;
	m_nIHuttingLevel = 170;
	m_nStableTime = 10;
	m_nDeltaStableTime = 60;
	m_fVRefErrLevel = 1.85f;
	m_fIRefErrLevel = 4.89f;
	//}}AFX_DATA_INIT
	m_nSelModuleID = 0;
}

CParamSetDlg::~CParamSetDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}


bool CParamSetDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CParamSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParamSetDlg)
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ctrlModuleList);
	DDX_Check(pDX, IDC_V_AUOT_CAL_CHECK, m_bVAutoCal);
	DDX_Check(pDX, IDC_I_AUTO_CAL_CHECK, m_bIAutoCal);
	DDX_Text(pDX, IDC_CHECK_COUNT_EDIT, m_nHuttingCount);
	DDV_MinMaxUInt(pDX, m_nHuttingCount, 0, 20);
	DDX_Text(pDX, IDC_DELTA_V_EDIT, m_nVhuttingLevel);
	DDV_MinMaxUInt(pDX, m_nVhuttingLevel, 0, 1000);
	DDX_Text(pDX, IDC_DELTA_I_EDIT, m_nIHuttingLevel);
	DDV_MinMaxUInt(pDX, m_nIHuttingLevel, 1, 1000);
	DDX_Text(pDX, IDC_CHECK_TIME1_EDIT, m_nStableTime);
	DDV_MinMaxUInt(pDX, m_nStableTime, 0, 3600);
	DDX_Text(pDX, IDC_CHECK_TIME2_EDIT, m_nDeltaStableTime);
	DDV_MinMaxUInt(pDX, m_nDeltaStableTime, 0, 3600);
	DDX_Text(pDX, IDC_5V_ERR_LEVEL_EDIT, m_fVRefErrLevel);
	DDV_MinMaxFloat(pDX, m_fVRefErrLevel, 0.f, 100.f);
	DDX_Text(pDX, IDC_2A_ERR_LEVEL_EDIT, m_fIRefErrLevel);
	DDV_MinMaxFloat(pDX, m_fIRefErrLevel, 0.f, 100.f);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParamSetDlg, CDialog)
	//{{AFX_MSG_MAP(CParamSetDlg)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	ON_EN_CHANGE(IDC_5V_ERR_LEVEL_EDIT, OnChange5vErrLevelEdit)
	ON_EN_CHANGE(IDC_2A_ERR_LEVEL_EDIT, OnChange2aErrLevelEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParamSetDlg message handlers

BOOL CParamSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	
	for(int i =0; i < EPGetInstalledModuleNum(); i++)
	{
		m_ctrlModuleList.AddString(::GetModuleName(::EPGetModuleID(i)));
		m_ctrlModuleList.SetItemData(i, ::EPGetModuleID(i));
	}
	UpdateText();

	DisableAllControl();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CParamSetDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here
	int index = m_ctrlModuleList.GetCurSel();
	if(index < 0)	return;

	EP_MD_CONFIG_DATA	configData;
	LPVOID lpData = NULL;
	CString strTemp;

	//이전 선택 전송 
	SendConfigDataToModule(m_nSelModuleID);
	
	m_nSelModuleID = m_ctrlModuleList.GetItemData(index);
/*	if( ::EPGetProtocolVer(m_nSelModuleID) < 0x3004)				//version check
	{
		strTemp.Format(::GetStringTable(IDS_MSG_NOT_SUPPORT_VERSION), ::GetModuleName(m_nSelModuleID),  ::EPGetProtocolVer(m_nSelModuleID));
		AfxMessageBox(strTemp);
		DisableAllControl();
		return;
	}

	if( EPGetModuleState(m_nSelModuleID) == EP_STATE_LINE_OFF)		//line state check
	{
		strTemp.Format(::GetStringTable(IDS_MSG_NOT_CONNECTED), ::GetModuleName(m_nSelModuleID));
		AfxMessageBox(strTemp);
		DisableAllControl();
		return;
	}
*/
	//Request Module Configuration Data
	int nSize = sizeof(EP_MD_CONFIG_DATA);
	lpData = EPSendDataCmd(m_nSelModuleID, 0, 0, EP_CMD_CONFIG_REQUEST, nSize);
	
	if(lpData == NULL)
	{
		AfxMessageBox(TEXT_LANG[1]);
		DisableAllControl();
		return;
	}

	memcpy(&configData, lpData, sizeof(EP_MD_CONFIG_DATA));

	UpdateConfigData(configData);

}

void CParamSetDlg::DisableAllControl()
{
	GetDlgItem(IDC_V_AUOT_CAL_CHECK)->EnableWindow(FALSE);
	GetDlgItem(IDC_I_AUTO_CAL_CHECK)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_COUNT_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_DELTA_V_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_DELTA_I_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_TIME1_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_TIME2_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_5V_ERR_LEVEL_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_2A_ERR_LEVEL_EDIT)->EnableWindow(FALSE);
}

void CParamSetDlg::EnableAllControl()
{
	GetDlgItem(IDC_V_AUOT_CAL_CHECK)->EnableWindow(TRUE);
	GetDlgItem(IDC_I_AUTO_CAL_CHECK)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_COUNT_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_DELTA_V_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_DELTA_I_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_TIME1_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_TIME2_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_5V_ERR_LEVEL_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_2A_ERR_LEVEL_EDIT)->EnableWindow(TRUE);

}

void CParamSetDlg::UpdateConfigData(EP_MD_CONFIG_DATA &configData)
{
	EnableAllControl();
	m_bVAutoCal	= configData.vAutoCal ;				//Voltage auto calibration flag
	m_bIAutoCal	= configData.iAutoCal ;				//Current auto calibration flag
	m_nHuttingCount	= configData.nHuttingCout ;			//Count of Hutting Check
	m_nVhuttingLevel=	configData.vHuttingLevel/EP_VTG_M_EXP ;			//Voltage Hutting Level
	m_nIHuttingLevel=	configData.iHuttingLevel/EP_CRT_EXP ;			//Cuttent Hutting Level
	m_nStableTime=	configData.nStableTime/EP_TIME_EXP ;				//Wait Time to Stabilization
	m_nDeltaStableTime=	configData.nDeltaVStableTime/EP_TIME_EXP ;		//Stabiliztion Time for Delta V

	m_fVRefErrLevel =	(float)configData.vRefNGLevel/16383.0f*100.0f;
	m_fIRefErrLevel =	(float)configData.iRefNGLevel/16383.0f*100.0f;

	UpdateText();
	
	UpdateData(FALSE);
}


void CParamSetDlg::OnChange5vErrLevelEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	UpdateText();
}

void CParamSetDlg::OnChange2aErrLevelEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CString strTemp;
	UpdateText();	
}

void CParamSetDlg::UpdateText()
{
	CString strTemp;
	strTemp.Format(TEXT_LANG[0], int(16383*m_fIRefErrLevel/100.0f)); //"(%d Digit 이상시 Trouble)"
	GetDlgItem(IDC_2A_STATIC)->SetWindowText(strTemp);
	strTemp.Format(TEXT_LANG[0], int(16383*m_fVRefErrLevel/100.0f)); //"(%d Digit 이상시 Trouble)"
	GetDlgItem(IDC_5V_STATIC)->SetWindowText(strTemp);

}

void CParamSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	SendConfigDataToModule(m_nSelModuleID);

	CDialog::OnOK();
}

BOOL CParamSetDlg::SendConfigDataToModule(int nModuleID)
{
	//이전 선택 모듈 Version  검사 후 변했으면 Update 실시
	CString strTemp;

/*	if( EPGetProtocolVer(nModuleID) >= 0x3004							//version Check
		&& GetDlgItem(IDC_V_AUOT_CAL_CHECK)->IsWindowEnabled()			//설정값 변화 여부 검사
		&& EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)			//Line 상태 검사
	{
*/
	if( GetDlgItem(IDC_V_AUOT_CAL_CHECK)->IsWindowEnabled()			//설정값 변화 여부 검사
		&& EPGetModuleState(nModuleID) != EP_STATE_LINE_OFF)			//Line 상태 검사
	{
		EP_MD_CONFIG_DATA	configData;
		UpdateData(TRUE);
		
		ZeroMemory(&configData, sizeof(EP_CMD_CONFIG_DATA));

		configData.vAutoCal = (BYTE)m_bVAutoCal;						//Voltage auto calibration flag
		configData.iAutoCal = (BYTE)m_bIAutoCal;						//Current auto calibration flag
		configData.nHuttingCout = (WORD)m_nHuttingCount;				//Count of Hutting Check
		configData.vHuttingLevel = m_nVhuttingLevel*EP_VTG_M_EXP;			//Voltage Hutting Level
		configData.iHuttingLevel = m_nIHuttingLevel*EP_CRT_EXP;			//Cuttent Hutting Level
		configData.nStableTime = m_nStableTime*EP_TIME_EXP;					//Wait Time to Stabilization
		configData.nDeltaVStableTime = m_nDeltaStableTime*EP_TIME_EXP;		//Stabiliztion Time for Delta V
		configData.vRefNGLevel = int(16383*m_fVRefErrLevel/100.0f);
		configData.iRefNGLevel = int(16383*m_fIRefErrLevel/100.0f);

		int nRtn;
		if((nRtn = EPSendCommand(m_nSelModuleID, 0, 0, EP_CMD_SET_CONFIG, &configData, sizeof(configData)))!= EP_ACK)
		{
			strTemp.Format("%s (%s - Code : %d)", TEXT_LANG[2], ::GetModuleName(m_nSelModuleID, 0), nRtn);		
			AfxMessageBox(strTemp);
			return FALSE;
		}

		// EPSendCommand(m_nSelModuleID);
	}
	return TRUE;
}
