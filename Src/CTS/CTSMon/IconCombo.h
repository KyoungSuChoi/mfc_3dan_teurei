#if !defined(AFX_ICONCOMBO_H__08EE884F_3729_46C7_94E0_A197E36A46FA__INCLUDED_)
#define AFX_ICONCOMBO_H__08EE884F_3729_46C7_94E0_A197E36A46FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IconCombo.h : header file
//

#define _MAX_ICON_NUM	256

/////////////////////////////////////////////////////////////////////////////
// CIconCombo window
class CIconCombo : public CComboBox
{
// Construction
public:
	CIconCombo();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIconCombo)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void ResetContent();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetItemIcon(int nIndex, int nIconIndex);
	virtual ~CIconCombo();

	/** Sets the image list to use - will be show only if SetUseImage is called. */
	void SetImageList(CImageList* pImageList) { m_pImageList = pImageList; }


	// Generated message map functions
protected:
	int m_aIconIndex[_MAX_ICON_NUM];
	CImageList *m_pImageList;
	//{{AFX_MSG(CIconCombo)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICONCOMBO_H__08EE884F_3729_46C7_94E0_A197E36A46FA__INCLUDED_)
