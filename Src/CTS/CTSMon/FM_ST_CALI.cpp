#include "stdafx.h"

#include "CTSMon.h"

#include "FMS.h"
#include "FM_Unit.h"

#include "FM_STATE.h"

CFM_ST_CALI::CFM_ST_CALI(FM_STATUS_ID _Eqstid, FM_STATUS_ID _stid, CFM_Unit* _unit)
:CFM_STATE(_Eqstid, _stid, _unit)
, m_pstCACali(NULL)
, m_pstCAError(NULL)
, m_pstCATrayIn(NULL)
, m_pstCAVacancy(NULL)
, m_pstCAReady(NULL)
, m_pstCAEnd(NULL)
{
}


CFM_ST_CALI::~CFM_ST_CALI(void)
{
	if(m_pstCAVacancy)
	{
		delete m_pstCAVacancy;
		m_pstCAVacancy = NULL;
	}
	if(m_pstCAReady)
	{
		delete m_pstCAReady;
		m_pstCAReady = NULL;
	}
	if(m_pstCATrayIn)
	{
		delete m_pstCATrayIn;
		m_pstCATrayIn = NULL;
	}
	if(m_pstCACali)
	{
		delete m_pstCACali;
		m_pstCACali = NULL;
	}
	if(m_pstCAError)
	{
		delete m_pstCAError;
		m_pstCAError = NULL;
	}
	if(m_pstCAEnd)
	{
		delete m_pstCAEnd;
		m_pstCAEnd = NULL;
	}
}

VOID CFM_ST_CALI::fnEnter()
{
	CFMS* fms = m_Unit->fnGetFMS();

	//마지막이 캘리였다면 상태불러오기
	if(fms->m_LastMode == EP_OPERATION_CALIBRATION && fms->m_LastInit == FALSE)
	{
		fms->m_LastInit = TRUE;
		int nModuleID = m_Unit->fnGetModuleID();
		CString strTemp;
		
		//state
		EP_GP_STATE_DATA prevState;
		memcpy(&prevState, (const void *)&EPGetGroupData(nModuleID, NULL).gpState, sizeof(EP_GP_STATE_DATA));
		prevState.state = EPFmsStateToEPState(fms->m_LastState);
		CFormModule *pModule = NULL;
		pModule = m_Unit->fnGetModule();
		pModule->SetState(prevState);	

		//tray id
		if(fms->m_LastTrayID[0].GetLength() == 6 && fms->m_LastTrayID[1].GetLength() == 6)

		{
			fms->fnSetTrayID(fms->m_LastTrayID[0] + fms->m_LastTrayID[1]);
		}

		strTemp.Format("StageLastState_%02d", nModuleID);
		AfxGetApp()->WriteProfileInt(FMS_REG, strTemp, fms->m_LastState);
		strTemp.Format("StageLastTray1_%02d", nModuleID);
		AfxGetApp()->WriteProfileString(FMS_REG, strTemp, fms->m_LastTrayID[0]);
		strTemp.Format("StageLastTray2_%02d",nModuleID);
		AfxGetApp()->WriteProfileString(FMS_REG, strTemp, fms->m_LastTrayID[1]);

		
	}
	CHANGE_STATE(CALI_ST_VACANCY);

	TRACE("CFM_ST_CALI::fnEnter %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CALI::fnProc()
{
	TRACE("CFM_ST_CALI::fnProc %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CALI::fnExit()
{
	TRACE("CFM_ST_CALI::fnExit %d \n", m_Unit->fnGetModuleIdx());
}

VOID CFM_ST_CALI::fnSBCPorcess(WORD _state)
{
	if(m_Unit->fnGetModule()->GetState() == EP_STATE_LINE_OFF)
	{
		CHANGE_STATE(EQUIP_ST_OFF);
	}
	else
	{
		int nModuleID = m_Unit->fnGetModuleID();
		CString strTemp;
		switch(m_Unit->fnGetModule()->GetOperationMode())
		{
		case EP_OPERATION_LOCAL:
			m_Unit->fnGetModule()->ResetGroupData();	
			CHANGE_STATE(EQUIP_ST_LOCAL);
			break;
		case EP_OPEARTION_MAINTENANCE:
			m_Unit->fnGetModule()->ResetGroupData();	
			CHANGE_STATE(EQUIP_ST_MAINT);
			break;
		case EP_OPERATION_AUTO:
			m_Unit->fnGetModule()->ResetGroupData();	
			CHANGE_STATE(EQUIP_ST_AUTO);
			break;
		case EP_OPERATION_CALIBRATION:
//			CHANGE_STATE(EQUIP_ST_CALIBRATION);
			break;
		}
	}
	TRACE("CFM_ST_CALI::fnSBCPorcess %d \n", m_Unit->fnGetModuleIdx());
}

FMS_ERRORCODE CFM_ST_CALI::fnFMSPorcess( st_HSMS_PACKET* _recvData)
{
	FMS_ERRORCODE _error = ER_NO_Process;

// 	switch(_msgId)
// 	{
// 	default:
// 		{
// 		}
// 		break;
// 	}

	TRACE("CFM_ST_CALI::fnFMSPorcess %d \n", m_Unit->fnGetModuleIdx());

	return _error;

}