#if !defined(AFX_CALFILEDLG_H__DC56652F_0EC6_4397_B657_EE568C57B37A__INCLUDED_)
#define AFX_CALFILEDLG_H__DC56652F_0EC6_4397_B657_EE568C57B37A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalFileDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCalFileDlg dialog
#include <afxinet.h>

class CCalFileDlg : public CDialog
{
// Construction
public:
	BOOL AddModuleFileList(CString strFileName);
	BOOL UpdateModuleFileList(int nModuleID);
	int GetBoardNo(CString strFileName);
	CString GetIPAddress(int nModuleID);
	BOOL SendFileToModule(int nModuleID, CStringArray &strArray);
	BOOL LoadFileList(int nModuleID = 0);
	BOOL InitList();
	BOOL SetModuleNum(int nNum);
	int m_nInstalledModuleNum;
	CCalFileDlg(CWnd* pParent = NULL);   // standard constructor
//	CString GetModuleName(int nModuleID, int nGroupIndex = 0);
	CString m_strModuleName;
//	CString m_strGroupName;
//	int m_nModulePerRack ;
//	BOOL m_bUseRackIndex;
//	BOOL m_bUseGroupSet;
	void Recurse(CString strCurLocation, int nModuleID = 0);
// Dialog Data
	//{{AFX_DATA(CCalFileDlg)
	enum { IDD = IDD_CALIBRATION_DLG };
	CListCtrl	m_ctrlModuleFileList;
	CListCtrl	m_ctrlFileList;
	CComboBox	m_ctrlModuleSel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalFileDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCalFileDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnUpdateButton();
	afx_msg void OnSelchangeSelModuleCombo();
	afx_msg void OnAllUpdateButton();
	afx_msg void OnDblclkModuleFileList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkCalFileList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALFILEDLG_H__DC56652F_0EC6_4397_B657_EE568C57B37A__INCLUDED_)
