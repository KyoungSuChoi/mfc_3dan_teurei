// IconCombo.cpp : implementation file
//

#include "stdafx.h"
#include "IconCombo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIconCombo

CIconCombo::CIconCombo()
: m_pImageList(NULL)
{
	memset(m_aIconIndex, -1, _MAX_ICON_NUM*sizeof(int));
}

CIconCombo::~CIconCombo()
{
}


BEGIN_MESSAGE_MAP(CIconCombo, CComboBox)
	//{{AFX_MSG_MAP(CIconCombo)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIconCombo message handlers

void CIconCombo::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your code to draw the specified item
	if (GetCount() == 0 || lpDrawItemStruct->itemID > GetCount()) return;
   
   CString str;
   GetLBText(lpDrawItemStruct->itemID,str);

   CDC dc;
//  TRACE("Item %d- %s\n", lpDrawItemStruct->itemID,str);
   dc.Attach(lpDrawItemStruct->hDC);

   // Save these value to restore them when done drawing.
   COLORREF crOldTextColor = dc.GetTextColor();
   COLORREF crOldBkColor = dc.GetBkColor();

   BOOL bold = FALSE;
   CRect rect(lpDrawItemStruct->rcItem);
   rect.DeflateRect(1,0);

   // If this item is selected, set the background color 
   // and the text color to appropriate values. Erase
   // the rect by filling it with the background color.
   if ((lpDrawItemStruct->itemAction | ODA_SELECT) &&
      (lpDrawItemStruct->itemState  & ODS_SELECTED))
   {
      dc.SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
      dc.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
      dc.FillSolidRect(&rect, ::GetSysColor(COLOR_HIGHLIGHT));
   }
   else
    dc.FillSolidRect(&rect, crOldBkColor);

    //�ڡ� ljb 2009128 S �ڡڡڡڡڡڡڡ�
   if(m_pImageList)
   {
	   if(lpDrawItemStruct->itemID < _MAX_ICON_NUM)
	   {
		   int index = m_aIconIndex[lpDrawItemStruct->itemID];
		   if(index >=0 && index < m_pImageList->GetImageCount())
		   {
			   HICON hICon = m_pImageList->ExtractIcon(index);
			   //  [7/8/2009 kky ]
			   // for Error �ּ�			   
			   DrawIconEx(dc.GetSafeHdc(),rect.left,rect.top, hICon,0, 0, 0, NULL, DI_NORMAL);
			   // DrawIconEx(dc.GetSafeHdc(),rect.left,rect.top, m_pImageList->ExtractIcon(index),0, 0, 0, NULL, DI_NORMAL);
			   DestroyIcon( hICon );
			   IMAGEINFO imageInfo;
			   m_pImageList->GetImageInfo( 0,   &imageInfo );
			   rect.left += imageInfo.rcImage.right + 2;
		   }
	   }   
   }
   //�ڡ� ljb 2009128 E �ڡڡڡڡڡڡڡ�
   //  SetItemHeight(lpDrawItemStruct->itemID, imageInfo.rcImage.bottom);
   CFont *curFont = dc.GetCurrentFont();
   CFont boldFont,*oldFont;

   LOGFONT lf;
   curFont->GetLogFont(&lf);
   lf.lfWeight = FW_BOLD;
   boldFont.CreateFontIndirect(&lf);
   if(bold)
	   oldFont = dc.SelectObject(&boldFont);

   // Draw the text.
   dc.DrawText(
      str,
      -1,
      &rect,
      DT_LEFT|DT_SINGLELINE|DT_VCENTER);
   if (bold)
		dc.SelectObject(oldFont);
	boldFont.DeleteObject();
   // Reset the background color and the text color back to their
   // original values.
   dc.SetTextColor(crOldTextColor);
   dc.SetBkColor(crOldBkColor);
   dc.Detach();
}

void CIconCombo::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	// TODO: Add your code to determine the size of specified item
	IMAGEINFO imageInfo;
	if(m_pImageList)
	{
		m_pImageList->GetImageInfo( 0,   &imageInfo );
		lpMeasureItemStruct->itemHeight = imageInfo.rcImage.bottom;
	}
}

BOOL CIconCombo::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
//	  ::SendMessage(m_hWnd, CB_SETITEMHEIGHT, (WPARAM)-1, 34L);
	return CComboBox::PreCreateWindow(cs);
}

void CIconCombo::SetItemIcon(int nIndex, int nIconIndex)
{
	if(nIconIndex < 0 || nIconIndex>= _MAX_ICON_NUM)	return;
	
	m_aIconIndex[nIndex] = nIconIndex;
}

void CIconCombo::ResetContent()
{
	memset(m_aIconIndex, -1, _MAX_ICON_NUM*sizeof(int));
	CComboBox::ResetContent();
}
