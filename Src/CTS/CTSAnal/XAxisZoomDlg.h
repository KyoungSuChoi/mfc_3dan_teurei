#if !defined(AFX_XAXISZOOMDLG_H__550C9200_AB44_11D4_905E_0001027DB21C__INCLUDED_)
#define AFX_XAXISZOOMDLG_H__550C9200_AB44_11D4_905E_0001027DB21C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// XAxisZoomDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CXAxisZoomDlg dialog

class CXAxisZoomDlg : public CDialog
{
// Construction
public:
	CXAxisZoomDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CXAxisZoomDlg)
	enum { IDD = IDD_HSCROLL_POINT };
	UINT	m_nHscrollPonitNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXAxisZoomDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CXAxisZoomDlg)
	afx_msg void OnOk();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XAXISZOOMDLG_H__550C9200_AB44_11D4_905E_0001027DB21C__INCLUDED_)
