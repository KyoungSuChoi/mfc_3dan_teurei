#if !defined(AFX_SEARCHMDBSETFIELDDLG_H__F75E7213_C060_40F3_B8F7_1B3A59569E57__INCLUDED_)
#define AFX_SEARCHMDBSETFIELDDLG_H__F75E7213_C060_40F3_B8F7_1B3A59569E57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SearchMDBSetFieldDlg.h : header file
//
#include "MyGridWnd.h"	// Added by ClassView

/////////////////////////////////////////////////////////////////////////////
// CSearchMDBSetFieldDlg dialog

class CSearchMDBSetFieldDlg : public CDialog
{
// Construction
public:
	virtual ~CSearchMDBSetFieldDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
	CString Fun_GetSaveUnit(CString strField);
	BOOL LoadMDBField();
	void InitGrid();
	CSearchMDBSetFieldDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSearchMDBSetFieldDlg)
	enum { IDD = IDD_SEARCH_MDB_FIELD_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchMDBSetFieldDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSearchMDBSetFieldDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnAdd();
	afx_msg void OnBtnDel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CMyGridWnd m_wndFieldGrid;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHMDBSETFIELDDLG_H__F75E7213_C060_40F3_B8F7_1B3A59569E57__INCLUDED_)
