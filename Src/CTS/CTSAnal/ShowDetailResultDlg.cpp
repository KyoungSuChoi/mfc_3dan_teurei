// ShowDetailResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ShowDetailResultDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShowDetailResultDlg dialog


CShowDetailResultDlg::CShowDetailResultDlg(CFormResultFile * pFile, CWnd* pParent /*=NULL*/)
	: CDialog(CShowDetailResultDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CShowDetailResultDlg"));
	//{{AFX_DATA_INIT(CShowDetailResultDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pResultFile = pFile;
	ASSERT(m_pResultFile);
}

CShowDetailResultDlg::~CShowDetailResultDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CShowDetailResultDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}




void CShowDetailResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShowDetailResultDlg)
	DDX_Control(pDX, IDC_FILE_TREE, m_fileTree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CShowDetailResultDlg, CDialog)
	//{{AFX_MSG_MAP(CShowDetailResultDlg)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShowDetailResultDlg message handlers

BOOL CShowDetailResultDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString str, strTemp;

	GetDlgItem(IDC_EXT_DATA)->SetWindowText(m_pResultFile->GetFileName());
	HTREEITEM rghItem, subItem1, subItem2, subItem3, subItem4, subItem5, subItem6;

	//////////////////////////////////////////////////////////////////////////
	EP_FILE_HEADER *pHeader = m_pResultFile->GetFileHeader();
	rghItem = m_fileTree.InsertItem(TEXT_LANG[0]); //"파일정보"
	str.Format("File ID : %s", pHeader->szFileID);
	m_fileTree.InsertItem(str, rghItem);
	str.Format("File Ver : %s", pHeader->szFileVersion);
	m_fileTree.InsertItem(str, rghItem);
	str.Format("Date Time : %s", pHeader->szCreateDateTime);
	m_fileTree.InsertItem(str, rghItem);
	str.Format(TEXT_LANG[2]+" : %s", pHeader->szDescrition); //"설명
	m_fileTree.InsertItem(str, rghItem);
	str.Format(TEXT_LANG[1]+" : %s", pHeader->szReserved); //"부가정보
	m_fileTree.InsertItem(str, rghItem);
	
	//////////////////////////////////////////////////////////////////////////
	rghItem = m_fileTree.InsertItem(TEXT_LANG[3]); //"작업정보"

	RESULT_FILE_HEADER *pRltHeader = m_pResultFile->GetResultHeader();
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[4], rghItem); //"공정정보"
	str.Format("ModuleID : %d", pRltHeader->nModuleID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Group : %d", pRltHeader->wGroupIndex+1);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Jig : %d", pRltHeader->wJigIndex+1);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[5]+" : %s", pRltHeader->szDateTime); //"시간"
	m_fileTree.InsertItem(str, subItem1);
	str.Format("IP : %s", pRltHeader->szModuleIP);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[6]+" : %s", pRltHeader->szTrayNo); //"Tray 번호
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[7]+" : %s", pRltHeader->szLotNo); //"Lot 번호
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[8]+" : %s", pRltHeader->szOperatorID); //"작업자 ID
	m_fileTree.InsertItem(str, subItem1);
	str.Format("TestSerial : %s", pRltHeader->szTestSerialNo);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("TraySerial : %s", pRltHeader->szTraySerialNo);
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	EP_MD_SYSTEM_DATA *pSysData = m_pResultFile->GetMDSysData();
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[9], rghItem); //"모듈정보"
	str.Format("ModuleID : %d", pSysData->nModuleID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Protocol Ver : 0x%x", pSysData->nVersion);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[10]+" : %d", pSysData->nControllerType); //"제어기 Type
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[11]+" : %d", pSysData->wInstalledBoard); //"설치 보드수"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[12]+" : %d", pSysData->wChannelPerBoard); //"보드별채널수
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[13]+" : %d", pSysData->nModuleGroupNo);//"모듈 GroupNo
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[14]+" : %d", pSysData->nTotalChNo); //"Channel 수
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[15]+" : %d", pSysData->wTotalTrayNo); //"설치 Tray수
	m_fileTree.InsertItem(str, subItem1);
	str = TEXT_LANG[16]+" : "; //"Tray별채널수

	int a = 0;
	for(a = 0; a<pSysData->wTotalTrayNo; a++)
	{
		strTemp.Format("%d/", pSysData->awChInTray[a]);
		str += strTemp;
	}
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Tray Type : %d", pSysData->wTrayType);
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[17], rghItem); //"기타정보"
	STR_FILE_EXT_SPACE *pExtSpace = m_pResultFile->GetExtraData();
	str.Format(TEXT_LANG[18] +" : %s", pExtSpace->szResultFileName);//"File명"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[19]+" : %d", pExtSpace->nCellNo); //"Cell번호
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[20]+" : %d", pExtSpace->nInput1stCellNo); //"1단 입력수량"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[21]+" : %d", pExtSpace->nInput2ndCellNo); //"2단 입력수량
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[21]+" : %d", pExtSpace->nInput3rdCellNo); //"3단 입력수량
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[22]+" : %d", pExtSpace->nReserved); //"Tray Cell 수량
	m_fileTree.InsertItem(str, subItem1);
	str.Format("System ID : %d", pExtSpace->nSystemID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("System Type : %d", pExtSpace->nSystemType);
	m_fileTree.InsertItem(str, subItem1);
	str.Format("Module Name : %s", pExtSpace->szModuleName);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[1]+" : %s", pExtSpace->szReserved); //"부가정보"
	m_fileTree.InsertItem(str, subItem1);

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem("Cell ID", rghItem);
	PNE_RESULT_CELL_SERIAL *pSerial = m_pResultFile->GetResultCellSerial();
	for(int i=0; i<EP_MAX_CH_PER_MD; i++)
	{
		str.Format("CH %d :: %s", i+1, pSerial->szCellNo[i]);
		m_fileTree.InsertItem(str, subItem1);
	}

	//////////////////////////////////////////////////////////////////////////
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[24], rghItem); //"센서 Mapping"
	subItem2 = m_fileTree.InsertItem(TEXT_LANG[25]+"1", subItem1); //"센서"
	_MAPPING_DATA *pSensorMap = m_pResultFile->GetSensorMap(CFormResultFile::sensorType1);
	int s = 0;
	for(s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		if(pSensorMap[s].nChannelNo < 0)
		{
			str.Format("Ch %02d: "+TEXT_LANG[26], s+1); //"사용안함"
		}
		else if(pSensorMap[s].nChannelNo == 0)
		{
			str.Format("Ch %02d: Universal", s+1);
		}
		else
		{
			str.Format("Ch %02d: %s => "+TEXT_LANG[27]+" %d", s+1, pSensorMap[s].szName, pSensorMap[s].nChannelNo); //"충방전채널"
		}
		m_fileTree.InsertItem(str, subItem2);

	}
	subItem2 = m_fileTree.InsertItem(TEXT_LANG[25]+"2", subItem1); //"센서"
	pSensorMap = m_pResultFile->GetSensorMap(CFormResultFile::sensorType2);
	for( s=0; s<EP_MAX_SENSOR_CH; s++)
	{
		if(pSensorMap[s].nChannelNo < 0)
		{
			str.Format("Ch %02d: "+TEXT_LANG[26], s+1); //사용안함"
		}
		else if(pSensorMap[s].nChannelNo == 0)
		{
			str.Format("Ch %02d: Universal", s+1);
		}
		else
		{
			str.Format("Ch %02d: %s => "+TEXT_LANG[27]+" %d", s+1, pSensorMap[s].szName, pSensorMap[s].nChannelNo);//"충방전채널"
		}
		m_fileTree.InsertItem(str, subItem2);
	}

	//////////////////////////////////////////////////////////////////////////
	rghItem = m_fileTree.InsertItem(TEXT_LANG[28]); //"시험조건"
	subItem1 = m_fileTree.InsertItem(TEXT_LANG[29], rghItem); //"모델정보"
	//STR_CONDITION *pCon = m_pResultFile->GetConditionMain();
	CTestCondition *pCon = m_pResultFile->GetTestCondition();
	str.Format("DB PK : %d", pCon->GetModelInfo()->lID);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[30]+" : %d", pCon->GetModelInfo()->lNo); //"No"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[31]+" : %d", pCon->GetModelInfo()->lType); //"Type"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[32]+" : %s", pCon->GetModelInfo()->szName); //"Name"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[2]+" : %s", pCon->GetModelInfo()->szDescription);//"설명"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[33]+" : %s", pCon->GetModelInfo()->szCreator);//"작성자"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[34]+" : %s", pCon->GetModelInfo()->szModifiedTime);//"작성일"
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[4], rghItem); //"공정정보"
	str.Format("DB PK : %s", pCon->GetModelInfo()->szProcesstype);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[30]+" : %d", pCon->GetTestInfo()->lNo);//"No"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[31]+" : %d", pCon->GetTestInfo()->lType);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[32]+" : %s", pCon->GetTestInfo()->szName);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[2]+" : %s", pCon->GetTestInfo()->szDescription);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[33]+" : %s", pCon->GetTestInfo()->szCreator);
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[34]+" : %s", pCon->GetTestInfo()->szModifiedTime);
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[35], rghItem); //"Step수"
	str.Format(TEXT_LANG[36]+" : %d", pCon->GetTotalStepNo()); //"총 Step수"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[37]+" : %d", 0); //"총 등급 Step수"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[1]+"1 : %s", ""); //"부가정보"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[1]+"2 : %s", "");
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[1]+"3 : %d", "");
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[38], rghItem); //"검사조건"
	str.Format(TEXT_LANG[39]+" : %f", pCon->GetCheckParam()->fOCVUpperValue); //"전압상한"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[40]+" : %f", pCon->GetCheckParam()->fOCVLowerValue); //"전압하한"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[41]+" : %f", pCon->GetCheckParam()->fVRef); //"초기하한전압"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[43]+" : %f", pCon->GetCheckParam()->fIRef); //"전류설정"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[5]+" : %f", pCon->GetCheckParam()->fTime); //"시간"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[44]+" : %f", pCon->GetCheckParam()->fDeltaVoltage); //"상한전류"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[45]+" : %f", pCon->GetCheckParam()->fMaxFaultBattery); //"하한전류"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[46]+" : %f", pCon->GetCheckParam()->fDeltaVoltageLimit); //"DeltaVoltageLimit"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[47]+" : %d", pCon->GetCheckParam()->compFlag); //"사용여부"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[48]+" : %d", pCon->GetCheckParam()->autoProcessingYN); //"자동공정"
	m_fileTree.InsertItem(str, subItem1);
	str.Format(TEXT_LANG[1]+" : %d", pCon->GetCheckParam()->reserved); //"부가정보"
	m_fileTree.InsertItem(str, subItem1);

	subItem1 = m_fileTree.InsertItem(TEXT_LANG[49], rghItem); //"Step조건"
	STR_COMMON_STEP *pStep;
	for(a=0; a < pCon->GetTotalStepNo(); a++)
	{
		str.Format("Step %d", a+1);
		subItem2 = m_fileTree.InsertItem(str, subItem1);
		pStep = (STR_COMMON_STEP *)pCon->GetStep(a);
		
		subItem3 = m_fileTree.InsertItem("Step Header", subItem2);
		str.Format(TEXT_LANG[50]+" : %d", pStep->stepHeader.stepIndex); //"Step번호"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[31]+" : %d", pStep->stepHeader.type); //"Type"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[51]+" : %d", pStep->stepHeader.mode); //"Mode"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[53]+" : %d", pStep->stepHeader.gradeSize); //"등급Step수"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[52]+" : 0x%x", pStep->stepHeader.nProcType); //"공정 Type"
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[42]+" : %f", pStep->fVref); //"전압설정
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[43]+" : %f", pStep->fIref); //"전류설정"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[54]+" : %f", pStep->fEndTime); //"종료시간"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[55]+" : %f", pStep->fEndV); //"종료전압"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[56]+" : %f", pStep->fEndI); //"종료전류"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[57]+" : %f", pStep->fEndC); //"종료용량"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[58]+" : %f", pStep->fEndDV); //"종료전압변화"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[59]+" : %f", pStep->fEndDI); //"종료전류변화"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[60]+" : %d", pStep->bUseActucalCap); //"용량기준 Falg"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[1]+"1 : %d", pStep->bReserved); //"부가정보"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[61]+" : %f", pStep->fSocRate); //"종료 SOC"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[39]+" : %f", pStep->fOverV); //"전압상한"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[40]+" : %f", pStep->fLimitV); //"전압하한"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[62]+" : %f", pStep->fOverI); //"전류상한"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[63]+" : %f", pStep->fLimitI); //"전류하한"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[64]+" : %f", pStep->fOverC); //"용량상한"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[65]+" : %f", pStep->fLimitC); //"용량하한"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[66]+" : %f", pStep->fOverImp); //"저항상한"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[67]+" : %f", pStep->fLimitImp); //"저항하한"
		m_fileTree.InsertItem(str, subItem2);
		
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[68], subItem2); //"전압비교조건"
		int b = 0;
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[69]+" %d : %f< %f< %f", b+1, pStep->fCompVLow[b],  pStep->fCompTimeV[b],  pStep->fCompVHigh[b]); //"비교전압"
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[84], subItem2); //"전류비교조건"
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[70]+" %d : %f< %f< %f", b+1, pStep->fCompILow[b],  pStep->fCompTimeI[b],  pStep->fCompIHigh[b]); //"비교전류"
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[71], subItem2); //"변화량조건"
		str.Format(TEXT_LANG[72]+": %f/%f", pStep->fDeltaV, pStep->fDeltaTimeV); //"전압변화량 불량"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[73]+": %f/%f", pStep->fDeltaI, pStep->fDeltaTimeI); //"전류변화량 불량"
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[74]+" : %d", pStep->nLoopInfoGoto); //이동 Step
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[75]+" : %d", pStep->nLoopInfoCycle); //반복횟수
		m_fileTree.InsertItem(str, subItem2);

		subItem3 = m_fileTree.InsertItem(TEXT_LANG[76], subItem2); //"저장조건"
		str.Format(TEXT_LANG[77]+" : %f", pStep->fRecDeltaTime); //"시간변화"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[78]+" : %f", pStep->fRecDeltaV); //"전압변화"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[79]+" : %f", pStep->fRecDeltaI); //"전류변화"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[80]+" : %f", pStep->fRecDeltaT); //"온도변화"
		m_fileTree.InsertItem(str, subItem3);

	
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[81], subItem2); //"등급조건"
		for(b = 0; b<EP_MAX_GRADE_STEP; b++)
		{
			str.Format(TEXT_LANG[81]+" %d : No %d, Item %d, %f <Value <%f =>%c", b+1, //"등급" 
						pStep->grade[b].index+1, pStep->grade[b].gradeItem, pStep->grade[b].fMin, pStep->grade[b].fMax, pStep->grade[b].gradeCode);
			m_fileTree.InsertItem(str, subItem3);
		}

		//float		fReserved[16];
	}

	rghItem = m_fileTree.InsertItem(TEXT_LANG[85]); //"스텝결과"

	STR_STEP_RESULT *pResult;
	int nStepSize = m_pResultFile->GetStepSize();
	for( a = 0; a<nStepSize; a++)
	{
		pResult = m_pResultFile->GetStepData(a);
		if(pResult == NULL)	break;

		str.Format("Step %d", a+1);
		subItem1 = m_fileTree.InsertItem(str, rghItem);

		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[86], subItem1); //"상태"
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[87], subItem2); //"지그상태"
		str.Format(TEXT_LANG[88]+": %d", pResult->gpStateSave.gpState.state); //"그룹상태"
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Fail Code : %d", pResult->gpStateSave.gpState.failCode);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor1 : 0x%x", pResult->gpStateSave.gpState.sensorState1);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor2 : 0x%x", pResult->gpStateSave.gpState.sensorState2);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor3 : 0x%x", pResult->gpStateSave.gpState.sensorState3);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor4 : 0x%x", pResult->gpStateSave.gpState.sensorState4);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor5 : 0x%x", pResult->gpStateSave.gpState.sensorState5);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor6 : 0x%x", pResult->gpStateSave.gpState.sensorState6);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor7 : 0x%x", pResult->gpStateSave.gpState.sensorState7);
		m_fileTree.InsertItem(str, subItem3);
		str.Format("Sensor8 : 0x%x", pResult->gpStateSave.gpState.sensorState8);
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[1]+" : %d", pResult->gpStateSave.gpState.lReserved[0]); //"부가정보"
		m_fileTree.InsertItem(str, subItem3);

		subItem3 = m_fileTree.InsertItem(TEXT_LANG[89], subItem2); //"센서상태"
		subItem4 = m_fileTree.InsertItem(TEXT_LANG[25]+"1", subItem3); //"센서"
		int s = 0;
		for(s=0; s<EP_MAX_SENSOR_CH; s++)
		{
			str.Format("Ch %d", s+1);
			subItem5 = m_fileTree.InsertItem(str, subItem4);

			str.Format(TEXT_LANG[90]+" : %f", pResult->gpStateSave.sensorChData1[s].fData); //"Sensor값"
			m_fileTree.InsertItem(str, subItem5);
			str.Format("Reserved : %d", pResult->gpStateSave.sensorChData1[s].lReserved);
			m_fileTree.InsertItem(str, subItem5);
			subItem6 = m_fileTree.InsertItem(TEXT_LANG[91], subItem5); //"최대최소"
			str.Format(TEXT_LANG[92]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMax); //최대값
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[93]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMin); //최소값
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[94]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lAvg); //평균값
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[95]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMaxIndex); //최대값Index
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[96]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMinIndex); //최소값Index
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[97]+" : %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMaxDateTime); //최대값 시간
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[98]+": %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMinDateTime); //최소값 시간
			m_fileTree.InsertItem(str, subItem6);
		}

		subItem4 = m_fileTree.InsertItem(TEXT_LANG[25]+"2", subItem3); //"센서"
		for( s=0; s<EP_MAX_SENSOR_CH; s++)
		{
			str.Format("Ch %d", s+1);
			subItem5 = m_fileTree.InsertItem(str, subItem4);

			str.Format(TEXT_LANG[90]+" : %f", pResult->gpStateSave.sensorChData1[s].fData); //"Sensor값"
			m_fileTree.InsertItem(str, subItem5);
			str.Format("Reserved : %d", pResult->gpStateSave.sensorChData2[s].lReserved);
			m_fileTree.InsertItem(str, subItem5);
			subItem6 = m_fileTree.InsertItem(TEXT_LANG[91], subItem5); //"최대최소"
			str.Format(TEXT_LANG[92]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMax); //최대값
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[93]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lMin); //최소값
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[94]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.lAvg); //평균값
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[95]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMaxIndex); //최대값Index
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[96]+" : %d", pResult->gpStateSave.sensorChData1[s].minmaxData.wMinIndex); //최소값Index
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[97]+" : %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMaxDateTime); //최대값 시간
			m_fileTree.InsertItem(str, subItem6);
			str.Format(TEXT_LANG[98]+": %s", pResult->gpStateSave.sensorChData1[s].minmaxData.szMinDateTime); //최소값 시간
			m_fileTree.InsertItem(str, subItem6);
		}

		//
		subItem2 = m_fileTree.InsertItem("Step Header", subItem1);
		str.Format("ModuleID : %d", pResult->stepHead.nModuleID);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Group : %d", pResult->stepHead.wGroupIndex+1);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("Jig : %d", pResult->stepHead.wJigIndex+1);
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[120]+" : %s", pResult->stepHead.szStartDateTime); //시작시간
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[54]+" : %s", pResult->stepHead.szEndDateTime); //"종료 시간"
		m_fileTree.InsertItem(str, subItem2);
		str.Format("IP : %s", pResult->stepHead.szModuleIP);
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[6]+" : %s", pResult->stepHead.szTrayNo); //"Tray 번호"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[8]+" : %s", pResult->stepHead.szOperatorID); //"작업자 ID"
		m_fileTree.InsertItem(str, subItem2);
		str.Format("TestSerial : %s", pResult->stepHead.szTestSerialNo);
		m_fileTree.InsertItem(str, subItem2);
		str.Format("TraySerial : %s", pResult->stepHead.szTraySerialNo);
		m_fileTree.InsertItem(str, subItem2);

		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[121], subItem1); //"조건"	
		STR_COMMON_STEP *pStep = &pResult->stepCondition;
		subItem3 = m_fileTree.InsertItem("Step Header", subItem2);
		str.Format(TEXT_LANG[50]+" : %d", pStep->stepHeader.stepIndex); //"Step번호"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[31]+" : %d", pStep->stepHeader.type); //"Type"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[51]+" : %d", pStep->stepHeader.mode); //"Mode"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[53]+" : %d", pStep->stepHeader.gradeSize); //"등급Step수"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[52]+" : 0x%x", pStep->stepHeader.nProcType); //공정 Type
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[42]+" : %f", pStep->fVref); //전압설정
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[43]+" : %f", pStep->fIref); //전류설정
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[54]+" : %f", pStep->fEndTime); //종료시간
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[55]+" : %f", pStep->fEndV); //종료전압
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[56]+" : %f", pStep->fEndI); //종료전류
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[57]+" : %f", pStep->fEndC); //종료용량
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[58]+" : %f", pStep->fEndDV); //종료전압변화
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[59]+" : %f", pStep->fEndDI); //종료전류변화
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[60]+" Falg : %d", pStep->bUseActucalCap); //용량기준
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[1]+"1 : %d", pStep->bReserved); //부가정보
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[61]+" : %f", pStep->fSocRate); //종료 SOC
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[39]+" : %f", pStep->fOverV); //전압상한
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[40]+" : %f", pStep->fLimitV); //전압하한
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[62]+" : %f", pStep->fOverI); //전류상한
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[63]+" : %f", pStep->fLimitI); //전류하한
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[64]+" : %f", pStep->fOverC); //용량상한
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[65]+" : %f", pStep->fLimitC); //용량하한
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[66]+" : %f", pStep->fOverImp); //저항상한
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[67]+" : %f", pStep->fLimitImp); //저항하한
		m_fileTree.InsertItem(str, subItem2);
		
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[68], subItem2); //"전압비교조건"
		int b=0;
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[69]+" %d : %f< %f< %f", b+1, pStep->fCompVLow[b],  pStep->fCompTimeV[b],  pStep->fCompVHigh[b]); //비교전압
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[84], subItem2); //"전류비교조건"
		for(b = 0; b<EP_COMP_POINT; b++)
		{
			str.Format(TEXT_LANG[70]+" %d : %f< %f< %f", b+1, pStep->fCompILow[b],  pStep->fCompTimeI[b],  pStep->fCompIHigh[b]); //비교전류
			m_fileTree.InsertItem(str, subItem3);
		}
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[71], subItem2); //"변화량조건"
		str.Format(TEXT_LANG[72]+": %f/%f", pStep->fDeltaV, pStep->fDeltaTimeV); //"전압변화량 불량"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[73]+": %f/%f", pStep->fDeltaI, pStep->fDeltaTimeI); //"전류변화량 불량"
		m_fileTree.InsertItem(str, subItem3);
		
		str.Format(TEXT_LANG[74]+" : %d", pStep->nLoopInfoGoto); //"이동 Step"
		m_fileTree.InsertItem(str, subItem2);
		str.Format(TEXT_LANG[75]+" : %d", pStep->nLoopInfoCycle); //"반복횟수"
		m_fileTree.InsertItem(str, subItem2);

		subItem3 = m_fileTree.InsertItem(TEXT_LANG[76], subItem2); //"저장조건"
		str.Format(TEXT_LANG[77]+" : %f", pStep->fRecDeltaTime); //"시간변화"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[78]+" : %f", pStep->fRecDeltaV); //"전압변화"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[79]+" : %f", pStep->fRecDeltaI); //"전류변화"
		m_fileTree.InsertItem(str, subItem3);
		str.Format(TEXT_LANG[80]+" : %f", pStep->fRecDeltaT); //"온도변화"
		m_fileTree.InsertItem(str, subItem3);
	
		subItem3 = m_fileTree.InsertItem(TEXT_LANG[81], subItem2); //"등급조건"
		for(b = 0; b<EP_MAX_GRADE_STEP; b++)
		{
			str.Format(TEXT_LANG[82]+" %d : No %d, Item %d, %f <Value <%f =>%c", b+1, //"등급" 
						pStep->grade[b].index+1, pStep->grade[b].gradeItem, pStep->grade[b].fMin, pStep->grade[b].fMax, pStep->grade[b].gradeCode);
			m_fileTree.InsertItem(str, subItem3);
		}

		//
		subItem2 = m_fileTree.InsertItem(TEXT_LANG[100], subItem1); //"집계"
		for( b =0; b<EP_RESULT_ITEM_NO; b++)
		{
			str.Format(TEXT_LANG[101]+" %d", b+1); //Item
			subItem3 = m_fileTree.InsertItem(str, subItem2);
			str.Format(TEXT_LANG[102]+" : %f",pResult->averageData[b].GetAvg()); //Average
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[103]+" : %f",pResult->averageData[b].GetMinValue()); //Min
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[104]+" : %d",pResult->averageData[b].GetMinIndex()+1); //Min Ch
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[105]+" : %f",pResult->averageData[b].GetMaxValue()); //Max
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[106]+" : %d",pResult->averageData[b].GetMaxIndex()+1); //Max Ch
			m_fileTree.InsertItem(str, subItem3);	
			str.Format(TEXT_LANG[107]+" : %f",pResult->averageData[b].GetSTDD()); //표준편차
			m_fileTree.InsertItem(str, subItem3);	
			str.Format(TEXT_LANG[108]+" : %d",pResult->averageData[b].GetDataCount()); //정상수
			m_fileTree.InsertItem(str, subItem3);	
		}
		
		//
		subItem2 = m_fileTree.InsertItem("Channel", subItem1);
		STR_SAVE_CH_DATA *pChData;
		for(b =0; b<pResult->aChData.GetSize(); b++)
		{
			str.Format("Ch %d", b+1);
			subItem3 = m_fileTree.InsertItem(str, subItem2);

			pChData = (STR_SAVE_CH_DATA *)pResult->aChData[b]; 
			str.Format(TEXT_LANG[109]+" : %d", pChData->wChIndex+1); //Ch Number
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[86]+" : %d", pChData->state); //상태
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[110]+" : %c", pChData->grade); //등급코드
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[111]+" : %d", pChData->channelCode); //불량코드
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[50]+": %d", pChData->nStepNo); //Step 번호
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[112]+" : %f", pChData->fStepTime); //스텝시간
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[113]+" : %f", pChData->fTotalTime); //총시간
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[114]+" : %f", pChData->fVoltage); //전압
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[115]+" : %f", pChData->fCurrent); //전류
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[122]+" : %f", pChData->fWatt); //Watt
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[123]+" : %f", pChData->fWattHour); //WattHour
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[116]+" : %f", pChData->fCapacity); //용량
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[117]+" : %f", pChData->fImpedance); //저항
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[118]+" : %f", pChData->fAuxData[0]); //온도
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[119]+" : %d", pChData->wModuleChIndex+1); //모듈채널번호
			m_fileTree.InsertItem(str, subItem3);
			str.Format(TEXT_LANG[1]+"1 : %d", pChData->wReserved); //부가정보
			m_fileTree.InsertItem(str, subItem3);
			
			//str.Format("Ch %d", pChData->fReserved[0]);
			//subItem4 = m_fileTree.InsertItem(str, subItem3);

		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CShowDetailResultDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if(::IsWindow(this->GetSafeHwnd()))
	{
		RECT rect;
		::GetClientRect(m_hWnd, &rect);

		CRect rectGrid;
		if(m_fileTree.GetSafeHwnd())
		{
			m_fileTree.GetWindowRect(rectGrid);	//Step Grid Window Position
			ScreenToClient(&rectGrid);
			m_fileTree.MoveWindow(5, rectGrid.top, rect.right-10, rect.bottom-rectGrid.top-5, FALSE);
			Invalidate();
		}
	}	
}
