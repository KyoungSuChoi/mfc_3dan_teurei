// AxisSettingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "AxisSettingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAxisSettingDlg dialog


CAxisSettingDlg::CAxisSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAxisSettingDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CAxisSettingDlg"));
	//{{AFX_DATA_INIT(CAxisSettingDlg)
	m_bYVoltage = TRUE;
	m_bYCurrent = TRUE;
	m_bYCapacity = TRUE;
	m_bYImpedance = FALSE;
	m_bYTemperature = FALSE;
	m_bYPressure = FALSE;
	m_bXData = FALSE;
	m_bXItem = FALSE;

	m_fYVolMin = 0.0f;
	m_fYCurMin = -2000.0f;
	m_fYCapMin = 0.0f;
	m_fYImpMin = 0.0f;
	m_fYTempMin = 0.0f;
	m_fYPressMin = 0.0f;
	m_fXDataMin = 0.0f;

	m_nXMaxReadPoint = MAX_XAXIS_POINT;

	m_fYVolMax = 5.0f;
	m_fYCurMax = 2000.0f;
	m_fYCapMax = 2000.0f;
	m_fYImpMax = 500.0f;
	m_fYTempMax = 200.0f;
	m_fYPressMax = 1000.0f;
	m_fXDataMax = 20000.0f;

	m_nXItem = 0;

	//}}AFX_DATA_INIT
	m_bChangedFlag = FALSE;
}

CAxisSettingDlg::~CAxisSettingDlg()
{

	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CAxisSettingDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}

void CAxisSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAxisSettingDlg)
	DDX_Check(pDX, IDC_YVOLTAGE, m_bYVoltage);
	DDX_Check(pDX, IDC_YCURRENT, m_bYCurrent);
	DDX_Check(pDX, IDC_YCAPACITY, m_bYCapacity);
	DDX_Check(pDX, IDC_YIMPEDANCE, m_bYImpedance);
	DDX_Check(pDX, IDC_YTEMPERATURE, m_bYTemperature);
	DDX_Check(pDX, IDC_YPRESSURE, m_bYPressure);
	DDX_Text(pDX, IDC_YVOL_MIN, m_fYVolMin);
	DDX_Text(pDX, IDC_YCUR_MIN, m_fYCurMin);
	DDX_Text(pDX, IDC_YCAP_MIN, m_fYCapMin);
	DDX_Text(pDX, IDC_YIMP_MIN, m_fYImpMin);
	DDX_Text(pDX, IDC_YTEMP_MIN, m_fYTempMin);
	DDX_Text(pDX, IDC_YPRESS_MIN, m_fYPressMin);
	DDX_Text(pDX, IDC_YVOL_MAX, m_fYVolMax);
	DDX_Text(pDX, IDC_YCUR_MAX, m_fYCurMax);
	DDX_Text(pDX, IDC_YCAP_MAX, m_fYCapMax);
	DDX_Text(pDX, IDC_YIMP_MAX, m_fYImpMax);
	DDX_Text(pDX, IDC_YTEMP_MAX, m_fYTempMax);
	DDX_Text(pDX, IDC_YPRESS_MAX, m_fYPressMax);
	DDX_Control(pDX, IDC_YVOL_UNIT, m_ctrlYVolUnit);
	DDX_Control(pDX, IDC_YCUR_UNIT, m_ctrlYCurUnit);
	DDX_Control(pDX, IDC_YCAP_UNIT, m_ctrlYCapUnit);
	DDX_Control(pDX, IDC_YIMP_UNIT,m_ctrlYImpUnit);
	DDX_Control(pDX, IDC_YTEMP_UNIT, m_ctrlYTempUnit);
	DDX_Control(pDX, IDC_YPRESS_UNIT, m_ctrlYPressUnit);
	DDX_Control(pDX, IDC_XDATA_UNIT, m_ctrlXDataUnit);
	DDX_Text(pDX, IDC_MAX_READE_POINT, m_nXMaxReadPoint);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAxisSettingDlg, CDialog)
	//{{AFX_MSG_MAP(CAxisSettingDlg)
	ON_CBN_SELCHANGE(IDC_YVOL_UNIT, OnSelchangeYvolUnit)
	ON_CBN_SELCHANGE(IDC_YCUR_UNIT, OnSelchangeYcurUnit)
	ON_CBN_SELCHANGE(IDC_YIMP_UNIT, OnSelchangeYimpUnit)
	ON_CBN_SELCHANGE(IDC_YPRESS_UNIT, OnSelchangeYpressUnit)
	ON_CBN_SELCHANGE(IDC_YTEMP_UNIT, OnSelchangeYtempUnit)
	ON_CBN_SELCHANGE(IDC_YCAP_UNIT, OnSelchangeYcapUnit)
	ON_CBN_SELCHANGE(IDC_XDATA_UNIT, OnSelchangeXdataUnit)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_EN_CHANGE(IDC_MAX_READE_POINT, OnChangeMaxReadePoint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAxisSettingDlg message handlers

BOOL CAxisSettingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	if(!m_YAxisSet[0].nYItemUnit)
	{
		GetDlgItem(IDC_YVOL_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YVOL_MAX)->EnableWindow(FALSE);
	}
	if(!m_YAxisSet[1].nYItemUnit)
	{
		GetDlgItem(IDC_YCUR_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YCUR_MAX)->EnableWindow(FALSE);
	}
	if(!m_YAxisSet[2].nYItemUnit)
	{
		GetDlgItem(IDC_YCAP_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YCAP_MAX)->EnableWindow(FALSE);
	}
	if(!m_YAxisSet[3].nYItemUnit)
	{
		GetDlgItem(IDC_YIMP_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YIMP_MAX)->EnableWindow(FALSE);
	}
	if(!m_YAxisSet[4].nYItemUnit)
	{
		GetDlgItem(IDC_YTEMP_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YTEMP_MAX)->EnableWindow(FALSE);
	}
	if(!m_YAxisSet[5].nYItemUnit)
	{
		GetDlgItem(IDC_YPRESS_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YPRESS_MAX)->EnableWindow(FALSE);
	}
	
	m_bYVoltage =  m_YAxisSet[0].bItemSel;
	m_fYVolMin = m_YAxisSet[0].fAxisMin;
	m_fYVolMax = m_YAxisSet[0].fAxisMax;
	m_ctrlYVolUnit.SetCurSel(m_YAxisSet[0].nYItemUnit);

	m_bYCurrent = m_YAxisSet[1].bItemSel;
	m_fYCurMin = m_YAxisSet[1].fAxisMin;
	m_fYCurMax = m_YAxisSet[1].fAxisMax;
	m_ctrlYCurUnit.SetCurSel(m_YAxisSet[1].nYItemUnit);

	m_bYCapacity = m_YAxisSet[2].bItemSel;
	m_fYCapMin = m_YAxisSet[2].fAxisMin;
	m_fYCapMax = m_YAxisSet[2].fAxisMax;
	m_ctrlYCapUnit.SetCurSel(m_YAxisSet[2].nYItemUnit);

	m_bYImpedance = m_YAxisSet[3].bItemSel;
	m_fYImpMin = m_YAxisSet[3].fAxisMin;
	m_fYImpMax = m_YAxisSet[3].fAxisMax;
	m_ctrlYImpUnit.SetCurSel(m_YAxisSet[3].nYItemUnit);

	m_bYTemperature  = m_YAxisSet[4].bItemSel ;
	m_fYTempMin = m_YAxisSet[4].fAxisMin;
	m_fYTempMax  = m_YAxisSet[4].fAxisMax;
	m_ctrlYTempUnit.SetCurSel(m_YAxisSet[4].nYItemUnit);
	
	m_bYPressure = m_YAxisSet[5].bItemSel;
	m_fYPressMin = m_YAxisSet[5].fAxisMin;
	m_fYPressMax = m_YAxisSet[5].fAxisMax;
	m_ctrlYPressUnit.SetCurSel(m_YAxisSet[5].nYItemUnit);

	m_ctrlXDataUnit.SetCurSel(m_nXItem);
	
	UpdateData(FALSE);
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAxisSettingDlg::OnSelchangeYvolUnit() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlYVolUnit.GetCurSel() > 0)
	{
		GetDlgItem(IDC_YVOL_MIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_YVOL_MAX)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_YVOL_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YVOL_MAX)->EnableWindow(FALSE);
	}
}

void CAxisSettingDlg::OnSelchangeYcurUnit() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlYCurUnit.GetCurSel() > 0)
	{
		GetDlgItem(IDC_YCUR_MIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_YCUR_MAX)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_YCUR_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YCUR_MAX)->EnableWindow(FALSE);
	}
}

void CAxisSettingDlg::OnSelchangeYimpUnit() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlYImpUnit.GetCurSel() > 0)
	{
		GetDlgItem(IDC_YIMP_MIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_YIMP_MAX)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_YIMP_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YIMP_MAX)->EnableWindow(FALSE);
	}
}

void CAxisSettingDlg::OnSelchangeYcapUnit() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlYCapUnit.GetCurSel() > 0)
	{
		GetDlgItem(IDC_YCAP_MIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_YCAP_MAX)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_YCAP_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YCAP_MAX)->EnableWindow(FALSE);
	}
}

void CAxisSettingDlg::OnSelchangeYpressUnit() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlYPressUnit.GetCurSel() > 0)
	{
		GetDlgItem(IDC_YPRESS_MIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_YPRESS_MAX)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_YPRESS_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YPRESS_MAX)->EnableWindow(FALSE);
	}
}

void CAxisSettingDlg::OnSelchangeYtempUnit() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlYTempUnit.GetCurSel() > 0)
	{
		GetDlgItem(IDC_YTEMP_MIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_YTEMP_MAX)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_YTEMP_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_YTEMP_MAX)->EnableWindow(FALSE);
	}
}

void CAxisSettingDlg::OnSelchangeXdataUnit() 
{
	// TODO: Add your control notification handler code here
	m_bChangedFlag = TRUE;
}

void CAxisSettingDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString strTemp;

	if(GetSelItemCount() > MAX_SEL_AXIS)	
	{
		strTemp.Format(TEXT_LANG[0], MAX_SEL_AXIS); //"최대 %d개의 Y축을 선택 할 수 있습니다."
		AfxMessageBox(strTemp);
		return;
	}

	if(GetSelItemCount() <=0 )	
	{
		strTemp.Format(TEXT_LANG[1]); //"최소 1개의 Y축을 선택 하여야 합니다."
		AfxMessageBox(strTemp);
		return;
	}

	if(m_fYVolMin >= m_fYVolMax)
	{
		AfxMessageBox(TEXT_LANG[2]); //"전압 범위가 잘못 설정 되었습니다."
		GetDlgItem(IDC_YVOL_MAX)->SetFocus();
		return;
	}
	if(m_fYCurMin >= m_fYCurMax)
	{
		AfxMessageBox(TEXT_LANG[3]); //"전류 범위가 잘못 설정 되었습니다."
		GetDlgItem(IDC_YCUR_MAX)->SetFocus();
		return;
	}
	if(m_fYCapMin >= m_fYCapMax)
	{
		AfxMessageBox(TEXT_LANG[4]); //"용량 범위가 잘못 설정 되었습니다."
		GetDlgItem(IDC_YCAP_MAX)->SetFocus();
		return;
	}
	if(m_fYImpMin >= m_fYImpMax)
	{
		AfxMessageBox(TEXT_LANG[5]); //"DCIR 범위가 잘못 설정 되었습니다."
		GetDlgItem(IDC_YIMP_MAX)->SetFocus();
		return;
	}
	if(m_fYTempMin >= m_fYTempMax)
	{
		AfxMessageBox(TEXT_LANG[6]); //"온도 범위가 잘못 설정 되었습니다."
		GetDlgItem(IDC_YTEMP_MAX)->SetFocus();
		return;
	}
	if(m_fYPressMin >= m_fYPressMax)
	{
		AfxMessageBox(TEXT_LANG[7]); //"압력 범위가 잘못 설정 되었습니다."
		GetDlgItem(IDC_YPRESS_MAX)->SetFocus();
		return;
	}

	m_YAxisSet[0].bItemSel = m_bYVoltage;
	m_YAxisSet[0].fAxisMin = m_fYVolMin;
	m_YAxisSet[0].fAxisMax = m_fYVolMax;
	m_YAxisSet[0].nYItemUnit = m_ctrlYVolUnit.GetCurSel();

	m_YAxisSet[1].bItemSel = m_bYCurrent;
	m_YAxisSet[1].fAxisMin = m_fYCurMin;
	m_YAxisSet[1].fAxisMax = m_fYCurMax;
	m_YAxisSet[1].nYItemUnit = m_ctrlYCurUnit.GetCurSel();

	m_YAxisSet[2].bItemSel = m_bYCapacity;
	m_YAxisSet[2].fAxisMin = m_fYCapMin;
	m_YAxisSet[2].fAxisMax = m_fYCapMax;
	m_YAxisSet[2].nYItemUnit = m_ctrlYCapUnit.GetCurSel();

	m_YAxisSet[3].bItemSel = m_bYImpedance;
	m_YAxisSet[3].fAxisMin = m_fYImpMin;
	m_YAxisSet[3].fAxisMax = m_fYImpMax;
	m_YAxisSet[3].nYItemUnit = m_ctrlYImpUnit.GetCurSel();

	m_YAxisSet[4].bItemSel = m_bYTemperature;
	m_YAxisSet[4].fAxisMin = m_fYTempMin;
	m_YAxisSet[4].fAxisMax = m_fYTempMax;
	m_YAxisSet[4].nYItemUnit = m_ctrlYTempUnit.GetCurSel();
	
	m_YAxisSet[5].bItemSel = m_bYPressure;
	m_YAxisSet[5].fAxisMin = m_fYPressMin;
	m_YAxisSet[5].fAxisMax = m_fYPressMax;
	m_YAxisSet[5].nYItemUnit = m_ctrlYPressUnit.GetCurSel();

	m_nXItem = m_ctrlXDataUnit.GetCurSel();

	CDialog::OnOK();
}

void CAxisSettingDlg::OnChangeMaxReadePoint() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_CHANGE flag ORed into the lParam mask.
	
	// TODO: Add your control notification handler code here
	m_bChangedFlag = TRUE;
}

int CAxisSettingDlg::GetSelItemCount()
{
	int count = 0;
	UpdateData(TRUE);
	if(m_bYVoltage)	count++;
	if(m_bYCurrent)	count++;
	if(m_bYCapacity)	count++;
	if(m_bYImpedance)	count++;
	if(m_bYTemperature)	count++;
	if(m_bYPressure)	count++;
	return count;
}
