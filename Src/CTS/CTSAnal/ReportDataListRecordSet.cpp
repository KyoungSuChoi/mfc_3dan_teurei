// ReportDataListRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ReportDataListRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReportDataListRecordSet

IMPLEMENT_DYNAMIC(CReportDataListRecordSet, CRecordset)

CReportDataListRecordSet::CReportDataListRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CReportDataListRecordSet)
	m_ProgressID = 0;
	m_Name = _T("");
	m_DataType = 0;
	m_No = 0;
	m_Display = FALSE;
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CReportDataListRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=CTSProduct");
}

CString CReportDataListRecordSet::GetDefaultSQL()
{
	return _T("[ReportDataList]");
}

void CReportDataListRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CReportDataListRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Int(pFX, _T("[ProgressID]"), m_ProgressID);
	RFX_Text(pFX, _T("[Name]"), m_Name);
	RFX_Long(pFX, _T("[DataType]"), m_DataType);
	RFX_Long(pFX, _T("[No]"), m_No);
	RFX_Bool(pFX, _T("[Display]"), m_Display);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CReportDataListRecordSet diagnostics

#ifdef _DEBUG
void CReportDataListRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CReportDataListRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
