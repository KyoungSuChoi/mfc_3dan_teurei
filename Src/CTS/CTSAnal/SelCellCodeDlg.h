#if !defined(AFX_SELCELLCODEDLG_H__F5E5A6A4_AEB5_4822_85DB_4CB7CA13667C__INCLUDED_)
#define AFX_SELCELLCODEDLG_H__F5E5A6A4_AEB5_4822_85DB_4CB7CA13667C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelCellCodeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelCellCodeDlg dialog

class CSelCellCodeDlg : public CDialog
{
// Construction
public:
	STR_MSG_DATA m_selCode;
	CPtrArray *m_pChCodeMsg;
	CSelCellCodeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelCellCodeDlg)
	enum { IDD = IDD_USER_SEL_CODE_DIALOG };
	CComboBox	m_codeCombo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelCellCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelCellCodeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELCELLCODEDLG_H__F5E5A6A4_AEB5_4822_85DB_4CB7CA13667C__INCLUDED_)
