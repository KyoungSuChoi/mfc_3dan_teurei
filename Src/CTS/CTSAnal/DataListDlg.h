#if !defined(AFX_DATALISTDLG_H__5AAFA992_1C15_40BE_8C35_85A5DD89A136__INCLUDED_)
#define AFX_DATALISTDLG_H__5AAFA992_1C15_40BE_8C35_85A5DD89A136__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataListDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataListDlg dialog
#include "MyGridWnd.h"

class CDataListDlg : public CDialog
{
// Construction
public:
	virtual ~CDataListDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

	BOOL InitListGrid();
	CDataListDlg(CWnd* pParent = NULL);   // standard constructor
	FIELD		m_Field[FIELDNO];
// Dialog Data
	//{{AFX_DATA(CDataListDlg)
	enum { IDD = IDD_DATA_LIST_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CMyGridWnd m_TestGrid;

	// Generated message map functions
	//{{AFX_MSG(CDataListDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATALISTDLG_H__5AAFA992_1C15_40BE_8C35_85A5DD89A136__INCLUDED_)
