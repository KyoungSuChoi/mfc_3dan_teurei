// SearchView.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanal.h"
#include "SearchView.h"
#include "SetOnlineDataFolderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchView

IMPLEMENT_DYNCREATE(CSearchView, CFormView)

CSearchView::CSearchView()
	: CFormView(CSearchView::IDD)
{
	LanguageinitMonConfig(_T("CSearchView"));
	//{{AFX_DATA_INIT(CSearchView)
	m_strTrayNum = _T("");
	m_strUnitName = _T("");	
	m_strCurData = _T("");
	m_toTime = 0;
	m_fromTime = 0;
	m_bCheckPeriod = FALSE;
	m_strCurDataFolder = _T("");
	//}}AFX_DATA_INIT
}

CSearchView::~CSearchView()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CSearchView::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}



void CSearchView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchView)
	DDX_Text(pDX, IDC_TRAYNUM_FILTER, m_strTrayNum);
	DDX_Text(pDX, IDC_UNITNAME_FILTER, m_strUnitName);
	DDX_Control(pDX, IDC_TOTALFILE_NUM, m_strTotalFileNum);
	DDX_Text(pDX, IDC_CUR_DATA, m_strCurData);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER3, m_toTime);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_fromTime);
	DDX_Check(pDX, IDC_CHECK_TODAY, m_bCheckPeriod);
	DDX_Text(pDX, IDC_CURDATA_FOLDER, m_strCurDataFolder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSearchView, CFormView)
	//{{AFX_MSG_MAP(CSearchView)
	ON_BN_CLICKED(IDC_REFRESH, OnRefresh)
	ON_BN_CLICKED(IDC_CURDATA_SEARCH, OnCurdataSearch)
	ON_BN_CLICKED(IDC_BTN_TODAY, OnBtnToday)
	ON_BN_CLICKED(IDC_CURDATAFOLDER_SEARCH, OnCurdatafolderSearch)
	ON_UPDATE_COMMAND_UI(ID_ONLINEDATA_PATH, OnUpdateOnlinedataPath)
	ON_COMMAND(ID_ONLINEDATA_PATH, OnOnlinedataPath)
	//}}AFX_MSG_MAP		
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnDbClickedRowCol)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchView diagnostics

#ifdef _DEBUG
void CSearchView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSearchView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSAnalDoc* CSearchView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSAnalDoc)));
	return (CCTSAnalDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSearchView message handlers

void CSearchView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	m_pDoc = GetDocument();
	m_strTotalFileNum.SetTextColor(RGB(255, 0, 0));

	m_strCurDataFolder = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Search_DataForder", "");

	m_fromTime = CTime::GetCurrentTime();
	m_toTime = CTime::GetCurrentTime();
	
	InitFileListGrid();	
	UpdateData(FALSE);
}


void CSearchView::InitFileListGrid()
{
	m_wndFileListGrid.SubclassDlgItem(IDC_RESULT_FILE_LIST, this);
	//m_wndBoardListGrid.m_bRowSelection = TRUE;
	m_wndFileListGrid.m_bSameColSize  = FALSE;
	m_wndFileListGrid.m_bSameRowSize  = FALSE;
	m_wndFileListGrid.m_bCustomWidth  = TRUE;
	
	CRect rect;
	m_wndFileListGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);	
	float fWidth = float(rect.Width()-50)/100.0f;
	m_wndFileListGrid.m_nWidth[1]	= 0;
	m_wndFileListGrid.m_nWidth[2]	= fWidth* 15.0f;
	m_wndFileListGrid.m_nWidth[3]	= fWidth* 10.0f;
	m_wndFileListGrid.m_nWidth[4]	= fWidth* 15.0f;
	m_wndFileListGrid.m_nWidth[5]	= fWidth* 15.0f;
	m_wndFileListGrid.m_nWidth[6]	= fWidth* 10.0f;
	m_wndFileListGrid.m_nWidth[7]	= fWidth* 10.0f;
	m_wndFileListGrid.m_nWidth[8]	= fWidth* 10.0f;
	m_wndFileListGrid.m_nWidth[9]	= 0;
	
	m_wndFileListGrid.Initialize();	
	m_wndFileListGrid.SetColCount(9);
	m_wndFileListGrid.SetDefaultRowHeight(22);
	m_wndFileListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndFileListGrid.m_BackColor	= RGB(255,255,255);
	m_wndFileListGrid.SetColWidth(0, 0, 50);	
	m_wndFileListGrid.m_bCustomColor 	= FALSE;
	
	// 공정날짜, 설비위치, 트레이정보, 작업이름, 양품, 불량....
	m_wndFileListGrid.SetValueRange(CGXRange(0,2),  TEXT_LANG[0]);//"공정날짜"
	m_wndFileListGrid.SetValueRange(CGXRange(0,3),  TEXT_LANG[1]);//"설비위치"
	m_wndFileListGrid.SetValueRange(CGXRange(0,4),  TEXT_LANG[2]);//"트레이정보"
	m_wndFileListGrid.SetValueRange(CGXRange(0,5),  TEXT_LANG[3]);//"작업이름"
	m_wndFileListGrid.SetValueRange(CGXRange(0,6),  TEXT_LANG[4]);//"투입수량"
	m_wndFileListGrid.SetValueRange(CGXRange(0,7),  TEXT_LANG[5]);//"양품수량"
	m_wndFileListGrid.SetValueRange(CGXRange(0,8),  TEXT_LANG[6]);//"불량수량"
	m_wndFileListGrid.SetValueRange(CGXRange(0,9),  TEXT_LANG[7]);//"에러코드"	
	
	m_wndFileListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);	
	m_wndFileListGrid.EnableCellTips();
	m_wndFileListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE));
	m_wndFileListGrid.SetScrollBarMode(1,1);
	
	m_wndFileListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndFileListGrid.Redraw();
}

void CSearchView::OnRefresh() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if( m_strTrayNum.IsEmpty() && m_strUnitName.IsEmpty() && m_bCheckPeriod==FALSE )
	{
		if(MessageBox(TEXT_LANG[8], TEXT_LANG[9], MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) !=IDYES) //"검색 조건이 설정되어 있지 않습니다. 모든 Data를 검색하시겠습니까?\nData량에 따라 지연될 수 있습니다." //"검색에러"
		{
			return;
		}
	}
	SearchFile();
	//  [6/3/2009 kky ]
	// for 검색된 파일 갯수를 표시
	CString strTemp;
	strTemp.Format("%d",m_wndFileListGrid.GetRowCount());
	m_strTotalFileNum.SetText(strTemp);
}

BOOL CSearchView::SearchFile()
{
	CWaitCursor wait;
	int nRow = m_wndFileListGrid.GetRowCount();
	if(nRow >0)
	{
		m_wndFileListGrid.RemoveRows(1, nRow);
	}	
	m_strSelFileName.Empty();

	CString strTemp, strTemp1, strFolder;
	CFileFind FileFinder, subFileFind;

	if( m_strCurDataFolder.IsEmpty() )
	{
		AfxMessageBox(TEXT_LANG[10]); //"Data 저장 경로가 설정되지 않았습니다!"
		return FALSE;
	}

	FindResultFile(m_strCurDataFolder);
	
	UpdateData(FALSE);
	return TRUE;
}

//  [6/3/2009 kky ]
// for 파일 경로를 통해서 파일을 검색한다.
BOOL CSearchView::FindResultFile(CString strFolder, BOOL bFindSubFolder)
{
	//recursive function
	UpdateData(TRUE);
	CFileFind FileFinder;	
	BOOL bFind = FileFinder.FindFile(strFolder+"\\*");
	while(bFind)
	{
		bFind = FileFinder.FindNextFile();
		if(FileFinder.IsDots())
		{
			continue;
		}
		else if(FileFinder.IsDirectory())
		{
			if(bFindSubFolder)
			{
				if( m_bCheckPeriod )						// 날짜표시가 되어 있으면..
				{		
					//  [6/3/2009 kky ]
					// for 시간 비교
					CString FromTime;
					CString ToTime;
					FromTime = m_fromTime.Format("%Y%m%d");
					ToTime = m_toTime.Format("%Y%m%d");

				 	CString strNextFileName(FileFinder.GetFileName());				
					
					if( atoi(FromTime) <= atoi(strNextFileName) && atoi(strNextFileName)<= atoi(ToTime) )
					{
						CompareUnitandTray( FileFinder.GetFilePath() );						
					}					
				}
				else
				{
					CompareUnitandTray( FileFinder.GetFilePath() );
				}				
			}
		}
	}
	return TRUE;
}

//  [6/3/2009 kky ]
// for 설비위치와 트레이정보를 비교
BOOL CSearchView::CompareUnitandTray( CString strResult )
{
	CFileFind FileFinder;
	CString strNextFileName;	
	CString strData;
	CString strTemp;	
	BOOL bFind = FileFinder.FindFile(strResult+"\\*");
	while(bFind)
	{
		bFind = FileFinder.FindNextFile();		
		if(FileFinder.IsDots())
		{
			continue;
		}
		else if(FileFinder.IsDirectory())
		{			
			if( !m_strUnitName.IsEmpty() || !m_strTrayNum.IsEmpty() )
			{
				// Unit 만 비교
				if( !m_strUnitName.IsEmpty() && m_strTrayNum.IsEmpty())
				{
					strNextFileName = FileFinder.GetFileName();
					AfxExtractSubString(strData,strNextFileName, 0, '_');
					if( m_strUnitName == strData )
					{
						InsertFormFileList(FileFinder.GetFilePath());							
					}
				}
				// Tray 만 비교
				else if( m_strUnitName.IsEmpty() && !m_strTrayNum.IsEmpty())
				{
					strNextFileName = FileFinder.GetFileName();
					AfxExtractSubString(strData,strNextFileName, 1, '_');
					if( m_strTrayNum == strData )
					{
						InsertFormFileList(FileFinder.GetFilePath());							
					}
				}
				// Unit, Tray 비교
				else if( !m_strUnitName.IsEmpty() && !m_strTrayNum.IsEmpty())
				{
					strNextFileName = FileFinder.GetFileName();
					AfxExtractSubString(strData,strNextFileName,0,'_');
					if( m_strUnitName == strData )
					{
						AfxExtractSubString(strData, strNextFileName, 1,'_');
						if( m_strTrayNum == strData )
						{							
							InsertFormFileList(FileFinder.GetFilePath());							
						}						
					}					
				}				
			}
			else
			{
				InsertFormFileList(FileFinder.GetFilePath());
			}
		}
	}
	return TRUE;
}

void CSearchView::OnCurdataSearch() 
{
	// TODO: Add your control notification handler code here
	CString strFilter;
	strFilter.Format(TEXT_LANG[11], BF_SUMMARY_FILE_EXT, BF_SUMMARY_FILE_EXT); //"Summary 결과 파일(*.%s)|*.%s|"
	CFileDialog dlgFile(TRUE, BF_SUMMARY_FILE_EXT, m_strSelFileName, OFN_HIDEREADONLY|OFN_FILEMUSTEXIST, strFilter);
	dlgFile.m_ofn.lpstrTitle = TEXT_LANG[12]; //"Cell test system 결과 파일"
	
	if(dlgFile.DoModal() == IDOK)
	{
		if( m_wndFileListGrid.GetRowCount() > 0 )
		{
			m_wndFileListGrid.RemoveRows(1, m_wndFileListGrid.GetRowCount());			
		}		
		m_strSelFileName = dlgFile.GetPathName();
		m_strCurData = m_strSelFileName;
		InsertFormFileList(m_strSelFileName);
		UpdateData(FALSE);		
	}	
}

//  [6/3/2009 kky ]
// for 파일 저장 내역
// 공정날짜, 설비위치, 트레이정보, 작업이름, 양품, 불량....
BOOL CSearchView::InsertFormFileList(CString strFile)
{
	CFileFind FileFinder;
	CString strNextFileName;
	CString strReadData;
	CString strData;			// 
	CString strFileData;		// CStringArray 배열에 저장할 데이터
	CStdioFile sourceFile;
	int nSize;
	CFileException ex;

	BOOL bFind = FileFinder.FindFile(strFile+"\\*");
	while(bFind)
	{
		bFind = FileFinder.FindNextFile();		
		if(FileFinder.IsDots())
		{
			continue;
		}
		else
		{
			// strNextFileName = FileFinder.GetFileName();
			CString strFileName(FileFinder.GetFilePath());								
			// CString strExt( strFileName.Mid(strFileName.Find('.')+1) );
			CString strExt( strFileName.Mid(strFileName.ReverseFind('.')+1) );
			strExt.MakeLower();
			if(strExt == BF_SUMMARY_FILE_EXT)
			{
				// TRACE("%s\n", strFileName);
				if( !sourceFile.Open( strFileName, CFile::modeRead, &ex ))
				{
					return FALSE;
				}
				
				while(TRUE)
				{					
					sourceFile.ReadString(strReadData);
					nSize = strReadData.Find(',');
					if( nSize > 0 )
					{	
						ROWCOL nRow = m_wndFileListGrid.GetRowCount()+1;
						m_wndFileListGrid.InsertRows(nRow, 1);	
						m_wndFileListGrid.Redraw();
						AfxExtractSubString(strData, strReadData, 0,',');
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 1), strData);			
						AfxExtractSubString(strData, strReadData, 1,',');
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 2), strData);			
						AfxExtractSubString(strData, strReadData, 2,',');
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 3), strData);			
						AfxExtractSubString(strData, strReadData, 3,',');
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 4), strData);			
						AfxExtractSubString(strData, strReadData, 4,',');
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 5), strData);			
						AfxExtractSubString(strData, strReadData, 5,',');			
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 6), strData);			
						AfxExtractSubString(strData, strReadData, 6,',');			
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 7), strData);									
						AfxExtractSubString(strData, strReadData, 7,',');
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 8), strData);			
						AfxExtractSubString(strData, strReadData, 8,',');
						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 9), strData);			
					}
					else
					{
						// EndFile
						break;
					}
				}				
			}											
		}
	}
	return TRUE;
}

void CSearchView::OnBtnToday() 
{
	// TODO: Add your control notification handler code here
	m_fromTime = CTime::GetCurrentTime();
	m_toTime = CTime::GetCurrentTime();
	m_bCheckPeriod = TRUE;
	UpdateData(FALSE);
}

void CSearchView::OnCurdatafolderSearch() 
{
	// TODO: Add your control notification handler code here
	CFolderDialog dlg(m_strCurDataFolder);
	if( dlg.DoModal() == IDOK )
	{
		m_strCurDataFolder = dlg.GetPathName();		
		AfxGetApp()->WriteProfileString(FORMSET_REG_SECTION , "Search_DataForder", m_strCurDataFolder);
		UpdateData(FALSE);
	}	
}

// for 그리드에서 정보를 조합해서 Raw데이터가 있는 폴더 위치정보를 전송한다.
LRESULT CSearchView::OnDbClickedRowCol(WPARAM wParam, LPARAM lParam)
{	
	CString strTempDebug;
	CMyGridWnd *pGrid = (CMyGridWnd *)lParam;				//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);
	
	if( nRow < 0 || nCol < 0 )
	{
		return 0;
	}	
	if( m_strCurDataFolder.IsEmpty() )
	{
		AfxMessageBox(TEXT_LANG[13]);//"데이터 저장 폴더 정보가 없습니다!"
		return 0;
	}
	// Index 파일이 있는 경로 만들기.
	m_strFolderPath = __T("");
	m_strFolderPath += m_strCurDataFolder;
	m_strFolderPath += "\\";
	m_strFolderPath += m_wndFileListGrid.GetValueRowCol(nRow,1);
	m_strFolderPath += "\\";
	m_strFolderPath += m_wndFileListGrid.GetValueRowCol(nRow,3);
	m_strFolderPath += "_";
	m_strFolderPath += m_wndFileListGrid.GetValueRowCol(nRow,4);
	m_strFolderPath += "\\";
	m_strFolderPath += m_wndFileListGrid.GetValueRowCol(nRow,1);
	m_strFolderPath += "_";
	m_strFolderPath += m_wndFileListGrid.GetValueRowCol(nRow,3);
	m_strFolderPath += "_";
	m_strFolderPath += m_wndFileListGrid.GetValueRowCol(nRow,4);
	strTempDebug.Format(TEXT_LANG[14], m_strFolderPath); //"CTSAnal->CTSGraph 폴더 경로 : %s"
	TRACE(strTempDebug);				// 만들어진 폴더 경로 확인
	OnGraphView(5);						// CTSGraphAnal execute...

	return 1;
}

BOOL CSearchView::OnGraphView( int nType )
{	
	CString strTemp;	
	CString strCurPath;
	CString strProgramName;
	strCurPath = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION, "CTSAnalyzer", "");		// 프로그램 설치 경로를 가지고 온다
	strProgramName.Format("%s\\CTSGraphAnalyzer.exe", strCurPath);
	
	// 데이터 폴더 정보 확인
	if( m_strFolderPath.IsEmpty() )
	{
		AfxMessageBox(TEXT_LANG[15]);//"데이터 폴더 정보가 잘못되었습니다!"
		return FALSE;
	}

	CWnd *pWnd = FindWindow(NULL, "CTSGraphAnalyzer");
	if( pWnd == 0 )
	{
		if(GetDocument()->ExecuteProgram( strProgramName, "", "", "CTSGraphAnalyzer", FALSE, TRUE))	
		{	
			// Summary 데이터가 저장된 포더 경로를 Message로 전송한다.			
			CWnd *pWnd = FindWindow(NULL, "CTSGraphAnalyzer");
			if(pWnd)
			{					
				int nSize = m_strFolderPath.GetLength()+1;
				char *pData = new char[nSize];
				sprintf(pData, "%s", m_strFolderPath);
				pData[nSize-1] = '\0';
				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = nType;		//CTSMonPro Program 구별 Index
				CpStructData.cbData = nSize;
				CpStructData.lpData = pData;
				
				strTemp.Format("%d,%d\n", nType, nSize);
				TRACE(strTemp);				
				// TRACE("Send File %s\n", pData);
				::SendMessage(pWnd->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				delete [] pData;			
			}
			else
			{
				return FALSE;
			}
		}
	}
	else
	{
		if(pWnd)
		{
			int nSize = m_strFolderPath.GetLength()+1;
			char *pData = new char[nSize];
			sprintf(pData, "%s", m_strFolderPath);
			pData[nSize-1] = '\0';
			
			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = nType;		//CTSMonPro Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			strTemp.Format("%d,%d\n", nType, nSize);
			TRACE(strTemp);
			
			// TRACE("Send File %s\n", pData);
			::SendMessage(pWnd->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			delete [] pData;
		}
		else
		{
			return FALSE;
		}
	}		
	return TRUE;
}

//  [6/16/2009 kky ]
// for DataSearch 풀더 경로를 다수로 설정할때를 대비한 구현 전 코드
// OnUpdateOnlinedataPath(CCmdUI* pCmdUI) 
// OnOnlinedataPath()
// LoadOnlineDataTree()
void CSearchView::OnUpdateOnlinedataPath(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);	
}

void CSearchView::OnOnlinedataPath() 
{
	// TODO: Add your command handler code here
	SetOnlineDataFolderDlg dlg;
	if( dlg.DoModal() == IDOK )
	{
		LoadOnlineDataTree();
	}
}

void CSearchView::LoadOnlineDataTree()
{
	CString strTmp, str;
	for(int i = 0; i<10; i++)
	{
		strTmp.Format("OnlineData%d", i);
		str = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION , strTmp);		
	}
}
