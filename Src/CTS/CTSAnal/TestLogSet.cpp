// TestLogSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "TestLogSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestLogSet

IMPLEMENT_DYNAMIC(CTestLogSet, CRecordset)

CTestLogSet::CTestLogSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestLogSet)
	m_TestLogID = 0;
	m_TestSerialNo = _T("");
	m_TraySerialNo = 0;
	m_LotNo = _T("");
	m_CellNo = 0;
	m_TrayNo = _T("");
	m_ModelID = 0;
	m_ModelName = _T("");
	m_TestDone = FALSE;
	m_nFields = 10;

	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CTestLogSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=CTSProduct");
}

CString CTestLogSet::GetDefaultSQL()
{
	return _T("[TestLog]");
}

void CTestLogSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestLogSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[TestLogID]"), m_TestLogID);
	RFX_Text(pFX, _T("[TestSerialNo]"), m_TestSerialNo);
	RFX_Long(pFX, _T("[TraySerialNo]"), m_TraySerialNo);
	RFX_Text(pFX, _T("[LotNo]"), m_LotNo);
	RFX_Long(pFX, _T("[CellNo]"), m_CellNo);
	RFX_Text(pFX, _T("[TrayNo]"), m_TrayNo);
	RFX_Date(pFX, _T("[DateTime]"), m_DateTime);
	RFX_Long(pFX, _T("[ModelID]"), m_ModelID);
	RFX_Text(pFX, _T("[ModelName]"), m_ModelName);
	RFX_Bool(pFX, _T("[TestDone]"), m_TestDone);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestLogSet diagnostics

#ifdef _DEBUG
void CTestLogSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestLogSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
