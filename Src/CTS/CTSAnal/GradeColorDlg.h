#if !defined(AFX_GRADECOLORDLG_H__7E47F255_AA0E_458B_808B_23DB5E6F04FF__INCLUDED_)
#define AFX_GRADECOLORDLG_H__7E47F255_AA0E_458B_808B_23DB5E6F04FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GradeColorDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGradeColorDlg dialog

class CGradeColorDlg : public CDialog
{
// Construction
public:
	STR_GRADE_COLOR_CONFIG m_GradeColor;
	CGradeColorDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGradeColorDlg)
	enum { IDD = IDD_GRADE_COLOR_DLG };
	CListBoxColorPickerST	m_wndGradeColor;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradeColorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGradeColorDlg)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	afx_msg void OnButton3();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADECOLORDLG_H__7E47F255_AA0E_458B_808B_23DB5E6F04FF__INCLUDED_)
