#if !defined(AFX_MODULERECORDSET_H__1EC40DD7_5F22_4FF1_8AF1_5073987A0602__INCLUDED_)
#define AFX_MODULERECORDSET_H__1EC40DD7_5F22_4FF1_8AF1_5073987A0602__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModuleRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModuleRecordSet recordset

class CModuleRecordSet : public CRecordset
{
public:
	CModuleRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CModuleRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CModuleRecordSet, CRecordset)
	long	m_ProcedureID;
	long	m_TestLogID;
	long	m_ModuleID;
	long	m_BoardIndex;
	CString	m_TrayNo;
	CString	m_UserID;
	long	m_GroupIndex;
	CTime	m_DateTime;
//	CString	m_TraySerial;
	CString	m_TestSerialNo;
	CString	m_State;
	CString	m_LotNo;
	CString	m_SystemIP;
	CString	m_TestName;
	long	m_TestID;
	CString	m_TestResultFileName;
	long	m_ProcedureType;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModuleRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODULERECORDSET_H__1EC40DD7_5F22_4FF1_8AF1_5073987A0602__INCLUDED_)
