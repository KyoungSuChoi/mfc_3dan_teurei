#include "stdafx.h"
#include "rw_gfa.h"
#include "math.h"

#define PI 3.14159265358979323846

CString strChar_(WORD DecimalZahl)
{
	CString string;
	string.Format("%c", DecimalZahl);
	return string;
}

//CString MID(CString str, int nFirst, int nCount) const
//{
//	return str.Mid(nFirst - 1, nCount);
//}

//CString MID( int nFirst, int nCount ) const;
//throw( CMemoryException );

/*
Public Const Pi As Double = 3.14159265358979

Static Function UPPER(str As String)
  UPPER = Format(str, ">")
End Function

Static Function LOWER(str As String)
  LOWER = Format(str, "<")
End Function

Static Function arcsin(x) As Double
'  Dim Pi As Double
'  Pi = 3.14159265358979
  arcsin = (Atn(x / Sqr(1 - x * x))) * 180 / Pi
End Function

Static Function arcsin_rad(x) As Double
  arcsin_rad = (Atn(x / Sqr(1 - x * x)))
End Function

Static Function sinh(x) As Double
  sinh = 0.5 * (Exp(x) - Exp(-x))
End Function

Static Function cosh(x) As Double
  cosh = 0.5 * (Exp(x) + Exp(-x))
End Function

Static Function tanh(x) As Double
  tanh = sinh(x) / cosh(x)
End Function
*/
double arsinh(double x) 
{
  return log(x + sqrt(x * x + 1.0));
}

double arcosh(double x) 
{
  return log(x + sqrt(x * x - 1.0));
}

double artanh(double x)
{
  return 0.5 * log((1 + x) / (1.0 - x));
}

/*
Static Function Log10(x) As Double
  Log10 = Log(x) / Log(10#)
End Function

Function arccos(x) As Double
  If x = 1 Then
    arccos = 0
  ElseIf x = -1 Then
    arccos = 180
  Else
    arccos = (Pi / 2 - Atn(x / Sqr(1 - x * x))) * 180 / Pi
  End If
End Function

Function arccos_rad(x) As Double
  If x = 1 Then
    arccos_rad = 0
  ElseIf x = -1 Then
    arccos_rad = Pi / 2
  Else
    arccos_rad = (Pi / 2 - Atn(x / Sqr(1 - x * x)))
  End If
End Function
*/

double DEG(double x)
{
  return x * 180.0 / PI; // PI;
}

double RAD(double x) 
{
  return x * PI / 180.0;	//PI
}

