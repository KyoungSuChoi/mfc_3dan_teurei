// GraphWnd.cpp : implementation file
//

#include "stdafx.h"
//#include "PowerBTS.h"
#include "GraphWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGraphWnd

CGraphWnd::CGraphWnd()
{
	m_hWndPE = NULL;
	m_YColor[0] = RGB(255,  0,  0);
	m_YColor[1] = RGB(255,255,  0);
	m_YColor[2] = RGB(  0,255,  0);
	m_YColor[3] = RGB(  0,255,255);
	m_YColor[4] = RGB(255,192,255);
	m_YColor[5] = RGB(255,255,255);
	m_YColor[6] = RGB(192,192,255);
	m_YColor[7] = RGB(  0,  0,255);

	m_SetsetNum = 6;
	m_TotalPoint = 200;
	m_ScreenPoint = 50;
	m_Delay = 1000;
	m_MainTitle = "공정";
	m_bShowArray[0] = TRUE;
	m_bShowArray[1] = TRUE;
	m_bShowArray[2] = TRUE;
	m_bShowArray[3] = TRUE;
	m_bShowArray[4] = TRUE;
	m_bShowArray[5] = TRUE;
	m_bShowArray[6] = TRUE;
	m_bShowArray[7] = TRUE;

	m_TimerID = 0;
	m_pData[0] = NULL;
	m_pData[1] = NULL;
	m_pData[2] = NULL;
	m_pData[3] = NULL;
	m_pData[4] = NULL;
	m_pData[5] = NULL;

	for(int i=0; i<6; i++)
	{
		m_Min[i] =0; 
		m_Max[i] = 10;
	}
	
		m_TimerPtr = NULL;

	m_XName = "";
}

CGraphWnd::~CGraphWnd()
{
}


BEGIN_MESSAGE_MAP(CGraphWnd, CStatic)
	//{{AFX_MSG_MAP(CGraphWnd)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CGraphWnd message handlers

void CGraphWnd::PreSubclassWindow() 
{
	CStatic::PreSubclassWindow();

	CRect trect, rect;

	GetClientRect(trect);
	rect.left = 1;
	rect.right = trect.Width() - 2;
	rect.top = 1;
	rect.bottom = trect.Height() - 2;

	m_hWndPE = PEcreate(PECONTROL_GRAPH, WS_BORDER, &rect, m_hWnd, 10000);
	if(!m_hWndPE)
	{
		AfxMessageBox("Creation Error");
		return;
	}

	InitGraph();

//***********************
	int	nArray[8];
	int num=0;
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
//***********************************************

	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
}

void CGraphWnd::InitGraph()
{
	SetN(PEP_bPREPAREIMAGES, 1);
	SetN(PEP_bDATASHADOWS, FALSE);
	SetN(PEP_nDATAPRECISION, 3);
	SetN(PEP_bALLOWBESTFITLINE, 1);
	SetN(PEP_bALLOWHISTOGRAM, FALSE);
	SetN(PEP_nGRIDLINECONTROL, PEGLC_NONE);

	SetN(PEP_bMOUSECURSORCONTROL, TRUE);
	SetN(PEP_nCURSORMODE, 3);
	SetN(PEP_nCURSORPROMPTSTYLE, 3);
//	SetN(PEP_bCURSORPROMPTTRACKING, TRUE);

	SetN(PEP_bALLOWUSERINTERFACE, FALSE);
	SetN(PEP_bALLOWPOPUP, FALSE);
	SetN(PEP_bALLOWCUSTOMIZATION, TRUE);

	SetN(PEP_bALLOWDATAHOTSPOTS, TRUE);
	SetN(PEP_bALLOWSUBSETHOTSPOTS, FALSE);
	SetN(PEP_bALLOWPOINTHOTSPOTS, TRUE);
	SetN(PEP_bALLOWTABLEHOTSPOTS, TRUE);

	SetN(PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);
	SetN(PEP_bAPPENDTOEND, 1);
	SetSZ(PEP_szMAINTITLEFONT, "Arial");

	SetN(PEP_nSUBSETS, m_SetsetNum);
	SetN(PEP_nPOINTS, m_TotalPoint);
	SetN(PEP_nPOINTSTOGRAPH, m_ScreenPoint);
	SetN(PEP_nPOINTSTOGRAPHINIT, PEPTGI_FIRSTPOINTS);
	SetN(PEP_bNORANDOMPOINTSTOGRAPH, TRUE);
	SetN(PEP_nFORCEVERTICALPOINTS, TRUE);
	SetVCell(PEP_szaPOINTLABELS, m_TotalPoint-1, " ");
	SetSZ(PEP_szMANUALMAXPOINTLABEL, "##########");
	SetN(PEP_nTARGETPOINTSTOTABLE, m_ScreenPoint);
	SetN(PEP_nALTFREQTHRESHOLD, m_ScreenPoint);

	SetSZ(PEP_szMAINTITLE, (LPSTR) (LPCTSTR) m_MainTitle);
	SetSZ(PEP_szSUBTITLE, (LPSTR) (LPCTSTR)  m_SubTitle);
	SetSZ(PEP_szXAXISLABEL,(LPSTR)(LPCTSTR) m_XName);


	SetN(PEP_dwDESKCOLOR, RGB(192,192,192));
	SetN(PEP_dwGRAPHBACKCOLOR, RGB(0,0,0));
	SetN(PEP_dwGRAPHFORECOLOR, RGB(255,255,255));
	SetV(PEP_dwaSUBSETCOLORS, m_YColor, 8);

	double NullData = 0;
	SetV(PEP_fNULLDATAVALUE, &NullData, 0);

	int slt[8] = {0,0,0,0,0,0,0,0};
	SetV(PEP_naSUBSETLINETYPES, slt, 8);

	int mas[8] = {1,1,1,1,1,1,1,1};
	SetV(PEP_naMULTIAXESSUBSETS, mas, m_SetsetNum);

	int oma[6] = {0,0};
	oma[0] = m_SetsetNum;
	SetV(PEP_naOVERLAPMULTIAXES, oma, 1);

	for(int i=0;i<m_SetsetNum;i++)
	{
		InitSubset(i,  m_Min[i], m_Max[i], m_SubsetName[i], m_SubsetYName[i]);
	}

	SetN(PEP_nWORKINGAXIS, 0);

}

void CGraphWnd::InitSubset(int subID, double min, double max, LPCTSTR title, LPCTSTR yname)
{
	SetN(PEP_nWORKINGAXIS, subID);

	SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
	SetV(PEP_fMANUALMINY, &min, 0);
	SetV(PEP_fMANUALMAXY, &max, 0);
	SetN(PEP_nPLOTTINGMETHOD, PEGPM_LINE);

	SetVCell(PEP_szaSUBSETLABELS, subID, (LPSTR) title);
	SetSZ(PEP_szYAXISLABEL, (LPSTR) yname);
	SetN(PEP_dwYAXISCOLOR, m_YColor[subID]);
}

void CGraphWnd::ShowSubset(int id, BOOL bShow)
{
	m_bShowArray[id] = bShow;
	int	nArray[8];
	int num=0;
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
	PEreinitialize(m_hWndPE);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CGraphWnd::SetSplit(BOOL bSplit)
{
	int oma[6];
	float map[6];
	if(bSplit)
	{
		float onesize = 1.0f / m_SetsetNum;
		for(int j=0;j<m_SetsetNum;j++)
		{
			oma[j] = 1;
			map[j] = onesize;
		}
		SetV(PEP_naOVERLAPMULTIAXES, oma, m_SetsetNum);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, m_SetsetNum);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_MEDIUM);
	}
	else
	{
		oma[0] = m_SetsetNum;
		map[0] = 1.0f;
		SetV(PEP_naOVERLAPMULTIAXES, oma, 1);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, 0);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_NONE);
	}
	PEreinitialize(m_hWndPE);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CGraphWnd::ClearGraph()
{
	char str[8];
	float data = -99999;
	double NullData = -99999;

	BeginWaitCursor();
	memset(str, 0, 8);
	SetV(PEP_fNULLDATAVALUE, &NullData, 0);
	for(int i=0;i<m_SetsetNum;i++)
	{
		for(int j=0;j<m_TotalPoint;j++)
		{
			PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, i, j, (void FAR*) str);
			PEvsetcellEx(m_hWndPE, PEP_faYDATA, i, j, &data);
		}
	}
	EndWaitCursor();

	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CGraphWnd::Start()
{
	m_Count = 0;
	m_OldTime = 99999999L;

	SetN(PEP_nHORZSCROLLPOS, 0);

	ClearGraph();

	for(int i=0;i<m_SetsetNum;i++)
	{
		InitSubset(i,  m_Min[i], m_Max[i], m_SubsetName[i], m_SubsetYName[i]);
	}

	time(&m_StartTime);

	if(m_TimerID)
		KillTimer(m_TimerID);
	m_TimerID = SetTimer(11, m_Delay, NULL);
}

void CGraphWnd::Stop()
{
	if(m_TimerID)
		KillTimer(m_TimerID);
	m_TimerID = 0;
}

void CGraphWnd::SetDataPtr(int id, float *data)
{
	m_pData[id] = data;
}

void CGraphWnd::OnTimer(UINT nIDEvent) 
{
	unsigned long newTime = *m_TimerPtr;
	if(newTime == m_OldTime)
	{
		CStatic::OnTimer(nIDEvent);
		return;
	}
	m_OldTime = newTime;

	float data[8];
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_pData[i])
			data[i] = *(m_pData[i]);
		else
			data[i] = 0;
	}

	CString str;
	str.Format("%7ld", newTime);
	if(m_Count>=m_TotalPoint)
	{
		if(m_Count==m_TotalPoint)
			SetN(PEP_nHORZSCROLLPOS, m_Count);
		m_Count++;
		SetV(PEP_szaAPPENDPOINTLABELDATA, (void FAR*) (const char*) str, 1);
		SetV(PEP_faAPPENDYDATA, data, 1);
	}
	else
	{
		for(int i=0;i<m_SetsetNum;i++)
		{
			PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, i, m_Count, (void FAR*) (const char*) str);
			PEvsetcellEx(m_hWndPE, PEP_faYDATA, i, m_Count, &data[i]);
		}
		if(m_Count>=m_ScreenPoint)
		{
			SetN(PEP_nHORZSCROLLPOS, m_Count-m_ScreenPoint-1);
		}
		else
		{
		}
		PEreinitialize(m_hWndPE);
		PEresetimage(m_hWndPE, 0, 0);
		::InvalidateRect(m_hWndPE, NULL, FALSE);

		m_Count++;
	}

	CStatic::OnTimer(nIDEvent);
}

void CGraphWnd::OnDestroy() 
{
	CStatic::OnDestroy();
	
	PEdestroy(m_hWndPE);
	if(m_TimerID)
		KillTimer(m_TimerID);
	
}

BOOL CGraphWnd::OnEraseBkgnd(CDC* pDC) 
{
	BOOL ret =  CStatic::OnEraseBkgnd(pDC);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
	return ret;
}

void CGraphWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

//*********** Functions for Graph ******************************
void CGraphWnd::SetN(UINT type, INT value)
{
	PEnset(m_hWndPE, type, value);
}

void CGraphWnd::SetV(UINT type, VOID FAR * lpData, UINT nItems)
{
	PEvset(m_hWndPE, type, lpData, nItems);
}

void CGraphWnd::SetVCell(UINT type, UINT nCell, VOID FAR * lpData)
{
	PEvsetcell(m_hWndPE, type, nCell, lpData);
}

void CGraphWnd::SetSZ(UINT type, CHAR FAR *str)
{
	PEszset(m_hWndPE, type, str);
}

void CGraphWnd::Print()
{
	PElaunchprintdialog(m_hWndPE, TRUE, NULL);
}

void CGraphWnd::SetData(int subset, int pos, LPCTSTR xData, double yData)
{
	float data = (float) yData;
	PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, subset, pos, (void FAR*) xData);
	PEvsetcellEx(m_hWndPE, PEP_faYDATA, subset, pos, &data);
}

void CGraphWnd::SetMaximize()
{
	PElaunchmaximize(m_hWndPE);
}

void CGraphWnd::SetHorizonLine()
{
	int		nTypes[2];
	int		nAxis[2];
	double	fYLoc[2];
	bool	bl[2] ;
	COLORREF	yColor[2];
	nTypes[0] = PELT_DOT ;	nTypes[1] = PELT_DOT ;
	nAxis[0] = 0 ;	nAxis[1] = 1 ;
	fYLoc[0] = 0.0F ;	fYLoc[1] = 0.0F ;
	yColor[0] = RGB(128,128,128);	yColor[1] = RGB(128,128,128);
	bl[0] = 1 ;		bl[1] = 1 ;
//	PEvsetcell(m_hWndPE, PEP_bSHOWHORZLINEANNOTATIONS, 0, bl);
//	PEvsetcellEx(m_hWndPE, PEP_bSHOWHORZLINEANNOTATIONS, 0, 0, bl);

//	SetN(PEP_naHORZLINEANNOTATIONAXIS, 0);
//	SetN(PEP_nWORKINGAXIS, 0);

/*	PEvsetcell(m_hWndPE, PEP_faHORZLINEANNOTATION, 0,  fYLoc);
	PEvsetcell(m_hWndPE, PEP_naHORZLINEANNOTATIONTYPE, 0, nTypes);
	PEvsetcell(m_hWndPE, PEP_dwaHORZLINEANNOTATIONCOLOR, 0, yColor);
	PEvsetcell(m_hWndPE, PEP_faHORZLINEANNOTATION, 1, fYLoc);
	PEvsetcell(m_hWndPE, PEP_naHORZLINEANNOTATIONTYPE, 1, nTypes);
	PEvsetcell(m_hWndPE, PEP_dwaHORZLINEANNOTATIONCOLOR, 1, yColor);*/


//	PEvsetcellEx(m_hWndPE, PEP_naHORZLINEANNOTATIONAXIS, 1, 0, nAxis);
//	PEvsetcellEx(m_hWndPE, PEP_faHORZLINEANNOTATION, 1, 0, fYLoc);
//	PEvsetcellEx(m_hWndPE, PEP_naHORZLINEANNOTATIONTYPE, 1, 0, nTypes);
//	PEvsetcellEx(m_hWndPE, PEP_dwaHORZLINEANNOTATIONCOLOR, 1, 0, yColor);

/*	PEvsetcellEx(m_hWndPE, PEP_faHORZLINEANNOTATION, 0, 0, fYLoc);
	PEvsetcellEx(m_hWndPE, PEP_naHORZLINEANNOTATIONTYPE, 0, 0, nTypes);
	PEvsetcellEx(m_hWndPE, PEP_dwaHORZLINEANNOTATIONCOLOR, 0, 0, yColor);
	PEvsetcellEx(m_hWndPE, PEP_faHORZLINEANNOTATION, 1, 0, fYLoc);
	PEvsetcellEx(m_hWndPE, PEP_naHORZLINEANNOTATIONTYPE, 1, 0, nTypes);
	PEvsetcellEx(m_hWndPE, PEP_dwaHORZLINEANNOTATIONCOLOR, 1, 0, yColor);
	
	int		nTypes[1];
	double	fYLoc[1];
	COLORREF	yColor[1];
	nTypes[0] = PELT_DOT ;
	fYLoc[0] = 0.0F ;
	yColor[0] = RGB(128,128,128);*/

//	int nArray[2];	nArray[0]=1;	nArray[1]=1;
//	SetV(PEP_naMULTIAXESSUBSETS, nArray, 2);

	SetN(PEP_nWORKINGAXIS, 1);
//	SetSZ(PEP_szYAXISLABEL,"Second Y Axis");
	SetN(PEP_naHORZLINEANNOTATIONAXIS, 1);
	SetV(PEP_faHORZLINEANNOTATION, fYLoc,1);
	SetV(PEP_naHORZLINEANNOTATIONTYPE, nTypes,1);
	SetV(PEP_dwaHORZLINEANNOTATIONCOLOR, yColor,1);
}

void CGraphWnd::SetVerticalLine()
{
	int		nTypes[1];
	double	fXLoc[1];
	COLORREF	xColor[1];
	nTypes[0] = PELT_DOT ;
	fXLoc[0] = 100.0F ;
	xColor[0] = RGB(128,128,128);
	SetV(PEP_faVERTLINEANNOTATION, fXLoc,1);
	SetV(PEP_naVERTLINEANNOTATIONTYPE, nTypes,1);
	SetV(PEP_dwaVERTLINEANNOTATIONCOLOR, xColor,1);
}

void CGraphWnd::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(nChar == 27 || nChar == VK_RETURN)
	{
		SetN(PEP_bZOOMMODE, FALSE);
		PEresetimage(m_hWndPE, 0, 0);
	}
	CStatic::OnChar(nChar, nRepCnt, nFlags);
}

BOOL CGraphWnd::OnCommand(WPARAM wp, LPARAM lp)
{
    HOTSPOTDATA HSData;

    switch (HIWORD(wp))
    {
        case PEWN_CLICKED:
            PEvget(m_hWndPE, PEP_structHOTSPOTDATA, &HSData);
            if (HSData.nHotSpotType == PEHS_SUBSET)
            {
				/*
                char szTmp[128];
                char szSubset[48];
                szTmp[0] = 0;
                strcat(szTmp, "You clicked subset ");
                sprintf(szSubset, "%i", HSData.w1);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".  Subset label is ");
                PEvgetcell(m_hPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".");
                MessageBox(szTmp, "Subset Drill Down", MB_OK | MB_ICONINFORMATION);
				*/
            } 
            else if (HSData.nHotSpotType == PEHS_POINT)
            {
				/*
                if (PECONTROL_PIE != PEnget(m_hWndPE, PEP_nOBJECTTYPE))
                {
                    char szTmp[128];
                    char szSubset[48];
                    szTmp[0] = 0;
                    strcat(szTmp, "You clicked point ");
                    sprintf(szSubset, "%i", HSData.w1);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".  Point label is ");
                    PEvgetcell(m_hPE, PEP_szaPOINTLABELS, (UINT) HSData.w1, szSubset);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".");
                    MessageBox(szTmp, "Point Drill Down", MB_OK | MB_ICONINFORMATION);
                }    
                else
                 return CView::OnCommand(wp, lp);
				 */
            }
            else if ((HSData.nHotSpotType == PEHS_TABLE) || (HSData.nHotSpotType == PEHS_DATAPOINT))
            {
				/*
                UINT offset = ((int) HSData.w1 * (int) PEnget(m_hWndPE, PEP_nPOINTS)) + (int) HSData.w2;
                char    szTmp[128];
                char    szSubset[128];
                char    szTmp2[48];
                float   fData;
                szTmp[0] = 0;
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, "항목 : ");
                strcat(szTmp, szSubset);
                strcat(szTmp, "  데이타값 : ");
                PEvgetcell(m_hWndPE, PEP_faYDATA, offset, (LPVOID) &fData);
                sprintf(szTmp2, "%.4f", fData);             
                strcat(szTmp, szTmp2);
                MessageBox(szTmp, "선택된 항목 값", MB_OK | MB_ICONINFORMATION);
				*/
            }
            return TRUE;
    }
    return CStatic::OnCommand(wp, lp);
}
