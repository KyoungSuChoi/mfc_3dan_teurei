// ChErrorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ChErrorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChErrorDlg dialog


CChErrorDlg::CChErrorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChErrorDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CChErrorDlg"));
	//{{AFX_DATA_INIT(CChErrorDlg)
	m_nCodeCount = 0;
	//}}AFX_DATA_INIT
	memset(m_code, 0, sizeof(m_code));
	
	m_nTotalCh = 0;
}

CChErrorDlg::~CChErrorDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CChErrorDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}



void CChErrorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChErrorDlg)
	DDX_Control(pDX, IDC_CODE_SEL_COMBO, m_ctrlCodeCombo);
	DDX_Control(pDX, IDC_MODULE_COMBO, m_ctrlModuleSelCombo);
	DDX_Text(pDX, IDC_CODE_COUNT, m_nCodeCount);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChErrorDlg, CDialog)
	//{{AFX_MSG_MAP(CChErrorDlg)
	ON_BN_CLICKED(IDC_BUTTON_SEARCH, OnButtonSearch)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_CODE_SEL_COMBO, OnSelchangeCodeSelCombo)
	ON_BN_CLICKED(IDC_PRINT_GRID, OnPrintGrid)
	ON_BN_CLICKED(IDC_SAVE_FILE, OnSaveFile)
	ON_BN_CLICKED(IDC_UPDATE_BUTTON, OnUpdateButton)
	ON_BN_CLICKED(IDC_BUTTON_INIT, OnButtonInit)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDoubleClick)
	ON_MESSAGE(WM_GRID_MOVECELL,OnMovedCurrentCell)

END_MESSAGE_MAP()    

/////////////////////////////////////////////////////////////////////////////
// CChErrorDlg message handlers
BOOL CChErrorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitChannelGrid();
	InitErrorKindGrid();

	m_errorColor.AttachButton(IDC_ERROR_COLOR, this);
	m_errorColor.SetColor(RGB(255, 128, 0));

	try
	{
		m_dbServer.Open(ODBC_PROD_DB_NAME);		
	}
    catch (CDBException* e)			//DataBase Open Fail
	{
		AfxMessageBox(e->m_strError);
    	e->Delete();
	}
	
	CRecordset rs(&m_dbServer);
	CString strQuery;
	strQuery = "SELECT DISTINCT ModuleName FROM PinLog order by ModuleName";
	rs.Open( CRecordset::forwardOnly, strQuery);
	m_ctrlModuleSelCombo.AddString("All");
	while(!rs.IsEOF())
	{
		rs.GetFieldValue((short)0, strQuery);
		m_ctrlModuleSelCombo.AddString(strQuery);
		rs.MoveNext();
	}
	rs.Close();
	m_ctrlModuleSelCombo.SetCurSel(0);

	m_ctrlCodeCombo.AddString("All");
	m_ctrlCodeCombo.SetItemData(0, -1);
	
	strQuery.Format("%s", m_pDoc->ChCodeMsg(EP_CODE_CELL_NONE));
	m_ctrlCodeCombo.AddString(strQuery);
	m_ctrlCodeCombo.SetItemData(1, EP_CODE_CELL_NONE);

	strQuery.Format("%s", m_pDoc->ChCodeMsg(EP_CODE_FAIL_VTG_HIGH));
	m_ctrlCodeCombo.AddString(strQuery);
	m_ctrlCodeCombo.SetItemData(2, EP_CODE_FAIL_VTG_HIGH);
	
	strQuery.Format("%s", m_pDoc->ChCodeMsg(EP_CODE_FAIL_VTG_LOW));
	m_ctrlCodeCombo.AddString(strQuery);
	m_ctrlCodeCombo.SetItemData(3, EP_CODE_FAIL_VTG_LOW);
	
	strQuery.Format("%s", m_pDoc->ChCodeMsg(EP_CODE_FAIL_CRT_HIGH));
	m_ctrlCodeCombo.AddString(strQuery);
	m_ctrlCodeCombo.SetItemData(4, EP_CODE_FAIL_CRT_HIGH);
	
	strQuery.Format("%s", m_pDoc->ChCodeMsg(EP_CODE_FAIL_CRT_LOW));
	m_ctrlCodeCombo.AddString(strQuery);
	m_ctrlCodeCombo.SetItemData(5, EP_CODE_FAIL_CRT_LOW);

	strQuery.Format("%s", m_pDoc->ChCodeMsg(EP_CODE_FAIL_V_HUNTING));
	m_ctrlCodeCombo.AddString(strQuery);
	m_ctrlCodeCombo.SetItemData(6, EP_CODE_FAIL_V_HUNTING);

	strQuery.Format("%s", m_pDoc->ChCodeMsg(EP_CODE_FAIL_I_HUNTING));
	m_ctrlCodeCombo.AddString(strQuery);
	m_ctrlCodeCombo.SetItemData(7, EP_CODE_FAIL_I_HUNTING);
	
	m_ctrlCodeCombo.SetCurSel(0);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChErrorDlg::InitChannelGrid()
{
	m_ChannelGrid.SubclassDlgItem(IDC_CHERROR, this);
	m_ChannelGrid.m_bSameRowSize = TRUE;
	m_ChannelGrid.m_bSameColSize = TRUE;
//---------------------------------------------------------------------//
	m_ChannelGrid.m_bCustomWidth = FALSE;
	m_ChannelGrid.m_bCustomColor = FALSE;

	m_ChannelGrid.Initialize();
	m_ChannelGrid.LockUpdate();

	int nTrayColSize = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayColSize", EP_DEFAULT_TRAY_COL_COUNT);
	int nTrayRowSize = AfxGetApp()->GetProfileInt(FORMSET_REG_SECTION, "TrayRowSize", EP_DEFAULT_TRAY_ROW_COUNT);

	m_ChannelGrid.SetRowCount(nTrayRowSize);
	m_ChannelGrid.SetColCount(nTrayColSize*2);
	m_ChannelGrid.SetColWidth(0, 0, 0);
	m_ChannelGrid.SetRowHeight(0, 0, 0);
	m_ChannelGrid.SetDefaultRowHeight(26);
	m_ChannelGrid.EnableGridToolTips();

	m_ChannelGrid.SetValueRange(CGXRange(0, 0), "CH");

	m_ChannelGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_ChannelGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[0]))); //"굴림"

	int k = 0;
#ifdef DSP_TYPE_HORIZENTAL
	for(int i = 0; i < m_ChannelGrid.GetRowCount() ; i++)
	{
		for(int j = 0; j < m_ChannelGrid.GetColCount()/2; j++)
		{
#else
	for(int j = 0; j < m_ChannelGrid.GetColCount()/2; j++)
	{
		for(int i = 0; i < m_ChannelGrid.GetRowCount() ; i++)
		{		
#endif
			m_ChannelGrid.SetStyleRange(CGXRange(i+1, j*2+1), CGXStyle().SetValue(long(k+1)).SetInterior(RGB(230, 230, 230)).SetDraw3dFrame(gxFrameRaised));
			k++;
			if(k >= EP_MAX_CH_PER_MD)	break;
		}
	}
	m_ChannelGrid.Redraw();	
	m_ChannelGrid.LockUpdate(FALSE);
	m_ChannelGrid.Redraw();
}

void CChErrorDlg::InitErrorKindGrid()
{
	m_ErrorKindGrid.SubclassDlgItem(IDC_CHERROR2, this);
	m_ErrorKindGrid.m_bSameRowSize = FALSE;
	m_ErrorKindGrid.m_bSameColSize = TRUE;
//---------------------------------------------------------------------//
	m_ErrorKindGrid.m_bCustomWidth = FALSE;
	m_ErrorKindGrid.m_bCustomColor = FALSE;

	m_ErrorKindGrid.Initialize();
	m_ErrorKindGrid.LockUpdate();

	m_ErrorKindGrid.SetRowCount(EP_MAX_PINLOG);
	m_ErrorKindGrid.SetColCount(2);
	m_ErrorKindGrid.SetDefaultRowHeight(20);
//	m_ErrorKindGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);

	m_ErrorKindGrid.SetColWidth(0,0, 50);
	m_ErrorKindGrid.EnableGridToolTips();

	m_ErrorKindGrid.SetValueRange(CGXRange(0,0), TEXT_LANG[1]); //"최근"
	m_ErrorKindGrid.SetValueRange(CGXRange(0,1), TEXT_LANG[2]); //"CODE"
	m_ErrorKindGrid.SetValueRange(CGXRange(0,2), TEXT_LANG[3]); //"연속/누적횟수"

//	m_ErrorKindGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	//Row Header Setting
//	m_ErrorKindGrid.SetStyleRange(CGXRange().SetCols(1),
//			CGXStyle().SetEnabled(TRUE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_ErrorKindGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[0]))); //"굴림"
	
	m_ErrorKindGrid.LockUpdate(FALSE);
	m_ErrorKindGrid.Redraw();
}

void CChErrorDlg::OnButtonSearch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	int nIndex = m_ctrlModuleSelCombo.GetCurSel();
	if(nIndex == LB_ERR)		return;

	CString strTemp, strModule, strSQL, strQurey;
	m_ctrlModuleSelCombo.GetLBText(nIndex, strModule);
	memset(m_code, 0, sizeof(m_code));

	CWaitCursor wait;
	CDBVariant val;
	for(int i=0; i<EP_MAX_PINLOG; i++)
	{
		if(i == EP_MAX_PINLOG-1)	
		{
			strTemp.Format("Code%d", i+1);
		}
		else
		{
			strTemp.Format("Code%d,", i+1);
		}
		strSQL += strTemp;
	}

	if(nIndex == 0)
	{
		strQurey.Format("SELECT ChIndex, CurField, %s FROM PinLog ORDER BY ChIndex", strSQL);
		m_strSelModule.Empty();
	}
	else
	{
		strQurey.Format("SELECT ChIndex, CurField, %s FROM PinLog WHERE ModuleName = '%s' ORDER BY ChIndex", strSQL, strModule);
		m_strSelModule = strModule;
	}

	CRecordset rs(&m_dbServer);
	rs.Open(CRecordset::forwardOnly, strQurey);
	long lChIndex, lCurField = 1;
	int nMaxCh = 0;
	long alCode[EP_MAX_PINLOG];
	while(!rs.IsEOF())
	{
		ZeroMemory(alCode, sizeof(alCode));

		rs.GetFieldValue((short)0, val, SQL_C_SLONG);
		lChIndex = val.m_lVal;
		if(nMaxCh < lChIndex)	
			nMaxCh = lChIndex;										// nMaxCh == Pin 상태 값을 입력할 Code Index 번호

		rs.GetFieldValue((short)1, val, SQL_C_SLONG);
		lCurField = val.m_lVal;

//		if(lChIndex >= 0  && lChIndex < EP_MAX_CH_PER_MD)
//		{
			for(int c=0; c<rs.GetODBCFieldCount()-2; c++)
			{
				rs.GetFieldValue((short)c+2, val, SQL_C_SLONG);		// +2 
				alCode[c] = val.m_lVal;								// Pin 상태값 30 가지를 배열로 저장 
			}
//		}		
		//최근 History부터 sort한다.
		int nIncrese = 0;
		int f = 0;

		for(f = lCurField; f > 0; f--)
		{
			m_code[lChIndex][nIncrese++] = alCode[f-1];
		}
		for(f = EP_MAX_PINLOG; f > lCurField; f--)
		{
			m_code[lChIndex][nIncrese++] = alCode[f-1];
		}

		rs.MoveNext();
	}
	rs.Close();

	nMaxCh++;	//1Base Ch
	m_nTotalCh = nMaxCh;
	int nRowCnt = 0;
	int nC = m_ChannelGrid.GetColCount()/2;
	if(nC > 0)
	{
		nRowCnt = nMaxCh/nC;
		if(nMaxCh%nC)	
			nRowCnt++;
	}

	m_ChannelGrid.SetRowCount(nRowCnt);

	UpdateGridData();

	GetDlgItem(IDC_CODE_SEL_COMBO)->EnableWindow();
	GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow();
	GetDlgItem(IDC_ERROR_COLOR)->EnableWindow();
	GetDlgItem(IDC_CODE_COUNT)->EnableWindow();
}

LONG CChErrorDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CString      str;
//	int    row = 1;

	nCol = LOWORD(wParam);
	nRow = HIWORD(wParam);

	if(nRow < 1 || nCol < 1)	return 0;
	CMyGridWnd *pGrid = (CMyGridWnd *)lParam;
	if(pGrid != &m_ChannelGrid)		return 0;
	
	if(nCol%2)	nCol++;

#ifdef DSP_TYPE_HORIZENTAL
	int channelNo = (nRow -1)*(m_ChannelGrid.GetColCount()/2)+(nCol/2);
#else
	int channelNo = (nCol/2 -1)*m_ChannelGrid.GetRowCount()+nRow;
#endif

	if(channelNo > m_nTotalCh)	return 0;

	DrawPinLogList(channelNo);

	return 0;
}

LONG CChErrorDlg::OnGridDoubleClick(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return 0;
}

void CChErrorDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	if(m_dbServer.IsOpen())
	{
		m_dbServer.Close();
	}
}

void CChErrorDlg::UpdateGridData()
{
	UpdateData(TRUE);

	int nCode = -1;
	int nSelCode =  m_ctrlCodeCombo.GetCurSel();
	if(nSelCode != CB_ERR)	nCode = m_ctrlCodeCombo.GetItemData(nSelCode);

	int nCount[EP_MAX_CH_PER_MD];
	memset(nCount, 0, sizeof(nCount));

	for(int ch =0; ch<m_nTotalCh; ch++)
	{
		for(int i=0; i<EP_MAX_PINLOG; i++)
		{
			if(nCode < 0)	//All Error
			{
				//사용자 종료는 정상으로 처리
				if(!::IsNormalCell(m_code[ch][i]) && m_code[ch][i] != EP_CODE_END_USER_STOP && m_code[ch][i] != EP_CODE_CELL_NONE)
				{
					nCount[ch]++;
				}	
			}
			else
			{
				if(nCode == m_code[ch][i])
				{
					nCount[ch]++;
				}
			}
		}
	}

//	int count;
	COLORREF 	color = m_errorColor.GetColor();

	CString   str;
	int       k = 0;

#ifdef DSP_TYPE_HORIZENTAL
	for(int i = 0; i < m_ChannelGrid.GetRowCount() ; i++)
	{
		for(int j = 0; j < m_ChannelGrid.GetColCount()/2; j++)
		{
#else
	for(int j = 0; j < m_ChannelGrid.GetColCount()/2; j++)
	{
		for(int i = 0; i < m_ChannelGrid.GetRowCount() ; i++)
		{		
#endif
			str.Format("%ld", nCount[k]);		
			if(m_nCodeCount > 0 && nCount[k] >= m_nCodeCount)
			{
				m_ChannelGrid.SetStyleRange(CGXRange(i+1, (j+1)*2), CGXStyle().SetValue(str).SetInterior(color));
			}
			else
			{
				m_ChannelGrid.SetStyleRange(CGXRange(i+1, (j+1)*2), CGXStyle().SetValue(str).SetInterior(RGB(255, 255, 255)));
			}
			m_ChannelGrid.SetStyleRange(CGXRange(i+1, j*2+1), CGXStyle().SetValue(long(k+1)).SetInterior(RGB(230, 230, 230)).SetDraw3dFrame(gxFrameRaised));
		
			k++;
			if(k >= EP_MAX_CH_PER_MD)	break;
		}
	}
	m_ChannelGrid.Redraw();
}

void CChErrorDlg::OnSelchangeCodeSelCombo() 
{
	// TODO: Add your control notification handler code here
	OnUpdateButton();
}

void CChErrorDlg::OnPrintGrid() 
{
	// TODO: Add your control notification handler code here
	CDC dc;
    CPrintDialog printDlg(FALSE);

    if (printDlg.DoModal() == IDCANCEL)         // Get printer settings from user
        return;

    dc.Attach(printDlg.GetPrinterDC());         // Attach a printer DC
    dc.m_bPrinting = TRUE;

    CString strTitle;                           // Get the application title
	if(dc.m_hDC == NULL)
	{
		strTitle.Format(TEXT_LANG[4], printDlg.GetPortName()); //"%s 장치를 사용할 수 없습니다."
		AfxMessageBox(strTitle, MB_ICONSTOP|MB_OK);
		return;
	}
    strTitle.LoadString(AFX_IDS_APP_TITLE);

    DOCINFO di;                                 // Initialise print document details
    ::ZeroMemory (&di, sizeof (DOCINFO));
    di.cbSize = sizeof (DOCINFO);
    di.lpszDocName = strTitle;
	

    BOOL bPrintingOK = dc.StartDoc(&di);        // Begin a new print job

    // Get the printing extents and store in the m_rectDraw field of a 
    // CPrintInfo object
    CPrintInfo Info;
    Info.m_rectDraw.SetRect(0,0, 
                            dc.GetDeviceCaps(HORZRES), 
                         dc.GetDeviceCaps(VERTRES));
   	Info.SetMaxPage(0xffff);

	Info.m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	Info.m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	
	
	
	CFont font;
	font.CreateFont(80, 50, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, HANGUL_CHARSET, 0, 0, 0, VARIABLE_PITCH, TEXT_LANG[0]); //"굴림"
	dc.SelectObject(&font);

    OnBeginPrinting(&dc, &Info);                // Call your "Init printing" funtion
    for (UINT page = Info.GetMinPage(); 
         page <= Info.GetMaxPage() && bPrintingOK; 
         page++)
    {
        dc.StartPage();                         // begin new page
        Info.m_nCurPage = page;
        OnPrint(&dc, &Info);                    // Call your "Print page" function
        bPrintingOK = (dc.EndPage() > 0);       // end page
    }
    OnEndPrinting(&dc, &Info);                  // Call your "Clean up" funtion

    if (bPrintingOK)
        dc.EndDoc();                            // end a print job
    else
        dc.AbortDoc();                          // abort job.

    dc.Detach();              	
}

void CChErrorDlg::OnBeginPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	int nXMargin = 400;
	int nYMargin = 800;
	int nTextHeight = 80;
	int nLineCount = 0;
	CString strTemp;
	CRect strRect;
	
	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	m_ctrlModuleSelCombo.GetLBText(m_ctrlModuleSelCombo.GetCurSel(), strTemp);
	strTemp = TEXT_LANG[6]+ strTemp; //"검색 조건: "
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);
	nLineCount++;

	strRect.SetRect(nXMargin, nYMargin + (nLineCount*nTextHeight) ,10000, nYMargin + (nLineCount+1)*nTextHeight);
	m_ctrlCodeCombo.GetLBText(m_ctrlCodeCombo.GetCurSel(), strTemp);
	strTemp = TEXT_LANG[7]+ strTemp; //"불량 코드: "
	pDC->DrawText( _T(strTemp), strRect, DT_WORDBREAK | DT_EXPANDTABS);


	m_ChannelGrid.GetParam()->GetProperties()->SetBlackWhite(FALSE);
	m_ChannelGrid.GetParam()->GetProperties()->SetMargins(180,10,80,10);
	m_ChannelGrid.SetColWidth(1, m_ChannelGrid.GetColCount(), 40);
	m_ChannelGrid.OnGridBeginPrinting(pDC, pInfo);

}

void CChErrorDlg::OnEndPrinting(CDC *pDC, CPrintInfo *pInfo)
{
	m_ChannelGrid.OnGridEndPrinting(pDC, pInfo);
}

void CChErrorDlg::OnPrint(CDC *pDC, CPrintInfo *pInfo)
{
	m_ChannelGrid.OnGridPrint(pDC, pInfo);
}


void CChErrorDlg::OnSaveFile() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	CTime curTime =  CTime::GetCurrentTime();
	strFileName.Format("%s.csv", curTime.Format("%Y_%m_%d"));	
	
	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();

		CString strTemp;
		m_ctrlModuleSelCombo.GetLBText(m_ctrlModuleSelCombo.GetCurSel(), strTemp);
		fprintf(fp, TEXT_LANG[6]+" :, %s\n", (LPSTR)(LPCTSTR)strTemp);	// FileName //"검색 조건"

		m_ctrlCodeCombo.GetLBText(m_ctrlCodeCombo.GetCurSel(), strTemp);
		strTemp = TEXT_LANG[7]+":, "+ strTemp; //"불량 코드"
		fprintf(fp, "%s\n", (LPSTR)(LPCTSTR)strTemp);

		fprintf(fp, "\n");
		
		for(int row = 0; row<m_ChannelGrid.GetRowCount()+1; row++)
		{
			for(int col = 0; col<m_ChannelGrid.GetColCount()+1; col++)
			{
				fprintf(fp, "%s,", m_ChannelGrid.GetValueRowCol(row, col));
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
		EndWaitCursor();
	}		
}

void CChErrorDlg::OnUpdateButton() 
{
	// TODO: Add your control notification handler code here
	UpdateGridData();
}


void CChErrorDlg::OnButtonInit() 
{
	// TODO: Add your control notification handler code here
	
	//현재 선택된 Pin의 로그를 Reset 한다.

	ROWCOL nRow, nCol;
	if(m_ChannelGrid.GetCurrentCell(nRow, nCol) == FALSE)	return;
	if(nRow < 1 || nCol < 1)	return;

	if(nCol%2)	nCol++;

#ifdef DSP_TYPE_HORIZENTAL
	int channelNo = (nRow -1)*(m_ChannelGrid.GetColCount()/2)+(nCol/2);
#else
	int channelNo = (nCol/2 -1)*m_ChannelGrid.GetRowCount()+nRow;
#endif
	if(channelNo < 1 && channelNo > EP_MAX_CH_PER_MD)		return;

	CString strTemp, strSQL;
	if(m_strSelModule.IsEmpty())	//All Module
	{
		strTemp.Format(TEXT_LANG[8], channelNo);//"모든 장비의 Channel %d Pin log를 초기화 하시겠습니까?"
	}
	else
	{
		strTemp.Format(TEXT_LANG[9], m_strSelModule, channelNo); //"%s의 Channel %d Pin log를 초기화 하시겠습니까?"
	}

	if(AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION) ==IDYES)
	{
		//1. DataBase Reset
		CWaitCursor wait;
		for(int i=0; i<EP_MAX_PINLOG; i++)
		{
			if(i == EP_MAX_PINLOG-1)	strTemp.Format("Code%d = 0", i+1);
			else						strTemp.Format("Code%d = 0,", i+1);
			strSQL += strTemp;
		}
		if(m_strSelModule.IsEmpty())
		{
			strTemp.Format("UPDATE PinLog SET CurField = 0, %s WHERE ChIndex = %d", strSQL, channelNo-1);
		}
		else
		{
			strTemp.Format("UPDATE PinLog SET CurField = 0, %s WHERE ChIndex = %d AND ModuleName = '%s'", strSQL, channelNo-1, m_strSelModule);
		}

		m_dbServer.BeginTrans();
		m_dbServer.ExecuteSQL(strTemp);
		m_dbServer.CommitTrans();
	
		//2. Load 된 PinLog Data Reset
		memset(&m_code[channelNo-1], 0, sizeof(int)*EP_MAX_PINLOG);		

		//3. Grid 표시 Reset
		UpdateGridData();
		//4.현재 PinLog List Reset
		DrawPinLogList(channelNo);
	}
}

void CChErrorDlg::DrawPinLogList(int nChNo)
{
	CString str;
	BOOL bLock = m_ErrorKindGrid.LockUpdate();
	str.Format(TEXT_LANG[10], nChNo, EP_MAX_PINLOG); //"Channel %d 최근 %d건 이력"
    GetDlgItem(IDC_CHANNEL_NUMBER)->SetWindowText(str);

	int i = 0;

	for(i = 0 ; i < EP_MAX_PINLOG; i++)
	{
		str = m_pDoc->ChCodeMsg((BYTE)m_code[nChNo-1][i]);
    	m_ErrorKindGrid.SetValueRange(CGXRange(i+1, 1), str);
	}

	int nCnt = 0, nHisCnt = 0;
	for( i = EP_MAX_PINLOG ; i > 0; i--)
	{
		if(!::IsNormalCell((BYTE)m_code[nChNo-1][i-1]) && m_code[nChNo-1][i-1] != EP_CODE_END_USER_STOP && m_code[nChNo-1][i-1] != EP_CODE_CELL_NONE)
		{
			nCnt++;
			nHisCnt++;
			str.Format("%d / %d", nHisCnt, nCnt);
    		m_ErrorKindGrid.SetValueRange(CGXRange(i, 2), str);
			m_ErrorKindGrid.SetStyleRange(CGXRange(i, 2), CGXStyle().SetInterior(RGB(255 ,(255-(255/EP_MAX_PINLOG)*nHisCnt),(255-(255/EP_MAX_PINLOG)*nHisCnt))));
		}
		else
		{
			str.Empty();
			nHisCnt = 0;
			m_ErrorKindGrid.SetStyleRange(CGXRange(i, 2), CGXStyle().SetInterior(RGB(255,255,255)));
		}
    	m_ErrorKindGrid.SetValueRange(CGXRange(i, 2), str);
	}

	m_ErrorKindGrid.LockUpdate(bLock);
	m_ErrorKindGrid.Redraw();
}

void CChErrorDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	CString strTemp, strSQL;
	if(m_strSelModule.IsEmpty())	//All Module
	{
		strTemp.Format(TEXT_LANG[11]); //"모든 장비의 Pin log를 초기화 하시겠습니까?"
	}
	else
	{
		strTemp.Format(TEXT_LANG[12], m_strSelModule);//"%s의 Pin log를 초기화 하시겠습니까?"
	}

	if(AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION) ==IDYES)
	{
		//1. DataBase Reset
		CWaitCursor wait;
		for(int i=0; i<EP_MAX_PINLOG; i++)
		{
			if(i == EP_MAX_PINLOG-1)	strTemp.Format("Code%d = 0", i+1);
			else						strTemp.Format("Code%d = 0,", i+1);
			strSQL += strTemp;
		}
		if(m_strSelModule.IsEmpty())
		{
			strTemp.Format("UPDATE PinLog SET CurField = 0, %s", strSQL);
		}
		else
		{
			strTemp.Format("UPDATE PinLog SET CurField = 0, %s WHERE ModuleName = '%s'", strSQL, m_strSelModule);
		}

		m_dbServer.BeginTrans();
		m_dbServer.ExecuteSQL(strTemp);
		m_dbServer.CommitTrans();
	
		//2. Load 된 PinLog Data Reset
		memset(&m_code, 0, sizeof(m_code));		

		//3. Grid 표시 Reset
		UpdateGridData();
		//4.현재 PinLog List Reset
	}	
}
