// ProcTypeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ProcTypeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProcTypeRecordSet

IMPLEMENT_DYNAMIC(CProcTypeRecordSet, CRecordset)

CProcTypeRecordSet::CProcTypeRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CProcTypeRecordSet)
	m_ProcType = 0;
	m_ProcID = 0;
	m_Description = _T("");
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CProcTypeRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=CTSSchedule");
}

CString CProcTypeRecordSet::GetDefaultSQL()
{
	return _T("[ProcType]");
}

void CProcTypeRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CProcTypeRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[ProcType]"), m_ProcType);
	RFX_Long(pFX, _T("[ProcID]"), m_ProcID);
	RFX_Text(pFX, _T("[Description]"), m_Description);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CProcTypeRecordSet diagnostics

#ifdef _DEBUG
void CProcTypeRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CProcTypeRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
