#ifndef __PARSING_DATA_H__
#define	__PARSING_DATA_H__

#define		MAX_FIELD_NUM			10
#define		CHARGE					0x01
#define		DISCHARGE				0x02
#define		REST					0x03
#define		ENDSTEP					0x04

#define		STR_STEP_TIME			"Step Time"
#define		STR_STEP_TYPE			"Step Type"
#define		STR_STEP_NO				"Step No"
#define		STR_CH_NO				"Ch No"
#define		STR_VOLTAGE				"Voltage"
#define		STR_CURRENT				"Current"
#define		STR_CAPACITANCE			"Capacitance"

#define		PARSING_ERROR			-1
#define		STEP_TIME				1
#define		VOLTAGE					2
#define		CURRENT					3
#define		CAPACITANCE				4
#define		STEP_TYPE				5
#define		STEP_NO					6
#define		CHANNEL_NO				7


/*class CFieldData 
{
public :
	void InitField(int nNum)
	{
		m_fVoltage = new float[nNum];
		m_fCurrent = new float[nNum];
		m_fCapacity = new float[nNum];
		m_nChNum = new int[nNum];
		m_nStepNum = new int[nNum];
		m_nStepType = new int[nNum];
		m_nTime = new UINT[nNum];
	}
	~CFieldData()
	{
		if(m_fVoltage)	delete [] m_fVoltage;
		if(m_fCurrent)	delete [] m_fCurrent;
		if(m_fCapacity)	delete [] m_fCapacity;
		if(m_nChNum)	delete [] m_nChNum;
		if(m_nStepNum)	delete [] m_nStepNum;
		if(m_nStepType)	delete [] m_nStepType;
		if(m_nTime)		delete [] m_nTime;
	}
	UINT* m_nTime;
	float* m_fVoltage;
	float* m_fCurrent;
	float* m_fCapacity;
	
	int* m_nChNum;
	int* m_nStepNum;
	int* m_nStepType;
};

class CDataFormat
{
public :
	CDataFormat()
	{
		m_nFieldNum = 0; m_nDataNum = 0;
		for(int i = 0; i < MAX_FIELD_NUM; i++)
			m_nFieldID[i] = 0;
	}
	~CDataFormat()
	{
		char* pTmp = NULL;
		for(int i = 0 ; i < m_aFieldName.GetSize(); i++ )
		{
			pTmp = (char *)m_aFieldName[i];
			//delete pTmp;
			pTmp = NULL;
		}
		m_aFieldName.RemoveAll();
	}
	void InitField(int nDataNum)
	{
		m_fldData.InitField(nDataNum);
	}
	UINT m_nFieldNum;
	UINT m_nDataNum;
	int m_nFieldID[MAX_FIELD_NUM];
	CPtrArray m_aFieldName;
	CFieldData m_fldData;
};
*/
typedef struct CFieldData
{
public :
	float* m_fVoltage;
	float* m_fCurrent;
	float* m_fCapacity;
	
	int* m_nChNum;
	int* m_nStepNum;
	int* m_nStepType;
	UINT* m_nTime;
	
	void InitField(int nNum)
	{
		m_fVoltage = new float[nNum];
		m_fCurrent = new float[nNum];
		m_fCapacity = new float[nNum];
		m_nChNum = new int[nNum];
		m_nStepNum = new int[nNum];
		m_nStepType = new int[nNum];
		m_nTime = new UINT[nNum];
	}
	~CFieldData()
	{
		if(m_fVoltage)	delete [] m_fVoltage;
		if(m_fCurrent)	delete [] m_fCurrent;
		if(m_fCapacity)	delete [] m_fCapacity;
		if(m_nChNum)	delete [] m_nChNum;
		if(m_nStepNum)	delete [] m_nStepNum;
		if(m_nStepType)	delete [] m_nStepType;
		if(m_nTime)		delete [] m_nTime;
	}
} CFieldData ;

typedef struct CDataFormat
{
public :
	CDataFormat()
	{
		m_fVoltage = NULL;
		m_fCurrent = NULL;
		m_fCapacity = NULL;
		m_nTime = NULL;
		
		m_nFieldNum = 0; m_nDataNum = 0;
		for(int i = 0; i < MAX_FIELD_NUM; i++)
			m_nFieldID[i] = 0;
	}
	~CDataFormat()
	{
		if(m_fVoltage)	delete [] m_fVoltage;
		if(m_fCurrent)	delete [] m_fCurrent;
		if(m_fCapacity)	delete [] m_fCapacity;
		if(m_nTime)		delete [] m_nTime;
	}
	void InitField(int nDataNum)
	{
		m_fVoltage = new float[nDataNum];
		m_fCurrent = new float[nDataNum];
		m_fCapacity = new float[nDataNum];
		m_nTime = new UINT[nDataNum];
	}
	UINT m_nFieldNum;
	UINT m_nDataNum;
	int m_nFieldID[MAX_FIELD_NUM];
	float* m_fVoltage;
	float* m_fCurrent;
	float* m_fCapacity;
	UINT* m_nTime;
} CDataFormat;

#endif //__PARSING_DATA_H__