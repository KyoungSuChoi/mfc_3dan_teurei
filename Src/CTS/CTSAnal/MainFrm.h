// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__7E9AA9FC_E5F8_48BB_8F8A_CD99A14D463F__INCLUDED_)
#define AFX_MAINFRM_H__7E9AA9FC_E5F8_48BB_8F8A_CD99A14D463F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CTSAnalView.h"
#include "ResultView.h"
#include "SearchView.h"
#include "SearchMDBView.h"

class CResultGraphView;

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

// Attributes
public:
	GRAPH_CFG          m_GraphConfig;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
private:
// 20201103 KSCHOI Add Build Date Display By KSJ START
	CString MacroDateToCString(const char *MacroDate);
// 20201103 KSCHOI Add Build Date Display By KSJ END

public:
	BOOL OpenDataFile(CString strFileName);
	BOOL OpenGraphFile(CString strFileName);
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
//	CFtpDlg               *m_pFtpDlg;
	CCTSAnalView		*m_pDbView;
	CResultView			*m_pResultView;
	CResultGraphView	*m_pResultGraphView;
	CSearchView			*m_pSearchView;
	CSearchMDBView		*m_pSearchMDBView;

	// CStatusBar  m_wndStatusBar;
	// CToolBar    m_wndToolBar;
	CFont   	m_ActiveFont;
	CFont   	m_InactiveFont;

	SEC3DTabWnd m_tabWnd;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnFtpDlg();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnUserOption();
	afx_msg void OnFileOpen();
	afx_msg void OnPinError();
	afx_msg void OnOpTime();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__7E9AA9FC_E5F8_48BB_8F8A_CD99A14D463F__INCLUDED_)
