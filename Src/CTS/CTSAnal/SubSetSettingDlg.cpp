// SubSetSettingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "SubSetSettingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubSetSettingDlg dialog


CSubSetSettingDlg::CSubSetSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSubSetSettingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSubSetSettingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSubSetSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubSetSettingDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	DDX_Control	(pDX, IDC_CH1_COLOR, 	m_btnChColor[0]);
	DDX_Control	(pDX, IDC_CH2_COLOR, 	m_btnChColor[1]);
	DDX_Control	(pDX, IDC_CH3_COLOR, 	m_btnChColor[2]);
	DDX_Control	(pDX, IDC_CH4_COLOR, 	m_btnChColor[3]);
	DDX_Control	(pDX, IDC_CH5_COLOR, 	m_btnChColor[4]);
	DDX_Control	(pDX, IDC_CH6_COLOR, 	m_btnChColor[5]);
	DDX_Control	(pDX, IDC_CH7_COLOR, 	m_btnChColor[6]);
	DDX_Control	(pDX, IDC_CH8_COLOR, 	m_btnChColor[7]);

}


BEGIN_MESSAGE_MAP(CSubSetSettingDlg, CDialog)
	//{{AFX_MSG_MAP(CSubSetSettingDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubSetSettingDlg message handlers

BOOL CSubSetSettingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	int i=0, j=0;
	for(j =0; j<MAX_LINE_NUM; j++)
	{
		m_bitmap[j].LoadBitmap(IDB_LINE_THINSOLID_BMP+j);
	}

	for(i=0; i<MAX_MUTI_AXIS; i++)
	{
		m_BitmapCombo[i].SubclassDlgItem( IDC_VTG_LINE+i, this);	
	
		for(j =0; j<MAX_LINE_NUM; j++)
		{
			m_BitmapCombo[i].AddBitmap(&m_bitmap[j], "");		// combo에 bitmap 과 string 을 추가한다.
		}
		m_BitmapCombo[i].SetCurSel(m_YAxisSet[i].nLineStyles);	
	}

	for ( i = 0; i < MAX_CHANNEL_NUM; i++)
	{
		m_btnChColor[i].SetColor(m_ChColor[i]);
	}

	return FALSE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSubSetSettingDlg::OnOK() 
{
	// TODO: Add extra validation here
	int i=0;
	for (i = 0; i <MAX_CHANNEL_NUM ; i++)
	{
		m_ChColor[i] = m_btnChColor[i].GetColor();
	}
	for (i = 0; i<MAX_MUTI_AXIS; i++)
	{
		m_YAxisSet[i].nLineStyles =  m_BitmapCombo[i].GetCurSel();
	}
	CDialog::OnOK();
}
