#if !defined(AFX_ANNOTATIONSETDLG_H__7031873D_0718_4DE7_9EFC_956E7691F934__INCLUDED_)
#define AFX_ANNOTATIONSETDLG_H__7031873D_0718_4DE7_9EFC_956E7691F934__INCLUDED_

#include "Mydefine.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AnnotationSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAnnotationSetDlg dialog
#include "SubSetSettingDlg.h"

class CAnnotationSetDlg : public CDialog
{
// Construction
public:
	CBitmap m_bitmap[MAX_LINE_NUM];
	void InitListCtrl();
	STR_GRID_SET m_LineInfo;
	CAnnotationSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAnnotationSetDlg)
	enum { IDD = IDD_ANNOTATION_SET_DLG };
	stingray::foundation::SECWellButton	m_btnLineColor2;
	stingray::foundation::SECWellButton	m_btnLineColor1;
	CListCtrl	m_ctrlList;
	CBitmapPickerCombo	m_ctrlAnnoLineStyle;
	CBitmapPickerCombo	m_ctrlAddLineStyle;
	CComboBox	m_ctrlDevied;
	BOOL	m_bAutoGrid;
	BOOL	m_bAddedGrid;
	BOOL	m_bSpecialGrid;
	float	m_fAddMin;
	float	m_fAddMax;
	float	m_fAddInterval;
	float	m_fAnnoPoint;
	CString	m_strAnnoComment;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAnnotationSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAnnotationSetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnOk();
	afx_msg void OnAnnoDelete();
	afx_msg void OnAnnoAdd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ANNOTATIONSETDLG_H__7031873D_0718_4DE7_9EFC_956E7691F934__INCLUDED_)
