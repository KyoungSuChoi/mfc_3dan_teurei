#if !defined(AFX_SEARCHMDBVIEW_H__F29DFAD2_D420_4B88_B5F2_2AEAF2C1EEA5__INCLUDED_)
#define AFX_SEARCHMDBVIEW_H__F29DFAD2_D420_4B88_B5F2_2AEAF2C1EEA5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SearchMDBView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSearchMDBView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "MyGridWnd.h"
#include "CTSAnal.h"
#include "CTSAnalDoc.h"

class CSearchMDBView : public CFormView
{
protected:
	CSearchMDBView();           // protected constructor used by dynamic creation
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
	DECLARE_DYNCREATE(CSearchMDBView)

// Form Data
public:
	void SearchDataBase();

	//{{AFX_DATA(CSearchMDBView)
	enum { IDD = IDD_SEARCH_MDB_DLG };
	CLabel	m_strTotalFileNum;
	CTime	m_Time_Start_S;
	CTime	m_Time_Start_E;
	CTime	m_Time_End_S;
	CTime	m_Time_End_E;
	CString	m_strBarCode;
	CString	m_strLotID;
	CString	m_strTrayNum;
	BOOL	m_bCheckStartDay;
	BOOL	m_bCheckEndDay;
	//}}AFX_DATA

// Attributes
public:
	CCTSAnalDoc* GetDocument();
	CCTSAnalDoc *m_pDoc;
	CMyGridWnd m_wndDBListGrid;
	CStringArray m_arryDBField;
	CStringArray m_arryDataUnit;
	CStringArray m_arryDisplayField;

// Operations
public:
	void RedrawListGrid();
	void InitListGrid();
	BOOL LoadMDBField();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchMDBView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSearchMDBView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSearchMDBView)
	afx_msg void OnBtnSearch();
	afx_msg void OnBtnToday();
	afx_msg void OnBtnToday2();
	afx_msg void OnBtnExcelTrans();
	afx_msg void OnBtnFieldSet();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PowerFormationView.cpp
inline CCTSAnalDoc* CSearchMDBView::GetDocument()
{ return (CCTSAnalDoc*)m_pDocument; }
#endif
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHMDBVIEW_H__F29DFAD2_D420_4B88_B5F2_2AEAF2C1EEA5__INCLUDED_)
