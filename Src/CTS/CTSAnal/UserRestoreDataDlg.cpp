// UserRestoreDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "UserRestoreDataDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserRestoreDataDlg dialog


CUserRestoreDataDlg::CUserRestoreDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUserRestoreDataDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUserRestoreDataDlg)
	m_strFileLoacation = _T("");
	m_strLotNo = _T("");
	m_strTrayNo = _T("");
	m_nCellNo = 0;
	m_bDataFile = TRUE;
	//}}AFX_DATA_INIT
	m_pDB = NULL;
}


void CUserRestoreDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserRestoreDataDlg)
	DDX_Control(pDX, IDC_RACK_NAME, m_ctrlRackCombo);
	DDX_Text(pDX, IDC_FOLDER_NAME, m_strFileLoacation);
	DDX_Text(pDX, IDC_LOT_NO, m_strLotNo);
	DDX_Text(pDX, IDC_TRAY_NO, m_strTrayNo);
	DDX_Text(pDX, IDC_CELL_NO, m_nCellNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserRestoreDataDlg, CDialog)
	//{{AFX_MSG_MAP(CUserRestoreDataDlg)
	ON_BN_CLICKED(IDC_RESTORE_BUTTON, OnRestoreButton)
	ON_BN_CLICKED(IDC_DATA_FOLDER_SETTING, OnDataFolderSetting)
	ON_BN_CLICKED(IDC_LOCATION_FILE, OnLocationFile)
	ON_BN_CLICKED(IDC_LOCATION_RACK, OnLocationRack)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserRestoreDataDlg message handlers

void CUserRestoreDataDlg::OnRestoreButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_bDataFile)
	{
		CString strTemp;			
		strTemp.Format("%s\\R8-1 Step1.csv", m_strFileLoacation);

		//ch, state, Grade, chCode, StepNo, StepTime, TotalTime, V, I, Watt, WattHour, Capacity, Imp
//		float fTemp;
		float fData[EP_BATTERY_PER_TRAY][EP_RESULT_ITEM_NO];
		BYTE	code[EP_BATTERY_PER_TRAY][2];

		FILE *fp = fopen(strTemp, "rt");
		if(fp == NULL)		return ;

		char buff[13][64];
		
		//Title Read
		if(fscanf(fp, "%s %s %s %s %s %s %s %s %s %s %s %s %s", buff, buff, buff, buff, buff,buff,buff,buff,buff,buff,buff,buff,buff) < 13)
		{
			fclose(fp);
			return ;
		}

		for(int i=0; i< EP_BATTERY_PER_TRAY; i++)
		{
			if(fscanf(fp, "%s %s %s %s %s %s %s %s %s %s %s %s %s", 
				buff[0],buff[1],buff[2],buff[3],buff[4],buff[5],buff[6], buff[7],buff[8],buff[9],buff[10],buff[11],buff[12]) < 13) 
			{
				fclose(fp);
				return ;
			}
			strTemp = buff[5];		strTemp.Replace(",", "");				//step Time
			fData[i][EP_TIME_BIT_POS] = atof((LPSTR)(LPCTSTR)strTemp);
			strTemp = buff[7];		strTemp.Replace(",", "");				//voltage
			fData[i][EP_VOLTAGE_BIT_POS] = atof((LPSTR)(LPCTSTR)strTemp);
			strTemp = buff[8];		strTemp.Replace(",", "");				//current
			fData[i][EP_CURRENT_BIT_POS] = atof((LPSTR)(LPCTSTR)strTemp);
			strTemp = buff[11];		strTemp.Replace(",", "");				//capacity
			fData[i][EP_CAPACITY_BIT_POS] = atof((LPSTR)(LPCTSTR)strTemp);	
			strTemp = buff[9];		strTemp.Replace(",", "");				//watt
			fData[i][EP_WATT_BIT_POS] = atof((LPSTR)(LPCTSTR)strTemp);
			strTemp = buff[10];		strTemp.Replace(",", "");				//watt_hour
			fData[i][EP_WATT_HOUR_BIT_POS] = atof((LPSTR)(LPCTSTR)strTemp);
			strTemp = buff[12];		strTemp.Replace(",", "");				//Impedance
			fData[i][EP_IMPEDANCE_BIT_POS] = atof((LPSTR)(LPCTSTR)strTemp);
			strTemp = buff[2];		strTemp.Replace(",", "");				//grade Code
			code[i][0] = (BYTE)atoi((LPSTR)(LPCTSTR)strTemp);
			strTemp = buff[3];		strTemp.Replace(",", "");				//cell Code
			code[i][1] = (BYTE)atoi((LPSTR)(LPCTSTR)strTemp);
		}
		fclose(fp);
	}

/*		rsTestLog.m_TestSerialNo.Format("%s", procData.szTestSerialNo);
		rsTestLog.m_TraySerialNo = procData.lTraySerialNo;
		rsTestLog.m_LotNo.Format("%s", procData.szLotNo);
		rsTestLog.m_CellNo = procData.nCellNo;
		rsTestLog.m_TrayNo.Format("%s", procData.szTrayNo);
//			rsTestLog.m_DateTime;
		rsTestLog.m_ModelID = procData.lModelID;
		rsTestLog.m_ModelName.Format("%s", procData.szModelName);
		rsTestLog.Update();
		strTemp.Format("새로운 Tray 공정 시작 (TestSerialNo => %s )", procData.szTestSerialNo);
		rsTestLog.Requery();
		nRtn = rsTestLog.m_TestLogID;
*/

	//적용 모델 검사
	//Tray 와 Lot 번호로 기존 Data 검사
//	CRecordset rs(m_pDB);
	CString strSQL;
//	strSQL.Format("SELECT * FROM TestLog WHERE LotNo = '%s' AND TrayNo = '%s'");
	strSQL.Format("INSERT INTO TestLog () VALUES ()");
	m_pDB->BeginTrans();
	m_pDB->ExecuteSQL(strSQL);
	m_pDB->CommitTrans();
}

BOOL CUserRestoreDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	CString strTemp;
	for(int i=0; i<32; i++)
	{
		strTemp.Format("Rack %d-%d", i/3+1, i%3+1);
		m_ctrlRackCombo.AddString(strTemp);
	}

	m_bDataFile = TRUE;

	((CButton *)GetDlgItem(IDC_LOCATION_FILE))->SetCheck(m_bDataFile);
	((CButton *)GetDlgItem(IDC_LOCATION_RACK))->SetCheck(!m_bDataFile);
	GetDlgItem(IDC_FOLDER_NAME)->EnableWindow(TRUE);
	GetDlgItem(IDC_DATA_FOLDER_SETTING)->EnableWindow(TRUE);
	GetDlgItem(IDC_RACK_NAME)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUserRestoreDataDlg::OnDataFolderSetting() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	char dir[MAX_PATH] = {"c\\"};
	BROWSEINFO bi;
	bi.hwndOwner = m_hWnd;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = dir;
	bi.lpszTitle = "Please select a Step Data folder";
	bi.ulFlags = 0;
	bi.lpfn = NULL;

	char pszDisplayName[MAX_PATH];
	LPITEMIDLIST lpID = SHBrowseForFolder(&bi);
	TRACE("%s\n", dir);
	if (lpID != NULL)
	{	
		if (SHGetPathFromIDList(lpID, pszDisplayName))
		{
			m_strFileLoacation.Format("%s", pszDisplayName);
		}
	}
	UpdateData(FALSE);
}

void CUserRestoreDataDlg::OnLocationFile() 
{
	// TODO: Add your control notification handler code here
	m_bDataFile = TRUE;
	GetDlgItem(IDC_FOLDER_NAME)->EnableWindow(TRUE);
	GetDlgItem(IDC_DATA_FOLDER_SETTING)->EnableWindow(TRUE);
	GetDlgItem(IDC_RACK_NAME)->EnableWindow(FALSE);
}

void CUserRestoreDataDlg::OnLocationRack() 
{
	// TODO: Add your control notification handler code here
	m_bDataFile = FALSE;	
	GetDlgItem(IDC_FOLDER_NAME)->EnableWindow(FALSE);
	GetDlgItem(IDC_DATA_FOLDER_SETTING)->EnableWindow(FALSE);
	GetDlgItem(IDC_RACK_NAME)->EnableWindow(TRUE);
}

void CUserRestoreDataDlg::SetDB(CDatabase *pDB)
{
	ASSERT(pDB);
	m_pDB = pDB;
}
