// SearchMDBSetFieldDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanal.h"
#include "SearchMDBSetFieldDlg.h"
#include "RealEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchMDBSetFieldDlg dialog


CSearchMDBSetFieldDlg::CSearchMDBSetFieldDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchMDBSetFieldDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CSearchMDBSetFieldDlg"));
	//{{AFX_DATA_INIT(CSearchMDBSetFieldDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSearchMDBSetFieldDlg::~CSearchMDBSetFieldDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CSearchMDBSetFieldDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}



void CSearchMDBSetFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchMDBSetFieldDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSearchMDBSetFieldDlg, CDialog)
	//{{AFX_MSG_MAP(CSearchMDBSetFieldDlg)
	ON_BN_CLICKED(IDC_BTN_ADD, OnBtnAdd)
	ON_BN_CLICKED(IDC_BTN_DEL, OnBtnDel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchMDBSetFieldDlg message handlers

BOOL CSearchMDBSetFieldDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitGrid();
	LoadMDBField();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSearchMDBSetFieldDlg::InitGrid()
{
	m_wndFieldGrid.SubclassDlgItem(IDC_SEARCH_FIELD_GRID, this);
	m_wndFieldGrid.Initialize();
//	m_wndFieldGrid.m_bSameColSize  = FALSE;
	m_wndFieldGrid.m_bSameRowSize  = FALSE;

	CRect rect;
	m_wndFieldGrid.GetParam()->EnableUndo(FALSE);
	
	m_wndFieldGrid.SetRowCount(0);
	m_wndFieldGrid.SetColCount(2);		//ljb
	m_wndFieldGrid.SetDefaultRowHeight(22);
	m_wndFieldGrid.GetClientRect(&rect);

	m_wndFieldGrid.SetColWidth(0		, 0			, 30);
	m_wndFieldGrid.SetColWidth(1		, 1			, 50);
	m_wndFieldGrid.SetColWidth(2		, 2			, 50);

	m_wndFieldGrid.SetStyleRange(CGXRange(0,0			),	CGXStyle().SetValue("No"));
	m_wndFieldGrid.SetStyleRange(CGXRange(0,1			),	CGXStyle().SetValue("ITEM"));
	m_wndFieldGrid.SetStyleRange(CGXRange(0,2			),	CGXStyle().SetValue("Display Name"));
	
	m_wndFieldGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndFieldGrid.GetParam()->EnableUndo(TRUE);

	
	//Static Ctrl/////////////////////////////////////
	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(0),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(2),
		CGXStyle()
  			.SetControl(GX_IDS_CTRL_EDIT)
	);

	CString strField;
	strField = "BarCode\r\nLotID\r\nTrayName\r\nStartTime\r\nLastEndTime\r\n";
	strField += "ChargeV\r\nChargeI\r\nChargeT\r\nChargeC\r\nCharge1V\r\nCharge1I\r\nCharge1T\r\nCharge1C\r\n";
	strField += "Charge2V\r\nCharge2I\r\nCharge2T\r\nCharge2C\r\nCharge3V\r\nCharge3I\r\nCharge3T\r\nCharge3C\r\n";
	strField += "Charge4V\r\nCharge4I\r\nCharge4T\r\nCharge4C\r\nCharge5V\r\nCharge5I\r\nCharge5T\r\nCharge5C\r\nCharge6V\r\nCharge6I\r\nCharge6T\r\nCharge6C\r\n";
	strField += "DisChargeV\r\nDisChargeI\r\nDisChargeT\r\nDisChargeC\r\nDisCharge1V\r\nDisCharge1I\r\nDisCharge1T\r\nDisCharge1C\r\n";
	strField += "DisCharge2V\r\nDisCharge2I\r\nDisCharge2T\r\nDisCharge2C\r\nDisCharge3V\r\nDisCharge3I\r\nDisCharge3T\r\nDisCharge3C\r\n";
	strField += "DisCharge4V\r\nDisCharge4I\r\nDisCharge4T\r\nDisCharge4C\r\nDisCharge5V\r\nDisCharge5I\r\nDisCharge5T\r\nDisCharge5C\r\nDisCharge6V\r\nDisCharge6I\r\nDisCharge6T\r\nDisCharge6C\r\n";
	strField += "RestV\r\nRestT\r\nRest1V\r\nRest1T\r\n";
	strField += "OCV\r\nOCV1\r\nOCV2\r\nOCV3\r\nOCV4\r\nOCV5\r\nOCV6\r\n";
	strField += "ACIR1\r\nACIR2\r\nACIR3\r\nACIR4\r\nACIR5\r\nACIR6\r\n";
	strField += "Voltage1\r\nVoltage2\r\nVoltage3\r\nVoltage4\r\nVoltage5\r\nVoltage6\r\n";

	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)	//GX_IDS_CTRL_COMBOBOX
		//.SetChoiceList(_T("Charge\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\n"))
		.SetChoiceList(strField)
		.SetValue(_T("BarCode"))
	);
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_EDIT)
// 		);

	m_wndFieldGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndFieldGrid, IDS_CTRL_REALEDIT));
	m_wndFieldGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndFieldGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndFieldGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);

	
// 	m_wndFieldGrid.GetParam()->SetSortRowsOnDblClk(TRUE);	
// //	m_wndFieldGrid.EnableCellTips();
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE));
// 	m_wndFieldGrid.SetScrollBarMode(1,1);
// 	
// 	m_wndFieldGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
// 	m_wndFieldGrid.Redraw();

// 	char str[12], str2[7], str3[5];
// 	sprintf(str, "%%d", 1);		
// 	sprintf(str2, "%d", 2);		//정수부
// 
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT),
// 		CGXStyle()
// 			.SetHorizontalAlignment(DT_CENTER)
// 			.SetControl(IDS_CTRL_REALEDIT)
// 			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
// 			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63"))
// 			.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
// 			.SetValue(0L)
// 			
// 	);
// 
// 	sprintf(str2, "%d", 8);		//정수부
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
// 		CGXStyle()
//   			.SetControl(IDS_CTRL_REALEDIT)
// 			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetValue(1L)
// 	);
// 	//전송 속도 기본값자 자리수 셋팅
// 	sprintf(str, "%%d", 1);		//정수부
// 	sprintf(str2, "%d", 5);		
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_TIME),
// 		CGXStyle()
// 		.SetControl(IDS_CTRL_REALEDIT)
// 		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 		.SetValue(10L)
// 	);
// 
// 	sprintf(str3, "%d", 5);		//소숫점 이하
// 	sprintf(str, "%%.%dlf", 5);		
// 	sprintf(str2, "%d", 5);		//정수부
// 
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_FACTOR),
// 		CGXStyle()
//   			.SetControl(IDS_CTRL_REALEDIT)
// 			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
// 			.SetValue(1L)
// 	);
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_OFFSET),
// 		CGXStyle()
// 		.SetControl(IDS_CTRL_REALEDIT)
// 		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
// 		.SetValue(0L)
// 	);
// 	//기본 전송 값, 기본값자 자리수 셋팅
// 	sprintf(str3, "%d", 1);		//소숫점 이하
// 	sprintf(str, "%%.%dlf", 1);		
// 	sprintf(str2, "%d", 12);		//정수부
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
// 		CGXStyle()
// 		.SetControl(IDS_CTRL_REALEDIT)
// 		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
// 		.SetValue(0L)
// 		);
// 
// 	//division 전송 값, 기본값자 자리수 셋팅
// 	sprintf(str, "%%d", 2);		
// 	sprintf(str2, "%d", 4);		//정수부
// 	
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION),
// 		CGXStyle()
// 		.SetHorizontalAlignment(DT_CENTER)
// 		.SetControl(IDS_CTRL_REALEDIT)
// 		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 		// 		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
// 		// 		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("1000"))
// 		// 		.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
// 		.SetValue(0L)
// 		);
// 
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER),
// 			CGXStyle()
// 				.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
// 				.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
// 				.SetValue(_T("Intel"))
// 		);
// 	m_wndFieldGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
// 			CGXStyle()
// 				.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
// 				.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
// 				//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\n"))
// 				.SetValue(_T("Unsigned"))
// 		);

}

void CSearchMDBSetFieldDlg::OnBtnAdd() 
{
	CRowColArray ra;
	m_wndFieldGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndFieldGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndFieldGrid.InsertRows(m_wndFieldGrid.GetRowCount()+1, 1);
	m_wndFieldGrid.Redraw();	
}

void CSearchMDBSetFieldDlg::OnBtnDel() 
{
	CRowColArray ra;
	m_wndFieldGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndFieldGrid.RemoveRows(ra[i], ra[i]);
	}	
}

BOOL CSearchMDBSetFieldDlg::LoadMDBField()
{
	CString strSQL, strTemp;
	CDatabase  db;
	try
	{
		db.OpenEx(DSN_DATABASE_NAME,0);
	}
	catch (CDBException* e)
	{
		char sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz); 
		e->Delete();
		return FALSE;
	}
	catch (CMemoryException* e)
	{
		AfxMessageBox("Select ReportCellField MemoryException");
		return FALSE;
	}
		
	strSQL="SELECT DBFieldName, DisplayName FROM ReportCellField ORDER BY DisplayNum asc";
	CRecordset rs(&db);
	try
	{
		rs.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL, CRecordset::none);
	}
	catch (CException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();
		return FALSE;
	}

//	if (rs.GetRecordCount() > 0) m_wndFieldGrid.InsertRows(m_wndFieldGrid.GetRowCount()+1, rs.GetRecordCount());
	while(1)
	{
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			ROWCOL nRow = m_wndFieldGrid.GetRowCount()+1;
			m_wndFieldGrid.InsertRows(nRow, 1);
			rs.GetFieldValue("DBFieldName", strTemp);
			m_wndFieldGrid.SetValueRange(CGXRange(nRow, 1), strTemp);
			rs.GetFieldValue("DisplayName", strTemp);
			m_wndFieldGrid.SetValueRange(CGXRange(nRow, 2), strTemp);
			rs.MoveNext();
		}
		else
			break;
			
	}
	m_wndFieldGrid.Redraw();
	// 		if(!rs.IsBOF() && !rs.IsEOF())
	rs.Close();
	db.Close();
	return TRUE;
}

void CSearchMDBSetFieldDlg::OnOK() 
{
	// TODO: Add extra validation here
	int nRow = m_wndFieldGrid.GetRowCount();
	if (nRow < 1)
	{
		AfxMessageBox(TEXT_LANG[0]); //"저장할 데이터가 없습니다."	
		return;
	}

	CString strSQL, strField, strDisplay, strUnit;
	CDatabase  db;
	try
	{
		db.OpenEx(DSN_DATABASE_NAME,0);
	}
	catch (CDBException* e)
	{
		char sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz); 
		e->Delete();
		return;
	}
	catch (CMemoryException* e)
	{
		AfxMessageBox("MemoryException");
		return ;
	}
	
	strSQL = "DELETE * FROM ReportCellField";
	db.ExecuteSQL(strSQL);

	int i;
	for (i = 1; i<= nRow ;i++ )
	{
		strField = m_wndFieldGrid.GetValueRowCol(i,1);
		strUnit = Fun_GetSaveUnit(strField);
		strDisplay = m_wndFieldGrid.GetValueRowCol(i,2);
		if (strDisplay.IsEmpty()) strDisplay=strField;

		strSQL.Format("INSERT INTO ReportCellField(DBFieldName, DisplayName, DisplayNum, Unit) VALUES ('%s','%s',%d,'%s')",
			strField,strDisplay,i,strUnit);
		db.ExecuteSQL(strSQL);
		Sleep(100);
	}
	db.Close();	

	CDialog::OnOK();
}

// [10/27/2009 KKY] - S for 단위 추가
CString CSearchMDBSetFieldDlg::Fun_GetSaveUnit(CString strField)
{
	CString strReturn;
	strReturn="";

	if (strField == "ChargeV" || strField == "Charge1V" || strField == "Charge2V" || strField == "Charge3V" || strField == "Charge4V" || strField == "Charge5V" || strField == "Charge6V"
		|| strField == "DisChargeV" || strField == "DisCharge1V" || strField == "DisCharge2V" || strField == "DisCharge3V" || strField == "DisCharge4V" || strField == "DisCharge5V" || strField == "DisCharge6V"
		|| strField == "RestV" || strField == "Rest1V" 
		|| strField == "OCV" || strField == "OCV1" || strField == "OCV2" || strField == "OCV3" || strField == "OCV4" || strField == "OCV5" || strField == "OCV6" )
	{
		strReturn = "V";
	}

	if (strField == "ChargeI" || strField == "Charge1I" || strField == "Charge2I" || strField == "Charge3I" || strField == "Charge4I" || strField == "Charge5I" || strField == "Charge6I"
		|| strField == "DisChargeI" || strField == "DisCharge1I" || strField == "DisCharge2I" || strField == "DisCharge3I" || strField == "DisCharge4I" || strField == "DisCharge5I" || strField == "DisCharge6I" )
	{
		strReturn = "A";
	}

	if (strField == "ChargeT" || strField == "Charge1T" || strField == "Charge2T" || strField == "Charge3T" || strField == "Charge4T" || strField == "Charge5T" || strField == "Charge6T"
		|| strField == "DisChargeT" || strField == "DisCharge1T" || strField == "DisCharge2T" || strField == "DisCharge3T" || strField == "DisCharge4T" || strField == "DisCharge5T" || strField == "DisCharge6T")
	{
		strReturn = "T";
	}
	if (strField == "ChargeC" || strField == "Charge1C" || strField == "Charge2C" || strField == "Charge3C" || strField == "Charge4C" || strField == "Charge5C" || strField == "Charge6C"
		|| strField == "DisChargeC" || strField == "DisCharge1C" || strField == "DisCharge2C" || strField == "DisCharge3C" || strField == "DisCharge4C" || strField == "DisCharge5C" || strField == "DisCharge6C" )
	{
		strReturn = "C";
	}

	if( strField == "Voltage1" || strField == "Voltage2" || strField == "Voltage3" || strField == "Voltage4" || strField == "Voltage5" || strField == "Voltage6" )
	{
		strReturn = "V";
	}
	return strReturn;
}
// [10/27/2009 KKY] - E