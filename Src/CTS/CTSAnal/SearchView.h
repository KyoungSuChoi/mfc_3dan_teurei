#if !defined(AFX_SEARCHVIEW_H__E40E9543_892E_4A4B_9DA1_168155DF1059__INCLUDED_)
#define AFX_SEARCHVIEW_H__E40E9543_892E_4A4B_9DA1_168155DF1059__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SearchView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSearchView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "MyGridWnd.h"
#include "CTSAnal.h"
#include "CTSAnalDoc.h"

class CSearchView : public CFormView
{
protected:
	CSearchView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSearchView)

// Form Data
public:
	void InitFileListGrid();
	BOOL InsertFormFileList(CString strFile);
	BOOL FindResultFile(CString strFolder, BOOL bFindSubFolder = TRUE );
	BOOL CompareUnitandTray( CString strResult );
	BOOL SearchFile();
	void LoadOnlineDataTree();			// Online Mode에서 Summary 데이터 저장 경로 설정
	BOOL OnGraphView( int nType );	
	
	//{{AFX_DATA(CSearchView)
	enum { IDD = IDD_SEARCH_DLG };
	CString	m_strTrayNum;
	CString	m_strUnitName;
	CLabel	m_strTotalFileNum;
	CString	m_strCurData;
	CTime	m_toTime;
	CTime	m_fromTime;
	BOOL	m_bCheckPeriod;
	CString	m_strCurDataFolder;
	//}}AFX_DATA

// Attributes
public:
	CCTSAnalDoc* GetDocument();
	CCTSAnalDoc *m_pDoc;
	CMyGridWnd m_wndFileListGrid;
	
	CString m_strSelFileName;		// Result data name
	CString m_strFolderPath;		// 결과데이터 저장 폴더 정보

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSearchView();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSearchView)
	afx_msg void OnRefresh();
	afx_msg void OnCurdataSearch();
	afx_msg void OnBtnToday();
	afx_msg void OnCurdatafolderSearch();
	afx_msg void OnUpdateOnlinedataPath(CCmdUI* pCmdUI);
	afx_msg void OnOnlinedataPath();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnDbClickedRowCol(WPARAM wParam, LPARAM lParam);	
};

#ifndef _DEBUG  // debug version in PowerFormationView.cpp
inline CCTSAnalDoc* CSearchView::GetDocument()
{ return (CCTSAnalDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHVIEW_H__E40E9543_892E_4A4B_9DA1_168155DF1059__INCLUDED_)
