#if !defined(AFX_LOGINDLG_H__8CAB0A61_042C_11D6_A021_00010282CC7B__INCLUDED_)
#define AFX_LOGINDLG_H__8CAB0A61_042C_11D6_A021_00010282CC7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogInDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogInDlg dialog

class CLogInDlg : public CDialog
{
// Construction
public:
	CLogInDlg(CWnd* pParent = NULL);   // standard constructor
	void   InitDlg();

// Dialog Data
	//{{AFX_DATA(CLogInDlg)
	enum { IDD = IDD_LOGON_DLG };
	CString	m_strLogInID;
	CString	m_strPassWord;
	BOOL	m_Remember;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogInDlg)
	afx_msg void OnOk();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGINDLG_H__8CAB0A61_042C_11D6_A021_00010282CC7B__INCLUDED_)
