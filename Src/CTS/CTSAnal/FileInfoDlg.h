#if !defined(AFX_FILEINFODLG_H__550C9201_AB44_11D4_905E_0001027DB21C__INCLUDED_)
#define AFX_FILEINFODLG_H__550C9201_AB44_11D4_905E_0001027DB21C__INCLUDED_

#include "CTSAnal.h"
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FileInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFileInfoDlg dialog
//#include "Label.h"

class CFileInfoDlg : public CDialog
{
// Construction
public:
	CFileInfoDlg(CWnd* pParent = NULL);   // standard constructor
	STR_FILE_INFO *m_pFileInfo;
// Dialog Data
	//{{AFX_DATA(CFileInfoDlg)
	enum { IDD = IDD_FILE_INFO_DLG };
	CLabel	m_strTotalTime;
	CLabel	m_strTotalCycleNo;
	CLabel	m_strTotalPointNo;
	CLabel	m_strCreateDate;
	CLabel	m_strProcedureName;
	CLabel	m_strChannelNo;
	CLabel	m_strModuleNo;
	CLabel	m_strFileSize;
	CLabel	m_strFilePathName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFileInfoDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILEINFODLG_H__550C9201_AB44_11D4_905E_0001027DB21C__INCLUDED_)
