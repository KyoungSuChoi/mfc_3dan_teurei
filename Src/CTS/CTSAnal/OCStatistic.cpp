// OCStatistic.cpp: implementation of the OCStatistic class.
//
//////////////////////////////////////////////////////////////////////
#ifdef _OBJCHART_UT_DLL
#undef AFXAPI_DATA
#define AFXAPI_DATA __based(__segname("_DATA"))
#endif

#include "ocutstd.h"
#include <math.h>
#include <float.h>

#define REFINE 4
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//@doc OCStatistic
//@mfunc OCStatistic contructor. The constructor create the data structure and 
// storage memory for the output data, and also create the data structure for the 
// input data. However, the input data memory is not created at this time, which 
// are set by the call SetData function. 
//@rdesc 
OCStatistic::OCStatistic()
{
	m_nDivision = 5;
	m_dStep = 1.0;
	m_bOwnIn = FALSE;
	m_bShowNormal = TRUE;

	SRGDoubleStructData* pD = new SRGDoubleStructData;
	pD->SetCyclic(FALSE);
	pD->SetNull(FALSE);
	pD->SetStructureSize(sizeof(double));
	m_Graph.GetGroup(0)->AddTail(pD);
	m_Graph.GetGroup(0)->SetDynamic();

	pD = new SRGDoubleStructData;
	pD->SetCyclic(FALSE);
	pD->SetNull(FALSE);
	m_pOutX = new double[2 * m_nDivision];
	pD->SetStructureSize(sizeof(double));
	pD->SetStructure(m_pOutX);
	m_Graph.GetGroup(1)->AddTail(pD);
	m_Graph.GetGroup(1)->SetDynamic();

	pD = new SRGDoubleStructData;
	pD->SetCyclic(FALSE);
	pD->SetNull(FALSE);
	m_pOutY = new double[2 * m_nDivision];
	pD->SetStructureSize(sizeof(double));
	pD->SetStructure(m_pOutY);
	m_Graph.GetGroup(2)->AddTail(pD);
	m_Graph.GetGroup(2)->SetDynamic();

	pD = new SRGDoubleStructData;
	pD->SetCyclic(FALSE);
	pD->SetNull(FALSE);
	pD->SetStructureSize(sizeof(double));
	m_pNX = new double[2 * REFINE * m_nDivision];
	pD->SetStructure(m_pNX);
	m_Graph.GetGroup(3)->AddTail(pD);
	m_Graph.GetGroup(3)->SetDynamic();

	pD = new SRGDoubleStructData;
	pD->SetCyclic(FALSE);
	pD->SetNull(FALSE);
	pD->SetStructureSize(sizeof(double));
	m_pNY = new double[2 * REFINE * m_nDivision];
	pD->SetStructure(m_pNY);
	m_Graph.GetGroup(4)->AddTail(pD);
	m_Graph.GetGroup(4)->SetDynamic();
}

//@doc OCStatistic
//@mfunc Clear the responsible memory and kill the components and the data in the graph.
//@rdesc 
OCStatistic::~OCStatistic()
{
	if( m_bOwnIn )
		delete [] ((SRGDoubleStructData*)m_Graph.GetGroup(0)->GetHead())->GetStructure();
	if( m_pOutX )
		delete [] m_pOutX;
	if( m_pOutY )
		delete [] m_pOutY;
	if( m_pNX)
		delete [] m_pNX;
	if( m_pNY )
		delete [] m_pNY;
	m_Graph.KillGraph();
}

//@doc OCStatistic
//@mfunc Clear the responsible memory and kill the components and the data in the graph
//@rdesc void 
void OCStatistic::Clear()
{
	if( m_bOwnIn )
	{
		delete [] ((SRGDoubleStructData*)m_Graph.GetGroup(0)->GetHead())->GetStructure();
		m_bOwnIn = FALSE;
	}
	if( m_pOutX )
		delete [] m_pOutX;
	m_pOutX = NULL;
	if( m_pOutY )
		delete [] m_pOutY;
	m_pOutY = NULL;
	if( m_pNX )
		delete [] m_pNX;
	m_pNX = NULL;
	if( m_pNY )
		delete [] m_pNY;
	m_pNY = NULL;
	m_Graph.KillGraph();
}

//@doc OCStatistic
//@mfunc Clear the responsible memory used by the input/output data and delete the
// data list in the graph. 
//@rdesc void 
void OCStatistic::ClearData()
{
	if( m_bOwnIn )
	{
		delete [] ((SRGDoubleStructData*)m_Graph.GetGroup(0)->GetHead())->GetStructure();
		m_bOwnIn = FALSE;
	}
	if( m_pOutX )
		delete [] m_pOutX;
	m_pOutX = NULL;
	if( m_pOutY )
		delete [] m_pOutY;
	m_pOutY = NULL;
	if( m_pNX )
		delete [] m_pNX;
	m_pNX = NULL;
	if( m_pNY )
		delete [] m_pNY;
	m_pNY = NULL;

	m_Graph.KillData();
}

void OCStatistic::InitializeData()
{
}

//@doc OCStatistic
//@mfunc Setup the component list of the internal graph. Be default, it add an
// SRGraphDisplay and SRGraphTitle component into the graph. The all use CX_PERCENT
// measurement. The display use CX_GRAPH_VBAR chart type.
//@rdesc void 
void OCStatistic::SetupComponents()
{
	m_Graph.KillComponentList();

	SRGraphDisplay* pd = new OCStatisticDisplay;
	pd->SetMeasurement(CX_PERCENT);
	pd->SetRect(-1, -1, -1, 90);
	pd->GetStyle()->SetGraphStyle(CX_GRAPH_XYSCATTERG);
	pd->GetStyle()->SetAxisStyle(CX_AXIS_XYSCATTER);
	pd->GetStyle()->SetXLCompatibility(FALSE);
	pd->GetStyle()->SetComponentFillStyle(CX_SOLID_FILL);
	pd->GetStyle()->SetRelIndex(TRUE);
	m_Graph.AddComponent(pd);

	SRGraphTitle* pt = new SRGraphTitle;
	pt->SetMeasurement(CX_PERCENT);
	pt->SetRect(-1, 90, -1 , -1);

	m_Graph.AddComponent(pt);
}

//@doc OCStatistic
//@mfunc Process the input data to find the Mean and Deviation, calculate the distribution
// of the input according to the data division and step. 
//@rdesc void 
void OCStatistic::ProcessData()
{
	int size;
	if( m_Graph.GetGroup(0)->IsDynamic())
	{
		SRGDoubleStructData* pD = (SRGDoubleStructData*)m_Graph.GetGroup(0)->GetHead();
		size = pD->GetBufferSize();
	}
	else
		size = m_Graph.GetGroup(0)->GetCount();

	if( size <= 0 )
		return;

	double x;

	// Find mean and deviation
	m_dMean = 0.0;
	m_dDev = 0.0;
	for(int i = 0; i < size; i++)
	{
		x = m_Graph.GetSafeData(i, 0)->GetValue();
		m_dMean += x;
		m_dDev += x * x;
	}
	m_dMean /= size;
	m_dDev = sqrt(m_dDev/size - m_dMean * m_dMean);

	if( m_dDev <= 1000 * DBL_EPSILON) // Too small
		m_dDev = 1000 * DBL_EPSILON;

	double *tmpBuf = new double[2 * m_nDivision];
	memset(tmpBuf, 0, sizeof(double) * 2 * m_nDivision);

	int tmp;

	for(i = 0; i < size; i++)
	{
		x = m_Graph.GetSafeData(i, 0)->GetValue();

		tmp = m_nDivision + (int)floor((x - m_dMean)/(m_dDev * m_dStep));
		if( tmp < 0 )
			tmp = 0;
		else if( tmp >= 2 * m_nDivision)
			tmp = 2 * m_nDivision - 1;
		tmpBuf[tmp] += 1;
	}

	CString s;

	if( m_Graph.GetGroup(1)->IsDynamic())
	{
		SRGraphDynamicData* pD = (SRGraphDynamicData*)m_Graph.GetGroup(1)->GetHead();
		pD->SetBufferSize(2 * m_nDivision);
	}
	if( m_Graph.GetGroup(2)->IsDynamic())
	{
		SRGraphDynamicData* pD = (SRGraphDynamicData*)m_Graph.GetGroup(2)->GetHead();
		pD->SetBufferSize(2 * m_nDivision);
	}

	if( m_bShowNormal && m_Graph.GetGroup(3)->IsDynamic())
	{
		SRGraphDynamicData* pD = (SRGraphDynamicData*)m_Graph.GetGroup(3)->GetHead();
		pD->SetBufferSize(2 * REFINE * m_nDivision);
	}
	if( m_bShowNormal && m_Graph.GetGroup(4)->IsDynamic())
	{
		SRGraphDynamicData* pD = (SRGraphDynamicData*)m_Graph.GetGroup(4)->GetHead();
		pD->SetBufferSize(2 * REFINE * m_nDivision);
	}

	for(i = 0; i < 2 * m_nDivision; i++)
	{
		m_Graph.SetValue(i, 1, m_dMean + (i - m_nDivision + 0.5) * m_dDev * m_dStep);
		
		// Here we need to set the density of the data distribution in the given interval.
		m_Graph.SetValue(i, 2, tmpBuf[i]/(size * m_dDev * m_dStep));
	}

	for(i = 0; i < 2 * m_nDivision; i++) 
	{
		for(int j = 0; j < REFINE; j++)
		{
			x = (i - m_nDivision + ( j + 0.5 )/REFINE ) * m_dDev * m_dStep;
			m_Graph.SetValue(i * REFINE + j, 3, m_dMean + x );
			// Set the density of the normal distribution
			m_Graph.SetValue(i * REFINE + j, 4, exp(- x * x / (2 * m_dDev * m_dDev) ) / (m_dDev * sqrt(TWO_PI)) );
		}
	}

	((OCStatisticDisplay*)m_Graph.GetComponent(0, IDS_SRG_DISPLAYTYPE))->SetBarWidth(m_dDev * m_dStep * 0.95);

	delete [] tmpBuf;
}

//@doc OCStatistic
//@mfunc Set a new number of divison. After this function, it is usually necessary to call 
// ProcessData() to obtain the new output.
//@rdesc void 
//@parm int | d | Half of the actual groups of the output data.
void OCStatistic::SetDataDivision(int d)
{
	ASSERT( d > 0);
	if( m_nDivision == d && m_pOutX != NULL)
		return;
	m_nDivision = d;
	if( m_pOutX )
		delete [] m_pOutX;
	if( m_pOutY)
		delete [] m_pOutY;
	if( m_pNX )
		delete [] m_pNX;
	if( m_pNY )
		delete [] m_pNY;
	m_pOutX = new double[2 * m_nDivision];
	m_pOutY = new double[2 * m_nDivision];

	((SRGDoubleStructData*)m_Graph.GetGroup(1)->GetHead())->SetBufferSize(2 * m_nDivision);
	((SRGDoubleStructData*)m_Graph.GetGroup(1)->GetHead())->SetStructure(m_pOutX);
	((SRGDoubleStructData*)m_Graph.GetGroup(2)->GetHead())->SetBufferSize(2 * m_nDivision);
	((SRGDoubleStructData*)m_Graph.GetGroup(2)->GetHead())->SetStructure(m_pOutY);

	m_pNX = new double[2 * REFINE * m_nDivision];
	m_pNY = new double[2 * REFINE * m_nDivision];
	((SRGDoubleStructData*)m_Graph.GetGroup(3)->GetHead())->SetBufferSize(2 * REFINE * m_nDivision);
	((SRGDoubleStructData*)m_Graph.GetGroup(3)->GetHead())->SetStructure(m_pNX);
	((SRGDoubleStructData*)m_Graph.GetGroup(4)->GetHead())->SetBufferSize(2 * REFINE * m_nDivision);
	((SRGDoubleStructData*)m_Graph.GetGroup(4)->GetHead())->SetStructure(m_pNY);

	SRGraphDisplay* pd = (SRGraphDisplay*)m_Graph.GetComponent(0, IDS_SRG_DISPLAYTYPE);
	if( pd == NULL )
		return;

	if( m_bShowNormal )
		pd->SetScope(-1, -1, 1, -1);
	else
		pd->SetScope(-1, -1, 1, 2);
}

//@doc OCStatistic
//@mfunc Set the distribution inteval length, the actual length is (step * m_dDev). 
// It is usually necessary to call ProcessData() to obtain the new output.
//@rdesc void 
//@parm double | step | Step length factor.
void OCStatistic::SetStep(double step)
{
	if( m_dStep == fabs(step) && m_pOutX != NULL)
		return;
	m_dStep = fabs(step);
	if( m_pOutX == NULL ) // Otherwise, it is setup already
	{
		m_pOutX = new double[2 * m_nDivision];
		((SRGDoubleStructData*)m_Graph.GetGroup(1)->GetHead())->SetBufferSize(2 * m_nDivision);
		((SRGDoubleStructData*)m_Graph.GetGroup(1)->GetHead())->SetStructure(m_pOutX);
		
		m_pOutY = new double[2 * m_nDivision];
		((SRGDoubleStructData*)m_Graph.GetGroup(2)->GetHead())->SetBufferSize(2 * m_nDivision);
		((SRGDoubleStructData*)m_Graph.GetGroup(2)->GetHead())->SetStructure(m_pOutY);
		
		m_pNX = new double[2 * REFINE * m_nDivision];
		((SRGDoubleStructData*)m_Graph.GetGroup(3)->GetHead())->SetBufferSize(2 * REFINE * m_nDivision);
		((SRGDoubleStructData*)m_Graph.GetGroup(3)->GetHead())->SetStructure(m_pNX);
		
		if( m_pNY )
			delete [] m_pNY;
		m_pNY = new double[2 * REFINE * m_nDivision];
		((SRGDoubleStructData*)m_Graph.GetGroup(4)->GetHead())->SetBufferSize(2 * REFINE * m_nDivision);
		((SRGDoubleStructData*)m_Graph.GetGroup(4)->GetHead())->SetStructure(m_pNY);

		SRGraphDisplay* pd = (SRGraphDisplay*)m_Graph.GetComponent(0, IDS_SRG_DISPLAYTYPE);
		if( pd == NULL )
			return;
		
		if( m_bShowNormal )
			pd->SetScope(-1, -1, 1, -1);
		else
			pd->SetScope(-1, -1, 1, 2);
	}
}

//@doc OCStatistic
//@mfunc Set the input data memory.
//@rdesc void 
//@parm int | size | Input array size, this number should be positive.
//@parm  double* | pIn | Pointer to the input data array memory block. It can be NULL.
// If a NULL pointer is passed in, then OCStatistic will manage the memory for the 
// input sample data according the size argument. In this case user are free of 
// memory management burden.
void OCStatistic::SetData(int size, double *pIn)
{
	ASSERT( size > 0 );
	double *p;
	if( m_bOwnIn )
	{
		delete [] ((SRGDoubleStructData*)m_Graph.GetGroup(0)->GetHead())->GetStructure();
		m_bOwnIn = FALSE;
	}
	if( pIn == NULL )
	{
		m_bOwnIn = TRUE;
		p = new double[size];
	}
	else
	{
		m_bOwnIn = FALSE;
		p = pIn;
	}
	((SRGDoubleStructData*)m_Graph.GetGroup(0)->GetHead())->SetBufferSize(size);
	((SRGDoubleStructData*)m_Graph.GetGroup(0)->GetHead())->SetStructure(p);
}

void OCStatistic::SetShowNormal(BOOL v)
{
	if( m_bShowNormal == v )
		return;
	m_bShowNormal = v;
	if(m_bShowNormal)
		((OCStatisticDisplay*)m_Graph.GetComponent(0, IDS_SRG_DISPLAYTYPE))->SetScope(-1, -1, 1, -1);
	else
		((OCStatisticDisplay*)m_Graph.GetComponent(0, IDS_SRG_DISPLAYTYPE))->SetScope(-1, -1, 1, 2);
}

BOOL OCStatistic::GetShowNormal()
{
	return m_bShowNormal;
}

