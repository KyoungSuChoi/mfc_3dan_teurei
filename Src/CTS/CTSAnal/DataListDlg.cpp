// DataListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "DataListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataListDlg dialog


CDataListDlg::CDataListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDataListDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDataListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDataListDlg::~CDataListDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CDataListDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}



void CDataListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataListDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDataListDlg, CDialog)
	//{{AFX_MSG_MAP(CDataListDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataListDlg message handlers

BOOL CDataListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitListGrid();	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CDataListDlg::InitListGrid()
{
//	FieldSelect();
 	
	m_TestGrid.SubclassDlgItem(IDC_TESTLIST, this);
	m_TestGrid.m_bSameRowSize = FALSE;
	m_TestGrid.m_bSameColSize = TRUE;
	m_TestGrid.m_bCustomWidth = FALSE;
	m_TestGrid.m_bCustomColor = FALSE;

	m_TestGrid.Initialize();
	m_TestGrid.LockUpdate();

	m_TestGrid.EnableCellTips();
	m_TestGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE).SetAutoSize(TRUE));

	m_TestGrid.SetRowCount(135);
	m_TestGrid.SetColCount(3);
	m_TestGrid.SetRowHeight(0,0,0);
	m_TestGrid.SetDefaultRowHeight(18);
	m_TestGrid.SetScrollBarMode(SB_VERT, gxnEnabled | gxnEnhanced);

	m_TestGrid.SetValueRange(CGXRange(1,1),"Data Report");

	m_TestGrid.SetStyleRange(CGXRange().SetRows(2),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));

	m_TestGrid.SetValueRange(CGXRange(132, 1), TEXT_LANG[0]); //"Ave"
	m_TestGrid.SetValueRange(CGXRange(133, 1), TEXT_LANG[1]); //"Max"
	m_TestGrid.SetValueRange(CGXRange(134, 1), TEXT_LANG[2]); //"Min"
	m_TestGrid.SetValueRange(CGXRange(135, 1), TEXT_LANG[3]); //"편차"

 	m_TestGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);

	//Row Header Setting
	m_TestGrid.SetStyleRange(CGXRange().SetRows(132, 135),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_TestGrid.SetStyleRange(CGXRange().SetCols(1),
			CGXStyle().SetEnabled(FALSE).SetTextColor(RGB(0,0,0)).SetInterior(RGB(192,192,192)));
	m_TestGrid.SetStyleRange(CGXRange().SetTable(),
		CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName(TEXT_LANG[4]))); //"굴림"
	
	m_TestGrid.SetFrozenRows(3,0);
	m_TestGrid.LockUpdate(FALSE);
	
//	DrawTestGrid();
	return TRUE;
}
