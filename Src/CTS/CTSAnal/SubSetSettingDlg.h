#if !defined(AFX_SUBSETSETTINGDLG_H__042B9D20_AA5B_11D4_905E_0001027DB21C__INCLUDED_)
#define AFX_SUBSETSETTINGDLG_H__042B9D20_AA5B_11D4_905E_0001027DB21C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SubSetSettingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSubSetSettingDlg dialog
#include "BitmapPickerCombo.h"
#include "mydefine.h"
#define MAX_LINE_NUM	7

class CSubSetSettingDlg : public CDialog
{
// Construction
public:
	CSubSetSettingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSubSetSettingDlg)
	enum { IDD = IDD_SUBSET_SETTING };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	COLORREF		m_ChColor[MAX_CHANNEL_NUM];
	STR_Y_AXIS_SET m_YAxisSet[MAX_MUTI_AXIS];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSubSetSettingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	stingray::foundation::SECWellButton m_btnChColor[MAX_CHANNEL_NUM];
	CBitmapPickerCombo	m_BitmapCombo[MAX_MUTI_AXIS];	
	CBitmap m_bitmap[MAX_LINE_NUM];

	// Generated message map functions
	//{{AFX_MSG(CSubSetSettingDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUBSETSETTINGDLG_H__042B9D20_AA5B_11D4_905E_0001027DB21C__INCLUDED_)
