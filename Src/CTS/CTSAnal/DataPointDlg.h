#if !defined(AFX_DATAPOINTDLG_H__550C9202_AB44_11D4_905E_0001027DB21C__INCLUDED_)
#define AFX_DATAPOINTDLG_H__550C9202_AB44_11D4_905E_0001027DB21C__INCLUDED_

#include "CTSAnal.h"
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DataPointDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataPointDlg dialog

#define NOTREADY		0
#define RUNREADY		1
#define RUN				2
#define PAUSE			3
#define END				4
#define FAULT			5
#define CHTESTING		6
#define EMERGENCY		7

/* Step Type */
#define CHARGE		0x01
#define DISCHARGE	0x02
#define REST		0x03
#define TEMPSTOP	0x04
#define ENDTEST		0x05
#define LOOP		0x06
#define ADVCYCLE	0x07

/* Step mode type */
#define CC			0x01
#define CV			0x02
#define CP			0x03
#define CR			0x04
#define VRAMP		0x05
#define IRAMP		0x06
#define CCCV		0x07

//#include "Label.h"
#include "CTSAnalDoc.h"

class CDataPointDlg : public CDialog
{
// Construction
public:
	CImageList m_smallImageList;
	CDataPointDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDataPointDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

/*	int m_nDataPoint;
	int m_nLoadedFileNo;

*/	CString GetModeMsg(int Mode);
	CString GetTypeMsg(int Type);
	CString GetChannelStateMsg(int state);

	STR_FILE_INFO	m_pFileInfo[MAX_CHANNEL_NUM];
	CCTSAnalDoc *m_pDoc;
	int m_nDataPointIndex;

// Dialog Data
	//{{AFX_DATA(CDataPointDlg)
	enum { IDD = IDD_DATA_POINT_DLG };
	CLabel	m_strDataTitle;
	CListCtrl	m_ctrlDataList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataPointDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataPointDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAPOINTDLG_H__550C9202_AB44_11D4_905E_0001027DB21C__INCLUDED_)
