// DataPointDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "DataPointDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataPointDlg dialog


CDataPointDlg::CDataPointDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDataPointDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CDataPointDlg"));
	//{{AFX_DATA_INIT(CDataPointDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nDataPointIndex =0;
}

CDataPointDlg::~CDataPointDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CDataPointDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}



void CDataPointDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataPointDlg)
	DDX_Control(pDX, IDC_DATA_TITLE, m_strDataTitle);
	DDX_Control(pDX, IDC_DATA_LIST, m_ctrlDataList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDataPointDlg, CDialog)
	//{{AFX_MSG_MAP(CDataPointDlg)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataPointDlg message handlers

BOOL CDataPointDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CRect rect;
	m_smallImageList.Create(16, 16, TRUE, 1, 0);		//16*16 BitMap
	HICON	hIcon = ::LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_MODULE));
	m_smallImageList.Add(hIcon);

	m_ctrlDataList.SetImageList(&m_smallImageList, LVSIL_SMALL);

	m_ctrlDataList.GetClientRect(&rect);
	int nColInterval = rect.Width()/28;

	m_ctrlDataList.InsertColumn(0, _T(TEXT_LANG[0]), LVCFMT_LEFT, nColInterval*3); //"Channel"
	m_ctrlDataList.InsertColumn(1, _T(TEXT_LANG[1]), LVCFMT_LEFT, nColInterval*3); //"FileName"
	m_ctrlDataList.InsertColumn(2, _T(TEXT_LANG[2]), LVCFMT_LEFT, nColInterval*3); //"State"
	m_ctrlDataList.InsertColumn(3, _T(TEXT_LANG[3]), LVCFMT_LEFT, nColInterval*2); //"Type"
	m_ctrlDataList.InsertColumn(4, _T(TEXT_LANG[4]), LVCFMT_LEFT, nColInterval*2); //"Mode"
	m_ctrlDataList.InsertColumn(5, _T(TEXT_LANG[5]), LVCFMT_LEFT, nColInterval); //"CycleNum"
	m_ctrlDataList.InsertColumn(6, _T(TEXT_LANG[6]), LVCFMT_LEFT, nColInterval*4); //"TotalTime"
	m_ctrlDataList.InsertColumn(7, _T(TEXT_LANG[7]), LVCFMT_LEFT, nColInterval); //"StepNo"
	m_ctrlDataList.InsertColumn(8, _T(TEXT_LANG[8]), LVCFMT_LEFT, nColInterval*4); //"StepTime"
	m_ctrlDataList.InsertColumn(9, _T(TEXT_LANG[9]), LVCFMT_LEFT, nColInterval*3); //"Voltage"
	m_ctrlDataList.InsertColumn(10, _T(TEXT_LANG[10]), LVCFMT_LEFT, nColInterval*3); //"Current"
	m_ctrlDataList.InsertColumn(11, _T(TEXT_LANG[11]), LVCFMT_LEFT, nColInterval*3); //"Capacity"
	m_ctrlDataList.InsertColumn(12, _T(TEXT_LANG[12]), LVCFMT_LEFT, nColInterval*3); //"DCIR"
	m_ctrlDataList.InsertColumn(13, _T(TEXT_LANG[13]), LVCFMT_LEFT, nColInterval*3); //"Temperature"
	m_ctrlDataList.InsertColumn(14, _T(TEXT_LANG[14]), LVCFMT_LEFT, nColInterval*3); //"Pressure"


	m_ctrlDataList.SetRedraw(FALSE);
	// Remove whatever style is there currently
	m_ctrlDataList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlDataList.ModifyStyle(0, LVS_REPORT/*|LVS_SORTASCENDING*/);
	m_ctrlDataList.SetRedraw(TRUE);
	m_ctrlDataList.DeleteAllItems();

	// Use the LV_ITEM structure to insert the items
	LVITEM lvi;

	CString strTemp, fileExt;
	strTemp.Format(TEXT_LANG[15], m_nDataPointIndex); //"현재 X축 %d에서의 각 항목들의 값"
	m_strDataTitle.SetText(strTemp);

	STR_GEN_DATA	structGeneralData;
	STR_CAP_DATA	structCapData;

	if(m_pDoc->m_nLoadedFileNum == 0 || m_pDoc->m_pFileInfo == NULL)	return FALSE;
	if(m_nDataPointIndex <0 || m_nDataPointIndex >= m_pDoc->m_nMaxDataPoint)	return FALSE;

	int nIndexInFile = 0;
	BYTE colorFlag;

	for(int index =0; index<m_pDoc->m_nLoadedFileNum; index++)
	{
		ZeroMemory(&structGeneralData, sizeof(STR_GEN_DATA));
		ZeroMemory(&structCapData, sizeof(STR_CAP_DATA));

		lvi.mask =  LVIF_IMAGE | LVIF_TEXT;
		lvi.iItem = index;
		
		if(m_pDoc->m_pFileInfo[index].fp != NULL)
		{
			fileExt = m_pDoc->m_pFileInfo[index].strFileName.Mid(m_pDoc->m_pFileInfo[index].strFileName.ReverseFind('.')+1);
			fileExt.MakeUpper();
			lvi.iSubItem =0;
			strTemp.Format("M%dC%d",m_pDoc->m_pFileInfo[index].stFileHeader.moduleID+1,
									m_pDoc->m_pFileInfo[index].stFileHeader.channelID+1 );
			lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
			lvi.iImage = 0;		
			m_ctrlDataList.InsertItem(&lvi);
			
			lvi.iSubItem = 1;
			lvi.pszText = (LPTSTR)(LPCTSTR)(m_pDoc->m_pFileInfo[index].strFileName);
			m_ctrlDataList.SetItem(&lvi);
				
			if(fileExt == "FMT") //rpt file
			{
				nIndexInFile = m_pDoc->m_pFileInfo[index].pnDataIndexArray[m_nDataPointIndex];
				if(nIndexInFile >0)
				{
					fseek(m_pDoc->m_pFileInfo[index].fp, sizeof(STR_RESULT_DATA)+(nIndexInFile-1)*sizeof(STR_GEN_DATA), SEEK_SET); 
					fread(&structGeneralData, sizeof(STR_GEN_DATA), 1, m_pDoc->m_pFileInfo[index].fp);
				}

				lvi.iSubItem = 2;
				strTemp = GetChannelStateMsg(structGeneralData.state);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 3;
				strTemp = m_pDoc->GetTypeMsg(structGeneralData.type, colorFlag);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem =4;
				strTemp = GetModeMsg(structGeneralData.mode);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem =5;
				strTemp.Format("%d", structGeneralData.cycleNum);
				lvi.pszText =  (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 6;
				time_t tmptime = (time_t) (structGeneralData.totalTime / 10);
				int  modtime = structGeneralData.totalTime % 10;
				CTimeSpan totalTime(tmptime);
				strTemp.Format("%s.%d", totalTime.Format("%Dd %H:%M:%S"), modtime);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);
	 			
				lvi.iSubItem = 7;
				strTemp.Format("%d", structGeneralData.stepNo);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.InsertItem(&lvi);

				lvi.iSubItem = 8;
				tmptime = (time_t) (structGeneralData.stepTime / 10);
				modtime = structGeneralData.stepTime % 10;
				CTimeSpan stepTime(tmptime);
				strTemp.Format("%s.%d", stepTime.Format("%Dd %H:%M:%S"), modtime);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 9;
				strTemp.Format("%4.3f", structGeneralData.Voltage);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 10;
				strTemp.Format("%4.1f", structGeneralData.Current);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);
				
				lvi.iSubItem = 11;
				strTemp.Format("%4.1f", structGeneralData.Capacity);
				lvi.pszText =  (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 12;
				strTemp.Format("%4.1f", structGeneralData.Impedance);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 13;
				strTemp.Format("%4.1f", structGeneralData.Temperature);
				lvi.pszText =  (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 14;
				strTemp.Format("%4.1f", structGeneralData.Pressure);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

			}
			else //if(fileExt == "CRF") 	//crf file
			{
				nIndexInFile = m_nDataPointIndex;
				if(nIndexInFile >0)
				{
					fseek(m_pDoc->m_pFileInfo[index].fp, sizeof( STR_RESULT_DATA)+nIndexInFile*sizeof(STR_CAP_DATA), SEEK_SET); 
					fread(&structCapData, sizeof(STR_CAP_DATA), 1, m_pDoc->m_pFileInfo[index].fp);
				}
				lvi.iSubItem = 2;
				strTemp = GetChannelStateMsg(structCapData.state);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 3;
				strTemp = m_pDoc->GetTypeMsg(structCapData.type, colorFlag);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 4;
				strTemp = GetModeMsg(structCapData.mode);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 5;
				strTemp.Format("%d", m_nDataPointIndex/2);
				lvi.pszText =  (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 6;
				time_t tmptime = (time_t) (structCapData.totalTime / 10);
				int  modtime = structCapData.totalTime % 10;
				CTimeSpan totalTime(tmptime);
				strTemp.Format("%s.%d", totalTime.Format("%Dd %H:%M:%S"), modtime);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);
	 			
				lvi.iSubItem = 7;
				strTemp.Format("%d", structCapData.stepNo);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.InsertItem(&lvi);

				lvi.iSubItem = 8;
				tmptime = (time_t) (structCapData.stepTime / 10);
				modtime = structCapData.stepTime % 10;
				CTimeSpan stepTime(tmptime);
				strTemp.Format("%s.%d", stepTime.Format("%Dd %H:%M:%S"), modtime);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				lvi.iSubItem = 9;
				strTemp.Format("%4.3f", structCapData.Voltage);
				lvi.pszText = (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);

				
				lvi.iSubItem =11;
				strTemp.Format("%4.1f", structCapData.Capacity);
				lvi.pszText =  (LPTSTR)(LPCTSTR)(strTemp);
				m_ctrlDataList.SetItem(&lvi);
			}
		}
		else
		{
			lvi.iSubItem = 0;
			lvi.pszText = "File Loading Error";
			lvi.iImage = 0;		// There are 8 images in the image list
			m_ctrlDataList.SetItem(&lvi);
		}
		
	}
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


CString CDataPointDlg::GetChannelStateMsg(int state)
{
	CString msg;
	switch(state)
	{
		case NOTREADY:
			msg = TEXT_LANG[16]; //"Not Ready""
			break;
		case RUNREADY:
			msg = TEXT_LANG[17]; //"Ready"
			break;
		case RUN:
			msg = TEXT_LANG[18]; //"Running"
			break;
		case PAUSE:
			msg = TEXT_LANG[19]; //"Pause"
			break;
		case END:
			msg = TEXT_LANG[20]; //"End"
			break;
		case FAULT:
			msg = TEXT_LANG[21]; //"Error"
			break;
		case CHTESTING:
			msg = TEXT_LANG[22]; //"Testing"
			break;
		default:
			msg = TEXT_LANG[23]; //"Danger"
			break;
	}
	return msg;
}

CString CDataPointDlg::GetTypeMsg(int Type)
{
	CString msg;
	switch(Type)
	{
	case 0:
		msg = "";
		break;
	case CHARGE:
		msg = TEXT_LANG[24]; //"Charge"
		break;
	case DISCHARGE:
		msg = TEXT_LANG[25]; //"Discharge"
		break;
	case REST:
		msg = TEXT_LANG[26]; //"Rest"
		break;
	case TEMPSTOP:
		msg = TEXT_LANG[19]; //"Pause"
		break;
	case ENDTEST:
		msg = TEXT_LANG[20]; //"End"
		break;
	case LOOP:
		msg = TEXT_LANG[27]; //"Loop"
		break;
	case ADVCYCLE:
		msg = TEXT_LANG[28]; //"AdvCycle"
		break;
	default:
		msg = TEXT_LANG[21]; //"Error"
		break;
	}
	return msg;
}

CString CDataPointDlg::GetModeMsg(int Mode)
{
	/* Step mode type */
	CString msg;
	switch(Mode)
	{
	case 0:
		msg = "";
		break;
	case CC:
		msg = TEXT_LANG[29]; //"CC"
		break;
	case CV:
		msg = TEXT_LANG[30]; //"CV"
		break;
	case CP:
		msg = TEXT_LANG[31]; //"CP"
		break;
	case CR:
		msg = TEXT_LANG[32]; //"CR"
		break;
	case VRAMP:
		msg = TEXT_LANG[33]; //"VRamp"
		break;
	case IRAMP:
		msg = TEXT_LANG[34]; //"IRamp"
		break;
	case CCCV:
		msg = TEXT_LANG[35]; //"CC/CV"
		break;
	default:
		msg = TEXT_LANG[21]; //"Error"
		break;
	}
	return msg;
}

void CDataPointDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	RECT r;
	::GetClientRect(m_hWnd, &r);
	
	if(::IsWindow(GetDlgItem(IDOK)->GetSafeHwnd()))
	{
		CRect rect(r.right-120, r.top+8, r.right-10, r.top+32);
		GetDlgItem(IDOK)->MoveWindow(rect, FALSE);
	} 
	
	if(::IsWindow(m_ctrlDataList.GetSafeHwnd()))
	{
		CRect rect(5, 40, r.right-10, r.bottom-5);
		m_ctrlDataList.MoveWindow(rect, FALSE);
		Invalidate();
	} 
}
