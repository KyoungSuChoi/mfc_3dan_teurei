// FieldSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "FieldSetDlg.h"
#include "MainFrm.h"
#include "CTSAnalDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFieldSetDlg dialog


CFieldSetDlg::CFieldSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFieldSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFieldSetDlg)
	//}}AFX_DATA_INIT
	m_TotCol = 0;
}


void CFieldSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFieldSetDlg)
	DDX_Check(pDX, IDC_C1, m_C1);
	DDX_Check(pDX, IDC_C2, m_C2);
	DDX_Check(pDX, IDC_D1, m_D1);
	DDX_Check(pDX, IDC_D1C1, m_D1C1);
	DDX_Check(pDX, IDC_GRADING, m_Grading);
	DDX_Check(pDX, IDC_I1, m_I1);
	DDX_Check(pDX, IDC_I2, m_I2);
	DDX_Check(pDX, IDC_OCV, m_OCV);
	DDX_Check(pDX, IDC_OUTCH, m_OutCh);
	DDX_Check(pDX, IDC_OUTDCH, m_OutDCh);
	DDX_Check(pDX, IDC_OUTIMP, m_OutImp);
	DDX_Check(pDX, IDC_PREC, m_PreC);
	DDX_Check(pDX, IDC_PREIMP, m_PreImp);
	DDX_Check(pDX, IDC_PRET, m_PreT);
	DDX_Check(pDX, IDC_FAILCODE, m_FailCode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFieldSetDlg, CDialog)
	//{{AFX_MSG_MAP(CFieldSetDlg)
	ON_BN_CLICKED(IDC_C1, OnC1)
	ON_BN_CLICKED(IDC_C2, OnC2)
	ON_BN_CLICKED(IDC_D1, OnD1)
	ON_BN_CLICKED(IDC_D1C1, OnD1c1)
	ON_BN_CLICKED(IDC_FAILCODE, OnFailcode)
	ON_BN_CLICKED(IDC_GRADING, OnGrading)
	ON_BN_CLICKED(IDC_I1, OnI1)
	ON_BN_CLICKED(IDC_I2, OnI2)
	ON_BN_CLICKED(IDC_OCV, OnOcv)
	ON_BN_CLICKED(IDC_OUTCH, OnOutch)
	ON_BN_CLICKED(IDC_OUTDCH, OnOutdch)
	ON_BN_CLICKED(IDC_OUTIMP, OnOutimp)
	ON_BN_CLICKED(IDC_PREC, OnPrec)
	ON_BN_CLICKED(IDC_PREIMP, OnPreimp)
	ON_BN_CLICKED(IDC_PRET, OnPret)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFieldSetDlg message handlers
void CFieldSetDlg::InitField()
{
	m_PreImp = FALSE;
 	m_PreC = FALSE;
 	m_PreT = FALSE;
 	m_C1 = FALSE;
 	m_I1 = FALSE;
 	m_D1 = FALSE;
  	m_D1C1 = FALSE;
  	m_C2 = FALSE;
 	m_I2 = FALSE;
 	m_OCV = FALSE;
	m_OutCh = FALSE;
	m_OutDCh = FALSE;
 	m_OutImp = FALSE;
  	m_Grading = FALSE;
  	m_FailCode = FALSE;
}
void CFieldSetDlg::OnC1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_C1)
	{
		m_pDoc->m_field[3].bDisplay = TRUE;
	}
	else
	{
		m_pDoc->m_field[3].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnC2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_C2)
	{
		m_pDoc->m_field[7].bDisplay = TRUE;
	}
	else 
	{
		m_pDoc->m_field[7].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnD1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_D1) 
	{
		m_pDoc->m_field[5].bDisplay = TRUE;
	}
	else 
	{
		m_pDoc->m_field[5].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnD1c1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_D1C1)
	{
		m_pDoc->m_field[6].bDisplay = TRUE;
	}
	else
	{
		m_pDoc->m_field[6].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnFailcode() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_FailCode)
	{
		m_pDoc->m_field[14].bDisplay = TRUE;
	}
	else
	{
		m_pDoc->m_field[14].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnGrading() 
{
	// TODO: Add your control notification handler code here
//	CCTSAnalDoc *pDoc = (CCTSAnalDoc*)((CMainFrame*)AfxGetMainWnd());
	UpdateData(TRUE);
	if(m_Grading) 
	{
		m_pDoc->m_field[13].bDisplay = TRUE;
	}
	else  
	{
		m_pDoc->m_field[13].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnI1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_I1)
	{
		m_pDoc->m_field[4].bDisplay = TRUE;
	}
	else   
	{
		m_pDoc->m_field[4].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnI2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_I2)
	{
		m_pDoc->m_field[8].bDisplay = TRUE;
	}
	else  
	{
		m_pDoc->m_field[8].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnOcv() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_OCV)
	{
		m_pDoc->m_field[9].bDisplay = TRUE;
	}
	else  
	{
		m_pDoc->m_field[9].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnOutch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_OutCh)
	{
		m_pDoc->m_field[10].bDisplay = TRUE;
	}
	else 
	{
		m_pDoc->m_field[10].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnOutdch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_OutDCh)
	{
		m_pDoc->m_field[11].bDisplay = TRUE;
	}
	else
	{
		m_pDoc->m_field[11].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnOutimp() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_OutImp)
	{
		m_pDoc->m_field[12].bDisplay = TRUE;
	}
	else  
	{
		m_pDoc->m_field[12].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnPrec() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_PreC)
	{
		m_pDoc->m_field[1].bDisplay = TRUE;
	}
	else 
	{
		m_pDoc->m_field[1].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnPreimp() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_PreImp)
	{
		m_pDoc->m_field[0].bDisplay = TRUE;
	}
	else 
	{
		m_pDoc->m_field[0].bDisplay = FALSE;
	}
}

void CFieldSetDlg::OnPret() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_PreT)
	{
		m_pDoc->m_field[2].bDisplay = TRUE;
	}
	else 
	{
		m_pDoc->m_field[2].bDisplay = FALSE;
	}
}

BOOL CFieldSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitField();
	int i = 0;
	for(i = 0; i < FIELDNO; i++)
	{
		if(m_pView->m_field[i].bDisplay == TRUE)  m_TotCol++;
	}
	for(i = 0; i < m_TotCol; i++)
	{
		switch(m_pView->m_field[i].ID)
		{
		case 100:
			m_PreImp = TRUE;
    		UpdateData(FALSE);
			break;
		case 101:
			m_PreC = TRUE;
    		UpdateData(FALSE);
			break;
		case 102:
			m_PreT = TRUE;
    		UpdateData(FALSE);
			break;
		case 103:
			m_C1 = TRUE;
    		UpdateData(FALSE);
			break;
		case 104:
			m_I1 = TRUE;
    		UpdateData(FALSE);
			break;
		case 105:
			m_D1 = TRUE;
    		UpdateData(FALSE);
			break;
		case 106:
			m_D1C1 = TRUE;
    		UpdateData(FALSE);
			break;
		case 107:
			m_C2 = TRUE;
    		UpdateData(FALSE);
			break;
		case 108:
			m_I2 = TRUE;
    		UpdateData(FALSE);
			break;
		case 109:
			m_OCV = TRUE;
    		UpdateData(FALSE);
			break;
		case 110:
			m_OutCh = TRUE;
	    	UpdateData(FALSE);
			break;
		case 111:
			m_OutDCh = TRUE;
    		UpdateData(FALSE);
			break;
		case 112:
			m_OutImp = TRUE;
    		UpdateData(FALSE);
			break;
		case 10000:
			m_Grading = TRUE;
    		UpdateData(FALSE);
			break;
		case 10001:
			m_FailCode = TRUE;
	    	UpdateData(FALSE);
			break;
		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
