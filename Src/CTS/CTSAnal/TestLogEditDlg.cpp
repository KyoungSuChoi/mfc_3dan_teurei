// TestLogEditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "TestLogEditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestLogEditDlg dialog


CTestLogEditDlg::CTestLogEditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestLogEditDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestLogEditDlg)
	m_strValue = _T("");
	//}}AFX_DATA_INIT
}


void CTestLogEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestLogEditDlg)
	DDX_Control(pDX, IDC_EDIT1, m_ctrlValue);
	DDX_Text(pDX, IDC_EDIT1, m_strValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestLogEditDlg, CDialog)
	//{{AFX_MSG_MAP(CTestLogEditDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestLogEditDlg message handlers

void CTestLogEditDlg::SetTitle(CString strData)
{
	m_strTitle = strData;
}

BOOL CTestLogEditDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	GetDlgItem(IDC_EDIT_TITLE)->SetWindowText(m_strTitle);
	UpdateData(FALSE);
	m_ctrlValue.SetSel(0, -1);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTestLogEditDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	CDialog::OnOK();
}
