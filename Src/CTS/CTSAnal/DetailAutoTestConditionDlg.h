#pragma once

// CDetailAutoTestConditionDlg 대화 상자입니다.
#include "MyGridWnd.h"
#include "CTSAnalDoc.h"

class CDetailAutoTestConditionDlg : public CDialog
{
	DECLARE_DYNAMIC(CDetailAutoTestConditionDlg)

public:
	CDetailAutoTestConditionDlg(CTestCondition *pTestCondition, EP_FILE_HEADER *pFileHeader, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDetailAutoTestConditionDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
	
	// 대화 상자 데이터입니다.
	enum { IDD = IDD_DETAIL_AUTOTESTCONDITION_DLG };
	
	CCTSAnalDoc *m_pDoc;
	
private:
	CLabel m_Label1;
	CLabel m_Label2;
	CLabel m_Label3;

	CFont m_Font;

	int m_nFontSize;
	CMyGridWnd m_ProtectSettingGrid;
	CMyGridWnd m_StepGrid;
	CMyGridWnd m_ContactSettingGrid;
	CMyGridWnd m_AutoSettingGrid;
	
	CTestCondition *m_pTestCondition;
	EP_FILE_HEADER *m_pFileHeader;
	
public:
	VOID InitLabel();
	VOID InitFont();
	
	VOID InitProtectSettingGrid();
	VOID InitStepGrid();
	VOID InitContactSettingGrid();
	VOID InitAutoSettingGrid();
	
	VOID DisplayStepGrid();
	VOID DisplayProtectSettingGrid();
	VOID DisplayContactSettingGrid();
	VOID DisplayAutoSettingGrid();
	
	void DisplayCondition();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();	
	CLabel m_LabelType;
	CLabel m_LabelProcess;
	float m_fDCIR_RegTemp;
	float m_fDCIR_ResistanceRate;
	UINT m_nChargeLowVCheckTime;
	UINT m_nChargeVoltageChkTime;
	CString m_strChargeFanOffChk;
};
