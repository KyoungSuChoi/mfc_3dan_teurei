#if !defined(AFX_AXISSETTINGDLG_H__D6DE49A0_A67A_11D4_905E_0001027DB21C__INCLUDED_)
#define AFX_AXISSETTINGDLG_H__D6DE49A0_A67A_11D4_905E_0001027DB21C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AxisSettingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAxisSettingDlg dialog

class CAxisSettingDlg : public CDialog
{
// Construction
public:
	virtual ~CAxisSettingDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

	int GetSelItemCount();
	BOOL m_bChangedFlag;
	STR_Y_AXIS_SET m_YAxisSet[MAX_MUTI_AXIS];
	CAxisSettingDlg(CWnd* pParent = NULL);   // standard constructor
	int m_nXItem;
// Dialog Data
	//{{AFX_DATA(CAxisSettingDlg)
	enum { IDD = IDD_AXIS_SETTING_DLG };
	BOOL	m_bYVoltage;
	BOOL	m_bYCurrent;
	BOOL	m_bYCapacity;
	BOOL	m_bYImpedance;
	BOOL	m_bYTemperature;
	BOOL	m_bYPressure;
	BOOL	m_bXData;
	BOOL	m_bXItem;

	float	m_fYVolMin;
	float	m_fYCurMin;
	float	m_fYCapMin;
	float	m_fYImpMin;
	float	m_fYTempMin;
	float	m_fYPressMin;
	float	m_fXDataMin;

	float	m_fYVolMax;
	float	m_fYCurMax;
	float	m_fYCapMax;
	float	m_fYImpMax;
	float	m_fYTempMax;
	float	m_fYPressMax;
	float	m_fXDataMax;

	CComboBox	m_ctrlYVolUnit;
	CComboBox	m_ctrlYCurUnit;
	CComboBox	m_ctrlYCapUnit;
	CComboBox	m_ctrlYImpUnit;
	CComboBox	m_ctrlYTempUnit;
	CComboBox	m_ctrlYPressUnit;

	CComboBox	m_ctrlXDataUnit;
	int			m_nXMaxReadPoint;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAxisSettingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAxisSettingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeYvolUnit();
	afx_msg void OnSelchangeYcurUnit();
	afx_msg void OnSelchangeYimpUnit();
	afx_msg void OnSelchangeYpressUnit();
	afx_msg void OnSelchangeYtempUnit();
	afx_msg void OnSelchangeYcapUnit();
	afx_msg void OnSelchangeXdataUnit();
	afx_msg void OnOk();
	afx_msg void OnChangeMaxReadePoint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AXISSETTINGDLG_H__D6DE49A0_A67A_11D4_905E_0001027DB21C__INCLUDED_)
