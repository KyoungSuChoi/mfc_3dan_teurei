#if !defined(AFX_USERRESTOREDATADLG_H__236E084E_16CE_4B35_BF0D_D44834D24C6A__INCLUDED_)
#define AFX_USERRESTOREDATADLG_H__236E084E_16CE_4B35_BF0D_D44834D24C6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserRestoreDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserRestoreDataDlg dialog

class CUserRestoreDataDlg : public CDialog
{
// Construction
public:
	void SetDB(CDatabase *pDB);
	CUserRestoreDataDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUserRestoreDataDlg)
	enum { IDD = IDD_USER_DATA_RESTORE_DLG };
	CComboBox	m_ctrlRackCombo;
	CString	m_strFileLoacation;
	CString	m_strLotNo;
	CString	m_strTrayNo;
	UINT	m_nCellNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserRestoreDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CDatabase *m_pDB;
	BOOL m_bDataFile;

	// Generated message map functions
	//{{AFX_MSG(CUserRestoreDataDlg)
	afx_msg void OnRestoreButton();
	virtual BOOL OnInitDialog();
	afx_msg void OnDataFolderSetting();
	afx_msg void OnLocationFile();
	afx_msg void OnLocationRack();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERRESTOREDATADLG_H__236E084E_16CE_4B35_BF0D_D44834D24C6A__INCLUDED_)
