// SearchMDBView.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanal.h"
#include "SearchMDBView.h"
#include "SearchMDBSetFieldDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchMDBView

IMPLEMENT_DYNCREATE(CSearchMDBView, CFormView)

CSearchMDBView::CSearchMDBView()
	: CFormView(CSearchMDBView::IDD)
{
	LanguageinitMonConfig(_T("CSearchMDBView"));
	//{{AFX_DATA_INIT(CSearchMDBView)
	m_Time_Start_S = 0;
	m_Time_Start_E = 0;
	m_Time_End_S = 0;
	m_Time_End_E = 0;
	m_strBarCode = _T("");
	m_strLotID = _T("");
	m_strTrayNum = _T("");
	m_bCheckStartDay = FALSE;
	m_bCheckEndDay = FALSE;
	//}}AFX_DATA_INIT
}

CSearchMDBView::~CSearchMDBView()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CSearchMDBView::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}



void CSearchMDBView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchMDBView)
	DDX_Control(pDX, IDC_TOTALFILE_NUM, m_strTotalFileNum);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_START_S, m_Time_Start_S);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_START_E, m_Time_Start_E);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_END_S, m_Time_End_S);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_END_E, m_Time_End_E);
	DDX_Text(pDX, IDC_BARCODE_FILTER, m_strBarCode);
	DDX_Text(pDX, IDC_LOTID_FILTER, m_strLotID);
	DDX_Text(pDX, IDC_TRAYNUM_FILTER, m_strTrayNum);
	DDX_Check(pDX, IDC_CHECK_START_DAY, m_bCheckStartDay);
	DDX_Check(pDX, IDC_CHECK_END_DAY, m_bCheckEndDay);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSearchMDBView, CFormView)
	//{{AFX_MSG_MAP(CSearchMDBView)
	ON_BN_CLICKED(IDC_BTN_SEARCH, OnBtnSearch)
	ON_BN_CLICKED(IDC_BTN_TODAY, OnBtnToday)
	ON_BN_CLICKED(IDC_BTN_TODAY2, OnBtnToday2)
	ON_BN_CLICKED(IDC_BTN_EXCEL_TRANS, OnBtnExcelTrans)
	ON_BN_CLICKED(IDC_BTN_FIELD_SET, OnBtnFieldSet)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchMDBView diagnostics

#ifdef _DEBUG
void CSearchMDBView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSearchMDBView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
CCTSAnalDoc* CSearchMDBView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSAnalDoc)));
	return (CCTSAnalDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSearchMDBView message handlers

void CSearchMDBView::OnBtnSearch() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
	if( m_strTrayNum.IsEmpty() && m_strLotID.IsEmpty() && m_strBarCode.IsEmpty() && m_bCheckStartDay==FALSE && m_bCheckEndDay==FALSE )
	{
		AfxMessageBox(TEXT_LANG[0]); //"검색 조건이 설정되어 있지 않습니다."
		return;
	}

	int nRow = m_wndDBListGrid.GetRowCount();
	if (nRow >= 1) m_wndDBListGrid.RemoveRows(1,nRow);
	SearchDataBase();

	CString strTemp;
	strTemp.Format("%d",m_wndDBListGrid.GetRowCount());
	m_strTotalFileNum.SetText(strTemp);	
}

void CSearchMDBView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	m_pDoc = GetDocument();
	m_strTotalFileNum.SetTextColor(RGB(255, 0, 0));
	
	//m_strCurDataFolder = AfxGetApp()->GetProfileString(FORMSET_REG_SECTION , "Search_DataForder", "");
	
	m_Time_Start_S = CTime::GetCurrentTime();
	m_Time_Start_E = CTime::GetCurrentTime();
	m_Time_End_S = CTime::GetCurrentTime();
	m_Time_End_E = CTime::GetCurrentTime();

	if (LoadMDBField() == FALSE)	//mdb 에서 Display 할 Item을 가져 온다
	{
		AfxMessageBox("Field Item Load Fail");
		return;
	}
	InitListGrid();	 // Grid 초기화 
	UpdateData(FALSE);	
}

// 1. ODBC 에서 DB를 읽어오는 방식에서 DB파일을 바로 읽어 오는 방식으로 변경 ( 20120320 KKY )
BOOL CSearchMDBView::LoadMDBField()
{
	CString strSQL,strTemp, strData;
	COleVariant data;
	CString strDataBaseName;
	CDaoDatabase db;
	
	m_arryDBField.RemoveAll();
	m_arryDataUnit.RemoveAll();
	m_arryDisplayField.RemoveAll();
	try
	{
		CString strDBFolder = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");		//Get DataBase Folder
		strDataBaseName = strDBFolder+"\\"+LOG_DATABASE_NAME;

		if(strDataBaseName.IsEmpty())		return FALSE;

		db.Open(strDataBaseName);
	
		CDaoRecordset rs(&db);
		strSQL="SELECT DBFieldName,DisplayName,Unit FROM ReportCellField ORDER BY DisplayNum asc";
		try
		{
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		}
		catch (CException* e)
		{
			TCHAR sz[255];
			e->GetErrorMessage(sz,255);
			AfxMessageBox(sz);
			e->Delete();		
			return FALSE;
		}
			
		rs.MoveFirst();
		for (int iRecord=0 ; iRecord < rs.GetRecordCount() ; iRecord++)
		{
			data = rs.GetFieldValue(0);
			strData = data.pbVal;
			if( strData.IsEmpty() )
			{
				rs.MoveNext();
			}
			else
			{
				m_arryDBField.Add(strData);
			}

			data = rs.GetFieldValue(1);
			strData = data.pbVal;
			m_arryDisplayField.Add(strData);

			data = rs.GetFieldValue(2);
			strData = data.pbVal;
			m_arryDataUnit.Add(strData);
			rs.MoveNext();
		}

		rs.Close();
		db.Close();
	}
	catch (CDBException* e)
	{
		// AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
		e->Delete();
		return FALSE;
	}
	return TRUE;

	/* ODBC에서 DB를 읽어오는 방식
	CString strSQL,strTemp;
	COleVariant data;
	CString strDataBaseName;
	CDatabase  db;

	m_arryDBField.RemoveAll();
	m_arryDataUnit.RemoveAll();
	m_arryDisplayField.RemoveAll();
	try
	{
		db.OpenEx(DSN_DATABASE_NAME,0);	
		CRecordset rs(&db);

		strSQL="SELECT DBFieldName,DisplayName,Unit FROM ReportCellField ORDER BY DisplayNum asc";
		try
		{
			rs.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL, CRecordset::none);
		}
		catch (CException* e)
		{
			TCHAR sz[255];
			e->GetErrorMessage(sz,255);
			AfxMessageBox(sz);
			e->Delete();		
			return FALSE;
		}
			
		rs.MoveFirst();
		for (int iRecord=0 ; iRecord < rs.GetRecordCount() ; iRecord++)
		{
			rs.GetFieldValue("DBFieldName",strTemp);
			if (strTemp.IsEmpty()) rs.MoveNext();
			m_arryDBField.Add(strTemp);

			rs.GetFieldValue("DisplayName",strTemp);
			m_arryDisplayField.Add(strTemp);
			
			rs.GetFieldValue("Unit",strTemp);
			m_arryDataUnit.Add(strTemp);
			rs.MoveNext();			
		}

		rs.Close();
		db.Close();
	}
	catch (CDBException* e)
	{
		AfxMessageBox(e->m_strError+e->m_strStateNativeOrigin); 
		e->Delete();
		return FALSE;
	}
	return TRUE;
	*/
}

void CSearchMDBView::InitListGrid()
{
	m_wndDBListGrid.SubclassDlgItem(IDC_RESULT_FILE_LIST, this);
	//m_wndBoardListGrid.m_bRowSelection = TRUE;
	m_wndDBListGrid.m_bSameColSize  = FALSE;
	m_wndDBListGrid.m_bSameRowSize  = FALSE;
	//m_wndDBListGrid.m_bCustomWidth  = TRUE;
	
	CString strTemp;
	CRect rect;
	int i;
	m_wndDBListGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);	
	float fWidth = (float(rect.Width()-50)/m_arryDBField.GetSize());
	if (fWidth < 100) fWidth = 30.0f;

	for (i=0; i < m_arryDBField.GetSize() ; i++)
	{
		m_wndDBListGrid.m_nWidth[i+1]	= fWidth;
	}
	
	m_wndDBListGrid.Initialize();	
	m_wndDBListGrid.SetColCount(i);
	m_wndDBListGrid.SetDefaultRowHeight(22);
	m_wndDBListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndDBListGrid.m_BackColor	= RGB(255,255,255);
	m_wndDBListGrid.SetColWidth(0, 0, 50);	
	m_wndDBListGrid.m_bCustomColor 	= FALSE;
	
	// 공정날짜, 설비위치, 트레이정보, 작업이름, 양품, 불량....
	for (i=0; i < m_arryDBField.GetSize() ; i++)
	{
		strTemp = m_arryDataUnit.GetAt(i);
		if (strTemp.IsEmpty())
			m_wndDBListGrid.SetValueRange(CGXRange(0,i+1),  m_arryDisplayField.GetAt(i));
		else
		{
			if (strTemp == "V")
			{
				strTemp.Format("%s(%s)",m_arryDisplayField.GetAt(i),m_pDoc->m_strVUnit);
			}
			else if (strTemp == "A")
			{
				strTemp.Format("%s(%s)",m_arryDisplayField.GetAt(i),m_pDoc->m_strIUnit);
			}
			else if (strTemp == "T")
			{
				strTemp.Format("%s",m_arryDisplayField.GetAt(i));
			}
			else if (strTemp == "C")
			{
				strTemp.Format("%s(%s)",m_arryDisplayField.GetAt(i),m_pDoc->m_strCUnit);
			}
			m_wndDBListGrid.SetValueRange(CGXRange(0,i+1),  strTemp);
		}
	}
	
	m_wndDBListGrid.GetParam()->SetSortRowsOnDblClk(TRUE);	
	m_wndDBListGrid.EnableCellTips();
	m_wndDBListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetWrapText(FALSE));
	m_wndDBListGrid.SetScrollBarMode(1,1);
	
	m_wndDBListGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
	m_wndDBListGrid.Redraw();
}

void CSearchMDBView::SearchDataBase()
{
	CString strSQL,strTemp,strCondition;
	CDatabase  db;
	int i;

	try
	{
		db.OpenEx(DSN_DATABASE_NAME,0);
	}
	catch (CDBException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();
		return ;
	}
	catch (CMemoryException* e)
	{
		AfxMessageBox("Open CTSProduct.mdb MemoryException");
	}
		
	strSQL="SELECT ";

	for (i=0; i < m_arryDBField.GetSize()-1 ; i++)
	{
		strTemp = m_arryDBField.GetAt(i);
 		if( strTemp.Find('B') == 0 )					// BarCode 일 경우 'B'
 		{
			strTemp = "ReportCellData." + strTemp; 
  		}
		strSQL = strSQL + strTemp + ", ";
	}
	strTemp = m_arryDBField.GetAt(i);
	strSQL = strSQL + strTemp + " ";

	strTemp = "FROM ReportCellData LEFT OUTER JOIN CTSRSMeter ON ReportCellData.BarCode = CTSRSMeter.BarCode ";
	strSQL += strTemp;	
	
	strTemp = "WHERE ";
	strSQL += strTemp;

	strCondition = ""; 
	if (!m_strLotID.IsEmpty()) 
	{
		if (m_strLotID.Find('%') >= 0 ) 
			strTemp.Format("AND LotID LIKE '%s' ",m_strLotID);
		else
			strTemp.Format("AND LotID = '%s' ",m_strLotID);
		strCondition += strTemp;
	}
	if (!m_strTrayNum.IsEmpty())
	{
		if (m_strTrayNum.Find('%') >= 0)
			strTemp.Format("AND TrayName LIKE '%s' ",m_strTrayNum);
		else
			strTemp.Format("AND TrayName = '%s' ",m_strTrayNum);
		strCondition += strTemp;
	}
	if (!m_strBarCode.IsEmpty()) 
	{ 
		if (m_strBarCode.Find('%') >= 0)
			strTemp.Format("AND ReportCellData.BarCode LIKE '%s' ",m_strBarCode);
		else
			strTemp.Format("AND ReportCellData.BarCode = '%s' ",m_strBarCode);
		strCondition += strTemp;
	}
	if (m_bCheckStartDay)
	{
		CTimeSpan oneday(1,0,0,0);					//1일
		CTime endtime = m_Time_Start_E + oneday;

		//strTemp.Format("AND BETWEEN #'%s'# AND #'%s'# ",m_Time_Start_S.Format("%Y/%m/%d"), m_Time_Start_E.Format("%Y/%m/%d"));
		strTemp.Format("AND StartTime >= #%s# AND StartTime <= #%s# ",m_Time_Start_S.Format("%Y/%m/%d"),endtime.Format("%Y/%m/%d"));
		strCondition += strTemp;
	}
	if (m_bCheckEndDay)
	{
		CTimeSpan oneday(1,0,0,0);					//1일
		CTime endtime = m_Time_End_E + oneday;
		//strTemp.Format("AND BETWEEN 's' AND 's' ",m_Time_Start_S.Format("%Y/%m/%d"))
// 		if (m_Time_End_S == m_Time_End_E)
// 			strTemp.Format("AND LastEndTime = #%s# ",m_Time_End_S.Format("%Y/%m/%d"));
// 		else
			strTemp.Format("AND LastEndTime >= #%s# AND LastEndTime <= #%s# ",m_Time_End_S.Format("%Y/%m/%d"),endtime.Format("%Y/%m/%d"));
		strCondition += strTemp;
	}
	strSQL += strCondition.Right(strCondition.GetLength()-4); //AND 삭제
	strSQL += "ORDER BY ReportCellData.BarCode";
// 	AfxMessageBox(strSQL);

	CRecordset rs(&db);
	try
	{
		rs.Open( AFX_DB_USE_DEFAULT_TYPE, strSQL, CRecordset::none);
	}
	catch (CException* e)
	{
		TCHAR sz[255];
		e->GetErrorMessage(sz,255);
		AfxMessageBox(sz);
		e->Delete();
		return ;
	}
	catch (CMemoryException* e)
	{
		AfxMessageBox("Open CRecordset MemoryException");
	}
	
	int nRowCnt=1;
	while(1)
	{
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			ROWCOL nRow = m_wndDBListGrid.GetRowCount()+1;
			m_wndDBListGrid.InsertRows(nRow, 1);
			m_wndDBListGrid.Redraw();
			for (i=0; i < m_arryDBField.GetSize() ; i++)
			{
				rs.GetFieldValue(m_arryDBField.GetAt(i),strTemp);
				if (m_arryDataUnit.GetAt(i) == "A")
					m_wndDBListGrid.SetValueRange(CGXRange(nRow, i+1), m_pDoc->ValueString(atol(strTemp), EP_CURRENT));
				else if (m_arryDataUnit.GetAt(i) == "V")
					m_wndDBListGrid.SetValueRange(CGXRange(nRow, i+1), m_pDoc->ValueString(atol(strTemp), EP_VOLTAGE));
				else if (m_arryDataUnit.GetAt(i) == "T")
					m_wndDBListGrid.SetValueRange(CGXRange(nRow, i+1), m_pDoc->ValueString(atol(strTemp), EP_STEP_TIME));
				else if (m_arryDataUnit.GetAt(i) == "C")
					m_wndDBListGrid.SetValueRange(CGXRange(nRow, i+1), m_pDoc->ValueString(atol(strTemp), EP_CAPACITY));
				else
					m_wndDBListGrid.SetValueRange(CGXRange(nRow, i+1), strTemp);
			}
			nRowCnt++;
			rs.MoveNext();
		}
		else
			break;

	}
	if (nRowCnt == 1 ) AfxMessageBox(TEXT_LANG[1]); //"검색 결과가 없습니다"
	//rs.MoveFirst();
// 	for (int iRecord=0 ; iRecord < rs.GetRecordCount() ; iRecord++)
// 	{
// 		rs.GetFieldValue("DBFieldName",strTemp);
// 		if (strTemp.IsEmpty()) rs.MoveNext();
// 		m_arryDBField.Add(strTemp);
// 		rs.GetFieldValue("DisplayName",strTemp);
// 		m_arryDisplayField.Add(strTemp);
// 		rs.MoveNext();
// 	}
		
	// 		if(!rs.IsBOF() && !rs.IsEOF())
	rs.Close();
	db.Close();
}

// 					{	
// 						ROWCOL nRow = m_wndFileListGrid.GetRowCount()+1;
// 						m_wndFileListGrid.InsertRows(nRow, 1);	
// 						m_wndFileListGrid.Redraw();
// 						AfxExtractSubString(strData, strReadData, 0,',');
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 1), strData);			
// 						AfxExtractSubString(strData, strReadData, 1,',');
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 2), strData);			
// 						AfxExtractSubString(strData, strReadData, 2,',');
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 3), strData);			
// 						AfxExtractSubString(strData, strReadData, 3,',');
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 4), strData);			
// 						AfxExtractSubString(strData, strReadData, 4,',');
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 5), strData);			
// 						AfxExtractSubString(strData, strReadData, 5,',');			
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 6), strData);			
// 						AfxExtractSubString(strData, strReadData, 6,',');			
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 7), strData);									
// 						AfxExtractSubString(strData, strReadData, 7,',');
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 8), strData);			
// 						AfxExtractSubString(strData, strReadData, 8,',');
// 						m_wndFileListGrid.SetValueRange(CGXRange(nRow, 9), strData);			
// 					}


void CSearchMDBView::OnBtnToday() 
{
	// TODO: Add your control notification handler code here
	m_Time_Start_S = CTime::GetCurrentTime();
	m_Time_Start_E = CTime::GetCurrentTime();
	UpdateData(FALSE);
	
}

void CSearchMDBView::OnBtnToday2() 
{
	// TODO: Add your control notification handler code here
	m_Time_End_S = CTime::GetCurrentTime();
	m_Time_End_E = CTime::GetCurrentTime();
	UpdateData(FALSE);
}

void CSearchMDBView::OnBtnExcelTrans() 
{
	UpdateData(TRUE);
	CString strTempPath;
	CString strTemp;
	UINT iRowCnt,iColCnt,i,j=0;
	
	iRowCnt = m_wndDBListGrid.GetRowCount();	
	iColCnt = m_wndDBListGrid.GetColCount();	

	if (iRowCnt < 1)
	{
		AfxMessageBox(TEXT_LANG[2]);//"데이터가 없습니다."
		return;
	}
	CFileDialog dlg(FALSE, "csv", NULL, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "Result Data(*.csv)|*.csv|");
	dlg.m_ofn.lpstrTitle = TEXT_LANG[3];//"결과 데이타 저장"
	dlg.m_ofn.lpstrInitialDir = strTempPath;
	
	
	if( dlg.DoModal() == IDOK )
	{
		CString saveFilePath;
		saveFilePath = dlg.GetPathName();		
		
		CString strSaveFileData;
		CString strRData;
		CString strTitle;
		if( iRowCnt > 0 )
		{
			CFile file;
			if(file.Open(saveFilePath, CFile::modeWrite | CFile::modeCreate) == FALSE)
			{
				::MessageBox(NULL,TEXT_LANG[4],TEXT_LANG[5],MB_OK); //"화일이 열려있거나 쓸수 없습니다." //"알림"
				return;
			}

			for (j=1; j < iColCnt; j++)
			{
				strTitle += m_wndDBListGrid.GetValueRowCol(0,j) + ","; 
			}
			strTitle += m_wndDBListGrid.GetValueRowCol(0,j) + "\n";
			file.Write(strTitle.GetBuffer(0), strTitle.GetLength());  // Excel 파일 쓰기 

			
			for (i=1; i<=iRowCnt; i++)
			{
				strRData = __T("");
				for (j=1; j < iColCnt; j++)
				{
					strRData += m_wndDBListGrid.GetValueRowCol(i,j) + ","; 
				}
				strRData += m_wndDBListGrid.GetValueRowCol(i,j) + "\n";
				file.Write(strRData.GetBuffer(0), strRData.GetLength());  // Excel 파일 쓰기 
			}
			file.Close();

			if (::MessageBox(NULL,TEXT_LANG[6],TEXT_LANG[7],MB_YESNO) == IDYES) //"SAVE 완료. (EXCEL 파일을 Open 하시겠습니까?" //"알림"
			{
				CString strMsg = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"Excel Path", "");
				GetDocument()->ExecuteProgram(strMsg, saveFilePath);
			}
		}
	}	
	
}

void CSearchMDBView::OnBtnFieldSet() 
{
	// TODO: Add your control notification handler code here
	CSearchMDBSetFieldDlg	dlg;
	if (dlg.DoModal() == IDOK)
	{
		//Grid 다시 그리기
		if (LoadMDBField() == FALSE)	//mdb 에서 Display 할 Item을 가져 온다
		{
			AfxMessageBox("Field Item Load Fail");
			return;
		}
		RedrawListGrid();	 // Grid 초기화 
	}
}

void CSearchMDBView::RedrawListGrid()
{	
	CString strTemp;
	CRect rect;
	int i;
	m_wndDBListGrid.GetWindowRect(rect);	//Step Grid Window Position
	ScreenToClient(&rect);	
	float fWidth = (float(rect.Width()-50)/m_arryDBField.GetSize());
	if (fWidth < 100) 
		fWidth = 30.0f;
	
	for (i=0; i < m_arryDBField.GetSize() ; i++)
	{
		m_wndDBListGrid.m_nWidth[i+1]	= fWidth;
	}
	m_wndDBListGrid.SetRowCount(1);
	m_wndDBListGrid.SetColCount(i);

	// 공정날짜, 설비위치, 트레이정보, 작업이름, 양품, 불량....
	for (i=0; i < m_arryDBField.GetSize() ; i++)
	{
		strTemp = m_arryDataUnit.GetAt(i);
		if (strTemp.IsEmpty())
			m_wndDBListGrid.SetValueRange(CGXRange(0,i+1),  m_arryDisplayField.GetAt(i));
		else
		{
			if (strTemp == "V")
			{
				strTemp.Format("%s(%s)",m_arryDisplayField.GetAt(i),m_pDoc->m_strVUnit);
			}
			else if (strTemp == "A")
			{
				strTemp.Format("%s(%s)",m_arryDisplayField.GetAt(i),m_pDoc->m_strIUnit);
			}
			else if (strTemp == "T")
			{
				strTemp.Format("%s",m_arryDisplayField.GetAt(i));
			}
			else if (strTemp == "C")
			{
				strTemp.Format("%s(%s)",m_arryDisplayField.GetAt(i),m_pDoc->m_strCUnit);
			}
			m_wndDBListGrid.SetValueRange(CGXRange(0,i+1),  strTemp);
		}
	}
}
