// ModuleRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "ModuleRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleRecordSet

IMPLEMENT_DYNAMIC(CModuleRecordSet, CRecordset)

CModuleRecordSet::CModuleRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CModuleRecordSet)
	m_ProcedureID = 0;
	m_TestLogID = 0;
	m_ModuleID = 0;
	m_BoardIndex = 0;
	m_TrayNo = _T("");
	m_UserID = _T("");
	m_GroupIndex = 0;
//	m_TraySerial = _T("");
	m_TestSerialNo = _T("");
	m_State = _T("");
	m_LotNo = _T("");
	m_SystemIP = _T("");
	m_TestName = _T("");
	m_TestID = 0;
	m_TestResultFileName = _T("");
	m_ProcedureType = 0;
	m_nFields = 16;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CModuleRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=CTSProduct");
}

CString CModuleRecordSet::GetDefaultSQL()
{
	return _T("[Module]");
}

void CModuleRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CModuleRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[ProcedureID]"), m_ProcedureID);
	RFX_Long(pFX, _T("[TestLogID]"), m_TestLogID);
	RFX_Long(pFX, _T("[ModuleID]"), m_ModuleID);
	RFX_Long(pFX, _T("[BoardIndex]"), m_BoardIndex);
	RFX_Text(pFX, _T("[TrayNo]"), m_TrayNo);
	RFX_Text(pFX, _T("[UserID]"), m_UserID);
	RFX_Long(pFX, _T("[GroupIndex]"), m_GroupIndex);
	RFX_Date(pFX, _T("[DateTime]"), m_DateTime);
//	RFX_Text(pFX, _T("[TraySerial]"), m_TraySerial);
	RFX_Text(pFX, _T("[TestSerialNo]"), m_TestSerialNo);
	RFX_Text(pFX, _T("[State]"), m_State);
	RFX_Text(pFX, _T("[LotNo]"), m_LotNo);
	RFX_Text(pFX, _T("[SystemIP]"), m_SystemIP);
	RFX_Text(pFX, _T("[TestName]"), m_TestName);
	RFX_Long(pFX, _T("[TestID]"), m_TestID);
	RFX_Text(pFX, _T("[TestResultFileName]"), m_TestResultFileName);
	RFX_Long(pFX, _T("[ProcedureType]"), m_ProcedureType);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CModuleRecordSet diagnostics

#ifdef _DEBUG
void CModuleRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CModuleRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
