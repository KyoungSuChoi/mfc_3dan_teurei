#if !defined(AFX_REPORTDATALISTRECORDSET_H__2E718439_4828_4C6A_A806_5F4141D271A7__INCLUDED_)
#define AFX_REPORTDATALISTRECORDSET_H__2E718439_4828_4C6A_A806_5F4141D271A7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReportDataListRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CReportDataListRecordSet recordset

class CReportDataListRecordSet : public CRecordset
{
public:
	CReportDataListRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CReportDataListRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CReportDataListRecordSet, CRecordset)
	int		m_ProgressID;
	CString	m_Name;
	long	m_DataType;
	long	m_No;
	BOOL	m_Display;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReportDataListRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTDATALISTRECORDSET_H__2E718439_4828_4C6A_A806_5F4141D271A7__INCLUDED_)
