// AnnotationSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "AnnotationSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAnnotationSetDlg dialog


CAnnotationSetDlg::CAnnotationSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAnnotationSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAnnotationSetDlg)
	m_bAutoGrid = FALSE;
	m_bAddedGrid = FALSE;
	m_bSpecialGrid = FALSE;
	m_fAddMin = 0.0f;
	m_fAddMax = 0.0f;
	m_fAddInterval = 0.0f;
	m_fAnnoPoint = 0.0f;
	m_strAnnoComment = _T("");
	//}}AFX_DATA_INIT
	m_LineInfo.bAdded = FALSE;
	m_LineInfo.bAutoGrid = TRUE;
	m_LineInfo.bUserAddGrid = FALSE;
	m_LineInfo.colorLine = 0;
	m_LineInfo.devided = 0;
	m_LineInfo.fVal[0] = 0.0f;
	m_LineInfo.fVal[1] = 5.0f;
	m_LineInfo.fVal[2] = 0.1f;
	m_LineInfo.lineStyle = 0;
	m_LineInfo.nLineNo =0;
	m_LineInfo.select = 0;
}


void CAnnotationSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAnnotationSetDlg)
	DDX_Control(pDX, IDC_ANNO_COLOR, m_btnLineColor2);
	DDX_Control(pDX, IDC_MENUAL_COLOR, m_btnLineColor1);
	DDX_Control(pDX, IDC_ANNO_LIST, m_ctrlList);
//	DDX_Control(pDX, IDC_ANNO_LINE_STYLE, m_ctrlAnnoLineStyle);
//	DDX_Control(pDX, IDC_MANUAL_LINE_STYLE, m_ctrlAddLineStyle);
	DDX_Control(pDX, IDC_MAX_GRID_NO, m_ctrlDevied);
	DDX_Check(pDX, IDC_NORMAL_YGRID, m_bAutoGrid);
	DDX_Check(pDX, IDC_ADD_YGRID, m_bAddedGrid);
	DDX_Check(pDX, IDC_SPECIAL_ANNOTATION, m_bSpecialGrid);
	DDX_Text(pDX, IDC_MANUAL_MIN, m_fAddMin);
	DDX_Text(pDX, IDC_MANUAL_MAX, m_fAddMax);
	DDX_Text(pDX, IDC_MANUAL_INTERVAL, m_fAddInterval);
	DDX_Text(pDX, IDC_ANNO_VAL, m_fAnnoPoint);
	DDX_Text(pDX, IDC_ANNO_COMMENT, m_strAnnoComment);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAnnotationSetDlg, CDialog)
	//{{AFX_MSG_MAP(CAnnotationSetDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_ANNO_DELETE, OnAnnoDelete)
	ON_BN_CLICKED(IDC_ANNO_ADD, OnAnnoAdd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAnnotationSetDlg message handlers

BOOL CAnnotationSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	int j = 0;
	for(j =0; j<MAX_LINE_NUM; j++)
	{
		m_bitmap[j].LoadBitmap(IDB_LINE_THINSOLID_BMP+j);
	}

	m_ctrlAddLineStyle.SubclassDlgItem( IDC_MANUAL_LINE_STYLE, this);	
	m_ctrlAnnoLineStyle.SubclassDlgItem( IDC_ANNO_LINE_STYLE, this);	
	for(j =0; j<MAX_LINE_NUM; j++)
	{
		m_ctrlAddLineStyle.AddBitmap(&m_bitmap[j], "");		// combo에 bitmap 과 string 을 추가한다.
		m_ctrlAnnoLineStyle.AddBitmap(&m_bitmap[j], "");
	}
	m_ctrlAddLineStyle.SetCurSel(m_LineInfo.lineStyle);
	
	
	m_ctrlDevied.SetCurSel(m_LineInfo.devided);
	
	m_bAutoGrid = m_LineInfo.bAutoGrid;
	m_bAddedGrid = m_LineInfo.bAdded;
	m_bSpecialGrid = m_LineInfo.bUserAddGrid;
	m_fAddMin = m_LineInfo.fVal[0];
	m_fAddMax = m_LineInfo.fVal[1];
	m_fAddInterval = m_LineInfo.fVal[2];
	m_btnLineColor1.SetColor(m_LineInfo.colorLine);

	InitListCtrl();
	LVITEM lvi;
	char sztemp[16];
	for(int index =0; index<m_LineInfo.nLineNo; index++)
	{
		lvi.mask =  LVIF_TEXT;// | LVIF_STATE;
		lvi.iItem = index;

		lvi.iSubItem = 0;
		sprintf(sztemp, "%f", m_LineInfo.lineInfo[index].fVal);
		lvi.pszText = sztemp;
		m_ctrlList.InsertItem(&lvi);

		lvi.iSubItem = 1;
		lvi.pszText = m_LineInfo.lineInfo[index].szComment;
		m_ctrlList.SetItem(&lvi);

		m_ctrlList.SetItem(&lvi);
	}

	m_fAnnoPoint = 0.0f;
	m_btnLineColor2.SetColor(RGB(255,0,0));
	m_ctrlAnnoLineStyle.SetCurSel(0);
	m_strAnnoComment = "";

	if(m_LineInfo.select == 0)
	{
		((CButton *)GetDlgItem(IDC_ADD_AUTO_RADIO))->SetCheck(TRUE);
		((CButton *)GetDlgItem(IDC_ADD_MANUAL_RADIO))->SetCheck(FALSE);
	}
	else
	{
		((CButton *)GetDlgItem(IDC_ADD_AUTO_RADIO))->SetCheck(FALSE);
		((CButton *)GetDlgItem(IDC_ADD_MANUAL_RADIO))->SetCheck(TRUE);
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAnnotationSetDlg::InitListCtrl()
{
	CRect rect;
/*	CBitmap bitmap;

	m_smallImagelist.Create(16, 16, TRUE, 2, 0);		//16*16 BitMap

	bitmap.LoadBitmap(IDB_MODULE_DISCON);
	m_imageConTreelist.Add(&bitmap, RGB(255,255,255));			//BitMap Load
//	m_imagelist.Add(AfxGetApp()->LoadIcon());					//if Icon Load
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_MODULE_CONN);
	m_imageConTreelist.Add(&bitmap, RGB(255,255,255));			//BitMap Load
//	m_imagelist.Add(AfxGetApp()->LoadIcon());					//if Icon Load
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_MODULE_RUN);
	m_imageConTreelist.Add(&bitmap, RGB(255,255,255));			//BitMap Load
//	m_imagelist.Add(AfxGetApp()->LoadIcon());					//if Icon Load
	bitmap.DeleteObject();

	HICON	hIcon = ::LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_GENERAL_FILE));
	m_smallImagelist.Add(hIcon);
	hIcon = ::LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDI_CAPACITY_FILE));
	m_smallImagelist.Add(hIcon);

	// Attach them
	m_ctrlList.SetImageList(&m_smallImagelist, LVSIL_NORMAL);
	m_ctrlList.SetImageList(&m_smallImagelist, LVSIL_SMALL);
*/
	m_ctrlList.GetClientRect(&rect);
	int nColInterval = rect.Width()/3;
	m_ctrlList.InsertColumn(0, _T("Point"), LVCFMT_LEFT, nColInterval);
	m_ctrlList.InsertColumn(1, _T("Comment"), LVCFMT_LEFT, nColInterval*2);

	m_ctrlList.SetRedraw(FALSE);
	// Remove whatever style is there currently
	m_ctrlList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlList.ModifyStyle(0, LVS_REPORT/*|LVS_SORTASCENDING*/);
	m_ctrlList.SetRedraw(TRUE);

}

void CAnnotationSetDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_LineInfo.bAutoGrid = m_bAutoGrid;
	m_LineInfo.bAdded = m_bAddedGrid;
	m_LineInfo.bUserAddGrid = m_bSpecialGrid; 

	m_LineInfo.devided = (BYTE)m_ctrlDevied.GetCurSel();
	m_LineInfo.fVal[0] = m_fAddMin;
	m_LineInfo.fVal[1] = m_fAddMax;
	m_LineInfo.fVal[2] = m_fAddInterval;
	m_LineInfo.colorLine = m_btnLineColor1.GetColor();
	m_LineInfo.lineStyle = (BYTE)m_ctrlAddLineStyle.GetCurSel();

	if(((CButton *)GetDlgItem(IDC_ADD_AUTO_RADIO))->GetCheck())
	{
		m_LineInfo.select =0;
	}
	else if(((CButton *)GetDlgItem(IDC_ADD_MANUAL_RADIO))->GetCheck())
	{
		m_LineInfo.select =1;
	}
	CDialog::OnOK();
}

void CAnnotationSetDlg::OnAnnoDelete() 
{
	// TODO: Add your control notification handler code here
	int nSelected = -1;
	while((nSelected = m_ctrlList.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
	{
		m_ctrlList.DeleteItem(nSelected);
		m_LineInfo.nLineNo--;
	}
}

void CAnnotationSetDlg::OnAnnoAdd() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_LineInfo.lineInfo[m_LineInfo.nLineNo].fVal = m_fAnnoPoint;
	m_LineInfo.lineInfo[m_LineInfo.nLineNo].colorLine = m_btnLineColor2.GetColor();
	m_LineInfo.lineInfo[m_LineInfo.nLineNo].lineStyle = (BYTE)m_ctrlAnnoLineStyle.GetCurSel();
	sprintf(m_LineInfo.lineInfo[m_LineInfo.nLineNo].szComment, "%s", m_strAnnoComment);

	LVITEM lvi;
	lvi.mask =  LVIF_TEXT;// | LVIF_STATE;
	lvi.iItem = m_LineInfo.nLineNo;

	lvi.iSubItem = 0;
	char szTemp[16];
	sprintf(szTemp, "%f", m_LineInfo.lineInfo[m_LineInfo.nLineNo].fVal);
	lvi.pszText = szTemp;
	m_ctrlList.InsertItem(&lvi);


	lvi.iSubItem =1;
	lvi.pszText = m_LineInfo.lineInfo[m_LineInfo.nLineNo].szComment;
	m_ctrlList.SetItem(&lvi);
	m_LineInfo.nLineNo++;
}
