#if !defined(AFX_CHERRORDLG_H__96C10EA1_E014_11D5_A021_00010282CC7B__INCLUDED_)
#define AFX_CHERRORDLG_H__96C10EA1_E014_11D5_A021_00010282CC7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChErrorDlg.h : header file
//

#include "CTSAnalDoc.h"
#include "TestLogSet.h"
#include "ModuleRecordSet.h"
#include "TestRecordSet.h"
#include "ChValueRecordSet.h"
#include "ChResultSet.h"
#include "MyGridWnd.h"

#define  FAILCODE      1

/////////////////////////////////////////////////////////////////////////////
// CChErrorDlg dialog

class CChErrorDlg : public CDialog
{
// Construction
public:
	CChErrorDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChErrorDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
// Dialog Data
	//{{AFX_DATA(CChErrorDlg)
	enum { IDD = IDD_CHERROR_FORM };
	CComboBox	m_ctrlCodeCombo;
	CComboBox	m_ctrlModuleSelCombo;
	UINT	m_nCodeCount;
	//}}AFX_DATA
	CCTSAnalDoc     *m_pDoc;
	CMyGridWnd      m_ChannelGrid;
	CMyGridWnd      m_ErrorKindGrid;
	int		m_code[EP_MAX_CH_PER_MD][EP_MAX_PINLOG];
	stingray::foundation::SECWellButton 	m_errorColor;

public:
	void	DrawPinLogList(int nChNo);
	int		m_nTotalCh;
//	int		m_nModuleList[EP_MAX_MODULE_NUM];
	void	UpdateGridData();
	CDatabase m_dbServer;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChErrorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	void	OnPrint(CDC* pDC, CPrintInfo* pInfo);
	void	OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	void	OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	void	InitChannelGrid();
	void	InitErrorKindGrid();
    
// Implementation
protected:
	CString m_strSelModule;
	int m_nSelModule;

	// Generated message map functions
	//{{AFX_MSG(CChErrorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSearch();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeCodeSelCombo();
	afx_msg void OnPrintGrid();
	afx_msg void OnSaveFile();
	afx_msg void OnUpdateButton();
	afx_msg void OnButtonInit();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LONG OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);

	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHERRORDLG_H__96C10EA1_E014_11D5_A021_00010282CC7B__INCLUDED_)
