// FileInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "FileInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileInfoDlg dialog


CFileInfoDlg::CFileInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pFileInfo = NULL;
}


void CFileInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileInfoDlg)
	DDX_Control(pDX, IDC_TOTAL_TIME, m_strTotalTime);
	DDX_Control(pDX, IDC_CYCLE_NO, m_strTotalCycleNo);
	DDX_Control(pDX, IDC_POINT_NO, m_strTotalPointNo);
	DDX_Control(pDX, IDC_DATE, m_strCreateDate);
	DDX_Control(pDX, IDC_PROCEDURE_NAME, m_strProcedureName);
	DDX_Control(pDX, IDC_CHANNEL_NO, m_strChannelNo);
	DDX_Control(pDX, IDC_MODULE_NO, m_strModuleNo);
	DDX_Control(pDX, IDC_FILE_SIZE, m_strFileSize);
	DDX_Control(pDX, IDC_FILE_NAME, m_strFilePathName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFileInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CFileInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileInfoDlg message handlers

BOOL CFileInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString strTemp;
	long nTotTime;
	strTemp = m_pFileInfo->strFileName.Mid(m_pFileInfo->strFileName.ReverseFind('.')+1);
	strTemp.MakeUpper();
	if( strTemp == "FMT")	//일반 파일인 경우
	{	
		if(m_pFileInfo->fp)
		{
			STR_GEN_DATA stLastData;
			fseek(m_pFileInfo->fp, sizeof(STR_RESULT_DATA)+(m_pFileInfo->nDataSize-1)*sizeof(STR_GEN_DATA), SEEK_SET);																		//마지막을 찾는다.
			if(fread(&stLastData, sizeof(STR_GEN_DATA), 1, m_pFileInfo->fp) == 1)
			{
				nTotTime = stLastData.totalTime;
			}
		}
	}
	else //if(strTemp == "CRF")
	{
		if(m_pFileInfo->fp)
		{
			STR_CAP_DATA stLastData;
			fseek(m_pFileInfo->fp, sizeof(STR_RESULT_DATA)+(m_pFileInfo->nDataSize-1)*sizeof(STR_CAP_DATA), SEEK_SET);																		//마지막을 찾는다.
			if(fread(&stLastData, sizeof(STR_CAP_DATA), 1, m_pFileInfo->fp) == 1)
			{
				nTotTime = stLastData.totalTime;
			}
		}
	}

	time_t tmptime;
	int nModeTime = nTotTime%10;
	tmptime	 = (time_t)(nTotTime / 10);
	CTimeSpan totalTime(tmptime);
	strTemp.Format("%s.%d", totalTime.Format("%Dd %H:%M:%S"), nModeTime);
	m_strTotalTime.SetText(strTemp);

	fseek(m_pFileInfo->fp, 0, SEEK_END);																		//마지막을 찾는다.
	strTemp.Format("%d Byte", ftell(m_pFileInfo->fp));
	m_strFileSize.SetText(strTemp);

	strTemp.Format("%d Points", m_pFileInfo->nDataSize);
	m_strTotalPointNo.SetText(strTemp);

	m_strCreateDate.SetText(m_pFileInfo->stFileHeader.date);
	m_strProcedureName.SetText(m_pFileInfo->stFileHeader.procName);
	
	strTemp.Format("%d", m_pFileInfo->stFileHeader.channelID+1);
	m_strChannelNo.SetText(strTemp);
	
	strTemp.Format("%d", m_pFileInfo->stFileHeader.moduleID+1);
	m_strModuleNo.SetText(strTemp);

	m_strFilePathName.SetText(m_pFileInfo->strFileName);

	// TODO: Add extra initialization here
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
