// SelCellCodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "SelCellCodeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelCellCodeDlg dialog


CSelCellCodeDlg::CSelCellCodeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelCellCodeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelCellCodeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSelCellCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelCellCodeDlg)
	DDX_Control(pDX, IDC_CODE_COMBO, m_codeCombo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelCellCodeDlg, CDialog)
	//{{AFX_MSG_MAP(CSelCellCodeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelCellCodeDlg message handlers

BOOL CSelCellCodeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(m_pChCodeMsg);

	STR_MSG_DATA *pCode;

	// TODO: Add extra initialization here
	int nCount = 0;
	for(int i = 0; i<m_pChCodeMsg->GetSize(); i++)
	{
//		strQuery.Format("%s", m_pDoc->ChCodeMsg((BYTE)i));
		pCode = (STR_MSG_DATA *)m_pChCodeMsg->GetAt(i);
		if( pCode->nCode >= EP_CELL_FAIL_CODE_LOW && pCode->nCode <= EP_CELL_FAIL_CODE_HIGH
			&& EP_CODE_END_USER_STOP != pCode->nCode)	//사용자 Stop Code 있는 결과 Data는 DataBase에 보고 되지 않도록 되어있으므로 
														//사용자 Stop code를 부여 하지 못하도록 함
														// PowerFormation 의 void CMainFrame::OnSaveDataReceive(WPARAM wParam, LPARAM lParam) 에 있음 

		{
			m_codeCombo.AddString(pCode->szMessage);
			m_codeCombo.SetItemData(nCount++, pCode->nCode);
		}
	}
	m_codeCombo.SetCurSel(0);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelCellCodeDlg::OnOK() 
{
	// TODO: Add extra validation here
	int index = m_codeCombo.GetCurSel();
	if(index < 0)	return;
	m_selCode.nCode = m_codeCombo.GetItemData(index);
	m_codeCombo.GetLBText(index, m_selCode.szMessage);
	
	CDialog::OnOK();
}
