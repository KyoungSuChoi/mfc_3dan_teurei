// SensorDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "SensorDataDlg.h"
#include "PrntScreen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg dialog


CSensorDataDlg::CSensorDataDlg(CFormResultFile *pResult, CWnd* pParent /*=NULL*/)
	: CDialog(CSensorDataDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CSensorDataDlg"));
	//{{AFX_DATA_INIT(CSensorDataDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pResultFile = pResult;
	ASSERT(pResult);
	m_pDoc = NULL;
}

CSensorDataDlg::~CSensorDataDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CSensorDataDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CSensorDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX,IDC_COMBO_TEMP, m_CbTemp);
	//{{AFX_DATA_MAP(CSensorDataDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSensorDataDlg, CDialog)
	//{{AFX_MSG_MAP(CSensorDataDlg)
	ON_BN_CLICKED(IDC_BUTTON_EXCEL, OnButtonExcel)
	ON_BN_CLICKED(IDC_BUTTON_PRINT, OnButtonPrint)
	//}}AFX_MSG_MAP
//	ON_BN_CLICKED(IDCANCEL, &CSensorDataDlg::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBO_TEMP, &CSensorDataDlg::OnCbnSelchangeComboTemp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSensorDataDlg message handlers

BOOL CSensorDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	ASSERT(m_pDoc);
	m_CbTemp.SetCurSel(0);
	InitGrid();
	DisplayData();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSensorDataDlg::InitGrid()
{
	m_wndGrid.SubclassDlgItem(IDC_CUSTOM_SENSOR_GRID, this);
	//m_wndBoardListGrid.m_bRowSelection = TRUE;
	m_wndGrid.m_bSameColSize  = FALSE;
	m_wndGrid.m_bSameRowSize  = FALSE;
	m_wndGrid.m_bCustomWidth  = FALSE;

	m_wndGrid.Initialize();
	m_wndGrid.SetRowCount(0);
	m_wndGrid.SetColCount(1);
	m_wndGrid.SetFrozenCols(1);
	m_wndGrid.SetValueRange(CGXRange().SetCells(0, 0), "No");
	m_wndGrid.SetValueRange(CGXRange().SetCells(0, 1), "Step");
	m_wndGrid.SetDefaultRowHeight(18);
	m_wndGrid.SetDefaultColWidth(60);
	m_wndGrid.SetColWidth(0, 0, 40);
	m_wndGrid.SetColWidth(1, 1, 80);
	m_wndGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndGrid.m_BackColor	= RGB(255,255,255);
	m_wndGrid.m_bCustomColor 	= FALSE;

	m_wndGrid.GetParam()->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL);
}

BOOL CSensorDataDlg::DisplayData()
{
	if( m_pResultFile->GetFileVersion() != atol(RESULT_FILE_VER2) )
	{
		return FALSE;
	}


	Invalidate();
	m_wndGrid.SetRowCount(0);
	m_wndGrid.SetColCount(1);
	m_wndGrid.SetValueRange(CGXRange().SetCells(0, 0), "No");
	m_wndGrid.SetValueRange(CGXRange().SetCells(0, 1), "Step");
	m_wndGrid.SetDefaultRowHeight(18);
	m_wndGrid.SetDefaultColWidth(60);
	m_wndGrid.SetColWidth(0, 0, 40);
	m_wndGrid.SetColWidth(1, 1, 80);


	STR_STEP_RESULT *pStepResult;

	STR_TOP_CONFIG *topConfig = m_pDoc->GetTopConfig();

	ROWCOL col = m_wndGrid.GetColCount()+1;
	ROWCOL nRow = 1;

	CString strName;	
	if(m_CbTemp.GetCurSel()==0)
	{
		int c = 0;
		for(c=0; c<EP_MAX_JIGTEMP; c++)
		{
			m_wndGrid.InsertCols(col, 1);

			strName.Format("Temp %d", c+1);

			m_wndGrid.SetValueRange(CGXRange(0, col++), strName);	
		}

		BYTE colorFlag;
		
		//for(int i=0; i<m_pResultFile->GetLastStepNo(); i++)
		for(int i=0; i<m_pResultFile->GetLastIndexStepNo(); i++)  
		{
			pStepResult = m_pResultFile->GetStepData(i);
			if(pStepResult)
			{
				m_pDoc->GetTypeMsg(pStepResult->stepCondition.stepHeader.type, colorFlag);
				m_wndGrid.InsertRows(nRow, 1);
				m_wndGrid.SetValueRange(CGXRange(nRow, 0), (long)(pStepResult->stepCondition.stepHeader.stepIndex+1));
				m_wndGrid.SetValueRange(CGXRange(nRow, 1), m_pDoc->GetProcTypeName(pStepResult->stepCondition.stepHeader.nProcType, pStepResult->stepCondition.stepHeader.type));
				m_wndGrid.SetStyleRange(CGXRange(nRow, 1),
				CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));

				STR_SAVE_CH_DATA* lpChannel = (STR_SAVE_CH_DATA *)pStepResult->aChData[0];

				col = 2;
				for(c=0; c<EP_MAX_JIGTEMP; c++)			
				{	
					m_wndGrid.SetValueRange(CGXRange(nRow, col++), m_pDoc->ValueString(lpChannel->fJigTemp[c], EP_TEMPER));							
				}	
			}
			nRow++;
		}
	}
	else
	{
		int c = 0;
		for(c=0; c<EP_MAX_JIGTEMP; c++)
		{
			m_wndGrid.InsertCols(col, 1);

			strName.Format("Temp %d", c+1);

			m_wndGrid.SetValueRange(CGXRange(0, col++), strName);	
		}

		BYTE colorFlag;
		//for(int i=0; i<m_pResultFile->GetLastStepNo(); i++)
		for(int i=0; i<m_pResultFile->GetLastIndexStepNo(); i++)   
		{
			pStepResult = m_pResultFile->GetStepData(i);
			if(pStepResult)
			{
				m_pDoc->GetTypeMsg(pStepResult->stepCondition.stepHeader.type, colorFlag);
				m_wndGrid.InsertRows(nRow, 1);
				m_wndGrid.SetValueRange(CGXRange(nRow, 0), (long)(pStepResult->stepCondition.stepHeader.stepIndex+1));
				m_wndGrid.SetValueRange(CGXRange(nRow, 1), m_pDoc->GetProcTypeName(pStepResult->stepCondition.stepHeader.nProcType, pStepResult->stepCondition.stepHeader.type));
				m_wndGrid.SetStyleRange(CGXRange(nRow, 1),
					CGXStyle().SetTextColor(topConfig->m_TStateColor[colorFlag]).SetInterior(topConfig->m_BStateColor[colorFlag]));



				STR_SAVE_CH_DATA* lpChannel = (STR_SAVE_CH_DATA *)pStepResult->aChData[0];


				
					col = 2;

					for(c=0; c<EP_MAX_JIGTEMP; c++)			
					{	
						m_wndGrid.SetValueRange(CGXRange(nRow, col++), m_pDoc->ValueString(lpChannel->fAvrJigTemp[c], EP_TEMPER));

					}	
				
			}
			nRow++;
		}
	}
	
	return TRUE;
}

void CSensorDataDlg::OnButtonExcel() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;
	strFileName.Format("%s_sensor.csv", m_pResultFile->GetTestName());

	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, TEXT_LANG[0]);//"csv 파일(*.csv)|*.csv|"
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		if(strFileName.IsEmpty())	return ;

		FILE *fp = fopen(strFileName, "wt");
		if(fp == NULL)	return ;

		BeginWaitCursor();
			
		for(int i=0; i< m_wndGrid.GetRowCount()+1; i++)
		{
			for(int j=0; j< m_wndGrid.GetColCount()+1; j++)
			{
				if( j > 1)
					fprintf(fp, ",");

				fprintf(fp, "%s", m_wndGrid.GetValueRowCol(i, j));
			}
			fprintf(fp, "\n");
		}

		EndWaitCursor();
		
		fclose(fp);	
	}
}

void CSensorDataDlg::OnButtonPrint() 
{
	// TODO: Add your control notification handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;		
}

void CSensorDataDlg::SetDocumnet(CCTSAnalDoc *pDoc)
{
	m_pDoc = pDoc;
}
void CSensorDataDlg::OnCbnSelchangeComboTemp()
{


	
	if(m_CbTemp.GetCurSel()== 0)
	{		
		DisplayData();
		
	}
	else
	{	
		DisplayData();		
	}
}
