// FieldDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "FieldDlg.h"
#include "CTSAnalView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFieldDlg dialog


CFieldDlg::CFieldDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFieldDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CFieldDlg"));
	//{{AFX_DATA_INIT(CFieldDlg)
	m_bDisplayCheck = TRUE;
	//}}AFX_DATA_INIT
}

CFieldDlg::~CFieldDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CFieldDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}




void CFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFieldDlg)
	DDX_Control(pDX, IDC_LIST2, m_List2);
	DDX_Control(pDX, IDC_LIST1, m_List1);
	DDX_Check(pDX,IDC_RADIO1, m_bRadio1);
	DDX_Check(pDX,IDC_RADIO2, m_bRadio2);
	DDX_Check(pDX,IDC_RADIO3, m_bRadio3);
	DDX_Check(pDX,IDC_RADIO4, m_bRadio4);
	DDX_Check(pDX,IDC_RADIO5, m_bRadio5);
	DDX_Check(pDX, IDC_DISPLAY_CHECK, m_bDisplayCheck);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFieldDlg, CDialog)
	//{{AFX_MSG_MAP(CFieldDlg)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_DISPLAYINPUT_BTN, OnDisplayinputBtn)
	ON_BN_CLICKED(IDC_DISPLAYOUTPUT_BTN, OnDisplayoutputBtn)
	ON_BN_CLICKED(IDC_ADD_BTN, OnAddBtn)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	ON_BN_CLICKED(IDC_RADIO5, OnRadio5)
	ON_BN_CLICKED(IDC_INSERT_BTN, OnInsertBtn)
	ON_BN_CLICKED(IDC_DISPLAY_CHECK, OnDisplayCheck)
	ON_BN_CLICKED(IDC_DEL_BTN, OnDelBtn)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, OnItemchangedList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFieldDlg message handlers


BOOL CFieldDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ReportDataList.m_strFilter = "No";
	if(!m_ReportDataList.IsOpen())
	{
		try
		{
			m_ReportDataList.Open();
		}
		catch(CDBException *e)
		{
			e->Delete();
		}
	}

	CRect rect;
	GetWindowRect(rect);
	MoveWindow(rect.left, rect.top, rect.Width(), rect.Height()- 120);

	((CButton*)GetDlgItem(IDC_RADIO1))->SetCheck(1);
	m_DataType = EP_VOLTAGE;
	m_currentIndex = 0;
	m_bSize =  FALSE;

//	FieldSelect();
	InitList1();
	InitList2();
	ViewList1();
//	ViewList2();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFieldDlg::InitList1()
{
	LV_COLUMN lvc;

    lvc.mask = LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_SUBITEM;
    lvc.fmt = LVCFMT_RIGHT;

    lvc.pszText = TEXT_LANG[0].GetBuffer(); //"Field 목록"
    lvc.cx = m_List1.GetStringWidth(lvc.pszText)+143;
    lvc.iSubItem = 0;
    m_List1.InsertColumn(0,&lvc); 
}

void CFieldDlg::InitList2()
{
	LV_COLUMN lvc;

    lvc.mask = LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_SUBITEM;
    lvc.fmt = LVCFMT_RIGHT;

    lvc.pszText = TEXT_LANG[1].GetBuffer(); //"표시 목록"
    lvc.cx = m_List2.GetStringWidth(lvc.pszText)+146;
    lvc.iSubItem = 0;
    m_List2.InsertColumn(0,&lvc); 
}

void CFieldDlg::FieldSelect()
{
/*	for(int i = 0; i < FIELDNO; i++)  
	{
		m_field[i].No = 0;
		m_field[i].FieldName = "";
		m_field[i].ID = 0;
		m_field[i].DataType = 0;
		m_field[i].bDisplay = FALSE;
	}

	m_TotCol = 0;
	for( i = 0; i < m_pDoc->m_TotCol; i++)
	{
		if(m_pDoc->m_field[i].bDisplay == TRUE)
		{
   			m_field[m_TotCol].No       = m_pDoc->m_field[i].No;
    		m_field[m_TotCol].FieldName = m_pDoc->m_field[i].FieldName;
	    	m_field[m_TotCol].ID = m_pDoc->m_field[i].ID;
   			m_field[m_TotCol].DataType = m_pDoc->m_field[i].DataType;
    		m_field[m_TotCol].bDisplay = m_pDoc->m_field[i].bDisplay;
			m_TotCol++;
		}
	}
*/
}

void CFieldDlg::ViewList1()
{
	LV_ITEM lvItem;

	m_List1.DeleteAllItems(); 
	m_List2.DeleteAllItems(); 
	for(int i = 0; i <m_TotCol; i++)
	{
		lvItem.mask = LVIF_TEXT;
		lvItem.iSubItem = 0;
		lvItem.iItem = m_List1.GetItemCount() + 1;
		lvItem.pszText = (LPTSTR)(LPCTSTR)m_field[i].FieldName;
		m_List1.InsertItem(&lvItem);
	
		if(m_field[i].bDisplay)
		{
    		lvItem.mask = LVIF_TEXT;
			lvItem.iSubItem = 0;
			lvItem.iItem = m_List2.GetItemCount() + 1;
			lvItem.pszText = (LPTSTR)(LPCTSTR)m_field[i].FieldName;
			m_List2.InsertItem(&lvItem);
		}
	}
}


void CFieldDlg::ViewList2()
{
	LV_ITEM lvItem;

	m_List2.DeleteAllItems(); 
	for(int i = 0; i < m_TotCol; i++)
	{
  		if(m_field[i].bDisplay)
		{
  			lvItem.mask = LVIF_TEXT;
			lvItem.iSubItem = 0;
			lvItem.iItem = m_List2.GetItemCount() + 1;
			lvItem.pszText = (LPTSTR)(LPCTSTR)m_field[i].FieldName;
			m_List2.InsertItem(&lvItem);
		}
	}
}

BOOL CFieldDlg::Create() 
{
	return CDialog::Create(CFieldDlg::IDD);
}

void CFieldDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	if(m_ReportDataList.IsOpen())  
		m_ReportDataList.Close();

	CDialog::OnClose();
}


void CFieldDlg::OnDisplayinputBtn() 
{
	// TODO: Add your control notification handler code here
/*	LV_ITEM lvItem;

	int nIndex = m_List1.GetNextItem(-1, LVNI_SELECTED);
	if(nIndex == -1)  return;
	m_field[nIndex].bDisplay = TRUE;

//	FieldSelect();
*/
	int nItem;
	POSITION pos = m_List1.GetFirstSelectedItemPosition();
	if (pos == NULL)
	{
		TRACE0("No items were selected!\n");
	}
	else
	{
		while (pos)
		{
			nItem = m_List1.GetNextSelectedItem(pos);
			TRACE1("Item %d was selected!\n", nItem);
		  
			// you could do your own processing on nItem here
			for(int i = 0; i < m_TotCol; i++)
			{
				if(m_field[i].FieldName == m_List1.GetItemText(nItem, 0))
				{
					m_field[i].bDisplay = TRUE;
					break;
				}
			}
		}
	}
	ViewList2();
}

void CFieldDlg::OnDisplayoutputBtn() 
{
	// TODO: Add your control notification handler code here
//	int nIndex = m_List2.GetNextItem(-1, LVNI_SELECTED);
//	if(nIndex == -1)  return;

/*	for(int i = 0; i < FIELDNO; i++)
	{
		if(m_field[i].FieldName == m_field[nIndex].FieldName)
		{
			m_pDoc->m_field[i].bDisplay = FALSE;
			break;
		}
	}
*/
	
//	CListCtrl* pListCtrl = (CListCtrl*) GetDlgItem(IDC_YOURLISTCONTROL);
//	ASSERT(pListCtrl != NULL);

	int nItem;
	POSITION pos = m_List2.GetFirstSelectedItemPosition();
	if (pos == NULL)
	{
		TRACE0("No items were selected!\n");
	}
	else
	{
		while (pos)
		{
			nItem = m_List2.GetNextSelectedItem(pos);
			TRACE1("Item %d was selected!\n", nItem);
		  
			// you could do your own processing on nItem here
			for(int i = 0; i < m_TotCol; i++)
			{
				if(m_field[i].FieldName == m_List2.GetItemText(nItem, 0))
				{
					m_field[i].bDisplay = FALSE;
					break;
				}
			}
		}
	}
	
//	FieldSelect();
	ViewList2();	
}

void CFieldDlg::OnAddBtn() 
{
	// TODO: Add your control notification handler code here
	CRect rect;
	GetWindowRect(rect);

	if(!m_bSize)
	{
    	MoveWindow(rect.left, rect.top, rect.Width(), rect.Height() + 120);
		m_bSize = TRUE;
	}
	else 
	{
		MoveWindow(rect.left, rect.top, rect.Width(), rect.Height() - 120);
		m_bSize = FALSE;
	}
}

void CFieldDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
}

void CFieldDlg::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	m_DataType = EP_VOLTAGE;
}

void CFieldDlg::OnRadio2() 
{
	// TODO: Add your control notification handler code here
	m_DataType = EP_CURRENT;
}

void CFieldDlg::OnRadio3() 
{
	// TODO: Add your control notification handler code here
	m_DataType = EP_CAPACITY;
}

void CFieldDlg::OnRadio4() 
{
	// TODO: Add your control notification handler code here
	m_DataType = EP_IMPEDANCE;
}

void CFieldDlg::OnRadio5() 
{
	// TODO: Add your control notification handler code here
	m_DataType = EP_STEP_TIME;
}

void CFieldDlg::OnDisplayCheck() 
{
	// TODO: Add your control notification handler code here
	m_bDisplayCheck = !m_bDisplayCheck;
}

void CFieldDlg::OnItemchangedList1(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	m_currentIndex = m_List1.GetNextItem(-1,LVNI_SELECTED);

	*pResult = 0;
}

void CFieldDlg::OnInsertBtn() 
{
	// TODO: Add your control notification handler code here
	ASSERT(m_ReportDataList.IsOpen());

	if(m_TotCol >= FIELDNO)
	{
		MessageBox(TEXT_LANG[2], TEXT_LANG[3],  MB_ICONSTOP|MB_OK);//"입력한 Field 수가 너무 많습니다." //"입력 초과"
		return;
	}
	
	CString  str;
	GetDlgItem(IDC_EDIT1)->GetWindowText(str);
	
	m_field[m_TotCol].No = m_TotCol + 1;

	if(m_field[m_TotCol-1].ID == RPT_GRADING)
	{
    	m_field[m_TotCol].ID = m_field[m_TotCol-3].ID + 1;
	}
	else 
	{
		m_field[m_TotCol].ID = m_field[m_TotCol-1].ID + 1;
	}
	
	m_field[m_TotCol].DataType = m_DataType;
	m_field[m_TotCol].FieldName = (LPTSTR)(LPCTSTR)str;
	m_field[m_TotCol].bDisplay  = m_bDisplayCheck;
	
	m_ReportDataList.MoveLast();
	m_ReportDataList.AddNew();
	m_ReportDataList.m_No         = m_field[m_TotCol].No;
	m_ReportDataList.m_ProgressID = m_field[m_TotCol].ID;
	m_ReportDataList.m_Name       = m_field[m_TotCol].FieldName;
	m_ReportDataList.m_DataType   = m_field[m_TotCol].DataType;
	m_ReportDataList.m_Display    = m_field[m_TotCol].bDisplay;
   	m_ReportDataList.Update();

	m_TotCol++;

//	m_pDoc->LoadFieldList();
//	FieldSelect();

	ViewList1();
/*	if(m_bDisplayCheck)
	{
		ViewList2();
	}
*/
}

void CFieldDlg::OnDelBtn() 
{
	// TODO: Add your control notification handler code here
//	ASSERT(m_currentIndex >= 0 && m_currentIndex < FIELDNO);
/*	ASSERT(m_ReportDataList.IsOpen());
	
	m_ReportDataList.m_strFilter.Format("[ProgressID] = %d", m_field[m_currentIndex].ID);
	m_ReportDataList.Requery();

	if(!m_ReportDataList.IsBOF() && !m_ReportDataList.IsEOF())
	{
		m_ReportDataList.Delete();
	}

	for(int i=0; i<m_TotCol; i++)
	{
		if(i > m_currentIndex)
		{
			m_field[i-1] = m_field[i];
		}
	}
	m_TotCol--;

//	m_pDoc->LoadFieldList();
//	FieldSelect();

	ViewList1();
//	ViewList2();

*/	int nItem;
	POSITION pos = m_List1.GetFirstSelectedItemPosition();
	if (pos == NULL)
	{
		TRACE0("No items were selected!\n");
	}
	else
	{
		while (pos)
		{
			nItem = m_List1.GetNextSelectedItem(pos);
			TRACE1("Item %d was selected!\n", nItem);
		  
			m_ReportDataList.m_strFilter.Format("[ProgressID] = %d", m_field[nItem].ID);
			m_ReportDataList.Requery();
			if(!m_ReportDataList.IsBOF() && !m_ReportDataList.IsEOF())
			{
				m_ReportDataList.Delete();
			}
		}
	}

	m_TotCol = 0;
	m_ReportDataList.m_strFilter = "No";
	m_ReportDataList.Requery();
	while(!m_ReportDataList.IsEOF())
	{
		m_field[m_TotCol].No = m_ReportDataList.m_No;
		m_field[m_TotCol].ID = m_ReportDataList.m_ProgressID;
		m_field[m_TotCol].bDisplay = m_ReportDataList.m_Display;
		m_field[m_TotCol].DataType = m_ReportDataList.m_DataType;
		m_field[m_TotCol].FieldName = m_ReportDataList.m_Name;
		m_ReportDataList.MoveNext();
	}
	
//	FieldSelect();
	ViewList1();	
}
