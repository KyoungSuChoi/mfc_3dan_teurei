// ResultGraphView.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnal.h"
#include "CTSAnalDoc.h"
#include "ResultGraphView.h"

#include "AxisSettingDlg.h"
#include "SubSetSettingDlg.h"
#include "AnnotationSetDlg.h"
#include "FileInfoDlg.h"
#include "DataPointDlg.h"
#include "XAxisZoomDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CResultGraphView

IMPLEMENT_DYNCREATE(CResultGraphView, CFormView)

BEGIN_MESSAGE_MAP(CResultGraphView, CFormView)
	//{{AFX_MSG_MAP(CResultGraphView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_AXIS_SETTING, OnAxisSetting)
	ON_COMMAND(ID_SUBSET_SETTING, OnSubsetSetting)
	ON_COMMAND(ID_SPLITE_VIEW, OnSpliteView)
	ON_COMMAND(ID_GRAPH_SET, OnGraphSet)
	ON_COMMAND(ID_ZOOM_OUT, OnZoomOut)
	ON_COMMAND(ID_MAXIMIZE_VIEW, OnMaximizeView)
	ON_UPDATE_COMMAND_UI(ID_SPLITE_VIEW, OnUpdateSpliteView)
	ON_COMMAND(ID_GRID_ANNOTAION_SET, OnGridAnnotaionSet)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_WM_PAINT()
	ON_COMMAND(ID_ZOOM_IN, OnZoomIn)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResultGraphView construction/destruction

CResultGraphView::CResultGraphView()
	: CFormView(CResultGraphView::IDD)
{
	LanguageinitMonConfig(_T("CResultGraphView"));
	//{{AFX_DATA_INIT(CResultGraphView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	m_hWndPE = NULL;
	m_bSplit = FALSE;
}

CResultGraphView::~CResultGraphView()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CResultGraphView::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CResultGraphView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CResultGraphView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BOOL CResultGraphView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CResultGraphView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	ASSERT(pDoc);
	pDoc->LoadGraphSetting();

	CRect rect;
	GetClientRect(rect);
	
//	InitSlierBar(rect.right);
//	rect.top += 20;
	m_hWndPE = PEcreate(PECONTROL_GRAPH, WS_BORDER, &rect, m_hWnd, 10000);
	ASSERT(m_hWndPE);
	InitGraph();
}

/////////////////////////////////////////////////////////////////////////////
// CResultGraphView printing

BOOL CResultGraphView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CResultGraphView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CResultGraphView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CResultGraphView::OnPrint(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CResultGraphView diagnostics

#ifdef _DEBUG
void CResultGraphView::AssertValid() const
{
	CFormView::AssertValid();
}

void CResultGraphView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSAnalDoc* CResultGraphView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSAnalDoc)));
	return (CCTSAnalDoc*)m_pDocument;
}
#endif //_DEBUG



/////////////////////////////////////////////////////////////////////////////
// CResultGraphView message handlers

BOOL CResultGraphView::InitSlierBar(long lRight)
{
  	m_ctrlSlider.Create(WS_CHILD|WS_VISIBLE|WS_BORDER|TBS_AUTOTICKS|TBS_BOTTOM,		
						CRect( 0, 0, lRight, 20), this, 10002);	//Create Slider Bar
/*	m_ctrlSlider.SetRange(0, m_nXMaxReadPoint, TRUE);		//Set Slider Range
	m_ctrlSlider.SetTicFreq(m_nXMaxReadPoint/20);			//눈금간격 조절
	m_ctrlSlider.SetLineSize(m_nXMaxReadPoint/20);			//방향키에 대한 움질일 크기
	m_ctrlSlider.SetPageSize(m_nXMaxReadPoint);				//Page Up/Down key에 대한 움직일 크기
	*/
	return TRUE;
}

void CResultGraphView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	RECT r;
	::GetClientRect(m_hWnd, &r);
	if (m_hWndPE)
    {
        ::MoveWindow(m_hWndPE, 0, 0, r.right, r.bottom, FALSE);
/*		if(m_ctrlSlider.GetSafeHwnd())
		{
			::MoveWindow(m_ctrlSlider, 100, 0, r.right-100, 20, FALSE);
		}
*/  } 
}

void CResultGraphView::InitGraph()
{
	SetN(PEP_bPREPAREIMAGES, TRUE);
	SetN(PEP_bDATASHADOWS, FALSE);				//Data에 대한 그림자
	SetN(PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);	//Zoom V, H
	
	SetN(PEP_nFONTSIZE, PEFS_SMALL);					//Font size
	SetSZ(PEP_szMAINTITLEFONT, (LPSTR)(LPCTSTR)TEXT_LANG[0]); //"굴림"
	float value = 0.9f;
	PEvset(m_hWndPE, PEP_fFONTSIZEGLOBALCNTL, &value, 1);		//Font size
	value =0.7f;
	PEvset(m_hWndPE, PEP_fFONTSIZELEGENDCNTL, &value, 1);		//File 이름 Font Size
//	value =0.9f;
//	PEvset(m_hWndPE, PEP_fFONTSIZETITLECNTL, &value, 1);
	
	SetN(PEP_bALLOWDATAHOTSPOTS, TRUE);
	SetN(PEP_bALLOWSUBSETHOTSPOTS, TRUE);
	SetN(PEP_bALLOWPOINTHOTSPOTS, TRUE);
	SetN(PEP_bALLOWTABLEHOTSPOTS, TRUE);

//	PEnset(m_hWndPE, PEP_bFOCALRECT, TRUE);		//Graph에 Focus가 있도록
//	SetN(PEP_nDATAPRECISION, 3);				//테이블에서 소수점 아래 자리수 정하기
//	SetN(PEP_bALLOWBESTFITLINE, TRUE);			//근사 그래프 그릴수 있도록 한다.
//	SetN(PEP_bALLOWHISTOGRAM, TRUE);			//Histogram 사용할수 있도록

	SetN(PEP_nCURSORMODE, PECM_DATACROSS);	//
	SetN(PEP_bMOUSECURSORCONTROL, TRUE);	//
	SetN(PEP_nCURSORPROMPTSTYLE, 3);		//
	SetN(PEP_bCURSORPROMPTTRACKING, TRUE);

//	SetN(PEP_bALLOWUSERINTERFACE, TRUE);
//	SetN(PEP_bALLOWPOPUP, TRUE);
//	SetN(PEP_bALLOWCUSTOMIZATION, FALSE);
//	SetN(PEP_bALLOWMAXIMIZATION, FALSE);
//	SetN(PEP_bALLOWEXPORTING, FALSE);
	SetN(PEP_bALLOWCOORDPROMPTING, TRUE);


//	SetN(PEP_nPOINTSTOGRAPH, 1000);							//H_Scroll 시키는 XPoint 수
//	SetN(PEP_nPOINTSTOGRAPHINIT, PEPTGI_FIRSTPOINTS);
//	SetN(PEP_bNORANDOMPOINTSTOGRAPH, TRUE);
//	SetN(PEP_nFORCEVERTICALPOINTS, PEFVP_AUTO);

//	SetVCell(PEP_szaPOINTLABELS, m_TotalPoint-1, " ");
//	SetSZ(PEP_szMANUALMAXPOINTLABEL, "##########");
//	SetN(PEP_nTARGETPOINTSTOTABLE, m_ScreenPoint);
//	SetN(PEP_nALTFREQTHRESHOLD, m_ScreenPoint);
//	PEnset(m_hWndPE, PEP_nSCROLLINGSUBSETS , 1);		//동시에 보여지는 그래프 수를 나타 낸다. 

	SetN(PEP_nGRAPHPLUSTABLE, PEGPT_GRAPH);				//그래프와 테이블 동시에 
	SetN(PEP_nGRIDLINECONTROL, PEGLC_BOTH);				//Grid Line 표시 여부
//	PEnset(m_hWndPE, PEP_nGRIDSTYLE, PEGS_DOT);

//	SetN(PEP_dwDESKCOLOR, RGB(255,255,255));			//Outside of Table Color
//	SetN(PEP_dwGRAPHBACKCOLOR, RGB(255,255,255));		//Grid Back Color
	SetN(PEP_dwGRAPHFORECOLOR, RGB(192,192,192));		//Grid Line color
    SetN(PEP_bSHOWANNOTATIONS, TRUE);					//Annotation을 사용 한다.
	SetN(PEP_bALLOWANNOTATIONCONTROL, TRUE);
	char MGText[] = "###.####";
	PEszset(m_hWndPE, PEP_szRIGHTMARGIN, MGText);
	PEnset(m_hWndPE, PEP_nLINEANNOTATIONTEXTSIZE, 80);

	double NullData = 0;							
	SetV(PEP_fNULLDATAVALUE, &NullData, 1);			//0은 NULL로 간주하여 그래프를 그리지 않는다.

	SetSZ(PEP_szMAINTITLE, "Cycle Result");				//Main Title
	SetSZ(PEP_szXAXISLABEL, "Time");					//X축 Label
	SetSZ(PEP_szSUBTITLE, "");							//Sub Title

	float fData[10] = {0};	
	PEvset(m_hWndPE, PEP_faYDATA, fData, 10);			//Clear Graph

	SubSetSetting();									//1 file is Loaed Subset setting
	SetAnnotation();
//	SetN(PEP_nWORKINGAXIS, 0);
}

//*********** Functions for Graph ******************************
void CResultGraphView::SetN(UINT type, INT value)
{
	PEnset(m_hWndPE, type, value);
}

void CResultGraphView::SetV(UINT type, VOID FAR * lpData, UINT nItems)
{
	PEvset(m_hWndPE, type, lpData, nItems);
}

void CResultGraphView::SetVCell(UINT type, UINT nCell, VOID FAR * lpData)
{
	PEvsetcell(m_hWndPE, type, nCell, lpData);
}

void CResultGraphView::SetSZ(UINT type, CHAR FAR *str)
{
	PEszset(m_hWndPE, type, str);
}

void CResultGraphView::SetData(int subset, int pos, LPCTSTR xData, double yData)
{
	float data = (float) yData;
	PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, subset, pos, (void FAR*) xData);
	PEvsetcellEx(m_hWndPE, PEP_faYDATA, subset, pos, &data);
}

void CResultGraphView::SubSetSetting(int nFileNum)
{
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	int nLoadFileNumber, nSelItemNum;
	STR_Y_AXIS_SET *pYset = (STR_Y_AXIS_SET *)pDoc->GetAxisYSetting();	//Get Seleted Y Axis Setting
	ASSERT(pYset);
	nSelItemNum = pDoc->GetAxisCount();						//Get Seleted Y Axis Count

	if(nFileNum < 1)	nLoadFileNumber = 1;
	else				nLoadFileNumber = nFileNum;			//Get Loaded File Count

	SetN(PEP_nSUBSETS, nSelItemNum*nLoadFileNumber);		//Setting Subset Number(MAX 24 = 3*8), 선택한 파일 갯수 * item 수 만큼 Subset 생성	

	int mas[MAX_MUTI_AXIS];
	for(register char index = 0; index < nSelItemNum; index++)			//각 Item Y축에 Load된 파일 수만큼 SubSet 사용	
	{
		mas[index] = nLoadFileNumber;
	}					
	SetV(PEP_naMULTIAXESSUBSETS, mas, nSelItemNum);						//각 6개의 축에 보여줄 SubSet 갯수 지정(파일수/파일수/...) 
	SetV(PEP_naOVERLAPMULTIAXES, &nSelItemNum, 1);						//1 Y축에 선택한 Item Y축을 한꺼번에 표시			

	int nSubsetLineType[MAX_CHANNEL_NUM * MAX_MUTI_AXIS] = {0};			//Line Type
	COLORREF colorSubset[MAX_CHANNEL_NUM * MAX_MUTI_AXIS] = {0};		//SubSet Color
	CString strSubSetTitle;
	
	int nSubIndex = 0, nNextIndex = 0;

	for(int i=0; i <nSelItemNum; i++)
	{	
		// 현재 Display 선택된 아이템에 해당하는 색을 지정하기 위함
		for(nSubIndex = nNextIndex; nSubIndex < nSelItemNum; nSubIndex++ )
		{
			if ( pDoc->m_YSet[nSubIndex].bItemSel )
			{
				nNextIndex = nSubIndex + 1;
				break;
			}
		}
		for (int j=0; j < nLoadFileNumber; j++ )
		{
			nSubsetLineType[i*nLoadFileNumber+j] = pYset[i].nLineStyles;
			colorSubset[i*nLoadFileNumber+j] = pDoc->m_ChColor[nSubIndex];		//Channel 별 색깔이 다르게
			if(pDoc->m_pFileInfo != NULL && !pDoc->m_pFileInfo[j].strFileName.IsEmpty())
				strSubSetTitle.Format("%s(%s)", 
				pDoc->m_pFileInfo[j].strFileName.Mid(
					pDoc->m_pFileInfo[j].strFileName.ReverseFind('\\')+1), 
					pYset[i].szSubsetName
					);
			else
				strSubSetTitle.Format("%s", pYset[i].szSubsetName);
			//PEvsetcell(m_hWndPE, PEP_szaSUBSETLABELS, i*nLoadFileNumber+j, (LPSTR)(LPCTSTR)strSubSetTitle);
		}

		SetN(PEP_nWORKINGAXIS, i);										//Axis Set				
		SetAxisRange(i , pYset[i].fAxisMin, pYset[i].fAxisMax, pYset[i].nYItemUnit);
		SetSZ(PEP_szYAXISLABEL, pYset[i].szSubsetName);					//Y Axis Name
		SetN(PEP_dwYAXISCOLOR, pYset[i].axisColor);						//Y Axis Color
	}
	//Show Selected SubSet
	//ShowItemSubset(i, nLoadFileNumber*nSelItemNum, pYset[i].bItemSel);
	PEvset (m_hWndPE, PEP_naRANDOMSUBSETSTOGRAPH, nSubsetLineType, 0);		//Show All Subset
	
	SetV(PEP_naSUBSETLINETYPES, nSubsetLineType, nLoadFileNumber*nSelItemNum);	//Channel 별 Line 모양 적용
	SetV(PEP_dwaSUBSETCOLORS, colorSubset, nLoadFileNumber*nSelItemNum);		//Item별 색깔 적용

	if(m_bSplit)												//If Splite Set the splite
		SetSplit(TRUE);
	
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CResultGraphView::ShowItemSubset(int /*nItemIndex*/,  int nSubSetNo, BOOL /*bShow*/)	 //if Item is True Show Item Subset or Show channel SubSet
{
/*	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	STR_Y_AXIS_SET *pYset = (STR_Y_AXIS_SET *)pDoc->GetAxisYSetting();

	if(nItemIndex >=0 && nItemIndex < MAX_MUTI_AXIS)		//Display Item
	{
		pYset[nItemIndex].bItemSel = bShow;
		for(char j =0; j<pDoc->m_nLoadedFileNum; j++)
		{
			m_bShowArray[nItemIndex*pDoc->m_nLoadedFileNum+j] = pYset[nItemIndex].bItemSel;
		}
	}
	else		//Default Display(1 File display Setting, Voltage, Current)
	{
		memset(m_bShowArray, FALSE, sizeof(m_bShowArray));
		m_bShowArray[0] = TRUE;		//Show First File Voltage
	}
	
	int	nArray[MAX_CHANNEL_NUM * MAX_MUTI_AXIS];
	int num=0;

	for(register int i=0; i<nSubSetNo; i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
*/
	int	nArray[MAX_CHANNEL_NUM * MAX_MUTI_AXIS];
	for(register int i=0; i<nSubSetNo; i++)
	{
		nArray[i] = i;
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, nSubSetNo);
}

void CResultGraphView::DisplayData(BOOL bUpdateAll, BOOL bShowXLabel)
{
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	CString strTemp, strSubSetTitle;
	int nItemNo = pDoc->GetAxisCount();	
	SubSetSetting(pDoc->m_nLoadedFileNum);

	if(pDoc->m_pFileInfo == NULL || pDoc->m_pData == NULL)	return;

	//PEvsetcell(m_hWndPE, PEP_szaMULTISUBTITLES, i, (LPTSTR)(LPCTSTR)pDoc->m_pFileInfo[i].strFileName);
	//	PEvset(m_hWndPE, PEP_szaSUBSETLABELS, , );
	
	if(bUpdateAll)
	{
		register int i = 0;

		for(i = 0; i<pDoc->m_nLoadedFileNum; i++)	//File Name을 SubSet Title로 표시
		{
			strTemp += pDoc->m_pFileInfo[i].strFileName+"  ";
		}
		SetSZ(PEP_szSUBTITLE, (LPTSTR)(LPCTSTR)strTemp);
		
		//JSH Modify Here
		SetN(PEP_nPOINTS,  pDoc->m_nXMaxReadPoint);	//한번에 보여지는 XPoint 수->File Load시 결정 

		char szTemp[16];
		if(bShowXLabel)
		{
			time_t tmptime;

			//JSH Modify Here
			for(i=0; i<=pDoc->m_nXMaxReadPoint; i++)					//X축 Label 표시
			//for(i=1; i<=pDoc->m_nXMaxReadPoint; i++)					//X축 Label 표시
			{
	//			nModeTime = pDoc->m_pFileInfo[pDoc->m_nMaxDataFileIndex].pDataHeader[i].totalTime%10;
				tmptime	 = (time_t)(i*(pDoc->m_fSecPerPoint/10.0f));
	//			tmptime	 = (time_t)(pDoc->m_pFileInfo[pDoc->m_nMaxDataFileIndex].pDataHeader[i].totalTime / 10);
				CTimeSpan totalTime(tmptime);
				sprintf(szTemp, "%s", totalTime.Format("%Dd %H:%M:%S"));
				PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, 0, i, szTemp);//(void FAR*)(const char*)strTemp);

				if(i == pDoc->m_nXMaxReadPoint / 4 && pDoc->m_pProgressWnd)
				{
					pDoc->m_pProgressWnd->SetText(TEXT_LANG[1]); //"변환한 데이터를 화면에 출력중입니다.."
					pDoc->m_pProgressWnd->SetPos(600);
					pDoc->m_pProgressWnd->PeekAndPump(FALSE);
				}
				else if (i == pDoc->m_nXMaxReadPoint / 2 && pDoc->m_pProgressWnd)
				{
					pDoc->m_pProgressWnd->SetText(TEXT_LANG[1]); //"변환한 데이터를 화면에 출력중입니다.."
					pDoc->m_pProgressWnd->SetPos(800);
					pDoc->m_pProgressWnd->PeekAndPump(FALSE);
				}
			}
		}
		else
		{
			//JSH Modify Here
			for(i=0; i<pDoc->m_nXMaxReadPoint; i++)					//X축 Label 표시
			//for(i=0; i<=pDoc->m_nXMaxReadPoint; i++)				//X축 Label 표시
			{
				if(i%2 == 0)
				{	
					sprintf(szTemp, "%d", i/2);
					PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, 0, i, szTemp);
				}
				else
				{		
					PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, 0, i, "");
				}
			}
		}
	}

	//PEvset(m_hWndPE, PEP_faYDATA, pDoc->m_pData, (pDoc->m_nXMaxReadPoint+1)*nItemNo*pDoc->m_nLoadedFileNum);	//Display Data
	PEvset(m_hWndPE, PEP_faYDATA, pDoc->m_pData, (pDoc->m_nXMaxReadPoint)*nItemNo*pDoc->m_nLoadedFileNum);	//Display Data
	
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
	
	TRACE("Data Display Updated\n");
}


void CResultGraphView::OnCsvFileOpen() 
{
	// TODO: Add your command handler code here
	LoadFileA(TRUE);
//	DisplayData();
}

void CResultGraphView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	PEdestroy(m_hWndPE);
}

//각 축의 최대 최소값 설정
void CResultGraphView::SetAxisRange(int nItem , double dMin, double dMax, int nUnit)
{
	SetN(PEP_nWORKINGAXIS, nItem);
	if(nUnit)
	{
		SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
		SetV(PEP_fMANUALMINY, &dMin, 1);
		SetV(PEP_fMANUALMAXY, &dMax, 1);
	}
	else				//Auto Setting
	{
		SetN(PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
	}
}
void CResultGraphView::OnAxisSetting() 
{
	// TODO: Add your command handler code here
	CAxisSettingDlg dlg;
	CCTSAnalDoc	*pDoc = (CCTSAnalDoc *)GetDocument();
	memcpy(dlg.m_YAxisSet, pDoc->m_YSet, sizeof(STR_Y_AXIS_SET)*MAX_MUTI_AXIS);
	dlg.m_nXItem = pDoc->m_nXItem;
	dlg.m_nXMaxReadPoint = pDoc->m_nMaxDataPoint;
	if(IDOK == dlg.DoModal())
	{
		memcpy(pDoc->m_YSet, dlg.m_YAxisSet, sizeof(STR_Y_AXIS_SET)*MAX_MUTI_AXIS);
		pDoc->m_nXItem = dlg.m_nXItem;
		pDoc->m_nMaxDataPoint = dlg.m_nXMaxReadPoint;
		LoadFileA(FALSE);
	}
}

void CResultGraphView::OnSubsetSetting() 
{
	// TODO: Add your command handler code here
	CCTSAnalDoc	*pDoc = (CCTSAnalDoc *)GetDocument();
	CSubSetSettingDlg dlg;
	memcpy(dlg.m_ChColor, pDoc->m_ChColor, sizeof(COLORREF)*MAX_CHANNEL_NUM);
	memcpy(dlg.m_YAxisSet, pDoc->m_YSet, sizeof(STR_Y_AXIS_SET)*MAX_MUTI_AXIS);
	if(IDOK == dlg.DoModal())
	{
		memcpy(pDoc->m_ChColor, dlg.m_ChColor, sizeof(COLORREF)*MAX_CHANNEL_NUM);
		memcpy(pDoc->m_YSet, dlg.m_YAxisSet, sizeof(STR_Y_AXIS_SET)*MAX_MUTI_AXIS);
		SubSetSetting(pDoc->m_nLoadedFileNum);
	}
}

//분할 보기 설정
void CResultGraphView::SetSplit(BOOL bSplit)
{
	int oma[MAX_MUTI_AXIS];
	float map[MAX_MUTI_AXIS];
	int nSelItemNum;
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	nSelItemNum = pDoc->GetAxisCount();						//Get Seleted Y Axis Count

	if(bSplit)
	{
		float onesize = 1.0f / nSelItemNum;
		for(register char index = 0; index < nSelItemNum; index++)			//각 Item Y축에 Load된 파일 수만큼 SubSet 사용	
		{
			oma[index] = 1;
			map[index] = onesize;
		}
		SetV(PEP_naOVERLAPMULTIAXES, oma, nSelItemNum);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, nSelItemNum);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_MEDIUM);
	}
	else
	{
		oma[0] = nSelItemNum;
		map[0] = 1.0f;
		SetV(PEP_naOVERLAPMULTIAXES, oma, 1);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, 0);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_NONE);
	}

	PEreinitialize(m_hWndPE);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

//분할 보기 
void CResultGraphView::OnSpliteView() 
{
	// TODO: Add your command handler code here
	m_bSplit = !m_bSplit;
	SetSplit(m_bSplit);
}

//Graph Menu 
void CResultGraphView::OnGraphSet() 
{
	// TODO: Add your command handler code here
	CPoint point(205, -59);
	PElaunchpopupmenu(m_hWndPE, &point);
}

//Graph Zoom Out( Horizential Scroll Bar를 없엔다.)
void CResultGraphView::OnZoomOut() 
{
	// TODO: Add your command handler code here
	SetN(PEP_nPOINTSTOGRAPH, 0);			//H_Scroll 시키는 XPoint 수
//	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

//Graph Maximize View
void CResultGraphView::OnMaximizeView() 
{
	// TODO: Add your command handler code here
	PElaunchmaximize(m_hWndPE);
}

void CResultGraphView::OnUpdateSpliteView(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_bSplit);
}

//Pro Essentional Griph Notification
BOOL CResultGraphView::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	HOTSPOTDATA HSData;
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	ASSERT(pDoc);
//	STR_Y_AXIS_SET *pYset = (STR_Y_AXIS_SET *)pDoc->GetAxisYSetting();	//Get Seleted Y Axis Setting

	switch (HIWORD(wParam))
    {
        case PEWN_CLICKED:
            PEvget(m_hWndPE, PEP_structHOTSPOTDATA, &HSData);
            if (HSData.nHotSpotType == PEHS_SUBSET)		//위쪽 서브셋 클릭
            {
				if(pDoc->m_nLoadedFileNum <=0 )		break;
				int nFileindex = (HSData.w1%pDoc->m_nLoadedFileNum)%pDoc->GetAxisCount();
				if(nFileindex >=0 && nFileindex<pDoc->m_nLoadedFileNum)
				{
					//JSH Modify
					/*if(pDoc->m_pFileInfo != NULL)
					{
						CFileInfoDlg dlg;
						dlg.m_pFileInfo = &pDoc->m_pFileInfo[nFileindex];
						dlg.DoModal();
					}*/
				}			
            } 
			else if (HSData.nHotSpotType == PEHS_POINT || HSData.nHotSpotType == PEHS_TABLE)		//X Point Click
            {
				//JSH Modify
				//DisplayDataTabel(HSData);
			}
/*            else if ((HSData.nHotSpotType == PEHS_TABLE) || (HSData.nHotSpotType == PEHS_DATAPOINT))
            {
                UINT offset = ((int) HSData.w1 * (int) PEnget(m_hWndPE, PEP_nPOINTS)) + (int) HSData.w2;	//Tabel Data
                char    szTmp[128];
                char    szSubset[128];
                char    szTmp2[48];
                float   fData;
                szTmp[0] = 0;
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
 //             strcat(szTmp, "항목 : ");
                strcat(szTmp, szSubset);
                strcat(szTmp, " =  ");
                PEvgetcell(m_hWndPE, PEP_faYDATA, offset, (LPVOID) &fData);
                sprintf(szTmp2, "%.4f", fData);             
                strcat(szTmp, szTmp2);
                MessageBox(szTmp, "선택된 항목 값", MB_OK | MB_ICONINFORMATION);
				
            }
*/			break;
		case PEWN_ZOOMIN:
/*			if(!pYset[0].nYItemUnit)
			{
				OnGridAnnotaionSet(); 
			}
*/			break;
		case PEWN_ZOOMOUT:
/*			if(!pYset[0].nYItemUnit)
			{
				OnGridAnnotaionSet(); 
			}
*/			break;
	}
	return CFormView::OnCommand(wParam, lParam);
}

//Y축 Grid Line을 설정 한다.
void CResultGraphView::SetAnnotation()
{
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	ASSERT(pDoc);

	if(pDoc->m_GraphLineSet.bAutoGrid)
	{
		SetN(PEP_nGRIDLINECONTROL, PEGLC_BOTH);				//Grid Line 표시 여부
	}
	else
	{
		SetN(PEP_nGRIDLINECONTROL, PEGLC_XAXIS);				//Grid Line 표시 여부
	}
	
	if(pDoc->m_GraphLineSet.bAdded == FALSE && 	pDoc->m_GraphLineSet.bUserAddGrid == FALSE)
 	{
	    SetN(PEP_bSHOWANNOTATIONS, FALSE);					//Annotation을 사용 한다.
		return;
	}

	double dMaxY, dMinY;
	PEvget(m_hWndPE, PEP_fMANUALMAXY, &dMaxY);
	PEvget(m_hWndPE, PEP_fMANUALMINY, &dMinY);			//Get Current Y Mininum, Maxinum
	if(dMaxY <= dMinY)	return;

	float fRange = (float)(dMaxY - dMinY);						//Get Current Y Range
	float fInterval;					//Line Interval
	double HLA[MAX_H_LINE_NO+MAX_ADD_ANNO_NO];							//Point
	int HLAT[MAX_H_LINE_NO+MAX_ADD_ANNO_NO];							//Line Style
	long HLAC[MAX_H_LINE_NO+MAX_ADD_ANNO_NO];							//Line Color
	int count =0;
	CString strTemp, strGridText;						//Comment	

	if(pDoc->m_GraphLineSet.bAdded)
	{
		if(pDoc->m_GraphLineSet.select)
		{
			dMinY =  pDoc->m_GraphLineSet.fVal[0];
			dMaxY =  pDoc->m_GraphLineSet.fVal[1];
			fInterval = pDoc->m_GraphLineSet.fVal[2];
		}
		else
		{	
			switch(pDoc->m_GraphLineSet.devided)
			{
			case 0:		fInterval = fRange/10.0f;		break;			//Line Interval
			case 1:		fInterval = fRange/50.0f;		break;
			case 2:		fInterval = fRange/100.0f;		break;
			case 3:		fInterval = fRange/200.0f;		break;
			default:	fInterval = fRange/100.0f;		break;
			}
		}

		for(float f = (float)dMinY; f<dMaxY; f += 	fInterval)
		{
			HLA[count] = f;
			HLAT[count] = pDoc->m_GraphLineSet.lineStyle;
			HLAC[count++] = pDoc->m_GraphLineSet.colorLine;
			strTemp.Format("|R%.3f\t", f); 
			strGridText += strTemp;				
			if(count>=MAX_H_LINE_NO)	break;
		}
	}
	
	if(pDoc->m_GraphLineSet.bUserAddGrid)
	{
		for(int i=0; i<pDoc->m_GraphLineSet.nLineNo; i++)
		{
			HLA[count] = pDoc->m_GraphLineSet.lineInfo[i].fVal;
			HLAT[count] =  pDoc->m_GraphLineSet.lineInfo[i].lineStyle;
			HLAC[count++] = pDoc->m_GraphLineSet.lineInfo[i].colorLine;
			strTemp.Format("|c%s\t", pDoc->m_GraphLineSet.lineInfo[i].szComment); 
			strGridText += strTemp;				
		}
	}

	PEvset(m_hWndPE, PEP_faHORZLINEANNOTATION, HLA, count);
	PEvset(m_hWndPE, PEP_szaHORZLINEANNOTATIONTEXT, (LPSTR)(LPCTSTR)strGridText, count);
	PEvset(m_hWndPE, PEP_naHORZLINEANNOTATIONTYPE, HLAT, count);
	PEvset(m_hWndPE, PEP_dwaHORZLINEANNOTATIONCOLOR, HLAC, count);
	SetN(PEP_bSHOWANNOTATIONS, TRUE);					//Annotation을 사용 한다.
}

void CResultGraphView::OnGridAnnotaionSet() 
{
	// TODO: Add your command handler code here
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	ASSERT(pDoc);
	CAnnotationSetDlg dlg;
	dlg.m_LineInfo = pDoc->m_GraphLineSet;
	if(IDOK != dlg.DoModal())		return;
	pDoc->m_GraphLineSet = dlg.m_LineInfo;
	SetAnnotation();
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CResultGraphView::OnFileSave() 
{
	// TODO: Add your command handler code here
	PElaunchexport(m_hWndPE);	
}

void CResultGraphView::OnFilePrint() 
{
	// TODO: Add your command handler code here
	PElaunchprintdialog(m_hWndPE, TRUE, NULL);
}

void CResultGraphView::DisplayDataTabel(HOTSPOTDATA HSData)
{
//	char szSubset[48];
//	PEvgetcell(m_hWndPE, PEP_szaPOINTLABELS, (UINT) HSData.w1, szSubset);
//	int nDataIndex = m_nReadPoint+HSData.w1;//atoi(szSubset);
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	ASSERT(pDoc);

	CDataPointDlg dlg;
	dlg.m_pDoc = pDoc;
//	dlg.m_pFileInfo = pDoc->m_pFileInfo;
	if (HSData.nHotSpotType == PEHS_POINT)
		dlg.m_nDataPointIndex = HSData.w1;
	else if(HSData.nHotSpotType == PEHS_TABLE)		//X Point Click
		dlg.m_nDataPointIndex = HSData.w2;
	dlg.DoModal();
}

void CResultGraphView::OnZoomIn() 
{
	// TODO: Add your command handler code here
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	ASSERT(pDoc);

	CXAxisZoomDlg dlg;
	dlg.m_nHscrollPonitNo = 0;
	int nSize  = 0;
	if(IDOK == dlg.DoModal())
	{
		nSize = dlg.m_nHscrollPonitNo;
		if(nSize <0 || nSize >= pDoc->m_nMaxDataPoint)
			nSize = pDoc->m_nMaxDataPoint;
		
		SetN(PEP_nPOINTSTOGRAPH, nSize);			//H_Scroll 시키는 XPoint 수
		PEreinitialize(m_hWndPE);
		PEresetimage(m_hWndPE, 0, 0);
		::InvalidateRect(m_hWndPE, NULL, FALSE);
	}

}

// CSV File Loading..
void CResultGraphView::LoadFileA(BOOL bLoad, CString strFileName)
{
	CCTSAnalDoc *pDoc = (CCTSAnalDoc *)GetDocument();
	ASSERT(pDoc);

	CString strLoadFileName[MAX_CHANNEL_NUM];
	CString strTemp;
	int		nSelFileNo =0;

	if(bLoad)
	{
		if(strFileName.IsEmpty())		//File Open Dialog
		{
			strTemp = "*.csv";
			CFileDialog dlgFile(TRUE, 
				"", strTemp, OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_ENABLESIZING|OFN_ALLOWMULTISELECT, 
				TEXT_LANG[3]+"(*.csv)|*.csv|");
			dlgFile.m_ofn.lpstrTitle = TEXT_LANG[3]; //"PowerFormation 결과 CSV 파일"
			dlgFile.m_ofn.nFilterIndex = GENERAL_FILE;
			
			if(dlgFile.DoModal() == IDCANCEL)		return ;
				
			CFileFind fFind;
			
			POSITION pos = dlgFile.GetStartPosition();
			while(pos!=NULL && nSelFileNo < MAX_CHANNEL_NUM)		//Get Seleted File Name List(MAX 8)
			{
				strLoadFileName[nSelFileNo] = dlgFile.GetNextPathName(pos);
				nSelFileNo++;
			}			
			//strLoadFileName = dlgFile.GetPathName();
		}
		else								//Drag and Droped File
		{
			strLoadFileName[0] = strFileName;
			strTemp = strFileName.Mid(strFileName.ReverseFind('.')+1);
			strTemp.MakeUpper();
			if(strTemp != "CSV")
			{
				AfxMessageBox(TEXT_LANG[2]); //"지원되지 않는 파일 형식입니다."
				return;
			}
		}
	}
	if(pDoc->LoadFileData(bLoad, strLoadFileName, nSelFileNo) <0)	
		return;
	DisplayData(bLoad, TRUE);
	
/*	if (pDoc->m_pProgressWnd)
		delete pDoc->m_pProgressWnd;
	CString strMsg = strLoadFileName + " 파일을 읽고 있습니다";
	pDoc->m_pProgressWnd = new CProgressWnd(AfxGetApp()->m_pMainWnd, strLoadFileName, TRUE);
	pDoc->m_pProgressWnd->SetColor(RGB(0,0,192));
	pDoc->m_pProgressWnd->SetRange(0,1000);
	pDoc->m_pProgressWnd->SetText(strMsg);
	pDoc->m_pProgressWnd->PeekAndPump(FALSE);
	pDoc->m_pProgressWnd->Show();
	BeginWaitCursor();

	if(pDoc->LoadFileDataA(bLoad, strLoadFileName) <0)	return;
	DisplayData(bLoad, TRUE);

	pDoc->m_pProgressWnd->SetText("출력이 완료되었습니다..");
	pDoc->m_pProgressWnd->SetPos(1000);
	pDoc->m_pProgressWnd->PeekAndPump(FALSE);

	Sleep(500);
	pDoc->m_pProgressWnd->Hide();
	delete pDoc->m_pProgressWnd ;
	pDoc->m_pProgressWnd = NULL;
	EndWaitCursor();*/
}
