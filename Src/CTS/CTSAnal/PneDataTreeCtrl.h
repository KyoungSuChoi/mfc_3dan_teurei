#if !defined(AFX_PNEDATATREECTRL_H__96ECB95E_CFF8_4F11_A846_D9A7113AC863__INCLUDED_)
#define AFX_PNEDATATREECTRL_H__96ECB95E_CFF8_4F11_A846_D9A7113AC863__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PneDataTreeCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPneDataTreeCtrl window

class CPneDataTreeCtrl : public CTreeCtrl
{
// Construction
public:
	CPneDataTreeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPneDataTreeCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	void ExpendTree(HTREEITEM hItem = NULL);
	void SetRootDir(CString strRoot);
	virtual ~CPneDataTreeCtrl();

	// Generated message map functions
protected:
	CString m_strRootDir;
	//{{AFX_MSG(CPneDataTreeCtrl)
	afx_msg void OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PNEDATATREECTRL_H__96ECB95E_CFF8_4F11_A846_D9A7113AC863__INCLUDED_)
