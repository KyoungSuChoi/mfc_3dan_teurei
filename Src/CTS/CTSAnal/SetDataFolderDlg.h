#if !defined(AFX_SETDATAFOLDERDLG_H__86D94D48_1283_4602_B08E_8284A9996F5C__INCLUDED_)
#define AFX_SETDATAFOLDERDLG_H__86D94D48_1283_4602_B08E_8284A9996F5C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetDataFolderDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetDataFolderDlg dialog

class CSetDataFolderDlg : public CDialog
{
// Construction
public:
	CSetDataFolderDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetDataFolderDlg)
	enum { IDD = IDD_DATA_FOLDER_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetDataFolderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	SECListBoxDirEditor m_LBDirEditor;		
	// Generated message map functions
	//{{AFX_MSG(CSetDataFolderDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETDATAFOLDERDLG_H__86D94D48_1283_4602_B08E_8284A9996F5C__INCLUDED_)
