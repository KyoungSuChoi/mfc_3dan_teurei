// LogInDlg.cpp : implementation file
//
#include "stdafx.h"
#include "CTSAnal.h"
#include "LogInDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogInDlg dialog


CLogInDlg::CLogInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLogInDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLogInDlg)
	m_strLogInID = _T("");
	m_strPassWord = _T("");
	m_Remember = FALSE;
	//}}AFX_DATA_INIT
	m_Remember = FALSE;
}


void CLogInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogInDlg)
	DDX_Text(pDX, IDC_LOGINID, m_strLogInID);
	DDX_Text(pDX, IDC_PASSWORD, m_strPassWord);
	DDX_Check(pDX, IDC_REMEMBER, m_Remember);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogInDlg, CDialog)
	//{{AFX_MSG_MAP(CLogInDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogInDlg message handlers
void CLogInDlg::InitDlg()
{

}
void CLogInDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CDialog::OnOK();
}

