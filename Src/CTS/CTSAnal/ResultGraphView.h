#if !defined(AFX_RESULTGRAPHVIEW_H__9CD2FCF8_DC5C_426F_AFC2_3455386786AD__INCLUDED_)
#define AFX_RESULTGRAPHVIEW_H__9CD2FCF8_DC5C_426F_AFC2_3455386786AD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ResultGraphView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CResultGraphView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "PEGRPAPI.h"
#include "pemessag.h"
#include "mydefine.h"

class CCTSAnalDoc;

class CResultGraphView : public CFormView
{
protected: // create from serialization only
	CResultGraphView();
	DECLARE_DYNCREATE(CResultGraphView)

public:
	//{{AFX_DATA(CResultGraphView)
	enum{ IDD = IDD_RESULT_GRAPH_VIEW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

	CCTSAnalDoc* GetDocument();
	void SetN(UINT type, INT value);
	void SetV(UINT type, VOID FAR * lpData, UINT nItems);
	void SetSZ(UINT type, CHAR FAR *str);
	void SetVCell(UINT type, UINT nCell, VOID FAR * lpData);
	
	void SubSetSetting(int nFileNum = 1);
	void ShowItemSubset(int nItemIndex, int nSubSetNo, BOOL bShow = TRUE);	 //if Item is True Show Item Subset or Show channel SubSet
	void SetAxisRange(int nItem , double dMin, double dMax, int nUnit);
	void SetSplit(BOOL bSplit= TRUE);
	void DisplayDataTabel(HOTSPOTDATA HSData);
	void OnCsvFileOpen();


//	void ShowSubset(int id, BOOL bShow=TRUE);
//	void SetSplit(BOOL bSplit);
//	void SetDataPtr(int id, float *data);
//	void ClearGraph();
	void SetData(int subset, int pos, LPCTSTR xData, double yData);


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CResultGraphView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetAnnotation();
	void LoadFileA(BOOL bLoad, CString strFileName = "");
	BOOL m_bSplit;
	void DisplayData(BOOL	bUpdateAll = TRUE, BOOL bShowXLabel = TRUE);
	BOOL m_bShowArray[MAX_CHANNEL_NUM * MAX_MUTI_AXIS];
	virtual ~CResultGraphView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	HWND		m_hWndPE;
	CSliderCtrl	m_ctrlSlider;
	void InitGraph();

// Generated message map functions
protected:
	BOOL InitSlierBar(long lRight);
	//{{AFX_MSG(CResultGraphView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnAxisSetting();
	afx_msg void OnSubsetSetting();
	afx_msg void OnSpliteView();
	afx_msg void OnGraphSet();
	afx_msg void OnZoomOut();
	afx_msg void OnMaximizeView();
	afx_msg void OnUpdateSpliteView(CCmdUI* pCmdUI);
	afx_msg void OnGridAnnotaionSet();
	afx_msg void OnFileSave();
	afx_msg void OnFilePrint();
	afx_msg void OnZoomIn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CTSAnalView.cpp
inline CCTSAnalDoc* CResultGraphView::GetDocument()
   { return (CCTSAnalDoc*)m_pDocument; }
#endif
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESULTGRAPHVIEW_H__9CD2FCF8_DC5C_426F_AFC2_3455386786AD__INCLUDED_)
