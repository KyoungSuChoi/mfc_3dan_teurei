#if !defined(AFX_CHVALUERECORDSET_H__A624C20B_0149_414A_9600_112F8FD808D8__INCLUDED_)
#define AFX_CHVALUERECORDSET_H__A624C20B_0149_414A_9600_112F8FD808D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChValueRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChValueRecordSet recordset

class CChValueRecordSet : public CRecordset
{
public:
	CChValueRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CChValueRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CChValueRecordSet, CRecordset)
	long	m_Index;
	long	m_TestIndex;
//	long	m_dataType;
	float    m_ch[128];
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChValueRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHVALUERECORDSET_H__A624C20B_0149_414A_9600_112F8FD808D8__INCLUDED_)
