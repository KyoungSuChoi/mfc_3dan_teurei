#if !defined(AFX_FIELDSETDLG_H__F9FE0E16_7B16_4518_8F1A_EB167B272165__INCLUDED_)
#define AFX_FIELDSETDLG_H__F9FE0E16_7B16_4518_8F1A_EB167B272165__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FieldSetDlg.h : header file
//
#include "CTSAnalView.h"
#include "CTSAnalDoc.h"
/////////////////////////////////////////////////////////////////////////////
// CFieldSetDlg dialog

class CFieldSetDlg : public CDialog
{
// Construction
public:
	FIELD     m_Field[FIELDNO];
	int       m_TotCol;
	CCTSAnalView         *m_pView;
	CCTSAnalDoc          *m_pDoc;
public:
	CFieldSetDlg(CWnd* pParent = NULL);   // standard constructor
	void      InitField();

// Dialog Data
	//{{AFX_DATA(CFieldSetDlg)
	enum { IDD = IDD_FIELDSET_DLG };
	BOOL	m_C1;
	BOOL	m_C2;
	BOOL	m_D1;
	BOOL	m_D1C1;
	BOOL	m_Grading;
	BOOL	m_I1;
	BOOL	m_I2;
	BOOL	m_OCV;
	BOOL	m_OutCh;
	BOOL	m_OutDCh;
	BOOL	m_OutImp;
	BOOL	m_PreC;
	BOOL	m_PreImp;
	BOOL	m_PreT;
	BOOL	m_FailCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFieldSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFieldSetDlg)
	afx_msg void OnC1();
	afx_msg void OnC2();
	afx_msg void OnD1();
	afx_msg void OnD1c1();
	afx_msg void OnFailcode();
	afx_msg void OnGrading();
	afx_msg void OnI1();
	afx_msg void OnI2();
	afx_msg void OnOcv();
	afx_msg void OnOutch();
	afx_msg void OnOutdch();
	afx_msg void OnOutimp();
	afx_msg void OnPrec();
	afx_msg void OnPreimp();
	afx_msg void OnPret();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIELDSETDLG_H__F9FE0E16_7B16_4518_8F1A_EB167B272165__INCLUDED_)
