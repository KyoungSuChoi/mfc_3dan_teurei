#if !defined(AFX_FIELDDLG_H__76179091_2386_4933_BBE5_DF493347777C__INCLUDED_)
#define AFX_FIELDDLG_H__76179091_2386_4933_BBE5_DF493347777C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FieldDlg.h : header file
//
//#include "CTSAnalView.h"
#include "CTSAnalDoc.h"
#include "ReportDataListRecordSet.h"
class CCTSAnalView;
/////////////////////////////////////////////////////////////////////////////
// CFieldDlg dialog

class CFieldDlg : public CDialog
{
// Construction
public:
//	CCTSAnalView          *m_pView;
//	CCTSAnalDoc           *m_pDoc;
	CReportDataListRecordSet  m_ReportDataList;
	FIELD					  m_field[FIELDNO];
	int                       m_TotCol;
	BOOL                      m_bRadio1;
	BOOL                      m_bRadio2;
	BOOL                      m_bRadio3;
	BOOL                      m_bRadio4;
	BOOL                      m_bRadio5;
	int                       m_DataType;
	int                       m_currentIndex;
	BOOL                      m_bSize;             //dialog Settingâ ����, ����
public:
	CFieldDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFieldDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;

// Dialog Data
	//{{AFX_DATA(CFieldDlg)
	enum { IDD = IDD_FIELD_DLG };
	CListCtrl	m_List2;
	CListCtrl	m_List1;
	BOOL	m_bDisplayCheck;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFieldDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
public:
	BOOL Create();
	void InitList1();
	void InitList2();
	void ViewList1();
	void ViewList2();
	void FieldSelect();

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFieldDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnDisplayinputBtn();
	afx_msg void OnDisplayoutputBtn();
	afx_msg void OnAddBtn();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	afx_msg void OnRadio5();
	afx_msg void OnInsertBtn();
	afx_msg void OnDisplayCheck();
	afx_msg void OnDelBtn();
	afx_msg void OnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIELDDLG_H__76179091_2386_4933_BBE5_DF493347777C__INCLUDED_)
