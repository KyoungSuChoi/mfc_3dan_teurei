# Microsoft Developer Studio Project File - Name="CTSAnal" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CTSAnal - Win32 DebugE
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CTSAnal.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CTSAnal.mak" CFG="CTSAnal - Win32 DebugE"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CTSAnal - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CTSAnal - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "CTSAnal - Win32 DebugE" (based on "Win32 (x86) Application")
!MESSAGE "CTSAnal - Win32 ReleaseE" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CTSAnal - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/CTSAnal_Release"
# PROP Intermediate_Dir "../../../obj/CTSAnal_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 PEGRAP32.LIB /nologo /subsystem:windows /machine:I386 /out:"../../../bin/Release/CTSAnal.exe"

!ELSEIF  "$(CFG)" == "CTSAnal - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/CTSAnal_Debug"
# PROP Intermediate_Dir "../../../obj/CTSAnal_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W4 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 PEGRAP32.LIB Winmm.lib /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/Debug/CTSAnal.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CTSAnal - Win32 DebugE"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CTSAnal___Win32_DebugE"
# PROP BASE Intermediate_Dir "CTSAnal___Win32_DebugE"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/CTSAnal_DebugE"
# PROP Intermediate_Dir "../../../obj/CTSAnal_DebugE"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W4 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W4 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_EDLC_TEST_SYSTEM" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 BFCtrlD.lib PEGRAP32.LIB BFCommonD.lib Winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 PEGRAP32.LIB Winmm.lib /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/DebugE/CTSAnal.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CTSAnal - Win32 ReleaseE"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CTSAnal___Win32_ReleaseE"
# PROP BASE Intermediate_Dir "CTSAnal___Win32_ReleaseE"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/CTSAnal_ReleaseE"
# PROP Intermediate_Dir "../../../obj/CTSAnal_ReleaseE"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_EDLC_TEST_SYSTEM" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 BFCtrl.lib PEGRAP32.LIB BFCommon.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 PEGRAP32.LIB /nologo /subsystem:windows /machine:I386 /out:"../../../bin/ReleaseE/CTSAnal.exe"

!ENDIF 

# Begin Target

# Name "CTSAnal - Win32 Release"
# Name "CTSAnal - Win32 Debug"
# Name "CTSAnal - Win32 DebugE"
# Name "CTSAnal - Win32 ReleaseE"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AnnotationSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AxisSettingDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BitmapPickerCombo.cpp
# End Source File
# Begin Source File

SOURCE=.\CellGradeListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CellInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CellListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CellListSaveDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ChCodeRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ChErrorDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ChResultSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ChResultViewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ChValueRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorSelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CpkSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CpkViewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CreditStatic.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSAnal.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSAnal.rc
# End Source File
# Begin Source File

SOURCE=.\CTSAnalDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSAnalView.cpp
# End Source File
# Begin Source File

SOURCE=.\DataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataFieldSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataPointDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DetailTestConditionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DeviationGraphWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\FieldAddDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FieldDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FieldSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FileSaveDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FormelParser.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FormResultFile.cpp
# End Source File
# Begin Source File

SOURCE=.\FtpDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\FtpDownLoad.cpp
# End Source File
# Begin Source File

SOURCE=.\GradeColorDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Grading.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\IconCombo.cpp
# End Source File
# Begin Source File

SOURCE=.\IpSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LogInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleRunTimeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleStepEndData.cpp
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\NewGridWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.cpp
# End Source File
# Begin Source File

SOURCE=.\ProcTypeRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressProcDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.Cpp
# End Source File
# Begin Source File

SOURCE=.\ReportDataListRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ResultFileLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ResultGraphView.cpp
# End Source File
# Begin Source File

SOURCE=.\ResultView.cpp
# End Source File
# Begin Source File

SOURCE=.\Rw_gfa.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchMDBSetFieldDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchMDBView.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchView.cpp
# End Source File
# Begin Source File

SOURCE=.\SelCellCodeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectTrayDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SensorDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SerarchDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SetDataFolderDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SetOnlineDataFolderDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellContextMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellPidl.cpp
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellString.cpp
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellTreeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ShowDetailResultDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SimplexParserDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\Step.cpp
# End Source File
# Begin Source File

SOURCE=.\SubSetSettingDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\UtilityClass\src\TestCondition.cpp
# End Source File
# Begin Source File

SOURCE=.\TestLogEditDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TestLogSet.cpp
# End Source File
# Begin Source File

SOURCE=.\TestRecordSet.cpp
# End Source File
# Begin Source File

SOURCE=.\UserExpDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UserRestoreDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UserSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\WaitingTreeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\XAxisZoomDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AnnotationSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\AxisSettingDlg.h
# End Source File
# Begin Source File

SOURCE=.\BitmapPickerCombo.h
# End Source File
# Begin Source File

SOURCE=.\CellGradeListDlg.h
# End Source File
# Begin Source File

SOURCE=.\CellInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\CellListDlg.h
# End Source File
# Begin Source File

SOURCE=.\CellListSaveDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChCodeRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\ChErrorDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChResultSet.h
# End Source File
# Begin Source File

SOURCE=.\ChResultViewDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChValueRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\ColorSelDlg.h
# End Source File
# Begin Source File

SOURCE=.\CpkSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\CpkViewDlg.h
# End Source File
# Begin Source File

SOURCE=.\CreditStatic.h
# End Source File
# Begin Source File

SOURCE=.\CTSAnal.h
# End Source File
# Begin Source File

SOURCE=.\CTSAnalDoc.h
# End Source File
# Begin Source File

SOURCE=.\CTSAnalView.h
# End Source File
# Begin Source File

SOURCE=.\DataDlg.h
# End Source File
# Begin Source File

SOURCE=.\DataFieldSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\DataListDlg.h
# End Source File
# Begin Source File

SOURCE=.\DataPointDlg.h
# End Source File
# Begin Source File

SOURCE=.\DetailTestConditionDlg.h
# End Source File
# Begin Source File

SOURCE=.\DeviationGraphWnd.h
# End Source File
# Begin Source File

SOURCE=.\FieldAddDlg.h
# End Source File
# Begin Source File

SOURCE=.\FieldDlg.h
# End Source File
# Begin Source File

SOURCE=.\FieldSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\FileSaveDlg.h
# End Source File
# Begin Source File

SOURCE=.\FormelParser.h
# End Source File
# Begin Source File

SOURCE=.\FtpDlg.h
# End Source File
# Begin Source File

SOURCE=.\GradeColorDlg.h
# End Source File
# Begin Source File

SOURCE=.\GraphWnd.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\HourglassFX.h
# End Source File
# Begin Source File

SOURCE=.\IconCombo.h
# End Source File
# Begin Source File

SOURCE=.\IpSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\LogInDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModuleRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\ModuleRunTimeDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModuleStepEndData.h
# End Source File
# Begin Source File

SOURCE=.\Mydefine.h
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.h
# End Source File
# Begin Source File

SOURCE=.\NewGridWnd.h
# End Source File
# Begin Source File

SOURCE=.\ParsingData.h
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.h
# End Source File
# Begin Source File

SOURCE=.\ProcTypeRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ProgressFX.h
# End Source File
# Begin Source File

SOURCE=.\ProgressProcDlg.h
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ReportDataListRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\ResultFileLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\ResultGraphView.h
# End Source File
# Begin Source File

SOURCE=.\ResultView.h
# End Source File
# Begin Source File

SOURCE=.\Rw_gfa.h
# End Source File
# Begin Source File

SOURCE=.\SearchMDBSetFieldDlg.h
# End Source File
# Begin Source File

SOURCE=.\SearchMDBView.h
# End Source File
# Begin Source File

SOURCE=.\SearchView.h
# End Source File
# Begin Source File

SOURCE=.\SelCellCodeDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelectTrayDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\SensorDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\SerarchDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\SetDataFolderDlg.h
# End Source File
# Begin Source File

SOURCE=.\SetOnlineDataFolderDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellContextMenu.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellPidl.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellString.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellTreeCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\ShellWrappers.h
# End Source File
# Begin Source File

SOURCE=.\ShowDetailResultDlg.h
# End Source File
# Begin Source File

SOURCE=.\SimplexParserDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\SmartInterfacePtr.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\SubSetSettingDlg.h
# End Source File
# Begin Source File

SOURCE=.\TestLogEditDlg.h
# End Source File
# Begin Source File

SOURCE=.\TestLogSet.h
# End Source File
# Begin Source File

SOURCE=.\TestRecordSet.h
# End Source File
# Begin Source File

SOURCE=.\UserExpDlg.h
# End Source File
# Begin Source File

SOURCE=.\UserRestoreDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\UserSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShellDirTree\WaitingTreeCtrl.h
# End Source File
# Begin Source File

SOURCE=.\XAxisZoomDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\b0.ico
# End Source File
# Begin Source File

SOURCE=.\res\BadFace.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap5.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap6.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\c1.ico
# End Source File
# Begin Source File

SOURCE=.\res\c6.ico
# End Source File
# Begin Source File

SOURCE=.\res\Chart6.ico
# End Source File
# Begin Source File

SOURCE=.\res\cross.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CTSAnal.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSAnal.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CTSAnalDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\ElicoPower.bmp
# End Source File
# Begin Source File

SOURCE=.\res\EPLogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\GoodFace.bmp
# End Source File
# Begin Source File

SOURCE=.\res\graphico.ico
# End Source File
# Begin Source File

SOURCE=.\res\greenButton.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Grid.ico
# End Source File
# Begin Source File

SOURCE=.\res\Grid2.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico_folder.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon3.ico
# End Source File
# Begin Source File

SOURCE=".\res\Index Cards.ico"
# End Source File
# Begin Source File

SOURCE=.\res\magnify.ico
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\module.ico
# End Source File
# Begin Source File

SOURCE=.\res\PHYSICS3.ICO
# End Source File
# Begin Source File

SOURCE=.\res\poker.ico
# End Source File
# Begin Source File

SOURCE=.\res\print.bmp
# End Source File
# Begin Source File

SOURCE=.\res\RedButton.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Search document.ico"
# End Source File
# Begin Source File

SOURCE=.\res\Search.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Search.ico
# End Source File
# Begin Source File

SOURCE=.\res\SmallBlueArrow.bmp
# End Source File
# Begin Source File

SOURCE=.\res\SmallCross.bmp
# End Source File
# Begin Source File

SOURCE=.\res\State16.bmp
# End Source File
# Begin Source File

SOURCE=.\res\state_ic.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tick.bmp
# End Source File
# Begin Source File

SOURCE=.\res\TIMER.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Untitled.ico
# End Source File
# Begin Source File

SOURCE=.\res\VECTOR.ICO
# End Source File
# Begin Source File

SOURCE=".\res\Virtual Snooker.ico"
# End Source File
# Begin Source File

SOURCE=.\res\writingpad.ico
# End Source File
# Begin Source File

SOURCE=.\res\YellowBar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Zbench.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\res\hourglass.ani
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
