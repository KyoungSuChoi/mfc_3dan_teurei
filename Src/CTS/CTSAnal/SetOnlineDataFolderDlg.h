#if !defined(AFX_SETONLINEDATAFOLDERDLG_H__7AE1AB4D_6017_4EE9_A3EB_ADA21C1EB7F5__INCLUDED_)
#define AFX_SETONLINEDATAFOLDERDLG_H__7AE1AB4D_6017_4EE9_A3EB_ADA21C1EB7F5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetOnlineDataFolderDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SetOnlineDataFolderDlg dialog

class SetOnlineDataFolderDlg : public CDialog
{
// Construction
public:
	SetOnlineDataFolderDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SetOnlineDataFolderDlg)
	enum { IDD = IDD_ONLINEDATA_FOLDER_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SetOnlineDataFolderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	SECListBoxDirEditor m_LBDirEditor;

	// Generated message map functions
	//{{AFX_MSG(SetOnlineDataFolderDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETONLINEDATAFOLDERDLG_H__7AE1AB4D_6017_4EE9_A3EB_ADA21C1EB7F5__INCLUDED_)
