#if !defined(AFX_TESTLOGEDITDLG_H__E741F5AA_C126_489B_94C8_CE1096935E6F__INCLUDED_)
#define AFX_TESTLOGEDITDLG_H__E741F5AA_C126_489B_94C8_CE1096935E6F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestLogEditDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestLogEditDlg dialog

class CTestLogEditDlg : public CDialog
{
// Construction
public:
	void SetTitle(CString strData);
	CTestLogEditDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTestLogEditDlg)
	enum { IDD = IDD_TEST_LOG_EDIT_DLG };
	CEdit	m_ctrlValue;
	CString	m_strValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestLogEditDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTestLogEditDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString m_strTitle;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTLOGEDITDLG_H__E741F5AA_C126_489B_94C8_CE1096935E6F__INCLUDED_)
