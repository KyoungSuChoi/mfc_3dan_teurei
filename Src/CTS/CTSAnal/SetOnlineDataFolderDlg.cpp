// SetOnlineDataFolderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanal.h"
#include "SetOnlineDataFolderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SetOnlineDataFolderDlg dialog


SetOnlineDataFolderDlg::SetOnlineDataFolderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SetOnlineDataFolderDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SetOnlineDataFolderDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void SetOnlineDataFolderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SetOnlineDataFolderDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SetOnlineDataFolderDlg, CDialog)
	//{{AFX_MSG_MAP(SetOnlineDataFolderDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SetOnlineDataFolderDlg message handlers

BOOL SetOnlineDataFolderDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	char szBuff[512];
	GetCurrentDirectory(511, szBuff);
	
	CListBox* pLB = (CListBox*)GetDlgItem(IDC_DIRLIST);
	
	//default data
	CString str = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"OnlineData");		//Get Data Folder
	if(str.IsEmpty())
	{
		str.Format("%s\\Data", szBuff);
	}

	CString strTmp;
	for(int i = 0; i<10; i++)
	{
		strTmp.Format("OnlineData%d", i);
		str = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION , strTmp);
		if(str.IsEmpty() == FALSE)
		{
			pLB->AddString(_T(str));
		}
	}
	VERIFY(m_LBDirEditor.Initialize(this, IDC_DIRLIST));
	m_LBDirEditor.SetWindowText(_T("&Directories:"));	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SetOnlineDataFolderDlg::OnOK() 
{
	// TODO: Add extra validation here
	CListBox* pLB = (CListBox*)GetDlgItem(IDC_DIRLIST);
	CString str, strTmp;
	int nCnt = 0;
	for(int i = 0; i<pLB->GetCount(); i++)
	{
		strTmp.Format("OnlineData%d", nCnt++);
		pLB->GetText(i, str);
		if(str.IsEmpty() == FALSE)
		{
			str.TrimRight("\\");
			AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION , strTmp, str);
		}
		else
		{
			AfxGetApp()->WriteProfileString(FORM_PATH_REG_SECTION , strTmp, "");
		}
	}	
	CDialog::OnOK();
}
