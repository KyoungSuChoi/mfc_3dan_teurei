#if !defined(AFX_CHRESULTSET_H__FA9B59E1_F6BB_11D5_A021_00010282CC7B__INCLUDED_)
#define AFX_CHRESULTSET_H__FA9B59E1_F6BB_11D5_A021_00010282CC7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChResultSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChResultSet recordset

class CChResultSet : public CRecordset
{
public:
	CChResultSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CChResultSet)

// Field/Param Data
	//{{AFX_FIELD(CChResultSet, CRecordset)
	long	m_Index;
	CString	m_TestSerialNo;
	long    m_ch[128];
	long	m_dataType;
	long	m_ProcedureID;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChResultSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHRESULTSET_H__FA9B59E1_F6BB_11D5_A021_00010282CC7B__INCLUDED_)
