//==============================================================================
//  프로그램명   : TopMonitoringConfigDlg.cpp                                            
//  프로그램개요 : 
//  함수         :                                                          
//=============================================================================

#include "stdafx.h"
#include "TopMonitoringConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTopMonitoringConfigDlg dialog


CTopMonitoringConfigDlg::CTopMonitoringConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTopMonitoringConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTopMonitoringConfigDlg)
	m_DisplayText 		= FALSE;
	m_DisplayColor 		= FALSE;
	m_bVOverBlink = FALSE;
	m_bOverColor = FALSE;
	m_fVRange = 0.0f;
	m_fIRange = 0.0f;
	m_bIOverBlink = FALSE;
	//}}AFX_DATA_INIT
	m_bBlinkFlag = TRUE;
	m_bInitColor = FALSE;
}


void CTopMonitoringConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTopMonitoringConfigDlg)
	DDX_Check	(pDX, IDC_TOPCFG_DISPLAY_TEXT, 		m_DisplayText);
	DDX_Check	(pDX, IDC_TOPCFG_DISPLAY_COLOR, 	m_DisplayColor);
	DDX_Check(pDX, IDC_V_OVER_BLINK, m_bVOverBlink);
	DDX_Check(pDX, IDC_I_OVER_BLINK, m_bIOverBlink);
	DDX_Check	(pDX, IDC_VI_RANGE_CHECK, 	m_bOverColor);
	DDX_Text(pDX, IDC_V_RANGE, m_fVRange);
	DDX_Text(pDX, IDC_I_RANGE, m_fIRange);
	DDX_Control	(pDX, IDC_V_OVER_COLOR, m_VBOverColor);
	DDX_Control	(pDX, IDC_I_OVER_COLOR, m_IBOverColor);
	DDX_Control	(pDX, IDC_V_OVER_TCOLOR, m_VTOverColor);
	DDX_Control	(pDX, IDC_I_OVER_TCOLOR, m_ITOverColor);
	DDX_Control	(pDX, IDC_I_OVER_RESULT, m_IOverResult);
	DDX_Control	(pDX, IDC_V_OVER_RESULT, m_VOverResult);
	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_TOPCFG_TCOLOR1, m_TWellColor[0]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR2, m_TWellColor[1]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR3, m_TWellColor[2]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR4, m_TWellColor[3]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR5, m_TWellColor[4]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR6, m_TWellColor[5]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR7, m_TWellColor[6]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR8, m_TWellColor[7]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR9, m_TWellColor[8]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR10, m_TWellColor[9]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR11, m_TWellColor[10]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR12, m_TWellColor[11]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR13, m_TWellColor[12]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR14, m_TWellColor[13]);
	DDX_Control(pDX, IDC_TOPCFG_TCOLOR15, m_TWellColor[14]);

	DDX_Control(pDX, IDC_TOPCFG_BCOLOR1, m_BWellColor[0]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR2, m_BWellColor[1]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR3, m_BWellColor[2]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR4, m_BWellColor[3]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR5, m_BWellColor[4]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR6, m_BWellColor[5]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR7, m_BWellColor[6]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR8, m_BWellColor[7]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR9, m_BWellColor[8]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR10, m_BWellColor[9]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR11, m_BWellColor[10]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR12, m_BWellColor[11]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR13, m_BWellColor[12]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR14, m_BWellColor[13]);
	DDX_Control(pDX, IDC_TOPCFG_BCOLOR15, m_BWellColor[14]);
	
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE1, 	m_Flash[0]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE2, 	m_Flash[1]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE3, 	m_Flash[2]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE4, 	m_Flash[3]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE5, 	m_Flash[4]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE6, 	m_Flash[5]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE7, 	m_Flash[6]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE8, 	m_Flash[7]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE9, 	m_Flash[8]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE10, 	m_Flash[9]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE11, 	m_Flash[10]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE12, 	m_Flash[11]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE13, 	m_Flash[12]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE14, 	m_Flash[13]);
	DDX_Check	(pDX, IDC_TOPCFG_INVERSE15, 	m_Flash[14]);

	DDX_Control	(pDX, IDC_TOPCFGE_RESULT1, 	m_Result[0]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT2, 	m_Result[1]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT3, 	m_Result[2]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT4, 	m_Result[3]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT5, 	m_Result[4]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT6, 	m_Result[5]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT7, 	m_Result[6]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT8, 	m_Result[7]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT9, 	m_Result[8]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT10, 	m_Result[9]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT11, 	m_Result[10]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT12, 	m_Result[11]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT13, 	m_Result[12]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT14, 	m_Result[13]);
	DDX_Control	(pDX, IDC_TOPCFGE_RESULT15, 	m_Result[14]);

	DDX_Control	(pDX, IDC_RESULT_EDIT1, 	m_colorEdit[0]);
	DDX_Control	(pDX, IDC_RESULT_EDIT2, 	m_colorEdit[1]);
	DDX_Control	(pDX, IDC_RESULT_EDIT3, 	m_colorEdit[2]);
	DDX_Control	(pDX, IDC_RESULT_EDIT4, 	m_colorEdit[3]);
	DDX_Control	(pDX, IDC_RESULT_EDIT5, 	m_colorEdit[4]);
	DDX_Control	(pDX, IDC_RESULT_EDIT6, 	m_colorEdit[5]);
	DDX_Control	(pDX, IDC_RESULT_EDIT7, 	m_colorEdit[6]);
	DDX_Control	(pDX, IDC_RESULT_EDIT8, 	m_colorEdit[7]);
	DDX_Control	(pDX, IDC_RESULT_EDIT9, 	m_colorEdit[8]);
	DDX_Control	(pDX, IDC_RESULT_EDIT10, 	m_colorEdit[9]);
	DDX_Control	(pDX, IDC_RESULT_EDIT11, 	m_colorEdit[10]);
	DDX_Control	(pDX, IDC_RESULT_EDIT12, 	m_colorEdit[11]);
	DDX_Control	(pDX, IDC_RESULT_EDIT13, 	m_colorEdit[12]);
	DDX_Control	(pDX, IDC_RESULT_EDIT14, 	m_colorEdit[13]);
	DDX_Control	(pDX, IDC_RESULT_EDIT15, 	m_colorEdit[14]);
}


BEGIN_MESSAGE_MAP(CTopMonitoringConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CTopMonitoringConfigDlg)
	ON_BN_CLICKED(IDC_TOPCFG_SETDEFAULT, OnTopcfgSetdefault)
	ON_BN_CLICKED(IDC_TOPCFG_DISPLAY_TEXT, OnTopcfgDisplayText)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE1, OnTopcfgInverse1)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE2, OnTopcfgInverse2)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE3, OnTopcfgInverse3)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE4, OnTopcfgInverse4)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE5, OnTopcfgInverse5)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE6, OnTopcfgInverse6)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE7, OnTopcfgInverse7)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE8, OnTopcfgInverse8)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE9, OnTopcfgInverse9)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE10, OnTopcfgInverse10)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE11, OnTopcfgInverse11)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE12, OnTopcfgInverse12)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE13, OnTopcfgInverse13)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE14, OnTopcfgInverse14)
	ON_BN_CLICKED(IDC_TOPCFG_INVERSE15, OnTopcfgInverse15)
	ON_BN_CLICKED(IDC_V_OVER_BLINK, OnVOverBlink)
	ON_BN_CLICKED(IDC_I_OVER_BLINK, OnIOverBlink)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_TOPCFG_DISPLAY_COLOR, OnTopcfgDisplayColor)
	//}}AFX_MSG_MAP
	ON_MESSAGE(CB_BTCLICK, OnColorBtnClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTopMonitoringConfigDlg message handlers

BOOL CTopMonitoringConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//Load From config from registry
//	UINT nSize;
//	LPVOID* pData;
//	AfxGetApp()->GetProfileBinary(CT_CONFIG_REG_SEC, "State Color", (LPBYTE *)&pData, &nSize);
//
//	if(nSize == sizeof(m_TopCfg))	memcpy(&m_TopCfg, pData, nSize);
//	else		//Load Default config
//	{
//		m_TopCfg = GetDefaultColorConfig();
//	}
//	delete [] pData;

	if(AfxGetApp()->GetProfileInt("Config", "Crt Unit Mode", 0))
		GetDlgItem(IDC_I_UNIT_STATIC)->SetWindowText("uA");


	if(m_bInitColor == FALSE)
		m_TopCfg = *PSGetColorCfgData(FALSE);	//pscommon.dll

	SetTimer(100, 1000, NULL);

	DrawColor();

	return FALSE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// 1. Func.  : 
// 2. Desc   : 
//=============================================================================
void CTopMonitoringConfigDlg::OnOK() 
{
	UpdataColor();

//	AfxGetApp()->WriteProfileBinary(CT_CONFIG_REG_SEC, "State Color",(LPBYTE)&m_TopCfg, sizeof(m_TopCfg));

//	if(WriteColorCfgData(m_TopCfg)==FALSE)
//	{
//		AfxMessageBox("설정 저장에 실패 하였습니다.");
//	}

	CDialog::OnOK();
}

void CTopMonitoringConfigDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CTopMonitoringConfigDlg::OnTopcfgSetdefault() 
{
	m_TopCfg = GetDefaultColorConfig();
	DrawColor();
}

void CTopMonitoringConfigDlg::DrawColor()
{
	m_DisplayText 		= m_TopCfg.bShowText;
	m_DisplayColor 		= m_TopCfg.bShowColor;

	for (int i = 0; i < PS_MAX_STATE_COLOR; i++)
	{
		m_TWellColor[i].SetColor(m_TopCfg.stateConfig[i].TStateColor);
		m_BWellColor[i].SetColor(m_TopCfg.stateConfig[i].BStateColor);
		
		if(m_DisplayColor)
		{
			//result update
			m_Result[i].SetBkColor(m_TopCfg.stateConfig[i].BStateColor);
			m_Result[i].SetTextColor(m_TopCfg.stateConfig[i].TStateColor);

			m_colorEdit[i].SetTextColor(m_TopCfg.stateConfig[i].TStateColor);
			m_colorEdit[i].SetBkColor(m_TopCfg.stateConfig[i].BStateColor);
		}
		else
		{
			m_Result[i].SetBkColor(m_TopCfg.stateConfig[i].BStateColor);
			m_Result[i].SetTextColor(m_TopCfg.stateConfig[i].TStateColor);

			m_colorEdit[i].SetTextColor(RGB(0,0,0));
			m_colorEdit[i].SetBkColor(RGB(255,255,255));
		}
		
		if(m_DisplayText)
			m_colorEdit[i].SetWindowText(m_TopCfg.stateConfig[i].szMsg);
		else
			m_colorEdit[i].SetWindowText("");

		m_Flash[i] = m_TopCfg.stateConfig[i].bStateFlash;
	}


	m_bOverColor = m_TopCfg.bShowOver;
	m_bVOverBlink = m_TopCfg.VOverConfig.bFlash;
	m_bIOverBlink = m_TopCfg.IOverConfig.bFlash;
	m_VBOverColor.SetColor(m_TopCfg.VOverConfig.BOverColor);
	m_IBOverColor.SetColor(m_TopCfg.IOverConfig.BOverColor);
	m_VTOverColor.SetColor(m_TopCfg.VOverConfig.TOverColor);
	m_ITOverColor.SetColor(m_TopCfg.IOverConfig.TOverColor);

	//Result Update
	m_VOverResult.SetBkColor(m_TopCfg.VOverConfig.BOverColor);
	m_VOverResult.SetTextColor(m_TopCfg.VOverConfig.TOverColor);
	m_IOverResult.SetBkColor(m_TopCfg.IOverConfig.BOverColor);
	m_IOverResult.SetTextColor(m_TopCfg.IOverConfig.TOverColor);

	m_fVRange = m_TopCfg.VOverConfig.fValue;
	m_fIRange = m_TopCfg.IOverConfig.fValue;

	UpdateData(FALSE);
}


PS_COLOR_CONFIG CTopMonitoringConfigDlg::GetDefaultColorConfig()
{
//typedef struct tag_State_Display
//{
//	BOOL		bStateFlash;
//	COLORREF	TStateColor;
//	COLORREF	BStateColor;
//	char		szMsg[32];
//} _CT_STATE_CONFIG;
//
//typedef struct tag_Over_Display
//{
//	float		fValue;			//0 : Voltage //1: Current
//	BOOL		bFalsh;
//	COLORREF	TOverColor;
//	COLORREF	BOverColor;	
//} _CT_OVER_CONFIG;
//
//typedef struct tag_Color_Config
//{
//	BYTE		bShowText;
//	BYTE		bShowColor;
//	BYTE		bShowOver;
//
//	_CT_STATE_CONFIG	stateConfig[CT_MAX_STATE_COLOR];
//	_CT_OVER_CONFIG		VOverConfig;
//	_CT_OVER_CONFIG		IOverConfig;
//
//} CT_COLOR_CONFIG;
	
	PS_COLOR_CONFIG stateColor = 
	{
		TRUE,
		TRUE,
		FALSE,
		//state Color Config
		{ 
			{FALSE, RGB(200,200,0), RGB(255, 255, 255), "Vacancy"},	//0
			{FALSE, RGB(0,200,0), RGB(255, 255, 255), "준비"},	//1
			{FALSE, RGB(200,0,0), RGB(255, 255, 255), "대기"},	//2
			{FALSE, RGB(0,0,200), RGB(255, 255, 255), "작업중"},	//3 
			{FALSE, RGB(0,200,200), RGB(255, 255, 255), "잠시멈춤"},//4
			{FALSE, RGB(255,255,255), RGB(200, 200, 0), "점검중"},	//5
			{FALSE, RGB(255,255,255), RGB(0, 200, 200), "중지"},	//6
			{FALSE, RGB(255,255,255), RGB(94, 64, 196), "완료"},	//7
			{FALSE, RGB(255,0,0), RGB(255, 255, 0), "오류"},	//8
			{FALSE, RGB(255,255,255), RGB(255, 0, 0), "위험"},	//9
			{FALSE, RGB(0,0,0), RGB(255, 255, 255), "OCV"},		//10
			{FALSE, RGB(255,0,0), RGB(255, 255, 255), "충전"},	//11
			{FALSE, RGB(0,0,255), RGB(255, 255, 255), "방전"},	//12
			{FALSE, RGB(200,200,0), RGB(255, 255, 255), "휴지"},	//13
			{FALSE, RGB(0,200,0), RGB(255, 255, 255), "DCIR"},//14
			{FALSE, RGB(0,0,0), RGB(255, 255, 255), ""}			//15
		},
		//Voltage Over Color
		{ 
			0.0f, FALSE, RGB(255,255,255), RGB(64, 128, 255)
		},
		//Current Over Color
		{ 
			0.0f, FALSE, RGB(255,255,255), RGB(255, 128, 64)
		}
	};

	return stateColor;
}

LRESULT CTopMonitoringConfigDlg::OnColorBtnClick(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	UpdataColor();
	DrawColor();
	return 1;
}

void CTopMonitoringConfigDlg::UpdataColor()
{
	if (UpdateData(TRUE)==FALSE)
		return;

	CString strTemp;
	for (int i = 0; i < PS_MAX_STATE_COLOR; i++)
	{
		m_TopCfg.stateConfig[i].TStateColor = m_TWellColor[i].GetColor();
		m_TopCfg.stateConfig[i].BStateColor = m_BWellColor[i].GetColor();
		m_TopCfg.stateConfig[i].bStateFlash = m_Flash[i];

		m_colorEdit[i].GetWindowText(strTemp);
		sprintf(m_TopCfg.stateConfig[i].szMsg, "%s", strTemp);

	}

	m_TopCfg.bShowText 		 = (BYTE)m_DisplayText;
	m_TopCfg.bShowColor 		 = (BYTE)m_DisplayColor;
		
	m_TopCfg.bShowOver = (BYTE)m_bOverColor;

	m_TopCfg.VOverConfig.bFlash = m_bVOverBlink;
	m_TopCfg.IOverConfig.bFlash = m_bIOverBlink;

	m_TopCfg.VOverConfig.BOverColor = m_VBOverColor.GetColor();
	m_TopCfg.IOverConfig.BOverColor = m_IBOverColor.GetColor();
	m_TopCfg.VOverConfig.TOverColor = m_VTOverColor.GetColor();
	m_TopCfg.IOverConfig.TOverColor = m_ITOverColor.GetColor();

	m_TopCfg.VOverConfig.fValue = m_fVRange;
	m_TopCfg.IOverConfig.fValue = m_fIRange;
}

void CTopMonitoringConfigDlg::OnTopcfgDisplayText() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_TopCfg.bShowText = (BYTE)m_DisplayText;
	
	DrawColor();
}

void CTopMonitoringConfigDlg::OnTopcfgDisplayColor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	m_TopCfg.bShowColor = (BYTE)m_DisplayColor;
	
	DrawColor();	
}


void CTopMonitoringConfigDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_bBlinkFlag)
	{
		for (int i = 0; i < PS_MAX_STATE_COLOR; i++)
		{
			if(m_Flash[i])
				m_colorEdit[i].SetBkColor(m_TopCfg.stateConfig[i].BStateColor);
			if(m_bVOverBlink)
				m_VOverResult.SetBkColor(m_TopCfg.VOverConfig.BOverColor);
			if(m_bIOverBlink)
				m_IOverResult.SetBkColor(m_TopCfg.IOverConfig.BOverColor);
		}
		
	}
	else
	{
		for (int i = 0; i < PS_MAX_STATE_COLOR; i++)
		{
			if(m_Flash[i])
				m_colorEdit[i].SetBkColor(RGB(255,255,255));
			if(m_bVOverBlink)
				m_VOverResult.SetBkColor(RGB(255,255,255));
			if(m_bIOverBlink)
				m_IOverResult.SetBkColor(RGB(255,255,255));
		}
	}

	m_bBlinkFlag = !m_bBlinkFlag;
	CDialog::OnTimer(nIDEvent);
}


void CTopMonitoringConfigDlg::OnTopcfgInverse1() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[0])
		m_colorEdit[0].SetBkColor(m_TopCfg.stateConfig[0].BStateColor);
}

void CTopMonitoringConfigDlg::OnTopcfgInverse2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[1])
		m_colorEdit[1].SetBkColor(m_TopCfg.stateConfig[1].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse3() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[2])
		m_colorEdit[2].SetBkColor(m_TopCfg.stateConfig[2].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse4() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[3])
		m_colorEdit[3].SetBkColor(m_TopCfg.stateConfig[3].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse5() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[4])
		m_colorEdit[4].SetBkColor(m_TopCfg.stateConfig[4].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse6() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[5])
		m_colorEdit[5].SetBkColor(m_TopCfg.stateConfig[5].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse7() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[6])
		m_colorEdit[6].SetBkColor(m_TopCfg.stateConfig[6].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse8() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[7])
		m_colorEdit[7].SetBkColor(m_TopCfg.stateConfig[7].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse9() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[8])
		m_colorEdit[8].SetBkColor(m_TopCfg.stateConfig[8].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse10() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[9])
		m_colorEdit[9].SetBkColor(m_TopCfg.stateConfig[9].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse11() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[10])
		m_colorEdit[10].SetBkColor(m_TopCfg.stateConfig[10].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse12() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[11])
		m_colorEdit[11].SetBkColor(m_TopCfg.stateConfig[11].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse13() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[12])
		m_colorEdit[12].SetBkColor(m_TopCfg.stateConfig[12].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse14() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[13])
		m_colorEdit[13].SetBkColor(m_TopCfg.stateConfig[13].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnTopcfgInverse15() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_Flash[14])
		m_colorEdit[14].SetBkColor(m_TopCfg.stateConfig[14].BStateColor);
	
}

void CTopMonitoringConfigDlg::OnVOverBlink() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_bVOverBlink)
		m_VOverResult.SetBkColor(m_TopCfg.VOverConfig.BOverColor);
	
}

void CTopMonitoringConfigDlg::OnIOverBlink() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_bIOverBlink)
		m_IOverResult.SetBkColor(m_TopCfg.IOverConfig.BOverColor);

}


PS_COLOR_CONFIG CTopMonitoringConfigDlg::GetColorConfigData()
{
	return m_TopCfg;
}

void CTopMonitoringConfigDlg::SetColorCfgData(PS_COLOR_CONFIG cfgData)
{
	m_TopCfg = cfgData;
	m_bInitColor = TRUE;
}
