// Table.cpp: implementation of the CTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Table.h"
#include <math.h>
#include <float.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
CTable::CTable(LONG lIndex) : m_lIndex(lIndex)
{
	// 각 Transducer의 마지막 데이터를 얻지 못하였고
	m_bLoadLastData  = FALSE;
	m_pfltLastData   = NULL;
//	m_fltDCIR        = 0.0f;
//	m_fltOCV         = 0.0f;

	// 전체 데이터도 loading되지 않았음을 표시
	m_bLoadData      = FALSE;
	m_lNumOfData     = 0L;
	m_lLoadedDataCount = 0L;
	m_ppfltData      = NULL;
	m_lStepNo        = 0L;
	m_lLastRecordIndex = 0L;
	m_lRecordIndex	= 0L;
	m_lTotCycle = 0L;
	m_wType = PS_STEP_NONE;
}
*/
CTable::CTable(LPCTSTR strTitle, LPCTSTR strFromList)
{
	CString strName(strTitle);		//TableList 파일의 Column Header 목록  
	CString strValue(strFromList);	//현재 Table Index의 값

	// (1) m_strlistTitle
	// -> strName("Table List의 둘째 줄")에서 저장된 각 Transducer의 Name을 List에 넣는다.
	int s=0, p1=0;
	while(p1!=-1)
	{
		p1 = strName.Find(',', s);
		if(p1!=-1)
		{
			CString Title = strName.Mid(s, p1-s);
			s  = p1+1;
			//No(StepNo), DCIR, OCV는 실처리 정보가 아니다.(Step에서 오직 1개의 값만을 갖는다.)
//			if( Title.CompareNoCase("No") != 0 && Title.CompareNoCase("IR")!= 0 && Title.CompareNoCase("OCV") != 0)
			m_strlistTitle.AddTail(Title);

			//////////////////////////////////////////////////////////////////////////
			m_awlistItem.Add(GetItemID(Title));
			//////////////////////////////////////////////////////////////////////////
		}
	}

	// strValue에서 Table의 Index와 각 Transducer의 마지막 값을 얻는다.
	s=0, p1=0;

//	// (2) m_lIndex: Table No
//	p1 = strValue.Find(',', s);
//	m_lIndex = atoi(strValue.Mid(s, p1-s));
//	s  = p1+1;

	// (3) m_pfltLastData: 각 Transducer들의 마지막 값
	int a = m_strlistTitle.GetCount();
	m_pfltLastData = new float[a];
	for(int i=0;i<a;i++)
	{
		p1 = strValue.Find(',', s);
		m_pfltLastData[i] = (float) atof(strValue.Mid(s, p1-s));
		s  = p1+1;
	}
	m_bLoadLastData = TRUE;

	//Step No
	int nIndex1;
//	nIndex1 = FindIndexOfTitle(PS_CHANNEL_NO);

	nIndex1 = FindIndexOfTitle((WORD)PS_STATE);
	if(nIndex1 < 0 )		m_chState = 0;
	else					m_chState = (BYTE)m_pfltLastData[nIndex1];
	
	m_chDataSelect = 0x01;	//SFT_SAVE_STEP_END;

	nIndex1 = FindIndexOfTitle(PS_CODE);
	if(nIndex1 < 0 )		m_chCode = 0;
	else					m_chCode = (BYTE)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_GRADE_CODE);
	if(nIndex1 < 0 )		m_chGradeCode = 0;
	else					m_chGradeCode = (BYTE)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_CUR_CYCLE);
	if(nIndex1 < 0 )		m_nCurrentCycleNum = 0;
	else					m_nCurrentCycleNum = (ULONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_DATA_SEQ);
	if(nIndex1 < 0 )		m_lSaveSequence = 0;
	else					m_lSaveSequence = (ULONG)m_pfltLastData[nIndex1];


	nIndex1 = FindIndexOfTitle(PS_STEP_NO);
	if(nIndex1 < 0 )		m_lStepNo = 0;
	else					m_lStepNo = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_TOT_CYCLE);
	if(nIndex1 < 0 )		m_lTotCycle = 0;
	else					m_lTotCycle = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_STEP_TYPE);
	if(nIndex1 < 0 )		m_wType = 0;
	else					m_wType = (WORD)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle("IndexFrom");
	if(nIndex1 < 0 )		m_lRecordIndex = 0;
	else					m_lRecordIndex = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle("IndexTo");
	if(nIndex1 < 0 )		m_lLastRecordIndex = 0;
	else					m_lLastRecordIndex = (LONG)m_pfltLastData[nIndex1];

	m_lNumOfData = m_lLastRecordIndex-m_lRecordIndex+1;
	m_lLoadedDataCount = m_lNumOfData;

	// 각 Transducer의 마지막 데이터는 얻었고

	// 전체 데이터는 아직 loading하지 않았음을 표시한다.
	m_bLoadData     = FALSE;
	m_ppfltData     = NULL;
}

CTable::CTable(CWordArray *paItem, LPPS_STEP_END_RECORD lpRecord)
{
	ASSERT(lpRecord);
	ASSERT(paItem);
	ASSERT(paItem->GetSize() > 0);
	
	m_awlistItem.Copy(*paItem);
	m_pfltLastData = new float[m_awlistItem.GetSize()];

	LPPS_STEP_END_RECORD_DATA pData = (LPPS_STEP_END_RECORD_DATA)lpRecord;

	
	CString strItemName;
	for(int i = 0; i<m_awlistItem.GetSize(); i++)
	{
		m_pfltLastData[i] = (float)pData->fData[i];

		//////////////////////////////////////////////////////////////////////////
		m_strlistTitle.AddTail(GetItemName(m_awlistItem.GetAt(i)));
		//////////////////////////////////////////////////////////////////////////
	}

	m_bLoadLastData = TRUE;

	//////////////////////////////////////////////////////////////////////////
	m_chNo			= lpRecord->chNo;					// Channel Number
	m_chState		= lpRecord->chState;				// Run, Stop(Manual, Error), End
	m_chDataSelect = lpRecord->chDataSelect;			// For Display Data, For Saving Data
	m_chCode		= lpRecord->chCode;

	m_chGradeCode	= lpRecord->chGradeCode;
	//User grading

	m_nCurrentCycleNum = lpRecord->nCurrentCycleNum;
	m_lSaveSequence = lpRecord->lSaveSequence;			// Expanded Field

	m_lStepNo = lpRecord->chStepNo;
	m_lTotCycle = lpRecord->nTotalCycleNum;
	m_wType = lpRecord->chStepType;
	
	m_lRecordIndex = lpRecord->nIndexFrom;
	m_lLastRecordIndex = lpRecord->nIndexTo;
	m_lNumOfData = m_lLastRecordIndex-m_lRecordIndex+1;
	m_lLoadedDataCount = m_lNumOfData;
	//////////////////////////////////////////////////////////////////////////

	// 전체 데이터는 아직 loading하지 않았음을 표시한다.
	m_bLoadData     = FALSE;
	m_ppfltData     = NULL;
}

CTable::~CTable()
{
	ReleaseRawDataMemory();

	if(m_pfltLastData!=NULL)
	{
		delete [] m_pfltLastData;
		m_pfltLastData = NULL;
	}
}

/*
void CTable::LoadData(LPCTSTR strChPath)
{
	if(m_bLoadData) return;

	// Table file path
	CString strPath;
	strPath.Format("%s\\ADP%04d.cyc", strChPath, m_lIndex);	//각 Step의 Data가 저장되어 있는 파일 

	CFileFind afinder;
	if(afinder.FindFile(strPath))
	{
		afinder.FindNextFile();
		CStdioFile afile;
		if(afile.Open(strPath, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			//
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if(!strContent.IsEmpty()) strlist.AddTail(strContent);
			}
			afile.Close();

			//
			POSITION pos = strlist.GetHeadPosition();
			int s=0,p1=0,p2=0;

			// 첫줄에서
			strContent   = strlist.GetNext(pos);
			s  = strContent.Find("StartT");
			p1 = strContent.Find('=', s);
			p2 = strContent.Find(',', s);
			m_StartTime.ParseDateTime(strContent.Mid(p1+1,p2-p1-1));
			s  = strContent.Find("Step");
			p1 = strContent.Find('=', s);
			p2 = strContent.Find(',', s);
			m_lStepNo = atoi(strContent.Mid(p1+1,p2-p1-1));
			if(!m_bLoadLastData)
			{
				s  = strContent.Find("IR");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_fltDCIR = (float) atof(strContent.Mid(p1+1,p2-p1-1));
				s  = strContent.Find("OCV");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_fltOCV = (float) atof(strContent.Mid(p1+1,p2-p1-1));
			}

			// 둘째줄에서
			strContent = strlist.GetNext(pos);
			if(!m_bLoadLastData)
			{
				s=0, p1=0;
				while(p1!=-1)
				{
					p1 = strContent.Find(',', s);
					m_strlistTitle.AddTail(strContent.Mid(s, p1-s));
					s  = p1+1;
				}
			}

			// 다음줄부터 데이터를 Loading함
			int N        = m_strlistTitle.GetCount();
			m_lNumOfData = strlist.GetCount()-2L;
			if(m_lNumOfData>0)
			{
				m_ppfltData = new float*[N];
				for(int i=0;i<N;i++) m_ppfltData[i] = new float[m_lNumOfData];

				//
				int j = 0;
				while(pos)
				{
					strContent = strlist.GetNext(pos);
					//
					s=0, p1=0;
					for(int i=0; i<N; i++)
					{
						p1 = strContent.Find(',', s);
						float fltData = (float)atof(strContent.Mid(s, p1-s));
						s  = p1+1;
						m_ppfltData[i][j]=fltData;
					}
					//
					j++;
				}
				//

				//TableList 라는 파일에서 읽은 Step의 가장 마지막값을 무시 하고 Step의 가장 마지막값을 Load 한다.
//ksh				if(!m_bLoadLastData)
				{
						if(m_bLoadLastData) delete [] m_pfltLastData;
						m_pfltLastData = new float[N];
//ksh					for(int i=0; i<N; i++) m_pfltLastData[i] = m_ppfltData[i][m_lNumOfData-1];
						for(int i=0; i<N; i++) m_pfltLastData[i] = m_ppfltData[i][m_lNumOfData-2];
				}
				//
				m_bLoadData     = TRUE;
				m_bLoadLastData = TRUE;
			}
		}
	}
}
*/


//ADPower Style data
void CTable::LoadDataA(LPCTSTR strChPath)
{
	if(m_bLoadData)		
		return;

	// Table file path
	CString strPath;
	int i = 0;
//	strPath.Format("%s\\*_%06d.cyc", strChPath, 0/*m_lIndex*/);	//각 Step의 Data가 저장되어 있는 파일 
//자기 Table Data만 Loading 하도록 수정 필요 
	
	strPath.Format("%s\\*.%s", strChPath, PS_RAW_FILE_NAME_EXT);	//각 Step의 Data가 저장되어 있는 파일 

	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)
	{
		return;
	}
	
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	//저장된 Record수를 검색
	if(m_lNumOfData <=0 )
	{
		return;		//실처리로 저장된 Data가 없음 
	}

	//Data를 읽어들임
	FILE *fp = fopen(strPath, "rb");
	if(fp == NULL)		return;

	//File Header를 읽음
	PS_FILE_ID_HEADER fileID;
	if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
	{
		fclose(fp);
		return;
	}
	if(fileID.nFileID != 5640)
	{
		fclose(fp);
		return;
	}

	PS_RECORD_FILE_HEADER *pRSHeader = new PS_RECORD_FILE_HEADER;
	ZeroMemory(pRSHeader, sizeof(PS_RECORD_FILE_HEADER));

	//File Header를 읽음
	if(fread(pRSHeader, sizeof(PS_RECORD_FILE_HEADER), 1, fp) < 1)	
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}

	//저장된 Column수를 구함 
	if(pRSHeader->nColumnCount < 1)		
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}	
	m_DataItemList.RemoveAll();
	for(i=0; i<pRSHeader->nColumnCount; i++)
	{
		m_DataItemList.Add(pRSHeader->awColumnItem[i]);
	}
	delete pRSHeader;

	int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_RECORD_FILE_HEADER);
	//저장된 Data의 크기 
	size_t nRecordSize = sizeof(float)*m_DataItemList.GetSize();

	//주어진 Index로 이동한다.
	fseek(fp, m_lRecordIndex*nRecordSize+nHeaderSize,SEEK_SET);
	
	ASSERT(m_ppfltData == NULL);
	//2차원 동적 배열 
	m_ppfltData = new float*[GetRecordItemSize()];
	for(i=0;i<GetRecordItemSize();i++) m_ppfltData[i] = new float[m_lNumOfData];

	float *fpBuff = new float[GetRecordItemSize()];
	int rsCount = 0;
	while(fread(fpBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lNumOfData)
	{	
		for(int i=0; i<GetRecordItemSize(); i++)
		{
			m_ppfltData[i][rsCount] = fpBuff[i];
		}
		rsCount++;
	}
	delete [] fpBuff;
	fclose(fp);
	m_bLoadData     = TRUE;
}

//ADP Style data
//1. 실처리 Data Loading
//2. Step start data loading

//=> Step 시작 data 포함과 미포함 data 요청 명령이 분리되어야 한다.
//Data 복구시등에서는 Step 시작 data가 포함되지 않은 data를 return하여야 한다.

void CTable::LoadDataB(LPCTSTR strChPath)
{
	//이미 Loading하였거나 저장된 Record가 없음 
	if(m_bLoadData || m_lNumOfData <=0 )	return;		//실처리로 저장된 Data가 없음 

	// Table file path
	CString strTemp;
	CString strPath, strStepStartPath;
	CFileFind afinder;
	FILE *fp = NULL;

	//Raw data file
	strTemp.Format("%s\\*.%s", strChPath, PS_RAW_FILE_NAME_EXT);		//각 Step의 Data가 저장되어 있는 파일 
	if(afinder.FindFile(strTemp) == FALSE)	return;
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	afinder.Close();

	//msec save file
	float fStartData[5][600];
	int nStepStartDataCount = 0;	
	strTemp.Format("%s\\StepStart\\*_C%06d_S%02d.csv", strChPath, GetCycleNo(), GetStepNo());
	if(afinder.FindFile(strTemp))
	{
		afinder.FindNextFile();
		strStepStartPath = afinder.GetFilePath();
		afinder.Close();
		
		fp = fopen(strStepStartPath, "rb");
		if(fp)
		{
			float fStepTime, fDataVtg, fDataCrt, fDataCap, fWattHour;
			//모든 Data를 Laoding한다.
			char szBuff[64];
			if(fscanf(fp, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
			{
				//Line 수 검사 
				while(fscanf(fp, "%f,%f,%f,%f,%f", &fStepTime, &fDataVtg, &fDataCrt, &fDataCap, &fWattHour) > 0)
				{
					fStartData[0][nStepStartDataCount] = fStepTime;
					fStartData[1][nStepStartDataCount] = fDataVtg;
					fStartData[2][nStepStartDataCount] = fDataCrt;
					fStartData[3][nStepStartDataCount] = fDataCap;
					fStartData[4][nStepStartDataCount] = fWattHour;
					nStepStartDataCount++;
					if(nStepStartDataCount >= 600)	break;
				}
			}

			if(fp) fclose(fp);
		}
	}

	//step data를 포함한다.
	m_lLoadedDataCount = m_lNumOfData + nStepStartDataCount;
	
	//Data를 읽어들임
	fp = fopen(strPath, "rb");
	if(fp)
	{
		//File Header를 읽음
		PS_FILE_ID_HEADER fileID;
		if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
		{
			fclose(fp);
			return;
		}
		if(fileID.nFileID != 5640)
		{
			fclose(fp);
			return;
		}

		PS_RECORD_FILE_HEADER *pRSHeader = new PS_RECORD_FILE_HEADER;
		ZeroMemory(pRSHeader, sizeof(PS_RECORD_FILE_HEADER));

		//File Header를 읽음
		if(fread(pRSHeader, sizeof(PS_RECORD_FILE_HEADER), 1, fp) < 1)	
		{
			delete pRSHeader;
			fclose(fp);
			return;
		}

		//저장된 Column수를 구함 
		if(pRSHeader->nColumnCount < 1)		
		{
			delete pRSHeader;
			fclose(fp);
			return;
		}	

		m_DataItemList.RemoveAll();
		int i = 0;
		for(i=0; i<pRSHeader->nColumnCount; i++)
		{
			m_DataItemList.Add(pRSHeader->awColumnItem[i]);
		}
		delete pRSHeader;

		int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_RECORD_FILE_HEADER);
		//저장된 Data의 크기 
		size_t nRecordSize = sizeof(float)*m_DataItemList.GetSize();

		//주어진 Index로 이동한다.
		fseek(fp, m_lRecordIndex*nRecordSize+nHeaderSize,SEEK_SET);
		
		ASSERT(m_ppfltData == NULL);
		//2차원 동적 배열 
		m_ppfltData = new float*[GetRecordItemSize()];
		for(i=0; i<GetRecordItemSize(); i++) 
		{
			m_ppfltData[i] = new float[m_lLoadedDataCount];
		}

		float *fpBuff = new float[GetRecordItemSize()];
		int rsCount = 0;
		int nIndicator = 0;
		
		int time_index = FindItemIndex(PS_STEP_TIME);
		int vtg_index = FindItemIndex(PS_VOLTAGE);
		int crt_index = FindItemIndex(PS_CURRENT);
		int cap_Index = FindItemIndex(PS_CAPACITY);
		int watthour_Index = FindItemIndex(PS_WATT_HOUR);
		int data_Index = FindItemIndex(PS_DATA_SEQ);

		while(fread(fpBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lLoadedDataCount)
		{	
			//Step Start data를 추가한다.
			//시간이 중복되더라도 추가한다.(그래프 그리시 생략)
			while(fStartData[0][nIndicator] <= fpBuff[time_index] &&  nIndicator < nStepStartDataCount)
			{
				if(data_Index >= 0)	m_ppfltData[0][rsCount] = 0;		//Serial을 0으로 한다.
				if(time_index >= 0)	m_ppfltData[time_index][rsCount] = fStartData[0][nIndicator];
				if(vtg_index >= 0)	m_ppfltData[vtg_index][rsCount] = fStartData[1][nIndicator];
				if(crt_index >= 0)	m_ppfltData[crt_index][rsCount] = fStartData[2][nIndicator];
				if(cap_Index >= 0)	m_ppfltData[cap_Index][rsCount] = fStartData[3][nIndicator];
				if(watthour_Index >= 0)	m_ppfltData[watthour_Index][rsCount] = fStartData[4][nIndicator];
				nIndicator++;
				rsCount++;
			}

			for(int i=0; i<GetRecordItemSize(); i++)
			{
				m_ppfltData[i][rsCount] = fpBuff[i];
			}
			rsCount++;
		}
		
		delete [] fpBuff;
		fclose(fp);

		m_bLoadData     = TRUE;
	}
}

//해당 table의 Data 시작 위치를 찾을 때 
//Index를 활용하지 않고 처음 부터 Scan한다.
void CTable::LoadDataC(LPCTSTR strChPath)
{
	if(m_bLoadData)		return;

	// Table file path
	CString strPath;
//	strPath.Format("%s\\*_%06d.cyc", strChPath, 0/*m_lIndex*/);	//각 Step의 Data가 저장되어 있는 파일 
//자기 Table Data만 Loading 하도록 수정 필요 
	
	strPath.Format("%s\\*.%s", strChPath, PS_RAW_FILE_NAME_EXT);	//각 Step의 Data가 저장되어 있는 파일 

	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return;
	
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	//저장된 Record수를 검색
	if(m_lNumOfData <=0 )	return;		//실처리로 저장된 Data가 없음 

	//Data를 읽어들임
	FILE *fp = fopen(strPath, "rb");
	if(fp == NULL)		return;

	//File Header를 읽음
	PS_FILE_ID_HEADER fileID;
	if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
	{
		fclose(fp);
		return;
	}
	if(fileID.nFileID != 5640)
	{
		fclose(fp);
		return;
	}

	PS_RECORD_FILE_HEADER *pRSHeader = new PS_RECORD_FILE_HEADER;
	ZeroMemory(pRSHeader, sizeof(PS_RECORD_FILE_HEADER));

	//File Header를 읽음
	if(fread(pRSHeader, sizeof(PS_RECORD_FILE_HEADER), 1, fp) < 1)	
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}

	//저장된 Column수를 구함 
	if(pRSHeader->nColumnCount < 1)		
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}	
	m_DataItemList.RemoveAll();
	int i=0;
	for(i=0; i<pRSHeader->nColumnCount; i++)
	{
		m_DataItemList.Add(pRSHeader->awColumnItem[i]);
	}
	delete pRSHeader;

	int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_RECORD_FILE_HEADER);

	//저장된 Data의 크기 
	size_t nRecordSize = sizeof(float)*m_DataItemList.GetSize();

	//주어진 Index로 이동한다.
	fseek(fp, m_lRecordIndex*nRecordSize+nHeaderSize,SEEK_SET);
	
	//2차원 동적 배열 
	ASSERT(m_ppfltData == NULL);
	m_ppfltData = new float*[GetRecordItemSize()];
	for( i=0; i<GetRecordItemSize(); i++) 
	{
		m_ppfltData[i] = new float[m_lNumOfData];
	}

	//File read buffer
	float *fpBuff = new float[GetRecordItemSize()];
	
	int rsCount = 0;
	//read file
	while(fread(fpBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lNumOfData)
	{	
		for(int i=0; i<GetRecordItemSize(); i++)
		{
			m_ppfltData[i][rsCount] = fpBuff[i];
		}
		rsCount++;
	}

	//release buffer
	delete [] fpBuff;

	//close file
	fclose(fp);

	m_bLoadData     = TRUE;
}

int CTable::FindItemIndex(WORD nItem)
{
	for(int a=0; a<m_DataItemList.GetSize(); a++)
	{
		if(m_DataItemList.GetAt(a) == nItem)
		{
			return a;
			break;
		}
	}
	return -1;
}

		
float CTable::GetLastData(LPCTSTR strTitle, WORD wPoint)
{
	if(!m_bLoadLastData) return FLT_MAX;

	int nIndex1 =0, nIndex2 = 0, nIndex3 = 0, nIndex4 = 0;
	//sbc에서 계산한값을 사용하도록 변경 
	//2006/8/4
/*	if(CString(strTitle).CompareNoCase(RS_COL_WATT)==0)
	{
		nIndex1 = FindIndexOfTitle(PS_VOLTAGE);
		nIndex2 = FindIndexOfTitle(PS_CURRENT);
		if(nIndex1 < 0 || nIndex2 < 0)		return 0.0f;

		return m_pfltLastData[nIndex1]*m_pfltLastData[nIndex2];
	}
	else 
*/
	if(CString(strTitle).CompareNoCase("V_MaxDiff")==0)
	{
		nIndex1 = FindIndexOfTitle(RS_COL_VOLTAGE1);
		nIndex2 = FindIndexOfTitle(RS_COL_VOLTAGE2);
		nIndex3 = FindIndexOfTitle(RS_COL_VOLTAGE3);
		nIndex4 = FindIndexOfTitle(RS_COL_VOLTAGE4);

		if(nIndex1 < 0 || nIndex2 < 0 || nIndex3< 0 || nIndex4 < 0)		return 0.0f;

		float fltV1 = m_pfltLastData[nIndex1];
		float fltV2 = m_pfltLastData[nIndex2];
		float fltV3 = m_pfltLastData[nIndex3];
		float fltV4 = m_pfltLastData[nIndex4];
		//
		BYTE byVPoint = LOBYTE(wPoint);
		if     (byVPoint==0x03) // 1번-2번
		{
			return (float)fabs(fltV1-fltV2);
		}
		else if(byVPoint==0x05) // 1번-3번
		{
			return (float)fabs(fltV1-fltV3);
		}
		else if(byVPoint==0x09) // 1번-4번
		{
			return (float)fabs(fltV1-fltV4);
		}
		else if(byVPoint==0x06) // 2번-3번
		{
			return (float)fabs(fltV2-fltV3);
		}
		else if(byVPoint==0x0A) // 2번-4번
		{
			return (float)fabs(fltV2-fltV4);
		}
		else if(byVPoint==0x0C) // 3번-4번
		{
			return (float)fabs(fltV3-fltV4);
		}
		else if(byVPoint==0x07) // 1번-2번-3번
		{
			return __max(__max(fltV1,fltV2),fltV3)-__min(__min(fltV1,fltV2),fltV3);
		}
		else if(byVPoint==0x0B) // 1번-2번-4번
		{
			return __max(__max(fltV1,fltV2),fltV4)-__min(__min(fltV1,fltV2),fltV4);
		}
		else if(byVPoint==0x0D) // 1번-3번-4번
		{
			return __max(__max(fltV1,fltV3),fltV4)-__min(__min(fltV1,fltV3),fltV4);
		}
		else if(byVPoint==0x0E) // 2번-3번-4번
		{
			return __max(__max(fltV2,fltV3),fltV4)-__min(__min(fltV2,fltV3),fltV4);
		}
		else if(byVPoint==0x0F) // 1번-2번-3번-4번
		{
			return __max(__max(__max(fltV1,fltV2),fltV3),fltV4)-__min(__min(__min(fltV1,fltV2),fltV3),fltV4);
		}
		else                    // 한개 선택 or 선택하지 않음
		{
			return 0.0f;
		}
	}
	else if(CString(strTitle).CompareNoCase("T_MaxDiff")==0)
	{
		nIndex1 = FindIndexOfTitle(RS_COL_TEMPERATURE1);
		nIndex2 = FindIndexOfTitle(RS_COL_TEMPERATURE2);
		if(nIndex1 < 0 || nIndex2 < 0 || nIndex3< 0 || nIndex4 < 0)		return 0.0f;

		float fltT1 = m_pfltLastData[nIndex1];
		float fltT2 = m_pfltLastData[nIndex2];
		//
		BYTE byTPoint = HIBYTE(wPoint);
		if(byTPoint==0x03) // 1번-2번
		{
			return (float)fabs(fltT1-fltT2);
		}
		else               // 한개 선택 or 선택하지 않음
		{
			return 0.0f;
		}
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_OCV)==0)	//첫번째 전압이 OCV임 
	{
		//OCV 항목을 그리기 이전에 반드시 LoadDataA()를 먼저 호출 하여야 한다.
		if(m_bLoadData == FALSE)
		{
			return 0.0f;
		}
		nIndex1 = FindIndexOfTitle(PS_VOLTAGE);
		if(nIndex1 < 0)	return 0.0f;
		else 		return m_ppfltData[nIndex1][0];		//재일 첫번째 전압이 OCV이다.
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_TYPE)==0)
	{
		return (float)GetType();
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_STEP_NO)==0)
	{
		return (float)GetStepNo();
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_TOT_CYCLE)==0)
	{
		return (float)GetCycleNo();
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_STATE)==0)
	{
		return (float)m_chState;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_CUR_CYCLE)==0)
	{
		return (float)m_nCurrentCycleNum;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_CODE)==0)
	{
		return (float)m_chCode;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_GRADE)==0)
	{
		return (float)m_chGradeCode;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_CH_NO)==0)
	{
		return (float)m_chNo;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_SAVE_ACCU)==0)
	{
		return (float)m_chDataSelect;
	}
	else if(CString(strTitle).CompareNoCase(RS_COL_SEQ)==0)
	{
		return (float)m_lSaveSequence;
	}

	//default data
	nIndex1 = FindIndexOfTitle(strTitle);
	if(nIndex1 < 0 )		return 0.0f;

	return m_pfltLastData[nIndex1];
}

fltPoint* CTable::GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
{
	if(!m_bLoadData) return NULL;

	int iXIndex=0, iYIndex1=0, iYIndex2=0, iYIndex3=0, iYIndex4=0;
	
	iXIndex = FindRsColumnIndex(strXAxisTitle);

	if(CString(strYAxisTitle).CompareNoCase(RS_COL_WATT)==0)
	{
		iYIndex1 = FindRsColumnIndex(PS_VOLTAGE);
		iYIndex2 = FindRsColumnIndex(PS_CURRENT);
	}
// 	else if(CString(strYAxisTitle).CompareNoCase(RS_COL_FARAD)==0)
// 	{
// 		iYIndex1 = FindRsColumnIndex(PS_WATT);
// 	}
	else if(CString(strYAxisTitle).CompareNoCase("V_MaxDiff")==0)
	{
		iYIndex1 = FindRsColumnIndex(RS_COL_VOLTAGE1);
		iYIndex2 = FindRsColumnIndex(RS_COL_VOLTAGE2);
		iYIndex3 = FindRsColumnIndex(RS_COL_VOLTAGE3);
		iYIndex4 = FindRsColumnIndex(RS_COL_VOLTAGE4);
	}
	else if(CString(strYAxisTitle).CompareNoCase("T_MaxDiff")==0)
	{
		iYIndex1 = FindRsColumnIndex(RS_COL_TEMPERATURE1);
		iYIndex2 = FindRsColumnIndex(RS_COL_TEMPERATURE2);
	}
	else 
	{
		iYIndex1 = FindRsColumnIndex(strYAxisTitle);
	}

	fltPoint* pfltPt = NULL;
	//Data가 저장되어 있지 않을 경우 
	if(iXIndex < 0 || iYIndex1 < 0 || iYIndex2 < 0 || iYIndex3 < 0 || iYIndex4 < 0)
	{
		lDataNum = 0;
		return pfltPt;
	}

	//
	lDataNum = m_lLoadedDataCount;
	pfltPt   = new fltPoint[lDataNum];
	//
	for(int i=0; i<m_lLoadedDataCount; i++)
	{
		//
		// X축값
		//
		pfltPt[i].x = m_ppfltData[iXIndex][i];

		//
		// Y축값
		//
		if(CString(strYAxisTitle).CompareNoCase(RS_COL_WATT)==0)
		{
			//Bug Fixed 2006/5/8  
			//Watt / 1000.0f
			pfltPt[i].y = m_ppfltData[iYIndex1][i]*m_ppfltData[iYIndex2][i]/1000.0f;
		}
		else if(CString(strYAxisTitle).CompareNoCase("V_MaxDiff")==0)
		{
			BYTE byVPoint = LOBYTE(wPoint);
			if     (byVPoint==0x03) // 1번-2번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex2][i]);
			}
			else if(byVPoint==0x05) // 1번-3번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex3][i]);
			}
			else if(byVPoint==0x09) // 1번-4번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x06) // 2번-3번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex2][i]-m_ppfltData[iYIndex3][i]);
			}
			else if(byVPoint==0x0A) // 2번-4번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex2][i]-m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0C) // 3번-4번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex3][i]-m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x07) // 1번-2번-3번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i])
					       -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]);
			}
			else if(byVPoint==0x0B) // 1번-2번-4번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0D) // 1번-3번-4번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0E) // 2번-3번-4번
			{
				pfltPt[i].y=__max(__max(m_ppfltData[iYIndex2][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(m_ppfltData[iYIndex2][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
			}
			else if(byVPoint==0x0F) // 1번-2번-3번-4번
			{
				pfltPt[i].y=__max(__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
					       -__min(__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
			}
			else                    // 한개 선택 or 선택하지 않음
			{
				pfltPt[i].y=0.0f;
			}
		}
		else if(CString(strYAxisTitle).CompareNoCase("T_MaxDiff")==0)
		{
			BYTE byTPoint = HIBYTE(wPoint);
			if(byTPoint==0x03) // 1번-2번
			{
				pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex2][i]);
			}
			else               // 한개 선택 or 선택하지 않음
			{
				pfltPt[i].y=0.0f;
			}
		}
		else
		{
			pfltPt[i].y = m_ppfltData[iYIndex1][i];
		}
	}

	//
	return pfltPt;
}

//최종 결과의 Column의 Index를 구한다.
int CTable::FindIndexOfTitle(LPCTSTR title)
{
	BOOL bCheck = FALSE;
	int i = 0;
	for(i=0; i<m_strlistTitle.GetCount(); i++)
	{
		if(m_strlistTitle.GetAt(m_strlistTitle.FindIndex(i)).CompareNoCase(title)==0)
		{
			bCheck = TRUE;
			break;
		}
	}

	if(bCheck) return i;
	
	return -1;
}


//최종 결과의 Column의 Index를 구한다.
int CTable::FindIndexOfTitle(WORD wItem)
{
//	BOOL bCheck = FALSE;
	for(int i=0; i<m_awlistItem.GetSize(); i++)
	{
		if(m_awlistItem.GetAt(i) == wItem)
		{
			return i;
		}
	}
	return -1;
}

//실처리 Data의 Column Index를 구한다.
long CTable::FindRsColumnIndex(WORD nItem)
{
	for(int i=0; i<GetRecordItemSize(); i++)
	{
		if(nItem == m_DataItemList.GetAt(i))
		{
			return i;
		}
	}

	return -1;
//	return 0;
}


long CTable::FindRsColumnIndex(LPCTSTR szTitle)
{

	if(!CString(szTitle).CompareNoCase(RS_COL_VOLTAGE))
	{
		return FindRsColumnIndex(PS_VOLTAGE);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_CURRENT))
	{
		return FindRsColumnIndex(PS_CURRENT);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_CAPACITY))
	{
		return FindRsColumnIndex(PS_CAPACITY);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_TIME))
	{
		return FindRsColumnIndex(PS_STEP_TIME);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_SEQ))
	{
//		int a = FindRsColumnIndex(PS_DATA_INDEX);
		return FindRsColumnIndex(PS_DATA_SEQ);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_WATT))
	{
		return FindRsColumnIndex(PS_WATT);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_WATT_HOUR))
	{
		return FindRsColumnIndex(PS_WATT_HOUR);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_TEMPERATURE))
	{
		return FindRsColumnIndex(PS_TEMPERATURE);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_PRESSURE))
	{
		return FindRsColumnIndex(PS_PRESSURE);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_AVG_VTG))
	{
		return FindRsColumnIndex(PS_AVG_VOLTAGE);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_AVG_CRT))
	{
		return FindRsColumnIndex(PS_AVG_CURRENT);
	}
	

//	return 0;
	return -1;
}


void CTable::ReleaseRawDataMemory()
{
	//
	if(m_ppfltData!=NULL)
	{
//		for(int i=0; i<m_strlistTitle.GetCount(); i++)
//		{
//			if(m_ppfltData[i]!=NULL) delete [] m_ppfltData[i];
//		}
//		delete [] m_ppfltData;
		
		for(int i=0; i<GetRecordItemSize(); i++)
		{
			if(m_ppfltData[i]!=NULL)
			{
				delete [] m_ppfltData[i];
				m_ppfltData[i] = NULL;
			}
		}
		delete [] m_ppfltData;
		m_ppfltData = NULL;

	}
}

LONG CTable::GetRecordCount()
{
	return m_lNumOfData;
}

WORD CTable::GetItemID(CString strTitle)
{
//	if(str)
//	No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temp,Press,AvgVoltage,AvgCurrent,
	if(!strTitle.CompareNoCase(RS_COL_VOLTAGE))
	{
		return PS_VOLTAGE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CURRENT))
	{
		return PS_CURRENT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CAPACITY))
	{
		return PS_CAPACITY;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TIME))
	{
		return PS_STEP_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_WATT))
	{
		return PS_WATT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_WATT_HOUR))
	{
		return PS_WATT_HOUR;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TEMPERATURE))
	{
		return PS_TEMPERATURE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_PRESSURE))
	{
		return PS_PRESSURE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_SEQ))
	{
		return PS_DATA_SEQ;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AVG_VTG))
	{
		return PS_AVG_VOLTAGE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AVG_CRT))
	{
		return PS_AVG_CURRENT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_STEP_NO))
	{
		return PS_STEP_NO;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CUR_CYCLE))
	{
		return PS_CUR_CYCLE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TOT_CYCLE))
	{
		return PS_TOT_CYCLE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TYPE))
	{
		return PS_STEP_TYPE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_STATE))
	{
		return PS_STATE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TOT_TIME))
	{
		return PS_TOT_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CODE))
	{
		return PS_CODE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_GRADE))
	{
		return PS_GRADE_CODE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_IR))
	{
		return PS_IMPEDANCE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CH_NO))
	{
		return PS_CHANNEL_NO;
	}

	return 0xffff;
}

CString CTable::GetItemName(WORD wItem)
{
	switch (wItem)
	{
	case PS_VOLTAGE:		return RS_COL_VOLTAGE;
	case PS_CURRENT:		return RS_COL_CURRENT;
	case PS_CAPACITY:		return RS_COL_CAPACITY;
	case PS_STEP_TIME:		return RS_COL_TIME;
	case PS_WATT:			return RS_COL_WATT;
	case PS_WATT_HOUR:		return RS_COL_WATT_HOUR;
	case PS_TEMPERATURE:	return RS_COL_TEMPERATURE;
	case PS_PRESSURE:		return RS_COL_PRESSURE;
	case PS_DATA_SEQ:		return RS_COL_SEQ;
	case PS_AVG_VOLTAGE:	return RS_COL_AVG_VTG;
	case PS_AVG_CURRENT:	return RS_COL_AVG_CRT;
	case PS_STEP_NO:		return RS_COL_STEP_NO;

	case PS_CUR_CYCLE:		return RS_COL_CUR_CYCLE;
	case PS_TOT_CYCLE:		return RS_COL_TOT_CYCLE;
	case PS_STEP_TYPE:		return RS_COL_TYPE;
	case PS_STATE:			return RS_COL_STATE;
	case PS_TOT_TIME:		return RS_COL_TOT_TIME;
	case PS_CODE:			return RS_COL_CODE;
	case PS_GRADE_CODE:		return RS_COL_GRADE;
	case PS_IMPEDANCE:		return RS_COL_IR;

	case PS_CHANNEL_NO:		return RS_COL_CH_NO;
	}
	return "Unknown";
}

