// Grading.cpp: implementation of the CGrading class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Grading.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGrading::CGrading()
{

}

CGrading::~CGrading()
{
	m_ptArray.RemoveAll();
}

CString CGrading::GetGradeCode(float fData)
{
	for(int i=0; i<m_ptArray.GetSize(); i++)
	{
		GRADE_STEP& stepData = m_ptArray.ElementAt(i);

		if(stepData.fMin <= fData && stepData.fMax > fData)
		{
			return stepData.strCode;
		}
	}
	return "";
}

BOOL CGrading::AddGradeStep(long litem, float fMin, float fMax, CString Code)
{
//	if(fMin >= fMax || Code.IsEmpty())	return FALSE;
	
	GRADE_STEP step;
	step.lGradeItem =litem;
	step.fMin = fMin;
	step.fMax = fMax;
	step.strCode = Code;
	m_ptArray.Add(step);

	return FALSE;
}

BOOL CGrading::ClearStep()
{
//	m_lGradingItem = 0;
	m_ptArray.RemoveAll();
	return TRUE;
}
/*
void CGrading::operator=(const CGrading grading) 
{

}
*/
int CGrading::GetGradeStepSize()
{
	return m_ptArray.GetSize();
}

long CGrading::GetGradeItem(int iPos)
{
	return m_ptArray.GetAt(iPos).lGradeItem;
}

GRADE_STEP CGrading::GetStepData(int nIndex)
{
	ASSERT(nIndex >= 0 && nIndex <GetGradeStepSize());
	
	GRADE_STEP stepData = m_ptArray.GetAt(nIndex);
	return stepData;
}

BOOL CGrading::SetGradingData(CGrading &grading)
{
	ClearStep();
	
	GRADE_STEP step;
	for(int i = 0; i<grading.GetGradeStepSize(); i++)
	{
		step = grading.GetStepData(i);
		m_ptArray.Add(step);
	}
//	m_lGradingItem = grading.m_lGradingItem;
	return TRUE;
}
