/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: ChData.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CChData class is defined
*************************************************************/

// ChData.h: interface for the CChData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHDATA_H__40068523_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CHDATA_H__40068523_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include
class CTable;
class CScheduleData;
//class CPattern;

// #define

// Class definition
//---------------------------------------------------------------------------
//	Class Name: CChData
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  The first settlement
//---------------------------------------------------------------------------
class AFX_EXT_CLASS CChData  
{
////////////////
// Attributes //
////////////////
public:
	CString                          m_strPath;   // Path of Channel Data
//	CPattern*                        m_pPattern;  // Applied Pattern
	CScheduleData*                   m_pSchedule;  // Applied Pattern
	CTypedPtrList<CPtrList, CTable*> m_TableList; // Table List

private:
	CString	m_strUserID;
	CString m_strDescript;
	CString m_strSerial;
	CString m_strEndT;
	CString m_strStartT;

//	char m_szEndT[64];
//	char m_szStartT[64];


///////////////////////////////////////////////////////////
// Operations: "Construction, Destruction, and Creation" //
///////////////////////////////////////////////////////////

public:
	CString		GetTrayNo()		{		return m_strTrayNo;	}
	CString		GetStartTime()	{		return m_strStartT;	}
	CString		GetEndTime()	{		return m_strEndT;	}
	CString		GetTestSerial()	{		return m_strSerial;	}
	CString		GetUserID()		{		return m_strUserID;	}
	CString		GetDescript()	{		return m_strDescript;	}
	CChData()	{		m_pSchedule = NULL;	};
	CChData(LPCTSTR strPath);
	virtual ~CChData();
	BOOL Create(BOOL bIncludeWorkingStep = TRUE);
	BOOL CreateA(BOOL bIncludeWorkingStep = TRUE);
	

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////

public:

	// m_strPath
	CString GetPath() { return m_strPath; };

	// m_pPattern
	CScheduleData* GetPattern();

///////////////////////////////////
// Operations: "Other functions" //
///////////////////////////////////

public:
	BOOL UpdateDCIRData(LONG lIndex, float fData);
	BOOL UpdateTestInfo(CString strTray, CString strCellSerial);
	int GetChannelNo();
	int GetModuleID();
	CString GetTestName();
	
	static CTable * GetLastDataRecord(CString strFileName);
	CTable * GetTableAt(LONG lIndex);
	CTable * GetNextTable(POSITION &pos);
	POSITION GetFirstTablePosition();
	CWordArray *	GetRecordItemList(LONG lIndex);
	CString GetLogFileName();
	float GetCapacitySum(int nStepIndex = -1);
	float GetWattHourSum(int nStepIndex = -1);						// 2009_02_17 kky
	long	GetTotalRecordCount()	{	return	m_nTotalRecordCount;	}	//Step start data가 포함이 안된 record의 크기
	BOOL ReLoadData();
	long GetFirstTableIndexOfCycle(LONG lCycleID);
	LONG GetTableIndex(int nNo);
	LONG	GetTableNo(int nIndex);
	int		GetTableSize()	{	return m_TableList.GetCount();	};
	BOOL	IsOKCell();
	void	SetPath(CString strPath)	{ m_strPath = strPath;	};
	BOOL	IsLoadedData()	{	if(m_strStartT.IsEmpty())	return FALSE;		return TRUE; };

	float	  GetLastDataOfCycle(LONG lCycleID, DWORD dwMode, LPCTSTR strYAxisTitle);
	float     GetLastDataOfTable(LONG lIndex, LPCTSTR strTitle, WORD wPoint=0x0000);

	fltPoint* GetDataOfTable(LONG lIndex, LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);
	fltPoint* GetDataOfCycle(LONG lCycleID, LONG& lDataNum, DWORD dwMode, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);
	
protected:
	CString m_strTrayNo;
//	float m_fCapacitySum;
	long m_nTotalRecordCount;
	void ResetData();
};

#endif // !defined(AFX_CHDATA_H__40068523_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
