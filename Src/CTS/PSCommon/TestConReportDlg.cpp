// TestConReportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TestConReportDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <math.h>


/////////////////////////////////////////////////////////////////////////////
// CTestConReportDlg dialog

CTestConReportDlg::CTestConReportDlg(CScheduleData* sData, CWnd* pParent /*=NULL*/)
	: CDialog(CTestConReportDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestConReportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pData = sData;
	m_nCurStepIndex = 0;
	ASSERT(m_pData);

/*	char szBuff[32], szUnit[16], szDecimalPoint[16];
	m_nCurrentUnitMode = AfxGetApp()->GetProfileInt("Config", "Crt Unit Mode", 0);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWUnit = szUnit;
	m_nWDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "Wh Unit", "mWh 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWhUnit = szUnit;
	m_nWhDecimal = atoi(szDecimalPoint);


	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);
*/

}


void CTestConReportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestConReportDlg)
	DDX_Control(pDX, IDC_GRADE_LIST, m_ctrlGrade);
	DDX_Control(pDX, IDC_STEP_LIST, m_ctrlList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestConReportDlg, CDialog)
	//{{AFX_MSG_MAP(CTestConReportDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_STEP_LIST, OnItemchangedStepList)
	ON_BN_CLICKED(IDC_EXCEL_BUTTON, OnExcelButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestConReportDlg message handlers

BOOL CTestConReportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	Init();	
	
	GetDlgItem(IDC_EDIT_MODELNAME)->SetWindowText(m_pData->GetModelName());
	GetDlgItem(IDC_EDIT_MODEL_DEACRIPT)->SetWindowText(m_pData->GetModelDescript());

	GetDlgItem(IDC_EDIT_SCHNAME)->SetWindowText(m_pData->GetScheduleName());
	GetDlgItem(IDC_EDIT_AUTHOR)->SetWindowText(m_pData->GetScheduleCreator());
	GetDlgItem(IDC_EDIT_DEACRIPT)->SetWindowText(m_pData->GetScheduleDescript());
	GetDlgItem(IDC_EDIT_DATE)->SetWindowText(m_pData->GetScheduleEditTime());

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTestConReportDlg::Init()
{
	DWORD style = 	m_ctrlList.GetExtendedStyle();
	char szBuff[32];
	
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_ctrlList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

	m_imgList.Create(IDB_CELL_STATE_ICON_P, 19, 14, RGB(255,255,255));
	m_ctrlList.SetImageList(&m_imgList, LVSIL_SMALL);

	// Column 삽입
	m_ctrlList.InsertColumn(0, "No",  LVCFMT_RIGHT,  40,  0);
	m_ctrlList.InsertColumn(1, "Type",  LVCFMT_CENTER,  90,  1);
	sprintf(szBuff, "전압(%s)", m_UnitTrans.GetUnitString(PS_VOLTAGE));
	m_ctrlList.InsertColumn(2, szBuff,  LVCFMT_RIGHT,  60,  2);
	sprintf(szBuff, "전류(%s)", m_UnitTrans.GetUnitString(PS_CURRENT));
	m_ctrlList.InsertColumn(3, szBuff,  LVCFMT_RIGHT,  60,  3);
	m_ctrlList.InsertColumn(4, "온도(℃)",  LVCFMT_RIGHT,  60,  3);

	//Grade grid 
	m_ctrlGrade.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);
	// Column 삽입
	m_ctrlGrade.InsertColumn(0, "No",  LVCFMT_RIGHT,  30,  0);
	m_ctrlGrade.InsertColumn(1, "하한값",  LVCFMT_RIGHT,  80,  1);
	m_ctrlGrade.InsertColumn(2, "상한값",  LVCFMT_RIGHT,  80,  2);
	m_ctrlGrade.InsertColumn(3, "Code",  LVCFMT_RIGHT,  40,  3);

	//////////////////////////////////////////////////////////////////////////
/*	CString str;
	m_Grid.EnableDragAndDrop(FALSE);
 	m_Grid.SetEditable(TRUE);
	m_Grid.SetVirtualMode(FALSE);
	
	//Cell의 색상 
	m_Grid.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
   
	//여러 Row와 Column을 동시에 선택 가능하도록 한다.
	m_Grid.SetSingleRowSelection(FALSE);
	m_Grid.SetSingleColSelection(FALSE);
	m_Grid.SetHeaderSort(FALSE);	//Sort기능 사용안함 
	
	//크기 조정을 사용자가 못하도록 한다.
	m_Grid.SetColumnResize(FALSE);
	m_Grid.SetRowResize(FALSE);
	
	//Header 부분을 각각 1칸씩 사용한다.
	m_Grid.SetFixedRowCount(1); 
    m_Grid.SetFixedColumnCount(1); 

	m_Grid.SetColumnCount(4);
	m_Grid.SetRowCount(20);

	m_Grid.SetItemText(0, 0, "No");
	m_Grid.SetItemFormat(0, 0, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	m_Grid.SetItemText(0, 1, "하한값");
	m_Grid.SetItemFormat(0, 1, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	m_Grid.SetItemText(0, 2, "상한값");
	m_Grid.SetItemFormat(0, 2, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	m_Grid.SetItemText(0, 3, "CODE");
	m_Grid.SetItemFormat(0, 3, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	
	m_Grid.SetColumnWidth(0, 30);
	m_Grid.SetColumnWidth(1, 50);
	m_Grid.SetColumnWidth(2, 50);
	m_Grid.SetColumnWidth(3, 30);
	//////////////////////////////////////////////////////////////////////////
*/
	
//	IDC_EDIT_SCHNAME

	CStep *pStep;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	for(int step =0; step < m_pData->GetStepSize(); step++)
	{
		lvItem.iItem = step;
		lvItem.iSubItem = 0;
		lvItem.iImage = -1;
		sprintf(szBuff,"%d", step+1);
		lvItem.pszText = szBuff; 
		
		//20080821 Added by KBH
		//현재 Step 표시
		if(m_nCurStepIndex == step)
		{
			lvItem.iImage = 9;
		}

		m_ctrlList.InsertItem(&lvItem);
		m_ctrlList.SetItemData(lvItem.iItem, step);
		
		pStep = m_pData->GetStepData(step);
		ASSERT(pStep);
		
		lvItem.iSubItem = 1;
		if(pStep->m_type == PS_STEP_CHARGE)				lvItem.iImage = 2;
		else if(pStep->m_type == PS_STEP_DISCHARGE)		lvItem.iImage = 1;
		else if(pStep->m_type == PS_STEP_REST)			lvItem.iImage = 11;
		else if(pStep->m_type == PS_STEP_OCV)			lvItem.iImage = 7;
		else if(pStep->m_type == PS_STEP_IMPEDANCE)		lvItem.iImage = 10;
		else if(pStep->m_type == PS_STEP_END)			lvItem.iImage = 4;		
		else if(pStep->m_type == PS_STEP_BALANCE)		lvItem.iImage = 13;
		else lvItem.iImage = 8;

		sprintf(szBuff,"%s", 	m_pData->GetTypeString(pStep->m_type));
		lvItem.pszText = szBuff; 
		m_ctrlList.SetItem(&lvItem);
		
		if( pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE || pStep->m_type == PS_STEP_BALANCE ||
			(pStep->m_type == PS_STEP_IMPEDANCE && pStep->m_mode == PS_MODE_DCIMP))
		{
			//sprintf(szBuff,"%.1f", 	pStep->m_fVref);
			m_ctrlList.SetItemText(lvItem.iItem, 2, ValueString(pStep->m_fVref, PS_VOLTAGE));

			//sprintf(szBuff,"%.1f", 	pStep->m_fIref);
			m_ctrlList.SetItemText(lvItem.iItem, 3, ValueString(pStep->m_fIref, PS_CURRENT));
		}
		
		if(pStep->m_type != PS_STEP_ADV_CYCLE && pStep->m_type != PS_STEP_LOOP && pStep->m_type != PS_STEP_END)
		{
			m_ctrlList.SetItemText(lvItem.iItem, 4, ValueString(pStep->m_fTref, PS_TEMPERATURE));
		}
	}

	if(m_pData->GetStepSize() > 0)
	{
		m_ctrlList.SetItemState(m_nCurStepIndex, LVIS_SELECTED, LVIS_SELECTED);
		SetStepData(m_nCurStepIndex);
	}
}


void CTestConReportDlg::SetStepData(int nIndex)
{
	CStep *pStep;

	pStep = m_pData->GetStepData(nIndex);
	if(pStep == NULL)	return;

	CString strCmd;
//	strCmd.Format("%3.3f", pStep->m_);
//	SetDlgItemText(IDC_EDIT_C_REFV, strCmd);
//	strCmd.Format("%3.3f", (float)m_pData->spStepData[nCIndex].lIRef/DIVIDE_CURRENT);
//	SetDlgItemText(IDC_EDIT_C_REFI, strCmd);

	strCmd.Format("Step %d 적용 조건", nIndex+1);
	SetDlgItemText(IDC_STEP_STATIC, strCmd);
	
	//safty condition
//	strCmd.Format("%3.1f", pStep->m_fLowLimitV);
	SetDlgItemText(IDC_EDIT_SAFEVTGMIN, ValueString(pStep->m_fLowLimitV, PS_VOLTAGE, TRUE));
//	strCmd.Format("%3.1f", pStep->m_fHighLimitV);
	SetDlgItemText(IDC_EDIT_SAFEVTGMAX, ValueString(pStep->m_fHighLimitV, PS_VOLTAGE, TRUE));

//	strCmd.Format("%.1f", pStep->m_fLowLimitI);
	SetDlgItemText(IDC_EDIT_SAFECRTMIN, ValueString(pStep->m_fLowLimitI, PS_CURRENT, TRUE));
//	strCmd.Format("%.1f", pStep->m_fHighLimitI);
	SetDlgItemText(IDC_EDIT_SAFECRTMAX, ValueString(pStep->m_fHighLimitI, PS_CURRENT, TRUE));

//	strCmd.Format("%.1f", pStep->m_fLowLimitC);
	SetDlgItemText(IDC_EDIT_SAFECAPMIN, ValueString(pStep->m_fLowLimitC, PS_CAPACITY, TRUE));
//	strCmd.Format("%.1f", pStep->m_fHighLimitC);
	SetDlgItemText(IDC_EDIT_SAFECAPMAX, ValueString(pStep->m_fHighLimitC, PS_CAPACITY, TRUE));

//	strCmd.Format("%.1f", pStep->m_fHighLimitImp);
	SetDlgItemText(IDC_EDIT_SAFEIMPMIN, ValueString(pStep->m_fHighLimitImp, PS_IMPEDANCE, TRUE));
//	strCmd.Format("%.1f", pStep->m_fLowLimitImp);
	SetDlgItemText(IDC_EDIT_SAFEIMPMAX, ValueString(pStep->m_fLowLimitImp, PS_IMPEDANCE, TRUE));

	SetDlgItemText(IDC_EDIT_SAFEWATTMIN, ValueString(pStep->m_fHighLimitTemp, PS_TEMPERATURE, TRUE));
	SetDlgItemText(IDC_EDIT_SAFEWATTMAX, ValueString(pStep->m_fLowLimitTemp, PS_TEMPERATURE, TRUE));
	
	//Report Condition
//	strCmd.Format("%3.1f", pStep->m_fReportV);
	SetDlgItemText(IDC_EDIT_C_SAVEV, ValueString(pStep->m_fReportV, PS_VOLTAGE, TRUE));
//	strCmd.Format("%3.1f", pStep->m_fReportI);
	SetDlgItemText(IDC_EDIT_C_SAVEI, ValueString(pStep->m_fReportI, PS_CURRENT, TRUE));
//	CTimeSpan t(pStep->m_ulReportTime/100);
//	strCmd.Format("%s.%02d", t.Format("%H:%M:%S"), pStep->m_ulReportTime%100);
	SetDlgItemText(IDC_EDIT_C_SAVET, ValueString(pStep->m_fReportTime, PS_STEP_TIME, TRUE));
	SetDlgItemText(IDC_EDIT_T_SAVET, ValueString(pStep->m_fReportTemp, PS_TEMPERATURE, TRUE));
	
	//End Condition
//	strCmd.Format("%3.1f", pStep->m_fEndV);
	SetDlgItemText(IDC_EDIT_C_ENDV, ValueString(pStep->m_fEndV, PS_VOLTAGE, TRUE));
//	strCmd.Format("%3.1f", pStep->m_fEndI);
	SetDlgItemText(IDC_EDIT_C_ENDI, ValueString(pStep->m_fEndI, PS_CURRENT, TRUE));
	strCmd.Format("%3.1f", pStep->m_fEndC);
	SetDlgItemText(IDC_EDIT_C_ENDCAP, ValueString(pStep->m_fEndC, PS_CAPACITY, TRUE));
/*	CTimeSpan endT( pStep->m_ulEndTime/100);
	if(endT.GetDays() > 0)
	{
		strCmd.Format("%s.%02d", endT.Format("%dD %H:%M:%S"), pStep->m_ulEndTime%100);
	}
	else
	{
		strCmd.Format("%s.%02d", endT.Format("%H:%M:%S"), pStep->m_ulEndTime%100);
	}
*/	SetDlgItemText(IDC_EDIT_C_ENDTIME, ValueString(pStep->m_fEndTime, PS_STEP_TIME, TRUE));

	strCmd.Format("%.3f", pStep->m_fEndDV);
	SetDlgItemText(IDC_EDIT_C_ENDDELTAV, ValueString(pStep->m_fEndDV, PS_VOLTAGE, TRUE));
	strCmd.Format("%.3f", pStep->m_fEndW);
	SetDlgItemText(IDC_EDIT_C_ENDWATT, ValueString(pStep->m_fEndW, PS_WATT, TRUE));
	strCmd.Format("%.3f", pStep->m_fEndWh);
	SetDlgItemText(IDC_EDIT_C_ENDWATTHOUR, ValueString(pStep->m_fEndWh, PS_WATT_HOUR, TRUE));

	strCmd.Empty();
	//충전 방전 Step
	if(pStep->m_bUseActualCapa)
	{
		strCmd.Format("용량기준값으로 사용 ");
	}//충전 방전 step
	else if(pStep->m_UseAutucalCapaStepNo > 0 && pStep->m_fSocRate > 0)
	{
		if(pStep->m_type == PS_STEP_CHARGE)
		{
			strCmd.Format("Step %d SOC %.1f%% ", pStep->m_UseAutucalCapaStepNo, pStep->m_fSocRate);
		}
		else if(pStep->m_type == PS_STEP_DISCHARGE)
		{
			strCmd.Format("Step %d DOD %.1f%% ", pStep->m_UseAutucalCapaStepNo, pStep->m_fSocRate);
		}
		else
			strCmd.Empty();
	}//loop step
	else if(pStep->m_nLoopInfoCycle > 0 && pStep->m_nLoopInfoGoto >= 0)
	{
		if(pStep->m_nLoopInfoGoto <= 0)
		{
			strCmd.Format("반복 %d회 후 Next로 이동 ", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGoto);
		}
		else
		{
			strCmd.Format("반복 %d회 후 %d로 이동 ", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGoto);
		}
	}
	
	CString str;
//	if(pStep->m_fStartT > 0.0f)
//	{
//		str.Format("StartT %.2f℃ ", pStep->m_fStartT);
//	}
//	strCmd += str; 
	if(pStep->m_fEndT > 0.0f)
	{
		str.Format("EndT %.2f℃ ", pStep->m_fEndT);
	}
	strCmd += str; 

/*	if(pStep->m_nLoopInfoEndVGoto > 0)
	{
		if(str.GetLength() > 0)
			str += "\r\n"; 
		str.Format("EndV ☞ Step%d ", pStep->m_nLoopInfoEndVGoto);
	}
	strCmd += str; 

	if(pStep->m_nLoopInfoEndTGoto > 0)
	{
		if(str.GetLength() > 0)
			str += "\r\n"; 
		str.Format("EndT ☞ Step%d ", pStep->m_nLoopInfoEndTGoto);
	}
	strCmd += str; 

	if(pStep->m_nLoopInfoEndCGoto > 0)
	{
		if(str.GetLength() > 0)
			str += "\r\n"; 
		str.Format("EndC ☞ Step%d ", pStep->m_nLoopInfoEndCGoto);
	}
	strCmd += str; */

	SetDlgItemText(IDC_END_ETC_STATIC, strCmd);

	//Grading condition
	m_ctrlGrade.DeleteAllItems();

	char szBuff[32];
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;//|LVIF_IMAGE;
	GRADE_STEP gStep;
	for(int step =0; step < pStep->m_Grading.GetGradeStepSize(); step++)
	{
		lvItem.iItem = step;
		lvItem.iSubItem = 0;
		sprintf(szBuff,"%d", step+1);
		lvItem.pszText = szBuff; 
		m_ctrlGrade.InsertItem(&lvItem);

		gStep = pStep->m_Grading.GetStepData(step);	
		if(/*pStep->m_Grading.m_lGradingItem*/ gStep.lGradeItem == PS_GRADE_CAPACITY)		//EDLC 용량은 F단위로 표시한다.
		{
//			sprintf(szBuff,"%.1f", gStep.fMin);
			m_ctrlGrade.SetItemText(lvItem.iItem, 1, ValueString(gStep.fMin, PS_CAPACITY));
//			sprintf(szBuff,"%.1f", gStep.fMax);
			m_ctrlGrade.SetItemText(lvItem.iItem, 2, ValueString(gStep.fMax, PS_CAPACITY));
		}
		else if(gStep.lGradeItem == PS_GRADE_IMPEDANCE)
		{
//			sprintf(szBuff,"%.1f", gStep.fMin/1000.0);
			m_ctrlGrade.SetItemText(lvItem.iItem, 1, ValueString(gStep.fMin, PS_IMPEDANCE));
//			sprintf(szBuff,"%.1f", gStep.fMax/1000.0);
			m_ctrlGrade.SetItemText(lvItem.iItem, 2, ValueString(gStep.fMax, PS_IMPEDANCE));
		}
		else
		{
//			sprintf(szBuff,"%.1f", gStep.fMin);
			m_ctrlGrade.SetItemText(lvItem.iItem, 1, ValueString(gStep.fMin, PS_VOLTAGE));
//			sprintf(szBuff,"%.1f", gStep.fMax);
			m_ctrlGrade.SetItemText(lvItem.iItem, 2, ValueString(gStep.fMax, PS_VOLTAGE));
		}

		if(!gStep.strCode.IsEmpty())
		{
			sprintf(szBuff,"%c", gStep.strCode[0]);	
		}
		else
		{
			sprintf(szBuff,"");	
		}
		m_ctrlGrade.SetItemText(lvItem.iItem, 3, szBuff);
	}

	
}
/*
CString CTestConReportDlg::GetTime(int nSec)
{
	CString strRetval;
	int nHour, nMin;
	if( nSec >= 3600 )
	{
		nHour = nSec / 3600;
		nSec -= 3600 * nHour;
		nMin = nSec / 60;
		nSec -= 60 * nMin;
		strRetval.Format("%02d:%02d:%02d", nHour, nMin, nSec);
	}
	else if ( nSec >= 60 )
	{
		nMin = nSec / 60;
		nSec -= 60 * nMin;
		strRetval.Format("%02d:%02d:%02d", 0, nMin, nSec);
	}
	else
	{
		strRetval.Format("%02d:%02d:%02d", 0, 0, nSec);
	}
	return strRetval;
}
*/
void CTestConReportDlg::OnItemchangedStepList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	POSITION pos = m_ctrlList.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_ctrlList.GetNextSelectedItem(pos);
		int nStepIndex = m_ctrlList.GetItemData(nItem);
		SetStepData(nStepIndex);	
	}
	*pResult = 0;
}

void CTestConReportDlg::OnExcelButton() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;

	strFileName.Format("%s_%s.csv", m_pData->GetModelName(), m_pData->GetScheduleName());

	CFileDialog pDlg(FALSE, "csv", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");
	if(IDOK != pDlg.DoModal())
	{
		return;
	}

	strFileName = pDlg.GetPathName();
	if(m_pData->SaveToExcelFile(strFileName) == FALSE)
	{
		AfxMessageBox(strFileName+" 저장 실패!!!");
	}
}

CString CTestConReportDlg::ValueString(double dData, int item, BOOL bUnit)
{
	return m_UnitTrans.ValueString(dData, item, bUnit);

/*
	CString strMsg, strTemp;
	double dTemp;
	char szTemp[8];

	dTemp = dData;
	switch(item)
	{
	case PS_STEP_TYPE:	strMsg = ::PSGetTypeMsg((WORD)dData);
		break;

	case PS_STATE:		strMsg = ::PSGetStateMsg((WORD)dData);
		break;
		
	case PS_VOLTAGE:		//voltage
		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		else if(m_strVUnit == "uV")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}
		//else //if(m_strVUnit == "mV")
		//{
		//}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg+= (" "+m_strVUnit);
		
		break;

	case PS_CURRENT:		//current
//		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//u 단위
		if(m_nCurrentUnitMode)		dTemp = dTemp  / 1000.0f;
		
		//current
		if(m_strIUnit == "A")
		{
			dTemp = dTemp/1000.0f;
		}  
		else if(m_strIUnit == "uA")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}
		//else if(m_strIUnit == "mA")	//mA
		//{
		//}

		if(m_nIDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strIUnit);

		break;
			
	case PS_WATT	:
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp / 1000.0f;

		if(m_strWUnit == "W")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strWUnit == "KW")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWUnit == "uW")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWUnit);
		break;	
	
	case PS_WATT_HOUR:			
		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp / 1000.0f;

		if(m_strWhUnit == "Wh")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strWhUnit == "KWh")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWUnit == "uWh")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWhDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWhDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWhUnit);
		
		break;
		
	case PS_CAPACITY:		//capacity

		if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp / 1000.0f;

		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strCUnit == "uAh" || m_strCUnit == "uF")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nCDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strCUnit);
		break;

	case PS_IMPEDANCE:	
		strMsg.Format("%.1f", dTemp);
		if(bUnit) strMsg+= " mΩ";
		break;

	case PS_CODE:	//failureCode
		::PSCellCodeMsg((BYTE)dData, strMsg, strTemp);	//Pscommon.dll API
		break;

	case PS_TOT_TIME:
	case PS_STEP_TIME:

		if(m_nTimeUnit == 1)	//sec 표시
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strMsg.Format("%.2f", dTemp/60.0f);
		}
		else
		{
			float fData = dTemp - (ULONG)dTemp;
			CTimeSpan timeSpan((ULONG)dTemp);
			if(timeSpan.GetDays() > 0)
			{
				strMsg.Format("%s.%d", timeSpan.Format("%Dd %H:%M:%S"), int(fData*10));
			}
			else
			{
				strMsg.Format("%s.%d", timeSpan.Format("%H:%M:%S"), int(fData*10));
			}
		}

		break;

	case PS_GRADE_CODE:	
		strMsg.Format("%c", (BYTE)dData);
		break;
	
	case PS_TEMPERATURE:
		if(0xFFFFFFFF == (float)dTemp)
		{
			strMsg = "미사용";
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " ℃";
		}
		
		break;

	case PS_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
*/
}

//설정된 종료 조건을 String으로 만든다.
CString CTestConReportDlg::MakeEndString(CStep *pStep)
{
	if(pStep == NULL)	"Error";
	
	CString strTemp, strTemp1;
	switch(pStep->m_type)
	{
	case PS_STEP_CHARGE:			//Charge
		if(fabs(pStep->m_fEndV) > 0.0f)	
		{
			strTemp.Format("V > %s", ValueString(pStep->m_fEndV, PS_VOLTAGE));
		}
		if(fabs(pStep->m_fEndI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("I < %s", ValueString(pStep->m_fEndI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(pStep->m_fEndTime > 0)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("t > %s", ValueString(pStep->m_fEndTime, PS_STEP_TIME));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndC) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("C > %.1f", ValueString(pStep->m_fEndC, PS_CAPACITY));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDV) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dVp > %s", ValueString(pStep->m_fEndDV, PS_VOLTAGE));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dI > %s", ValueString(pStep->m_fEndDI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndW) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("W > %s", ValueString(pStep->m_fEndW, PS_WATT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndWh) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("Wh > %s", ValueString(pStep->m_fEndWh, PS_WATT_HOUR));
			strTemp += strTemp1;
		}


		break;
	
	case PS_STEP_DISCHARGE:		//Discharge
		if(fabs(pStep->m_fEndV) > 0.0f)	
		{
			strTemp.Format("V < %s", ValueString(pStep->m_fEndV, PS_VOLTAGE));
		}
		if(fabs(pStep->m_fEndI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("I < %s", ValueString(pStep->m_fEndI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(pStep->m_fEndTime > 0)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("t > %s", ValueString(pStep->m_fEndTime, PS_STEP_TIME));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndC) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("C > %s", ValueString(pStep->m_fEndC, PS_CAPACITY));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDV) < 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dVp < %s", ValueString(pStep->m_fEndDV, PS_VOLTAGE));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dI > %s", ValueString(pStep->m_fEndDI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndW) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("W > %s", ValueString(pStep->m_fEndW, PS_WATT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndWh) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("Wh > %s", ValueString(pStep->m_fEndWh, PS_WATT_HOUR));
			strTemp += strTemp1;
		}
		break;
	
	case PS_STEP_REST:			//Rest
	case PS_STEP_IMPEDANCE:		//Impedance
		{
			strTemp.Format("t > %s", ValueString(pStep->m_fEndTime, PS_STEP_TIME));
			break;
		}
	
	case PS_STEP_OCV:				//Ocv
	case PS_STEP_END:				//End
	case PS_STEP_ADV_CYCLE:
		break;
	case PS_STEP_LOOP:
		if(pStep->m_nLoopInfoGoto <= 0)
		{
			strTemp.Format("반복 %d회 후 다음 Cycle로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGoto);
		}
		else
		{
			strTemp.Format("반복 %d회 후 Step %d로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGoto);
		}
		break;
	default:
		strTemp = "";
	}

	return strTemp;
}
