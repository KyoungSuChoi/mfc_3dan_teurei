#if !defined(AFX_TESTCONREPORTDLG_H__7DB43751_9998_4304_B4D6_070CB7D42226__INCLUDED_)
#define AFX_TESTCONREPORTDLG_H__7DB43751_9998_4304_B4D6_070CB7D42226__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestConReportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestConReportDlg dialog
#include "resource.h"
#include "ScheduleData.h"
#include "UnitTrans.h"
//#include "./GridCtrl_src/GridCtrl.h"

class AFX_EXT_CLASS CTestConReportDlg : public CDialog
{
// Construction
public:
	CString MakeEndString(CStep *pStep);
	CString ValueString(double dData, int item, BOOL bUnit = FALSE);
	int m_nCurStepIndex;
//	CString GetTime(int nSec);
	void SetStepData(int nIndex);
	CScheduleData* m_pData;
	void Init();
	CTestConReportDlg(CScheduleData* sData, CWnd* pParent = NULL);   // standard constructor

//	CString m_strVUnit;
//	CString m_strIUnit;
//	CString m_strCUnit;
//	CString m_strWUnit;
//	CString m_strWhUnit;

	CUnitTrans m_UnitTrans;

	//Editable Grid
//	CGridCtrl	m_Grid;

// Dialog Data
	//{{AFX_DATA(CTestConReportDlg)
	enum { IDD = IDD_TESTCON_VIEW_DLG };
	CListCtrl	m_ctrlGrade;
	CListCtrl	m_ctrlList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestConReportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	int	m_nCurrentUnitMode;
	int m_nTimeUnit;
	int m_nVDecimal ;
	int m_nIDecimal;
	int m_nCDecimal;
	int m_nWDecimal;
	int m_nWhDecimal;

	CImageList m_imgList;
	// Generated message map functions
	//{{AFX_MSG(CTestConReportDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedStepList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnExcelButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTCONREPORTDLG_H__7DB43751_9998_4304_B4D6_070CB7D42226__INCLUDED_)
