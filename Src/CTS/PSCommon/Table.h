/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Table.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CTable class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// Table.h: interface for the CTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CTable
//
//	Comment: 
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
#define RS_COL_VOLTAGE		"Voltage"
#define RS_COL_VOLTAGE1		"Voltage1"
#define RS_COL_VOLTAGE2		"Voltage2"
#define RS_COL_VOLTAGE3		"Voltage3"
#define RS_COL_VOLTAGE4		"Voltage4"
#define RS_COL_CURRENT		"Current"
#define RS_COL_CAPACITY		"Capacity"
#define RS_COL_TIME			"Time"
#define RS_COL_WATT			"Power"
//#define RS_COL_FARAD		"Farad"
#define RS_COL_WATT_HOUR	"WattHour"
#define RS_COL_TEMPERATURE	"Temperature"
#define RS_COL_TEMPERATURE1	"Temperature1"
#define RS_COL_TEMPERATURE2	"Temperature2"
#define RS_COL_PRESSURE		"Press"
#define RS_COL_AVG_VTG		"VoltageAverage"
#define RS_COL_AVG_CRT		"CurrentAverage"
#define RS_COL_STEP_NO		"No"
#define RS_COL_INDEX_FROM	"IndexFrom"
#define RS_COL_INDEX_TO		"IndexTo"
#define RS_COL_CUR_CYCLE	"CurCycle"
#define RS_COL_TOT_CYCLE	"TotalCycle"
#define RS_COL_TYPE			"Type"
#define RS_COL_STATE		"State"
#define RS_COL_TOT_TIME		"TotTime"
#define RS_COL_CODE			"Code"
#define RS_COL_GRADE		"Grade"
#define RS_COL_IR			"IR"
#define RS_COL_CH_NO		"ChNo"
#define RS_COL_SAVE_ACCU	"Select"
#define RS_COL_SEQ			"Sequence"
#define RS_COL_OCV			"OCV"

class AFX_EXT_CLASS CTable  
{
////////////////
// Attributes //
////////////////

private:
	int FindItemIndex(WORD nItem);
	void				ReleaseRawDataMemory();
	LONG				m_lLastRecordIndex;
	LONG				m_lRecordIndex;
	LONG                m_lIndex;

	BOOL                m_bLoadData;
	BOOL                m_bLoadLastData;

	COleDateTime        m_StartTime;
	LONG                m_lStepNo;

	CStringList         m_strlistTitle;
	CWordArray			m_awlistItem;

	float*              m_pfltLastData;

	float**             m_ppfltData;
	LONG				m_lTotCycle;
	WORD				m_wType;
	CWordArray			m_DataItemList;

	LONG                m_lNumOfData;				//.cyc에 저장된 data 크기 
	long				m_lLoadedDataCount;			//step start data까지 포함된 크기

	BYTE	m_chNo;			
	BYTE	m_chState;		
	BYTE	m_chDataSelect; 
	BYTE	m_chCode;		
	BYTE	m_chGradeCode;	

	ULONG	m_nCurrentCycleNum;
	ULONG	m_lSaveSequence;

///////////////////////////////////////////////
// Operations: "Construction and destruction //
///////////////////////////////////////////////
public:
//	CTable(LONG lIndex);
	CTable(LPCTSTR strTitle, LPCTSTR strFromList);
	CTable(CWordArray *paItem, LPPS_STEP_END_RECORD lpRecord);
	virtual ~CTable();

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:
	CString GetItemName(WORD wItem);
	WORD GetItemID(CString strTitle);
	CWordArray *	GetRecordItemList()	{	return &m_DataItemList;		}
	LONG	GetRecordItemSize()		{	return m_DataItemList.GetSize();	}
	LONG	GetRecordCount();
	WORD	GetType()			{	return m_wType;		}
	LONG	GetRecordIndex()	{	return m_lRecordIndex;	}
	LONG	GetLastRecordIndex()	{	return	m_lLastRecordIndex	; 	}
	LONG    GetStepNo()				{	return m_lStepNo;		};
	LONG    GetCycleNo()			{	return m_lTotCycle;		};
	void	LoadDataA(LPCTSTR strChPath);
	void	LoadDataB(LPCTSTR strChPath);
	void	LoadDataC(LPCTSTR strChPath);
//	void    LoadData(LPCTSTR strChPath);
	float   GetLastData(LPCTSTR strTitle, WORD wPoint=0x0000);
//	float   GetDCIR()			{	return m_fltDCIR;		};
//	float   GetOCV()			{	return m_fltOCV;		};
	fltPoint * GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);

protected:
	long FindRsColumnIndex(LPCTSTR szTitle);
	long FindRsColumnIndex(WORD nItem);
	int  FindIndexOfTitle(LPCTSTR title);
	int FindIndexOfTitle(WORD wItem);

};

#endif // !defined(AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
