// DataClt.h : main header file for the DATACLT DLL
//

#if !defined(AFX_DATACLT_H__65EFEBCC_4613_44D5_AB7D_17376D9CA02A__INCLUDED_)
#define AFX_DATACLT_H__65EFEBCC_4613_44D5_AB7D_17376D9CA02A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDataCltApp
// See DataClt.cpp for the implementation of this class
//

class CDataCltApp : public CWinApp
{
public:
	CDataCltApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataCltApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDataCltApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATACLT_H__65EFEBCC_4613_44D5_AB7D_17376D9CA02A__INCLUDED_)
