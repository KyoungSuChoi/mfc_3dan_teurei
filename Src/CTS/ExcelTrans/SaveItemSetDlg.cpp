// SaveItemSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ExcelTrans.h"
#include "SaveItemSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSaveItemSetDlg dialog


CSaveItemSetDlg::CSaveItemSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSaveItemSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSaveItemSetDlg)
	m_bState = TRUE;
	m_bTime = TRUE;
	m_bVoltage = TRUE;
	m_bCurrent = TRUE;
	m_bCapacity = TRUE;
	m_bCapEff = TRUE;
	m_bImpedance = TRUE;
	m_bWatt = TRUE;
	m_bWattHour = TRUE;
	m_bGradeCode = TRUE;
	m_bCellCode = TRUE;
	m_bType = TRUE;
	m_bSumCap = FALSE;
	//}}AFX_DATA_INIT
}


void CSaveItemSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSaveItemSetDlg)
	DDX_Check(pDX, IDC_CHECK2, m_bState);
	DDX_Check(pDX, IDC_CHECK3, m_bTime);
	DDX_Check(pDX, IDC_CHECK4, m_bVoltage);
	DDX_Check(pDX, IDC_CHECK5, m_bCurrent);
	DDX_Check(pDX, IDC_CHECK6, m_bCapacity);
	DDX_Check(pDX, IDC_CHECK12, m_bCapEff);
	DDX_Check(pDX, IDC_CHECK7, m_bImpedance);
	DDX_Check(pDX, IDC_CHECK8, m_bWatt);
	DDX_Check(pDX, IDC_CHECK9, m_bWattHour);
	DDX_Check(pDX, IDC_CHECK10, m_bGradeCode);
	DDX_Check(pDX, IDC_CHECK11, m_bCellCode);
	DDX_Check(pDX, IDC_CHECK1, m_bType);
	DDX_Check(pDX, IDC_CHECK13, m_bSumCap);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSaveItemSetDlg, CDialog)
	//{{AFX_MSG_MAP(CSaveItemSetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaveItemSetDlg message handlers

void CSaveItemSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	AfxGetApp()->WriteProfileInt(REG_SECTION, "State", m_bState);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Time", m_bTime);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Voltage", m_bVoltage);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Current", m_bCurrent);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Capacity", m_bCapacity);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "DCIR", m_bImpedance);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Watt", m_bWatt);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "WattHour", m_bWattHour);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "GradeCode", m_bGradeCode);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "CellCode", m_bCellCode);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "Type", m_bType);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "CapEff", m_bCapEff);
	AfxGetApp()->WriteProfileInt(REG_SECTION, "CapaSum", m_bSumCap);

	CDialog::OnOK();
}

BOOL CSaveItemSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_bState = AfxGetApp()->GetProfileInt(REG_SECTION, "State", TRUE);
	m_bTime = AfxGetApp()->GetProfileInt(REG_SECTION, "Time", TRUE);
	m_bVoltage = AfxGetApp()->GetProfileInt(REG_SECTION, "Voltage", TRUE);
	m_bCurrent = AfxGetApp()->GetProfileInt(REG_SECTION, "Current", TRUE);
	m_bCapacity = AfxGetApp()->GetProfileInt(REG_SECTION, "Capacity", TRUE);
	m_bImpedance = AfxGetApp()->GetProfileInt(REG_SECTION, "DCIR", TRUE);
	m_bWatt = AfxGetApp()->GetProfileInt(REG_SECTION, "Watt", TRUE);
	m_bWattHour = AfxGetApp()->GetProfileInt(REG_SECTION, "WattHour", TRUE);
	m_bGradeCode = AfxGetApp()->GetProfileInt(REG_SECTION, "GradeCode", TRUE);
	m_bCellCode = AfxGetApp()->GetProfileInt(REG_SECTION, "CellCode", TRUE);
	m_bType = AfxGetApp()->GetProfileInt(REG_SECTION, "Type", TRUE);
	m_bCapEff = AfxGetApp()->GetProfileInt(REG_SECTION, "CapEff", TRUE);
	m_bSumCap = AfxGetApp()->GetProfileInt(REG_SECTION, "CapaSum", FALSE);

	UpdateData(FALSE);
	
	// TODO: Add extra initialization here
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
