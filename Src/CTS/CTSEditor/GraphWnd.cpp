// GraphWnd.cpp : implementation file
//

#include "stdafx.h"
#include "GraphWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define WM_SET_RANGE		WM_USER + 100
#define WM_STEP_IT			WM_USER + 101
#define WM_PROGRESS_SHOW	WM_USER + 102

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeWnd
CDCRScopeWnd::CDCRScopeWnd()
{
	m_hWndPE = NULL;
	m_YColor[0] = RGB(230,  50,  0);
	m_YColor[1] = RGB(  35, 50, 200);
	m_YColor[2] = RGB(  0,255,  0);
	m_YColor[3] = RGB(255,255,  0);
	m_YColor[4] = RGB(  0,255,255);
	m_YColor[5] = RGB(255,192,255);
	m_YColor[6] = RGB(255,255,255);
	m_YColor[7] = RGB(192,192,255);

	m_SetsetNum = PE_MAX_SUBSET;
	m_TotalPoint = 0;
	m_ScreenPoint = 50000;
	m_Delay = 1000;

	m_TimerID = 0;
	for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		m_pData[i] = NULL;
		m_nDataPointCount[i] =0;
		m_bShowArray[i] = FALSE;
		m_dMin[i] = 0.0; 
		m_dMax[i] = 5.0;
//		m_strSubsetName[i].Format("Data %d", i+1);
	}
	m_bShowArray[0] = TRUE;

	m_TimerPtr = NULL;

//	m_strMainTitle = "";
//	m_strXName = "시간";
}

CDCRScopeWnd::~CDCRScopeWnd()
{
	for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		if(m_pData[i])	
		{
			delete[] m_pData[i];
			m_pData[i] = NULL;
		}
	}
}

BEGIN_MESSAGE_MAP(CDCRScopeWnd, CStatic)
	//{{AFX_MSG_MAP(CDCRScopeWnd)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CHAR()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDCRScopeWnd message handlers

void CDCRScopeWnd::PreSubclassWindow() 
{
	CStatic::PreSubclassWindow();

	CRect trect, rect;

	GetClientRect(trect);
	rect.left = 1;
	rect.right = trect.Width() - 2;
	rect.top = 1;
	rect.bottom = trect.Height() - 2;
    m_hWndPE = PEcreate(PECONTROL_SGRAPH, WS_VISIBLE|WS_BORDER, &rect, m_hWnd, 1000); 
//    m_hWndPE = PEcreate(PECONTROL_GRAPH/*PECONTROL_SGRAPH*/, WS_VISIBLE|WS_BORDER, &rect, m_hWnd, 1000); 
	if(!m_hWndPE)
	{
		TRACE("Creation Error");
		return;
	}

	SetGlobalPEStuff();

//***********************
	int	nArray[PE_MAX_SUBSET];
	int num=0;
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
//***********************************************

	
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);        

}

void CDCRScopeWnd::SetGlobalPEStuff()
{
	SetN(PEP_bPREPAREIMAGES, TRUE);					//mem DC 이용 
	SetN(PEP_bDATASHADOWS, FALSE);					//Data의 그림자 
	SetN(PEP_nDATAPRECISION, 3);					//소수점 3자리 표기 
	SetN(PEP_bALLOWBESTFITLINE, TRUE);				//근사 곡선 사용 
	SetN(PEP_bALLOWHISTOGRAM, FALSE);				//Histogram 사용 여부 
	SetN(PEP_nGRIDLINECONTROL, PEGLC_BOTH);			//그래프 상에 Grid Line 

	SetN(PEP_bMOUSECURSORCONTROL, TRUE);			//마우스로  data를 Click하면 이동함 
	SetN(PEP_nCURSORMODE, PECM_NOCURSOR);			//현재 Data 위치 표시 방법
	SetN(PEP_bCURSORPROMPTTRACKING, TRUE);			//좌측 상단에 Mouse 위치 좌표 표시 
	SetN(PEP_nCURSORPROMPTSTYLE, 3);//

	SetN(PEP_bALLOWUSERINTERFACE, TRUE);		//Popup 명령 사용			
	SetN(PEP_bALLOWPOPUP, TRUE);				//Popup 명령 사용				
	SetN(PEP_bALLOWCUSTOMIZATION, TRUE);		//Cutsomize dialog enable

	SetN(PEP_bALLOWDATAHOTSPOTS, TRUE);			//마우스로 Click시 Event발생함 
	SetN(PEP_bALLOWSUBSETHOTSPOTS, TRUE);		//
	SetN(PEP_bALLOWPOINTHOTSPOTS, TRUE);		//
	SetN(PEP_bALLOWTABLEHOTSPOTS, TRUE);		//
	SetN(PEP_nFONTSIZE, PEFS_SMALL);			//전체적으로 글자체를 작게 한다.

	SetN(PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);	//가로세로 모두 확대 가능 

	//real time 
	//SetN(PEP_bAPPENDTOEND, TRUE);				//우에서 좌로 Scroll(실시간 data 그래프시)
	//SetSZ(PEP_szMANUALMAXPOINTLABEL, "##########");
	
	SetSZ(PEP_szMAINTITLEFONT, "Arial");		//
	SetN(PEP_nXAXISSCALECONTROL, PEAC_NORMAL);	//X축 Scale 방식 
	SetN(PEP_nYAXISSCALECONTROL, PEAC_AUTO);	//Y축 Scale 방식 

	SetN(PEP_nSUBSETS, m_SetsetNum);			//
	SetN(PEP_nPOINTS, m_TotalPoint);			//
	SetN(PEP_nPOINTSIZE, PEPS_MICRO);

	//Graph Object
	SetN(PEP_nGRAPHPLUSTABLE, PEGPT_BOTH);				//Graph + Table 표시
	SetN(PEP_nPOINTSTOGRAPHINIT, PEPTGI_FIRSTPOINTS);	//초기 Point 위치 
	SetN(PEP_nFORCEVERTICALPOINTS, PEFVP_AUTO);			//X축 라벨 가로/세로 표기 방식 
	SetN(PEP_bNORANDOMPOINTSTOGRAPH, TRUE);				//

//	SetN(PEP_nPOINTSTOGRAPH, m_ScreenPoint);			//해당 Point 수로 수평 Scroll 시킴 
//	SetN(PEP_nTARGETPOINTSTOTABLE, 10);	
//	SetN(PEP_nALTFREQTHRESHOLD, 10);

	//All graph
	SetVCell(PEP_szaPOINTLABELS, m_TotalPoint-1, " ");
	SetSZ(PEP_szMAINTITLE, "");
	SetSZ(PEP_szSUBTITLE, "");
//	SetSZ(PEP_szXAXISLABEL,(LPSTR)(LPCTSTR) m_strXName);
	SetN(PEP_dwDESKCOLOR, RGB(255,255,255));
	SetN(PEP_dwGRAPHBACKCOLOR, RGB(230,230,220));
	SetN(PEP_dwGRAPHFORECOLOR, RGB(192,192,192));
	SetV(PEP_dwaSUBSETCOLORS, m_YColor, PE_MAX_SUBSET);

	SetAnnotation(TRUE);

//	PEnset(m_hWndPE, PEP_bAUTOSCALEDATA, FALSE);					//This property controls whether the ProEssentials will automatically scale data that is very small or very large.

	//Null Data setting
	double NullData = NULL_DATA;
	SetV(PEP_fNULLDATAVALUE, &NullData, 0);

	int slt[PE_MAX_SUBSET];
	int mas[PE_MAX_SUBSET];
	int i = 0;

	for(i = 0; i<PE_MAX_SUBSET; i++)
	{
		slt[i] = 0;
		mas[i] = 1;
	}
	SetV(PEP_naSUBSETLINETYPES, slt, PE_MAX_SUBSET);
	SetV(PEP_naMULTIAXESSUBSETS, mas, PE_MAX_SUBSET);

	int oma = PE_MAX_SUBSET;
	SetV(PEP_naOVERLAPMULTIAXES, &oma, 1);
    
	//분할 비율
//	float map[2] = {.75F, .25F};
//  PEvset(m_hWndPE, PEP_faMULTIAXESPROPORTIONS, map, 2);

	for(i=0;i<m_SetsetNum;i++)
	{
		InitSubset(i, m_YColor[i]);
	}
	SetN(PEP_nWORKINGAXIS, 0);
}

void CDCRScopeWnd::InitSubset(int subID, COLORREF color, LPCTSTR title, LPCTSTR yname)
{
	SetN(PEP_nWORKINGAXIS, subID);
//	SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
//	SetV(PEP_fMANUALMINY, &min, 0);
//	SetV(PEP_fMANUALMAXY, &max, 0);
	SetN(PEP_nPLOTTINGMETHOD, PEGPM_POINTSPLUSLINE);

//	SetVCell(PEP_szaSUBSETLABELS, subID, (LPSTR) title);
//	SetSZ(PEP_szYAXISLABEL, (LPSTR) yname);
	m_YColor[subID] = color;
	SetN(PEP_dwYAXISCOLOR, m_YColor[subID]);

	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::SetYAxisLabel(int subID, CString str)
{
	SetN(PEP_nWORKINGAXIS, subID);
	SetSZ(PEP_szYAXISLABEL, (LPSTR)(LPCTSTR) str);
}

void CDCRScopeWnd::SetDataTitle(int nSubID, CString str)
{
	SetVCell(PEP_szaSUBSETLABELS, nSubID, (LPSTR)(LPCTSTR) str);
}

void CDCRScopeWnd::SetXAxisLabel(CString str)
{
	SetSZ(PEP_szXAXISLABEL,(LPSTR)(LPCTSTR) str);
}

void CDCRScopeWnd::ShowSubset(int id, BOOL bShow)
{
	m_bShowArray[id] = bShow;
	int	nArray[8];
	int num=0;
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
	PEreinitialize(m_hWndPE);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::SetSplit(BOOL bSplit)
{
	int oma[6];
	float map[6];
	if(bSplit)
	{
		float onesize = 1.0f / m_SetsetNum;
		for(int j=0;j<m_SetsetNum;j++)
		{
			oma[j] = 1;
			map[j] = onesize;
		}
		SetV(PEP_naOVERLAPMULTIAXES, oma, m_SetsetNum);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, m_SetsetNum);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_MEDIUM);
	}
	else
	{
		oma[0] = m_SetsetNum;
		map[0] = 1.0f;
		SetV(PEP_naOVERLAPMULTIAXES, oma, 1);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, 0);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_NONE);
	}
	PEreinitialize(m_hWndPE);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::ClearGraph()
{
	//Max point
	for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		if(m_pData[i] != NULL)
		{
			delete[]	m_pData[i];
			m_pData[i] = NULL;
			m_nDataPointCount[i] = 0;
		}
	}
	m_TotalPoint =0;

	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::Start()
{
	m_Count = 0;
	m_OldTime = 99999999L;

	SetN(PEP_nHORZSCROLLPOS, 0);

	ClearGraph();

	for(int i=0;i<m_SetsetNum;i++)
	{
//		InitSubset(i,  m_dMin[i], m_dMax[i]);
	}

	time(&m_StartTime);

	if(m_TimerID)
		KillTimer(m_TimerID);
	m_TimerID = SetTimer(11, m_Delay, NULL);
}

void CDCRScopeWnd::Stop()
{
	if(m_TimerID)
		KillTimer(m_TimerID);
	m_TimerID = 0;
}

void CDCRScopeWnd::SetDataPtr(int id, int nCnt, float *fXData, float *fYData)
{
	//Data Copy
	if(m_pData[id] != NULL)
	{
		delete[]	m_pData[id];
		m_pData[id] = NULL;
	}
	m_pData[id] = new float[nCnt];
	memcpy(m_pData[id], fYData, sizeof(float)*nCnt);

	//Max point
	int nTotalPoint = 0;
	int i = 0;
	m_nDataPointCount[id] = nCnt;
	for(i=0; i<	PE_MAX_SUBSET; i++)
	{
		if(m_pData[i] != NULL)
		{
			if(nTotalPoint < m_nDataPointCount[i])
				nTotalPoint = m_nDataPointCount[i];
		}
	}
	m_TotalPoint = nTotalPoint;

	SetN(PEP_nPOINTS, m_TotalPoint);	//

	this->GetParent()->SendMessage(WM_SET_RANGE, m_TotalPoint, 0);
	this->GetParent()->SendMessage(WM_PROGRESS_SHOW, SW_SHOW, 0);
	
	//Draw data
	char szName[48];
	float a, b;
	float fMin = 999999999, fMax = -999999999;
	for(i=0; i<m_TotalPoint; i++)
	{
		a = fXData[i];
		if(i < m_nDataPointCount[id])
		{
			b = m_pData[id][i];
		}
		else
		{
			b = NULL_DATA;
		}

		PEvsetcell(m_hWndPE, PEP_faXDATA, i+id*m_TotalPoint, &a);
		PEvsetcell(m_hWndPE, PEP_faYDATA, i+id*m_TotalPoint, &b);
		
		sprintf(szName, "%f", fXData[i]);
		SetVCell(PEP_szaPOINTLABELS, i, szName);
		this->GetParent()->SendMessage(WM_STEP_IT, 0, 0);
	}
	this->GetParent()->SendMessage(WM_PROGRESS_SHOW, SW_HIDE, 0);
}

void CDCRScopeWnd::OnTimer(UINT nIDEvent) 
{
	unsigned long newTime = *m_TimerPtr;
	if(newTime == m_OldTime)
	{
		CStatic::OnTimer(nIDEvent);
		return;
	}
	m_OldTime = newTime;

	float data[8];
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_pData[i])
			data[i] = *(m_pData[i]);
		else
			data[i] = 0;
	}

	CString str;
	str.Format("%7ld", newTime);
	if(m_Count>=m_TotalPoint)
	{
		if(m_Count==m_TotalPoint)
			SetN(PEP_nHORZSCROLLPOS, m_Count);
		m_Count++;
		SetV(PEP_szaAPPENDPOINTLABELDATA, (void FAR*) (const char*) str, 1);
		SetV(PEP_faAPPENDYDATA, data, 1);
	}
	else
	{
		for(int i=0;i<m_SetsetNum;i++)
		{
			PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, i, m_Count, (void FAR*) (const char*) str);
			PEvsetcellEx(m_hWndPE, PEP_faYDATA, i, m_Count, &data[i]);
		}
		if(m_Count>=m_ScreenPoint)
		{
			SetN(PEP_nHORZSCROLLPOS, m_Count-m_ScreenPoint-1);
		}
		else
		{
		}
		PEreinitialize(m_hWndPE);
		PEresetimage(m_hWndPE, 0, 0);
		::InvalidateRect(m_hWndPE, NULL, FALSE);

		m_Count++;
	}

	CStatic::OnTimer(nIDEvent);
}

void CDCRScopeWnd::OnDestroy() 
{
	CStatic::OnDestroy();
	
	PEdestroy(m_hWndPE);
	if(m_TimerID)
		KillTimer(m_TimerID);
	
}

BOOL CDCRScopeWnd::OnEraseBkgnd(CDC* pDC) 
{
//	BOOL ret =  CStatic::OnEraseBkgnd(pDC);
//	::InvalidateRect(m_hWndPE, NULL, FALSE);
	return FALSE;
}

void CDCRScopeWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

//*********** Functions for Graph ******************************
void CDCRScopeWnd::SetN(UINT type, INT value)
{
	PEnset(m_hWndPE, type, value);
}

void CDCRScopeWnd::SetV(UINT type, VOID FAR * lpData, UINT nItems)
{
	PEvset(m_hWndPE, type, lpData, nItems);
}

void CDCRScopeWnd::SetVCell(UINT type, UINT nCell, VOID FAR * lpData)
{
	PEvsetcell(m_hWndPE, type, nCell, lpData);
}

void CDCRScopeWnd::SetSZ(UINT type, CHAR FAR *str)
{
	PEszset(m_hWndPE, type, str);
}

void CDCRScopeWnd::Print()
{
	PElaunchprintdialog(m_hWndPE, TRUE, NULL);
}

void CDCRScopeWnd::SetData(int subset, int pos, LPCTSTR xData, double yData)
{
	float data = (float) yData;
	PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, subset, pos, (void FAR*) xData);
	PEvsetcellEx(m_hWndPE, PEP_faYDATA, subset, pos, &data);
}

void CDCRScopeWnd::SetMaximize()
{
	PElaunchmaximize(m_hWndPE);
}

void CDCRScopeWnd::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(nChar == 27 || nChar == VK_RETURN)
	{
		SetN(PEP_bZOOMMODE, FALSE);
		PEresetimage(m_hWndPE, 0, 0);
	}
	CStatic::OnChar(nChar, nRepCnt, nFlags);
}

BOOL CDCRScopeWnd::OnCommand(WPARAM wp, LPARAM lp)
{
    HOTSPOTDATA HSData;

    switch (HIWORD(wp))
    {
        case PEWN_CLICKED:
            PEvget(m_hWndPE, PEP_structHOTSPOTDATA, &HSData);
            if (HSData.nHotSpotType == PEHS_SUBSET)
            {
				/*
                char szTmp[128];
                char szSubset[48];
                szTmp[0] = 0;
                strcat(szTmp, "You clicked subset ");
                sprintf(szSubset, "%i", HSData.w1);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".  Subset label is ");
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".");
                MessageBox(szTmp, "Subset Drill Down", MB_OK | MB_ICONINFORMATION);
				*/
            } 
            else if (HSData.nHotSpotType == PEHS_POINT)
            {
				/*
                if (PECONTROL_PIE != PEnget(m_hWndPE, PEP_nOBJECTTYPE))
                {
                    char szTmp[128];
                    char szSubset[48];
                    szTmp[0] = 0;
                    strcat(szTmp, "You clicked point ");
                    sprintf(szSubset, "%i", HSData.w1);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".  Point label is ");
                    PEvgetcell(m_hWndPE, PEP_szaPOINTLABELS, (UINT) HSData.w1, szSubset);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".");
                    MessageBox(szTmp, "Point Drill Down", MB_OK | MB_ICONINFORMATION);
                }    
                else
                 return CView::OnCommand(wp, lp);
				 */
            }
            else if ((HSData.nHotSpotType == PEHS_TABLE) || (HSData.nHotSpotType == PEHS_DATAPOINT))
            {
				/*
                UINT offset = ((int) HSData.w1 * (int) PEnget(m_hWndPE, PEP_nPOINTS)) + (int) HSData.w2;
                char    szTmp[128];
                char    szSubset[128];
                char    szTmp2[48];
                float   fData;
                szTmp[0] = 0;
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, "항목 : ");
                strcat(szTmp, szSubset);
                strcat(szTmp, "  데이타값 : ");
                PEvgetcell(m_hWndPE, PEP_faYDATA, offset, (LPVOID) &fData);
                sprintf(szTmp2, "%.4f", fData);             
                strcat(szTmp, szTmp2);
                MessageBox(szTmp, "선택된 항목 값", MB_OK | MB_ICONINFORMATION);
				*/
            }
            return TRUE;
    }
    return CStatic::OnCommand(wp, lp);
}



void CDCRScopeWnd::SetAnnotation(BOOL bEnable)
{
    PEnset(m_hWndPE, PEP_bSHOWANNOTATIONS, bEnable);					//Annotation을 사용 한다.
	PEnset(m_hWndPE, PEP_bALLOWANNOTATIONCONTROL, bEnable);			//사용자가 선택 가능 
	PEnset(m_hWndPE, PEP_nLINEANNOTATIONTEXTSIZE, 100);				//Annotation을 보여 준다.
}

void CDCRScopeWnd::OnSize(UINT nType, int cx, int cy) 
{
	CStatic::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	RECT r;
	if(::IsWindow(this->GetSafeHwnd()))
	{
		::GetClientRect(m_hWnd, &r);
		::MoveWindow(m_hWndPE, 0, 0, r.right, r.bottom, TRUE);
	}	
}


void CDCRScopeWnd::SetMainTitle(CString str)
{
	SetSZ(PEP_szMAINTITLE, (LPSTR) (LPCTSTR) str);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::SetSubTitle(CString str)
{
	SetSZ(PEP_szSUBTITLE, (LPSTR) (LPCTSTR)  str);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::SaveGraph()
{
	PElaunchexport(m_hWndPE);
	//PElaunchtextexport(m_hWndPE, TRUE, "c:\\aa.txt");
}

void CDCRScopeWnd::AddXAnnotation(float fXData1, float fXData2, CString str1, CString str2)
{
	double	m_dVLA[2];							//Point
	int		m_nVLAT[2];						//Line Style
	long	m_lVLAC[2];						//Line Color
	char szBuff[64];

	sprintf(szBuff, "|t%s\t|t%s\t", str1, str2);
	m_dVLA[0] = fXData1;
	m_dVLA[1] = fXData2;
	m_nVLAT[0] = PELT_THINSOLID;
	m_nVLAT[1] = PELT_THINSOLID;
	m_lVLAC[0] = ANNOTATION_COLOR;
	m_lVLAC[1] = ANNOTATION_COLOR;

	//Vertical line annotation
	PEvset(m_hWndPE, PEP_faVERTLINEANNOTATION, m_dVLA, 2);
//	PEvset(m_hWndPE, PEP_szaVERTLINEANNOTATIONTEXT, szBuff, 2);
	PEvset(m_hWndPE, PEP_naVERTLINEANNOTATIONTYPE, m_nVLAT, 2);
	PEvset(m_hWndPE, PEP_dwaVERTLINEANNOTATIONCOLOR, m_lVLAC, 2);


	//X축 Annotation
	sprintf(szBuff, "%s", str1);
	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 0, &m_dVLA[0]);
	PEvsetcell(m_hWndPE, PEP_szaXAXISANNOTATIONTEXT, 0, szBuff);
	PEvsetcell(m_hWndPE, PEP_dwaXAXISANNOTATIONCOLOR, 0, &m_lVLAC[0]);
	sprintf(szBuff, "%s", str2);
	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 1, &m_dVLA[1]);
	PEvsetcell(m_hWndPE, PEP_szaXAXISANNOTATIONTEXT, 1, szBuff);
	PEvsetcell(m_hWndPE, PEP_dwaXAXISANNOTATIONCOLOR, 1, &m_lVLAC[1]);
	PEnset(m_hWndPE, PEP_nAXESANNOTATIONTEXTSIZE, 80);
		

//	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 0, szBuff);
//	PEnset(m_hWndPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);


	


/*  PEnset(m_hWndPE, PEP_bSHOWGRAPHANNOTATIONS, TRUE);					//Annotation을 사용 한다.
	double GAX[15] ={	70.0F, 93.0F, 70.0F, 170.0F, 195.0F, 170.0F,
                        270.0F, 295.0F, 270.0F,  370.0F, 395.0F, 370.0F,    
                        470.0F, 495.0F, 470.0F	};

    double GAY[15] = {	280.0F, 115.0F, 280.0F, 280.0F, 115.0F, 280.0F,
                        280.0F, 115.0F, 280.0F, 280.0F, 115.0F, 280.0F,
                        280.0F, 115.0F, 280.0F	};

    int GAT[15] = {		PEGAT_LINECONTINUE, PEGAT_DOWNTRIANGLESOLID, PEGAT_DOWNTRIANGLESOLID, 
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK};
    long GAC[15] = {	0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0)	};

	double GAX[15]	= {	 70.0F,  93.0F,  70.0F	};
    double GAY[15]	= {	280.0F, 115.0F, 280.0F	};
    int GAT[15]		= {	PEGAT_TOPLEFT, 	PEGAT_BOTTOMRIGHT,	PEGAT_LINECONTINUE};
    long GAC[15]	= {	0L, 0L, RGB(0,255,0)	};
	
	PEvset(m_hWndPE, PEP_faGRAPHANNOTATIONX, GAX, 3);
	PEvset(m_hWndPE, PEP_faGRAPHANNOTATIONY, GAY, 3);
	PEvset(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, GAT,3);
	PEvset(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, GAC, 3);

	int LAT[2] = {PEGAT_MEDIUMSOLIDLINE};
	long LAC[2] = {RGB(0, 0, 255)};
	char LAText[] = "Test Area\t";
	PEvset(m_hWndPE, PEP_naLEGENDANNOTATIONTYPE, LAT, 1);
	PEvset(m_hWndPE, PEP_dwaLEGENDANNOTATIONCOLOR, LAC, 1);
	PEvset(m_hWndPE, PEP_szaLEGENDANNOTATIONTEXT, LAText, 1);
*/            
	::InvalidateRect(m_hWndPE, NULL, FALSE);	
}

void CDCRScopeWnd::AddLineAnnotation(int nIndex, float fStartX, float fStartY, float fEndX, float fEndY, CString str)
{
    int nType = PEGAT_MEDIUMSOLIDLINE;
	DWORD color = ANNOTATION_COLOR;
	double fx, fy;

//	PEnset(m_hWndPE, PEP_bSHOWGRAPHANNOTATIONS, TRUE);					//Annotation을 사용 한다.

	fx = fStartX;
	fy = fStartY;
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONX, nIndex*2, &fx);
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONY, nIndex*2, &fy);
	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, nIndex*2, &color);
	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, nIndex*2, &nType);
	nType = PEGAT_LINECONTINUE;
	fx = fEndX;
	fy = fEndY;
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONX, nIndex*2+1, &fx);
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONY, nIndex*2+1, &fy);
	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, nIndex*2+1, &color);
	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, nIndex*2+1, &nType);

	char szBuff[64];
	ZeroMemory(szBuff, sizeof(szBuff));
	sprintf(szBuff, "%s", str);

	PEvsetcell(m_hWndPE, PEP_szaGRAPHANNOTATIONTEXT, nIndex*2, szBuff);
	PEvsetcell(m_hWndPE, PEP_szaGRAPHANNOTATIONTEXT, nIndex*2+1, "");
	PEnset(m_hWndPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);

//	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, m_nVLAT, 2);
//	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR,nIndex*2, &color);

	::InvalidateRect(m_hWndPE, NULL, FALSE);	
}
/*
void CDCRScopeWnd::CopyImg()
{
	PEcopybitmaptoclipboard(m_hWndPE, NULL);
}
*/

void CDCRScopeWnd::CustomizeDialog()
{
	PElaunchcustomize(m_hWndPE);
}
