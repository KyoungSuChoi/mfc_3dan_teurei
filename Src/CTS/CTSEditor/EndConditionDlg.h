#if !defined(AFX_ENDCONDITIONDLG_H__C1738E09_89C0_40EE_95E1_D18F031BCBA2__INCLUDED_)
#define AFX_ENDCONDITIONDLG_H__C1738E09_89C0_40EE_95E1_D18F031BCBA2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EndConditionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEndConditionDlg dialog
#include "DateTimeCtrl.h"
#include "CTSEditorDoc.h"

#define WM_END_DLG_CLOSE		WM_USER+3001

class CEndConditionDlg : public CDialog
{
// Construction
public:
	virtual ~CEndConditionDlg();
	bool LanguageinitMonConfig(CString strClassName);
	CString *TEXT_LANG;
	int LispUpEndVGotoStepCombo();
	void DisplayData();
	int ListUpGotoStepCombo();
	void InitToolTips();
	void UpdateEndCondition();
	int m_nStepNo;
	CEndConditionDlg(CWnd* pParent = NULL);   // standard constructor

	SECCurrencyEdit	m_EndV;
	SECCurrencyEdit	m_EndI;
	SECCurrencyEdit	m_EndC;
//	CMyDateTimeCtrl	m_EndTime;
	SECCurrencyEdit	m_EnddV;
	SECCurrencyEdit	m_EnddI;
	SECCurrencyEdit	m_EndWattHour;
	SECCurrencyEdit	m_EndTemp;
	SECCurrencyEdit	m_StartTemp;
//	SECCurrencyEdit	m_EndSocC;
	
	
	CWnd *m_pParent;
	CToolTipCtrl m_tooltip;

	CCTSEditorDoc *m_pDoc;
	BOOL   m_bEditType_CP_AD;	//ljb 0:Formation -> CP Mode 없음	1 : 출력선별기 -> CP Mode 있음


	void OnOK();
//	CCTSEditorView *m_pView;

// Dialog Data
	//{{AFX_DATA(CEndConditionDlg)
	enum { IDD = IDD_END_COND_FORM_DLG };
	CComboBox	m_ctrlEndVGoto;
	CComboBox	m_ctrlGoto;
	CDateTimeCtrl	m_ctrlEndTime;
	CComboBox	m_ctrlCapaStepList;
	CLabel	m_strStepNo;
	UINT	m_nLoopInfoGoto;
	UINT	m_nLoopInfoCycle;
	UINT	m_nLoopInfoEndVGoto;
	UINT	m_nLoopInfoEndTGoto;
	UINT	m_nLoopInfoEndCGoto;
	BOOL	m_bUseActualCapa;
	float	m_fSocRate;
	UINT	m_nEndDay;
	UINT	m_lEndMSec;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEndConditionDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL m_bUseTemp;
	int ListUpCapaStepCombo();

	// Generated message map functions
	//{{AFX_MSG(CEndConditionDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnCheck1();
	afx_msg void OnSelchangeCapaStepCombo();
	afx_msg void OnChangeEndDayEdit();
	afx_msg void OnDeltaposEndTimeMsecSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillFocusEndMsec();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENDCONDITIONDLG_H__C1738E09_89C0_40EE_95E1_D18F031BCBA2__INCLUDED_)
