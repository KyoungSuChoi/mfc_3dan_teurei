#if !defined(AFX_GRIDCOMBOBOX_H__D24E56E2_3CF1_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_GRIDCOMBOBOX_H__D24E56E2_3CF1_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GridComboBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGridComboBox window

#define WM_COMBO_SELECT			WM_USER+3301
#define WM_COMBO_DROP_DOWN		WM_USER+3302


class CGridComboBox : public CGXComboBox
{
// Construction
public:
	CGridComboBox(CWnd *pFormView, CGXGridCore* pGrid, UINT nEditID, UINT nListBoxID, UINT nFlags);

// Attributes
public:
	CWnd * m_pFormView;
	int		m_OldSel;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridComboBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	void ResetItemData();
	int GetItemDataSize();
	CUIntArray m_aItemData;
	DWORD GetItemData( int nIndex ) const;
	int SetItemData( int nIndex, DWORD dwItemData );
	virtual ~CGridComboBox();

	void OnStoreDroppedList(CListBox* pLbox);
	void OnFillDroppedList(CListBox* pLbox);

	// Generated message map functions
protected:
	//{{AFX_MSG(CGridComboBox)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDCOMBOBOX_H__D24E56E2_3CF1_11D2_80A6_0020741517CE__INCLUDED_)
