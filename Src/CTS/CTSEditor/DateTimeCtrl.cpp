// DateTimeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "DateTimeCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyDateTimeCtrl

CMyDateTimeCtrl::CMyDateTimeCtrl()
{
}

CMyDateTimeCtrl::~CMyDateTimeCtrl()
{
}


BEGIN_MESSAGE_MAP(CMyDateTimeCtrl, SECDateTimeCtrl)
	//{{AFX_MSG_MAP(CMyDateTimeCtrl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMyDateTimeCtrl message handlers

void CMyDateTimeCtrl::OnChanged()
{
	SECDateTimeCtrl::OnChanged();
	if(IsWindowEnabled())
		GetParent()->PostMessage(WM_DATETIME_CHANGED, (WPARAM) GetDlgCtrlID(), (LPARAM) this);
}
