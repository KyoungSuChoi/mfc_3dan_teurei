#if !defined(AFX_CELLSTATERECORDSET_H__426C03DA_E388_420F_A419_BD0097C15A7C__INCLUDED_)
#define AFX_CELLSTATERECORDSET_H__426C03DA_E388_420F_A419_BD0097C15A7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CellStateRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCellStateRecordSet recordset

class CCellStateRecordSet : public CRecordset
{
public:
	CCellStateRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CCellStateRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CCellStateRecordSet, CRecordset)
	long	m_ID;
	long	m_TraySerial;
	long	m_CellCode;
	CString	m_CellGrade;
	long	m_ChNo;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCellStateRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELLSTATERECORDSET_H__426C03DA_E388_420F_A419_BD0097C15A7C__INCLUDED_)
