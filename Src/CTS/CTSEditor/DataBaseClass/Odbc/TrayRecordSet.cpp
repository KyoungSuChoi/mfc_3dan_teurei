// TrayRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "TrayRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrayRecordSet

IMPLEMENT_DYNAMIC(CTrayRecordSet, CRecordset)

CTrayRecordSet::CTrayRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTrayRecordSet)
	m_ID = 0;
	m_TraySerial = 0;
	m_JigID = _T("");
	m_TeskID = 0;
	m_TrayNo = _T("");
	m_TrayName = _T("");
	m_UserName = _T("");
	m_Description = _T("");
	m_ModelKey = 0;
	m_TestKey = 0;
	m_LotNo = _T("");
	m_TestSerialNo = _T("");
	m_CellNo = 0;
	m_InputCellNo = 0;
	m_NormalCount = 0;
	m_FailCount = 0;
	m_CellCode = _T("");
	m_GradeCode = _T("");
	m_OperatorID = _T("");
	m_ModuleID = 0;
	m_GroupIndex = 0;
	m_StepIndex = 0;
	m_TrayType = 0;
	m_nFields = 25;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CTrayRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CTrayRecordSet::GetDefaultSQL()
{
	return _T("[Tray]");
}

void CTrayRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTrayRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[ID]"), m_ID);
	RFX_Long(pFX, _T("[TraySerial]"), m_TraySerial);
	RFX_Text(pFX, _T("[JigID]"), m_JigID);
	RFX_Long(pFX, _T("[TeskID]"), m_TeskID);
	RFX_Date(pFX, _T("[RegistedTime]"), m_RegistedTime);
	RFX_Text(pFX, _T("[TrayNo]"), m_TrayNo);
	RFX_Text(pFX, _T("[TrayName]"), m_TrayName);
	RFX_Text(pFX, _T("[UserName]"), m_UserName);
	RFX_Text(pFX, _T("[Description]"), m_Description);
	RFX_Long(pFX, _T("[ModelKey]"), m_ModelKey);
	RFX_Long(pFX, _T("[TestKey]"), m_TestKey);
	RFX_Text(pFX, _T("[LotNo]"), m_LotNo);
	RFX_Text(pFX, _T("[TestSerialNo]"), m_TestSerialNo);
	RFX_Date(pFX, _T("[TestDateTime]"), m_TestDateTime);
	RFX_Long(pFX, _T("[CellNo]"), m_CellNo);
	RFX_Long(pFX, _T("[InputCellNo]"), m_InputCellNo);
	RFX_Long(pFX, _T("[NormalCount]"), m_NormalCount);
	RFX_Long(pFX, _T("[FailCount]"), m_FailCount);
	RFX_Text(pFX, _T("[CellCode]"), m_CellCode);
	RFX_Text(pFX, _T("[GradeCode]"), m_GradeCode);
	RFX_Text(pFX, _T("[OperatorID]"), m_OperatorID);
	RFX_Long(pFX, _T("[ModuleID]"), m_ModuleID);
	RFX_Long(pFX, _T("[GroupIndex]"), m_GroupIndex);
	RFX_Long(pFX, _T("[StepIndex]"), m_StepIndex);
	RFX_Long(pFX, _T("[TrayType]"), m_TrayType);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTrayRecordSet diagnostics

#ifdef _DEBUG
void CTrayRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTrayRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
