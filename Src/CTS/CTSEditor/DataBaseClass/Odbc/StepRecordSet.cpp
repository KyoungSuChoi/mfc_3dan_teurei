// StepRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "StepRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet

IMPLEMENT_DYNAMIC(CStepRecordSet, CRecordset)

CStepRecordSet::CStepRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CStepRecordSet)
	m_StepID = 0;
	m_TestID = 0;
	m_StepNo = 0;
	m_StepType = 0;
	m_StepMode = 0;
	m_Vref = 0.0f;
	m_Iref = 0.0f;
	m_EndV = 0.0f;
	m_EndI = 0.0f;
	m_EndCapacity = 0.0f;
	m_End_dV = 0.0f;
	m_End_dI = 0.0f;
	m_OverV = 0.0f;
	m_LimitV = 0.0f;
	m_OverI = 0.0f;
	m_LimitI = 0.0f;
	m_OverCapacity = 0.0f;
	m_LimitCapacity = 0.0f;
	m_OverImpedance = 0.0f;
	m_LimitImpedance = 0.0f;
	m_DeltaTime = 0;
	m_DeltaV = 0.0f;
	m_DeltaI = 0.0f;
	m_Grade = FALSE;
	m_VoltageReport = 0;
	m_CurrentReport = 0;
	m_CapacityReport = 0;
	m_TimeReport = 0;
	m_ImpedanceReport = 0;
	m_WattReport = 0;
	m_WattHourReport = 0;
	m_CompTime1 = 0;
	m_CompValue1 = 0.0f;
	m_CompTime2 = 0;
	m_CompValue2 = 0.0f;
	m_CompTime3 = 0;
	m_CompValue3 = 0.0f;
	m_DeltaTime1 = 0;
	m_StepProcType = 0;
	m_nFields = 40;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CStepRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CStepRecordSet::GetDefaultSQL()
{
	return _T("[Step]");
}

void CStepRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CStepRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[StepID]"), m_StepID);
	RFX_Long(pFX, _T("[TestID]"), m_TestID);
	RFX_Long(pFX, _T("[StepNo]"), m_StepNo);
	RFX_Long(pFX, _T("[StepType]"), m_StepType);
	RFX_Long(pFX, _T("[StepMode]"), m_StepMode);
	RFX_Single(pFX, _T("[Vref]"), m_Vref);
	RFX_Single(pFX, _T("[Iref]"), m_Iref);
	RFX_Date(pFX, _T("[EndTime]"), m_EndTime);
	RFX_Single(pFX, _T("[EndV]"), m_EndV);
	RFX_Single(pFX, _T("[EndI]"), m_EndI);
	RFX_Single(pFX, _T("[EndCapacity]"), m_EndCapacity);
	RFX_Single(pFX, _T("[End_dV]"), m_End_dV);
	RFX_Single(pFX, _T("[End_dI]"), m_End_dI);
	RFX_Single(pFX, _T("[OverV]"), m_OverV);
	RFX_Single(pFX, _T("[LimitV]"), m_LimitV);
	RFX_Single(pFX, _T("[OverI]"), m_OverI);
	RFX_Single(pFX, _T("[LimitI]"), m_LimitI);
	RFX_Single(pFX, _T("[OverCapacity]"), m_OverCapacity);
	RFX_Single(pFX, _T("[LimitCapacity]"), m_LimitCapacity);
	RFX_Single(pFX, _T("[OverImpedance]"), m_OverImpedance);
	RFX_Single(pFX, _T("[LimitImpedance]"), m_LimitImpedance);
	RFX_Long(pFX, _T("[DeltaTime]"), m_DeltaTime);
	RFX_Single(pFX, _T("[DeltaV]"), m_DeltaV);
	RFX_Single(pFX, _T("[DeltaI]"), m_DeltaI);
	RFX_Bool(pFX, _T("[Grade]"), m_Grade);
	RFX_Long(pFX, _T("[VoltageReport]"), m_VoltageReport);
	RFX_Long(pFX, _T("[CurrentReport]"), m_CurrentReport);
	RFX_Long(pFX, _T("[CapacityReport]"), m_CapacityReport);
	RFX_Long(pFX, _T("[TimeReport]"), m_TimeReport);
	RFX_Long(pFX, _T("[ImpedanceReport]"), m_ImpedanceReport);
	RFX_Long(pFX, _T("[WattReport]"), m_WattReport);
	RFX_Long(pFX, _T("[WattHourReport]"), m_WattHourReport);
	RFX_Long(pFX, _T("[CompTime1]"), m_CompTime1);
	RFX_Single(pFX, _T("[CompValue1]"), m_CompValue1);
	RFX_Long(pFX, _T("[CompTime2]"), m_CompTime2);
	RFX_Single(pFX, _T("[CompValue2]"), m_CompValue2);
	RFX_Long(pFX, _T("[CompTime3]"), m_CompTime3);
	RFX_Single(pFX, _T("[CompValue3]"), m_CompValue3);
	RFX_Long(pFX, _T("[DeltaTime1]"), m_DeltaTime1);
	RFX_Long(pFX, _T("[StepProcType]"), m_StepProcType);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet diagnostics

#ifdef _DEBUG
void CStepRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CStepRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
