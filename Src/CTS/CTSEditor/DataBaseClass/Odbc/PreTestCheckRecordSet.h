#if !defined(AFX_PRETESTCHECKRECORDSET_H__2AEAC715_A0AF_4BA0_B006_F6423141D503__INCLUDED_)
#define AFX_PRETESTCHECKRECORDSET_H__2AEAC715_A0AF_4BA0_B006_F6423141D503__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreTestCheckRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet recordset

class CPreTestCheckRecordSet : public CRecordset
{
public:
	CPreTestCheckRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CPreTestCheckRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CPreTestCheckRecordSet, CRecordset)
	long	m_CheckID;
	long	m_TestID;
	float	m_MaxV;
	float	m_MinV;
	float	m_CurrentRange;
	float	m_OCVLimit;
	float	m_TrickleCurrent;
	CTime	m_TrickleTime;
	float	m_DeltaVoltage;
	long	m_MaxFaultBattery;
	CTime	m_AutoTime;
	BOOL	m_AutoProYN;
	BOOL	m_PreTestCheck;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreTestCheckRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRETESTCHECKRECORDSET_H__2AEAC715_A0AF_4BA0_B006_F6423141D503__INCLUDED_)
