#if !defined(AFX_USERRECORDSET_H__4C30AD53_1C58_4084_86C0_03FB965FB5A8__INCLUDED_)
#define AFX_USERRECORDSET_H__4C30AD53_1C58_4084_86C0_03FB965FB5A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserRecordset.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserRecordset recordset

class CUserRecordset : public CRecordset
{
public:
	CUserRecordset(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CUserRecordset)

// Field/Param Data
	//{{AFX_FIELD(CUserRecordset, CRecordset)
	long	m_Index;
	CString	m_UserID;
	CString	m_Password;
	CString	m_Name;
	long	m_Authority;
	CTime	m_RegistedDate;
	CString	m_Description;
	BOOL	m_AutoLogOut;
	long	m_AutoLogOutTime;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserRecordset)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERRECORDSET_H__4C30AD53_1C58_4084_86C0_03FB965FB5A8__INCLUDED_)
