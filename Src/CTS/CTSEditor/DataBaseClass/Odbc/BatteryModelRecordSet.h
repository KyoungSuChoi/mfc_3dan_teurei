#if !defined(AFX_MODELRECORDSET_H__570E22A0_5BA8_41DD_8988_D6FD7E292F6A__INCLUDED_)
#define AFX_MODELRECORDSET_H__570E22A0_5BA8_41DD_8988_D6FD7E292F6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BatteryModelRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBatteryModelRecordSet recordset

class CBatteryModelRecordSet : public CRecordset
{
public:
	CBatteryModelRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CBatteryModelRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CBatteryModelRecordSet, CRecordset)
	long	m_ModelID;
	long	m_No;
	CString	m_ModelName;
	CString	m_Description;
	CTime	m_CreatedTime;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBatteryModelRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELRECORDSET_H__570E22A0_5BA8_41DD_8988_D6FD7E292F6A__INCLUDED_)
