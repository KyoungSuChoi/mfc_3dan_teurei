#if !defined(AFX_STEPLISTRECORDSET_H__4537D979_3291_4E13_AAAA_7EA042CFEDE9__INCLUDED_)
#define AFX_STEPLISTRECORDSET_H__4537D979_3291_4E13_AAAA_7EA042CFEDE9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StepRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet recordset

class CStepRecordSet : public CRecordset
{
public:
	CStepRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CStepRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CStepRecordSet, CRecordset)
	long	m_StepID;
	long	m_TestID;
	long	m_StepNo;
	long	m_StepType;
	long	m_StepMode;
	float	m_Vref;
	float	m_Iref;
	CTime	m_EndTime;
	float	m_EndV;
	float	m_EndI;
	float	m_EndCapacity;
	float	m_End_dV;
	float	m_End_dI;
	float	m_OverV;
	float	m_LimitV;
	float	m_OverI;
	float	m_LimitI;
	float	m_OverCapacity;
	float	m_LimitCapacity;
	float	m_OverImpedance;
	float	m_LimitImpedance;
	long	m_DeltaTime;
	float	m_DeltaV;
	float	m_DeltaI;
	BOOL	m_Grade;
	long	m_VoltageReport;
	long	m_CurrentReport;
	long	m_CapacityReport;
	long	m_TimeReport;
	long	m_ImpedanceReport;
	long	m_WattReport;
	long	m_WattHourReport;
	long	m_CompTime1;
	float	m_CompValue1;
	long	m_CompTime2;
	float	m_CompValue2;
	long	m_CompTime3;
	float	m_CompValue3;
	long	m_DeltaTime1;
	long	m_StepProcType;

	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStepRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STEPLISTRECORDSET_H__4537D979_3291_4E13_AAAA_7EA042CFEDE9__INCLUDED_)
