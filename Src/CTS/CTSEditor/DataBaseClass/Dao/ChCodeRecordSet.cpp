// ChCodeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "ChCodeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChCodeRecordSet

IMPLEMENT_DYNAMIC(CChCodeRecordSet, CDaoRecordset)

CChCodeRecordSet::CChCodeRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CChCodeRecordSet)
	m_Code = 0;
	m_Messge = _T("");
	m_nFields = 2;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CChCodeRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
//	return _T("E:\\FormationV3_2\\PowerFormation\\DataBase\\Condition.mdb");
}

CString CChCodeRecordSet::GetDefaultSQL()
{
	return _T("[ChannelCode]");
}

void CChCodeRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CChCodeRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[Code]"), m_Code);
	DFX_Text(pFX, _T("[Messge]"), m_Messge);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CChCodeRecordSet diagnostics

#ifdef _DEBUG
void CChCodeRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CChCodeRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
