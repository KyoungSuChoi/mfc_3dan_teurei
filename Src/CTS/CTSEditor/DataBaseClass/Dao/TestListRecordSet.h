#if !defined(AFX_TESTLISTRECORDSET_H__23D69716_11CD_4DD7_A3E9_0F911004512F__INCLUDED_)
#define AFX_TESTLISTRECORDSET_H__23D69716_11CD_4DD7_A3E9_0F911004512F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestListRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestListRecordSet DAO recordset

class CTestListRecordSet : public CDaoRecordset
{
public:
	CTestListRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestListRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CTestListRecordSet, CDaoRecordset)
	long	m_TestID;
	long	m_ModelID;
	CString	m_TestName;
	CString	m_Description;
	CString	m_Creator;
	COleDateTime	m_ModifiedTime;
	BOOL	m_PreTestCheck;
	long	m_ProcTypeID;
	long	m_TestNo;

	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestListRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTLISTRECORDSET_H__23D69716_11CD_4DD7_A3E9_0F911004512F__INCLUDED_)
