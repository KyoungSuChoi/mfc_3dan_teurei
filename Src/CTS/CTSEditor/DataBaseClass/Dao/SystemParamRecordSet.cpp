// SystemParamRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "SystemParamRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSystemParamRecordSet

IMPLEMENT_DYNAMIC(CSystemParamRecordSet, CDaoRecordset)

CSystemParamRecordSet::CSystemParamRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	m_ModuleID = 0;
	m_ModuleType = 0;
	m_MaxVoltage = 0.0f;
	m_MinVoltage = 0.0f;
	m_MaxCurrent = 0.0f;
	m_MinCurrent = 0.0f;
	m_OverTemperature = 0;
	m_UnderTemperature = 0;
	m_DifferTemp = 0;
	m_V24InMax = 0.0f;
	m_V24InMin = 0.0f;
	m_V24OutMax = 0.0f;
	m_V24OutMin = 0.0f;
	m_V12OutMax = 0.0f;
	m_V12OutMin = 0.0f;
	m_AutoProcess = FALSE;
	m_UseTempLimitFlag = FALSE;
	m_OverGas = 0;
	m_UseGasLimitFlag = FALSE;
	m_nFields = 20;
	m_SaveInterval = 0;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}

CString CSystemParamRecordSet::GetDefaultDBName()
{
return _T(GetDataBaseName());
//	return _T("C:\\My Documents\\Condition.mdb");
}


CString CSystemParamRecordSet::GetDefaultSQL()
{
	return _T("[SystemConfig]");
}

void CSystemParamRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSystemParamRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ModuleID]"), m_ModuleID);
	DFX_Long(pFX, _T("[ModuleType]"), m_ModuleType);
	DFX_Single(pFX, _T("[MaxVoltage]"), m_MaxVoltage);
	DFX_Single(pFX, _T("[MinVoltage]"), m_MinVoltage);
	DFX_Single(pFX, _T("[MaxCurrent]"), m_MaxCurrent);
	DFX_Single(pFX, _T("[MinCurrent]"), m_MinCurrent);
	DFX_Long(pFX, _T("[OverTemperature]"), m_OverTemperature);
	DFX_Long(pFX, _T("[UnderTemperature]"), m_UnderTemperature);
	DFX_Long(pFX, _T("[DifferTemp]"), m_DifferTemp);
	DFX_Single(pFX, _T("[V24InMax]"), m_V24InMax);
	DFX_Single(pFX, _T("[V24InMin]"), m_V24InMin);
	DFX_Single(pFX, _T("[V24OutMax]"), m_V24OutMax);
	DFX_Single(pFX, _T("[V24OutMin]"), m_V24OutMin);
	DFX_Single(pFX, _T("[V12OutMax]"), m_V12OutMax);
	DFX_Single(pFX, _T("[V12OutMin]"), m_V12OutMin);
	DFX_Bool(pFX, _T("[AutoProcess]"), m_AutoProcess);
	DFX_Bool(pFX, _T("[UseTempLimitFlag]"), m_UseTempLimitFlag);
	DFX_Long(pFX, _T("[OverGas]"), m_OverGas);
	DFX_Bool(pFX, _T("[UseGasLimitFlag]"), m_UseGasLimitFlag);
	DFX_Long(pFX, _T("[SaveInterval]"), m_SaveInterval);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSystemParamRecordSet diagnostics

#ifdef _DEBUG
void CSystemParamRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSystemParamRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
