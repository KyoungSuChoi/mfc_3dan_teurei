#if !defined(AFX_SYSTEMPARAMRECORDSET_H__AE2C9939_6B4E_4398_9473_85839FE99A51__INCLUDED_)
#define AFX_SYSTEMPARAMRECORDSET_H__AE2C9939_6B4E_4398_9473_85839FE99A51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SystemParamRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSystemParamRecordSet DAO recordset

class CSystemParamRecordSet : public CDaoRecordset
{
public:
	CSystemParamRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSystemParamRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CSystemParamRecordSet, CDaoRecordset)
	long	m_ModuleID;
	long	m_ModuleType;
	float	m_MaxVoltage;
	float	m_MinVoltage;
	float	m_MaxCurrent;
	float	m_MinCurrent;
	long	m_OverTemperature;
	long	m_UnderTemperature;
	long	m_DifferTemp;
	float	m_V24InMax;
	float	m_V24InMin;
	float	m_V24OutMax;
	float	m_V24OutMin;
	float	m_V12OutMax;
	float	m_V12OutMin;
	BOOL	m_AutoProcess;
	BOOL	m_UseTempLimitFlag;
	long	m_OverGas;
	BOOL	m_UseGasLimitFlag;
	long	m_SaveInterval;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSystemParamRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYSTEMPARAMRECORDSET_H__AE2C9939_6B4E_4398_9473_85839FE99A51__INCLUDED_)
