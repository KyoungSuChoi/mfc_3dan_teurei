#if !defined(AFX_CELLSTATERECORDSET_H__3D33D569_E10B_4435_B0E2_1AC20A58AFD0__INCLUDED_)
#define AFX_CELLSTATERECORDSET_H__3D33D569_E10B_4435_B0E2_1AC20A58AFD0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CellStateRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCellStateRecordSet DAO recordset

class CCellStateRecordSet : public CDaoRecordset
{
public:
	int m_nTrayType;
	CCellStateRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CCellStateRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CCellStateRecordSet, CDaoRecordset)
	long	m_ID;
	long	m_TraySerial;
	long	m_CellCode;
	CString	m_CellGrade;
	long	m_ChNo;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCellStateRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELLSTATERECORDSET_H__3D33D569_E10B_4435_B0E2_1AC20A58AFD0__INCLUDED_)
