#if !defined(AFX_GRADERECORDSET_H__6689593C_FD2A_48D4_B9F9_6ECE776D64A3__INCLUDED_)
#define AFX_GRADERECORDSET_H__6689593C_FD2A_48D4_B9F9_6ECE776D64A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GradeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGradeRecordSet DAO recordset

class CGradeRecordSet : public CDaoRecordset
{
public:
	CGradeRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CGradeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CGradeRecordSet, CDaoRecordset)
	long	m_GradeID;
	long	m_StepID;
	long	m_ModelID;
	long	m_GradeItem;
	float	m_Value;
	float	m_Value1;
	CString	m_GradeCode;
	long	m_GradeIndex;	//20081007 kjh Grade의 순서를 사용하도록 변경
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradeRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRADERECORDSET_H__6689593C_FD2A_48D4_B9F9_6ECE776D64A3__INCLUDED_)
