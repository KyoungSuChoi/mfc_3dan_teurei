#if !defined(AFX_CHCODERECORDSET_H__D83220A0_5330_42CB_B47A_EA8919875FF6__INCLUDED_)
#define AFX_CHCODERECORDSET_H__D83220A0_5330_42CB_B47A_EA8919875FF6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChCodeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChCodeRecordSet DAO recordset

class CChCodeRecordSet : public CDaoRecordset
{
public:
	CChCodeRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CChCodeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CChCodeRecordSet, CDaoRecordset)
	long	m_Code;
	CString	m_Messge;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChCodeRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHCODERECORDSET_H__D83220A0_5330_42CB_B47A_EA8919875FF6__INCLUDED_)
