#if !defined(AFX_PROCTYPERECORDSET_H__DC8D0C25_062D_49AE_A377_AD6616377E2F__INCLUDED_)
#define AFX_PROCTYPERECORDSET_H__DC8D0C25_062D_49AE_A377_AD6616377E2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProcTypeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProcTypeRecordSet DAO recordset

class CProcTypeRecordSet : public CDaoRecordset
{
public:
	CProcTypeRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CProcTypeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CProcTypeRecordSet, CDaoRecordset)
	long	m_ProcType;
	long	m_ProcID;
	CString	m_Description;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProcTypeRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROCTYPERECORDSET_H__DC8D0C25_062D_49AE_A377_AD6616377E2F__INCLUDED_)
