///////////////////////////////////////
//
//		ADPOWER Battery Charge/Discharge System
//		DataBase Header File
//
///////////////////////////////////////

#ifndef _ADPOWER_SCHEDULER_DATA_BASE_INCLUDE_H_
#define _ADPOWER_SCHEDULER_DATA_BASE_INCLUDE_H_

#ifdef _DATABASE_CONNECTION_ODBC
#include "./Odbc/BatteryModelRecordSet.h"
#include "./Odbc/GradeRecordSet.h"
#include "./Odbc/PreTestCheckRecordSet.h"
#include "./Odbc/StepRecordSet.h"
#include "./Odbc/TestListRecordSet.h"
#include "./Odbc/ProcTypeRecordSet.h"
#include "./Odbc/ChCodeRecordSet.h"
#include "./Odbc/SystemParamRecordSet.h"
#include "./Odbc/TrayRecordSet.h"
#include "./Odbc/CellStateRecordSet.h"
#include "./Odbc/UserRecordSet.h"

#else

#include "./Dao/BatteryModelRecordSet.h"
#include "./Dao/GradeRecordSet.h"
#include "./Dao/PreTestCheckRecordSet.h"
#include "./Dao/StepRecordSet.h"
#include "./Dao/TestListRecordSet.h"
#include "./Dao/ProcTypeRecordSet.h"
#include "./Dao/ChCodeRecordSet.h"
#include "./Dao/SystemParamRecordSet.h"
#include "./Dao/TrayRecordSet.h"
#include "./Dao/CellStateRecordSet.h"
#include "./Dao/UserRecordSet.h"

#endif	//_DATABASE_CONNECTION_ODBC

#endif	//_ADPOWER_SCHEDULER_DATA_BASE_INCLUDE_H_


