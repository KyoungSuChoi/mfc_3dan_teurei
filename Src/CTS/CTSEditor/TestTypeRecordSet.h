#if !defined(AFX_TESTTYPERECORDSET_H__6E2D6678_8EAA_4E8C_86A7_F9154FD29739__INCLUDED_)
#define AFX_TESTTYPERECORDSET_H__6E2D6678_8EAA_4E8C_86A7_F9154FD29739__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestTypeRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestTypeRecordSet DAO recordset

class CTestTypeRecordSet : public CDaoRecordset
{
public:
	CTestTypeRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestTypeRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CTestTypeRecordSet, CDaoRecordset)
	long	m_TestType;
	CString	m_TestTypeName;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestTypeRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTTYPERECORDSET_H__6E2D6678_8EAA_4E8C_86A7_F9154FD29739__INCLUDED_)
