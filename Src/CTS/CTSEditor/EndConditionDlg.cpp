// EndConditionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "EndConditionDlg.h"
//#include "..\\PowerFormation\\Msgdefine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CEndConditionDlg dialog


CEndConditionDlg::CEndConditionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEndConditionDlg::IDD, pParent)
{
	LanguageinitMonConfig(_T("CEndConditionDlg"));
	//{{AFX_DATA_INIT(CEndConditionDlg)
	m_nLoopInfoGoto = 0;
	m_nLoopInfoCycle = 0;
	m_bUseActualCapa = FALSE;
	m_fSocRate = 0.0f;
	m_nEndDay = 0;
	m_lEndMSec = 0;
	//}}AFX_DATA_INIT
//	m_nStepNo =0;
	m_pParent = pParent;
	m_nStepNo = 0;
	m_nLoopInfoEndVGoto = 0;
	m_nLoopInfoEndTGoto = 0;
	m_nLoopInfoEndCGoto = 0;
	m_bEditType_CP_AD = FALSE;
}

CEndConditionDlg::~CEndConditionDlg()
{
	if(TEXT_LANG != NULL){
		delete[] TEXT_LANG;
		TEXT_LANG = NULL;
	}
}

bool CEndConditionDlg::LanguageinitMonConfig(CString strClassName) 
{
	int i=0;
	int nTextCnt = 0;
	TEXT_LANG = NULL;

	CString strErr = _T("");
	CString strTemp = _T("");
	strTemp = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, _T("TEXT_")+strClassName+_T("_CNT"),  _T("TEXT_")+strClassName+_T("_CNT"));

	nTextCnt = atoi(strTemp);

	if( nTextCnt > 0 )
	{
		TEXT_LANG = new CString[nTextCnt]; //동적할당

		for( i=0; i<nTextCnt; i++ )
		{
			strTemp.Format("TEXT_%s_%d",strClassName, i);
			TEXT_LANG[i] = GIni::ini_GetLangText(g_strLangPath, _T("IDD_")+strClassName, strTemp, strTemp, g_nLanguage);

			if( TEXT_LANG[i] == strTemp )
			{
				if( strErr.IsEmpty() )
				{
					strErr = "Language error " + strTemp; 
				}
				else
				{
					strErr += "," + strTemp;
				}
			}
		}

		if( !strErr.IsEmpty() )
		{
			AfxMessageBox(strErr);
			return false;
		}
	}
	return true;
}


void CEndConditionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEndConditionDlg)
	DDX_Control(pDX, IDC_GOTO_COMBO, m_ctrlGoto);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_ctrlEndTime);
	DDX_Control(pDX, IDC_CAPA_STEP_COMBO, m_ctrlCapaStepList);
	DDX_Control(pDX, IDC_STEP_NO, m_strStepNo);
#ifdef _CYCLER_
	DDX_Control(pDX, IDC_ENDV_GOTO_COMBO, m_ctrlEndVGoto);
#endif
	DDX_Text(pDX, IDC_EDIT_GOTO, m_nLoopInfoGoto);
	DDX_Text(pDX, IDC_EDIT_CYCLE, m_nLoopInfoCycle);
	DDX_Check(pDX, IDC_CHECK1, m_bUseActualCapa);
	DDX_Text(pDX, IDC_SOC_RATE_EDIT, m_fSocRate);
	DDV_MinMaxFloat(pDX, m_fSocRate, 0.f, 100.f);
	DDX_Text(pDX, IDC_END_DAY_EDIT, m_nEndDay);
	DDX_Text(pDX, IDC_END_MSEC_EDIT, m_lEndMSec);
#ifdef _CYCLER_
	DDX_Text(pDX, IDC_EDIT_ENDVGOTO, m_nLoopInfoEndVGoto);
	DDX_Text(pDX, IDC_EDIT_ENDTGOTO, m_nLoopInfoEndTGoto);
	DDX_Text(pDX, IDC_EDIT_ENDCGOTO, m_nLoopInfoEndCGoto);
#endif
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEndConditionDlg, CDialog)
	//{{AFX_MSG_MAP(CEndConditionDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_CBN_SELCHANGE(IDC_CAPA_STEP_COMBO, OnSelchangeCapaStepCombo)
	ON_EN_CHANGE(IDC_END_DAY_EDIT, OnChangeEndDayEdit)
	ON_EN_KILLFOCUS(IDC_END_MSEC_EDIT, OnKillFocusEndMsec)
	ON_NOTIFY(UDN_DELTAPOS, IDC_END_TIME_MSEC_SPIN, OnDeltaposEndTimeMsecSpin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEndConditionDlg message handlers

BOOL CEndConditionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();



	m_strStepNo.SetTextColor(RGB(255, 0, 0));
	
	// TODO: Add extra initialization here
	COleDateTime ole;
	SECCurrencyEdit::Format fmt(FALSE);
	fmt.SetMonetarySymbol(0);
	fmt.SetPositiveFormat(2);
	fmt.SetNegativeFormat(2);
	fmt.EnableLeadingZero(FALSE);
	fmt.SetFractionalDigits(1);
	fmt.SetThousandSeparator(0);
	fmt.SetDecimalDigits(0);

/*	m_EndTime.AttachDateTimeCtrl(IDC_END_TIME, this, 0);
	m_EndTime.SetFormat("HH:mm:ss");
	m_EndTime.SetTime(ole);
*/
	int a = 3, b= 3;
	b = m_pDoc->GetVtgUnitFloat();
	if(m_pDoc->GetVtgUnit() == "V")
	{
		a = 3;
	}
	else
	{
		a = 6;
	}
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);

	m_EndV.Initialize(this, IDC_END_VTG);
	m_EndV.SetFormat(fmt); //m_EndV.SetValue(0.0);

	m_EnddV.Initialize(this, IDC_END_DELTAV);
	m_EnddV.SetFormat(fmt); //m_EnddV.SetValue(0.0);

	b = m_pDoc->GetCrtUnitFloat();
	if(m_pDoc->GetCrtUnit() == "A")
	{
		a = 3;
	}
	else
	{
		a = 6;
	}
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);
	m_EndI.Initialize(this, IDC_END_CRT);
	m_EndI.SetFormat(fmt); //m_EndI.SetValue(0.0);


	b = m_pDoc->GetCapUnitFloat();
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);
	m_EndC.Initialize(this, IDC_END_CAP);
	m_EndC.SetFormat(fmt); //m_EndC.SetValue(0.0);

	//End Watt
	b = m_pDoc->GetWattUnitFloat();
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);
	m_EnddI.Initialize(this, IDC_END_DELTAI);
	m_EnddI.SetFormat(fmt); //m_EnddI.SetValue(0.0);

	//End WattHour
	b = m_pDoc->GetWattHourUnitFloat();
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);
	m_EndWattHour.Initialize(this, IDC_END_WATTHOUR);
	m_EndWattHour.SetFormat(fmt); //m_EnddI.SetValue(0.0);
	
	//End Temp
	fmt.SetFractionalDigits(1);	fmt.SetDecimalDigits(3);
	m_EndTemp.Initialize(this, IDC_EDIT_END_TEMP);
	m_EndTemp.SetFormat(fmt); //m_EnddI.SetValue(0.0);

#ifdef _CYCLER_
	fmt.SetFractionalDigits(1);	fmt.SetDecimalDigits(3);
	m_StartTemp.Initialize(this, IDC_EDIT_START_TEMP);
	m_StartTemp.SetFormat(fmt); //m_EnddI.SetValue(0.0);
#endif

	

	//NI-MH Cell인 경우 종료 Delta V에서 Delta Vp를 입력 받는다.
//#ifndef _NIMH_CELL_
//	GetDlgItem(IDC_DELTA_VP_STATIC)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_END_DELTAV)->ShowWindow(SW_HIDE);	
//	GetDlgItem(IDC_END_DELTA_V_UNIT_STATIC)->ShowWindow(SW_HIDE);		
//#endif

/*	fmt.SetFractionalDigits(2);	fmt.SetDecimalDigits(2);
	m_EndSocC.Initialize(this, IDC_SOC_RATE_EDIT);
	m_EndSocC.SetFormat(fmt); //m_EndV.SetValue(0.0);
*/
	GetDlgItem(IDC_END_CRT_UNIT_STATIC)->SetWindowText(m_pDoc->GetCrtUnit());
	GetDlgItem(IDC_END_CAPA_UNIT_STATIC)->SetWindowText(m_pDoc->GetCapUnit());
	GetDlgItem(IDC_END_VTG_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());
	GetDlgItem(IDC_END_DELTA_V_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());

//	//End Watt로 사용 2006/5/1
//	GetDlgItem(IDC_END_CRT_UNIT_STATIC2)->SetWindowText(m_pDoc->GetCrtUnit());
	GetDlgItem(IDC_END_CRT_UNIT_STATIC2)->SetWindowText(m_pDoc->GetWattUnit());
	GetDlgItem(IDC_END_WATTHOUR_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattHourUnit());
	GetDlgItem(IDC_END_MSEC_EDIT)->SendMessage(EM_LIMITTEXT, 3, 0);


	m_ctrlEndTime.SetTime(DATE(0));
	m_ctrlEndTime.SetFormat("HH:mm:ss");

	//cycle, goto를 감춘다.
#ifndef _CYCLER_

#endif

	m_bUseTemp = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseTemp", FALSE);


	InitToolTips();
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEndConditionDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	if(m_nStepNo < 1)	return;
	
	char szTemp[10];
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	ASSERT(pStep);
	if (pStep->nProcType == 0 && pStep->chType == 0) return;

	BOOL	bEnable = FALSE;
	if(bShow)
	{
		m_ctrlGoto.ResetContent();

		sprintf(szTemp, "Step %3d", m_nStepNo);
		m_strStepNo.SetText(szTemp);

		GetDlgItem(IDC_EDIT_GOTO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GOTO_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_CYCLE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_END_TEMP)->EnableWindow(FALSE);
#ifdef _CYCLER_
		GetDlgItem(IDC_EDIT_START_TEMP)->EnableWindow(FALSE);
		GetDlgItem(IDC_ENDV_GOTO_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_ENDV_H)->SetWindowText("종료전압");
		GetDlgItem(IDC_STATIC_ENDC)->SetWindowText("종료용량");
#endif
		
//		GetDlgItem(IDC_CHECK1)->EnableWindow(FALSE);		//ljb REST flag
		GetDlgItem(IDC_CAPA_STEP_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_DATETIMEPICKER1)->SetFocus();
		GetDlgItem(IDC_END_CRT_STATIC)->SetWindowText(TEXT_LANG[0]); //"종료전류"		
		GetDlgItem(IDC_END_CRT_UNIT_STATIC)->SetWindowText(m_pDoc->GetCrtUnit());
		

		if(m_pDoc->m_lLoadedProcType ==  PS_PGS_AGING)
		{	//시간 종료만 가능 
			bEnable = FALSE;
		}
		else 
		{	
			bEnable = TRUE;
		}	

		switch(pStep->chType)
		{
		case PS_STEP_CHARGE:
			if (m_bEditType_CP_AD) //ljb 출력 선별기
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			else
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_END_MSEC_EDIT)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_STATIC_MSEC)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(bEnable); 
			GetDlgItem(IDC_END_DELTAI)->EnableWindow(bEnable); //EndWatt
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(bEnable); 
			GetDlgItem(IDC_CHECK1)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_END_TEMP)->EnableWindow(m_bUseTemp);
// 	#ifndef _EDLC_CELL_
// 			GetDlgItem(IDC_END_CAP)->EnableWindow(bEnable); 
// 			GetDlgItem(IDC_EDIT_ENDCGOTO)->EnableWindow(bEnable); 			
// 	#endif
			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE);	//ljb 임시 추가
			GetDlgItem(IDC_SOC_STATIC)->SetWindowText(TEXT_LANG[1]); //"의 SOC"

			if(ListUpCapaStepCombo() > 1)
			{
	//////////////////////////////////////////////////////////////////////////
	//2005/12/8 가장 마지막에 설정한 Capa 만 사용하도록 고정 시킨다.(사용자 설정이 안됨)
	//			GetDlgItem(IDC_CAPA_STEP_COMBO)->EnableWindow(TRUE);
	//////////////////////////////////////////////////////////////////////////
			}

			if(pStep->chMode == PS_MODE_CC)
			{
				GetDlgItem(IDC_END_VTG)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(bEnable);  
//				GetDlgItem(IDC_END_VTG)->EnableWindow(TRUE);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CCCV)
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				GetDlgItem(IDC_END_VTG)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(bEnable);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				//End Watt
				GetDlgItem(IDC_END_DELTAI)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				GetDlgItem(IDC_END_VTG)->EnableWindow(bEnable);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else
			{
				GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}	
		
			break;

		case PS_STEP_DISCHARGE:
			if(pStep->chMode == PS_MODE_CC)
			{
				GetDlgItem(IDC_END_VTG)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(bEnable);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CCCV)
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				GetDlgItem(IDC_END_VTG)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(bEnable);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_DELTAI)->EnableWindow(FALSE); 				//End Watt
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				GetDlgItem(IDC_END_VTG)->EnableWindow(bEnable);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else
			{
				GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);  
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}	
// 		#ifndef _EDLC_CELL_
// 			GetDlgItem(IDC_END_CAP)->EnableWindow(bEnable); 
// 			GetDlgItem(IDC_EDIT_ENDCGOTO)->EnableWindow(bEnable); 			
// 		#endif
			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE);	//ljb 임시 추가
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE);
			if (m_bEditType_CP_AD) //ljb 출력 선별기
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			else
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_END_MSEC_EDIT)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_STATIC_MSEC)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_SOC_STATIC)->SetWindowText(TEXT_LANG[2]); //"의 DOD"
			GetDlgItem(IDC_EDIT_END_TEMP)->EnableWindow(m_bUseTemp);

			GetDlgItem(IDC_END_DELTAV)->EnableWindow(bEnable); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(bEnable); 

			if(ListUpCapaStepCombo() > 1)
			{
	//////////////////////////////////////////////////////////////////////////
	//2005/12/8 가장 마지막에 설정한 Capa 만 사용하도록 고정 시킨다.(사용자 설정이 안됨)
	//			GetDlgItem(IDC_CAPA_STEP_COMBO)->EnableWindow(TRUE);
	//////////////////////////////////////////////////////////////////////////
			}
			GetDlgItem(IDC_CHECK1)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_DELTAI)->EnableWindow(bEnable); 
			
			break;
		case PS_STEP_REST:
			GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);                     
			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
//			GetDlgItem(IDC_CHECK1)->EnableWindow(TRUE);


			if (m_bEditType_CP_AD) //ljb 출력 선별기
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			else
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_END_MSEC_EDIT)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_STATIC_MSEC)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_EDIT_END_TEMP)->EnableWindow(m_bUseTemp);

			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 			
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAI)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 
			break;

		case PS_STEP_IMPEDANCE:
			if(pStep->chMode == PS_MODE_ACIMP)
			{
				GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			}
			else//pStep->chMode == PS_MODE_DCIMP)
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
				GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);   

			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 
						
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAI)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 
			GetDlgItem(IDC_EDIT_END_TEMP)->EnableWindow(m_bUseTemp);
			break;

		case PS_STEP_LOOP:
			ListUpGotoStepCombo();
			GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);      
			
			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 
			 			
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAI)->EnableWindow(FALSE); 
			GetDlgItem(IDC_GOTO_COMBO)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_GOTO)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_CYCLE)->EnableWindow(TRUE);
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 
			
			GetDlgItem(IDC_EDIT_CYCLE)->SetFocus();

			GetDlgItem(IDC_ENDV_GOTO_COMBO)->EnableWindow(FALSE);
			break;

// 		case PS_STEP_PATTERN:				
// 			
// 			GetDlgItem(IDC_END_CRT_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());
// 			GetDlgItem(IDC_END_VTG)->EnableWindow(TRUE);  			
// 			GetDlgItem(IDC_END_CRT)->EnableWindow(TRUE); 
// 			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
// 			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
// 			if (m_bEditType_CP_AD) //ljb 출력 선별기
// 			{
// 				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
// 				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
// 			}
// 			else
// 			{
// 				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->ShowWindow(SW_HIDE);
// 				GetDlgItem(IDC_END_MSEC_EDIT)->ShowWindow(SW_HIDE);
// 				GetDlgItem(IDC_STATIC_MSEC)->ShowWindow(SW_HIDE);
// 				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
// 				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
// 			}
// 			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE); 
// 				
// 			GetDlgItem(IDC_END_DELTAV)->EnableWindow(TRUE); 
// 			GetDlgItem(IDC_END_DELTAI)->EnableWindow(TRUE);		//end power 
// 			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(TRUE); 
// 			GetDlgItem(IDC_EDIT_END_TEMP)->EnableWindow(m_bUseTemp);
// 			break;
			
		case PS_STEP_OCV:
		case PS_STEP_END:
		default:
			GetDlgItem(IDC_END_VTG)->EnableWindow(FALSE);    
			GetDlgItem(IDC_EDIT_ENDVGOTO)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
			//GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); //ljb 삭제
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 
			GetDlgItem(IDC_EDIT_ENDCGOTO)->EnableWindow(FALSE); 			
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAI)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 
			GetDlgItem(IDC_EDIT_ENDTGOTO)->EnableWindow(FALSE); 
			break;
		}

		DisplayData();
	}
	else			//Hide Window
	{
		UpdateEndCondition();
	}
}

void CEndConditionDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
}

void CEndConditionDlg::OnOK()
{
	UpdateEndCondition();
	CDialog::OnOK();
}

void CEndConditionDlg::UpdateEndCondition()
{
	UpdateData();
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	ASSERT(pStep);
		
	if(m_nStepNo <1 || m_nStepNo >SCH_MAX_STEP)		return;
	double	dwValue =0.0;
	if(pStep)
	{
		m_EndV.GetValue(dwValue);		pStep->fEndV = m_pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);		//V단위 표기 
		m_EndI.GetValue(dwValue);		pStep->fEndI = m_pDoc->IUnitTrans(dwValue);	//(dwValue*I_UNIT_FACTOR);	
		m_EndC.GetValue(dwValue);		pStep->fEndC = m_pDoc->CUnitTrans(dwValue);	//(dwValue*C_UNIT_FACTOR);											//F단위 표기 
		m_EnddV.GetValue(dwValue);		pStep->fEndDV = m_pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);
		m_EnddI.GetValue(dwValue);				pStep->fEndW = m_pDoc->WattUnitTrans(dwValue);	//(dwValue*I_UNIT_FACTOR);
		m_EndWattHour.GetValue(dwValue);		pStep->fEndWh = m_pDoc->WattHourUnitTrans(dwValue);	//(dwValue*I_UNIT_FACTOR);
		m_EndTemp.GetValue(dwValue);		pStep->fEndTemp =  (float)dwValue;	//(dwValue*I_UNIT_FACTOR);
		m_StartTemp.GetValue(dwValue);		pStep->fStartT =  (float)dwValue;	//(dwValue*I_UNIT_FACTOR);

		
		COleDateTime endTime;
		m_ctrlEndTime.GetTime(endTime);
//		endTime = m_EndTime.GetDateTime();
		pStep->ulEndTime = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() + m_nEndDay * 86400)*100;		
		pStep->ulEndTime += (m_lEndMSec/10);
		
		//pStep->nLoopInfoGoto			= m_nLoopInfoGoto;
		if(m_ctrlGoto.GetCurSel() != LB_ERR)	
			pStep->nLoopInfoGoto			= m_ctrlGoto.GetItemData(m_ctrlGoto.GetCurSel());
		pStep->nLoopInfoCycle			= m_nLoopInfoCycle;

		if(m_nLoopInfoEndVGoto > 0)
		{
			STEP *pGotoStep = (STEP*)m_pDoc->m_apStep[m_nLoopInfoEndVGoto-1];
			pStep->nLoopInfoEndVGoto       =  pGotoStep->nGotoStepID;
		}
		
		if(m_nLoopInfoEndTGoto > 0)
		{
			STEP *pGotoStep = (STEP*)m_pDoc->m_apStep[m_nLoopInfoEndTGoto-1];
			pStep->nLoopInfoEndTGoto       =  pGotoStep->nGotoStepID;
		}
		
		if(m_nLoopInfoEndCGoto > 0)
		{
			STEP *pGotoStep = (STEP*)m_pDoc->m_apStep[m_nLoopInfoEndCGoto-1];
			pStep->nLoopInfoEndCGoto       =  pGotoStep->nGotoStepID;
		}
		

		pStep->bUseActualCapa = m_bUseActualCapa;
		if(m_ctrlCapaStepList.GetCurSel() != LB_ERR)
			pStep->nUseDataStepNo = m_ctrlCapaStepList.GetItemData(m_ctrlCapaStepList.GetCurSel());
//		m_EndSocC.GetValue(dwValue);		pStep->fSocRate = (float)(dwValue);
		pStep->fSocRate = m_fSocRate;
// 		if (m_bUseActualCapa)
// 			pStep->fImpLimitLow = 1; //ljb 대기 REST 로 사용
	}

	if(m_pParent)
		m_pParent->PostMessage(WM_END_DLG_CLOSE, (WPARAM)m_nStepNo, (LPARAM) this);
}

BOOL CEndConditionDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_tooltip.RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}

void CEndConditionDlg::InitToolTips()
{
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);

	m_tooltip.AddTool(GetDlgItem(IDC_END_VTG), TEXT_LANG[3]); //"해당 전압에 도달하면 Step을 완료"
	m_tooltip.AddTool(GetDlgItem(IDC_END_CRT), TEXT_LANG[4]); //"해당 전류에 도달하면 Step을 완료"
	m_tooltip.AddTool(GetDlgItem(IDC_END_DAY_EDIT), TEXT_LANG[5]); //"입력한 시간만큼 Step을 진행후 완료"
	m_tooltip.AddTool(GetDlgItem(IDC_DATETIMEPICKER1), TEXT_LANG[5]); //"입력한 시간만큼 Step을 진행후 완료"
	m_tooltip.AddTool(GetDlgItem(IDC_END_CAP), TEXT_LANG[6]); //"입력한 용량만큼 충방전 실시"
	m_tooltip.AddTool(GetDlgItem(IDC_EDIT_CYCLE), TEXT_LANG[7]); //"Cycle 반복 구간을 주어진 횟수만큼 반복 실시"
	m_tooltip.AddTool(GetDlgItem(IDC_EDIT_GOTO), TEXT_LANG[8]); //"Cycle 반복을 완료후 입력된 Step을 이동(다른 Cycle을 시작이나 완료 Step을 입력)"
	m_tooltip.AddTool(GetDlgItem(IDC_GOTO_COMBO), TEXT_LANG[8]); //"Cycle 반복을 완료후 입력된 Step을 이동(다른 Cycle을 시작이나 완료 Step을 입력)"
	m_tooltip.AddTool(GetDlgItem(IDC_END_DELTAV), TEXT_LANG[9]); //"충전시 최고 전압에서 입력된 전압만큼 강하시 완료"
	m_tooltip.AddTool(GetDlgItem(IDC_END_DELTAI), TEXT_LANG[10]); //"입력된 전류변화가 발생시 완료"
	m_tooltip.AddTool(GetDlgItem(IDC_CHECK1), TEXT_LANG[11]); //"대기 REST 사용"
	m_tooltip.AddTool(GetDlgItem(IDC_CAPA_STEP_COMBO), TEXT_LANG[12]); // "비교할 용량 값 선택"
	m_tooltip.AddTool(GetDlgItem(IDC_SOC_RATE_EDIT), TEXT_LANG[13]); //"해당 비율 만큼 충방전 시킴"
}

//현재 Step보다 이전 Step에 비교할 Capa가 설정 되어 있는지 확인
int CEndConditionDlg::ListUpCapaStepCombo()
{
	STEP *pStep; 

	m_ctrlCapaStepList.ResetContent();
	m_ctrlCapaStepList.AddString(TEXT_LANG[14]); // "미사용"
	m_ctrlCapaStepList.SetItemData(0, 0);

	CString strTemp;
	int nCount = 1;
	//선택 가능한 List를 추가 한다.
	for(int i=0; i<m_nStepNo-1; i++)		//현재 step은 제외 하기 위해 -1한다.
	{
		pStep = (STEP*)m_pDoc->m_apStep[i];
		ASSERT(pStep);

		if(pStep->bUseActualCapa)
		{
			strTemp.Format("Step %d", i+1);
			m_ctrlCapaStepList.AddString(strTemp);
			m_ctrlCapaStepList.SetItemData(nCount++, i+1);			
		}
	}


	//현재 Step의 설정된 값을 표시 한다.
	pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	for(int j=0; j<m_ctrlCapaStepList.GetCount(); j++)
	{
		if(m_ctrlCapaStepList.GetItemData(j) == pStep->nUseDataStepNo)
		{
			m_ctrlCapaStepList.SetCurSel(j);
			break;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	//2005/12/8 가장 마지막에 설정한 Capa 만 사용하도록 고정 시킨다.
	// 다중 설정 가능하게 할려면 이부분을 삭제 
	m_ctrlCapaStepList.SetCurSel(m_ctrlCapaStepList.GetCount()-1);
	//////////////////////////////////////////////////////////////////////////
	
	OnSelchangeCapaStepCombo();
	
	return nCount;
}

void CEndConditionDlg::OnCheck1() 
{
	// TODO: Add your control notification handler code here
	UpdateEndCondition();
	STEP *pStep; 

	if(m_bUseActualCapa)		//Max 설정 수 검사 
	{
		//모든 step에 대해서 설정한 수가 MAX개 이상이면 경고 표시 
		int nCount = 0;

		for(int i=0; i<m_pDoc->m_apStep.GetSize(); i++)		//현재 step은 제외 하기 위해 -1한다.
		{
			pStep = (STEP*)m_pDoc->m_apStep[i];
			ASSERT(pStep);

			if(pStep->bUseActualCapa)
			{
				nCount++;
			}
		}
	
		/*if(nCount > 5)
		{
			AfxMessageBox("최대 용량기준값 설정 Step수를 초과 하였습니다.");
			m_bUseActualCapa = FALSE;
			UpdateData(FALSE);
		}*/
	}
	else
	{
		//다른 곳에서 현재의 Capa를 사용하고 있는지 확인하여 경고 표시 

		for(int i=0; i<m_pDoc->m_apStep.GetSize(); i++)		//현재 step은 제외 하기 위해 -1한다.
		{
			pStep = (STEP*)m_pDoc->m_apStep[i];
			ASSERT(pStep);

			if(pStep->nUseDataStepNo == m_nStepNo && pStep->fSocRate > 0.0f)
			{
				CString strTemp;
				strTemp.Format(TEXT_LANG[15], i+1); //"Step %d에서 현재 Step의 용량값을 참조 하고 있습니다. 설정을 해지 하시겠습니까?"

				if(AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION) == IDYES)
				{
					//설정되 Step해지 이전 
					pStep->nUseDataStepNo = 0;
					pStep->fSocRate = 0.0f;
				}
				else	//이전 상태로 복귀 
				{
					m_bUseActualCapa = TRUE;
					UpdateData(FALSE);
				}
			}
		}

	}
}

void CEndConditionDlg::OnSelchangeCapaStepCombo() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlCapaStepList.GetItemData(m_ctrlCapaStepList.GetCurSel()) == 0)
	{
		GetDlgItem(IDC_SOC_RATE_EDIT)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_SOC_RATE_EDIT)->EnableWindow(TRUE);
	}	
}

int CEndConditionDlg::ListUpGotoStepCombo()
{
	m_ctrlGoto.ResetContent();
	m_ctrlGoto.AddString("Next");
	m_ctrlGoto.SetItemData(0, PS_GOTO_NEXT_CYC);

	STEP *pCurStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];

	int nDefaultIndex = 0;

	STEP* pStep;
	CString strTemp;
	int nCount = 1;
	BOOL bEndStep = FALSE;

	//선택 가능한 List를 추가 한다.
	for(int i=0; i<m_pDoc->GetTotalStepNum(); i++)		//현재 step은 제외 하기 위해 -1한다.
	{
		pStep = (STEP*)m_pDoc->m_apStep[i];
		ASSERT(pStep);

		if(pStep->chType == PS_STEP_ADV_CYCLE )
		{
			if(pCurStep->nLoopInfoGoto == i+1)
			{
				nDefaultIndex = nCount;
			}

			strTemp.Format("Step %2d", i+1);
			m_ctrlGoto.AddString(strTemp);
			m_ctrlGoto.SetItemData(nCount++, i+1);	
			
		}
		else if(pStep->chType == PS_STEP_END)
		{
			if(pCurStep->nLoopInfoGoto == i+1)
			{
				nDefaultIndex = nCount;
			}

			strTemp.Format(TEXT_LANG[16]); //"완료"
			m_ctrlGoto.AddString(strTemp);
			m_ctrlGoto.SetItemData(nCount++, i+1);	
		}
	}

	// goto step check
/*	int nLoopIndex = 0;
	if(pCurStep->nLoopInfoGoto == 0)	nLoopIndex = m_nStepNo;	// m_curIndex+1;
	else nLoopIndex = pCurStep->nLoopInfoGoto-1;

	if(nLoopIndex > 0 && nLoopIndex < m_pDoc->GetTotalStepNum())
	{
		pStep = (STEP*)m_pDoc->m_apStep[nLoopIndex];
		if(pStep->chType == PS_STEP_END)
		{
			bEndStep = TRUE;
		}
	}
*/

/*	DWORD step = m_ctrlGoto.GetItemData(nDefaultIndex);	
	pCurStep = (STEP*)m_pDoc->m_apStep[step-1];
	if(pCurStep->chType == PS_STEP_END)
	{
		nDefaultIndex = 1;
	}
*/

	m_ctrlGoto.SetCurSel(nDefaultIndex);
	return 0;
}

void CEndConditionDlg::OnChangeEndDayEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData();

	//long으로 표현 가능한 날짜 (sec×100 된 숫자)
	//21474836.47sec => 248.55 Day 
	//최대 입력범위를 199일 23:59:59 으로 제한 
	if(m_nEndDay > 199)	m_nEndDay = 199;
	UpdateData(FALSE);
}

void CEndConditionDlg::OnDeltaposEndTimeMsecSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here

	CString strData;
	GetDlgItem(IDC_END_MSEC_EDIT)->GetWindowText(strData);

	int mtime = atol(strData);

/*	
	//end time은 500msec 이하 설정은 하지 못함 
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime == (500-MIN_TIME_INTERVAL))	mtime = 0;
		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if( mtime == MIN_TIME_INTERVAL)	mtime = 500;
		if(mtime >= 1000)	mtime = 0;
	}
*/
	//end time은 0msec 부터 설정 가능
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if(mtime >= 1000)	mtime = 0;
	}

	strData.Format("%03d", mtime);
	GetDlgItem(IDC_END_MSEC_EDIT)->SetWindowText(strData);	
	*pResult = 0;	
}

void CEndConditionDlg::DisplayData()
{
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	ASSERT(pStep);

	m_EndV.SetValue((double)m_pDoc->VUnitTrans(pStep->fEndV, FALSE));	
	m_EndI.SetValue((double)m_pDoc->IUnitTrans(pStep->fEndI, FALSE));	
	m_EndC.SetValue((double)m_pDoc->CUnitTrans(pStep->fEndC, FALSE));
	m_EnddV.SetValue((double)m_pDoc->VUnitTrans(pStep->fEndDV, FALSE));	
	m_EnddI.SetValue((double)m_pDoc->WattUnitTrans(pStep->fEndW, FALSE));	
	m_EndWattHour.SetValue((double)m_pDoc->WattHourUnitTrans(pStep->fEndWh, FALSE));
	m_EndTemp.SetValue((double)pStep->fEndTemp);
	m_StartTemp.SetValue((double)pStep->fStartT);

	COleDateTime endTime;
	ULONG lSecond = pStep->ulEndTime / 100;
	m_lEndMSec = (pStep->ulEndTime % 100)*10;
	m_nEndDay = lSecond / 86400;
	int nHour = lSecond % 86400;
	endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
	m_ctrlEndTime.SetTime(endTime);
	m_nLoopInfoCycle = pStep->nLoopInfoCycle;
	m_nLoopInfoGoto = pStep->nLoopInfoGoto;
	STEP * pGotoStep = m_pDoc->GetStepDataGotoID(pStep->nLoopInfoEndVGoto);
	if(pGotoStep != NULL)
		m_nLoopInfoEndVGoto = pGotoStep->chStepNo;
	else m_nLoopInfoEndVGoto = 0;

	pGotoStep = m_pDoc->GetStepDataGotoID(pStep->nLoopInfoEndTGoto);
	if(pGotoStep != NULL)
		m_nLoopInfoEndTGoto = pGotoStep->chStepNo;
	else m_nLoopInfoEndTGoto = 0;

	pGotoStep = m_pDoc->GetStepDataGotoID(pStep->nLoopInfoEndCGoto);
	if(pGotoStep != NULL)
		m_nLoopInfoEndCGoto = pGotoStep->chStepNo;
	else m_nLoopInfoEndCGoto = 0;

//	m_nLoopInfoEndVGoto = pStep->nLoopInfoEndVGoto;
//	m_nLoopInfoEndTGoto = pStep->nLoopInfoEndTGoto;
//	m_nLoopInfoEndCGoto = pStep->nLoopInfoEndCGoto;

		//20051117 SOC 종료 조건 추가 KBH
	m_bUseActualCapa = pStep->bUseActualCapa;
	m_fSocRate = pStep->fSocRate;

	UpdateData(FALSE);
}

void CEndConditionDlg::OnKillFocusEndMsec()
{
	CString strValue;
	GetDlgItem(IDC_END_MSEC_EDIT)->GetWindowText(strValue);
	int nValue = atoi(strValue);

	int nTemp = nValue % 50;
	if(nTemp > 24)
		nValue += 50 - nTemp;
	else
		nValue -= nTemp;

	if(nValue >= 1000)
		nValue = 0;
	if(nValue < 0)
		nValue = 0;
	strValue.Format("%d", nValue);
	GetDlgItem(IDC_END_MSEC_EDIT)->SetWindowText(strValue);
}

//각 Step 마다 임시 ID를 부여하고 모든 참조는 ID를 이용한다.
//Step 번호가 변경되면 ID를 이용해 Step 번호를 조회한다.
//잘라내기, 삭제시에는 ID를 지우고, 붙여넣기를 하면 새로운 ID를 부여한다.
int CEndConditionDlg::LispUpEndVGotoStepCombo()
{
	m_ctrlEndVGoto.ResetContent();
	m_ctrlEndVGoto.AddString("Next");
	m_ctrlEndVGoto.SetItemData(0, PS_GOTO_NEXT_CYC);

/*	STEP *pCurStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];

	int nDefaultIndex = 0;

	STEP* pStep;
	CString strTemp;
	int nCount = 1;
	BOOL bEndStep = FALSE;

	//선택 가능한 List를 추가 한다.
	for(int i=0; i<m_pDoc->GetTotalStepNum(); i++)		//현재 step은 제외 하기 위해 -1한다.
	{
		pStep = (STEP*)m_pDoc->m_apStep[i];
		ASSERT(pStep);

		if(pStep->chType == PS_STEP_ADV_CYCLE )
		{
			if(pCurStep->nLoopInfoGoto == i+1)
			{
				nDefaultIndex = nCount;
			}

			strTemp.Format("Step %2d", i+1);
			m_ctrlGoto.AddString(strTemp);
			m_ctrlGoto.SetItemData(nCount++, i+1);	
			
		}
		else if(pStep->chType == PS_STEP_END)
		{
			if(pCurStep->nLoopInfoGoto == i+1)
			{
				nDefaultIndex = nCount;
			}

			strTemp.Format("완료");
			m_ctrlGoto.AddString(strTemp);
			m_ctrlGoto.SetItemData(nCount++, i+1);	
		}
	}


	m_ctrlGoto.SetCurSel(nDefaultIndex);*/
	return 0;
}
