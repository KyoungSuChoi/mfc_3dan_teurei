#if !defined(AFX_COLORBUTTON_H__EB5FACDB_272F_4883_A997_659DDA42FD38__INCLUDED_)
#define AFX_COLORBUTTON_H__EB5FACDB_272F_4883_A997_659DDA42FD38__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorButton2 window
class CColorButton2 : public CButton
{
private:
	enum {	BUTTON_IN			= 0x01,
			BUTTON_OUT			= 0x02,
			BUTTON_BLACK_BORDER	= 0x04,};

	enum { FS_NORMAL = 0x00,
		   FS_BOLD = 0x01,
		   FS_ITALIC = 0x02,
		   FS_UNDERLINED = 0x04,
		   FS_STRIKETHROUGH = 0x08,
		   FS_ANTIALIAS = 0x10,};

	enum { FS_CENTER = 0x20,
		   FS_LEFT = 0x40,
		   FS_RIGHT = 0x80,};
	
// Construction
public:
	CColorButton2();
	CColorButton2(COLORREF text, COLORREF bkgnd);
	CColorButton2(COLORREF text, COLORREF bkgnd, COLORREF disabled);
	CColorButton2(COLORREF text, COLORREF bkgnd, COLORREF disabled, COLORREF light, COLORREF highlight, COLORREF shadow, COLORREF darkShadow);
	
// Attributes
public:

private:
	COLORREF m_TextColor;
	COLORREF m_BkgndColor;
	COLORREF m_DisabledBkgndColor;
	COLORREF m_Light;
	COLORREF m_Highlight;
	COLORREF m_Shadow;
	COLORREF m_DarkShadow;

	bool m_bBold;
	bool m_bItalic;
	bool m_bUnderlined;
	bool m_bStrikethrough;
	bool m_bAntialias;
		
	bool m_bCenter;
	bool m_bLeft;
	bool m_bRight;
	int m_nSize;
	CString m_szFont;

	
// Operations
public:
	void SetColor(COLORREF text, COLORREF bkgnd);
	void SetColor(COLORREF text, COLORREF bkgnd, COLORREF disabled);
	void SetColor(COLORREF text, COLORREF bkgnd, COLORREF disabled, COLORREF light, COLORREF highlight, COLORREF shadow, COLORREF darkShadow);
	void SetColorToWindowsDefault();
	void SetFontStyle( int nSize, DWORD dwStyle=0 );
	
private:
	void DrawFrame(CDC *pDC, CRect rc, int state);
	void DrawFilledRect(CDC *pDC, CRect rc, COLORREF color);
	void DrawLine(CDC *pDC, long sx, long sy, long ex, long ey, COLORREF color);
	void DrawButtonText(CDC *pDC, CRect rc, CString strCaption, COLORREF textcolor);
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorButton2)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CColorButton2();

	// Generated message map functions
protected:
	//{{AFX_MSG(CColorButton2)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORBUTTON_H__EB5FACDB_272F_4883_A997_659DDA42FD38__INCLUDED_)
